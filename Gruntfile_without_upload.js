'use strict';

module.exports = function (grunt) {

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Automatically load required Grunt tasks
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin'
  });

  // angular-gettext tools
  grunt.loadNpmTasks('grunt-angular-gettext');

  // Define the configuration for all the tasks
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: {
        src: [
          'Gruntfile.js',
          'src/app/**/*.js',
          'src/app/common/**/*.js'
        ]
      }
    },

    copy: {
      dist: {
        files: [
          {
            cwd: 'src',
            src: [
              'assets/**',
              'app/**/*.tmpl.html',
              'common/**/*.tmpl.html'
            ],
            dest: 'dist',
            expand: true
          },
          {
            src: 'src/index.html',
            dest: 'dist/index.aspx'
          },
          {
            cwd: 'src/vendor/bower_components/',
            src: [
              '**/*.woff',
              '**/*.woff2',
              '**/*.eot',
              '**/*.svg',
              '**/*.ttf'
            ],
            dest: 'dist/fonts',
            expand: true,
            flatten: true
          }
        ]
      }
    },

    clean: {
      build: {
        src: ['dist/']
      }
    },

    useminPrepare: {
      html: 'src/index.html',
      options: {
        dest: 'dist'
      }
    },
    // Concat
    concat: {
      options: {
        separator: ';'
      },
      // dist configuration is provided by useminPrepare
      dist: {}
    },

    // Uglify
    uglify: {
      options: {
        mangle: false,
        beautify: true
      },
      // dist configuration is provided by useminPrepare
      dist: {}
    },

    cssmin: {
      dist: {}
    },

    // Filerev
    filerev: {
      options: {
        encoding: 'utf8',
        algorithm: 'md5',
        length: 5
      },
      release: {
        // filerev:release hashes(md5) all assets (images, js and css )
        // in dist directory
        files: [{
          src: [
            'dist/scripts/*.js',
            'dist/styles/*.css'
          ]
        }]
      }
    },

    // Usemin
    // Replaces all assets with their revved version in html and css files.
    // options.assetDirs contains the directories for finding the assets
    // according to their relative paths
    usemin: {
      html: ['dist/*.aspx'],
      css: ['dist/styles/*.css'],
      options: {
        assetsDirs: ['dist', 'dist/styles']
      }
    },

    replace: {
      dev: {
        options: {
          patterns: [{
            match: 'environment',
            replacement: 'dev'  // replaces "@@environment" to "dev"
          }]
        },
        files: [
          {
            src: ['./dist/index.aspx'],
            dest: './dist/index.aspx'
          },
          {
            expand: true,
            flatten: true,
            src: ['./dist/scripts/*.js'],
            dest: './dist/scripts/'
          }
        ]
      },
      demo: {
        options: {
          patterns: [{
            match: 'environment',
            replacement: 'demo'  // replaces "@@environment" to "demo"
          }]
        },
        files: [
          {
            src: ['./dist/index.aspx'],
            dest: './dist/index.aspx'
          },
          {
            expand: true,
            flatten: true,
            src: ['./dist/scripts/*.js'],
            dest: './dist/scripts/'
          }
        ]
      }
    },

    exec: {
    },

    ngdocs: {
      options: {
        title: "The Dashboard - Technical Documentation",
        html5Mode: false
      },
      api: {
        src: ['src/app/**/*.js', 'src/common/**/*.js'],
        title: 'API documentation'
      }
    },

    nggettext_extract: {
      pot: {
        files: {
          'po/template.pot': ['./src/app/**/*.html', './src/app/**/*.js', './src/common/**/*.html', './src/common/**/*.js']
        }
      }
    },

    nggettext_compile: {
      all: {
        options: {
          module: 'dashboardApp'
        },
        files: {
          'src/app/translations.js': ['po/*.po']
        }
      }
    }
  });


  grunt.registerTask('build_dist', [
    'clean',
    'jshint',
    'useminPrepare',
    'concat',
    'cssmin',
    'uglify',
    'ngdocs',
    'copy',
    'filerev',
    'usemin'
  ]);

  grunt.registerTask('build', [
    'build_dist',
    'replace:dev'
  ]);

  grunt.registerTask('build_demo', [
    'build_dist',
    'replace:demo'
  ]);

  grunt.registerTask('default', ['build']);

};
