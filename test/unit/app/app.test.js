describe('Controller: helloWorldController', function () {

    // load the controller's module
    beforeEach(module('dashboardApp'));

    var helloWorldController, scope;

    beforeEach(inject(function ($controller, $rootScope) {

        // place here mocked dependencies

        scope = $rootScope.$new();

        helloWorldController = $controller('helloWorldController', {
            $scope: scope
        });
    }));

    it('should set hello = World', function () {
        expect(scope.hello).toBe("World");
    });

    it('should not set goodbye', function () {
        expect(scope.goodbye).toBeUndefined();
    });
});

