# The Dashboard 

As part of our research, we are evaluating how Continuous Assurance (CA) methodologies can be implemented using IT frameworks that specifically support CA methodology (as defined by the Institute of Internal Auditors). Electronic working paper applications have long supported auditors in conducting audit engagements in line with IIA standards, but no such applications have been forthcoming to encourage a shift towards Continuous Assurance.

"The Dashboard", developed as an open source solution as part of our research, aims to fill this gap. It is a web-based software framework that can be installed within a standard Microsoft SharePoint site, leveraging the SharePoint capabilities for user management and data storage that already exist in many enterprises without requiring extensive IT involvement.

Our hope is that by implementing a working example of CA methodology, we will aid businesses wanting to implement CA successfully while at the same time gathering new insights into CA theory and its practical application.

You are free to use The Dashboard in your own audit department, please check out our website at [https://www.the-dashboard.ch/](https://www.the-dashboard.ch/) for more details and instructions.


## Build from source

To build The Dashboard yourself, you need to check out our Gitlab repository, install the necessary dependencies and then run the grunt build code:

```
git clone https://gitlab.com/the-dashboard/the-dashboard.git
cd the-dashboard/
cp Gruntfile_without_upload.js Gruntfile.js
npm install
```

The third line is necessary because our development Gruntfile will try to automatically copy the built code to our development SharePoint. This obviously won't work for you, so you need to use the Gruntfile without automatic SharePoint upload. 

The Dashboard needs to know the base URL of the SharePoint site you want to use. You have to change this to correspond with your SharePoint site you want to copy The Dashboard to. Open `src/index.html` in a text editor and modify this part to point to your SharePoint site:

```
<script type="text/javascript">
    // SET THESE URLS TO POINT TO YOUR SHAREPOINT INSTANCE
    window.dashboardSharePointBaseUrl = '<GIVE THE FULL URL OF YOUR SHAREPOINT HERE WITH TRAILING SLASH>';
    window.dashboardSharePointApiBaseUrl = window.dashboardSharePointBaseUrl + '_api/';
</script>
```


If you have performed all of the above, you can then build (and re-build) The Dashboard using Grunt:


```
grunt build
```

If the build was successful you should find everything necessary to run The Dashboard in the `dist/` subfolder. Just copy the contents of that folder into an empty document library on your SharePoint site. Ideally you would want to use an otherwise empty site (or sub-site) as The Dashboard will create quite a few lists and libraries in that site that might confuse existing users there.


## Get in touch

If you have any questions about The Dashboard or the build does not work, please [get in touch](https://www.the-dashboard.ch/contact/).
