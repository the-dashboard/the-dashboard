'use strict';

/**
 * @ngdoc overview
 * @name auditFormsServices
 * @description
 *
 * This module provides services to retrieve audit forms and to store the data provided by the users using them.
 *
 */

angular.module('auditFormsServices', ['gettext', 'ngDialog', 'restServices', 'socialServices', 'miscServices', 'usersServices'])


/**
 * @ngdoc service
 * @name auditFormsServices.auditFormsService
 * @description
 *
 * This service supports retrieving audit forms and their details and to store the data provided by the users
 * using them.
 *
 */

  .service('auditFormsService', ['$q', 'gettext', 'ngDialog', 'restListService', 'restDocumentService', 'usersService', 'interactionService', 'subscriptionService', 'miscServices', function($q, gettext, ngDialog, restListService, restDocumentService, usersService, interactionService, subscriptionService, miscServices) {
    var auditFormsServ = {};

    // Cache for already retrieved audit forms
    var auditFormsPromise;
    var auditFormsDataByKeyword = {};
    var auditFormsDataById = {};


    // Helper function to turn retrieved REST data for audit forms data in a formatted JavaScript object
    var formatFormsData = function(restData) {
      var ret = {
        id: restData.Id,
        title: restData.Title,
        auditForm: restData.auditForm,
        sourceData: {},
        resultsData: {},
        result: restData.result,
        keywords: restData.keywords.split(',').filter(function(keyword) { return (keyword.trim() !== ''); }),
        createdInTask: restData.createdInTask,
        archivedTask: restData.archivedTask,
        createdInElement: restData.createdInElement,
        workflowStatus: restData.workflowStatus,
        workflowChanges: [],
        assigned_to: restData.AssignedToId,
        created_at: new Date(restData.Created),
        last_modified_at: new Date(restData.Modified),
        last_modified_by: restData.EditorId
      };

      try {
        ret.sourceData = JSON.parse(restData.sourceData);
      } catch(e) {
        console.log('ERROR in formatFormsData while parsing sourceData element:');
        console.log([restData, e]);
      }

      try {
        ret.resultsData = JSON.parse(restData.resultsData);
      } catch(e) {
        console.log('ERROR in formatFormsData while parsing resultsData element:');
        console.log([restData, e]);
      }

      try {
        ret.workflowChanges = JSON.parse(restData.workflowChanges);
      } catch(e) {
        console.log('ERROR in formatFormsData while parsing workflowChanges element:');
        console.log([restData, e]);
      }

      try {
        ret.interactionSummary = JSON.parse(restData.interactionSummary);
      } catch(e) {
        console.log('ERROR in formatFormsData while parsing workflowChanges element:');
        console.log([restData, e]);
        ret.interactionSummary = {};
      }

      return ret;
    };


    // Helper function to do the inverse, turning a formatted audit forms data element into a REST element
    var formsDataToRest = function(formsData) {
      // We need to clean-up sourceData => don't serialize anything that starts with '$' or '_' (internal data)
      var cleanedSourceData = {};
      if(formsData.sourceData) {
        Object.keys(formsData.sourceData).forEach(function(key) {
          if(key.substr(0, 1) !== '_' && key.substr(0, 1) !== '$') {
            cleanedSourceData[key] = formsData.sourceData[key];
          }
        });
      }

      return {
        'Title': formsData.title,
        'auditForm': formsData.auditForm,
        'sourceData': JSON.stringify(cleanedSourceData),
        'resultsData': JSON.stringify(formsData.resultsData ? formsData.resultsData : {}),
        'result': formsData.result,
        'keywords': formsData.keywords.join(',') + ',',
        'createdInTask': formsData.createdInTask,
        'archivedTask': formsData.archivedTask,
        'createdInElement': formsData.createdInElement,
        'workflowStatus': formsData.workflowStatus,
        'workflowChanges': JSON.stringify(formsData.workflowChanges ? formsData.workflowChanges : []),
        'assignedToId': formsData.assigned_to
      };
    };


    // Helper function to turn keyword into a regular expression
    var keywordToRegexp = function(keyword, isTemplate) {
      if(isTemplate) {
        return keyword.split(/\{\{[^\}]+\}\}/).map(function(part) { return miscServices.escapeForRegexp(part); }).join('.*');
      } else {
        return miscServices.escapeForRegexp(keyword);
      }
    };

    // Helper function to find all promise keys that *include* the given keyword
    var includingPromiseKeys = function(keyword) {
      // Idea: for the given `keyword`, we check whether there is a promise which matches this `keyword`
      //       as a regular expression

      return Object
        .keys(auditFormsDataByKeyword)
        .filter(function(key) {
          return (auditFormsDataByKeyword[key] !== undefined && keyword.match(key));
        });
    };


    /**
     * @ngdoc method
     * @name auditFormsServices.auditFormsService#getAllForms
     * @methodOf auditFormsServices.auditFormsService
     * @kind function
     *
     * @description
     * This method retrieves the details for all audit forms in the database, as a Object with the audit forms'
     * shortKeys as key and the formatted audit form as the values.
     *
     * @returns {Promise} A promise which resolves to the Object with the audit forms by shortKey.
     *
     */

    auditFormsServ.getAllForms = function() {
      if (auditFormsPromise === undefined) {
        auditFormsPromise = restDocumentService.getAllFiles('audit_forms')
          .then(
            function(res) {
              var auditForms = {};

              if(!res || !res.data || !res.data.d || !res.data.d.results || !Array.isArray(res.data.d.results)) {
                console.log('ERROR - The data returned from the server while trying to retrieve audit forms was not in the expected format:');
                console.log(res);

                return $q.reject(gettext('The data returned from the server while trying to retrieve audit forms was not in the expected format.'));
              }

              var resolveUriPromise = $q.resolve();

              res.data.d.results.forEach(function(d) {
                auditForms[d.shortKey] = {
                  id: d.Id,
                  title: d.Title,
                  shortKey: d.shortKey,
                  newFormIcon: d.newFormIcon,
                  resultExpression: d.resultExpression
                };

                try {
                  if(d.tabs) {
                    auditForms[d.shortKey].tabs = JSON.parse(d.tabs);
                  }
                } catch(e) {
                  console.log('An ERROR occured trying to parse the tabs for the audit form ' + d.shortKey + ':');
                  console.log(e);
                  console.log('Most likely the tabs JSON for this audit form is invalid.');

                  auditForms[d.shortKey].tabs = [];
                }

                try {
                  if(d.additionalSources) {
                    auditForms[d.shortKey].additionalSources = JSON.parse(d.additionalSources);
                  }
                } catch(e) {
                  console.log('An ERROR occured trying to parse the additionalSources for the audit form ' + d.shortKey + ':');
                  console.log(e);
                  console.log('Most likely the additionalSources JSON for this audit form is invalid.');

                  auditForms[d.shortKey].additionalSources = [];
                }

                try {
                  auditForms[d.shortKey].iconMap = JSON.parse(d.iconMap);
                } catch(e) {
                  console.log('An ERROR occured trying to parse the iconMap for the audit form ' + d.shortKey + ':');
                  console.log(e);
                  console.log('Most likely the iconMap JSON for this audit form is invalid.');

                  auditForms[d.shortKey].iconMap = {};
                }

                resolveUriPromise = resolveUriPromise
                  .then(function() {
                    return restDocumentService.expandFilePath(d.File.__deferred.uri);
                  })
                  .then(function(path) {
                    auditForms[d.shortKey].formUrl = path[0];
                  });
              });

              // TODO: Maybe factor resolving the URLs out when they are actually needed to improve performance...

              return resolveUriPromise
                .then(function() {
                  return auditForms;
                });
            },
            function(err) {
              console.log('An error occured retrieving audit forms:');
              console.log(err);

              return $q.reject(gettext('An error occured retrieving audit forms.'));
            }
          );
      }

      return auditFormsPromise;
    };



    /**
     * @ngdoc method
     * @name auditFormsServices.auditFormsService#getAuditForm
     * @methodOf auditFormsServices.auditFormsService
     * @kind function
     *
     * @description
     * This method retrieves the details for the given auditFormKey.
     *
     * @param {String} auditFormKey    The short key for the given audit form to be retrieved.
     *
     * @returns {Promise} A promise which resolves to the given audit form.
     *
     */

    auditFormsServ.getAuditForm = function(auditFormKey) {
      return auditFormsServ
        .getAllForms()
        .then(function(auditForms) {
          return auditForms[auditFormKey];
        });
    };



    /**
     * @ngdoc method
     * @name auditFormsServices.auditFormsService#findFormData
     * @methodOf auditFormsServices.auditFormsService
     * @kind function
     *
     * @description
     * This method searches for filled-in forms that match at least one of the `keywords` given. Optionally, you can
     * search by template, which means that you can pass keyword templates (including "{{placeholder}}" placeholders)
     * and the service will search all form data that could be the result of such a template.
     *
     * @param {Array}     keywords      An array of keywords that are used to query for existing audit forms data
     *                                  (`OR` linked, so only one keyword needs to match).
     * @param {boolean=}  isTemplate    If `isTemplate` is `true`, whenever the keyword contains template placeholders
     *                                  (as `{{placeholder}}`), we will perform partial matches irrespective of the
     *                                  value the placeholder was replaced with.
     *
     * @returns {Promise} A promise which resolves to an array with all audit form data matching at least one keyword.
     *
     */

    auditFormsServ.findFormData = function(keywords, isTemplate) {
      var keywordPromises = [];
      var regexpKeywords = [];
      var ourPromise;

      // TODO: Remove debug code
      console.log('findFormData called: ');
      console.log([keywords, isTemplate]);


      keywords.forEach(function(keyword) {
        var matchingPromises = includingPromiseKeys(keyword);

        // Do we have an including promise? Then we can use just the first one.
        if(matchingPromises.length > 0) { keywordPromises.push(auditFormsDataByKeyword[matchingPromises[0]]); }

        // TODO: Remove debug code
        console.log('findFormData: Tried to match existing promises:');
        console.log([keywords, keyword, matchingPromises, keywordPromises]);

        // Prepare array of keywords that have been turned into regular expression search strings
        regexpKeywords.push(keywordToRegexp(keyword, isTemplate));
      });

      if(keywordPromises.length < keywords.length) {
        // We are missing data and thus need to retrieve it ourselves

        var searchString = keywords
          .map(function(keywordToSplit) {
            if(isTemplate) {
              return "(" + keywordToSplit
                .split(/\{\{[^\}]+\}\}/)
                .filter(function(keywordPart) {
                  return keywordPart.trim() !== '';
                })
                .map(function(keywordPart) {
                  return "substringof('" + keywordPart + "', keywords)";
                })
                .join(" and ") + ")";
            } else {
              return "substringof('" + keywordToSplit + ",', keywords)";
            }
          })
          .join(' or ');

        // TODO: Remove debug code
        console.log('findFormData: Calling getMergedFilteredItems on audit_forms_data with search string: ' + searchString);

        ourPromise = restListService.getMergedFilteredItems('audit_forms_data', searchString).then(
          function(res) {
            // Success - got data from the server

            // Are we getting the right data?
            if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
              // Log error
              console.log('ERROR in findFormData: The obtained data object is not formatted as expected:');
              console.log(res);

              // Reject promise
              return $q.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
            }

            // Ok, let's map the entries.
            return res.data.d.results.map(formatFormsData);

            // Done.
          });

        // We fill the promises cache for all regexpKeywords that do not yet have a matching entry
        regexpKeywords.forEach(function(regexpKeyword) {
          if(auditFormsDataByKeyword[regexpKeyword] === undefined) {
            auditFormsDataByKeyword[regexpKeyword] = ourPromise;
          }
        });

      } else {
        // We have all the data and just need to combine it
        ourPromise = $q.all(keywordPromises)
          .then(function(arrayOfArrays) {
            // TODO: Remove debug code
            console.log('findFormData: Received data from existing promises:');
            console.log([keywordPromises, arrayOfArrays]);

            var ret = [];
            arrayOfArrays.forEach(function(arr) {
              ret = ret.concat(arr);
            });

            return ret;
          });
      }

      // We need to clean-up and filter again for the right keywords (as we might have overshot)
      return ourPromise
        .then(function(data) {
           // TODO: Remove debug code
           console.log('findFormData: Received raw data, will now filter:');
           console.log(data);

           return data
             .filter(function(el) {
               return regexpKeywords.findIndex(function(regexpKeyword) {
                 // At least one keyword needs to match
                 return el.keywords.findIndex(function(keyword) {
                   return keyword.match(regexpKeyword);
                 }) !== -1;
               }) !== -1;
             });
        });
    };




    /**
     * @ngdoc method
     * @name auditFormsServices.auditFormsService#getFormData
     * @methodOf auditFormsServices.auditFormsService
     * @kind function
     *
     * @description
     * This method returns form data for a single form data element, retrieved by its SharePoint-internal ID.
     *
     * @param {integer} formDataId    The SharePoint-internal ID for the audit forms data element to be retrieved.
     *
     * @returns {Promise} A promise which resolves to the form data with the given `formDataId`.
     *
     */

    auditFormsServ.getFormData = function(formDataId) {
      if(auditFormsDataById[formDataId] !== undefined) {
        return auditFormsDataById[formDataId];
      }

      auditFormsDataById[formDataId] = restListService
        .getItemById('audit_forms_data', formDataId)
        .then(
          function(res) {
          // Are we getting the right data?
          if (!res || !res.data || !res.data.d) {
            // Log error
            console.log('ERROR in getFormData: The obtained data object is not formatted as expected:');
            console.log(res);

            // Reject promise
            return $q.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
          }

          // Ok, we got the right data, we need to reformat it and return it.
          return formatFormsData(res.data.d);
          },
          function(err) {
            console.log('ERROR while trying to retrieve getFormData for formDataId = ' + formDataId + ':');
            console.log(err);

            return $q.reject(gettext('An error occured while trying to retrieve form data: ') + err);
          }
        );

      return auditFormsDataById[formDataId];
    };




    /**
     * @ngdoc method
     * @name auditFormsServices.auditFormsService#saveFormData
     * @methodOf auditFormsServices.auditFormsService
     * @kind function
     *
     * @description
     * This method creates a new or updates an existing audit form data entry for the given `formsData` data element.
     * It will convert the `formsData` object (which needs to be in the form returned by this service) to a REST
     * object prior to delivery to SharePoint.
     *
     * @param {Object}   formsData    The object with the form data to be stored. If the `formsData` object has a set
     *                                `id`, the object with this `id` will be updated. If no `id` is set, it will
     *                                create a new list entry for this audit forms data.
     *
     * @returns {Promise} A promise which resolves to the form data with the given ID.
     *
     */

    auditFormsServ.saveFormData = function(formsData) {
      var restData = formsDataToRest(formsData);
      var savePromise;

      if(formsData.id !== undefined && formsData.id !== null && formsData.id !== -1) {
        // Existing object, update in the database
        savePromise = restListService.updateItem('audit_forms_data', formsData.id, restData);
      } else {
        // New object, create in the database
        savePromise = restListService.addItem('audit_forms_data', restData);
      }

      // We update the provided object with new modification date and user and with its new ID (unchanged if update)
      return savePromise
        .then(function(res) {
          // We reset all promises which include this object
          formsData.keywords.forEach(function(keyword) {
            includingPromiseKeys(keyword).forEach(function(promiseKey) {
              auditFormsDataByKeyword[promiseKey] = undefined;
            });
          });

          // Updating modified date and modifiying user
          formsData.last_modified_at = new Date();

          if(res.data && res.data.d && res.data.d.Id !== undefined) {
            formsData.id = res.data.d.Id;
          }

          return usersService.getCurrentUser().then(function(user) {
            formsData.last_modified_by = user.id;

            // Return the modified formsData object
            return formsData;
          });
        });
    };



    /**
     * @ngdoc method
     * @name auditFormsServices.auditFormsService#deleteFormData
     * @methodOf auditFormsServices.auditFormsService
     * @kind function
     *
     * @description
     * This method deletes form data for a single form data element, which needs to be specified by its SharePoint-
     * internal ID.
     *
     * @param {integer} formDataId    The SharePoint-internal ID for the audit forms data element to be deleted.
     *
     * @returns {Promise} A promise which resolves to true if the form data was successfully deleted.
     *
     */

    auditFormsServ.deleteFormData = function(formDataId) {
      return restListService
        .deleteItem('audit_forms_data', formDataId)
        .then(
          function() {
            // We also remove news items and interactions for this auditFormData
            // (in the background, so we can return earlier...)
            subscriptionService
              .removeNewsBySource('auditFormData', formDataId)
              .then(
                function() {
                  return interactionService.removeInteractionsByTarget('auditFormData', formDataId);
                }
              );

            return true;
          },
          function(err) {
            console.log('ERROR while trying to delete getFormData for formDataId = ' + formDataId + ':');
            console.log(err);

            return $q.reject(gettext('An error occured while trying to delete form data: ') + err);

          }
        );
    };





    /**
     * @ngdoc method
     * @name auditFormsServices.auditFormsService#archiveFormData
     * @methodOf auditFormsServices.auditFormsService
     * @kind function
     *
     * @description
     * This method retrieves all audit forms data which references as source task the task with the ID `oldTaskId`. It
     * then updates all of them so that they reference the archived task `archivedTaskId`.
     *
     * It also sets the workflow status of the audit form data element to ``Archived'', meaning that it cannot be
     * changed afterwards anymore.
     *
     * @param {integer} oldTaskId       The SharePoint-internal ID of the original task prior to archiving. We will
     *                                  retrieve audit forms data for which `createdInTask` matches this ID.
     *
     * @param {integer} archivedTaskId  The new SharePoint-internal ID of the archived task in the archive library.
     *                                  We will set `createdInTask` to this ID.
     *
     * @returns {Promise} A promise which resolves to true if the form data was successfully updated.
     *
     */

    auditFormsServ.archiveFormData = function(oldTaskId, archivedTaskId) {
      var currUser;

      return usersService.getCurrentUser()
        .then(
          function(_currUser) {
            currUser = _currUser;

            // We get all form data which is linked to the oldTaskId and not yet archived...
            return restListService.getMergedFilteredItems('audit_forms_data', 'createdInTask eq ' + oldTaskId + ' and archivedTask eq 0', 'Id, createdInTask, archivedTask, workflowStatus, workflowChanges');
          }
        )

        // Now we need to process each of those and update them...
        .then(
          function(res) {
            if(!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
              console.log('ERROR: Could not archive audit forms data: The response received was not in the expected format:');
              console.log(res);

              return $q.reject(gettext('Could not archive audit forms data: The response received was not in the expected format.'));
            }

            // Go through each one of them.
            var processPromise = $q.resolve();

            res.data.d.results.forEach(
              function(formData) {
                // Update the workflowChanges...
                var workflowChanges;
                try {
                  workflowChanges = JSON.parse(formData.workflowChanges);
                } catch(e) {
                  workflowChanges = [];
                }


                workflowChanges.push({
                  status: 'Archived',
                  change_at: new Date(),
                  change_by: currUser.id
                });


                var updateItem = {
                  Id: formData.Id,
                  createdInTask: archivedTaskId,
                  archivedTask: true,
                  workflowStatus: 'Archived',
                  workflowChanges: JSON.stringify(workflowChanges)
                };


                // Send an update query.
                processPromise = processPromise.then(
                  function() {
                    return restListService.updateItem('audit_forms_data', formData.Id, updateItem);
                  }
                );
              }
            );

            return processPromise;
          }
        );

    };








    /**
     * @ngdoc method
     * @name auditFormsServices.auditFormsService#updateWorkflowStatus
     * @methodOf auditFormsServices.auditFormsService
     * @kind function
     *
     * @description
     * This method updates the workflow status (and the corresponding changes array) for the given audit form data
     * element. Note that this data element needs to be already saved to the database (e.g. it must contain a valid
     * `id` value).
     *
     * This function differs from just saving the element insofar as that any other changes that might already
     * exist in the given `formsData` object are *not* saved to the database. You can thus update the workflow status
     * without accidentally also saving changes the user might not yet have been ready to save.
     *
     * @param {Object} formsData    The data element whose workflow status should be updated. This method will change
     *                              the object by updating the `workflowStatus` and `workflowChanges` values.
     * @param {string} newStatus    The new status that should be assigned to the given audit data element.
     *
     * @returns {Promise} A promise which resolves to the updated `formsData` object on success.
     *
     */

    auditFormsServ.updateWorkflowStatus = function(formsData, newStatus) {
      // We need to get the user ID of the current user
      return usersService.getCurrentUser().then(function(currUser) {
        // Push the new status to the workflow statuses
        formsData.workflowChanges.push({
          status: newStatus,
          change_at: new Date(),
          change_by: currUser.id
        });

        // Update the current status
        formsData.workflowStatus = newStatus;

        return restListService.updateItem('audit_forms_data', formsData.id, {
          workflowChanges: JSON.stringify(formsData.workflowChanges),
          workflowStatus: formsData.workflowStatus
        });
      });
    };



    /**
     * @ngdoc method
     * @name auditFormsServices.auditFormsService#showFormData
     * @methodOf auditFormsServices.auditFormsService
     * @kind function
     *
     * @description
     * This method displays a dialog window with the audit form loaded with the specified audit forms data (specified
     * by the SharePoint-internal ID).
     *
     * @param {integer} formDataId    The SharePoint-internal ID for the audit forms data element to be retrieved.
     *
     * @returns {Promise} A promise which resolves to the ngDialog object of the opened dialog.
     *
     */

    auditFormsServ.showFormData = function(formDataId) {
      return auditFormsServ
        .getFormData(formDataId)
        .then(
          function (formData) {
            // We show this form data in the dialog box
            return ngDialog.open({
              template: 'common/directives/auditFormsFormDialog.tmpl.html',
              className: 'ngdialog-theme-default',
              data: {
                'auditFormData': formData,
                'readOnly': true
              }
            });
          },
          function (err) {
            console.log('An ERROR occured while trying to retrieve the form data ID ' + formDataId + ' to display in the dialog box:');
            console.log(err);

            return $q.reject(gettext('An error occured while trying to open this audit form.'));
          }
        );
    };






    return auditFormsServ;

  }]);


