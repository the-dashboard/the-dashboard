'use strict';

/**
 * @ngdoc overview
 * @name objectExplorerServices
 * @description
 *
 * This module provides services to manipulate the objects and their mappings to elements for the Object
 * Explorer functionality.
 *
 */

angular.module('objectExplorerServices', ['gettext', 'restServices', 'usersServices'])


  /**
   * @ngdoc service
   * @name objectExplorerServices.objectExplorerService
   * @description
   *
   * This service allows to manipulate the objects and their corresponding mapping which are
   * used by the object explorer directives defined in
   * {@link objectExplorerDirectives `objectExplorerDirectives`}.
   *
   * An object is just a reference object, holding a title, a description, an optional link and
   * arbitrary additional "columns" (keys with values). They can be for example elements from
   * outside risk management or similar systems that you want to link to your ORA dashboards.
   *
   */

  .service('objectExplorerService', ['$q', 'gettext', 'restListService', 'usersService', function($q, gettext, restListService, usersService) {
    var objectExplorerServ = {};

    // Cache for already retrieved objects and mappings
    var objectMappingPromise = {};

    // Cache for already retrieved objects by their type
    var objectsByTypePromise = {};

    // Helper function to clear those when backend data was changed
    var clearPromises = function() {
      objectMappingPromise = {};
      objectsByTypePromise = {};
    };



    /**
     * @ngdoc method
     * @name objectExplorerServices.objectExplorerService#formatObjectFromREST
     * @methodOf objectExplorerServices.objectExplorerService
     * @kind function
     *
     * @description
     * This method takes as input an object explorer object as returned from the SharePoint REST API as list item
     * and returns an object that is formatted suitably for our purposes.
     *
     * @param {Object} restObject The list item for the object as returned by the SharePoint REST API (one element
     *                            of the `results` array of any SharePoint query response).
     *
     * @returns {Object} The reformatted object as it would be expected by the object explorer directives.
     *
     */

    objectExplorerServ.formatObjectFromREST = function(restObject) {
      var retObject = {
        id: restObject.Id,
        title: restObject.Title,
        description: restObject.Description,
        link: restObject.Link
      };

      try {
        retObject.additionalColumns = JSON.parse(restObject.additionalColumnsJSON);
      } catch(err) {
        // If the additional columns JSON is invalid, we fail gracefully and log the error,
        // but otherwise proceed with an empty additional columns object.
        console.log('Could not parse additionalColumnsJSON for object ID ' + restObject.Id + ':');
        console.log(err);
        retObject.additionalColumns = {};
      }

      return retObject;
    };



    /**
     * @ngdoc method
     * @name objectExplorerServices.objectExplorerService#hasMappingRights
     * @methodOf objectExplorerServices.objectExplorerService
     * @kind function
     *
     * @description
     * This method resolves to a boolean value that indicates whether the currently logged-in user has write access
     * to the SharePoint list that defines the object mappings in the system. This defines whether the user may
     * map objects to process elements.
     *
     * @returns {Promise} A promise that resolves to whether the currently logged-in user has mapping rights.
     *
     */

    objectExplorerServ.hasMappingRights = function() {
      return usersService.hasRights('object_explorer_mapping');
    };


    /**
     * @ngdoc method
     * @name objectExplorerServices.objectExplorerService#getMappedObjects
     * @methodOf objectExplorerServices.objectExplorerService
     * @kind function
     *
     * @description
     * This method retrieves a list of all objects which are mapped to the given `element` on the `mappingKey`
     * and which are of type `objectType`.
     *
     * @param {string} mappingKey  The mapping key for which mapped objects should be retrieved (this is
     *                             given for each object explorer and is usually an unique identifier for
     *                             each object explorer).
     *
     * @param {string=} element The element for which mapped objects should be retrieved (optional, if not
     *                          given all elements for the given mappingKey will be retrieved).
     *
     * @param {string} objectType The type of mapped objects that should be retrieved.
     *
     * @returns {Promise} A promise which resolves to an array of objects including their additional
     *                    information.
     *
     */

    objectExplorerServ.getMappedObjects = function(mappingKey, element, objectType) {
      var deferred;

      // Is a non-empty element and objectType specified? Otherwise we will reject
      // the request.
      if(mappingKey === undefined || mappingKey === '' || objectType === undefined || objectType === '') {
        return $q.reject(gettext('You need to specify the mapping key and object type for which you want to retrieve mapped objects.'));
      }

      if(element === undefined) {
        element = '';
      }

      // Does a promise already exist?
      if(objectMappingPromise[mappingKey + '|||' + element + '|||' + objectType]) {
        // If yes, we can simply return it.
        return objectMappingPromise[mappingKey + '|||' + element + '|||' + objectType];
      }

      // Otherwise we need to create a new promise and get new data
      deferred = $q.defer();
      objectMappingPromise[mappingKey + '|||' + element + '|||' + objectType] = deferred.promise;

      // First we need to get the IDs of all relevant objects.
      restListService.getFilteredItems('object_explorer_mapping', 'MappingKey eq \'' + mappingKey.replace("'", "''") + '\'' +
                                                                (element !== '' ? ' and Element eq \'' + element.replace("'", "''") + '\'' : '') +
                                                                ' and Object/Type eq \'' + objectType.replace("'", "''") + '\'',
                                                                'Object/ID', 'Object/ID,Object/Type')
        .then(function(res) {
          // Malformed response? Return rejected promise.
          if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
            // Reject promise.
            return $q.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
          }

          // If there are no objects, we can return an empty array.
          if(res.data.d.results.length < 1) {
            return [];
          }

          // Now we need to combine all these IDs into a CAML
          var camlQuery = '<View>\n' +
            '<ViewFields>\n' +
            ' <FieldRef Name="Id" />\n' +
            ' <FieldRef Name="Title" />\n' +
            ' <FieldRef Name="Type" />\n' +
            ' <FieldRef Name="Description" />\n' +
            ' <FieldRef Name="additionalColumnsJSON" />\n' +
            ' <FieldRef Name="Link" />\n' +
            '</ViewFields>\n' +
            '<Query>\n' +
            ' <Where>\n' +
            '  <In>\n' +
            '   <FieldRef Name="ID" />\n' +
            '   <Values>\n';

          res.data.d.results.forEach(function(d) {
            if(d.Object && d.Object.ID) {
              camlQuery += '     <Value Type="Number">' + d.Object.ID + '</Value>\n';
            }
          });

          camlQuery +=  '   </Values>\n' +
            '  </In>\n' +
            ' </Where>\n' +
            '</Query>\n' +
            '</View>';

          // Retrieve CAML results of mapped objects from server
          return restListService.getItemsViaCAML('object_explorer_objects', camlQuery)
            .then(function(res) {
              // Are we getting the right data?
              if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
                // Returns already rejected promise
                return $q.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
              }

              // Yes, return the results array.
              return res.data.d.results;
            });
        })
        .then(
          function(resultsList) {
            // Success - got data from the server

            // TODO: Remove debug code
            console.log('Success in getting mapped objects for mappingKey ' + mappingKey + ', element ' + element + ', and type ' + objectType + '!');
            console.log(resultsList);

            var mappedObjects = resultsList.map(objectExplorerServ.formatObjectFromREST);

            deferred.resolve(mappedObjects);
          },
          function(restErr) {
            return $q.reject(gettext('An error occurred:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
          }
        )
        .catch(
          function(err) {
            deferred.reject(err);
          }
        );

      // Return newly created promise.
      return objectMappingPromise[mappingKey + '|||' + element + '|||' + objectType];
    };




    /**
     * @ngdoc method
     * @name objectExplorerServices.objectExplorerService#getObjectsByType
     * @methodOf objectExplorerServices.objectExplorerService
     * @kind function
     *
     * @description
     * This method retrieves a list of all objects which are of type `objectType`.
     *
     * @param {string} objectType The type of mapped objects that should be retrieved.
     *
     * @returns {Promise} A promise which resolves to an array of objects including their additional
     *                    information.
     *
     */

    objectExplorerServ.getObjectsByType = function(objectType) {
      var deferred;

      // Is a non-empty objectType specified? Otherwise we will reject
      // the request.
      if(objectType === undefined || objectType === '') {
        return $q.reject(gettext('You need to specify the object type for which you want to retrieve all objects.'));
      }

      // Does a promise already exist?
      if(objectsByTypePromise[objectType]) {
        // If yes, we can simply return it.
        return objectsByTypePromise[objectType];
      }

      // Otherwise we need to create a new promise and get new data
      deferred = $q.defer();
      objectsByTypePromise[objectType] = deferred.promise;

      var mappingsByObject = {};
      var objectList = [];

      // First we need to get all mappings for objects of this type.
      var mappingPromise = restListService.getMergedFilteredItems('object_explorer_mapping', 'Object/Type eq \'' + objectType.replace("'", "''") + '\'', 'Object,Object/ID,Object/Type,Element', 'Object/ID,Object/Type')
        .then(
          function(res) {
            // Malformed response? Return rejected promise.
            if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
              // Reject promise.
              return $q.reject(gettext('An error occurred: The obtained mapping data is not formatted as expected.'));
            } else {
              res.data.d.results.forEach(function(d) {
                if(d.Object && Array.isArray(mappingsByObject[d.Object.ID])) {
                  // Appending mapping to existing array
                  mappingsByObject[d.Object.ID].push(d.Element);
                } else {
                  // Set new array
                  mappingsByObject[d.Object.ID] = [ d.Element ];
                }
              });
            }
          },
          function(restErr) {
            deferred.reject(gettext('An error occurred while retrieving mappings:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
          }
        );


      // Then we need to get all objects of this type.
      var objectPromise = restListService.getMergedFilteredItems('object_explorer_objects', 'Type eq \'' + objectType.replace("'", "''") + '\'', undefined, undefined, 'Title')
        .then(
          function(res) {
            // Malformed response? Return rejected promise.
            if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
              // Reject promise.
              return $q.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
            } else {
              objectList = res.data.d.results.map(objectExplorerServ.formatObjectFromREST);
            }
          },
          function(restErr) {
            deferred.reject(gettext('An error occurred while retrieving objects:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
          }
        );

      // Then we need to put the two together.
      $q.all([mappingPromise, objectPromise])
        .then(
          function() {
            // TODO - Remove debug code
            console.log('Mappings for object type ' + objectType + ' retrieved:');
            console.log(mappingsByObject);

            // Combine objects with their mappings
            objectList.forEach(function(d) {
              d.mappings = (Array.isArray(mappingsByObject[d.id]) ? mappingsByObject[d.id] : []);
            });

            // Resolve with combined object list
            deferred.resolve(objectList);
          },
          function(err) {
            deferred.reject(err);
          }
        );

      // Return newly created promise.
      return objectsByTypePromise[objectType];
    };



    /**
     * @ngdoc method
     * @name objectExplorerServices.objectExplorerService#addMapping
     * @methodOf objectExplorerServices.objectExplorerService
     * @kind function
     *
     * @description
     * This method adds a new mapping from the given `element` to the given `objectId`.
     *
     * @param {string} objectId The ID of the object for which the mapping should be created.
     *
     * @param {string} mappingKey The mapping key which should be stored in the mapping.
     *
     * @param {string} element The Element which should be stored in the mapping.
     *
     * @returns {Promise} A promise which resolves when the mapping has been successfully created.
     *
     */

    objectExplorerServ.addMapping = function (objectId, mappingKey, element) {
      var mappingItem = {
        'ObjectId': objectId,
        'MappingKey': mappingKey,
        'Element': element
      };

      return restListService.addItem('object_explorer_mapping', mappingItem)
        .then(
          function(res) {
            if(!res || !res.data || !res.data.d || res.data.d.Id === undefined) {
               console.log('An invalid response was received from the server while trying to add a mapping for object ' + objectId + ' and element ' + element + ':');
               console.log(res);

               return $q.reject(gettext('The response from the server when trying to add the mapping was invalid!'));
            }

            // We need to remove all existing promises to ensure no old information is returned
            clearPromises();

            return res.data.d.Id;
          },
          function(restErr) {
            return $q.reject(gettext('An error occurred while adding the mapping:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
          }
        );
    };



    /**
     * @ngdoc method
     * @name objectExplorerServices.objectExplorerService#getMapping
     * @methodOf objectExplorerServices.objectExplorerService
     * @kind function
     *
     * @description
     * This method obtains the mapping between the given `mappingKey`, `element` and the given `objectId` or `undefined` if it does not
     * exist.
     *
     * @param {string} objectId The ID of the object for which the mapping should be retrieved.
     *
     * @param {string} mappingKey The mapping key for which the mapping should be retrieved.
     *
     * @param {string} element The Element for which the mapping should be retrieved.
     *
     * @returns {Promise} A promise which resolves to the obtained mapping or `undefined` if it does not exist.
     *
     */

    objectExplorerServ.getMapping = function (objectId, mappingKey, element) {
      return restListService.getFilteredItems('object_explorer_mapping', 'Object eq ' + objectId + ' and MappingKey eq \'' + mappingKey.replace("'", "''") + '\' and Element eq \'' + element.replace("'", "''") + '\'')
        .then(
          function(res) {
            if (!res || !res.data || !res.data.d || !res.data.d.results || !Array.isArray(res.data.d.results)) {
              console.log('An invalid response was received from the server while trying to obtain the mapping for object ' + objectId + ', mappingKey ' + mappingKey + ', and element ' + element + ':');
              console.log(res);

              return $q.reject(gettext('The response from the server when trying to retrieve the mapping was invalid!'));
            }

            if(res.data.d.results.length < 1) {
              // No mapping found
              return undefined;
            } else {
              // We obtain the retrieved mapping
              return res.data.d.results[0];
            }
          },
          function(restErr) {
            return $q.reject(gettext('An error occurred while retrieving the mapping:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
          }
        );
    };


    /**
     * @ngdoc method
     * @name objectExplorerServices.objectExplorerService#removeMapping
     * @methodOf objectExplorerServices.objectExplorerService
     * @kind function
     *
     * @description
     * This method removes a mapping between the given `element` and the given `objectId`.
     *
     * @param {string} objectId The ID of the object for which the mapping should be removed.
     *
     * @param {string} mappingKey The mapping key for which the mapping should be removed.
     *
     * @param {string} element The Element for which the mapping should be removed.
     *
     * @returns {Promise} A promise which resolves when the mapping has been successfully removed.
     *
     */

    objectExplorerServ.removeMapping = function (objectId, mappingKey, element) {
      // First we need to find the mapping.
      return objectExplorerServ.getMapping(objectId, mappingKey, element)
        .then(
          function (mapping) {
            // Delete this mapping.
            return restListService.deleteItem('object_explorer_mapping', mapping.Id)
              .then(
                function () {
                  // TODO: Remove debug code.
                  console.log('Mapping between object ' + objectId + ', mappingKey ' + mappingKey + ', and element ' + element + ' has been removed.');

                  // We need to remove all existing promises to ensure no old information is returned
                  clearPromises();
                },
                function (restErr) {
                  // We need to reformat REST errors
                  return $q.reject(gettext('An error occurred while deleting the mapping:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
                }
              );
          }
        );
    };


    return objectExplorerServ;
  }]);
