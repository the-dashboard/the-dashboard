/* jshint strict:false */
/* globals d3 */
/* globals dashboardCustomColors */

/**
 * @ngdoc overview
 * @name chartServices
 * @description
 *
 * This module exposes the D3 visualization library as an AngularJS service.
 */

angular.module('chartServices', [])

  /**
   * @ngdoc service
   * @name chartServices.d3
   * @description
   *
   * This service provides a wrapper around the D3 visualization library. We also provide an additional
   * property `ourSettings`, which provides locale information, standard chart colors and standard
   * formats.
   *
   * The Dashboard currently supports the following formatters:
   *
   *  * `time`: Formats datetime values in the format yyyy-mm-dd HH:MM:SS.
   *
   *  * `number`: Formats numbers with two decimals separated by a dot and thousands separated by comma.
   *
   *  * `integer`: Formats integers without decimals, thousands are separated by comma.
   *
   * At the moment, these settings are tailored to an international, English-language audience and
   * not yet localized.
   *
   */



  .factory('d3', function() {
    /* We could declare locals or other D3.js
     specific configurations here. */

    var availableLocales = {
      'en': {
        "decimal": ".",
        "thousands": ",",
        "grouping": [3],
        "currency": ["$", ""],
        "dateTime": "%a %b %e %X %Y",
        "date": "%m/%d/%Y",
        "time": "%H:%M:%S",
        "periods": ["AM", "PM"],
        "days": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        "shortDays": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        "months": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        "shortMonths": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
      },
      'de': {
        "decimal": ".",
        "thousands": "'",
        "grouping": [3],
        "currency": ["CHF ", ""],
        "dateTime": "%a %b %e %X %Y",
        "date": "%d.%m.%Y",
        "time": "%H:%M:%S",
        "periods": ["AM", "PM"],
        "days": ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"],
        "shortDays": ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
        "months": ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
        "shortMonths": ["Jan", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"]
      }
    };

    d3.ourSettings = {};

    // Allows to change the default locale and is called by app.js when user language settings are retrieved
    d3.ourSettings.setLocale = function(locale) {
      if(!availableLocales[locale]) {
        // Use English if specified locale doesn't exist
        d3.formatDefaultLocale(availableLocales.en);
      } else {
        d3.formatDefaultLocale(availableLocales[locale]);
      }

      // The administrator can provide its own dashboardCustomColors array in the index file, in this case we use this one for our charts
      // TODO: Make configurable via a config option, not code
      if(typeof dashboardCustomColors === 'undefined') {
        d3.ourSettings.chartColors = d3.schemeSet3;
      } else {
        d3.ourSettings.chartColors = dashboardCustomColors;
      }

      // Use the following to define your own chart colors
      // d3.ourSettings.chartColors = function() { return d3.scale.ordinal()
      //   .range(["#018717", "#011e87", "#87010a", "#875301", "#018287", "#84d67e", "#d6887e", "#7f7ed6", "#f0f28a", "#a6d5fc", "#fca6a8"] };

      d3.ourSettings.formats = {};

      d3.ourSettings.formats.time = d3.timeFormat("%Y-%m-%d %H:%M:%S");
      d3.ourSettings.formats.time.keyify = function(time) { return time.toISOString(); };

      d3.ourSettings.formats.date = function(date) { return (date ? d3.timeFormat("%Y-%m-%d")(date) : ''); };
      d3.ourSettings.formats.date.keyify = function(date) { return (date ? date.toISOString().substr(0,10) : ''); };

      // We overwrite the parse function for dates, because it is too strict for our purposes...
      var iterateDateParsers = [
        d3.timeParse("%Y-%m-%d"),
        d3.timeParse("%d.%m.%Y"),
        d3.timeParse("%m/%d/%Y")
      ];
      d3.ourSettings.formats.date.parse = function(str) {
        var res;
        for(var i = 0;i < iterateDateParsers.length;i++) {
          res = iterateDateParsers[i](str);
          if(res !== null) {
            return res;
          }
        }

        return null;
      };


      d3.ourSettings.formats.number = function(num) { return (isNaN(num) ? '' : d3.format(",.2f")(num)); };
      d3.ourSettings.formats.number.keyify = function(num) { return num.toFixed(2); };
      d3.ourSettings.formats.number.parse = parseFloat;

      d3.ourSettings.formats.percentage = function(num) { return (isNaN(num) ? '' : d3.format(".1%")(num)); };
      d3.ourSettings.formats.percentage.keyify = function(num) { return (num*100).toFixed(1); };
      d3.ourSettings.formats.percentage.parse = parseFloat;

      d3.ourSettings.formats.integer = function(num) { return (isNaN(num) ? '' : d3.format(",d")(num)); };
      d3.ourSettings.formats.integer.keyify = function(num) { return num.toFixed(); };
      d3.ourSettings.formats.integer.parse = parseInt;

      d3.ourSettings.formats.yesNo = function(txt) {
        if(txt === 'yes') {
          return '✅';
        } else if(txt === 'no') {
          return '❌';
          } else {
          return '';
        }
      };
      d3.ourSettings.formats.yesNo.keyify = function(txt) { return (txt === 'yes' ? 'yes' : 'no'); };
      d3.ourSettings.formats.yesNo.parse = function(input) {
        if(input === '✅' || input.toString().trim().toLowerCase() === 'yes' || input.toString().trim().toLowerCase() === 'true' || input === 1 || input.toString().trim() === '1' || input === true) {
          return 'yes';
        } else if(input === '❌' || input.toString().trim().toLowerCase() === 'no' || input.toString().trim().toLowerCase() === 'false' || input === 0 || input.toString().trim() === '0' || input === false) {
          return 'no';
        } else {
          return '';
        }
      };

      d3.ourSettings.formats.text = function(d) { return d; };
      d3.ourSettings.formats.text.parse = function(d) { return d; };
    };

    d3.ourSettings.setLocale('en');



    // Wraps SVG text elements
    // Taken from https://bl.ocks.org/mbostock/7555321

    /* jshint ignore:start */
    d3.wrapHelper = function(text, width) {
      text.each(function() {
        var text = d3.select(this),
          words = text.text().split(/\s+/).reverse(),
          word,
          line = [],
          lineNumber = 0,
          lineHeight = 1.1, // ems
          y = text.attr("y"),
          dy = parseFloat(text.attr("dy")),
          tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
        while (word = words.pop()) {
          line.push(word);
          tspan.text(line.join(" "));
          if (tspan.node().getComputedTextLength() > width) {
            line.pop();
            tspan.text(line.join(" "));
            line = [word];
            tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
          }
        }
      });
    };
    /* jshint ignore:end */

    return d3;
  });

