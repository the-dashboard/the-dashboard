/*jshint -W018 */
'use strict';


/**
 * @ngdoc overview
 * @name dashboardTreeServices
 * @description
 *
 * This module exposes services to retrieve the hierarchical entity tree which is the foundation
 * of the "Ongoing Risk Assessment" (ORA) part of the application.
 *
 */

angular.module('dashboardTreeServices', ['gettext', 'restServices', 'miscServices'])


  /**
   * @ngdoc service
   * @name dashboardTreeServices.treeService
   * @description
   *
   * This service provides various methods to obtain the hierarchical entity tree for the "Ongoing Risk Assessment"
   * from the relevant SharePoint list ("Dashboard - Tree").
   *
   */

  .factory('treeService', ['$q', 'gettext', 'restListService', 'miscServices', function($q, gettext, restListService, miscServices) {
    var treeServ = {};


    /**
     * @ngdoc method
     * @name dashboardTreeServices.treeService#getFlatTree
     * @methodOf dashboardTreeServices.treeService
     * @kind function
     *
     * @description
     * Retrieves the full hierarchical entity tree (current and past entries) and returns it in a "flat" structured
     * object. The returned object has the following structure:
     *
     * ```
     *  flatTree[ <entity ID> ] = {
     *    id: <entity ID>,
     *    title: <name of the entity>,
     *    parent: <ID of the parent entity in the tree>,
     *    valid_from: <Datetime from which the entity exists in the tree>,
     *    valid_to: <Datetime until which the entity existed in the tree>
     *    active: <boolean value, true if now is between valid_from and valid_to>
     *  }
     * ```
     *
     * @returns {Promise} Returns a single promise that resolves to an object which contains the
     * whole hierarchical entity tree in its "flat" structure.
     */

    treeServ.getFlatTree = function() {
      var deferred = $q.defer();

      restListService.getMergedAllItems('tree').then(
        function (res) {
          var flatData, currDatetime;

          // TODO: Remove debug code
          console.log('Success!');
          console.log(res);

          // TODO: Implement caching of tree hierarchy

          // Are we getting the right data?
          if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results) || res.data.d.results.length < 1) {
            deferred.reject(gettext('An error occurred: The obtained data object is not formatted as expected or the tree list is empty.'));
            return;
          }

          // We prepare nicely formatted flat data...
          flatData = {};
          currDatetime = new Date();

          res.data.d.results.forEach(function (d) {
            flatData[d.Id] = {
              id: d.Id,
              title: d.Title,
              parent: d.ParentId,
	      hasDashboard: (!!d.hasDashboard),
              valid_from: miscServices.parseDatetime(d.Valid_from),
              valid_to: miscServices.parseDatetime(d.Valid_to),
              active: (miscServices.parseDatetime(d.Valid_from) <= currDatetime && miscServices.parseDatetime(d.Valid_to) > currDatetime),
              orderAsc: d.OrderAsc,
              nodes: []
            };
          });

          // Return flatData
          deferred.resolve(flatData);
        },
        function(errRest) {
          // TODO: Remove debug code
          console.log('Error!');
          console.log(errRest);
          deferred.reject(gettext('An error occurred:') + ' ' + gettext('Status code') + ' ' + errRest.status + ', ' + errRest.statusText);
        }
      );

      return deferred.promise;
    };


    /**
     * @ngdoc method
     * @name dashboardTreeServices.treeService#getFullHierarchy
     * @methodOf dashboardTreeServices.treeService
     * @kind function
     *
     * @description
     * Retrieves the full hierarchical entity tree (current and past entries) and returns it in a "nested" structured
     * object. The returned object has the following structure:
     *
     * ```
     *  nestedTree = {
     *    id: <ID of tree origin node>,
     *    title: <name of the tree origin node>,
     *    parent: undefined,
     *    valid_from: <Datetime from which the tree origin exists in the tree>,
     *    valid_to: <Datetime until which the tree origin existed in the tree>
     *    active: <boolean value, true if now is between valid_from and valid_to>,
     *    nodes: [
     *      {
     *        <first child of tree origin (with the same object
     *         properties as  origin, including nested nodes array)>
     *      },
     *      {
     *        <second child of tree origin (with the same object
     *         properties as  origin, including nested nodes array)>
     *      }
     *    ]
     *  }
     * ```
     *
     * @returns {Promise} Returns a single promise that resolves to an object which contains the
     * whole hierarchical entity tree in a hierarchical structure, i.e. as nested objects.
     */

    treeServ.getFullHierarchy = function() {
      var deferred, treeStart, multipleRoots;

      deferred = $q.defer();
      treeServ.getFlatTree().then(
        function(flatData) {
          // Now we create the tree (good thing: JavaScript passes objects by reference!)
          treeStart = undefined;
          multipleRoots = false;
          Object.keys(flatData).forEach(function(k) {
            if(!flatData[k].parent) {
              // The root node has to be active (inactive root nodes are not read by our logic)
              if(flatData[k].active) {
                if(!treeStart) {
                  // Start of the tree
                  treeStart = flatData[k];
                } else {
                  // Multiple elements without parent are not allowed (=multiple root nodes)
                  multipleRoots = true;
                }
              }
            } else {
              // Node in the tree - put before first element with higher orderAsc value
              var higherIndex = flatData[flatData[k].parent].nodes.findIndex(
                  function(el) {
                    return (el.orderAsc > flatData[k].orderAsc);
                  }
                );

              // TODO: Remove debug code
              console.log('Parent nodes:');
              console.log(flatData[flatData[k].parent].nodes);
              console.log('Element will be inserted at index: ' + higherIndex);
              console.log(flatData[k]);

              if(higherIndex >= 0) {
                // There is a element with higher orderAsc value, we paste our element before that
                flatData[flatData[k].parent].nodes.splice(higherIndex, 0, flatData[k]);
              } else {
                // There was no element with higher orderAsc value, so we push our element to the end
                flatData[flatData[k].parent].nodes.push(flatData[k]);
              }

            }
          });

          // Do we have multiple active root nodes?
          if(multipleRoots) {
            deferred.reject(gettext('An error occurred: The tree has multiple active root nodes (=nodes without valid parent)!'));
            return;
          }

          // Is tree initialized?
          if(!treeStart) {
            deferred.reject(gettext('An error occurred: The tree does not have a root object without parent!'));
            return;
          }

          console.log("Built tree. FlatData:");
          console.log(flatData);
          console.log("Hierarchical tree:");
          console.log(treeStart);

          deferred.resolve(treeStart);
        },
        function(err) {
          deferred.reject(err);
        }
      );

      return deferred.promise;
    };



    /**
     * @ngdoc method
     * @name dashboardTreeServices.treeService#getElementTitle
     * @methodOf dashboardTreeServices.treeService
     * @kind function
     *
     * @description
     * Given a specific `elementId`, this function returns the title string (i.e. the name)
     * of that tree element.
     *
     * This function first requests the full (flat) tree data and then pulls the title out of
     * this object. For getting only the title this is very inefficient compared to directly
     * querying the SharePoint list for just the single entry. However, we expect that in most
     * cases the tree hierarchy will be cached anyways (as users will start with the hierarchy
     * overview) and this way the function can thus return without querying the server.
     *
     *
     * @param {number} elementId The ID of the tree element whose the title should be retrieved.
     *
     * @returns {Promise} Returns a single promise that resolves to the title string
     *                    of the tree element specified by `elementId`.
     */

    treeServ.getElementTitle = function(elementId) {
      return treeServ.getFlatTree().then(
        function(flatTree) {
          if(!flatTree[elementId]) {
            // Element not found, throw error
            var deferred = $q.defer();
            deferred.reject(gettext('Tree element was not found in tree.'));
            return deferred.promise;
          }

          // Element found, return element title
          return flatTree[elementId].title;
        }
      );
    };


    /**
     * @ngdoc method
     * @name dashboardTreeServices.treeService#isValidElement
     * @methodOf dashboardTreeServices.treeService
     * @kind function
     *
     * @description
     * Validates the given `element`: It needs to contain a non-empty title, a numeric ordering (`orderAsc`),
     * a numeric parent ID (`parent`) and (unless it is a new element, which means `newElement` is `true`)
     * it has a valid, numeric `element.id`.
     *
     * Also, if it is the main tree node (with `parent` set to `-1`), it must not have a finite `valid_to` date or
     * a `valid_from` date in the future.
     *
     * @param {Object} element The tree `element` that should be validated.
     * @param {boolean} newElement Should be `true` if this is a new, still-to-be-added element, `false` otherwise.
     *                             New elements do not yet have an `element.id`.
     *
     * @returns {boolean} Returns `true` if `element` is a valid tree element, `false` otherwise.
     */

    treeServ.isValidElement = function(element, newElement) {
      return (
        element.title !== undefined &&
        element.title !== '' &&
        !isNaN(element.orderAsc) &&
        !isNaN(element.parent) &&
        (element.parent !== -1 || (element.valid_from.getTime() < (new Date()).getTime() && element.valid_to.getTime() > (new Date('2999-12-31')).getTime())) &&
        (!isNaN(element.id) || newElement)
      );
    };



    /**
     * @ngdoc method
     * @name dashboardTreeServices.treeService#asRestElement
     * @methodOf dashboardTreeServices.treeService
     * @kind function
     *
     * @description
     * Converts the tree element from the Dashboard-internal representation (as returned by the
     * various `get...` methods) into the SharePoint-specific representation expected by the
     * SharePoint REST API.
     *
     * @param {Object} element The tree element to be converted into the SharePoint representation.
     *
     * @returns {Object} Returns the SharePoint-specific representation of the given `element` as JavaScript object.
     */

    treeServ.asRestElement = function(element) {
      var restElement = {
        'Title': element.title,
        'Valid_from': element.valid_from,
        'Valid_to': (element.valid_to ? element.valid_to : new Date('2999-12-31T12:00:00.000Z')),
        'ParentId': element.parent,
	'hasDashboard': (!!element.hasDashboard),
        'OrderAsc': element.orderAsc
      };

      if(!isNaN(element.id)) {
        restElement.Id = element.id;
      }

      return restElement;
    };




    /**
     * @ngdoc method
     * @name dashboardTreeServices.treeService#addElement
     * @methodOf dashboardTreeServices.treeService
     * @kind function
     *
     * @description
     * This functions adds the provided `element` to the SharePoint list with all the tree
     * elements of the hierarchical tree.
     *
     * Note that this function will not process child nodes - you need to manually add new
     * child nodes after the parent node has been added using this method.
     *
     *
     * @param {Object} element The new node to be added to the tree as JavaScript object.
     *
     * @returns {Promise} Returns a single promise that resolves to the created element (with filled in element ID).
     */

    treeServ.addElement = function(element) {
      var deferred = $q.defer();

      if(!treeServ.isValidElement(element, true)) {
        deferred.reject(gettext('The node you are trying to add is not valid. Please complete its title.'));
        return deferred.promise;
      }

      // Send to server
      restListService.addItem('tree', treeServ.asRestElement(element))
        .then(
          // Get ID and then update element with new id
          function(res) {
            // TODO: remove debug code
            console.log('Added element to SharePoint list:');
            console.log(res);

            element.id = res.data.d.Id;
            deferred.resolve(element);
          },
          function(restErr) {
            deferred.reject(gettext('An error occurred when saving the new element to the server:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
          }
        );

      return deferred.promise;
    };




    /**
     * @ngdoc method
     * @name dashboardTreeServices.treeService#changeElement
     * @methodOf dashboardTreeServices.treeService
     * @kind function
     *
     * @description
     * This functions changes the provided `element` on the SharePoint list with all the tree
     * elements of the hierarchical tree. It will read the `element.id` and update the list entry
     * corresponding to that `element.id`.
     *
     * Note that this function will not process child nodes - you need to manually update any
     * child nodes that might have changed.
     *
     * @param {Object} element The node to be added on the tree. Needs to contain the `element.id` that should be
     *                         updated.
     *
     * @returns {Promise} Returns a single promise that resolves to the updated element.
     */

    treeServ.changeElement = function(element) {
      var deferred = $q.defer();

      if(!treeServ.isValidElement(element, false)) {
        deferred.reject(gettext('The node you are trying to update is not valid. Please ensure it contains an ID and complete its title.'));
        return deferred.promise;
      }

      // TODO: Remove debug code
      console.log('Sending changed element to server...');

      // Send to server
      restListService.updateItem('tree', element.id, treeServ.asRestElement(element))
        .then(
          function() {


            // TODO: Remove debug code
            console.log('Element changed. Returning modified item.');

            // Return modified element
            deferred.resolve(element);
          },
          function(restErr) {

            // TODO: Remove debug code
            console.log('REST error occured. Rejecting this promise.');

            deferred.reject(gettext('An error occurred when updating the element on the server:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
          }
        );

      return deferred.promise;
    };



    return treeServ;
  }]);
