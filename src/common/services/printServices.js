'use strict';

/**
 * @ngdoc overview
 * @name printServices
 * @description
 *
 * This module provides functionality to prepare data for printing by inserting this data into a special template and
 * opening the rendered template in a new, empty browser window.
 *
 */

angular.module('printServices', ['gettext'])


  /**
   * @ngdoc service
   * @name printServices.printService
   * @description
   *
   * This service provides functionality to prepare data for printing by inserting this data into a special template and
   * opening the rendered template in a new, empty browser window.
   *
   */

  .factory('printService', ['$timeout', '$compile', '$templateRequest', '$q', '$window', '$rootScope', 'gettextCatalog', function($timeout, $compile, $templateRequest, $q, $window, $rootScope, gettextCatalog) {
    var printServ = {};



    /**
     * @ngdoc method
     * @name printServices.printService#openPrintView
     * @methodOf printServices.printService
     * @kind function
     *
     * @description
     * Opens a template for printing content in a new window (with the given `title`) and fills in the `data` using
     * AngularJS's included compile functionality.
     *
     * @param {string} template    URL of the template to be inserted into the global printService template.
     * @param {string} title       Title of the to-be-opened window. Will be translated using gettext.
     * @param {Object} data        Data elements which will be inserted into the `data` element in the scope of the
     *                             print template.
     *
     * @returns {Promise}  A promise that resolves (without content) after the print window has opened.
     */

    printServ.openPrintView = function(template, title, data) {
      var scope = $rootScope.$new(true);
      scope.data = data;
      scope._printView = true;

      var deferred = $q.defer();

      $templateRequest(template)
        .then(function(templateContent) {
          var printLinkFn = $compile(templateContent);
          var linkedContent = printLinkFn(scope);

          // TODO: Remove debug code
          console.log('openPrintView: OBTAINED LINK FUNCTION.');
          console.log([template, title, data, scope, linkedContent, linkedContent.html()]);

          // We wrap everything in a timeout here to avoid multiple digests running at the same time
          $timeout(function() {
            // Digest cycle for our scope
            scope.$apply();

            // TODO: Remove debug code
            console.log('openPrintView: RAN DIGEST CYCLE.');
            console.log([template, title, data, scope, linkedContent, linkedContent.html()]);

            // Open new window
            var newWindow = $window.open('common/services/printService.aspx', '_blank', '');
            
            var loadData = function() {
              if(newWindow.writeContent) {
                // Window has finally loaded and we can write our content.
                
                // TODO: Remove debug code
                console.log('New window has loaded!');
                console.log(newWindow);
  
                newWindow.writeContent(gettextCatalog.getString(title), linkedContent.html());
                deferred.resolve();
  
                // Destroy the newly created scope
                scope.$destroy();                
              } else {
                // New window isn't ready yet, we try again in 500msec
                $timeout(loadData, 500);
              }
            };
            loadData();

          }, 0);


        });

      return deferred.promise;
    };


    return printServ;
  }]);



