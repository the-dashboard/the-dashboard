'use strict';
/* globals html2canvas */

/**
 * @ngdoc overview
 * @name feedbackServices
 * @description
 *
 * This module provides various services to gather feedback from users (with a system screenshot).
 *
 */

angular.module('feedbackServices', ['gettext', 'restServices'])


  /**
   * @ngdoc service
   * @name feedbackServices.feedbackService
   * @description
   *
   * A service to gather feedback (including a current screenshot) from the user about
   * the system and how it can be improved.
   *
   * Feedbacks are written to the "Dashboard - Feedback" list. Unfortunately, SharePoint does not
   * display the formatting when accessing this list directly. You can use the following bookmarklet,
   * however, to turn the feedback into properly formatted HTML when you view the list entry in
   * the SharePoint list:
   *
   * ```
   * javascript:void (document.querySelector('#SPFieldNote div').innerHTML = document.querySelector('#SPFieldNote div').innerHTML.replace(/&lt;/gm, '<').replace(/&gt;/gm, '>'));
   * ```
   *
   */

  .factory('feedbackService', ['$q', '$document', 'restListService', function($q, $document, restListService) {
    var feedbackServ = {};


    /**
     * @ngdoc method
     * @name feedbackServices.feedbackService#getScreenshot
     * @methodOf feedbackServices.feedbackService
     * @kind function
     *
     * @description
     * This function takes a "screenshot" of the current page (re-building the DOM
     * in a canvas using [html2canvas](https://github.com/niklasvh/html2canvas) and
     * exporting this canvas as a PNG image data URL).
     *
     * @returns {Promise} A promise that gets resolved to a data URI string for the screenshot.
     */

    feedbackServ.getScreenshot = function() {
      return html2canvas($document[0].body).then(function(canvas) {
        return canvas.toDataURL();
      });
    };


    /**
     * @ngdoc method
     * @name feedbackServices.feedbackService#sendFeedback
     * @methodOf feedbackServices.feedbackService
     * @kind function
     *
     * @description
     * Writes the given feedback to the database.
     *
     * @param {string} feedback The feedback to be written to the database.
     *
     * @param {string=} screenshot A data URL containing the screenshot for the feedback (optional). Will be written as HTML attachment.
     *
     * @returns {Promise} A promise that resolves to `true` if the feedback was written successfully
     *                    or gets rejected otherwise.
     */

    feedbackServ.sendFeedback = function(feedback, screenshot) {
      if(!angular.isString(feedback)) { return $q.reject('Please provide a valid feedback string.'); }

      var restFeedback = {
        Title: 'Feedback',
        Feedback: feedback
      };

      // Send to server
      return restListService.addItem('feedback', restFeedback).then(
        function(res) {
          if(!res || !res.data || !res.data.d) {
            console.log('ERROR: The result returned by SharePoint was not in the expected format.');
            console.log(res);
          
            return $q.reject('The result returned by SharePoint was not in the expected format.');
          }
          
          var screenshotContent = '<html><body><img src="' + screenshot + '" /></body></html>';
        
          if(screenshot) {
            return restListService.addAttachmentFromString('feedback', res.data.d.Id, 'screenshot.html', screenshotContent);
          } else {
            return res;
          }
        }
      );
    };


    return feedbackServ;
  }]);



