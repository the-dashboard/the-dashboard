<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="UTF-8">
        <title>Print View</title>

        <style>
            body {
              font-family: -apple-system, ".SFNSText-Regular", "San Francisco", "Roboto", "Segoe UI", "Helvetica Neue", "Lucida Grande", sans-serif;
              font-size: 9pt;
              padding-top: 20px;
            }

            td {
              padding-left: 5px;
              padding-right: 5px;
            }

            h1 {
              font-size: 12pt;
            }

            h2 {
              font-size: 10pt;
            }

        </style>
    </head>

    <body>
    
    <script type="text/javascript">
      function writeContent(title, html) {
        document.title = title;
        document.body.innerHTML = html;
      }
    </script>
    </body>
</html>

