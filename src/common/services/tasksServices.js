'use strict';

/**
 * @ngdoc overview
 * @name tasksServices
 * @description
 *
 * This module provides services to add, edit and remove tasks for the Ongoing Control Assessment and to retrieve
 * and manipulate task details (including the detailed data for each task record).
 *
 */

angular.module('tasksServices', ['gettext', 'restServices', 'auditFormsServices', 'miscServices', 'usersServices', 'socialServices', 'configServices', 'datafilesServices'])


/**
 * @ngdoc filter
 * @name tasksServices#taskStatus
 * @description
 *
 * The task status filter transforms the task status into a readable description based on the language of the user.
 *
 */

  .filter('taskStatus', ['gettextCatalog', function(gettextCatalog) {
    return function(input) {
      return (input ? gettextCatalog.getString(input) : '');
    };
  }])


  /**
   * @ngdoc service
   * @name tasksServices.tasksService
   * @description
   *
   * This service supports retrieving and updating tasks in the OCA module. Tasks are data sets which need to be processed
   * by the auditors when performing OCA duties.
   *
   * All active OCA tasks have to be stored as CSV files in the "Dashboard - Task Data" SharePoint library. Once they
   * should no longer be displayed in the active task list, they can be archived: Archiving means moving them from the
   * "Task Data" to the "Dashboard - Task Archive" SharePoint library, which has the same fields and logic as the
   * "Task Data" library but tasks in the archive won't be displayed in the active task overviews and cannot be
   * modified. You might want to restrict write access to the "Task Archive" library to limit the number of people who
   * can modify archived tasks.
   *
   * The metadata fields in the library specify how the tasks should be presented and can be interacted with. The
   * following metadata fields are available in the library:
   *
   * - **Name:** The filename of the CSV file, needs to be unique.
   *
   * - **Title:** The title with which the OCA task will be displayed to the user.
   *
   * - **TaskDescription**: A longer description that explains the purpose and the work to be done on this OCA task.
   *   Will be presented to the user.
   *
   * - **DataDate**: The relevant date for which the data in the OCA task has been retrieved. Identifies how old the
   *   task data is and will be presented to the user.
   *
   * - **workflowStatus**: The current workflow status of the task, can be any of "New", "Open", "Done", "Reviewed",
   *   "Archived". "Archived" should only be used for tasks in the archive library (see above). Field is only relevant
   *   if the applicable `workflowRule` is `soft` or `hard` and will be ignored if `workflowRule` is `off` (see
   *   {@link tasksServices.tasksService#getWorkflowRule `getWorkflowRule`}).
   *
   * - **workflowChanges**: Whenever the `workflowStatus` of a task is updated, the system adds the change made to this
   *   JSON-serialized array of past workflow changes made. Each element of the array corresponds to one change in the
   *   workflow status of the task and is an object with the following properties: `status` specifies the new status
   *   after the change, `change_at` is the time and date the change was made (as ISO-formatted string) and `change_by`
   *   is the SharePoint user ID of the user who made the change.
   *
   * - **dataSpecification**: A JSON-serialized object which specifies the format of the CSV file and various rules
   *   and configuration options about how the CSV data should be displayed to and can be^manipulated by the user. See
   *   detailed specification below.
   *
   * - **assignedTo**: The SharePoint user to which this task has been assigned. Allows for filtering of tasks assigned
   *   to the individual user.
   *
   *
   * ### Specification of `dataSpecification`
   * The `dataSpecification` is a JSON-serialized object with the following properties:
   *
   * - **`showColumns`**: An array of column names (the `display` property of the column definition, see below). The
   *   columns given here will be displayed to the user in the task view. They will also be exported when the user opts
   *   for the Excel export of the task data.
   *   Columns specified in the CSV file but not given here can still be used for e.g. machine learning and in audit
   *   forms but will not be displayed in the table view of the task.
   *
   * - **`dataFileFormat`**: The file format of the CSV file, which specifies how the system will read the file. Uses
   *   the same specification as for ORA datafiles, see {@link datafilesServices.datafilesService#getAllDatafiles}.
   *
   * - **`keywords`**: Each entry in the task CSV file can be assigned one or multiple keywords which will be used to
   *   retrieve audit forms with matching keywords. This property specifies how keywords are constructed for the task
   *   entries: It is an array of strings with each string entry denoting a keyword to be assigned to each entry in the
   *   CSV file. The strings can contain placeholders for the columns of the data like so: `{{COLUMNNAME}}`. This
   *   double-bracket syntax will be replaced with the content of the column named `COLUMNNAME` of the given entry
   *   (based on the `display` name of the column as specified in the `dataFileFormat`).
   *
   * - **`showAuditForms`**: A boolean value (`false` or `true`) that specifies whether audit forms should be retrieved
   *   and displayed on the task entries (based on matching keywords as specified above).
   *
   * - **`newAuditForms`**: An array that specifies which types of audit forms can be created on elements of this task.
   *   If this array is missing or empty, no new audit forms can be created (but if `showAuditForms` is `true`, existing
   *   audit forms will still be displayed and can also be edited). The array should contain a string with the
   *   `shortKey` property of each audit form type that should be enabled (see
   *   {@link auditFormsServices.auditFormsService#getAuditForm `getAuditForm`}).
   *
   *   **`excelEnabled**: A boolean value (`false` or `true`) that specifies whether data from this task can be exported
   *   into an Excel spreadsheet and results imported out of the completed Excel spreadsheet.
   *
   * - **`resultsColumn`**: A string. If given, a column will be added to the task view that will capture results of the
   *   audit procedures performed by the auditors. The string given will be used as column header for this column.
   *   Results can be entered into this column either using the `resultsSelectionIterate` process or by adding audit
   *   forms that map to results for this column (see `mapResults` below).
   *   If you want to use any of `resultsSelectionIterate`, `mapResults`, and/or `machineEnabledResult`, you need to
   *   specify a column name here.
   *
   * - **`resultsSelectionIterate`**: A boolean value that specifies whether the user should be able to iterate through
   *   the possible result values specified in `resultsSelection`. If `true`, each click on a cell in the
   *   `resultsColumn` will iterate through the available elements of `resultsSelection`, allowing the user to select a
   *   specific result value through repeated clicks on the results column. If `false` (or unset), the results can only
   *   be changed through an audit form and the `mapResults` logic.
   *
   * - **`resultsSelection`**: An array of strings with allowed values for the `resultsColumn`. Required if
   *   `resultsSelectionIterate` is set to `true`, in which case the user can iterate the results for a specific task
   *   entry by clicking on the results column, which will iterate through the entries of `resultsSelection`.
   *
   * - **`machineEnabledResult`**: If this property and `machineEnabledColumns` is set, this will enable the machine
   *   learning functionality of the system. The learning system will try to identify which task entries are similar and
   *   provide a percentage to the user on how likely it believes a specific task entry to be a "hit". This property
   *   must contain a string with one of the possible results values (from `resultsSelection`): The machine learning
   *   classifier will treat entries with a result equal to the value of `machineEnabledResult` as "hit", while it will
   *   classify all other non-empty results as "miss" for its training.
   *   The machine learning percentage will thus indicate to the user how likely the system judges a given entry to have
   *   a result that is equal to the value of `machineEnabledResult`.
   *
   * - **`mapResults`**: This property is used to map audit forms that are attached to a given task entry to a single
   *   result that will be stored and displayed in the `resultsColumn`. `mapResults` should be an array that will be
   *   processed in order for each task entry and all its directly attached audit forms (meaning that results will only
   *   be mapped from audit forms that have been created directly on the task entry and are not drawn in just using the
   *   keywords; those are the audit forms that are displayed with a red border to the user).
   *
   *   The `mapResults` array is an array of objects with each object having the following properties: `auditForm`
   *   specifies the `shortKey` of the audit form types which should be matched by this entry. `mapFrom` specifies the
   *   result value of the audit form that should match this entry. Can be `"*"`, in which case all audit form result
   *   values will match. `mapTo` is optional and specifies which value should be written to the task entry results
   *   value if there is a match.
   *
   *   For each entry in the `mapResults` array, the system will try to find a matching audit form. If it finds one,
   *   it will set the value of `resultsColumn` to the `mapTo` value of the first matching `mapResults` entry and stop
   *   processing further. (If `mapTo` is not specified, the `resultsColumn` will be set to the result value of the
   *   matching audit form without any further mapping.) If there is no match when the end of `mapResults` is reached,
   *   the results value of the task entry will not be modified. Also, the results value will only be updated when an
   *   audit form is changed in the system. This allows that results can still be overwritten using the
   *   `resultsSelectionIterate` mechanism.
   *
   * - **`machineEnabledColumns`**: If machine learning should be enabled, this property must be set to an array of
   *   columns which will be used by the machine learning classifier to classify the task entries. The array is an array
   *   of objects with each object having two properties: `column` is the name of the column that should be used by the
   *   classifier (as given in the `display` property). `type` specifies the type of the column, which will influence
   *   the pre-processing done by the classifier and can thus be different from the data type specified in the data
   *   specification. Supported are `number` for numeric values (which will be standardized and then fed directly as
   *   features to the classifier), `date` for date values (which will be translated into different features for
   *   day-of-week, month etc. to allow the learning of seasonality) and `text` for text fields (which will be
   *   transformed into features using a "bag of words" approach).
   *
   */

  .service('tasksService', ['$q', '$http', 'gettext', 'auditFormsService', 'restListService', 'restDocumentService', 'usersService', 'datafileReader', 'datafileWriter', 'interactionService', 'subscriptionService', 'miscServices', 'configService', function($q, $http, gettext, auditFormsService, restListService, restDocumentService, usersService, datafileReader, datafileWriter, interactionService, subscriptionService, miscServices, configService) {
    var tasksServ = {};

    // Cache for already retrieved tasks
    var tasksPromise  = [undefined, undefined]; 
    var tasksPromiseWithResults = [undefined, undefined]; 

    var singleTaskPromise = [{}, {}];  // First element is active tasks, second is archived tasks

    var taskDataPromise = {};
    var taskResultsPromise = {};
    
    
    // Helper function to retrieve and format details for a given task object as returned by SharePoint
    // Returns the reformatted task object.
    var taskDetails = function(task) {
      var formattedTask = {
        id: task.Id,
        title: task.Title,
        description: task.TaskDescription,
        dataDate: miscServices.parseDate(task.DataDate),
        status: task.workflowStatus,
        assigned_to: task.AssignedToId,
        created_at: new Date(task.Created),
        last_modified_at: new Date(task.Modified),
        last_modified_by: task.EditorId,
        workflowChanges: [],
        originalId: task.originalId,
        interactionSummary: {},

        deferred_file_uri: task.File.__deferred.uri
      };

      // Do we have formatting details?
     try {
        // We "try" this, as JSON might not be formatted properly (will throw SyntaxError)
        formattedTask.dataSpecification = JSON.parse(task.dataSpecification);
      } catch(err) {
        // Return incomplete formattedTask
        console.log('ERROR - Invalid task data specification JSON:');
        console.log(task.dataSpecification);
        return formattedTask;
      }

      // Do we have an interactionSummary?
      try {
       formattedTask.interactionSummary = JSON.parse(task.interactionSummary);
      } catch(err) {
       console.log('interactionSummary for task could not be parsed. Using empty interactionSummary...');
       console.log(task.interactionSummary);
      }

      // Split workflow array to identify completer and reviewer
      if(task.workflowChanges) {
        try {
          // We "try" this, as JSON might not be formatted properly (will throw SyntaxError)
          formattedTask.workflowChanges = JSON.parse(task.workflowChanges);
        } catch(err) {
          console.log('ERROR - Invalid workflow change history JSON:');
          console.log(task.workflowChanges);
          return formattedTask;
        }

        if(Array.isArray(formattedTask.workflowChanges)) {
          formattedTask.workflowChanges.forEach(function(workflow) {
            if(workflow.status === 'Done' && (!formattedTask.done_at || formattedTask.done_at < new Date(workflow.change_at))) {
              // We are looking for the most recent change to "Done". This identifies the current "done_at" and "done_by".
              formattedTask.done_at = new Date(workflow.change_at);
              formattedTask.done_by = workflow.change_by;
            }

            if(workflow.status === 'Reviewed' && (!formattedTask.reviewed_at || formattedTask.reviewed_at < new Date(workflow.change_at))) {
              // We are looking for the most recent change to "Reviewed". This identifies the current "reviewed_at" and "reviewed_by".
              formattedTask.reviewed_at = new Date(workflow.change_at);
              formattedTask.reviewed_by = workflow.change_by;
            }
          });

          if(formattedTask.done_at && formattedTask.reviewed_at && formattedTask.done_at > formattedTask.reviewed_at) {
            // Task was re-"done" after it had been reviewed. We clear this (outdated) review.
            formattedTask.reviewed_at = undefined;
            formattedTask.reviewed_by = undefined;
          }
        }
      }

      return formattedTask;
    };


    // Helper function to turn a formatted task into a REST object suitable for updating the SharePoint database.
    var taskToREST = function(task) {
      var restTask = {
        'Title': task.title,
        'TaskDescription': task.description,
        'DataDate': task.dataDate,
        'workflowStatus': task.status,
        'workflowChanges': JSON.stringify(task.workflowChanges ? task.workflowChanges : [ ]),
        'dataSpecification': JSON.stringify(task.dataSpecification ? task.dataSpecification : { }),
        'assignedToId': task.assigned_to
      };

      return restTask;
    };


    /**
     * @ngdoc method
     * @name tasksServices.tasksService#getTasks
     * @methodOf tasksServices.tasksService
     * @kind function
     *
     * @description
     * This method returns all tasks (i.e. data sheets to be evaluated by the auditors as part of their OCA duties)
     * in the SharePoint database. By default, the tasks from the main library are returned. If you set `fromArchive`
     * to `true`, however, archived tasks will be obtained. Archived tasks have been moved to a separate archival
     * library in order not to take up space in the productive area.
     *
     * @param {boolean=} fromArchive      If set to `true`, all tasks from the archive document library will be 
     *                                    retrieved. Defaults to `false`, which will retrieve all not archived tasks.
     *
     * @param {boolean=} resolveResults   If given and set to `true`, the results tables for all tasks will be
     *                                    retrieved and the modified dates of the tasks will be updated to reflect
     *                                    any changes in the results data.
     *
     * @returns {Promise} A promise which resolves to an array of tasks items.
     *
     */

    tasksServ.getTasks = function (fromArchive, resolveResults) {
      var deferred;

      // The endpoint depends on whether we want the archived tasks (which reside in a different
      // Document Library)
      var endpoint = 'task_data' + (fromArchive ? '_archive' : '');
      var results_endpoint = 'task_results' + (fromArchive ? '_archive' : '');

      // Do we already have a tasksPromise?
      var relevantPromise = (resolveResults ? tasksPromiseWithResults : tasksPromise)[fromArchive ? 1 : 0];
      if (relevantPromise) {
        return relevantPromise;
      } else {
        // We need to create a new promise.
        deferred = $q.defer();

        if (resolveResults) {
          tasksPromiseWithResults[fromArchive ? 1 : 0] = deferred.promise;
        } else {
          tasksPromise[fromArchive ? 1 : 0] = deferred.promise;
        }
      }

      // Obtain all files from the relevant document library - they are the tasks in the OCA database.
      restDocumentService.getAllFiles(endpoint)
        .then(
          function (res) {
            // Did we get a valid response from the SharePoint server?
            if (!res || !res.data || !res.data.d || !res.data.d.results || !Array.isArray(res.data.d.results)) {
              return $q.reject(gettext('Unexpected response from server.'));
            }

            // TODO: Remove debug code
            console.log('Received Library data: ');
            console.log(res.data.d);

            var taskList = [];
            var taskHashList = {};

            res.data.d.results.forEach(function (d) {
              var thisTask = taskDetails(d);
              taskList.push(thisTask);
              taskHashList[thisTask.id.toString()] = thisTask;
            });

            if (!resolveResults) {
              return taskList;
            } else {
              return restDocumentService.getAllFiles(results_endpoint)
                .then(
                  function (res) {
                    if (!res || !res.data || !res.data.d || !res.data.d.results || !Array.isArray(res.data.d.results)) {
                      return $q.reject(gettext('Unexpected response from server.'));
                    }

                    // TODO: Remove debug code
                    console.log('Received results library data: ');
                    console.log(res.data.d);

                    res.data.d.results.forEach(function (d) {
                      if (taskHashList[d.taskId.toString()]) {
                        taskHashList[d.taskId.toString()].last_modified_at = new Date(d.Modified);
                        taskHashList[d.taskId.toString()].last_modified_by = d.EditorId;
                      }
                    });

                    return taskList;
                  }
                );
            }
          }
        )
        .then(
          function (taskList) {
            deferred.resolve(taskList);
          },
          function (err) {
            console.log('Error while obtaining task items:');
            console.log(err);

            deferred.reject(gettext('Error while obtaining task items.'));
          }
        );

      // We return the newly created promise.
      return deferred.promise;
    };




    /**
     * @ngdoc method
     * @name tasksServices.tasksService#getTask
     * @methodOf tasksServices.tasksService
     * @kind function
     *
     * @description
     * This method returns detailed information for the task with the given `taskId` in the SharePoint database. By
     * default, the task is searched in the main library are returned. If you set `fromArchive` to `true`, however,
     * archived tasks will be obtained. Archived tasks have been moved to a separate archival library in order not to
     * take up space in the productive area.
     *
     * @param {integer} taskId The ID of the task to be loaded in detail.
     *
     * @param {boolean=} fromArchive If set to `true`, all tasks from the archive document library will be retrieved.
     *                               Defaults to `false`, which will retrieve all not archived tasks.
     *
     * @returns {Promise} A promise which resolves to the object containing the details of the given task.
     *
     */

    tasksServ.getTask = function (taskId, fromArchive) {
      var fromArchiveBinary = (fromArchive ? 1 : 0);

      // The endpoint depends on whether we want the archived tasks (which reside in a different
      // Document Library)
      var endpoint = 'task_data' + (fromArchive ? '_archive' : '');

      // Is the ID a valid integer?
      if(!miscServices.isInt(taskId)) {
        return $q.reject(gettext('Specified task ID is invalid.'));
      }
      var taskIdVal = parseInt(taskId);

      // Do we already have a singleTaskPromise for this task?
      if(singleTaskPromise[fromArchiveBinary][taskId]) {
        return singleTaskPromise[fromArchiveBinary][taskId];
      }

      // If we already have all tasks loaded, we can use that promise to retrieve the single task.
      if(tasksPromise[(fromArchiveBinary ? 1 : 0)]) {
        singleTaskPromise[fromArchiveBinary][taskId] = tasksPromise[(fromArchiveBinary ? 1 : 0)].then(function(taskList) {
          // Find the relevant task
          return taskList.find(function(d) { return (d.id === taskIdVal); });
        });

        return singleTaskPromise[fromArchiveBinary][taskId];
      }

      // Ok, we need to retrieve the single task instead.
      singleTaskPromise[fromArchiveBinary][taskId] = restDocumentService.getFileById(endpoint, taskId).then(function(res) {
        if(!res || !res.data || !res.data.d) {
          return $q.reject(gettext('The retrieved task item was not in the expected format.'));
        }

        // TODO - remove debug
        console.log('Retrieved task item raw data:');
        console.log(res.data.d);

        // Return the formatted task item.
        return taskDetails(res.data.d);
      });


      // Done. Return the promise.
      return singleTaskPromise[fromArchiveBinary][taskId];
    };



    /**
     * @ngdoc method
     * @name tasksServices.tasksService#updateTask
     * @methodOf tasksServices.tasksService
     * @kind function
     *
     * @description
     * This method will update the provided `task` in the database, identifying it by its `task.id`. Note that it is not
     * possible to update archived tasks.
     *
     * @param {Object} task     The task which should be updated, in the format returned by
     *                          {@link tasksServices.tasksService#getTask `getTask`}.
     *
     * @returns {Promise} A promise which resolves to the updated task object if the update was successful.
     *
     */

    tasksServ.updateTask = function(task) {
      var restTask = taskToREST(task);

      return restListService.updateItem('task_data', task.id, restTask)
        .then(
          function() {
            // Clear relevant promises, so that we retrieve new tasks next time
            tasksPromise[0] = undefined;
	    tasksPromiseWithResults[0] = undefined;
            singleTaskPromise[0][task.id] = undefined;

            // Updating modified date and modifiying user
            task.last_modified_at = new Date();

            return usersService.getCurrentUser().then(function(user) {
              task.last_modified_by = user.id;

              // Return the task
              return task;
            });

          },
          function(err) {
            return $q.reject(gettext('An error occured updating the task: ') + err);
          }
        );
    };


    /**
     * @ngdoc method
     * @name tasksServices.tasksService#updateTaskStatus
     * @methodOf tasksServices.tasksService
     * @kind function
     *
     * @description
     * This method will update the workflow status of the given `task` to `newStatus`. It will update the task
     * object (updating both the current status and the workflow history) but also directly update the task in the
     * database with the new status.
     *
     * The method will also trigger any subscriptions on this task, notifying the subscribing users of the change in
     * task status. The user that triggers the status change will also be added to the "watchers" of the given task,
     * so that he/she will be notified automatically of future status changes.
     *
     * The method will take into account the workflow settings for the given task: if the workflow setting is "hard",
     * the promise will resolve to an error if any of the following is attempted:
     *
     *   a) a task is reviewed without being set to "done" first
     *   b) a task is reviewed by the same person who set it to "done"
     *
     * It returns a promise that will be resolved with the updated task once the database update completes.
     *
     * Note that you cannot update an archived task, so make sure you do not run this on an archived task.
     *
     *
     * @param {Object} task       The task which should be updated, in the format returned by
     *                            {@link tasksServices.tasksService#getTask `getTask`}.
     *
     * @param {String} newStatus  A string which identifies the new status the task should be set to. Has to be one
     *                            of "New", "Open", "Done", "Reviewed". (You cannot set "Archived" this way. For
     *                            archiving a task, use the {@link tasksServ.archiveTask `archiveTask`} method.)
     *
     * @returns {Promise} A promise which resolves to the updated task object if the update was successful.
     *
     */

    tasksServ.updateTaskStatus = function (task, newStatus) {
      // We need to get the user ID of the current user
      var currUser;
      var updatedTask;
      return usersService
        .getCurrentUser()
        .then(
          function (_currUser) {
            currUser = _currUser;
            return tasksServ.getWorkflowRule(task.id);
          }
        )
        .then(
          function (workflowRule) {
            if (workflowRule === 'hard' && newStatus === 'Reviewed') {
              // We need to validate whether the status update does not violate the "hard" workflow rules in place
              // Forbidden are:

              // a) a task is reviewed without being set to "done" first
              if (task.status !== 'Done' && task.status !== 'Reviewed') {
                return $q.reject(gettext('A task can only be reviewed when it is marked as "done".'));
              }

              // b) a task is reviewed by the same person who set it to "done"
              if (task.done_by === undefined || currUser.id.toString() === task.done_by.toString()) {
                return $q.reject(gettext('A task cannot be reviewed by the same person who marked it as "done".'));
              }
            }

            // Push the new status to the workflow statuses
            var currChangeAt = new Date();
            task.workflowChanges.push({
              status: newStatus,
              change_at: currChangeAt,
              change_by: currUser.id
            });

            // Update the current status
            task.status = newStatus;

            // Update the internal helper variables done_at, done_by, reviewed_at, reviewed_by
            if (newStatus === 'Done') {
              task.done_at = currChangeAt;
              task.done_by = currUser.id;
            } else if (newStatus !== 'Reviewed') {
              // We need to clear done_at / done_by, as the task has been reset to an earlier status
              task.done_at = undefined;
              task.done_by = undefined;
            }

            if (newStatus === 'Reviewed') {
              task.reviewed_at = currChangeAt;
              task.reviewed_by = currUser.id;
            } else {
              // We need to clear reviewed_at / reviewed_by as the task has been reset to an earlier status
              task.reviewed_at = undefined;
              task.reviewed_by = undefined;
            }

            // Send the task to the database for updating
            return tasksServ.updateTask(task);
          })
        .then(
          function (_updatedTask) {
            updatedTask = _updatedTask;

            // We need to trigger any subscriptions on this task
            return subscriptionService.triggerSubs(
              'task',
              updatedTask.id,
              'task',
              updatedTask.id,
              undefined,
              '#/tasks/' + updatedTask.id,
              {
                from: currUser.id,
                new_status: newStatus,
                task: task
              },
              'changed-task-status'
            );
          }
        )
        .then(
          function() {
            // If this is the first action of a user on this task, the user that has triggered the update needs to
            // start watching this task
            if(task.workflowChanges.findIndex(function(d) {
              return (d.change_by.toString() === currUser.id.toString());
            }) === (task.workflowChanges.length-1)) {
              // Only the last (newly created) workflowStatus change is by this user... so we need to add the watch.
              return interactionService.addWatch(currUser.id, 'task', updatedTask.id, updatedTask.title, '#/tasks/' + updatedTask.id);
            }
          }
        )
        .then(
          function() {
            return updatedTask;
          }
        );
    };



    /**
     * @ngdoc method
     * @name tasksServices.tasksService#archiveTask
     * @methodOf tasksServices.tasksService
     * @kind function
     *
     * @description
     * This method archives the given task.
     *
     * It returns a promise that will be resolved with the updated task once the database updates complete.
     *
     * The method performs the following actions subsequently:
     *
     * 1. Adds the task item itself to the "Task Data - Archive" library.
     *
     * 2. Removes the original task item from the "Task Data" library.
     *
     * 3. Updates the Task Results file to the new task ID in the archive [via
     *    {@link tasksServices.tasksService#archiveTaskResults `archiveTaskResults`}].
     *
     * 4. Updates all Audit Forms' source entries to the new task ID in the archive [via
     *    {@link auditFormsServices.auditFormService#archiveFormData `archiveFormData`}].
     *
     * If the last two operations fail but the task has already been moved to the Archive, you can re-run the
     * internal functions {@link tasksServices.tasksService#archiveTaskResults `archiveTaskResults`} and/or
     * {@link auditFormsServices.auditFormService#archiveFormData `archiveFormData`} to re-try moving the
     * remaining items.
     *
     *
     * @param {Object} task       The task which should be archived, in the format returned by
     *                            {@link tasksServices.tasksService#getTask `getTask`}.
     *
     * @returns {Promise} A promise which resolves to the updated task object if the archiving was successful.
     *
     */

    tasksServ.archiveTask = function(task) {

      var currUser;
      var workflowRule;
      var newArchivedId;
      var taskFileName;
      return usersService.getCurrentUser()
        .then(
          function(_currUser) {
            currUser = _currUser;
            return tasksServ.getWorkflowRule(task.id);
          }
        )

        .then(
          function(_workflowRule) {
            workflowRule = _workflowRule;

            // If hard workflow rules are enabled, only "Reviewed" tasks can be archived.
            if(workflowRule === 'hard' && task.status !== 'Reviewed') {
              return $q.reject(gettext('Only reviewed tasks can be archived.'));
            }

            // Update the workflow status in the task object
            task.status = 'Archived';

            task.workflowChanges.push({
              status: 'Archived',
              change_at: new Date(),
              change_by: currUser.id
            });

            // Now we need to retrieve the original task data file to be able to write that to the Archive
            // library
            return restDocumentService.expandFilePath(task.deferred_file_uri);
          }
        )

        .then(
          function(path) {
            taskFileName = path[1];

            return $http.get(path[0], { responseType: 'arraybuffer' });
          }
        )

        // No we need to write the task data file to the Archive library as new item
        .then(
          function(res) {
            if (!res.data) {
              console.log('ERROR: No data retrieved while retrieving the original task data:');
              console.log(res);

              return $q.reject(gettext('An error occured while trying to retrieve the original task data.'));
            }

            // New Task object
            var newTask = taskToREST(task);
            newTask.originalId = task.id;

            // Ok, we have the ArrayBuffer with the task data, now we can write it to a new library item...
            return restDocumentService.addArrayBufferAsFile('task_data_archive', (new Date()).toISOString().replace(/[^0-9TZ\-]/g, '-') + '_' + taskFileName, newTask, res.data, false);
          }
        )
        .then(
          function(addResult) {
            newArchivedId = addResult.data.d.Id;

            // Now we need to recycle the original task item...
            return restDocumentService.deleteFile('task_data', taskFileName);
          }
        )
        .then(
          function() {
            // Now we need start part 3 by calling archiveTaskResults...
            return tasksServ.archiveTaskResults(task, newArchivedId);
          }
        )
        .then(
          function() {
            // Now we need to start part 4 by calling archiveFormData...
            return auditFormsService.archiveFormData(task.id, newArchivedId);
          }
        );
    };




    /**
     * @ngdoc method
     * @name tasksServices.tasksService#archiveTaskResults
     * @methodOf tasksServices.tasksService
     * @kind function
     *
     * @description
     * When a task is being archived (using {@link tasksServices.tasksService#archiveTask `archiveTask`}) the system
     * also needs to update the result files so that they reference the new item in the Archive library
     * and no longer the old item.
     *
     * This method copies the results file for an archived task to the archive library.
     *
     * It returns a promise that will be resolved with the original task item once the update complete.
     *
     * This function can be called multiple times without any harm, if there are no more results to be updated to the
     * new archived task ID it will simply return without doing anything.
     *
     *
     * @param {Object} oldTask         The original task which has been archived, in the format returned by
     *                                 {@link tasksServices.tasksService#getTask `getTask`}.
     *
     * @param {integer} newArchivedId  The (new) SharePoint-internal ID the task has in the Archive library.
     *
     * @returns {Promise} A promise which resolves to `oldTask` if the archiving was successful.
     *
     */

    tasksServ.archiveTaskResults = function(oldTask, newArchivedId) {
       return restDocumentService

         // First we need to get the file details of any result set (if it exists)
         .getFileByFilename('task_results', 'task_' + oldTask.id + '.csv')

         .then(
           function(res) {
             // Do we have data in the right format?
             if(!res || !res.data || !res.data.d || !res.data.d.File || !res.data.d.File.__deferred || !res.data.d.File.__deferred.uri) {
               console.log('ERROR: While trying to load the task results the data returned was not in the expected format:');
               console.log(res);

               return $q.reject(gettext('While trying to load the task results the data returned was not in the expected format!'));
             }

             // Get file path
             return restDocumentService.expandFilePath(res.data.d.File.__deferred.uri)
               .then(
                 function(path) {
                   return $http.get(path[0], { responseType: 'arraybuffer' });
                 }
               )
               .then(
                 function(res) {
                   if (!res.data) {
                     console.log('ERROR: No data retrieved while retrieving the original task results:');
                     console.log(res);

                     return $q.reject(gettext('An error occured while trying to retrieve the original task results.'));
                   }

                   // Ok, we have the ArrayBuffer with the task results, now we can write it to a new library item...
                   return restDocumentService.addArrayBufferAsFile('task_results_archive', 'task_' + newArchivedId + '.csv', { 'taskId': newArchivedId }, res.data, false);
                 }
               )
               .then(
                 function() {
                   // Ok, the task results have been copied to the archive library, now we need to remove the original file.
                   return restDocumentService.deleteFile('task_results', 'task_' + oldTask.id + '.csv');
                 }
               );
           },
           function(err) {
             // We need to treat this error specially, because if a 404 has been returned this simply means we do not (yet) have
             // task results for this task. This should not be treated as an error but simply fail by returning the original task object.
             if(err.status === 404) {
               return oldTask;
             } else {
               console.log('ERROR: An unexpected error occured while trying to load task results:');
               console.log(err);

               return $q.reject(gettext('An error occured while trying to load task results.'));
             }
           }
         );
    };



    /**
     * @ngdoc method
     * @name tasksServices.tasksService#getTaskData
     * @methodOf tasksServices.tasksService
     * @kind function
     *
     * @description
     * This method returns the task data for the given task object by opening the file and parsing it according to the
     * formatting specification.
     *
     * @param {Object} task The object of the task for which data should be retrieved (as provided by the
     *                      `getTask` and `getAllTasks` methods).
     *
     * @returns {Promise} A promise which resolves to the object containing the task data.
     *
     */
    tasksServ.getTaskData = function(task) {
      if(!task || !task.deferred_file_uri) {
        return $q.reject(gettext('The task does not contain a deferred file retrieval URI.'));
      }

      // Do we already have a promise we can return?
      if(taskDataPromise[task.deferred_file_uri]) {
        return taskDataPromise[task.deferred_file_uri];
      }

      // We need to return a new promise. We get the deferred file URI and then open that file using the
      // datafileReader.
      taskDataPromise[task.deferred_file_uri] = restDocumentService.expandFilePath(task.deferred_file_uri)
        .then(function(path) {
         return datafileReader.readFormattedFile(path[0], task.dataSpecification.dataFileFormat);
        })
        .then(function(readerResult) {
          return readerResult[0];   // The first array entry contains the actual rows of data.
        });

      return taskDataPromise[task.deferred_file_uri];
    };


    /**
     * @ngdoc method
     * @name tasksServices.tasksService#getWorkflowRule
     * @methodOf tasksServices.tasksService
     * @kind function
     *
     * @description
     * This method returns workflow rule which (according to the global Dashboard configuration) is applicable to the
     * task with the given `taskId`.
     *
     * @param {Object} taskId   The id of the task for which the applicable workflow rule should be retrieved.
     *
     * @returns {Promise} A promise which resolves to the applicable workflow rule as text string
     *                    (`off`, `soft`, or `hard`).
     *
     */
    tasksServ.getWorkflowRule = function(taskId) {
      return configService.getConfiguration().then(function(config) {

        // We parse the JSON containing the task-specific exceptions to the global workflow setting.
        var workflowExceptions;
        try {
          workflowExceptions = JSON.parse(config['Workflow Exceptions']);
        } catch(e) {
          console.log('ERROR: An error occured while trying to parse workflowExceptions:');
          console.log(e);

          workflowExceptions = {};
        }

        if(workflowExceptions['tasks/' + taskId.toString()] !== undefined) {
          // An exception has been configured for this task, we return the defined exception
          return workflowExceptions['tasks/' + taskId.toString()];
        } else if(config['Workflow Mode'] !== undefined) {
          // A default workflow mode has been set in the global configuration, we return this one
          return config['Workflow Mode'];
        } else {
          // No matching workflow configuration was found, we return the default ('soft')
          return 'soft';
        }
      });
    };



    /**
     * @ngdoc method
     * @name tasksServices.tasksService#saveResults
     * @methodOf tasksServices.tasksService
     * @kind function
     *
     * @description
     * This method saves the user-provided results for the given `task` as a CSV file in the Task Results library. If a
     * CSV file for this task already exists, it will be overwritten. If no file exists, a new one will be created. The
     * CSV will contain all key columns of the original task data (to enable linking the results) based on the `isKey`
     * parameter in the task column definition, plus the `result` column which holds the user-provided result.
     *
     * It returns a promise that will be resolved when the results have been successfully saved.
     *
     *
     * @param {Object} task       The task for which results should be saved, in the format returned by
     *                            {@link tasksServices.tasksService#getTask `getTask`}.
     *
     * @param {Array}  taskData   The taskData object with `result` column which will be stored (the object should be
     *                            in the format returned by {@link tasksServices.tasksService#getTaskData `getTaskData`},
     *                            amended with a `result` column which holds the user-defined results).
     *
     * @returns {Promise} A promise which resolves if saving the results was successful.
     *
     */

    tasksServ.saveResults = function(task, taskData) {
      var csvLines = [];

      // Key columns
      var keyColumns = task.dataSpecification.dataFileFormat.columns.filter(function(col) { return col.isKey; });

      // Header line
      csvLines.push(
        '"' + keyColumns.map(function(col) { return col.source.replace(/"/g, '""'); }).join('","') + '","result"\n'
      );

      taskData.forEach(function(d) {
        if(d.result !== undefined && d.result !== null && d.result !== '') {
          // only store actual results
          csvLines.push(
            keyColumns.map(function(col) {
              if(col.format === 'text') {
                return '"' + d[col.display].replace(/"/g, '""') + '"';
              } else {
                return d[col.display];
              }
            }).join(',') + ',"' + d.result + '"\n'
          );
        }
      });

      // Turn these csvLines into a blob for addFile
      var csvBlob = new Blob(csvLines, { type: 'text/csv' });

      // Call addFile to store the file (overwrite if necessary)
      return restDocumentService.addFile('task_results', 'task_' + task.id + '.csv', { 'taskId': task.id }, csvBlob, true)
        .then(function() {
          // Clear existing promise (if set)
          taskResultsPromise['live_' + task.id] = undefined;
	  tasksPromiseWithResults[0] = undefined;
        });
    };






    /**
     * @ngdoc method
     * @name tasksServices.tasksService#mergeResults
     * @methodOf tasksServices.tasksService
     * @kind function
     *
     * @description
     * This method will merge task data with results data obtained from elsewhere (e.g. Dashboard-internal store or an
     * external Excel file or similar).
     *
     *
     * @param {Object} columnDef       The task for which results should be loaded, in the format returned by
     *                                 {@link tasksServices.tasksService#getTask `getTask`}.
     *
     * @param {Array}  taskData        The taskData (in the format returned by
     *                                 {@link tasksServices.tasksService#getTaskData `getTaskData`}) which will be
     *                                 amended with a `result` column based on the retrieved data.
     *
     * @param {Array} resultData       An object containing at least the key columns (according to `columnDef`) and
     *                                 a results column (see `resultsColumn` below) that will be mapped. Note that for
     *                                 elements in `taskData` that have no corresponding row in `resultData` the
     *                                 `result` column will be cleared.
     *
     * @param {string=} resultsColumn  An optional string that specifies the name of the column which contains the
     *                                 results in the `resultData` objects. If not given, the default `result` will
     *                                 be assumed.
     *
     *
     * @returns {Array}   The merged `taskData` object.
     *
     */

    tasksServ.mergeResults = function(columnDef, taskData, resultData, resultsColumn) {
      // Set default value for resultsColumn
      if(resultsColumn === undefined) {
        resultsColumn = 'result';
      }

      // Create key hashes object for faster mapping
      var keyColumns = columnDef.filter(function(col) { return col.isKey; });
      var makeHash = function(d) {
        return keyColumns.map(function(col) { return d[col.display]; }).join(':::');
      };

      var keyHashes = {};
      taskData.forEach(function(d) {
        // Add data element to key hashes
        keyHashes[makeHash(d)] = d;

        // Remove existing results, if any
        d.result = undefined;
      });

      // Perform the mapping to the result column
      resultData.forEach(function(row) {
        var thisElement = keyHashes[makeHash(row)];
        if(thisElement) {
          thisElement.result = row[resultsColumn];
        }
      });

      return taskData;
    };


    
    /**
     * @ngdoc method
     * @name tasksServices.tasksService#compressTask
     * @methodOf tasksServices.tasksService
     * @kind function
     *
     * @description 
     *
     * This method will compress merged task data (i.e. complete task data
     * containg a `resultsColumn`) based on the given criteria, i.e. by
     * reducing its columns (as per the `compressColumns` array) and its rows
     * (as per the `compressResults` array). The result will be a new merged
     * task element, that can then be written to a new task using the
     * {@link tasksServices.tasksService#newTask `newTask`} method of this
     * service.
     *
     * @param {Object} columnDef       The task for which results should be loaded, in the format returned by
     *                                 {@link tasksServices.tasksService#getTask `getTask`}.
     *
     * @param {Array}  mergedTaskData  The merged task and results data (in the format returned by
     *                                 {@link tasksServices.tasksService#mergeResults `mergeResults`}), which
     *                                 will be reduced according to the `compressColumns` and `compressResults`
     *                                 arrays.
     *
     * @param {Array=} compressColumns An array containing the columns (given using their display name) that should
     *                                 be kept in the resulting compressed task. If omitted or empty, all columns
     *                                 will be kept.
     *
     * @param {Array=} compressResults An array containing entries in the `resultsColumn`. Only lines with these
     *                                 values in the results column will be kept in the resulting compressed task.
     *                                 If omitted or empty, all columns will be kept.
     *
     * @param {string=} resultsColumn  An optional string that specifies the name of the column which contains the
     *                                 results in the `resultData` objects. If not given, the default `result` will
     *                                 be assumed.
     *
     * @returns {Array}   An array containing the compressed `columnDef` and the compressed, merged `taskData`
     *                    object.
     *
     */

    tasksServ.compressTask = function(columnDef, mergedTaskData, compressColumns, compressResults, resultsColumn) {
      // Set default value for resultsColumn
      if(resultsColumn === undefined) {
        resultsColumn = 'result';
      }

      // Filter columnDef
      var newColumnDef = columnDef.filter(
	function(col) {
	  return (!compressColumns || compressColumns.indexOf(col.display) !== -1);
	}
      );

      // We will need to add the results Column to the columns definition (if
      // included in compressColumns)
      if(!compressColumns || compressColumns.indexOf(resultsColumn) !== -1) {
        newColumnDef.push({
	  source: resultsColumn,
	  display: resultsColumn,
	  keyword: resultsColumn.replace(/[^a-zA-Z0-9]/g, '_'),
	  format: 'text',
	  isKey: false
	});
      }
      
      // Filter content object (needs to make copies of each element)
      var newTaskData = [];
      mergedTaskData.forEach(
	function(d) {
          if(!compressResults || compressResults.indexOf(d[resultsColumn]) !== -1) {
            var newD = {};

	    Object.keys(d).forEach(function(col) {
              if(!compressColumns || compressColumns.indexOf(col) !== -1) {
		newD[col] = d[col];
	      }
	    });

	    newTaskData.push(newD);
	  }
	}
      );
      
      return [newColumnDef, newTaskData];
    };


    
    /**
     * @ngdoc method
     * @name tasksServices.tasksService#newTask
     * @methodOf tasksServices.tasksService
     * @kind function
     *
     * @description 
     *
     * This method will add a new task to the OCA task library.
     *
     * @param {Object}   task            The details of the task that should be added to the OCA task library
     *                                   (object as returned by 
     *                                   {@link tasksServices.tasksService#getTask `getTask`}).
     *
     * @param {Array}    taskData        The data of the task to be added, as returned by the various reader
     *                                   functions. Will be converted to CSV data prior to being written, using
     *                                   the specification from `task.dataSpecification.dataFileFormat`.
     *
     * @param {string}   fileName        The filename for the new task in the task data document library.
     *
     * @param {boolean}  overwrite       If `true`, an existing task with the same `fileName` will be
     *                                   overwritten. If set to `false` (or not set at all), an existing task
     *                                   with the same name will produce an error.
     *
     * @returns {Promise}     A promise that will return after successful saving of the new task with the
     *                        original `task` object completed with the new ID obtained.
     *
     */

    tasksServ.newTask = function(task, taskData, fileName, overwrite) {
      // Default values
      overwrite = (overwrite ? true : false);
      
      // First we need to convert the task definition to the valid REST object for SharePoint
      var restTask = taskToREST(task);

      // Now we need to create a CSV file out of the task data, following the
      // data specification provided in the task object
      var csvString = datafileWriter.formatFile(taskData, task.dataSpecification.dataFileFormat);

      // Now we can add the new task to the task data library      
      return restDocumentService
	.addArrayBufferAsFile('task_data', fileName, restTask, csvString, overwrite)
	.then(
	  function(res) {
            if(!res.data || !res.data.d) {
	      console.log('An unexpected response was received while trying to write the OCA task to the library:');
	      console.log(res);
	      
              return $q.reject(gettext('An unexpected response was received while trying to write the OCA task to the library.'));
	    }
	    
	    task.id = res.data.d.Id;
	    return task;
	  }
	);
    };


    

    /**
     * @ngdoc method
     * @name tasksServices.tasksService#loadResults
     * @methodOf tasksServices.tasksService
     * @kind function
     *
     * @description
     * This method will load results data which has been stored using the
     * {@link tasksServices.tasksService#saveResults `saveResults`} method. It will change the provided `taskData`
     * array by adding a `result` column to the entries which contains the loaded result data.
     *
     *
     * @param {Object} task         The task for which results should be loaded, in the format returned by
     *                              {@link tasksServices.tasksService#getTask `getTask`}.
     *
     * @param {boolean} isArchived  Set to `true` if this task is an archived task and needs to be retrieved from the
     *                              archive data.
     *
     * @param {Array}  taskData   The taskData (in the format returned by
     *                            {@link tasksServices.tasksService#getTaskData `getTaskData`}) which will be amended
     *                            with a `result` column based on the retrieved data.
     *
     * @returns {Promise} A promise which resolves to the updated taskData object if the results retrieval was
     *                    successful.
     *
     */

    tasksServ.loadResults = function(task, isArchived, taskData) {
      var cacheKey = (isArchived ? 'archive_' : 'live_') + task.id;

      // Do we already have a promise we can return?
      if(taskResultsPromise[cacheKey]) {
        return taskResultsPromise[cacheKey];
      }

      // Data format (based on the key columns of the original task)
      var keyColumns = task.dataSpecification.dataFileFormat.columns.filter(function(col) { return col.isKey; });
      var endpoint = (isArchived ? 'task_results_archive' : 'task_results');
      var dataFileFormat = {
        'separator': ',',
        'columns': keyColumns.slice(0)
      };

      dataFileFormat.columns.push({
        source: 'result',
        display: 'result',
        keyword: 'result',
        format: 'text',
        isKey: false
      });


      // We need to return a new promise. We get the deferred file URI and then open that file using the
      // datafileReader.
      taskResultsPromise[cacheKey] = restDocumentService
        .getFileByFilename(endpoint, 'task_' + task.id + '.csv')
        .then(
          function(res) {
            // Do we have data in the right format?
            if(!res || !res.data || !res.data.d || !res.data.d.File || !res.data.d.File.__deferred || !res.data.d.File.__deferred.uri) {
              console.log('ERROR: While trying to load the task results the data returned was not in the expected format:');
              console.log(res);

              return $q.reject(gettext('While trying to load the task results the data returned was not in the expected format!'));
            }

            // Get file path
            return restDocumentService.expandFilePath(res.data.d.File.__deferred.uri)
              .then(function(path) {
                return datafileReader.readFormattedFile(path[0], dataFileFormat);
              })
              .then(function(readerResult) {
                return tasksServ.mergeResults(dataFileFormat.columns, taskData, readerResult[0]);   // Return the modified task data array.
              });
          },
          function(err) {
            // We need to treat this error specially, because if a 404 has been returned this simply means we do not (yet) have
            // task results for this task. This should not be treated as an error but simply fail by returning the original taskData
            // object.
            if(err.status === 404) {
              return taskData;
            } else {
              console.log('ERROR: An unexpected error occured while trying to load task results:');
              console.log(err);

              return $q.reject(gettext('An error occured while trying to load task results.'));
            }
          }
        );

      return taskResultsPromise[cacheKey];
    };




    /**
     * @ngdoc method
     * @name tasksServices.tasksService#hasArchivePermissions
     * @methodOf tasksServices.tasksService
     * @kind function
     *
     * @description
     * This method determines whether the logged-in user may archive tasks by moving them to the archive library.
     *
     * A user has permission to archive task if and only if he/she has write access to the archive document library.
     *
     * @returns {Promise} A promise which resolves to `true` if the logged-in user may archive tasks and `false`
     *                    otherwise.
     *
     */

    tasksServ.hasArchivePermissions = function() {
      return usersService.hasRights('task_data_archive');
    };


    return tasksServ;

  }]);
