'use strict';

/**
 * @ngdoc overview
 * @name dashboardViewServices
 * @description
 *
 * This module exposes services to retrieve the "Ongoing Risk Assessment" (ORA) dashboard layout and components for
 * individual tree entities.
 *
 * At the moment, the dashboards can contain the following view element types, which are mapped by the
 * {@link dashboardTree.controller:treeDashboardController treeDashboardController} to the right directives
 * to be included:
 *
 *  * `notes`: A note taking and display element which allows for notes to be recorded, edited and displayed on the
 *               specific dashboard. See {@link notesDirectives.dashboardNotes dashboardNotes}.
 *
 *  * `riskTracker`: A risk tracker, which displays a impact-likelihood-matrix on which risks can be entered and
 *                   updated. It also allows to view historic snapshots of risks at a given point in time. See
 *                   {@link riskTrackerDirectives.riskTracker riskTracker}.
 *
 *  * `timeSeries`: Loads time series data from a CSV file and displays it as either an interactive line or stacked
 *                  area chart. The charts can allow for note-taking on the time series data. See
 *                  {@link chartDirectives.timeSeriesChart timeSeriesChart}.
 *
 */

angular.module('dashboardViewServices', ['gettext', 'datafilesServices', 'restServices'])

  /**** CONFIGURATION FOR AVAILABLE DASHBOARD VIEW TYPES ****/

  .value('dashboardViewTypeDescriptions', {    // Has to correspond to option values in dashboardViewsDetails.tmpl.html
    html: 'HTML',
    iframe: 'IFrame',
    notes: 'Notes',
    timeSeries: 'Time Series Chart',
    stackedBars: 'Stacked Bar Chart',
    scatterplot: 'Scatterplot Chart',
    portfolio: 'Portfolio Chart',
    table: 'Table',
    riskTracker: 'Risk Tracker Matrix',
    objectExplorer: 'Object Explorer',
    tabDivider: 'Tab Divider'
  })

  /**
   * @ngdoc service
   * @name dashboardViewServices.dashboardViewService
   * @description
   *
   * This service provides various methods to obtain details of dashboard layouts and components for individual tree
   * entities. It thus provides structured access to the relevant SharePoint list "Dashboard - Views".
   *
   */

  .factory('dashboardViewService', ['restListService', '$q', 'gettext', 'dashboardViewTypeDescriptions', function(restListService, $q, gettext, dashboardViewTypeDescriptions) {
    var viewServ = {};

    var viewsPromises = {};


    /**
     * @ngdoc method
     * @name dashboardViewServices.dashboardViewService#getDashboardElements
     * @methodOf dashboardViewServices.dashboardViewService
     * @kind function
     *
     * @description
     * Retrieves all view elements of the dashboard specified by its `dashboardID` ordered by the ordering specified
     * in the configuration data (in SharePoint table "Dashboard - Views"). Each array element has the following
     * structure:
     *
     * ```
     *  viewElement = {
     *    id: <entity ID>,
     *    title: <name of the entity>,
     *    type: <Type of the dashboard element, e.g. timeSeriesChart, notes, riskTracker etc.>,
     *    options: <Object containing specific configuration options for the given element type (e.g. chart formatting)>
     *  }
     * ```
     *
     * For the available element types, see the {@link dashboardViewServices} overview.
     *
     *
     * @param {number} dashboardID The ID of the tree element whose dashboard elements should be retrieved.
     *
     * @param {boolean=} forceReload If set to `true` will reload data from SharePoint even if we already have
     *                               an existing dataset.
     *
     * @returns {Promise} Returns a single promise that resolves to an array which contains the
     * dashboard elements configured for the given dashboardID, ordered in their specified ordering.
     */

    viewServ.getDashboardElements = function(dashboardID, forceReload) {
      // Deferred action
      var deferred;

      // Valid dashboard ID?
      if (isNaN(dashboardID)) {
        // Invalid dashboard ID - return an already rejected promise.
        deferred = $q.defer();
        deferred.reject(gettext('Error retrieving dashboard view: Supplied dashboard ID is invalid.'));
        return deferred.promise;
      }

      // Do we already retrieve this dashboard and do not want a forced reload?
      if (viewsPromises[dashboardID] && !forceReload) {
        return viewsPromises[dashboardID];
      }

      // No, dashboard is not yet (being) retrieved or we force a reload. Create a new promise.
      deferred = $q.defer();
      viewsPromises[dashboardID] = deferred.promise;

      // Ok. First we need to get the views.
      // We can use getFilteredItems here as we do not expect to get more than 5000 view elements for a single tree
      // element. Note, however, that most likely the lookup field filter will fail if there are more than the list
      // view threshold element objects in the list overall (a restriction in SharePoint with lookup fields). The only
      // solution would be to change the treeElement field into a generic numeric field... to be evaluated at a later
      // stage.
      restListService.getFilteredItems('views', 'treeElement eq ' + dashboardID, undefined, undefined, 'orderAsc')
        .then(
          function (res) {
            // Ok, we got the view elements and can parse them.
            var viewsList = [];

            // Are we getting the right data?
            if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
              // Reject promise.
              deferred.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
              return;
            }

            // Ok, format properly.
            try {
              viewsList = res.data.d.results.map(
                function(d) {
                  // TODO: Remove debug code
                  console.log('Mapping view item: ');
                  console.log(d);

                  return {
                    id: d.Id,
                    title: d.Title,
                    type: d.elementType,
                    viewKey: d.viewKey,
                    options: JSON.parse(d.elementOptionsJSON),
                    orderAsc: d.orderAsc
                  };
                }
              );
            } catch (err) {
              deferred.reject(gettext('An error occurred parsing the dashboard view, probably some view options JSON is invalid.'));
            }

            // Ok, resolve promise
            deferred.resolve(viewsList);
          },
          function (restErr) {
            deferred.reject(gettext('An error occurred loading the dashboard view from SharePoint:') + ' ' + gettext('Error code') + ' ' + restErr.status + ', ' + restErr.statusText);
          }
        );

      // Return promise to dashboard view
      return viewsPromises[dashboardID];
    };


    /**
     * @ngdoc method
     * @name dashboardViewServices.dashboardViewService#isValidElement
     * @methodOf dashboardViewServices.dashboardViewService
     * @kind function
     *
     * @description
     * This method returns `true` if the given dashboard `element` is valid, which means it needs to contain at least
     * a title, valid type, valid (if empty) options object, numeric `orderAsc` and (if it's not a new element)
     * an `id` value.
     *
     *
     * @param {Object} element The JavaScript object representing the dashboard element that should be validated.
     *
     * @param {boolean} isNew This should be set to `true` if the element is new and not yet added to the database
     *                        (in which case it's valid not to have an `id` value) and `false` otherwise.
     *
     * @returns {boolean} Returns `true` if the dashboard element is valid and can be added to the database or `false`
     *                    otherwise.
     */

    viewServ.isValidElement = function(element, isNew) {
      return (
        element.title !== undefined &&
        element.title !== '' &&
        element.viewKey !== undefined &&
        element.viewKey !== '' &&
        !isNaN(element.orderAsc) &&
        element.options !== undefined &&
        dashboardViewTypeDescriptions[element.type] !== undefined &&
        (isNew || !isNaN(element.id))
      );
    };



    /**
     * @ngdoc method
     * @name dashboardViewServices.dashboardViewService#asRestElement
     * @methodOf dashboardViewServices.dashboardViewService
     * @kind function
     *
     * @description
     * Converts the dashboard view element from the Dashboard-internal representation (as returned by
     *  {@link dashboardViewServices.dashboardViewService#getDashboardElements getDashboardElements}
     *  method) into the SharePoint-specific representation expected by the SharePoint REST API.
     *
     * @param {integer} dashboardID The dashboard ID of the dashboard the element should be located on.
     *
     * @param {Object} element The tree element to be converted into the SharePoint representation.
     *
     * @returns {Object} Returns the SharePoint-specific representation of the given `element` as JavaScript object.
     */

    viewServ.asRestElement = function(dashboardID, element) {
       var restElement = {
         'Title': element.title,
         'viewKey': element.viewKey,
         'elementType': element.type,
         'elementOptionsJSON': JSON.stringify(element.options),
         'orderAsc': element.orderAsc,
         'treeElementId': dashboardID
       };

      if(!isNaN(element.id)) {
        restElement.Id = element.id;
      }

      return restElement;
    };



    /**
     * @ngdoc method
     * @name dashboardViewServices.dashboardViewService#addDashboardElement
     * @methodOf dashboardViewServices.dashboardViewService
     * @kind function
     *
     * @description
     * This method adds new dashboard elements to the relevant SharePoint list after ensuring that they are valid
     * dashboard elements.
     *
     * @param {integer} dashboardID The numeric ID of the dashboard the element should be added to.
     *
     * @param {Object} element The JavaScript object representing the dashboard element that should be added
     *                         to the given dashboardID. Must be in the format returned by
     *                         {@link dashboardViewServices.dashboardViewService#getDashboardElements getDashboardElements}.
     *
     * @returns {Promise} Returns a single promise that resolves to the completed element (i.e. including element ID)
     *                    after it has been added.
     */

    viewServ.addDashboardElement = function(dashboardID, element) {
      var deferred = $q.defer();

      if(!viewServ.isValidElement(element, true)) {
        deferred.reject(gettext('The view element you are trying to add is not valid. Please complete its title.'));
        return deferred.promise;
      }

      // Send to server
      restListService.addItem('views', viewServ.asRestElement(dashboardID, element))
        .then(
          // Get ID and then update element with new id
          function(res) {
            // TODO: remove debug code
            console.log('Added dashboard view element to SharePoint list:');
            console.log(res);

            element.id = res.data.d.Id;
            deferred.resolve(element);
          },
          function(restErr) {
            deferred.reject(gettext('An error occurred when saving the new element to the server:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
          }
        );

      return deferred.promise;
    };




    /**
     * @ngdoc method
     * @name dashboardViewServices.dashboardViewService#changeDashboardElement
     * @methodOf dashboardViewServices.dashboardViewService
     * @kind function
     *
     * @description
     * This method changes an existing dashboard element on the relevant SharePoint list after validating the changed
     * element for correctness.
     *
     * @param {integer} dashboardID The numeric ID of the dashboard the element is on.
     *
     * @param {Object} element The JavaScript object representing the dashboard element that should be updated.
     *                         Must be in the format returned by
     *                         {@link dashboardViewServices.dashboardViewService#getDashboardElements getDashboardElements}.
     *
     * @returns {Promise} Returns a single promise that resolves to the changed element after it has been added.
     */

    viewServ.changeDashboardElement = function(dashboardID, element) {
      var deferred = $q.defer();

      if(!viewServ.isValidElement(element, false)) {
        deferred.reject(gettext('The view element you are trying to change is not valid. Please complete its title.'));
        return deferred.promise;
      }

      // Send to server
      restListService.updateItem('views', element.id, viewServ.asRestElement(dashboardID, element))
        .then(
          // Get ID and then update element with new id
          function(res) {
            // TODO: remove debug code
            console.log('Updated dashboard view element on SharePoint list:');
            console.log(res);

            deferred.resolve(element);
          },
          function(restErr) {
            deferred.reject(gettext('An error occurred when saving the changed element to the server:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
          }
        );

      return deferred.promise;
    };




    /**
     * @ngdoc method
     * @name dashboardViewServices.dashboardViewService#deleteDashboardElement
     * @methodOf dashboardViewServices.dashboardViewService
     * @kind function
     *
     * @description
     * This method deletes the given dashboard view element from the database.
     *
     * @param {Object} element The JavaScript object representing the dashboard element that should be removed.
     *                         Must have a valid `id` value that will be used to identify it on SharePoint.
     *
     * @returns {Promise} Returns a promise that resolves when the element has been deleted.
     */

    viewServ.deleteDashboardElement = function(element) {
      // Send to server
      return restListService.deleteItem('views', element.id)
        .then(
          // Get ID and then update element with new id
          function(res) {
            // TODO: remove debug code
            console.log('Removed dashboard view element on SharePoint list:');
            console.log(res);
          },
          function(restErr) {
            return gettext('An error occurred when deleting the element on the server:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText;
          }
        );
    };


    return viewServ;
  }])


  /**
   * @ngdoc filter
   * @name dashboardViewServices.typeToDescription
   * @description
   *
   * This filter transforms the internal dashboard view types (which are stored in the `type` value of the
   * dashboard element) into a localized descriptive text.
   *
   */

  .filter('typeToDescription', ['gettext', 'dashboardViewTypeDescriptions', function(gettext, dashboardViewTypeDescriptions) {
    return function(input) {
      return (dashboardViewTypeDescriptions[input] ? gettext(dashboardViewTypeDescriptions[input]) : gettext('unknown'));
    };
  }]);
