'use strict';

/**
 * @ngdoc overview
 * @name datafilesServices
 * @description
 *
 * This module exposes services to obtain the definition of and to retrieve the actual content of DSV data files
 * which are specified in the SharePoint list "Dashboard - Datafiles". These datafiles and their specification in the
 * SharePoint list form the basis for various chart and table displays throughout the application.
 *
 */

angular.module('datafilesServices', ['gettext', 'restServices', 'chartServices'])


  /**
   * @ngdoc service
   * @name datafilesServices.datafilesService
   * @description
   *
   * This service provides methods to obtain the definition of datafiles specified in the SharePoint list
   * "Dashboard - Datafiles". These datafile specifications in the SharePoint list form the basis for various chart
   * and table displays throughout the application.
   *
   */

  .factory('datafilesService', ['restListService', '$q', 'gettext', 'd3', function(restListService, $q, gettext, d3) {
    var datafilesServ = {};

    var datafilesPromise;



    /**
     * @ngdoc method
     * @name datafilesServices.datafilesService#getAllDatafiles
     * @methodOf datafilesServices.datafilesService
     * @kind function
     *
     * @description
     * Retrieves all datafiles specified in the SharePoint list "Dashboard - Datafiles" as an unordered array. Each
     * array element has the following structure:
     *
     * ```
     *  datafile = {
     *    id: <ID of the file>
     *    dataFile: <identifier of the file (string,
     *               only alphanumeric characters and underscore)>,
     *    dataFileFormat: <format object that specifies
     *                     the format of the source file>,
     *    fileName: <Location of the data file to be retrieved
      *              (relative to the application or absolute URL)>
     *  }
     * ```
     *
     * The datafiles need to be delimiter-separated text files where the first line should contain column headings. They
     * are mapped to application internal data using their `dataFileFormat` specification, which has the following
     * format:
     *
     * ```
     * dataFileFormat = {
     *    separator: <string which separates columns, e.g. ';' or ','>,
     *    columns: [                      // Array of column definitions
     *      {
     *       source: <name of the column in the source file
     *                as specified on its first line>,
     *       display: <name of the column which should be
      *                displayed to the user in charts or tables>,
     *       keyword: <internal keyword of the column to be used for
      *                attaching notes to it, alphanumeric
       *               characters and underscore only>,
     *       format: <string specifying the formatter
     *                applied to the column values, see below>,
     *       isKey: <true or false, specifies whether column is
     *               used to uniquely identify each line>
     *      },
     *      {
     *       ...
     *      },
     *      ...
     *    ]
     * }
     * ```
     *
     * The formatters supported by The Dashboard are specified in {@link chartServices.d3}.
     *
     * @param {boolean=} forceReload If set to `true` will re-retrieve all datafile definitions from the server
     *                               even if they have already been retrieved.
     *
     * @returns {Promise} Returns a single promise that resolves to an array which contains the definitions of all
     * datafiles specified in the system.
     */

    datafilesServ.getAllDatafiles = function(forceReload) {
      // Do we already have an active promise and do not force a reload? Then we just return this one.
      if(!forceReload && datafilesPromise) {
        return datafilesPromise;
      }

      // Ok, no promise exists yet or we want to force a reload, so we define a new one.
      var deferred = $q.defer();
      datafilesPromise = deferred.promise;

      // Ok, now we can actually get the list from SharePoint
      // We can use getAllItems as the datafiles list should not exceed SharePoint's list view threshold (5000 items).
      restListService.getAllItems('datafiles').then(
        function (res) {
          // Are we getting the right data?
          if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
            // Returns already rejected promise
            deferred.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
            return;
          }

          // Ok, data is correct. Format properly...
          var datafilesMap = {};

          try {
            // We "try" this, as JSON might not be formatted properly (will throw SyntaxError)
            res.data.d.results.forEach(function(d) {
              datafilesMap[d.Title] = {
                id: d.Id,
                dataFile: d.Title,
                dataFileFormat: JSON.parse(d.dataFileFormatJSON),
                fileName: d.fileName
              };
            });

            // Resolve promise.
            deferred.resolve(datafilesMap);
          } catch(err) {
            // Reject promise - maybe JSON was not formatted properly.
            deferred.reject(gettext('Error parsing data file format:') + ' ' + err.message);
          }
        },
        function(errRest) {
          // An error occurred, reject promise.
          deferred.reject(gettext('An error occurred:') + ' ' + gettext('Status code') + ' ' + errRest.status + ', ' + errRest.statusText);
        }
      );

      // Ok, lets return our datafilesPromise.
      return datafilesPromise;
    };

    /**
     * @ngdoc method
     * @name datafilesServices.datafilesService#getDatafile
     * @methodOf datafilesServices.datafilesService
     * @kind function
     *
     * @description
     * Retrieves the definition of a specific datafile, as specified by `dataFileName`, which needs to match the
     * identifier of the datafile specified in the Title column of the SharePoint list "Dashboard - Datafiles".
     *
     * The returned object has the format for a single datafile as specified in
     * {@link datafilesServices.datafilesService#getAllDatafiles getAllDatafiles}.
     *
     * @param {string} dataFileName The identifier of the datafile whose definition is to be retrieved, needs to
     * match the Title column of the SharePoint list "Dashboard - Datafiles".
     *
     * @param {boolean=} forceReload If set to `true` will re-retrieve all datafile definitions from the server
     *                               even if they have already been retrieved.
     *
     * @returns {Promise} Returns a single promise that resolves to an object which contains the definition of the
     * specified datafile.
     *
     */

    datafilesServ.getDatafile = function(dataFileName, forceReload) {
      // We get the list of all data files and then filter the correct one
      return datafilesServ.getAllDatafiles(forceReload)
        .then(
          function(datafilesMap) {
            return datafilesMap[dataFileName];
          }
        );
    };



    /**
     * @ngdoc method
     * @name datafilesServices.datafilesService#getDatafileById
     * @methodOf datafilesServices.datafilesService
     * @kind function
     *
     * @description
     * Retrieves the definition of a specific datafile by its SharePoint-internal ID.
     *
     * The returned object has the format for a single datafile as specified in
     * {@link datafilesServices.datafilesService#getAllDatafiles getAllDatafiles}.
     *
     * @param {integer} datafileId The ID of the SharePoint element of the datafile whose definition is to be retrieved.
     *
     * @param {boolean=} forceReload If set to `true` will re-retrieve all datafile definitions from the server
     *                               even if they have already been retrieved.
     *
     * @returns {Promise} Returns a single promise that resolves to an object which contains the definition of the
     * specified datafile.
     *
     */

    datafilesServ.getDatafileById = function(datafileId, forceReload) {
      // We get the list of all data files and then filter the correct one
      return datafilesServ.getAllDatafiles(forceReload)
        .then(
          function(datafilesMap) {
            var foundDatafile;

            Object.keys(datafilesMap).some(
              function(k) {
                if(datafilesMap[k].id === datafileId) {
                  // We found it!
                  foundDatafile = datafilesMap[k];
                  return true;
                }

                return false;
              }
            );

            return foundDatafile;
          }
        );
    };




    /**
     * @ngdoc method
     * @name datafilesServices.datafilesService#isValidDatafile
     * @methodOf datafilesServices.datafilesService
     * @kind function
     *
     * @description
     * This method returns `true` if the given datafile is valid, which means it needs to contain all required
     * fields and they must match the required patterns.
     *
     *
     * @param {Object} datafile The JavaScript object representing the datafile that should be validated.
     *
     * @param {boolean} isNew This should be set to `true` if the datafile is new and not yet added to the database
     *                        (in which case it's valid not to have an `id` value) and `false` otherwise.
     *
     * @returns {boolean} Returns `true` if the datafile is valid and can be added to the database or `false`
     *                    otherwise.
     */

    datafilesServ.isValidDatafile = function(datafile, isNew) {
      var allColumnsValid = datafile.dataFileFormat && datafile.dataFileFormat.columns.every(function(col) {
          return col.source !== undefined &&
                 col.source !== '' &&
                 col.display !== undefined &&
                 col.display !== '' &&
                 col.keyword.match(/^[a-zA-Z0-9_-]+$/) &&
                 d3.ourSettings.formats[col.format] !== undefined;
        });

      return (
        datafile.dataFile.match(/^[a-zA-Z0-9_-]+$/) &&
        allColumnsValid &&
        datafile.dataFileFormat.separator !== undefined &&
        datafile.separator !== '' &&
        datafile.fileName !== undefined &&
        datafile.fileName !== '' &&
        (isNew || !isNaN(datafile.id))
      );
    };



    /**
     * @ngdoc method
     * @name datafilesServices.datafilesService#asRestData
     * @methodOf datafilesServices.datafilesService
     * @kind function
     *
     * @description
     * Converts the datafile from the Dashboard-internal representation into the SharePoint-specific representation
     * expected by the SharePoint REST API.
     *
     * @param {Object} datafile The datafile object to be converted into the SharePoint representation.
     *
     * @returns {Object} Returns the SharePoint-specific representation of the given `datafile` as JavaScript object.
     */

    datafilesServ.asRestData = function(datafile) {
      // We remove the $$hashKey, which is added by ng-repeat
      var dataFileFormatCopy = JSON.parse(JSON.stringify(datafile.dataFileFormat ? datafile.dataFileFormat : {}));
      dataFileFormatCopy.columns.forEach(function(d) {
        d.$$hashKey = undefined;
      });

      var restElement = {
        Title: datafile.dataFile,
        fileName: datafile.fileName,
        dataFileFormatJSON: JSON.stringify(dataFileFormatCopy)
      };

      if(!isNaN(datafile.id)) {
        restElement.Id = datafile.id;
      }

      return restElement;
    };



    /**
     * @ngdoc method
     * @name datafilesServices.datafilesService#addDatafile
     * @methodOf datafilesServices.datafilesService
     * @kind function
     *
     * @description
     * This method adds new datafile definitions to the relevant SharePoint list after ensuring that they are valid.
     *
     * @param {Object} datafile JavaScript object representing the datafile  that should be added.
     *
     * @returns {Promise} Returns a single promise that resolves to the completed datafile definition
     *                    (i.e. including ID) after it has been added.
     */

    datafilesServ.addDatafile = function(datafile) {
      var deferred = $q.defer();

      if(!datafilesServ.isValidDatafile(datafile, true)) {
        deferred.reject(gettext('The datafile you are trying to add is not valid.'));
        return deferred.promise;
      }

      // Send to server
      restListService.addItem('datafiles', datafilesServ.asRestData(datafile))
        .then(
          // Get ID and then update datafile with new id
          function(res) {
            // TODO: remove debug code
            console.log('Added datafile definition to SharePoint list:');
            console.log(res);

            // We remove all caches such that changed datafile is also used next time
            datafilesPromise = undefined;

            datafile.id = res.data.d.Id;
            deferred.resolve(datafile);
          },
          function(restErr) {
            deferred.reject(gettext('An error occurred when saving the new datafile definition to the server:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
          }
        );

      return deferred.promise;
    };



    /**
     * @ngdoc method
     * @name datafilesServices.datafilesService#changeDatafile
     * @methodOf datafilesServices.datafilesService
     * @kind function
     *
     * @description
     * This method changes an existing datafile definition on the relevant SharePoint list after validating the changed
     * data for correctness.
     *
     * @param {Object} datafile The JavaScript object representing the datafile definition that should be updated.
     *
     * @returns {Promise} Returns a single promise that resolves to the changed element after it has been added.
     */

    datafilesServ.changeDatafile = function(datafile) {
      var deferred = $q.defer();

      if(!datafilesServ.isValidDatafile(datafile, false)) {
        deferred.reject(gettext('The datafile definition you are trying to change is not valid.'));
        return deferred.promise;
      }

      // Send to server
      restListService.updateItem('datafiles', datafile.id, datafilesServ.asRestData(datafile))
        .then(
          // Get ID and then update element with new id
          function(res) {
            // TODO: remove debug code
            console.log('Updated datafile definition on SharePoint list:');
            console.log(res);

            // We remove all caches such that changed datafile is also used next time
            datafilesPromise = undefined;

            deferred.resolve(datafile);
          },
          function(restErr) {
            deferred.reject(gettext('An error occurred when saving the changed datafile definition to the server:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
          }
        );

      return deferred.promise;
    };



    /**
     * @ngdoc method
     * @name datafilesServices.datafilesService#deleteDatafile
     * @methodOf datafilesServices.datafilesService
     * @kind function
     *
     * @description
     * This method deletes the given datafile definition from the database.
     *
     * @param {Object} datafile The JavaScript object representing the datafile definition that should be removed.
     *                          Must have a valid `id` value that will be used to identify it on SharePoint.
     *
     * @returns {Promise} Returns a promise that resolves when the datafile definition has been deleted.
     */

    datafilesServ.deleteDatafile = function(datafile) {
      // Send to server
      return restListService.deleteItem('datafiles', datafile.id)
        .then(
          // Get ID and then update element with new id
          function(res) {
            // TODO: remove debug code
            console.log('Removed datafile definition on SharePoint list:');
            console.log(res);

            // We remove all caches such that changed datafile is also used next time
            datafilesPromise = undefined;
          },
          function(restErr) {
            return gettext('An error occurred when deleting the datafile definition on the server:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText;
          }
        );
    };



    return datafilesServ;
  }])




  /**
   * @ngdoc service
   * @name datafilesServices.datafileReader
   * @description
   *
   * This service provides methods to obtain the actual content of a given datafile, read and formatted using the
   * datafile definitions specified for the file.
   *
   */

  .factory('datafileReader', ['datafilesService', '$q', 'gettext', 'd3', function(datafilesService, $q, gettext, d3) {
    var datafilesRead = {};

    var datafilesPromises = {};

    var formattedFilePromises = {};



    /**
     * @ngdoc method
     * @name datafilesServices.datafileReader#readDatafile
     * @methodOf datafilesServices.datafileReader
     * @kind function
     *
     * @description
     * Retrieves the content of the datafile given by the `file` identifier, formatted according to the datafile
     * specification. In particular, the specified `delimiter` will be used to separate columns and columns will be
     * renamed and formatted according to the `columns` specification.
     *
     * The Dashboard uses D3's [`dsv`](https://github.com/d3/d3/wiki/CSV#dsv) file reading method and returns the
     * content as given by that method.
     *
     *
     * @param {string} file The identifier of the datafile to be retrieved, needs to match the Title column of the
     * SharePoint list "Dashboard - Datafiles". (This is not the filename of the file, which is retrieved from its
     * definition.)
     *
     * @returns {Promise} Returns a single promise that resolves to an array which contains as first element the content
     *                    of the formatted file as read by D3's `dsv` method and as second element the column definitions
     *                    from the formatting specification.
     */

    datafilesRead.readDatafile = function(file) {
      // Is data file already being read?
      if(datafilesPromises[file]) {
        return datafilesPromises[file];
      }

      // Ok, we need to create a new promise.
      var deferred = $q.defer();
      datafilesPromises[file] = deferred.promise;

      // Ok, let's obtain the file format for this data file.
      datafilesService.getDatafile(file)
        .then(
          function(fileDefinition) {
            // Ok, now we can read the data using the formatted file reader...
            return datafilesRead.readFormattedFile(fileDefinition.fileName, fileDefinition.dataFileFormat);
          }
        )
        .then(deferred.resolve, deferred.reject);

      // Ok, we return the promise to our newly minted data file.
      return datafilesPromises[file];
    };


    /**
     * @ngdoc method
     * @name datafilesServices.datafileReader#readFormattedFile
     * @methodOf datafilesServices.datafileReader
     * @kind function
     *
     * @description
     * Retrieves the content of the file at the given `url`, formatted according to the given `formattingSpec`. In
     * particular, the specified `delimiter` will be used to separate columns and columns will be renamed and
     * formatted according to the `columns` specification.
     *
     * The Dashboard uses D3's [`dsv`](https://github.com/d3/d3/wiki/CSV#dsv) file reading method and returns the
     * content as given by that method.
     *
     *
     * @param {string} url The URL of the formatted file to be retrieved.
     *
     * @param {Object} formattingSpec A formatting specification object that contains the `delimiter` and the `columns`
     *                                specification for the file to be read.
     *
     * @returns {Promise} Returns a single promise that resolves to an array which contains as first element the content
     *                    of the formatted file as read by D3's `dsv` method and as second element the column definitions
     *                    from the formatting specification.
     */

    datafilesRead.readFormattedFile = function(url, formattingSpec) {
      // Is formatted file already being read?
      if(formattedFilePromises[url]) {
        return formattedFilePromises[url];
      }

      // Ok, let's obtain the file format for this data file.
      var dsvFormat = d3.dsvFormat(formattingSpec.separator);

      // Ok, we need to create a new promise.
      var deferred = $q.defer();
      formattedFilePromises[url] = deferred.promise;

      d3.text(url, function(error, text) {
        if(error) {
          deferred.reject(gettext('Error code') + ' ' + error.status + ', ' + error.statusText);
          return;
        }

        var rowArray = dsvFormat.parse(text);

        // We map all column names to their lowercase equivalent
        var lowerToCol = {};
        rowArray.columns.forEach(function(col) {
          lowerToCol[col.toLowerCase()] = col;
        });

        // We iterate over all rows
        var retArray = rowArray.map(function(d) {
          var res = {};
          // Format each column according to column definition provided
          formattingSpec.columns.forEach(function (col) {
            res[col.display] = d3.ourSettings.formats[col.format].parse(d[lowerToCol[col.source.toLowerCase()]]);
          });
          return res;
        });

        deferred.resolve([retArray, formattingSpec.columns]);
      });

      // Ok, we return the promise to our newly minted data file.
      return formattedFilePromises[url];
    };

    return datafilesRead;
  }])



  /**
   * @ngdoc service
   * @name datafilesServices.datafileWriter
   * @description
   *
   * This service provides methods to write a datafile based on a Dashboard-internal data object, written using the
   * datafile definitions provided.
   *
   */

  .factory('datafileWriter', ['datafilesService', '$q', 'gettext', 'd3', function(datafilesService, $q, gettext, d3) {
    var datafilesWrite = {};


    /**
     * @ngdoc method
     * @name datafilesServices.datafileWriter#formatFile
     * @methodOf datafilesServices.datafileWriter
     * @kind function
     *
     * @description
     *
     * This function takes data as provided by the datafileReader methods and formats it as a CSV file, which it
     * returns as a string. The formatting specification for the CSV file is read from `formattingSpec`, in the
     * same format as expected by the datafileReader methods.
     *
     * The Dashboard uses D3's [`dsv`](https://github.com/d3/d3/wiki/CSV#dsv) file writing method and returns the
     * CSV string as given by that method.
     *
     * @param {Array}  data           The data to be formatted as CSV file. Needs to be in the format as provided by
     *                                the datafileReader methods.
     *
     * @param {Object} formattingSpec A formatting specification object that contains the `delimiter` and the `columns`
     *                                specification for the file to be formatted.
     *
     * @returns {string}  Returns a string containing the formatted CSV data.
     *
     */

    datafilesWrite.formatFile = function(data, formattingSpec) {
      // Ok, let's obtain the file format for this data file.
      var dsvFormat = d3.dsvFormat(formattingSpec.separator);


      // We prepare the column header array for the CSV output (using the
      // "source" column names, which are used in the CSV data).
      var columnHeader = formattingSpec.columns.map(
	function(col) {
	  return col.source;
	}
      );
      

      // Ok, now we need to map our data object, taking into account the
      // differing column names (we will move from the internally-used
      // "display" column names to the CSV "source" column names) and the
      // format of each column (the D3 formatRows function expects an array of
      // arrays of strings).
      return dsvFormat.formatRows(
	[ columnHeader ].concat(
	  data.map(
	    function(d) {
	      var res = [];
	      
	      formattingSpec.columns.forEach(function(col) {
		return d3.ourSettings.formats[col.format].keyify(d[col.display]);
	      });

	      return res;
	    }
	  )
	)
      );
    };

    
    return datafilesWrite;
  }])



;
