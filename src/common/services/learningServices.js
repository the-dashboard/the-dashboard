'use strict';

/**
 * @ngdoc overview
 * @name learningServices
 * @description
 *
 * This module provides machine learning in the browser using web workers
 * that run in the background to perform data classification.
 *
 */

angular.module('learningServices', [])

/**
 * @ngdoc service
 * @name learningServices.learningService
 * @description
 *
 * This service provides classification methods to classify new samples
 * based on prior observations. It runs machine learning algorithms in
 * a separate web worker thread in the background.
 *
 */

  .factory('learningService', function() {
    var learningServ = {};
    var existingClassifiers = {};

    /**
     * @ngdoc method
     * @name learningServices.learningService#newClassifier
     * @methodOf learningServices.learningService
     * @kind function
     *
     * @description
     * This function creates a new Classifier object and initializes the
     * corresponding web worker that will perform the classification work.
     * You need to pass it the data set `data` it is supposed to classify
     * and an `updateCallback` function that will be called whenever new
     * classification results are available (for performance reasons,
     * the classifier will batch new classification results and provide
     * an array with many updates at once).
     *
     * @param {Array} data The dataset the classifier should work on
     * @param {Array} columns The columns the classifier should use from the dataset.
     * @param {Function} updateCallback The callback that will be called whenever
     *                                  new classification results are available
     *                                  (batched, first parameter is a hash that
     *                                  maps from the "id" column of the data to
     *                                  the classification result).
     *
     * @returns {Object} A new Classifier object that can be used to classify the
     *                   provided data set by passing training examples to it.
     */

    learningServ.newClassifier = function(data, columns, updateCallback) {
      var retClassifier = {};

      // Get new Web Worker
      retClassifier.worker = new Worker('scripts/workers/learningClassifyWorker.js');

      // Helper function to change the callback function
      retClassifier.changeCallback = function(newCallback) {
        this.updCallback = newCallback;
      };
      retClassifier.changeCallback(updateCallback);

      // Set up listener on our worker
      retClassifier.worker.onmessage = function(e) {
        // TODO - Remove debug code!
        console.log('Received data from worker: ');
        console.log(e);

        // Do we have a callback? If yes, run it.
        if(retClassifier.updCallback) {
          retClassifier.updCallback(e.data[0], e.data[1]);
        }
      };

      // Initialize Web Worker with provided data and columns
      retClassifier.worker.postMessage(['init', data, columns]);

      // Function that allows to push training examples for
      // training the classifier.
      retClassifier.train = function(trainData) {
        console.log('Posting TRAIN to worker.');
        console.log(trainData);
        retClassifier.worker.postMessage(['train', trainData]);
      };

      // Function that allows to push "unlearn" data to the
      // classifier.
      retClassifier.unlearn = function(unlearnData) {
        console.log('Posting UNLEARN to worker.');
        retClassifier.worker.postMessage(['unlearn', unlearnData]);
      };

      // Function that requests an update from classifier.
      retClassifier.requestUpdate = function(idxes) {
        console.log('Posting UPDATE to worker.');
        retClassifier.worker.postMessage(['update', idxes]);
      };

      // Function that asks classifier to re-iterate training.
      retClassifier.requestIterate = function() {
        console.log('Posting ITERATE to worker.');
        retClassifier.worker.postMessage(['iterate']);
      };

      // Return the new classifier object
      return retClassifier;
    };


    /**
     * @ngdoc method
     * @name learningServices.learningService#getClassifier
     * @methodOf learningServices.learningService
     * @kind function
     *
     * @description
     * Returns the existing classifier stored under the ID `id`. If no
     * such classifier exists, returns `undefined`.
     *
     * @param {string} id The unique ID of the classifier to be returned.
     *
     * @returns {Object} The existing classifier stored for the given ID or
     *                   `undefined` if no such classifier exists.
     */

    learningServ.getClassifier = function(id) {
       return existingClassifiers[id];
    };


    /**
     * @ngdoc method
     * @name learningServices.learningService#endClassifier
     * @methodOf learningServices.learningService
     * @kind function
     *
     * @description
     * Terminates the web worker for the classifier identified with
     * ID `id` and removes the classifier from the system.
     *
     * @param {string} id The unique ID of the classifier to be terminated.
     *
     * @returns {boolean} Returns `true` if the classifier existed and was be
     *                    terminated or `false` otherwise.
     */

    learningServ.endClassifier = function(id) {
      if(existingClassifiers[id]) {
        existingClassifiers[id].worker.terminate();
        delete existingClassifiers[id];
        return true;
      } else {
        return false;
      }
    };



    /**
     * @ngdoc method
     * @name learningServices.learningService#addClassifier
     * @methodOf learningServices.learningService
     * @kind function
     *
     * @description
     * Creates a new classifier for the provided `data` and assigns it to
     * the given `id`. If the `id` is already taken, it will destroy the
     * existing classifier first.
     *
     * @param {string} id The unique identifier the classifier should be stored under.
     * @param {Array} data The dataset the classifier should work on.
     * @param {Array} columns The columns the classifier should use from the dataset.
     * @param {Function} updateCallback The callback that will be called whenever new classification
     *                                  results are available (batched, first parameter is a hash
     *                                  that maps from the "id" column of the data to the
     *                                  classification result).
     *
     * @returns {Object} The new classifier that can be used to classify elements from the data set.
     */

    learningServ.addClassifier = function(id, data, columns, updateCallback) {
      // Do we have a valid ID?
      if(id === undefined || id === null || id === '') {
         return undefined;
      }

      // Does the classifier already exist? If yes, shut it down first.
      if(learningServ.getClassifier(id)) {
        learningServ.endClassifier(id);
      }

      // Now we can create a new one.
      existingClassifiers[id] = learningServ.newClassifier(data, columns, updateCallback);

      // We will return that one.
      return existingClassifiers[id];
    };


    /**
     * @ngdoc method
     * @name learningServices.learningService#getOrAddClassifier
     * @methodOf learningServices.learningService
     * @kind function
     *
     * @description
     * If a classifier is already up and running for the given `id`,
     * we will return that classifier. Otherwise, we will create a
     * new classifier for the provided `data` and will assign it to
     * the given `id`.
     *
     * @param {string} id The unique identifier the classifier should be stored under.
     * @param {Array} data The dataset the classifier should work on.
     * @param {Array} columns The columns the classifier should use from the dataset.
     * @param {Function} updateCallback The callback that will be called whenever new classification
     *                                  results are available (batched, first parameter is a hash
     *                                  that maps from the "id" column of the data to the
     *                                  classification result).
     *
     * @returns {Object} The (new or existing) classifier that can be used to classify elements from the data set.
     */

    learningServ.getOrAddClassifier = function(id, data, columns, updateCallback) {
      return learningServ.getClassifier(id) || learningServ.addClassifier(id, data, columns, updateCallback);
    };


    return learningServ;
  });

