'use strict';

/**
 * @ngdoc overview
 * @name configServices
 * @description
 *
 * This module provides services to retrieve configuration options stored in the SharePoint lists.
 *
 */

angular.module('configServices', ['gettext', 'restServices'])


  /**
   * @ngdoc service
   * @name configServices.configService
   * @description
   *
   * This service allows to retrieve configuration settings for the application stored in the relevant SharePoint
   * lists.
   *
   */

  .factory('configService', ['restListService', '$q', 'gettext', function(restListService, $q, gettext) {
    var configServ = {};

    var configPromise;


    /**
     * @ngdoc method
     * @name configServices.configService#getConfiguration
     * @methodOf configServices.configService
     * @kind function
     *
     * @description
     * This method returns a promise to an object with all configuration settings stored in the
     * SharePoint list for central app configurations.
     *
     *
     * @returns {Promise} Returns a promise that resolves to a JavaScript object with configuration details.
     *
     */

    configServ.getConfiguration = function() {
      var deferred;

      // Do we already have an active promise? Then we just return this one.
      if(configPromise) {
        return configPromise;
      }

      // Ok, no promise exists yet, so we define a new one.
      deferred = $q.defer();
      configPromise = deferred.promise;

      // Ok, now we can actually get the data from SharePoint
      // We can use getAllItems as the configuration list should not exceed SharePoint's list view threshold (5000 items).
      restListService.getAllItems('configuration').then(
        function(res) {
          // Are we getting the right data?
          if (!res || !res.data || !res.data.d || !res.data.d.results || !Array.isArray(res.data.d.results)) {
            // Reject promise, data returned is invalid.
            deferred.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
          }

          // We will create an object with all configuration elements.
          var config = {};

          res.data.d.results.forEach(function(d) {
            config[d.Title] = d.Value;
          });

          // Ok, return configuration data.
          deferred.resolve(config);
        },
        function(restErr) {
          // An error occurred, reject promise.
          deferred.reject(gettext('An error occurred:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
        }
      );

      // Ok, lets return our promise.
      return configPromise;
    };


    return configServ;
  }]);
