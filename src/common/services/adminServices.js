'use strict';

/**
 * @ngdoc overview
 * @name adminServices
 * @description
 *
 * This module provides various services for the administrative functions of
 * The Dashboard (such as providing access to the initialization data required
 * for setting up The Dashboard the first time).
 */

angular.module('adminServices', ['restServices', 'usersServices'])

  /**
   * @ngdoc service
   * @name adminServices.adminService
   * @description
   *
   * This service provides access to data required by the administration
   * interface, such as the initialization data used by the SharePoint
   * List initialization feature.
   */

  .factory('adminService', ['usersService', '$q', '$http', function(usersService, $q, $http) {
    var adminServ = {};


    /**
     * @ngdoc method
     * @name adminServices.adminService#getListList
     * @methodOf adminServices.adminService
     * @kind function
     *
     * @description
     * Retrieves the JSON specification of all SharePoint lists required to be created
     * for The Dashboard.
     *
     * @returns {Promise} Returns a single promise that will be resolved with an array as
     * retrieved from the sharepointLists.js JSON file provided with The Dashboard.
     */

    adminServ.getListList = function() {
      return $http.get('assets/sharepointLists.js');
    };


    /**
     * @ngdoc method
     * @name adminServices.adminService#getLibraryList
     * @methodOf adminServices.adminService
     * @kind function
     *
     * @description
     * Retrieves the JSON specification of all SharePoint document libraries required to be
     * created for The Dashboard.
     *
     * @returns {Promise} Returns a single promise that will be resolved with an array as
     * retrieved from the sharepointLibraries.js JSON file provided with The Dashboard.
     */

    adminServ.getLibraryList = function() {
      return $http.get('assets/sharepointLibraries.js');
    };



    /**
     * @ngdoc method
     * @name adminServices.adminService#getInitialData
     * @methodOf adminServices.adminService
     * @kind function
     *
     * @description
     * Retrieves the JSON specification for the demo data provided with The Dashboard.
     * This data can be loaded by the "SharePoint List initialization" functionality
     * and loads a new Dashboard instance with dummy data for testing purposes.
     *
     * @returns {Promise} Returns a single promise that will be resolved with an array as
     * retrieved from the initialData.js JSON file provided with The Dashboard.
     */

    adminServ.getInitialData = function() {
      return $http.get('assets/initialData.js');
    };

    return adminServ;
  }]);