'use strict';

/**
 * @ngdoc overview
 * @name miscServices
 * @description
 *
 * This module provides various services for small, singular purposes.
 *
 */

angular.module('miscServices', ['gettext', 'chartServices'])


/**
 * @ngdoc filter
 * @name miscServices#getColumn
 * @description
 *
 * The getColumn filter retrieves a single column as array from an array of objects.
 *
 */

  .filter('getColumn', function() {
    return function(input, col) {
      if(!Array.isArray(input)) {
        // If input is not an array, we return it unmodified.
        return input;
      }

      return input.map(function(d) { return d[col]; });
    };
  })


  /**
   * @ngdoc filter
   * @name miscServices#highlightSearchResults
   * @description
   *
   * The highlightSearchResults filter adds "<mark></mark>" HTML code around all text
   * matching the given `searchText`.
   *
   */

  .filter('highlightSearchResults', function() {
    return function(input, searchText) {
      // We do not highlight if search text is empty or less than three characters long
      if(searchText === undefined || searchText === null || searchText.length < 3) {
        return input;
      }

      var searchTextUpCase = searchText.replace('<', '&lt;').replace('>', '&gt;').toUpperCase();
      var searchTextRegexp = new RegExp('(<[^>]+>|' + searchText.replace('<', '&lt;').replace('>', '&gt;').replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&') + ')', 'i');
      return input.split(searchTextRegexp).map(function(t) { return (t.toUpperCase() === searchTextUpCase ? '<mark>' + t + '</mark>' : t); }).join('');
    };
  })







  /**
   * @ngdoc service
   * @name miscServices.miscServices
   * @description
   *
   * A collection of small helper methods that are used throughout the application.
   *
   */

  .factory('miscServices', ['gettextCatalog', 'd3', function(gettextCatalog, d3) {
    var miscServices = {};


    /**
     * @ngdoc method
     * @name miscServices.miscServices#hashCode
     * @methodOf miscServices.miscServices
     * @kind function
     *
     * @description
     * Takes a (long) string as input and reduces it to a small numeric hash value, i.e. a value which has the property
     * that any change in the input string should lead to a different hash value and if two hash values are equal it is
     * very likely that the original strings that created these hashes where equal as well.
     *
     * Note that this is _not_ a cryptographical hash function and it _must not_ be used for security purposes. Its
     * only purpose is to take a long string (e.g. a serialization of a JavaScript object) and turn it into a short
     * value where a change in the short value indicates that the long string has changed and vice versa.
     *
     * We use it to unburden AngularJS' digest by watching only a hash value instead of a huge object that changes
     * only very infrequently.
     *
     * Code from [http://stackoverflow.com/a/15710692](http://stackoverflow.com/a/15710692).
     *
     * @param {string} s The original string to be turned into a hash value.
     *
     * @returns {number} A short, numeric hash value of the original string.
     */

    // Hash code function
    // from: http://stackoverflow.com/a/15710692
    miscServices.hashCode = function(s){
      return s.split("").reduce(function(a,b) {
          a= ( (a<<5) - a) + b.charCodeAt(0);      // jshint ignore:line
          return a&a;                              // jshint ignore:line
        } ,0);
    };




    /**
     * @ngdoc method
     * @name miscServices.miscServices#parseDate
     * @methodOf miscServices.miscServices
     * @kind function
     *
     * @description
     * Timezone independent date parser. Takes a ISO-8601 compliant datetime string at UTC and turns
     * it into a JavaScript date element, using only the date and not the time values of the original
     * string.
     *
     * Code adapted from [http://stackoverflow.com/a/2587398](http://stackoverflow.com/a/2587398).
     *
     * @param {string} input The date as ISO-8601 date string, e.g. `2016-06-02T00:00:00.000Z`.
     *
     * @returns {Date} A JavaScript Date object where converted only the date part of the `input`
     * string to a Date object at UTC.
     */

    miscServices.parseDate = function(input) {
      if(!angular.isString(input)) { return undefined; }

      var parts = input.split('T')[0].split('-');
      // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
      return new Date(Date.UTC(parts[0], parts[1]-1, parts[2])); // Note: months are 0-based
    };




    /**
     * @ngdoc method
     * @name miscServices.miscServices#parseDatetime
     * @methodOf miscServices.miscServices
     * @kind function
     *
     * @description
     * Timezone independent datetime parser. Takes a ISO-8601 compliant datetime string at UTC and turns
     * it into a JavaScript date element, using date and time values (at UTC).
     *
     * Code adapted from [http://stackoverflow.com/a/2587398](http://stackoverflow.com/a/2587398).
     *
     * @param {string} input The date as ISO-8601 date string, e.g. `2016-06-02T23:00:00.000Z`.
     *
     * @returns {Date} A JavaScript Date object converted at UTC.
     */

    miscServices.parseDatetime = function(input) {
      var parts = input.split(/[TZ:\-]/);
      // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
      return new Date(Date.UTC(parts[0], parts[1]-1, parts[2], parts[3], parts[4], parts[5])); // Note: months are 0-based
    };

    // TODO: make timezone specific




    /**
     * @ngdoc method
     * @name miscServices.miscServices#formatDate
     * @methodOf miscServices.miscServices
     * @kind function
     *
     * @description
     * Uses D3 library (from chartServices) to format a date object according to a format string.
     *
     * @param {Date} source    The Date object that should be formatted.
     * @param {String} format  The format string to be used.
     *
     * @returns {String} A string that contains the `source` date formatted according to the `format` string (using the
     *                   D3 formatting logic).
     */

    miscServices.formatDate = function(source, format) {
      var formatTime = d3.timeFormat(format);
      return formatTime(source);
    };




    /**
     * @ngdoc method
     * @name miscServices.miscServices#translatedAlert
     * @methodOf miscServices.miscServices
     * @kind function
     *
     * @description
     * Displays an alert with a translated alert text using the getString feature of gettext.
     *
     * @param {string} alert The (untranslated) alert to be displayed. You need to wrap it in gettext(...) to make
     *                       it parseable for gettext.
     *
     * @returns {Object} The return value of window.alert.
     */

    miscServices.translatedAlert = function(alert) {
      return window.alert(gettextCatalog.getString(alert));
    };


    /**
     * @ngdoc method
     * @name miscServices.miscServices#translatedConfirm
     * @methodOf miscServices.miscServices
     * @kind function
     *
     * @description
     * Displays a confirm message with a translated alert text using the getString feature of gettext.
     *
     * @param {string} alert The (untranslated) alert to be displayed. You need to wrap it in gettext(...) to make
     *                       it parseable for gettext.
     *
     * @returns {Object} The return value of window.confirm.
     */

    miscServices.translatedConfirm = function(alert) {
      return window.confirm(gettextCatalog.getString(alert));
    };





    /**
     * @ngdoc method
     * @name miscServices.miscServices#repeatString
     * @methodOf miscServices.miscServices
     * @kind function
     *
     * @description
     * Repeats a given string `pattern` the given `count` times and returns this (repeated) string.
     *
     * Code adapted from http://stackoverflow.com/questions/202605/repeat-string-javascript#202627.
     *
     * @param {string} pattern The pattern to be repeated `count` times.
     *
     * @param {number} count The number of times the `pattern` should be repeated.
     *
     * @returns {string} A string containing the `pattern` `count`-times concatenated to itself.
     */

    // Repeats a pattern count times
    // From http://stackoverflow.com/questions/202605/repeat-string-javascript#202627
    miscServices.repeatString = function(pattern, count) {
      if (count < 1) { return ''; }
      var result = '';
      while (count > 1) {
        if (count & 1) result += pattern;    // jshint ignore:line
        count >>= 1, pattern += pattern;     // jshint ignore:line
      }
      return result + pattern;
    };




    /**
     * @ngdoc method
     * @name miscServices.miscServices#isInt
     * @methodOf miscServices.miscServices
     * @kind function
     *
     * @description
     * Returns `true` if the given `value` is an integer or a string that represents an integer.
     *
     * Code from https://stackoverflow.com/a/14794066.
     *
     * @param {integer} value The variable to be checked for being an integer.
     *
     * @returns {boolean} `true` if `value` represents an integer, `false` otherwise.
     */
    miscServices.isInt = function(value) {
      if (isNaN(value)) {
        return false;
      }
      var x = parseFloat(value);
      return (x | 0) === x;       // jshint ignore:line
    };



    /**
     * @ngdoc method
     * @name miscServices.miscServices#intersect
     * @methodOf miscServices.miscServices
     * @kind function
     *
     * @description
     * Intersects two arrays, returning a new array.
     *
     * Code from https://stackoverflow.com/a/16227294.
     *
     * @param {Array} a The first array to be intersected with the second.
     * @param {Array} b The second array to be intersected with the first.
     *
     * @returns {Array} An array containing all array elements that exist in both `a` and `b`.
     */

    miscServices.intersect = function(a, b) {
      var t;
      if (b.length > a.length) {  // indexOf to loop over shorter
        t = b;
        b = a;
        a = t;
      }
      return a.filter(function (e) {
        return b.indexOf(e) > -1;
      });
    };



    /**
     * @ngdoc method
     * @name miscServices.miscServices#htmlToText
     * @methodOf miscServices.miscServices
     * @kind function
     *
     * @description
     * Helper function to strip HTML from a string.
     *
     * Code adapted from https://stackoverflow.com/a/47140708.
     *
     * @param {String} html The string from which HTML code should be stripped.
     *
     * @returns {String} The text with HTML code removed.
     */

    miscServices.htmlToText = function(html) {
      var doc = new DOMParser().parseFromString(html, 'text/html');
      return (doc.body && doc.body.textContent ? doc.body.textContent : '');
    };



    /**
     * @ngdoc method
     * @name miscServices.miscServices#escapeForRegexp
     * @methodOf miscServices.miscServices
     * @kind function
     *
     * @description
     * Helper function to escape all characters in a string such that it can be used as the input for a regular
     * expression.
     *
     * Code from https://stackoverflow.com/a/3561711.
     *
     * @param {String} html The string from which HTML code should be stripped.
     *
     * @returns {String} The text with HTML code removed.
     */

    miscServices.escapeForRegexp = function(s) {
      return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    };


    return miscServices;
  }]);



