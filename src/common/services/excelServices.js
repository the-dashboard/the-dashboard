/* jshint strict:false */
/* globals XlsxPopulate */

/**
 * @ngdoc overview
 * @name fileServices
 * @description
 *
 * This module provides functionality to export and import data to/from Excel XLSX files. It uses the
 * xlsx-populate (https://www.npmjs.com/package/xlsx-populate#browser-usage) library for this.
 */

angular.module('excelServices', [ 'fileServices', 'usersServices' ])

/**
 * @ngdoc service
 * @name excelServices.excelService
 * @description
 *
 * This service provides features to import or export data to/from Excel XLSX files. It uses the
 * xlsx-populate library for this.
 *
 */

  .factory('excelService', ['$q', '$http', '$location', 'FileSaver', 'usersService', function($q, $http, $location, FileSaver, usersService) {
    var excelServ = {};

    // Use AngularJS's $q for Promises in xlsx-populate
    XlsxPopulate.Promise = $q;


    /**
     * @ngdoc method
     * @name excelServices.excelService#exportData
     * @methodOf excelServices.excelService
     * @kind function
     *
     * @description
     * This method prepares an Excel XLSX file with the data provided. It uses a template file in the
     * `assets/exportTemplate.xlsx` location, which you can modify to suit your organisation's needs. It can write
     * multiple sheets, which you can provide as `sheets` array. Every sheet object must contain the following
     * information:
     *
     * ```
     *   {
     *     name: <sheet name to be used>,
     *     title: <title of the sheet to be displayed in the template>,
     *     color: <optional color of the tab in Excel, given as RGB string "#aabbcc">,
     *     columns: <column definitions in datafileReader format>,
     *     rows: <array of rows with columns as object elements>
     *   }
     * ```
     *
     * Note that the meanings of the column definitions are reversed to the `datafileReader` here, as we are exporting
     * and not reading. Meaning the `display` property specifies the Dashboard-internal representation while the
     * `source` property will be written to the Excel file.
     *
     *
     * @param {string} title          The title of this export. Will be written to the appropriate cell in the
     *                                export template.
     * @param {string} description    A longer description of the export. Will be written to the appropriate cell in
     *                                the export template.
     * @param {Array}  sheets         An array of worksheets to be written to the file (see above for format).
     *
     * @returns {Object} A Promise that resolves with a blob which can be provided to the user for download.
     */

    excelServ.exportData = function(title, description, sheets) {
      // TODO: Remove debug code
      console.log('excelService.exportData has been called:');
      console.log([title, description, sheets]);

      // We need some base data for our export:
      // We obtain the name of the current user, as that is written into the export
      var currUser;
      return usersService
        .getCurrentUser()
        .then(function(_currUser) {
          currUser = _currUser;

          // Now we load the export template
          return $http.get('assets/exportTemplate.xlsx', { responseType: 'arraybuffer' });
        })
        .then(function(res) {
          // Open the worksheet for processing
          return XlsxPopulate.fromDataAsync(res.data);
        })
        .then(function(workbook) {
          // Set the pre-defined fields for title, description, user details, current date/time etc.
          var setIfExists = function(definedName, value) {
            if(workbook.definedName(definedName)) {
              workbook.definedName(definedName).value(value);
            }
          };

          setIfExists('title', title);
          setIfExists('description', description);
          setIfExists('exporter', currUser.name);
          setIfExists('exporter_email', currUser.email);
          setIfExists('exporter_login', currUser.login);
          setIfExists('exported_at', new Date());

          if(workbook.definedName('source_url')) {
            workbook.definedName('source_url').value($location.absUrl());
            workbook.definedName('source_url').hyperlink($location.absUrl());
          }

          // Add the sheets
          sheets.forEach(function(sheetDef, idx) {
            // Populate the template
            if(workbook.definedName('sheetNames')) {
              workbook.definedName('sheetNames').cell(idx, 0).value(sheetDef.name);
            }

            if(workbook.definedName('sheetTitles')) {
              workbook.definedName('sheetTitles').cell(idx, 0).value(sheetDef.title);
            }

            // Add the sheet
            var sheet = workbook.addSheet(sheetDef.name);

            // Do we need to set the tab color?
            if(sheetDef.color) {
              sheet.tabColor(sheetDef.color);
            }

            // Write the header line with the column display names and format date columns correctly
            sheetDef.columns.forEach(function(col, idx) {
              sheet.cell(1, idx+1).value(col.source);

              if(col.format === 'date') {
                sheet.column(idx+1).style('numberFormat', 'yyyy-mm-dd');
              } else if(col.format === 'time') {
                sheet.column(idx+1).style('numberFormat', 'yyyy-mm-dd hh:mm:ss');
              } else if(col.format === 'percentage') {
                sheet.column(idx+1).style('numberFormat', '0.0%');
              }
            });

            // Format the header line as bold, rotate text by 45°, set header row height based on column name size
            sheet.row(1).style({ 'bold': true, 'rotateTextUp': true, 'horizontalAlignment': 'center' });

            var headerHeight = sheetDef.columns.reduce(function(accumul, col) {
              return Math.max(accumul, Math.min(col.source.length*6, 200));
            }, 40);
            sheet.row(1).height(headerHeight);

            // TODO: Remove debug code
            console.log('Header height set to: ' + headerHeight);


            // Write the content to the following lines
            sheet.cell(2, 1).value(
              sheetDef.rows.map(function(row) {
		// xlsx-populate creates invalid spreadsheets if a column value is set to "NaN". We thus need to check for
		// NaN values and replace them with the empty string. As IE11 does not support Number.isNaN, we need to use
		// the workaround that NaN is the only JavaScript value not equal to itself.
                return sheetDef.columns.map(function(col) { return (row[col.display] !== row[col.display] ? '' : row[col.display]); });
              })
            );

            // Determine the column width based on heuristic
            sheetDef.columns.forEach(function(col, idx) {
              // Default value for numbers
              var colWidth = 15;

              // For text, we need to calculate the width based on the largest text in there
              if(col.format === 'text') {
                colWidth = sheetDef.rows.reduce(
                  function(accumul, row) {
                    return Math.max(accumul, Math.min((angular.isString(row[col.display]) ? row[col.display].length*1.1 : 0), 60));
                  },
                  colWidth
                );
              }

              // TODO: Remove debug code
              console.log('Setting width of column "' + col.display + '" to ' + colWidth + '.');

              // Set column width
              sheet.column(idx+1).width(colWidth);
            });

            // Enable the autoFilter
            sheet.usedRange().autoFilter();

          });


          // TODO: patch xlsx-populate to support autoFilter, add autoFilter


          // Return the workbook as blob
          return workbook.outputAsync();
        });


    };



    /**
     * @ngdoc method
     * @name excelServices.excelService#exportDataAsDownload
     * @methodOf excelServices.excelService
     * @kind function
     *
     * @description
     * This method prepares an Excel export using {@link excelServices.excelService#exportData `exportData`} and then
     * directly presents it to the user for downloading. It uses the same parameters as
     * {@link excelServices.excelService#exportData `exportData`}.
     *
     * @param {string} title          The title of this export. Will be written to the appropriate cell in the
     *                                export template and used to create the filename of the export if none is
     *                                explicitly given.
     * @param {string} description    A longer description of the export. Will be written to the appropriate cell in
     *                                the export template.
     * @param {Array}  sheets         An array of worksheets to be written to the file (see
     *                                {@link excelServices.excelService#exportData `exportData`} for format).
     * @param {string=} filename      An optional string that will be used as filename for the export. if not given,
     *                                the filename will be created from the current date and the `title` of the export.
     *
     * @returns {Object} A Promise that resolves when the file has been provided to the user.
     */

    excelServ.exportDataAsDownload = function(title, description, sheets, filename) {
      if(filename === undefined || filename === null || filename === '') {
        var now = new Date();
        filename = now.getFullYear() + '-' + (now.getMonth() < 9 ? '0' : '') + (now.getMonth()+1) + '-' +
          (now.getDate() < 10 ? '0' : '') + now.getDate() + '_' +
          title.trim().replace(/[^0-9a-zA-Z\-]/g, '_') + '.xlsx';
       }

       return excelServ
         .exportData(title, description, sheets)
         .then(function(blob) {
           FileSaver.saveAs(blob, filename);
         });
    };






    /**
     * @ngdoc method
     * @name excelServices.excelService#loadData
     * @methodOf excelServices.excelService
     * @kind function
     *
     * @description
     * This method loads data from the given `sheets` in the given Excel XLSX `file` into a row structure that is
     * suitable for further processing.
     *
     *
     * @param {Object} file       A file as returned by the HTML5 filepicker.
     *
     * @param {Array}  sheets     An array of sheet names which should be extracted from the Excel XLSX file.
     *
     * @returns {Object}  A Promise that resolves to an array with one entry per sheet, for each sheet an array with
     *                    all rows (each row is an object with keys per column) will be returned.
     */

    excelServ.loadData = function(file, sheets) {
       // TODO: Remove debug code
      console.log('excelService.loadData called with:');
      console.log([file, sheets]);

       return XlsxPopulate
         .fromDataAsync(file)
         .then(function(workbook) {
           // TODO: Remove debug code
           console.log('Received Excel workbook.');

           return sheets.map(function(sheetName) {
             // We iterate over all given sheets and prepare a row array for each
             var rowArray = [];

             // What is the used area of that sheet?
             var usedRange = workbook.sheet(sheetName).usedRange();
             var usedColumns = usedRange.endCell().columnNumber() - usedRange.startCell().columnNumber() + 1;
             var usedRows = usedRange.endCell().rowNumber() - usedRange.startCell().rowNumber() + 1;

             // Get column names from first row
             var i;
             var j;
             var columnNames = [];
             for(j = 0;j < usedColumns;j++) {
               columnNames[j] = usedRange.cell(0, j).value();
             }

             // Iterate over rows
             for(i = 1;i < usedRows;i++) {
               var rowObj = {};
               for(j = 0;j < usedColumns;j++) {
                 if (columnNames[j] !== '') {
                   // We skip columns without name and empty values
                   var val = usedRange.cell(i, j).value();

                   if(val !== undefined && val !== null && val !== '') {
                     // We skip empty data
                     rowObj[columnNames[j]] = val;
                   }
                 }
               }

               // We only add non-empty objects
               if(Object.keys(rowObj).length > 0) {
                 rowArray.push(rowObj);
               }
             }

             return rowArray;
           });
         });
    };


    return excelServ;
  }]);

