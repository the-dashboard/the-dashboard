'use strict';

/**
 * @ngdoc overview
 * @name risksServices
 * @description
 *
 * This module provides services to add, edit and remove risks from the "Dashboard - Risks" SharePoint table, which are
 * used by the {@link riskTrackerDirectives.riskTracker `riskTracker`} directive.
 *
 */

angular.module('risksServices', ['gettext', 'restServices', 'miscServices', 'usersServices', 'socialServices'])


  /**
   * @ngdoc filter
   * @name risksServices#riskLabel
   * @description
   *
   * The risk label filter transforms an `input`value from 0-100 into a textual label by splitting the interval [0, 100]
   * into equal-sized slices based on the number of entries of the provided `labels` and providing the matching label.
   *
   */

  .filter('riskLabel', function() {
    return function(input, labels) {
      var i = Math.min(labels.length-1, Math.max(0, Math.floor(input / 100 * labels.length)));
      return labels[i];
    };
  })



  /**
   * @ngdoc service
   * @name risksServices.risksService
   * @description
   *
   * This service allows to add, edit and remove risk entries which are stored in the "Dashboard - Risks" SharePoint
   * table and used by the {@link riskTrackerDirectives.riskTracker `riskTracker`} directive.
   *
   * A risk element is characterized by a category, title, description and an x-axis and y-axis rating (usually impact
   * and likelihood). Risks can be `frozen` to provide a historic snapshot of how the risk situation looked like at a
   * specific point in time.
   *
   */

  .service('risksService', ['$q', 'gettext', 'restListService', 'usersService', 'subscriptionService', 'miscServices', function($q, gettext, restListService, usersService, subscriptionService, miscServices) {
    var risksServ = {};

    // Cache for already retrieved risks
    var risksPromises = {};
    var risksMinDate = {};




    /**
     * @ngdoc method
     * @name risksServices.risksService#getRisksByCategory
     * @methodOf risksServices.risksService
     * @kind function
     *
     * @description
     * This method retrieves a list of all risks in a given `riskCategory`. The method retrieves all risks, also those
     * which are no longer active but have been frozen in the past. Thus you can (but do not have to) specify a
     * `minDate` which will limit the items to be retrieved from the server to risk entries still active or frozen
     * at or after the `minDate`.
     *
     * The method returns an array of historic risk items, which are JavaScript objects of the following form:
     *
     * ```
     * [
     *  {
     *   id: <Numeric ID of the SharePoint list item>,
     *   initialId: <ID of the first list item for the given risk (see below)>,
     *   title: <Title of the risk>,
     *   description: <Description of the risk>,
     *   category: <The category string of the risk>,
     *   riskStatus: <The status of the risk, can be Final, Draft, Reviewed>,
     *   valid_from: <The datetime from which this list item was the valid representation of that risk>,
     *   valid_to: <The datetime until which this liste item was the valid representation of that risk>,
     *   xAxis: <The x-axis value of the risk from 0-100, e.g. the value for Impact>,
     *   yAxis: <The y-axis value of the risk from 0-100, e.g. the value for Likelihood>,
     *   lastAuthorId: <The SharePoint user ID of the last editor of this list item>,
     *   lastAuthor: <The name of the last editor of this liste item>,
     *   lastModifiedDate: <The date at which this item was last edited>
     *  }
     *  ,
     *  ...
     * ]
     * ```
     *
     * Note that a single risk can appear multiple times in this array as each "freeze" will generate a new entry
     * for the same risk. For this there exists the `initialId` value - all historic snapshots of a given risk will
     * have the same `initialId` value, which is the ID value of the first list item for the given risk. The
     * `valid_from` and `valid_to` values specify which item in this array was the valid representation of the given
     * risk at a specific point in time.
     *
     *
     * @param {string} riskCategory The name of the category for which risks should be retrieved.
     *
     * @param {Date=} minDate The minimum `valid_to` date of the risk, i.e. only show risks which existed at or after
     *                       the given `minDate`. Defaults to showing all risks.
     *
     * @returns {Promise} A promise which resolves to an array of risk items.
     *
     */

    risksServ.getRisksByCategory = function(riskCategory, minDate) {
      var deferred;

      // Is minDate set?
      if(!(minDate instanceof Date)) {
        minDate = new Date(1970, 1, 1, 0, 0, 0, 0);
      }

      // Does a promise already exist and is this promises minDate at least as small as ours?
      if(risksPromises[riskCategory] && risksMinDate[riskCategory] <= minDate) {
        // If yes, we can simply return it (filtering for the minDate where necessary).
        if (risksMinDate[riskCategory] < minDate) {
          return risksPromises[riskCategory]
            .then(function(risks) {
              return risks.filter(function(risk) { return (risk.valid_to >= minDate); });
            });
        } else {
          return risksPromises[riskCategory];
        }
      }

      // Otherwise we need to create a new promise and get new data
      deferred = $q.defer();
      risksPromises[riskCategory] = deferred.promise;
      risksMinDate[riskCategory] = minDate;

      // Retrieve risks from server
      restListService.getMergedFilteredItems('risks', "Category eq '" + riskCategory.replace("'", "''") + "' and Valid_to gt datetime'" + minDate.toISOString() + "'", undefined, undefined, 'initialID, Valid_to desc').then(
        function(res) {
          // Success - got data from the server

          // TODO: Remove debug code
          console.log('Success in getting risks for category ' + riskCategory + '!');
          console.log(res);

          // Are we getting the right data?
          if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
            // Returns already rejected promise
            deferred.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
          }

          var retrievedRisks = res.data.d.results.map(
            function(d) {
              return {
                id: d.Id,
                initialId: d.initialID,
                title: d.Title,
                description: d.Description,
                category: d.Category,
                riskStatus: d.riskStatus,
                valid_from: miscServices.parseDatetime(d.Valid_from),
                valid_to: miscServices.parseDatetime(d.Valid_to),
                xAxis: d.xAxis,
                yAxis: d.yAxis,
                lastAuthorId: d.EditorId,
                lastAuthor: '',
                lastModifiedDate: Date.parse(d.Modified)
              };
            }
          );


          // Sort this again as results returned by getMergedFilteredItems are not guaranteed to be properly
          // sorted.
          retrievedRisks.sort(function(a, b){
            return (a.initialId === b.initialId ? (b.Valid_to - a.Valid_to) : (a.initialId - b.initialId));
          });


          // Obtain editors
          var retrieveEditors = retrievedRisks.forEach(function(d) {
            return usersService.getUserById(d.lastAuthorId).then(
              function(res) {
                d.lastAuthor = res.name;
              }
            );
          });

          // Resolve when all editors have been obtained
          $q.all(retrieveEditors).then(
            function() {
              deferred.resolve(retrievedRisks);
            },
            function(err) {
              deferred.reject(err);
            }
          );
        },
        function(restErr) {
          deferred.reject(gettext('An error occurred:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
        }
      );

      // Create promise for risks by category
      return risksPromises[riskCategory];
    };




    /**
     * @ngdoc method
     * @name risksServices.risksService#isValidRisk
     * @methodOf risksServices.risksService
     * @kind function
     *
     * @description
     * This method validates a risk object for validity, checking if all mandatory fields have been filled and if the
     * field values are valid. New risks will not yet have valid ID fields, thus if `newRisk` is passed as `true` it
     * will skip checking for completed ID fields.
     *
     *
     * @param {Object} risk The JavaScript object which should be validated as valid risk item.
     *
     * @param {boolean} newRisk Should be set to `true` if and only if the risk is a new risk which is yet to be added
     *                          to the SharePoint list.
     *
     * @returns {boolean} Returns `true` if all mandatory fields of the risk are correctly filled in, `false` otherwise.
     *
     */

    risksServ.isValidRisk = function(risk, newRisk) {
      return (risk.category && risk.title !== '' && risk.description !== '' && risk.riskStatus !== '' &&
      (risk.valid_from instanceof Date) && (risk.valid_to instanceof Date) &&
      !isNaN(risk.xAxis) && risk.xAxis >= 0 && risk.xAxis <= 100 &&
      !isNaN(risk.yAxis) && risk.yAxis >= 0 && risk.yAxis <= 100 &&
      (newRisk || (!isNaN(risk.id) && risk.id >= 0 && !isNaN(risk.initialId) && risk.initialId >= 0)));
    };


    /**
     * @ngdoc method
     * @name risksServices.risksService#asRestRisk
     * @methodOf risksServices.risksService
     * @kind function
     *
     * @description
     * This method converts The Dashboard's internal representation object of a risk item into a risk item object
     * which is formatted such that it can be transferred to the SharePoint REST API as list item.
     *
     * Depending on whether the risk is a new item which is to be added to the list or is an existing item that should
     * be modified, internal ID values might be required or not. This is why the `newRisk` parameter needs to be set
     * to `true` if and only if the risk is a new risk.
     *
     *
     * @param {Object} risk The JavaScript object which should be used as basis for the SharePoint list item.
     *
     * @param {boolean} newRisk Should be set to `true` if and only if the risk is a new risk which is yet to be added
     *                          to the SharePoint list.
     *
     * @returns {Object} Returns a JavaScript object which is a properly formatted SharePoint list item (without
     *                   metadata) to be added to the "SharePoint - Risks" list.
     *
     */

    risksServ.asRestRisk = function(risk, newRisk) {
      var restRisk = {
        'Title': risk.title,
        'Description': risk.description,
        'Category': risk.category,
        'riskStatus': risk.riskStatus,
        'Valid_from': risk.valid_from,
        'Valid_to': risk.valid_to,
        'xAxis': risk.xAxis,
        'yAxis': risk.yAxis
      };

      if(!newRisk) {
        restRisk.Id = risk.id;
        restRisk.initialID = risk.initialId;
      }

      return restRisk;
    };






    /**
     * @ngdoc method
     * @name risksServices.risksService#updateRisk
     * @methodOf risksServices.risksService
     * @kind function
     *
     * @description
     * This method updates an existing SharePoint list item for a given `risk`, which needs to be passed as JavaScript
     * object representing the risk item.
     *
     * Note that this function does not create a new representation in time of the risk item, but merely updates (i.e.
     * overwrites) the currently valid, active list item for the given risk. A new representations in time are only
     * created by {@link risksServices.risksService#freezeRisk freezing} risks.
     *
     *
     * @param {Object}    risk          The JavaScript object representing the risk item which should be updated (in
     *                                  The Dashboard's internal format, `updateRisk` will call
     *                                  {@link risksServices.risksService#asRestRisk `asRestRisk`}.
     * @param {boolean=}  triggerSubs   If set to `true`, the update will trigger subscriptions to this risk item and
     *                                  to this risk category.
     *
     * @returns {Promise} Returns a promise which resolves to the updated risk item (with updated `lastAuthor` and
     *                    `lastModifiedDate`).
     *
     */

    risksServ.updateRisk = function(risk, triggerSubs) {
      var deferred = $q.defer();

      if(!risksServ.isValidRisk(risk, false)) {
        deferred.reject(gettext('The risk you are trying to update is not valid. Please complete title, description and select risk ratings.'));
        return deferred.promise;
      }

      // Send to server
      restListService.updateItem('risks', risk.id, risksServ.asRestRisk(risk))
        .then(
          // Get author and update element
          function() {
            // Set last modified date
            risk.lastModifiedDate = new Date();

            usersService.getCurrentUser().then(
              function(me) {
                // Set last author
                risk.lastAuthorId = me.Id;
                risk.lastAuthor = me.name;
                
                // Reset the promise so that changes are included if we reload the risk riskTracker                
                risksPromises[risk.category] = undefined;
                risksMinDate[risk.category] = undefined;

                // Trigger subscriptions
                if(triggerSubs) {
                  subscriptionService
                    // Individual risk
                    .triggerSubs(
                      'risk',
                      risk.initialId,
                      'risk',
                      risk.initialId,
                      undefined,
                      (risk.sourceLink ? '#' + risk.sourceLink + '%2F' + risk.initialId : undefined),
                      {
                        from: risk.lastAuthorId,
                        risk: risk
                      },
                      'changed-risk'
                    )
                    .then(
                      function() {
                        // Risk category
                        return subscriptionService.triggerSubs(
                          'riskCategory',
                          risk.category,
                          'risk',
                          risk.initialId,
                          undefined,
                          (risk.sourceLink ? '#' + risk.sourceLink + '%2F' + risk.initialId : undefined),
                          {
                            from: risk.lastAuthorId,
                            risk: risk
                          },
                          'changed-risk'
                        );
                      }
                    )
                    .then(
                      function() {
                        deferred.resolve(risk);
                      },
                      function(err) {
                        console.log('An ERROR occured while triggering subscriptions to the saved risk:');
                        console.log(err);
                        console.log(risk);

                        deferred.reject(gettext('An error occured while triggering subscriptions to the saved risk. Please try again later.'));
                      }
                    );

                } else {
                  // Resolve directly
                  deferred.resolve(risk);
                }

              }
            );
          },
          function(restErr) {
            deferred.reject(gettext('An error occurred when saving the note to the server:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
          }
        );

      return deferred.promise;
    };



    /**
     * @ngdoc method
     * @name risksServices.risksService#addRisk
     * @methodOf risksServices.risksService
     * @kind function
     *
     * @description
     * This method creates a new SharePoint list item for a given `risk`, which needs to be passed as JavaScript
     * object representing the risk item.
     *
     * Note that this method is used to create a genuinely new risk, with a new `initialID`. If you want to merely
     * create a new (frozen) representation in time for an existing risk, you need to use
     * {@link risksServices.risksService#freezeRisk `freezeRisk`}.
     *
     *
     * @param {Object} risk The JavaScript object representing the risk item which should be added (in The Dashboard's
     *                      internal format, `addRisk` will call
     *                      {@link risksServices.risksService#asRestRisk `asRestRisk`}.
     *
     * @returns {Promise} Returns a promise which resolves to the created risk item (with updated `id`, `initialID`,
     *                    `lastAuthor` and `lastModifiedDate`).
     *
     */

    risksServ.addRisk = function(risk) {
      var deferred = $q.defer();

      if(!risksServ.isValidRisk(risk, true)) {
        deferred.reject(gettext('The risk you are trying to add is not valid. Please complete title, description and select risk ratings.'));
        return deferred.promise;
      }

      // Send to server
      restListService.addItem('risks', risksServ.asRestRisk(risk))
        .then(
          // Get ID and then update risk with new initialID
          function(res) {
            // TODO: remove debug code
            console.log('Added risk to SharePoint list:');
            console.log(res);

            risk.id = res.data.d.Id;
            risk.initialId = res.data.d.Id;

            risksServ.updateRisk(risk, false).then(
              function(res) {
                // Note that updateRisk already flushes the cache, so we do not need to do that
                // here again.

                // We do need to trigger subscriptions, however, as updateRisk would send the wrong
                // action type
                subscriptionService
                  .triggerSubs(
                    'riskCategory',
                    risk.category,
                    'risk',
                    risk.initialId,
                    undefined,
                    (risk.sourceLink ? '#' + risk.sourceLink + '%2F' + risk.initialId : undefined),
                    {
                      from: res.EditorId,
                      risk: risk
                    },
                    'new-risk'
                  )
                  .then(
                    function() {
                      deferred.resolve(res);
                    },
                    function(err) {
                      console.log('An ERROR occured while trying to triggerSubs for the new risk:');
                      console.log(err);
                      console.log(risk);

                      deferred.reject(gettext('An error occured while trying to trigger the subscriptions for the new risk.'));
                    }
                  );

              },
              function(err) {
                deferred.reject(err);
              }
            );
          },
          function(restErr) {
            deferred.reject(gettext('An error occurred when saving the note to the server:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
          }
        );

      return deferred.promise;
    };



    /**
     * @ngdoc method
     * @name risksServices.risksService#deleteRisk
     * @methodOf risksServices.risksService
     * @kind function
     *
     * @description
     * This method deletes a SharePoint list item for a given risk.
     *
     * Note that this method actually deletes the given list item for the given risk. It does not just change the
     * validity date of the risk, but removes it. If you want to keep the list with a past validity, you first need to
     * {@link risksServices.risksService#freezeRisk freeze} the risk and afterwards delete the newly current
     * representation.
     *
     *
     * @param {Object} risk The JavaScript object representing the risk item which should be deleted.
     *
     * @returns {Promise} Returns a promise which resolves to the SharePoint REST API response.
     *
     */

    risksServ.deleteRisk = function(risk) {
      return restListService.deleteItem('risks', risk.id).then(
        function(res) {          
          // Reset the promise so that changes are included if we reload the risk riskTracker                
          risksPromises[risk.category] = undefined;
          risksMinDate[risk.category] = undefined;
                
          // Set the risk to deleted.
          risk.id = -1*risk.id;

          // Triggers any subscriptions to this risk or its risk category
          return usersService.getCurrentUser()
            .then(
              function(user) {
                return subscriptionService
                // Individual risk
                  .triggerSubs(
                    'risk',
                    risk.initialId,
                    'risk',
                    risk.initialId,
                    undefined,
                    (risk.sourceLink ? '#' + risk.sourceLink : undefined),
                    {
                      from: user.id,
                      risk: risk
                    },
                    'deleted-risk'
                  )
                  .then(
                    function() {
                      // Risk category
                      return subscriptionService.triggerSubs(
                        'riskCategory',
                        risk.category,
                        'risk',
                        risk.initialId,
                        undefined,
                        (risk.sourceLink ? '#' + risk.sourceLink : undefined),
                        {
                          from: user.id,
                          risk: risk
                        },
                        'deleted-risk'
                      );
                    }
                  )
                  .then(
                    function() {
                      return res;
                    }
                  );
              }
            );

        },
        function(restErr) {
          return gettext('An error occurred deleting the note:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText;
        }
      );
    };


    /**
     * @ngdoc method
     * @name risksServices.risksService#freezeRisk
     * @methodOf risksServices.risksService
     * @kind function
     *
     * @description
     * This method "freezes" a given `risk` in time.
     *
     * What this means is that the existing list item for the given risk will be updated with the `freezeDatetime` as
     * new value for `valid_to` and that a new list item will be created with content equal to the existing risk item,
     * valid_from starting at `freezeDatetime` and `initialID` the same as the old list item.
     *
     * The idea is that this allows to go back in time and to ask how the given risk looked like from the last freeze
     * date (or the creation date of the risk) up to the current freeze. The newly frozen list item will not be
     * modified anymore, only the new, active representation can be modified by the user.
     *
     * This method will usually be called on all active risks in a certain category. This is the reason why a
     * `freezeDatetime` can be passed instead of simply using the current date and time. If you freeze multiple risks,
     * you want all risks to be frozen at the same point in time, to make identifying a specific "freeze" easier.
     *
     * The method returns a promise to the newly created list item, which is the newly active representation of that
     * risk.
     *
     *
     * @param {Object} risk The JavaScript object representing the risk item which should be frozen.
     *
     * @param {Date} freezeDatetime The point in time which should be used as the `valid_to` date of the past
     *                              list item and as `valid_from` of the newly created list item.
     *
     * @returns {Promise} Returns a promise which resolves to the new list item oft the newly current representation
     *                    of the given risk.
     *
     */

    risksServ.freezeRisk = function(risk, freezeDatetime) {
      var deferred = $q.defer();

      if(!risksServ.isValidRisk(risk, false)) {
        deferred.reject(gettext('The risk you are trying to freeze is not valid. Please complete title, description and select risk ratings.'));
        return deferred.promise;
      }

      var updateStep;
      // Update risk with a new valid_to
      risk.valid_to = freezeDatetime;
      updateStep = risksServ.updateRisk(risk);

      // Ok, we have updated the old one, so now we can add the new one
      updateStep.then(
        function() {
          var newRisk = {
            id: -1*risk.id,
            initialId: risk.initialId,
            title: risk.title,
            description: risk.description,
            category: risk.category,
            riskStatus: risk.riskStatus,
            valid_from: freezeDatetime,
            valid_to: new Date(2999, 12, 31, 12, 0, 0),
            xAxis: risk.xAxis,
            yAxis: risk.yAxis
          };

          // We do not use the addRisk function as this resets the
          // initialId, which we do not need here.
          return restListService.addItem('risks', risksServ.asRestRisk(newRisk)).then(
            // Get ID and then update risk with new initialID
            function(res) {
              // TODO: remove debug code
              console.log('Added copy of frozen risk to SharePoint list:');
              console.log(res);

              newRisk.id = res.data.d.Id;
              return newRisk;
            },
            function(restErr) {
              deferred.reject(gettext('An error occurred when saving the note to the server:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
            }
          );
        }
      ).then(
        function(res) {    
          // Reset the promise so that changes are included if we reload the risk riskTracker                
          risksPromises[risk.category] = undefined;
          risksMinDate[risk.category] = undefined;
                
          // Done.
          deferred.resolve(res);
        },
        function(err) {
          deferred.reject(err);
        }
      );

      return deferred.promise;
    };

    return risksServ;
  }])



  /**
   * @ngdoc service
   * @name risksServices.impactsService
   * @description
   *
   * This service allows to retrieve, add, edit and remove impact flags which are stored in the "Dashboard - Impact
   * Flags" SharePoint table. Impact flags map items in the Dashboard (particular notes) with an impact type (e.g.
   * increasing or decreasing risk or "red flags") and optionally with a risk defined in a
   * {@link riskTrackerDirectives.riskTracker `riskTracker`} directive.
   *
   * An impact flag is characterized by an impact type (increasing, decreasing, "red flag"), a source element, a date at
   * which it is valid and a target risk category and item (optional).
   *
   */

  .service('impactsService', ['$q', 'gettext', 'restListService', 'interactionService', 'subscriptionService', 'miscServices', function($q, gettext, restListService, interactionService, subscriptionService, miscServices) {
    var impactServ = {};

    // Cache for already retrieved impact flags
    var impactsPromises = {};


    // Helper function to turn a Dashboard-internal impact object into a REST object that can be pushed to SharePoint.
    var asRestImpact = function(impact, newImpact) {
      var restImpact = {
        'Title': impact.impactType,
        'sourceEndpoint': impact.sourceEndpoint,
        'sourceId': impact.sourceId,
        'sourceKeywords': impact.sourceKeywords,
        'sourceUrl': impact.sourceUrl,
        'sourceDescription': impact.sourceDescription,
        'targetRiskCategory': (impact.targetRiskCategory !== undefined ? impact.targetRiskCategory : 'NONE'),
        'targetRiskInitialId': (impact.targetRiskInitialId === undefined ? -1 : impact.targetRiskInitialId)
      };

      if(!newImpact || impact.impactDate) {
        // If no impactDate is specified in a new impact object, this is fine -> the default will set it to today
        restImpact.impactDate = impact.impactDate;
      }

      if(!newImpact) {
        restImpact.Id = impact.id;
      }

      return restImpact;
    };


    // Helper function to turn a REST object as returned by SharePoint into a Dashboard-internal impact object.
    var fromRestImpact = function(restImpact) {
      return {
        'id': restImpact.Id,
        'impactType': restImpact.Title,
        'sourceEndpoint': restImpact.sourceEndpoint,
        'sourceId': restImpact.sourceId,
        'sourceKeywords': restImpact.sourceKeywords,
        'sourceUrl': restImpact.sourceUrl,
        'sourceDescription': restImpact.sourceDescription,
        'targetRiskCategory': restImpact.targetRiskCategory,
        'targetRiskInitialId': restImpact.targetRiskInitialId,
        'impactDate': miscServices.parseDatetime(restImpact.impactDate),
        'lastAuthorId': restImpact.EditorId,
        'lastModifiedDate': miscServices.parseDatetime(restImpact.Modified)
      };
    };

    // Helper function to clear promises when impact flags change
    var clearPromises = function(impact) {
      impactsPromises['CATEGORY-' + (impact.targetRiskCategory === undefined ? 'NONE' : impact.targetRiskCategory)] = undefined;
      impactsPromises['CATEGORY-ALL'] = undefined;
      impactsPromises['SOURCE-' + impact.sourceEndpoint + '-' + impact.sourceId] = undefined;

      Object.keys(impactsPromises).forEach(function(k) {
        var matched = k.match(/^SOURCE-KEYWORDS-([.+])-(FULL|PREFIX)$/);
        if(matched) {
          var impactKeywords = matched[1].split('|');

          impactKeywords.forEach(function(impactKeyword) {
            impact.sourceKeywords.forEach(function (sourceKeyword) {
              var minLength = Math.min(impactKeyword.length, sourceKeyword.length);
              if (impactKeyword === sourceKeyword || (matched[2] === 'PREFIX' && impactKeyword.substr(0, minLength) === sourceKeyword.substr(0, minLength))) {
                impactsPromises[k] = undefined;
              }
            });
          });
        }
      });
    };


    /**
     * @ngdoc method
     * @name risksServices.impactsService#addImpact
     * @methodOf risksServices.impactsService
     * @kind function
     *
     * @description
     * This method adds the given `impact` object to the database as a new imapct flag. It returns a Promise that
     * resolves to the created object once it has been updated in the database (with `id` value set).
     *
     *
     * @param {Object}   impact             The impact object which should be added to the database.
     * @param {String=}  targetDescription  A description of the target this impact is being attached to (e.g. the
     *                                      risk title). Will be used for any notifications about this impact.
     *
     * @returns {Promise} Returns a promise which resolves to the added impact object (including its new ID).
     *
     */

    impactServ.addImpact = function(impact, targetDescription) {
      var impactAuthorId;

      return restListService.addItem('impact_flags', asRestImpact(impact))
        .then(
          function(res) {
            // TODO: remove debug code
            console.log('Added impact to SharePoint list:');
            console.log(res);

            // Reset impactsPromises for this object
            clearPromises(impact);

            // Set last modified date
            impact.lastModifiedDate = new Date();

            // Set impact date
            impact.impactDate = miscServices.parseDatetime(res.data.d.impactDate);

            // Get ID
            impact.id = res.data.d.Id;

            // Get author
            impactAuthorId = res.data.d.AuthorId;

            // Do we need to notify subscribers of the risk, the risk category or the source of the added impact?
            if(impact.targetRiskInitialId !== undefined && impact.targetRiskInitialId !== null) {
              // This impact was assigned to an individual risk, so we notify the subscribers to this risk
              return subscriptionService.triggerSubs(
                'risk',
                impact.targetRiskInitialId,
                'impact',
                impact.id,
                undefined,
                impact.sourceUrl,
                {
                  from: impactAuthorId,
                  impact: impact,
                  targetDescription: targetDescription
                },
                'new-impact'
              );
            } else if(impact.targetRiskCategory && impact.targetRiskCategory !== 'NONE') {
              // Only if the impact is not assigned to an individual risk do we notify the risk category subscribers.
              return subscriptionService.triggerSubs(
                'riskCategory',
                impact.targetRiskCategory,
                'impact',
                impact.id,
                undefined,
                impact.sourceUrl,
                {
                  from: impactAuthorId,
                  impact: impact,
                  targetDescription: targetDescription
                },
                'new-impact'
              );
            }
          }
        )
        .then(
          function() {
            // Let's notify the watchers of the source endpoint about the new impact
            return subscriptionService.triggerSubs(
              subscriptionService.subscriptionTypeForEndpoint(impact.sourceEndpoint),
              impact.sourceId,
              'impact',
              impact.id,
              undefined,
              impact.sourceUrl,
              {
                from: impactAuthorId,
                impact: impact,
                targetDescription: targetDescription
              },
              'new-impact'
            );
          }
        )
        .then(
          function() {
            return impact;
          },
          function(err) {
            console.log('An ERROR occured while trying to addImpact:');
            console.log(impact);
            console.log(err);

            return $q.reject(gettext('An error occurred while trying to update the impacts for this note.'));
          }
        );
    };



    /**
     * @ngdoc method
     * @name risksServices.impactsService#updateImpact
     * @methodOf risksServices.impactsService
     * @kind function
     *
     * @description
     * This method updates the given `impact` object in the database, identifying it by its `impact.id` attribute. It
     * returns a Promise that resolves to the updated object once it has been updated in the database.
     *
     *
     * @param {Object} impact       The impact object which should be updated in the database.
     *
     * @returns {Promise} Returns a promise which resolves to the updated impact object.
     *
     */

    impactServ.updateImpact = function(impact) {
      return restListService.updateItem('impact_flags', impact.id, asRestImpact(impact))
        .then(
          // Get author and update element
          function() {
            // Set last modified date
            impact.lastModifiedDate = new Date();

            // Reset impactsPromises for this object
            clearPromises(impact);

            return impact;
          },
          function(restErr) {
            return $q.reject(gettext('An error occurred when saving the note to the server:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
          }
        );
    };



    /**
     * @ngdoc method
     * @name risksServices.impactsService#deleteImpact
     * @methodOf risksServices.impactsService
     * @kind function
     *
     * @description
     * This method removes the given `impact` object from the database, identifying it by its `impact.id` attribute. It
     * returns a Promise that resolves (empty) when the object has successfully been deleted. The method also calls the
     * `interactionService` and `subscriptionService` to remove any interactions or newsfeed entries for the given
     * impact (this happens in the background to avoid having to keep the user waiting).
     *
     *
     * @param {Object} impact       The impact object which should be removed from the database.
     *
     * @returns {Promise} Returns a promise which resolves if the object has been deleted.
     *
     */

    impactServ.deleteImpact = function(impact) {
      return restListService.deleteItem('impact_flags', impact.id).then(
        function() {
          // Reset impactsPromises for this object
          clearPromises(impact);

          // We also remove news items and interactions for this note
          // (in the background, so we can return earlier...)
          subscriptionService
            .removeNewsBySource('impact', impact.id)
            .then(
              function() {
                return interactionService.removeInteractionsByTarget('impact', impact.id);
              }
            );
        },
        function(restErr) {
          return $q.reject(gettext('An error occurred deleting the impact flag:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
        }
      );
    };



    /**
     * @ngdoc method
     * @name risksServices.impactsService#getImpactsByRiskCategory
     * @methodOf risksServices.impactsService
     * @kind function
     *
     * @description
     * This method retrieves a list of impact flags for the given `riskCategory`. You can also limit the search to only
     * retrieving impact flags that have been set between specific dates `fromDate` and `toDate`. Returns a promise
     * which resolves to an array of all obtained impact flags.
     *
     *
     * @param {string} riskCategory The Category of risks for which impact flags should be retrieved. Set to 'NONE' if
     *                              you want to obtain impact flags not associated to any risk category. Set to
     *                              `undefined` if you want to retrieve all impact flags irrespective of risk category.
     *
     * @param {Date=}   fromDate    If given, only flags with an `impactDate` at or after this date will be retrieved.
     *
     * @param {Date=}   toDate      If given, only flags with an `impactDate` at or before this date will be retrieved.
     *
     *
     * @returns {Promise} Returns a promise which resolves to an array of all obtained impact flags.
     *
     */

    impactServ.getImpactsByRiskCategory = function(riskCategory, fromDate, toDate) {
      var deferred;

      var promiseKey = 'CATEGORY-' + (riskCategory === undefined ? 'ALL' : riskCategory);

      // Does a promise already exist for this query with from and to dates at least as big as for this query?
      if(impactsPromises[promiseKey]) {
        var foundPromise = impactsPromises[promiseKey].find(function(d) {
          return    (fromDate ? (!d.fromDate || d.fromDate <= fromDate) : !d.FromDate) &&
                    (toDate   ? (!d.toDate   || d.toDate >= toDate)     : !d.toDate);
        });

        if(foundPromise) {
          // We have found a promise whose from/toDate are equal or larger than ours
          // We thus need to restrict the found impact flags to our from/toDates
          return foundPromise.promise
            .then(function(impacts) {
              return impacts.filter(function(impact) {
                return (!fromDate || impact.impactDate >= fromDate) && (!toDate || impact.impactDate <= toDate);
              });
            });
        }
      } else {
        impactsPromises[promiseKey] = [];
      }


      // Otherwise we need to create a new promise and get new data
      deferred = $q.defer();
      impactsPromises[promiseKey].push({
        fromDate: fromDate,
        toDate: toDate,
        promise: deferred.promise
      });

      // Create query string
      var queryArray = [];

      if(riskCategory !== undefined) {
        queryArray.push("targetRiskCategory eq '" + riskCategory.replace("'", "''") + "'");
      }

      if(fromDate) {
        queryArray.push("impactDate ge datetime'" + fromDate.toISOString().substr(0,10) + "T00:00:00'");
      }

      if(toDate) {
        queryArray.push("impactDate le datetime'" + toDate.toISOString().substr(0,10) + "T23:59:59'");
      }

      // Retrieve impacts from server
      restListService.getMergedFilteredItems('impact_flags', queryArray.join(' and '), undefined, undefined, 'Title, impactDate').then(
        function(res) {
          // Success - got data from the server

          // TODO: Remove debug code
          console.log('Success in getting impacts for category ' + riskCategory + '!');
          console.log(res);

          // Are we getting the right data?
          if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
            // Returns already rejected promise
            deferred.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
          }

          var retrievedImpacts = res.data.d.results.map(fromRestImpact);


          // Sort this again as results returned by getMergedFilteredItems are not guaranteed to be properly
          // sorted.
          retrievedImpacts.sort(function(a, b){
            if(a.impactType !== b.impactType) {
              return (a.impactType < b.impactType ? 1 : -1);
            }

            if(a.impactDate > b.impactDate) {
              return 1;
            }

            if(a.impactDate < b.impactDate) {
              return -1;
            }

            return (a.lastModifiedDate - b.lastModifiedDate);
          });


          deferred.resolve(retrievedImpacts);
        },
        function(restErr) {
          deferred.reject(gettext('An error occurred:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
        }
      );

      // Create promise for risks by category
      return deferred.promise;
    };



    /**
     * @ngdoc method
     * @name risksServices.impactsService#getImpactsBySource
     * @methodOf risksServices.impactsService
     * @kind function
     *
     * @description
     * This method retrieves a list of impact flags for the source element which is specified by its `sourceEndpoint`
     * and `sourceId`.
     *
     * @param {string} sourceEndpoint  The endpoint ID (as defined in the {@link restServices restServices} module) for
     *                                 the source element whose impact flags should be retrieved (usually `notes`).
     *
     * @param {integer} sourceId       The item ID (as given in the SharePoint database) for the source element whose
     *                                 impact flags should be retrieved.
     *
     *
     * @returns {Promise} Returns a promise which resolves to an array of all obtained impact flags.
     *
     */

    impactServ.getImpactsBySource = function(sourceEndpoint, sourceId) {
      var deferred;

      var promiseKey = 'SOURCE-' + sourceEndpoint + '-' + sourceId;

      // Does a promise already exist for this query?
      if(impactsPromises[promiseKey]) {
        return impactsPromises[promiseKey];
      }


      // Otherwise we need to create a new promise and get new data
      deferred = $q.defer();
      impactsPromises[promiseKey] = deferred.promise;

      // Retrieve impacts from server
      restListService.getMergedFilteredItems('impact_flags', "sourceEndpoint eq '" + sourceEndpoint.replace("'", "''") + "' and sourceId eq " + sourceId, undefined, undefined, 'impactDate').then(
        function(res) {
          // Success - got data from the server

          // TODO: Remove debug code
          console.log('Success in getting impacts for source ' + sourceEndpoint + ' / ' + sourceId + '!');
          console.log(res);

          // Are we getting the right data?
          if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
            // Returns already rejected promise
            deferred.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
          }

          // Resolve with the obtained impact flags
          deferred.resolve(res.data.d.results.map(fromRestImpact));

        },
        function(restErr) {
          deferred.reject(gettext('An error occurred:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
        }
      );

      // Create promise for risks by category
      return deferred.promise;
    };



    /**
     * @ngdoc method
     * @name risksServices.impactsService#getImpactsBySourceKeywords
     * @methodOf risksServices.impactsService
     * @kind function
     *
     * @description
     * This method retrieves a list of impact flags that match all the given `sourceKeywords` and the given
     * `sourceEndpoint`.
     *
     * @param {string} sourceEndpoint  The endpoint ID (as defined in the {@link restServices restServices} module) for
     *                                 the source elements whose impact flags should be retrieved (usually `notes`).
     *
     * @param {Array} sourceKeywords   An array of keyword IDs which must *all* be given as source keywords for an
     *                                 impact flag to be retrieved.
     *
     * @param {boolean=} isPrefix      If this is set to `true`, will also retrieve impact flags whose keywords have
     *                                 the given prefixes (and not only an exact match).
     *
     *
     * @returns {Promise} Returns a promise which resolves to an array of all obtained impact flags.
     *
     */

    impactServ.getImpactsBySourceKeywords = function(sourceEndpoint, sourceKeywords, isPrefix) {
      var deferred;

      var promiseKey = 'SOURCE-KEYWORDS-' + sourceEndpoint + '-' + sourceKeywords.join('|') + '-' + (isPrefix ? 'PREFIX' : 'FULL');

      // Does a promise already exist for this query?
      if(impactsPromises[promiseKey]) {
        return impactsPromises[promiseKey];
      }


      // Otherwise we need to create a new promise and get new data
      deferred = $q.defer();
      impactsPromises[promiseKey] = deferred.promise;

      var searchString = "sourceEndpoint eq '" + sourceEndpoint.replace("'", "''") + "' and " + sourceKeywords
        .map(function(searchTerm) {
          return "substringof('" + (isPrefix ? searchTerm : searchTerm + ',') + "', sourceKeywords)";
        })
        .join(' and ');

      // TODO: Remove debug code
      console.log('Getting impacts for sourcesKeywords:');
      console.log([sourceEndpoint, sourceKeywords, isPrefix, searchString]);

      // Retrieve impacts from server
      restListService.getMergedFilteredItems('impact_flags', searchString, undefined, undefined, 'impactDate').then(
        function(res) {
          // Success - got data from the server

          // TODO: Remove debug code
          console.log('Success in getting impacts for sourcesKeywords:');
          console.log([sourceEndpoint, sourceKeywords, isPrefix]);
          console.log(res);

          // Are we getting the right data?
          if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
            // Returns already rejected promise
            deferred.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
          }

          // Resolve with the obtained impact flags
          deferred.resolve(res.data.d.results.map(fromRestImpact));

        },
        function(restErr) {
          deferred.reject(gettext('An error occurred:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
        }
      );

      // Create promise for risks by category
      return deferred.promise;
    };


    return impactServ;
  }]);

