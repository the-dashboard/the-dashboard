'use strict';

/**
 * @ngdoc overview
 * @name dashboardScreenServices
 * @description
 *
 * This module provides services for the overall GUI of The Dashboard. This includes access functions for the
 * sidebar and helper functions for screen width etc. calculations.
 *
 */

angular.module('dashboardScreenServices', ['restServices', 'miscServices', 'ngDialog'])



  .run(['$rootScope', '$location', 'sidebarService', function($rootScope, $location, sidebarService) {
    /*
      When we load this module, we register for locationChangeSucess events => if they do not contain a sidebar
      parameter, we close the sidebar!
    */
    $rootScope.$on('$locationChangeSuccess', function () {
      var searchObject = $location.search();

      console.log('dashboardScreenServices captured locationChangeSuccess event!');
      console.log(searchObject);

      if (!searchObject.sidebar) {
        sidebarService.close();
      }
    });

    // When the sidebar is open and we open or close dialogs, we need to change its visibility
    $rootScope.$on('ngDialog.opened', function() {
      sidebarService.updateOpenVisibility('show-over-dialog');
    });

    $rootScope.$on('ngDialog.closed', function() {
      sidebarService.updateOpenVisibility('show');
    });

  }])


  /**
   * @ngdoc service
   * @name dashboardScreenServices.screenService
   * @description
   *
   * This service provides functionality to obtain the available screen size (for example for dynamically resizing
   * dashboards or tables) and other helper functions.
   *
   */

  .factory('screenService', function() {
    var screenServ = {};

    var sidebarPixels = {};
    var sidebarAlign = {};

    var setMainMargins;


    /**
     * @ngdoc method
     * @name dashboardScreenServices.screenService#getAvailableScreenSize
     * @methodOf dashboardScreenServices.screenService
     * @kind function
     *
     * @description
     * Returns the available screen size for content in The Dashboard, i.e. accounting for the available browser window
     * size and the window space already taken up by active navigation and sidebars within The Dashboard.
     *
     * @param {string=} ignoreSidebar If this parameter is given, the sidebar with the given key will not be counted
     *                                for the available screen size (relevant if it is this sidebar who wants to know
     *                                how much space it can take up).
     *
     * @returns {Array} An array with the width (element 0) and height (element 1) of the available screen size,
     *                  i.e. accounting for browser window and Dashboard internal side and navigation bars.
     */

    screenServ.getAvailableScreenSize = function(ignoreSidebar) {
      var sidebarPixelSum = Object.keys(sidebarPixels).reduce(function(prevVal, currKey) {
        return prevVal + ((ignoreSidebar && currKey === ignoreSidebar) ? 0 : sidebarPixels[currKey]);
      }, 0);

      return [ window.innerWidth - sidebarPixelSum, window.innerHeight - document.getElementById('mainNavbar').offsetHeight ];
    };



    /**
     * @ngdoc method
     * @name dashboardScreenServices.screenService#updateMainMargins
     * @methodOf dashboardScreenServices.screenService
     * @kind function
     *
     * @description
     * This method calculates the required left and right margins of the main div element depending on the defined
     * sidebars. It then calls the mainDivController helper function to adjust them (if registered).
     *
     *
     */

    screenServ.updateMainMargins = function() {
      if(setMainMargins) {
        var leftMargin = Object.keys(sidebarPixels).reduce(function(prevVal, currKey) {
          return prevVal + (sidebarAlign[currKey] === 'left' ? sidebarPixels[currKey] : 0);
        }, 0);

        var rightMargin = Object.keys(sidebarPixels).reduce(function(prevVal, currKey) {
          return prevVal + (sidebarAlign[currKey] === 'right' ? sidebarPixels[currKey] : 0);
        }, 0);

        setMainMargins(leftMargin, rightMargin);
      }
    };


    /**
     * @ngdoc method
     * @name dashboardScreenServices.screenService#setSidebarPixels
     * @methodOf dashboardScreenServices.screenService
     * @kind function
     *
     * @description
     * This method can be used to set the currently used pixel width of a specific sidebar (there can be multiple
     * sidebars, e.g. one at the left and one at the right or stacked sidebars).
     *
     * The number will be used to calculate screen size and will also be used to set margins on the main content
     * div, which will make sure that the content does not "underflow" under the sidebar.
     *
     * To clear, set the number of pixels for the specific sidebar to 0.
     *
     *
     * @param {string} sidebarName A string that uniquely identifies the given sidebar whose pixel width should be set.
     *
     * @param {number} noPixels The number of pixels that should be accounted for for this sidebar.
     *
     * @param {string} align Is the sidebar aligned to the left or to the right?
     *
     */

    screenServ.setSidebarPixels = function(sidebarName, noPixels, align) {
      sidebarPixels[sidebarName] = noPixels;
      sidebarAlign[sidebarName] = align;

      // Update the margins of the main div element
      screenServ.updateMainMargins();
    };



    /**
     * @ngdoc method
     * @name dashboardScreenServices.screenService#registerController
     * @methodOf dashboardScreenServices.screenService
     * @kind function
     *
     * @description
     * Registers the helper function of the mainDivController which allows us to change the margins of the central div
     * element if we add or remove "sticky" sidebars.
     *
     * @param {Function} _setMainMargins The function to be called when the main div margins need to be changed. First
     *                                   argument will be the left margin, second argument the right margin.
     *
     */

    screenServ.registerController = function(_setMainMargins) {
      setMainMargins = _setMainMargins;
    };

    return screenServ;
  })



  /**
   * @ngdoc service
   * @name dashboardScreenServices.sidebarService
   * @description
   *
   * This service provides functionality to open, close and fill with content the main sidebar that is available for
   * all The Dashboard views.
   *
   */

  .factory('sidebarService', ['$q', 'ngDialog', 'miscServices', 'screenService', function($q, ngDialog, miscServices, screenService) {
    var sidebarServ = {};

    // How big is the sidebar?
    var SIDEBAR_WIDTH = 300;

    // Is the sidebar open?
    var currVisibility = 'hide';

    // Registered functions from the sidebar controller
    var loadTemplate;
    var setVisibility;


    /**
     * @ngdoc method
     * @name dashboardScreenServices.sidebarService#open
     * @methodOf dashboardScreenServices.sidebarService
     * @kind function
     *
     * @description
     * This method "opens" the sidebar, i.e. it loads the given content `template` (if not already loaded) and `data`
     * and shows the sidebar. If `asOverlay` is `false` or unset, the sidebar will be displayed permanently and
     * "push aside" content. If `asOverlay` is `true`, it will be overlayed over the existing content.
     *
     * If `minPixels` is specified, the sidebar will only be shown if the remaining screen size after opening the
     * sidebar is at least as large as `minPixels`.
     *
     * @param {string} template The path of the template to be loaded.
     *
     * @param {Object} data An object of data elements which will be made available in the scope of the template.
     *
     * @param {boolean=} asOverlay If `true`, the sidebar will overlay existing content instead of "pushing" it away.
     *
     * @param {number=} minPixels If given, the sidebar will only be opened if the remaining screen size after
     *                            accounting for the sidebar is at least `minPixels` wide.
     *
     * @returns {boolean} Returns `true` if the sidebar is being opened. Returns `false` if the open request was
     *                    ignored because there whould not be sufficient `minPixels`.
     *
     */

    sidebarServ.open = function(template, data, asOverlay, minPixels) {
      // TODO: Remove debug code
      console.log('Sidebar: Open called...');

      // Fail if the controller has not been loaded yet
      if(!loadTemplate || !setVisibility) { return false; }

      // We fail if no template has been given.
      if(!template) { return false; }


      // TODO: Remove debug code
      console.log('Sidebar: Checking minPixels...');

      // We fail if the minPixels after opening would not be sufficient.
      if(minPixels && screenService.getAvailableScreenSize('main')[0] - SIDEBAR_WIDTH < minPixels) { return false; }

      // We already register the screen size the sidebar will now take up.
      screenService.setSidebarPixels('main', (asOverlay ? 0 : SIDEBAR_WIDTH), 'right');

      // Update current visibility
      currVisibility = (asOverlay ? 'overlay' : 'show');

      // If a dialog is open and we open a new "show" sidebar, we will have to make it visible *over* the dialog
      if(!asOverlay && ngDialog.getOpenDialogs().length > 0) {
        currVisibility = 'show-over-dialog';
      }

      // TODO: Remove debug code
      console.log('Sidebar: We are loading the template...');

      // Ok, we go ahead with opening the sidebar.
      loadTemplate(template, data);
      setVisibility(currVisibility);

      // Done.
      return true;
    };



    /**
     * @ngdoc method
     * @name dashboardScreenServices.sidebarService#close
     * @methodOf dashboardScreenServices.sidebarService
     * @kind function
     *
     * @description
     * Closes the sidebar, i.e. hides it from view. It does not unload the template, making it quicker to open it
     * again if need be, but taking up space in the DOM.
     *
     */

    sidebarServ.close = function() {
      // Are we already hidden?
      if(currVisibility === 'hide') { return; }

      // Fail if the controller has not been loaded yet
      if(!loadTemplate || !setVisibility) { return; }

      // Set visibility to hide.
      currVisibility = 'hide';
      setVisibility('hide');

      // Set sidebar pixels to 0.
      screenService.setSidebarPixels('main', 0, 'right');

      // Done.
    };




    /**
     * @ngdoc method
     * @name dashboardScreenServices.sidebarService#updateOpenVisibility
     * @methodOf dashboardScreenServices.sidebarService
     * @kind function
     *
     * @description
     * If a sidebar is currently open (not as an overlay!) we update its visibility with the given value.
     * Is being used to move the sidebar to the foreground / background when dialogs are opened or closed.
     *
     * @param {String} newVisibility The new visibility that should be set on the sidebar if it is currently open.
     *
     */

    sidebarServ.updateOpenVisibility = function(newVisibility) {
      // Is the current visibility "show" or "show-over-dialog"?
      if(currVisibility === 'show' || currVisibility === 'show-over-dialog') {
        currVisibility = newVisibility;
        setVisibility(newVisibility);
      }
    };



    /**
     * @ngdoc method
     * @name dashboardScreenServices.sidebarService#unload
     * @methodOf dashboardScreenServices.sidebarService
     * @kind function
     *
     * @description
     * Unloads the current template from the DOM. Also closes the sidebar, if it is not already closed.
     *
     */

    sidebarServ.unload = function() {
      // Fail if the controller has not been loaded yet
      if(!loadTemplate || !setVisibility) { return; }

      // Close the sidebar.
      sidebarServ.close();

      // Unload the template.
      loadTemplate('');

      // Done.
    };


    /**
     * @ngdoc method
     * @name dashboardScreenServices.sidebarService#registerController
     * @methodOf dashboardScreenServices.sidebarService
     * @kind function
     *
     * @description
     * Registers the helper functions in the controller required to change the sidebar's appearance.
     *
     * @param {Function} _loadTemplate The function to be called for loading a template and/or corresponding data in the
     *                                 sidebar. Will be passed the template and the data object to load in the sidebar's
     *                                 scope.
     *
     * @param {Function} _setVisibility The function to show or hide the sidebar. Will be called with a string value
     *                                  of `show` (to show), `overlay` (to show as overlay) or `hide` (to hide).
     *
     */

    sidebarServ.registerController = function(_loadTemplate, _setVisibility) {
      loadTemplate = _loadTemplate;
      setVisibility = _setVisibility;
    };



    return sidebarServ;
  }]);

