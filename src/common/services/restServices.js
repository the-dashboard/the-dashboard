'use strict';

/**
 * @ngdoc overview
 * @name restServices
 * @description
 *
 * This module provides services to communicate with the SharePoint REST API. It provides various methods to
 * manipulate SharePoint lists, list entries, attachments, user records, etc. using the REST API:
 *
 *    https://msdn.microsoft.com/en-us/library/office/jj860569.aspx
 *
 * The module also contains the configuration for the SharePoint server URLs to be used (see `sharepointConfig`
 * variable in the code's configuration part).
 *
 */

angular.module('restServices', ['gettext'])

  // Configuration for the SharePoint connection
  .config(['restBaseServiceProvider', 'restListServiceProvider', 'restUserServiceProvider', 'restDocumentServiceProvider', function(restBaseServiceProvider, restListServiceProvider, restUserServiceProvider, restDocumentServiceProvider) {
    // Main configuration for SharePoint connection
    var sharepointConfig = {
      baseUrl: window.dashboardSharePointBaseUrl,
      apiBaseUrl: window.dashboardSharePointApiBaseUrl,

      // Configuration for SharePoint list item limits
      maxItemsPerRequest: 200,  // How many items to retrieve in one request (otherwise will split using top / skiptoken)
      listViewThreshold: (window.dashboardSharePointListViewThreshold ? window.dashboardSharePointListViewThreshold : 5000),  // SharePoint's list view threshold (default 5000)

      httpGetConfig: {
        headers: {
          'Accept': 'application/json;odata=verbose'
        }
      },
      httpPostConfig: {
        headers: {
          'Accept': 'application/json;odata=verbose',
          'Content-Type': 'application/json;odata=verbose'
        }
      },
      httpUploadConfig: {
        transformRequest: angular.identity,
        headers: {
          'Accept': 'application/json;odata=verbose',
          'Content-Type': 'application/json;odata=verbose',
          'BinaryStringRequestBody': 'true'
        }
      },
      httpPutConfig: {
        headers: {
          'Accept': 'application/json;odata=verbose',
          'Content-Type': 'application/json;odata=verbose',
          'IF-MATCH': '*',
          'X-HTTP-Method': 'MERGE'
        }
      },
      httpDeleteConfig: {
        headers: {
          'Accept': 'application/json;odata=verbose',
          'Content-Type': 'application/json;odata=verbose',
          'IF-MATCH': '*',
          'X-HTTP-Method': 'DELETE'
        }
      }
    };

    // Endpoint configuration for SharePoint connection
    var sharepointEndpoints = {
      'tree':                    "web/lists/getbytitle%28'Dashboard%20-%20Tree'%29",
      'views':                   "web/lists/getbytitle%28'Dashboard%20-%20Views'%29",
      'notes':                   "web/lists/getbytitle%28'Dashboard%20-%20Notes'%29",
      'keywords':                "web/lists/getbytitle%28'Dashboard%20-%20Keywords'%29",
      'datafiles':               "web/lists/getbytitle%28'Dashboard%20-%20Datafiles'%29",
      'risks':                   "web/lists/getbytitle%28'Dashboard%20-%20Risks'%29",
      'configuration':           "web/lists/getbytitle%28'Dashboard%20-%20Configuration'%29",
      'user_settings':           "web/lists/getbytitle%28'Dashboard%20-%20User%20Settings'%29",
      'task_data':               "web/lists/getbytitle%28'Dashboard%20-%20Task%20Data'%29",
      'task_data_archive':       "web/lists/getbytitle%28'Dashboard%20-%20Task%20Archive'%29",
      'object_explorer_objects': "web/lists/getbytitle%28'Dashboard%20-%20Object%20Explorer%20Objects'%29",
      'object_explorer_mapping': "web/lists/getbytitle%28'Dashboard%20-%20Object%20Explorer%20Mapping'%29",
      'impact_flags':            "web/lists/getbytitle%28'Dashboard%20-%20Impact%20Flags'%29",
      'audit_forms':             "web/lists/getbytitle%28'Dashboard%20-%20Audit%20Forms'%29",
      'audit_forms_data':        "web/lists/getbytitle%28'Dashboard%20-%20Audit%20Forms%20Data'%29",
      'task_results':            "web/lists/getbytitle%28'Dashboard%20-%20Task%20Results'%29",
      'task_results_archive':    "web/lists/getbytitle%28'Dashboard%20-%20Task%20Archive%20Results'%29",
      'feedback':                "web/lists/getbytitle%28'Dashboard%20-%20Feedback'%29",
      'newsfeed':                "web/lists/getbytitle%28'Dashboard%20-%20Newsfeed'%29",
      'interactions':            "web/lists/getbytitle%28'Dashboard%20-%20Interactions'%29",
      'subscriptions':           "web/lists/getbytitle%28'Dashboard%20-%20Subscriptions'%29"
    };

    var sharepointEndpointsFolders = {
      'task_data':               "web/getfolderbyserverrelativeurl%28'Dashboard%20%20Task%20Data'%29",
      'task_data_archive':       "web/getfolderbyserverrelativeurl%28'Dashboard%20%20Task%20Archive'%29",
      'audit_forms':             "web/getfolderbyserverrelativeurl%28'Dashboard%20%20Audit%20Forms'%29",
      'task_results':            "web/getfolderbyserverrelativeurl%28'Dashboard%20%20Task%20Results'%29",
      'task_results_archive':    "web/getfolderbyserverrelativeurl%28'Dashboard%20%20Task%20Archive%20Results'%29"
    };

    // Example URL: https://sp10942.hostedoffice.ag/demo/_api/web/lists/getbytitle%28'Dashboard%20-%20Tree'%29/items

    // Call configratuion functions for all relevant providers
    restBaseServiceProvider.doConfig(sharepointConfig);
    restListServiceProvider.doConfig(sharepointEndpoints, sharepointConfig);
    restUserServiceProvider.doConfig(sharepointEndpoints, sharepointConfig);
    restDocumentServiceProvider.doConfig(sharepointEndpoints, sharepointEndpointsFolders, sharepointConfig);
  }])



  /**
   * @ngdoc service
   * @name restServices.restBaseService
   * @description
   *
   * This service provides basic SharePoint functionality, such as obtaining the contextinfo item required for
   * manipulating elements via the REST API.
   *
   */

  .provider('restBaseService', function() {
    var provider  = {};
    var config    = {};

     var spContextInfo;
     var spContextInfoLastUpdated;
     var spContextInfoPromise;

    provider.doConfig = function(configParam) {
      config = configParam;
    };

     // Return provided services
    provider.$get = ['$http', '$q', 'gettext', function($http, $q, gettext) {
      var service = {};

      /**
       * @ngdoc method
       * @name restServices.restBaseService#updateFormDigest
       * @methodOf restServices.restBaseService
       * @kind function
       *
       * @description
       * This method obtains the `FormDigestValue` from the `spContextInfo` object, which SharePoint uses to prevent
       * cross-site scripting attacks: any modifying REST API call requires the `FormDigestValue` to be passed in which
       * needs to be obtained via a `POST` request first.
       *
       * You can call this function and pass it the configuration object `configObject of your service (which needs to`
       * have `httpPostConfig`, `httpUploadConfig`, `httpPutConfig` and `httpDeleteConfig` objects which contain the
       * `headers` that will be passed to any `$http` call).
       *
       * The function will then obtain a contextinfo item, extract its `FormDigestValue` and update all the
       * `X-RequestDigest` headers SharePoint will expect. It will return a promise that resolves to the raw
       * `spContextInfo` object returned by SharePoint.
       *
       * @param {Object} configObject The configuration object of your REST service. Needs to have `httpPostConfig`,
       *                              `httpUploadConfig`, `httpPutConfig` and `httpDeleteConfig` objects which contain
       *                              a `headers` object. This method will set the `X-RequestDigest` element of these
       *                              `headers` objects.
       *
       *                              If you only want to retrieve the `spContextInfo` element, you can pass
       *                              `undefined` as `configObject`.
       *
       * @returns {Promise} A promise which resolves to the valid `spContextInfo` object which can be used to
       *                    manipulate SharePoint elements.
       *
       */

      service.updateFormDigest = function(configObject) {
        // Do we have an open promise that has not yet expired?
        // We set the right X-RequestDigest headers and return.
        if(spContextInfoPromise && (!spContextInfo || !spContextInfoLastUpdated || spContextInfo.FormDigestTimeoutSeconds*1000 > (Date.now() - spContextInfoLastUpdated))) {
          return spContextInfoPromise
            .then(function(spContextInfo) {
              // Update X-RequestDigest header
              if(configObject) {
                configObject.httpPostConfig.headers['X-RequestDigest'] = spContextInfo.FormDigestValue;
                configObject.httpUploadConfig.headers['X-RequestDigest'] = spContextInfo.FormDigestValue;
                configObject.httpPutConfig.headers['X-RequestDigest'] = spContextInfo.FormDigestValue;
                configObject.httpDeleteConfig.headers['X-RequestDigest'] = spContextInfo.FormDigestValue;
              }

              // Return the context info object
              return spContextInfo;
            });
        }

        // Otherwise we need to roll a new one.
        var deferred = $q.defer();

        spContextInfoPromise = deferred.promise;

        // No context info available or it is expired - update.
        $http.post(config.apiBaseUrl + 'contextinfo', '', config.httpPostConfig).then(
          function(res) {
            spContextInfoLastUpdated = Date.now();
            spContextInfo = res.data.d.GetContextWebInformation;

            // Update X-RequestDigest header
            if(configObject) {
              configObject.httpPostConfig.headers['X-RequestDigest'] = spContextInfo.FormDigestValue;
              configObject.httpUploadConfig.headers['X-RequestDigest'] = spContextInfo.FormDigestValue;
              configObject.httpPutConfig.headers['X-RequestDigest'] = spContextInfo.FormDigestValue;
              configObject.httpDeleteConfig.headers['X-RequestDigest'] = spContextInfo.FormDigestValue;
            }

            deferred.resolve(spContextInfo);
          },
          function(restErr) {
            deferred.reject(restErr);
          }
        );

        return deferred.promise;
      };



      /**
       * @ngdoc method
       * @name restServices.restBaseService#processRestList
       * @methodOf restServices.restBaseService
       * @kind function
       *
       * @description
       * This method can be used after a REST response has been received from the server that is
       * expected to provide a list of results in the `res.data.d.results` array. The method will
       * check whether the array exists and will return it if it does. Otherwise it will return
       * a rejected promise and provide details in the console log.
       *
       * @param {Object} res   The REST response as returned by the SharePoint server.
       *
       * @returns {Promise}    A promise which gets rejected if the REST response is not in the valid
       *                       format or resolved with the array of results encapsulated in the
       *                       REST response.
       *
       */

      service.processRestList = function(res) {
        if(!res || !res.data || !res.data.d || !res.data.d.results || !Array.isArray(res.data.d.results)) {
          console.log('The REST response returned by the server was not in the expected array list format:');
          console.log(res);

          return $q.reject(gettext('Unexpected response from server.'));
        }

        return $q.resolve(res.data.d.results);
      };




      /**
       * @ngdoc method
       * @name restServices.restBaseService#processRestError
       * @methodOf restServices.restBaseService
       * @kind function
       *
       * @description
       * This method processes a rest error returned by SharePoint. It returns a rejected
       * promise with a generic error message and logs the details to the console log.
       *
       * @param {Object} restErr   The REST error as returned by the SharePoint server.
       *
       * @returns {Promise}    A rejected promise with a generic error message.
       *
       */

      service.processRestError = function(restErr) {
        console.log('A REST error occured:');
        console.log(restErr);

        return $q.reject(gettext('Unexpected response from server.'));
      };


      return service;
    }];


    return provider;
  })


  /**
   * @ngdoc service
   * @name restServices.restListService
   * @description
   *
   * This service allows to add, edit and remove list entries and to add and remove SharePoint lists. It uses the
   * pre-configured SharePoint endpoints (lists to be used by the Dashboard) to access the REST API.
   *
   */

  .provider('restListService', function() {
    var provider  = {};
    var endpoints = {};
    var config    = {};

    var spListItemTypes = {};

    provider.doConfig = function(endpointsParam, configParam) {
      endpoints = endpointsParam;
      config = configParam;

    };
    
    
    // Return provided services
    provider.$get = ['$http', '$q', 'gettext', 'restBaseService', function($http, $q, gettext, restBaseService) {
      var service = {};


      // Get list item type for posting to a list
      var getListItemType = function(endpoint, listTitle) {
        var deferred = $q.defer();
        var listURL = service.getListURL(endpoint, listTitle);
        
        if(spListItemTypes[listURL] === undefined) {
          // Obtain list item type from SharePoint API
          $http.get(listURL + '?$select=ListItemEntityTypeFullName', config.httpGetConfig).then(
            function(res) {
              // TODO: remove debug code
              console.log('Obtained list item type from server: ');
              console.log(res);
              
              spListItemTypes[listURL] = res.data.d.ListItemEntityTypeFullName;
              deferred.resolve(spListItemTypes[listURL]);
            },
            function(restErr) {
              deferred.reject(restErr);
            }
          );        
        } else {
          // List item type already obtained, just use it
          deferred.resolve(spListItemTypes[listURL]);
        }
        
        return deferred.promise;
      };



      /**
       * @ngdoc method
       * @name restServices.restListService#getListEndpoints
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method obtains the `endpoints` configuration array, which contains all the list endpoints used by
       * The Dashboard internally.
       *
       * @returns {Array} The content of the `endpoints` array.
       *
       */

      service.getListEndpoints = function() {
        // Returns a promise that deals with getting all items
        // Note that by default SharePoint only retrieves 100 results, so we set $top to the maximum allowed
        // (the list view threshold). Note that if the list has more than 5000 items, this will not get the
        // complete list. Use `getMergedAllItems` (below) for this.
        return endpoints;
      };




      /**
       * @ngdoc method
       * @name restServices.restListService#getListURL
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * Helper function to construct a list URL from an endpoint specification, listTitle (if specified) or
       * a list's GUID (if specified).
       *
       * @oaram {string} endpoint   If specified, the list URL will be obtained for the specified SharePoint endpoint,
       *                            which needs to exist in the Dashboard configuration.
       * @param {string} listTitle  If `endpoint` is given as `undefined` but a `listTitle` is specified, the list URL
       *                            will be obtained based on the given title of the list.
       * @param {string} listGUID   If both `endpoint` and `listTitle` are `undefined`, a list's SharePoint-internal
       *                            GUID can be given as `listGUID` and the list URL will be created based on this
       *                            GUID.
       *
       * @returns {string} The generated list URL based on the `endpoint`, `listTitle` or `listGUID`.
       *
       */

      service.getListURL = function(endpoint, listTitle, listGUID) {
        if (endpoint !== undefined && endpoints[endpoint] !== undefined) {
          return config.apiBaseUrl + endpoints[endpoint];
        } else if (listTitle !== undefined) {
          return config.apiBaseUrl + "web/lists/getbytitle%28'" + encodeURIComponent(listTitle) + "'%29";
        } else if (listGUID !== undefined) {
          return config.apiBaseUrl + "web/lists%28guid'" + encodeURIComponent(listGUID) + "'%29";
        }

        return undefined;
      };



      /**
       * @ngdoc method
       * @name restServices.restListService#getAllItems
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method obtains all list elements for the list specified by the `endpoint` parameter. It returns a promise
       * that resolves to the raw results object `res` as returned by
       * [`$http`](https://docs.angularjs.org/api/ng/service/$http). The relevant data will be returned in
       * `res.data.d`, which will be an array of all list element objects from SharePoint.
       *
       * Note that this function is limited by SharePoint's list view threshold and does not return more items for
       * lists that exceed SharePoint's list view threshold. Use `getMergedAllItems` in this case.
       *
       * @param {string} endpoint A string indicating the endpoint (SharePoint list) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object returned by the REST API.
       *
       */

      service.getAllItems = function(endpoint) {
        // Returns a promise that deals with getting all items
        // Note that by default SharePoint only retrieves 100 results, so we set $top to the maximum allowed
        // (the list view threshold). Note that if the list has more than 5000 items, this will not get the
        // complete list. Use `getMergedAllItems` (below) for this.
        return $http.get(config.apiBaseUrl + endpoints[endpoint] + '/items?$top=' + config.listViewThreshold,
                        config.httpGetConfig);
      };


      /**
       * @ngdoc method
       * @name restServices.restListService#getMergedAllItems
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method obtains all list elements for the list specified by the `endpoint` parameter. It returns a promise
       * that resolves to the raw results object `res` as returned by
       * [`$http`](https://docs.angularjs.org/api/ng/service/$http). The relevant data will be returned in
       * `res.data.d`, which will be an array of all list element objects from SharePoint.
       *
       * This function iterates as long as it has really retrieved all items, exceeding SharePoint's list view
       * threshold. It also uses the configuration element maxItemsPerRequest to limit the number of items retrieved
       * per request.
       *
       * @param {string} endpoint A string indicating the endpoint (SharePoint list) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object.
       *
       * @param {string=} nextUrl Used to indicate the URL at which to continue to retrieve items. Can usually be left
       *                          `undefined`.
       *
       * @param {string=} listTitle If you want to retrieve items from a list which is not defined as a Dashboard endpoint,
       *                            you can set `endpoint` to `undefined` and specify a `listTitle` instead. The
       *                            list will then be retrieved by its name. Otherwise you can omit this parameter.
       *                            If `endpoint` is set, it will take precedence and `listTitle` will be ignored.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object returned by the REST API.
       *
       */

      service.getMergedAllItems = function(endpoint, nextUrl, listTitle) {
        return $http.get((nextUrl ? nextUrl : service.getListURL(endpoint, listTitle) + '/items?$top=' + config.maxItemsPerRequest), config.httpGetConfig)
          .then(function(res) {
            // Did we get a correct results object?
            if(!res || !res.data || !res.data.d || !res.data.d.results || !Array.isArray(res.data.d.results)) {
              // TODO: Remove debug code
              console.log('Received unexpected result in getMergedAllItems: ');
              console.log(res);

              return $q.reject('An unexpected result was received while trying to merge all items for the list.');
            }

            // Is there a next item? If yes, call iteratively...
            if(res.data.d.__next) {
              return service.getMergedAllItems(endpoint, res.data.d.__next, listTitle).then(function(newRes) {
                if(!newRes || !newRes.data || !newRes.data.d || !newRes.data.d.results || !Array.isArray(newRes.data.d.results)) {
                  // TODO: Remove debug code
                  console.log('When merging all items, received unexpected result: ');
                  console.log(newRes);

                  return $q.reject('An unexpected result was received while trying to merge all items for the list.');
                }

                newRes.data.d.results = res.data.d.results.concat(newRes.data.d.results);
                return newRes;
              });
            } else {
              // Otherwise we are done.
              return res;
            }
          });
      };



      /**
       * @ngdoc method
       * @name restServices.restListService#getFilteredItems
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method obtains a filtered array of list elements for the list specified by the `endpoint` parameter.
       *
       * The method also supports to specify which fields to be selected from the list (`select`), whether additional
       * fields should be retrieved from other lists linked by lookup columns (`expand`) and in which order the
       * items should be returned (`orderby`). Refer to
       * [Microsoft's oData documentation](https://msdn.microsoft.com/en-us/library/office/fp142385.aspx) for details
       * and examples on how to use these parameters.
       *
       * It returns a promise that resolves to the raw results object `res` as returned by
       * [`$http`](https://docs.angularjs.org/api/ng/service/$http). The relevant data will be returned in
       * `res.data.d`, which will be an array of all matching list element objects from SharePoint.
       *
       * @param {string} endpoint A string indicating the endpoint (SharePoint list) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object.
       *
       * @param {string} filter An oData-formatted filter string which is used to filter the list elements.
       *
       * @param {string} select A comma-separated list of fields which should be returned (leave undefined
       *                        if you want all fields).
       *
       * @param {string} expand A comma-separated list of fields from other lists which are linked to the list item via
       *                        a SharePoint lookup column and which you want to retrieve from those other lists. Note
       *                        that you also need to specify them in the `select` parameter.
       *
       * @param {string} orderby Comma-separated list of fields which define the sort order. Append a space and `asc`
       *                         or `desc` to field name to explicitly specify sort order for each field.
       *
       * @param {integer} top   If provided, is given as ``$top'' parameter to API. Limits retrieval of list items to
       *                        the number of items provided in ``top''.
       *
       * @param {string=} listTitle  If you omit the `endpoint` by setting it to `undefined`, you need to specify
       *                             a `listTitle` instead. The service will then try to access the relevant list not
       *                             by its endpoint specification but by the given `listTitle`. Otherwise omit.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object returned by the REST API.
       *
       */

      service.getFilteredItems = function(endpoint, filter, select, expand, orderby, top, listTitle) {
        // TODO: remove debug code
        console.log("getFilteredItems for list URL '" + service.getListURL(endpoint, listTitle) + "' and filter '" + filter + "' (with select = '" + select + "', expand = '" + expand + "', orderby = '" + orderby + "', top = " + top + ")");



        return $http.get(
          service.getListURL(endpoint, listTitle) + '/items?$filter=' + encodeURIComponent(filter) +
            (select !== undefined ? "&$select=" + encodeURIComponent(select) : "") +
            (expand !== undefined ? "&$expand=" + encodeURIComponent(expand) : "") +
            (orderby !== undefined ? "&$orderby=" + encodeURIComponent(orderby) : "") +
            (top !== undefined ? "&$top=" + encodeURIComponent(top) : ""),
          config.httpGetConfig
        );
      };



      /**
       * @ngdoc method
       * @name restServices.restListService#getMergedFilteredItems
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method obtains a filtered array of list elements for the list specified by the `endpoint` parameter.
       *
       * It supports similar parameters to the `getFilteredItems' method, but it works across the list view threshold
       * imposed by SharePoint (usually 5000 items) and across the `maxItemsPerRequest` configuration variable. All the
       * user has to do is to request `maxRecords` number of items and this function will work out how to retrieve
       * those and only stop once `maxRecords` have been reached or the whole list has been exhausted.
       *
       * It returns a promise that resolves to a results object `res` that contains the concatenated items as
       * `res.data.d.results`, the `nextUrl` for the next batch of results as `res.data.__next` and otherwise the
       * results from the last request made. (This choice has been made to stay compatible to the return values of
       * `getFilteredItems` such that both methods can be used. In general, `getMergedFilteredItems is preferable for
       * all lists that can reasonably be expected to return more than `maxItemsPerRequest` results.)
       *
       *
       * @param {string} endpoint A string indicating the endpoint (SharePoint list) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object.
       *
       * @param {string} filter An oData-formatted filter string which is used to filter the list elements.
       *
       * @param {string=} select A comma-separated list of fields which should be returned (leave undefined
       *                         if you want all fields).
       *
       * @param {string=} expand A comma-separated list of fields from other lists which are linked to the list item via
       *                         a SharePoint lookup column and which you want to retrieve from those other lists. Note
       *                         that you also need to specify them in the `select` parameter.
       *
       * @param {string=} orderby Comma-separated list of fields which define the sort order. Append a space and `asc`
       *                          or `desc` to field name to explicitly specify sort order for each field.
       *                          NOTE: The ordering is not ensured for lists that exceed the list view threshold,
       *                                as those will always be split into different ID segments! The application should
       *                                order the items again for any list that might exceed the list view threshold!
       *
       * @param {integer=} maxRecords  Determines the number of records to retrieve. This function will iterate as long
       *                               as neither this number nor the end of the list has been reached. Defaults to
       *                               Infinity (=retrieves all records).
       *
       * @param {string=} nextUrl      If this parameter is given, then all the previous parameters except for
       *                               `maxRecords` are ignored. The function will start retrieving records from
       *                               `nextUrl` until the (new) number of `maxRecords` has been reached or the list
       *                               has been exhausted. Usually you can just pass in the value of `res.data.__next`.
       *
       * @param {string=} listTitle    If you omit the `endpoint` by setting it to `undefined`, you need to specify
       *                               a `listTitle` instead. The service will then try to access the relevant list not
       *                               by its endpoint specification but by the given `listTitle`. Otherwise omit.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object returned by the REST API.
       *
       */

      service.getMergedFilteredItems = function(endpoint, filter, select, expand, orderby, maxRecords, nextUrl, listTitle) {
        // TODO: remove debug code
        console.log("getMergedFilteredItems for endpoint '" + endpoint + "' and filter '" + filter + "' (with select = '" + select + "', expand = '" + expand + "', orderby = '" + orderby + "', maxRecords = " + maxRecords + ", nextUrl = " + nextUrl + ")");

        var promise;
        var currMinRecords = 0;

        // maxRecords defaults to all records.
        if(maxRecords === undefined) {
          maxRecords = Infinity;
        }

        if(nextUrl) {
          // Do we have a nextUrl? Then we can use this.

          // Does the nextUrl contain an ID restriction for lists that exceed the list view threshold?
          // Then we obtain the currMinRecords from it. Otherwise we assume the list is small enough for not needing
          // the view filter.
          var viewFilter = nextUrl.match(/\(ID( |\+|%20)ge( |\+|%20)([0-9]+)( |\+|%20)and( |\+|%20)ID( |\+|%20)le( |\+|%20)([0-9]+)\)/);
          if(viewFilter) {
            currMinRecords = viewFilter[3];
          }

          // TODO: Remove debug code
          console.log('Received nextUrl with viewFilter, currMinRecords = ' + currMinRecords + '.');
          console.log(viewFilter);

          promise = $http.get(nextUrl, config.httpGetConfig);

        } else {
          // Otherwise we need to use our own logic.

          // We need to find out about the maximum ID in the list to determine currMinRecords.
          // TODO: Cache this query, as it gets called a lot and only depends on endpoint / listTitle!
          promise = service.getFilteredItems(endpoint, '', 'ID', '', 'ID desc', 1, listTitle).then(function(res) {
             if(!res.data || !res.data.d || !res.data.d.results || !Array.isArray(res.data.d.results)) {
               // TODO: Remove debug code
               console.log('Unexpected results set:');
               console.log(res);

               return $q.reject('When trying to obtain the maximum ID in the list, the result was not as expected!');
             }

             // What is the maximum ID?
             var maxID = (res.data.d.results.length > 0 ? res.data.d.results[0].Id : 0);
             var modifiedFilter;
             if(maxID > config.listViewThreshold) {
               currMinRecords = maxID - config.listViewThreshold;
               modifiedFilter = filter + ' and (ID ge ' + currMinRecords + ' and ID le ' + maxID + ')';
             } else {
               currMinRecords = 0;
               modifiedFilter = filter;
             }

             // Ok, now we can call getFilteredItems using the modified filter if necessary.
             return service.getFilteredItems(endpoint, modifiedFilter, select, expand, orderby, config.maxItemsPerRequest, listTitle);
          });
        }


        return promise.then(function(res) {
          if(!res.data || !res.data.d || !res.data.d.results || !Array.isArray(res.data.d.results)) {
            // TODO: Remove debug code
            console.log('Unexpected results set:');
            console.log(res);

            return $q.reject('When trying to retrieve results set of filtered list items, the result was not as expected!');
          }

          // Let's first construct the __next URL for further requests.
          // If there has been no __next provided, this means that the limit is not maxItemsPerRequest but maybe the
          // currMinRecords, so we need to construct our own __next if currMinRecords > 0.
          if(!res.data.d.__next && currMinRecords > 0) {
            var newMinRecords = (currMinRecords > config.listViewThreshold ? currMinRecords - config.listViewThreshold : 0);

            if(nextUrl) {
              // Start from old new URL, remove $skiptoken (if any) and replace ID view selection.
              res.data.d.__next = nextUrl.replace(/(%24|\$)skiptoken=(.+?)&/, '').replace(/\(ID( |\+|%20)ge( |\+|%20)([0-9]+)( |\+|%20)and( |\+|%20)ID( |\+|%20)le( |\+|%20)([0-9]+)\)/, '(ID+ge+' + newMinRecords + '+and+ID+le+' + (currMinRecords-1) + ')');
            } else {
              // Create new URL based on filter parameters.
              res.data.d.__next = service.getListURL(endpoint, listTitle) +
                                    '/items?$filter=' + encodeURIComponent(filter + ' and (ID ge ' + newMinRecords + ' and ID le ' + (currMinRecords-1) + ')') +
                                    (select !== undefined ? "&$select=" + encodeURIComponent(select) : "") +
                                    (expand !== undefined ? "&$expand=" + encodeURIComponent(expand) : "") +
                                    (orderby !== undefined ? "&$orderby=" + encodeURIComponent(orderby) : "") +
                                    "&$top=" + encodeURIComponent(config.maxItemsPerRequest);
            }
          }

          // Ok. Did we get maxRecords records or have we exhausted the list?
          // If yes we are done.
          if(res.data.d.results.length >= maxRecords || !res.data.d.__next) {
            // We are done.
            return res;
          }

          // If no, we need to call the __next ourselves recursively and append the results to our list...
          return service.getMergedFilteredItems(endpoint, filter, select, expand, orderby, maxRecords - res.data.d.results.length, res.data.d.__next, listTitle).then(function(resNew) {
            if(!resNew.data || !resNew.data.d || !resNew.data.d.results || !Array.isArray(resNew.data.d.results)) {
              // TODO: Remove debug code
              console.log('Unexpected results set:');
              console.log(resNew);

              return $q.reject('When trying to iteratively retrieve results, the result was not as expected!');
            }

            // Ok, we need to append the results sets.
            resNew.data.d.results = res.data.d.results.concat(resNew.data.d.results);
            return resNew;
          });

        });
      };





      /**
       * @ngdoc method
       * @name restServices.restListService#getItemsViaCAML
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method obtains an array of list elements assembled according to the instructions in the`camlXml`
       * CAML XML document. CAML can be used to prepare complex queries, such as JOINs across multiple
       * lists and filtering on elements across those lists.
       *
       * It returns a promise that resolves to the raw results object `res` as returned by
       * [`$http`](https://docs.angularjs.org/api/ng/service/$http). The relevant data will be returned in
       * `res.data.d`, which will be an array of all matching list element objects from SharePoint.
       *
       * @param {string} endpoint A string indicating the endpoint (SharePoint list) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object.
       *
       * @param {string} camlXml A valid CAML XML document that specifies the query.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object returned by the REST API.
       *
       */

      service.getItemsViaCAML = function(endpoint, camlXml) {
        // TODO: remove debug code
        console.log('getItemsViaCAML for endpoint "' + endpoint + '" and camlXml: ' + camlXml);

        // CAML requests are sent via POST and thus need a valid contextitem...
        var formDigestPromise = restBaseService.updateFormDigest(config);

        // .. and if we have both POST item to list.
        return formDigestPromise
          .then(
            function() {
              // Compile query object
              var queryObject = {
                'query': {
                  '__metadata': { 'type': 'SP.CamlQuery' },
                  'ViewXml': camlXml
                }
              };

              // ... and then POST CAML request
              return $http.post(config.apiBaseUrl + endpoints[endpoint] + '/getItems', queryObject, config.httpPostConfig);
            }
          );
      };



      /**
       * @ngdoc method
       * @name restServices.restListService#getItemById
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method returns a single list item, identified by its ID in the `itemId` parameter.
       *
       * The method returns the raw REST API result `res`, with the data of the element being encapsulated in
       * `res.data.d`.
       *
       * @param {string} endpoint A string indicating the endpoint (SharePoint list) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object.
       *
       * @param {Object} itemId The numeric ID of the list element to be retrieved.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object returned by the REST API.
       *
       */

      // Add new item to the list, given as JavaScript object "item"
      service.getItemById = function(endpoint, itemId) {
        return $http.get(
          config.apiBaseUrl + endpoints[endpoint] + '/getitembyid(' + itemId + ')',
          config.httpGetConfig
        );
      };


      /**
       * @ngdoc method
       * @name restServices.restListService#addItem
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method adds a new list element to the SharePoint list indicated by `endpoint`. The list element needs
       * to be provided as serializable JavaScript object with the required list fields as properties. The method
       * will add the required SharePoint meta data to the object prior to posting it (modifying it in the process).
       *
       * The SharePoint REST API returns the added item, including the ID it has been assigned. You can retrieve the
       * ID from the resolved promise result `res` as `res.data.d.id`.
       *
       * @param {string} endpoint A string indicating the endpoint (SharePoint list) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object. If you set this to
       *                          `undefined`, you need to specify a `listTitle` (see below).
       *
       * @param {Object} item The list element as serializable JavaScript object. The list fields that should be
       *                      populated have to be given as properties of the object.
       *
       * @param {string=} listTitle If you want to add items to a list which is not defined as a Dashboard endpoint,
       *                            you can set `endpoint` to `undefined` and specify a `listTitle` instead. The
       *                            list will then be retrieved by its name. Otherwise you can omit this parameter.
       *                            If `endpoint` is set, it will take precedence and `listTitle` will be ignored.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object returned by the REST API.
       *
       */

      // Add new item to the list, given as JavaScript object "item"
      service.addItem = function(endpoint, item, listTitle) {
        // Obtain contextitem...
        var formDigestPromise = restBaseService.updateFormDigest(config);
        
        // ... and list entity type ...
        var listTypePromise = getListItemType(endpoint, listTitle);

        // .. and if we have both POST item to list.
        return $q.all([formDigestPromise, listTypePromise]).then(
          function(res) {                        
             // Add SharePoint metadata to item
             item.__metadata = { 'type': res[1] }; // second promise returns list item type
          
            // ... and then POST item to list            
            return $http.post(service.getListURL(endpoint, listTitle) + '/items', item, config.httpPostConfig);
          }
        );
      };



      /**
       * @ngdoc method
       * @name restServices.restListService#updateItem
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method updates a list element specified by its ID `itemID` in the SharePoint list indicated by `endpoint`.
       * The list element needs to be provided as serializable JavaScript object with the required list fields as
       * properties. The method will add the required SharePoint meta data to the object prior to posting it (modifying
       * it in the process).
       *
       * The SharePoint REST API returns the modified item as stored in the list. You can retrieve it from the resolved
       * promise result `res` as `res.data.d` object.
       *
       * @param {string} endpoint A string indicating the endpoint (SharePoint list) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object. If `undefined`, you
       *                          need to specify a `listTitle` by which to identify the list instead.
       *
       * @param {number} itemID The numeric element ID of the list element to be updated.
       *
       * @param {Object} updateItem The updated list element as serializable JavaScript object. The list fields that
       *                            should be set have to be given as properties of the object.
       *
       * @param {string=} listTitle If you set `endpoint` to `undefined`, you can specify a `listTitle` instead. The
       *                            service will then try to access the SharePoint list by its title.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object returned by the REST API.
       *
       */

      service.updateItem = function(endpoint, itemID, updateItem, listTitle) {
        // Obtain contextitem...
        var formDigestPromise = restBaseService.updateFormDigest(config);

        // ... and list entity type ...
        var listTypePromise = getListItemType(endpoint, listTitle);

        // .. and if we have both PUT item to list.
        return $q.all([formDigestPromise, listTypePromise]).then(
          function(res) {
            // Add SharePoint metadata to item
            updateItem.__metadata = { 'type': res[1] }; // second promise returns list item type

            // ... and then POST item to list
            return $http.post(service.getListURL(endpoint, listTitle) + '/items(' + itemID + ')', updateItem, config.httpPutConfig);
          }
        );
      };




      /**
       * @ngdoc method
       * @name restServices.restListService#updateFilteredItems
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method updates list elements identified by the given OData `searchQuery`. All matching list elements will
       * be updated with the new data as provided by the serializable JavaScript object with the required list fields as
       * properties. The method will add the required SharePoint meta data to the object prior to posting it (modifying
       * it in the process).
       *
       * The SharePoint REST API returns the modified item as stored in the list. You can retrieve it from the resolved
       * promise result `res` as `res.data.d` object.
       *
       * @param {string} endpoint A string indicating the endpoint (SharePoint list) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object. If `undefined`, you
       *                          need to specify a `listTitle` by which to identify the list instead.
       *
       * @param {string} searchQuery The OData search query to identify all elements to be updated.
       *
       * @param {Object} updateItem The updated list element as serializable JavaScript object. The list fields that should be
       *                            set have to be given as properties of the object.
       *
       * @param {string=} listTitle If you set `endpoint` to `undefined`, you can specify a `listTitle` instead. The
       *                            service will then try to access the SharePoint list by its title.
       *
       * @returns {Promise} A promise which resolves when all elements have been correctly updated.
       *
       */

      service.updateFilteredItems = function(endpoint, searchQuery, updateItem, listTitle) {
        return service
          .getMergedFilteredItems(endpoint, searchQuery, 'Id', undefined, 'Id', undefined, undefined, listTitle)
          .then(function(res) {
            var myPromise = $q.resolve();

            if(!res.data || !res.data.d || !res.data.d.results || !Array.isArray(res.data.d.results)) {
              return $q.reject(gettext('An error occured while trying to update filtered items: The returned results set was not in the expected format.'));
            }

            res.data.d.results.forEach(function(item) {
              myPromise = myPromise.then(function() {
                return service.updateItem(endpoint, item.Id, updateItem, listTitle);
              });
            });

            return myPromise;
          });
      };



      /**
       * @ngdoc method
       * @name restServices.restListService#addAttachment
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method adds an attachment to a list item specified by its ID `itemID` in the list referenced by
       * `endpoint`. You need to pass the `fileName` the file should be given and a reference to the file itself
       * as returned by the HTML5 file selectors (the method will read the file using `FileReader`).
       *
       * @param {string} endpoint A string indicating the endpoint (SharePoint list) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object.
       *
       * @param {number} itemID The numeric element ID of the list element to be updated.
       *
       * @param {string} fileName The name with which the file should be stored on SharePoint.
       *
       * @param {Object} file A reference to the file as returned by the HTML5 file selectors.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object returned by the REST API.
       *
       */

      service.addAttachment = function(endpoint, itemID, fileName, file) {
        // Create new promise
        var deferred = $q.defer();

        // Asynchronously read file uploaded by user
        var reader = new FileReader();

        reader.onloadend = function () {
          if(reader.error) {
            deferred.reject({
              status: -1,
              statusText: reader.error.message
            });
          } else {

            // TODO: progress notifications

            // Obtain contextitem...
            restBaseService.updateFormDigest(config)
              .then(
                function() {
                  return $http.post(config.apiBaseUrl + endpoints[endpoint] + "/items(" + itemID + ")/AttachmentFiles/add(FileName='" + fileName.replace("'", "''") + "')", reader.result, config.httpUploadConfig);
                }
              )
              .then(
                function (res) {
                  deferred.resolve(res);
                }, function (restErr) {
                  deferred.reject(restErr);
                }
            );
          }
        };

        // Read the file
        reader.readAsArrayBuffer(file);

        // Return promise
        return deferred.promise;
      };



/**
       * @ngdoc method
       * @name restServices.restListService#addAttachmentFromString
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method adds an attachment to a list item specified by its ID `itemID` in the list referenced by
       * `endpoint`. You need to pass the `content` string that should be written to the file.
       *
       * @param {string} endpoint A string indicating the endpoint (SharePoint list) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object.
       *
       * @param {number} itemID The numeric element ID of the list element to be updated.
       *
       * @param {string} fileName The name with which the file should be stored on SharePoint.
       *
       * @param {Object} content The string with content that should be written to the file.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object returned by the REST API.
       *
       */

      service.addAttachmentFromString = function(endpoint, itemID, fileName, content) {
        return restBaseService.updateFormDigest(config)
              .then(
                function() {
                  return $http.post(config.apiBaseUrl + endpoints[endpoint] + "/items(" + itemID + ")/AttachmentFiles/add(FileName='" + fileName.replace("'", "''") + "')", content, config.httpUploadConfig);
                }
             );
      };





      /**
       * @ngdoc method
       * @name restServices.restListService#removeAttachment
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method removes the attachment given by its stored file name `fileName` from the list item specified by
       * its ID `itemID` in the list referenced by `endpoint`.
       *
       * @param {string} endpoint A string indicating the endpoint (SharePoint list) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object.
       *
       * @param {number} itemID The numeric element ID of the list element to be updated.
       *
       * @param {string} fileName The name with which the file is stored on SharePoint.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object returned by the REST API.
       *
       */

      service.removeAttachment = function(endpoint, itemID, fileName) {
        // Obtain contextitem...
        return restBaseService.updateFormDigest(config)
          .then(
            function() {
              // ... then DELETE attachment.
              return $http.post(config.apiBaseUrl + endpoints[endpoint] + "/items(" + itemID + ")/AttachmentFiles('" + fileName.replace("'", "''") + "')", '', config.httpDeleteConfig);
            }
          );
      };




      /**
       * @ngdoc method
       * @name restServices.restListService#deleteItem
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method removes a list element referenced by its ID `itemID` from the SharePoint list referenced by
       * `endpoint`.
       *
       * @param {string} endpoint A string indicating the endpoint (SharePoint list) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object. If `undefined`,
       *                          you need to specify a `listTitle` for the list to be accessed instead.
       *
       * @param {number} itemID The numeric element ID of the list element to be removed.
       *
       * @param {string=} listTitle If you set `endpoint` to `undefined`, you can specify a `listTitle` instead. The
       *                            service will then try to access the SharePoint list by its title.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object returned by the REST API.
       *
       */

      service.deleteItem = function(endpoint, itemID, listTitle) {
        // Obtain contextitem...
        return restBaseService.updateFormDigest(config)

        // ... then DELETE from the list.
          .then(
            function() {
              // ... and then POST item to list (we RECYCLE instead of delete)
              return $http.post(service.getListURL(endpoint, listTitle) + '/items(' + itemID + ')/recycle()', '', config.httpPostConfig);
            }
          );
      };




      /**
       * @ngdoc method
       * @name restServices.restListService#deleteFilteredItems
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method removes list elements specified by an OData `searchQuery` from the SharePoint list specified
       * by its `endpoint` or `listTitle`.
       *
       * @param {string} endpoint A string indicating the endpoint (SharePoint list) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object. If `undefined`,
       *                          you need to specify a `listTitle` for the list to be accessed instead.
       *
       * @param {string} searchQuery A valid OData search query to specify the list items to be removed.
       *
       * @param {string=} listTitle If you set `endpoint` to `undefined`, you can specify a `listTitle` instead. The
       *                            service will then try to access the SharePoint list by its title.
       *
       * @returns {Promise} A promise which resolves when all specified items have been removed.
       *
       */

      service.deleteFilteredItems = function(endpoint, searchQuery, listTitle) {
        return service
          .getMergedFilteredItems(endpoint, searchQuery, 'Id', undefined, 'Id', undefined, undefined, listTitle)
          .then(function(res) {
            var myPromise = $q.resolve();

            if(!res.data || !res.data.d || !res.data.d.results || !Array.isArray(res.data.d.results)) {
              return $q.reject(gettext('An error occured while trying to delete filtered items: The returned results set was not in the expected format.'));
            }

            res.data.d.results.forEach(function(item) {
              myPromise = myPromise.then(function() {
                return service.deleteItem(endpoint, item.Id, listTitle);
              });
            });

            return myPromise;
          });
      };




      /**
       * @ngdoc method
       * @name restServices.restListService#getAdditionalData
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * The SharePoint REST API provides additional URLs to request further data on individual list items. This
       * method allows to access these additional data points, returning a promise that resolves to the raw REST API
       * response.
       *
       * The method requires the SharePoint list given in `endpoint` the numeric ID of the list element for which the
       * data should be retrieved in `itemID` and a string with the additional path to be retrieved in `additionalData`.
       *
       * The most common use case for this method is with `additionalData = 'AttachmentFiles'`, which will return an
       * array in `res.data.d` of which each element is a JavaScript object with details on each file attached to this
       * list element.
       *
       * @param {string} endpoint A string indicating the endpoint (SharePoint list) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object.
       *
       * @param {number} itemID The numeric element ID of the list element for which additional data is to be retrieved.
       *
       * @param {string} additionalData A string specifying the additional URL path to be appended to the list item.
       *                                If set to `'AttachmentFiles'` retrieves an array of files attached to the
       *                                list element.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */

      service.getAdditionalData = function(endpoint, itemID, additionalData) {
        // Returns a promise that deals with getting all items
        return $http.get(config.apiBaseUrl + endpoints[endpoint] + '/items(' + itemID + ')/' + additionalData, config.httpGetConfig);
      };



      /**
       * @ngdoc method
       * @name restServices.restListService#getListInfo
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method returns details on a SharePoint list specified by its endpoint in `endpoint` or alternatively by
       * its title in `listTitle` (setting `endpoint` to `undefined`). The method returns a
       * promise that resolves to the raw data object `res` returned from the REST API query. The details of the
       * list item will be contained in the object `res.data.d`.
       *
       * @param {string} endpoint A string indicating the endpoint (SharePoint list) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object.
       *
       * @param {string=} listTitle If you set `endpoint` to `undefined`, you can specify a `listTitle` instead. The
       *                            service will then try to access the SharePoint list by its title.
       *
       * @param {string=} listGUID If you leave both `endpoint` and `listTitle` as `undefined`, you can alternatively
       *                           also specify the list by its GUID.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */

      service.getListInfo = function(endpoint, listTitle, listGUID) {
        return $http.get(service.getListURL(endpoint, listTitle, listGUID), config.httpGetConfig);
      };



      /**
       * @ngdoc method
       * @name restServices.restListService#getListFields
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method provides access to a list of all fields defined on the list specified by its endpoint in `endpoint`
       * or alternatively by its title in `listTitle` (setting `endpoint` to `undefined`). The method returns a promise
       * that resolves to the raw data object `res` returned from the REST API query. The array of all list items (each
       * as an object with its detailed properties) will be contained as `res.data.d`.
       *
       * Note that a SharePoint list contains a lot of technical fields not manually defined or used but returned by
       * this API call.
       *
       * @param {string} endpoint A string indicating the endpoint (SharePoint list) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object.
       *
       * @param {string=} listTitle If you set `endpoint` to `undefined`, you can specify a `listTitle` instead. The
       *                            service will then try to access the SharePoint list by its title.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */

      service.getListFields = function(endpoint, listTitle) {
        return $http.get(service.getListURL(endpoint, listTitle) + '/Fields', config.httpGetConfig);
      };




      /**
       * @ngdoc method
       * @name restServices.restListService#createList
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method creates a new SharePoint list. You need to provide the details of the list as `listObject`, which
       * is a JavaScript object with the following two properties:
       *
       *  * `Title`: The name of the list to be added.
       *  * `Description`: A longer description of the list.
       *  * `EnableVersioning`: Can be set to true if SharePoint should store versions for list items
       *
       * SharePoint knows more properties for lists, but this method will only transfer the ones detailed above.
       *
       * @param {Object} listObject A JavaScript object with the two properties `Title` (name of the list) and
       *                            `Description`.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */

      service.createList = function(listObject) {
        // Format list object for SharePoint
        var sharepointList = {
          '__metadata': { 'type': 'SP.List' },
          'Title': listObject.Title,
          'Description': listObject.Description,
          'AllowContentTypes': true,
          'BaseTemplate': 100,
          'ContentTypesEnabled': false,
          'EnableVersioning': (!!listObject.EnableVersioning)
        };

        // Obtain contextitem...
        return restBaseService.updateFormDigest(config)

        // ... then PUT the list.
          .then(
            function() {
              return $http.post(config.apiBaseUrl + 'web/lists', sharepointList, config.httpPostConfig);
            }
          );
      };



      /**
       * @ngdoc method
       * @name restServices.restListService#updateListField
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method updates an existing or creates a new field (depending on whether `oldTitle` is set) for an existing
       * SharePoint list identified by its name `listName`. You need to specify the details of the field you want to
       * create or update as a JavaScript object `fieldObject` with the following properties:
       *
       *  * `Title`: The name of the new field.
       *
       *  * `FieldTypeKind`: A recognized numeric value indicating the type of field to be created/updated (see below).
       *
       *  * `Required`: A string set to `'true'` or `'false'`. If `'true'`, this field needs to be filled for every
       *                list element.
       *
       *  * `EnforceUniqueValues`: A string set to `'true'` or `'false'`. If `'true'`, every value in this field needs
       *                           to be unique, i.e. there may not be two list elements with the same value in this
       *                           field.
       *
       *  * `StaticName`: The SharePoint-internal name of the field. Best to use the same as Title.
       *
       *  * `DefaultValue`: The default value for this field new list elements which do not specify an explicit value
       *                    for this field.
       *
       *  The method recognizes the following `FieldTypeKind` values:
       *
       *  * 2: Simple, one-line text field
       *  * 3: Multi-line text field (optionally rich text)
       *
       *  * 4: Date and time field
       *
       *  * 6: Choice field, where the user can select a value from a pre-determined list of possible values
       *
       *  * 7: Lookup field, where the user can select a value from some other SharePoint list
       *
       *  * 8: Yes/No field
       *
       *  * 9: Number field
       *
       *
       *  For multi-line text fields (`FieldTypeKind = 3`) the following additional properties can be specified:
       *
       *  * `RichText`: A string set to `'true'` or `'false'`. If `'true'`, this field can be formatted using
       *                SharePoint's rich-text formatting. (Note that The Dashboard uses its own editor and thus
       *                works with non-rich-text fields.)
       *
       *  * `NumberOfLines`: The number of lines to be displayed in the standard list entry form for this text field.
       *                     If the user enters longer text, a scrollbar appears.
       *
       *
       *  For date and time fields (`FieldTypeKind = 4`) the following additional properties can be specified:
       *
       *  * `DisplayFormat`: A numeric value specifying whether only the date or date and time should be displayed.
       *
       *
       *  For choice fields the following additional properties can be specified:
       *
       *  * `FillInChoice`: A string set to `'true'` or `'false'`. If `'true'`, the user can either choose between the
       *                    pre-defined values or enter a custom value. If `'false'`, one of the pre-defined values
       *                    has to be used.
       *
       *  * `Choices`: An array containing the different values the user can choose from.
       *
       *
       *  For lookup fields the following additional properties need to be specified:
       *
       *  * `LookupListName`: The name of the SharePoint list where the possible values should be looked up and
       *                      which contains the `lookupField`.
       *
       *  * `LookupField`: The name of the field in the list specified by `LookupListName` which should be used to
       *                   select the value for the lookup column.
       *
       *  * `LookupOtherColumns`: An array which can contain the field names of additional fields from the list given
       *                          by `lookupListName`. Those fields are then included in the list with the lookup
       *                          field and automatically populated based on the selection in the lookup field.
       *
       *                          Note that if you specify elements in this array for an existing field to be updated,
       *                          these columns will be added *in addition* to the existing dependent lookup columns.
       *
       *  * `RelationshipDeleteBehavior`: If set to `1`, will "cascade" deletes from the target list. (Requires
       *                                  `Indexed` to be set to `true`.)
       *
       *
       * SharePoint knows more field types and parameters but this method will only accept the ones listed above.
       *
       *
       * @param {string} listName A string giving the name of the SharePoint list to be queried. _Note that this method
       *                          expects the list name and not the endpoint definition!_ This is because this function
       *                          should also be used for lists not part of the application's endpoint definition.
       *
       * @param {Object} fieldObject A JavaScript object with the required properties for the field to be added.
       *
       * @param {string=} oldTitle If `oldTitle` is set, this function will update the existing field with that title. If
       *                           this parameter is not set, the function will create a new field.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */

      service.updateListField = function(listName, fieldObject, oldTitle) {
        // Format field object for SharePoint
        var sharepointListField = {
          '__metadata': { 'type': 'SP.Field' },
          'Title': fieldObject.Title,
          'FieldTypeKind': fieldObject.FieldTypeKind,
          'Required': fieldObject.Required,
          'Indexed': fieldObject.Indexed,
          'EnforceUniqueValues': fieldObject.EnforceUniqueValues,
          'StaticName': fieldObject.StaticName,
          'DefaultValue': fieldObject.DefaultValue
        };

        // SharePoint bug: The "UnlimitedLengthInDocumentLibrary" property cannot be set or
        // retrieved directly via the REST API, but needs to be set by modifying the
        // XML schema of the field afterwards
        var needToSetUnlimited = (!!fieldObject.UnlimitedLengthInDocumentLibrary);

        if(fieldObject.FieldTypeKind === 2) {
          sharepointListField.__metadata.type = 'SP.FieldText';
        }

        if(fieldObject.FieldTypeKind === 3) {
          // Multi-line text field
          sharepointListField.__metadata.type = 'SP.FieldMultiLineText';
          sharepointListField.RichText = (fieldObject.RichText === 'true' ? 'true' : 'false');
          sharepointListField.NumberOfLines = (fieldObject.NumberOfLines ? fieldObject.NumberOfLines : 6);
        }

        if(fieldObject.FieldTypeKind === 4) {
          // Date and time field
          sharepointListField.__metadata.type = 'SP.FieldDateTime';
          sharepointListField.DisplayFormat = (fieldObject.DisplayFormat !== undefined ? fieldObject.DisplayFormat : 1);
        }

        if(fieldObject.FieldTypeKind === 6) {
          // Choice field
          sharepointListField.__metadata.type = 'SP.FieldChoice';
          sharepointListField.FillInChoice = (fieldObject.FillInChoice === 'true' ? 'true' : 'false');
          sharepointListField.Choices = {'results': fieldObject.Choices};
        }

        if(fieldObject.FieldTypeKind === 8) {
          // Number field
          sharepointListField.__metadata.type = 'SP.Field';
        }

        if(fieldObject.FieldTypeKind === 9) {
          // Number field
          sharepointListField.__metadata.type = 'SP.FieldNumber';
        }

        if(fieldObject.FieldTypeKind === 20) {
          // User selection field
          sharepointListField.__metadata.type = 'SP.FieldUser';
          sharepointListField.SelectionMode = (fieldObject.SelectionMode === 1 ? 1 : 0);  // 0 = User only
        }


        if(fieldObject.FieldTypeKind === 7) {
          // Lookup field
          // For lookup fields, we need to query the API multiple times, which is why we treat those separately.
          // See https://msdn.microsoft.com/en-us/library/office/dn600182.aspx#bk_FieldLookup

          var obtainedId;
          var currPromise;

          if(oldTitle) {
            // Update existing field - note that this cannot change the lookup list ID!
            currPromise = restBaseService.updateFormDigest(config)
              .then(
                function () {
                  return $http.post(config.apiBaseUrl + 'web/lists/' + encodeURIComponent("getbytitle('" + listName + "')") + '/Fields/' + encodeURIComponent("getbytitle('" + oldTitle + "')"), fieldObject, config.httpPutConfig);
                }
              );

          } else {
            // Create new field - with advanced logic to obtain lookup list ID to be associated
            // Also, we need to use the "AddField" method of the API.
            var addFieldParameters = {
              '__metadata': { 'type': 'SP.FieldCreationInformation' },
              'Title': fieldObject.Title,
              'FieldTypeKind': fieldObject.FieldTypeKind,
              'Required': fieldObject.Required,
              'LookupFieldName': fieldObject.LookupField
            };

            // Get list GUID
            currPromise = service.getListInfo(undefined, fieldObject.LookupListName)
              .then(
                function (res) {
                  addFieldParameters.LookupListId = res.data.d.Id;
                }
              )

              // Obtain contextitem...
              .then(function () {
                return restBaseService.updateFormDigest(config);
              })

              // ... then POST the lookup field AddField request to the list (if new) or update
              //     field via PUT if updating existing field.
              .then(
                function () {
                  return $http.post(config.apiBaseUrl + 'web/lists/' + encodeURIComponent("getbytitle('" + listName + "')") + '/Fields/AddField', {'parameters': addFieldParameters}, config.httpPostConfig);
                }
              );
          }

          currPromise = currPromise
            // Store the ID of the new field.
            .then(
              function(res) {
                // TODO: Remove debug code
                console.log('We obtained data from the  Sharepoint field: ');
                console.log(res);

                obtainedId = res.data.d.Id;
              }
            );

          // Additional columns to be displayed?
          fieldObject.LookupOtherColumns.forEach(function(col) {
            currPromise = currPromise.then(
              function() {
                // POST to AddDependentLookupField
                // See: https://msdn.microsoft.com/en-us/library/office/dn600182.aspx#bk_FieldCollectionAddDependentLookupField
                return $http.post(config.apiBaseUrl + 'web/lists/' + encodeURIComponent("getbytitle('" + listName + "')") + '/Fields/adddependentlookupfield' + encodeURIComponent("(displayname='" + fieldObject.Title + '_' + col + "', primarylookupfieldid='" + obtainedId + "', showfield='" + col + "')"), '', config.httpPostConfig);
              }
            );
          });


          // RelationshipDeleteBehavior or Indexed to be set?
          if(fieldObject.Indexed || fieldObject.RelationshipDeleteBehavior) {
            currPromise = currPromise.then(
              function() {
                var updateObject = {
                  '__metadata': { 'type': 'SP.FieldLookup' },
                  'Indexed': true,
                  'RelationshipDeleteBehavior': fieldObject.RelationshipDeleteBehavior
                };

                return $http.post(config.apiBaseUrl + 'web/lists/' + encodeURIComponent("getbytitle('" + listName + "')") + '/Fields(\'' + obtainedId + '\')', updateObject, config.httpPutConfig);
                }
            );
          }


          return currPromise;

        } else {
          // No lookup field, so we can take the quick route.

          // Obtain contextitem...
          var returnPromise = restBaseService.updateFormDigest(config)

          // ... then PUT or POST the list item.
            .then(
              function() {
                return $http.post(
                  config.apiBaseUrl + 'web/lists/' + encodeURIComponent("getbytitle('" + listName + "')") + '/Fields' + (oldTitle ? '/' + encodeURIComponent("getbytitle('" + oldTitle + "')") : ''),
                  sharepointListField,
                  (oldTitle ? config.httpPutConfig : config.httpPostConfig)
                );
              }
            );

          // Do we need to modify the list item to set UnlimitedLengthInDocumentLibrary?
          if(fieldObject.FieldTypeKind === 3 && needToSetUnlimited) {
            var originalResponse;
            var newSchema;

            returnPromise = returnPromise
              .then(function(res) {
                originalResponse = res;

                // We replace the XML schema
                newSchema = originalResponse.data.d.SchemaXml.replace('/>', ' UnlimitedLengthInDocumentLibrary="TRUE" />');

                // The update object to be POSTed
                var updateObject = { "__metadata": sharepointListField.__metadata, "SchemaXml": newSchema };

                // TODO: Remove debug code
                console.log('Updating schema for ' + sharepointListField.Title + ': ' + newSchema);
                return $http.post(config.apiBaseUrl + 'web/lists/' + encodeURIComponent("getbytitle('" + listName + "')") + '/Fields/' + encodeURIComponent("getbytitle('" + sharepointListField.Title + "')"), updateObject, config.httpPutConfig);
              })
              .then(function() {
                // Success! We return the original response for further processing.
                originalResponse.data.d.SchemaXml = newSchema;
                return originalResponse;
              });
          }

          return returnPromise;
        }
      };


      /**
       * @ngdoc method
       * @name restServices.restListService#createListField
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method creates a new field for an existing SharePoint list identified by its name `listName`. You need
       * to specify the details of the field you want to create as a JavaScript object `fieldObject`. For details
       * refer to the documentation of the {@link restServices.restListService#updateListField `updatedListField`}
       * method.
       *
       * @param {string} listName A string giving the name of the SharePoint list to be queried. _Note that this method
       *                          expects the list name and not the endpoint definition!_ This is because this function
       *                          should also be used for lists not part of the application's endpoint definition.
       *
       * @param {Object} fieldObject A JavaScript object with the required properties for the field to be added.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */

      service.createListField = function(listName, fieldObject) {
        return service.updateListField(listName, fieldObject);
      };




      /**
       * @ngdoc method
       * @name restServices.restListService#deleteListField
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method deletes a field from the list given by its name `listName`. You need to provide the GUID of the
       * field in `fieldId`, which you can obtain for example using the
       * {@link restServices.restListService#getListFields `getListFields`} method.
       *
       * @param {string} listName A string giving the name of the SharePoint list to be queried. _Note that this method
       *                          expects the list name and not the endpoint definition!_ This is because this function
       *                          should also be used for lists not part of the application's endpoint definition.
       *
       * @param {string} fieldId The SharePoint GUID of the field to be removed.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */

      service.deleteListField = function(listName, fieldId) {
        // Obtain contextitem...
        return restBaseService.updateFormDigest(config)

        // ... then DELETE the list.
          .then(
            function() {
              return $http.post(config.apiBaseUrl + 'web/lists/' + encodeURIComponent("getbytitle('" + listName + "')") + '/Fields' + encodeURIComponent("(guid'" + fieldId  + "')"), '', config.httpDeleteConfig);
            }
          );
      };


      /**
       * @ngdoc method
       * @name restServices.restListService#deleteListFieldByTitle
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method deletes a field from the list given by its name `listName`. This method deletes a field directly
       * by its title, without requiring you to obtain the GUID.
       *
       * @param {string} listName A string giving the name of the SharePoint list to be queried. _Note that this method
       *                          expects the list name and not the endpoint definition!_ This is because this function
       *                          should also be used for lists not part of the application's endpoint definition.
       *
       * @param {string} fieldTitle The title of the field to be removed.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */

      service.deleteListFieldByTitle = function(listName, fieldTitle) {
        // Obtain contextitem...
        return restBaseService.updateFormDigest(config)

        // ... then DELETE the list.
          .then(
            function() {
              return $http.post(config.apiBaseUrl + 'web/lists/' + encodeURIComponent("getbytitle('" + listName + "')") + '/Fields/' + encodeURIComponent("getbytitle('" + fieldTitle + "')"), '', config.httpDeleteConfig);
            }
          );
      };


      /**
       * @ngdoc method
       * @name restServices.restListService#deleteList
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method deletes a SharePoint list given by its name `listName`. Note that SharePoint does not allow to
       * delete lists which are linked to lookup fields in use elsewhere. You first need to remove the lookup fields
       * in question before you can delete the list.
       *
       * @param {string} listName A string giving the name of the SharePoint list to be deleted. _Note that this method
       *                          expects the list name and not the endpoint definition!_ This is because this function
       *                          should also be used for lists not part of the application's endpoint definition.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */

      service.deleteList = function(listName) {
        // Obtain contextitem...
        return restBaseService.updateFormDigest(config)

        // ... then DELETE the list.
          .then(
            function() {
              return $http.post(config.apiBaseUrl + 'web/lists/' + encodeURIComponent("getbytitle('" + listName + "')"), '', config.httpDeleteConfig);
            }
          );
      };

      return service;
    }];

    return provider;
  })


  /**
   * @ngdoc service
   * @name restServices.restUserService
   * @description
   *
   * This service allows to query properties of the existing SharePoint users and their access rights.
   *
   */

  .provider('restUserService', function() {
    var provider  = {};
    var endpoints = {};
    var config    = {};


    provider.doConfig = function(endpointsParam, configParam) {
      endpoints = endpointsParam;
      config = configParam;
    };

    // Return provided services
    provider.$get = ['$http', '$q', 'restListService', function($http, $q, restListService) {
      var service = {};


      /**
       * @ngdoc method
       * @name restServices.restUserService#getUserById
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method returns the details of the user identified by the given `userId`. The method returns a promise that
       * resolves to the raw results `res` returned by the REST API. The details of the specified user are contained in
       * the `res.data.d` object.
       *
       * @param {number} userId The numeric ID of the user whose details are to be retrieved.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */

      var getUserByIdPromises = {};
      service.getUserById = function(userId) {
        if(isNaN(userId)) {
          // Invalid user ID, return rejected promise
          var deferred = $q.defer();
          deferred.reject({status: -1, statusText: 'Invalid user ID provided'});
        } else {
          // Only create new promise if not yet available
          if(!getUserByIdPromises[userId.toString()]) {
            getUserByIdPromises[userId.toString()] = $http.get(config.apiBaseUrl + 'web/GetUserById(' + userId + ')', config.httpGetConfig);
          }

          return getUserByIdPromises[userId.toString()];
        }
      };


      /**
       * @ngdoc method
       * @name restServices.restUserService#getUsersByGroup
       * @methodOf restServices.restUserService
       * @kind function
       *
       * @description
       * This method returns all users which are members of the group identified by `groupName`. 
       *
       * @param {String=} groupName  The name of the group whose members should be retrieved. 
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */


      service.getUsersByGroup = function(groupName) {
        return $http.get(config.apiBaseUrl + 'web/SiteGroups/GetByName(\'' + encodeURIComponent(groupName) + '\')/Users', config.httpGetConfig);
      };

      
      /**
       * @ngdoc method
       * @name restServices.restUserService#getAllUsers
       * @methodOf restServices.restUserService
       * @kind function
       *
       * @description
       * This method returns all users on The Dashboard's SharePoint site.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */

      service.getAllUsers = function() {
        return $http.get(config.apiBaseUrl + 'web/SiteUsers', config.httpGetConfig);
      };

      


      /**
       * @ngdoc method
       * @name restServices.restUserService#getCurrentUser
       * @methodOf restServices.restListService
       * @kind function
       *
       * @description
       * This method returns the details of the currently logged in user. The method returns a promise that resolves
       * to the raw results `res` returned by the REST API. The details of the logged in user are contained in the
       * `res.data.d` object.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */

      var currentUserPromise;
      service.getCurrentUser = function() {
        // If we already have a promise to get the current user, we will use that one
        if(!currentUserPromise) {
          currentUserPromise = $http.get(config.apiBaseUrl + 'web/currentUser', config.httpGetConfig);
        }

        return currentUserPromise;
      };



      /**
       * @ngdoc method
       * @name restServices.restUserService#getEffectiveBasePermissions
       * @methodOf restServices.restUserService
       * @kind function
       *
       * @description
       * This method returns the effective base permissions of the currently logged in user for a list specified by
       * the given `listName`. The method returns a promise that resolves to the raw results `res` returned by the REST
       * API. The base permissions are contained in the `res.data.d.EffectiveBasePermissions.High` and
       * `res.data.d.EffectiveBasePermissions.Low` values, see
       * [this list](http://web.archive.org/web/20130904153031/http://www.dctmcontent.com/sharepoint/Articles/Permissions%20and%20Mask%20Values.aspx}
       * for the available permission levels.
       *
       * @param {string} endpoint The endpoint to which access by the logged in user should be checked. Set to `undefined`
       *                          if you want to give a `listName` instead.
       *
       * @param {string} listName The name of the list to which access by the logged in user should be checked. Leave as
       *                          `undefined` if you have given an endpoint instead.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */

      var getEffectiveBasePermPromises = {};
      service.getEffectiveBasePermissions = function(endpoint, listName) {
        if(!getEffectiveBasePermPromises[endpoint + '_' + listName]) {
          if(endpoint) {
            getEffectiveBasePermPromises[endpoint + '_' + listName] = $http.get(restListService.getListURL(endpoint, listName) + '/EffectiveBasePermissions', config.httpGetConfig);
          } else {
            getEffectiveBasePermPromises[endpoint + '_' + listName] = $http.get(restListService.getListURL(endpoint, listName) + '/EffectiveBasePermissions', config.httpGetConfig);
          }
        }

        return getEffectiveBasePermPromises[endpoint + '_' + listName];
      };


      /**
       * @ngdoc method
       * @name restServices.restUserService#getSiteUserEffectivePermissions
       * @methodOf restServices.restUserService
       * @kind function
       *
       * @description
       * This method returns the effective user base permissions of the currently logged in user for the overall
       * SharePoint site. The method returns a promise that resolves to the raw results `res` returned by the REST
       * API. The base permissions are contained in the `res.data.d.GetUserEffectivePermissions.High` and
       * `res.data.d.GetUserEffectivePermissions.Low` values, see
       * [this list](http://web.archive.org/web/20130904153031/http://www.dctmcontent.com/sharepoint/Articles/Permissions%20and%20Mask%20Values.aspx}
       * for the available permission levels.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */

      var getSiteUserEffectivePermsPromise;
      service.getSiteUserEffectivePermissions = function() {
        if(!getSiteUserEffectivePermsPromise) {
          getSiteUserEffectivePermsPromise = service.getCurrentUser().then(function(res) {
            // Did we get the right data?
            if(!res || !res.data || !res.data.d || res.data.d.LoginName === undefined) {
              // TODO: Remove debug code
              console.log('WRONG RESPONSE RECEIVED:');
              console.log(res);

              return $q.reject({status: -1, statusText: 'Retrieved user data is invalid'});
            }

            // Identify the current user user name
            var myUser = res.data.d.LoginName;

            // Obtain the UserEffectivePermissions for this user
            return $http.get(config.apiBaseUrl + 'web/GetUserEffectivePermissions(@v)?@v=\'' + encodeURIComponent(myUser) + '\'', config.httpGetConfig);
          });
        }

        return getSiteUserEffectivePermsPromise;
      };

      return service;
    }];

    return provider;
  })


  /**
   * @ngdoc service
   * @name restServices.restDocumentService
   * @description
   *
   * This service allows to add, edit and remove documents from document libraries and to add and remove SharePoint
   * document librarires. It uses the pre-configured SharePoint endpoints (lists and libraries to be used by the
   * Dashboard) to access the REST API.
   *
   */

  .provider('restDocumentService', function() {
    var provider         = {};
    var endpoints        = {};
    var endpointsFolders = {};
    var config           = {};

    provider.doConfig = function(endpointsParam, endpointsFoldersParam, configParam) {
      endpoints = endpointsParam;
      endpointsFolders = endpointsFoldersParam;
      config = configParam;
    };


    // Return provided services
    provider.$get = ['$http', '$q', 'gettext', 'restBaseService', 'restListService', function($http, $q, gettext, restBaseService, restListService) {
      var service = {};

      /**
       * @ngdoc method
       * @name restServices.restDocumentService#getAllFiles
       * @methodOf restServices.restDocumentService
       * @kind function
       *
       * @description
       * This method obtains all items for the document library specified by the `endpoint` parameter. It returns a
       * promise that resolves to the raw results object `res` as returned by
       * [`$http`](https://docs.angularjs.org/api/ng/service/$http). The relevant data will be returned in
       * `res.data.d`, which will be an array of all document library objects from SharePoint.
       *
       * @param {string} endpoint A string indicating the endpoint (document library) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object returned by the REST API.
       *
       */

      service.getAllFiles = function(endpoint) {
        // Returns a promise that deals with getting all files
        return $http.get(config.apiBaseUrl + endpoints[endpoint] + '/items',
          config.httpGetConfig);
      };




      /**
       * @ngdoc method
       * @name restServices.restDocumentService#getFileById
       * @methodOf restServices.restDocumentService
       * @kind function
       *
       * @description
       * This method returns a single document library item, identified by its ID in the `fileId` parameter.
       *
       * The method returns the raw REST API result `res`, with the data of the element being encapsulated in
       * `res.data.d`.
       *
       * @param {string} endpoint A string indicating the endpoint (document library) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object.
       *
       * @param {Object} fileId The numeric ID of the list element to be retrieved.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object returned by the REST API.
       *
       */

      service.getFileById = function(endpoint, itemId) {
        return $http.get(
          config.apiBaseUrl + endpoints[endpoint] + '/getitembyid(' + itemId + ')',
          config.httpGetConfig
        );
      };


      /**
       * @ngdoc method
       * @name restServices.restDocumentService#getFileByFilename
       * @methodOf restServices.restDocumentService
       * @kind function
       *
       * @description
       * This method returns a single document library item, identified by its filename in the `fileName` parameter.
       *
       * The method returns the raw REST API result `res`, with the data of the element being encapsulated in
       * `res.data.d`.
       *
       * @param {string} endpoint A string indicating the endpoint (document library) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object.
       *
       * @param {Object} fileName  The filename of the file to be retrieved.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object returned by the REST API.
       *
       */

      service.getFileByFilename = function(endpoint, fileName) {
        return $http.get(
          config.apiBaseUrl + endpointsFolders[endpoint] + '/Files%28\'' + fileName + '\'%29/ListItemAllFields',
          config.httpGetConfig
        );
      };



      /**
       * @ngdoc method
       * @name restServices.restDocumentService#expandFilePath
       * @methodOf restServices.restDocumentService
       * @kind function
       *
       * @description
       * This method returns the relative URL of a document based on this document's deferred "File" URI as returned
       * when querying the document through the SharePoint List API (as the `getFileById`, `getAllFiles` methods do).
       *
       * @param {string} deferredUri The deferred File URI as returned by the SharePoint List API in the value
       *                             `File.__deferred.uri`.
       *
       * @returns {Promise} A promise which resolves to an array that contains two elements:
       *                    a) the relative server URL of the given file, b) the filename of the file.
       *
       */

      service.expandFilePath = function(deferredUri) {
        return $http
          .get(
            deferredUri,
            config.httpGetConfig
          )
          .then(
            function(res) {
              if(!res.data || !res.data.d || !res.data.d.ServerRelativeUrl) {
                return $q.reject(gettext('Returned SharePoint result is not in the expected format.'));
              }

              return [ res.data.d.ServerRelativeUrl, res.data.d.Name ];
            }
          );
      };


      /**
       * @ngdoc method
       * @name restServices.restDocumentService#addFile
       * @methodOf restServices.restDocumentService
       * @kind function
       *
       * @description
       * This method adds a new file the the SharePoint document library indicated by `endpoint`. The metadata for the
       * file needs to be passed in as JavaScript object `metadata`, which will be suitably serialized for SharePoint.
       * The file itself needs to be passed as an object as returned by the HTML5 file selectors.
       *
       * You can specify if SharePoint should overwrite the file if it already exists (by setting `overwrite` to `true`),
       * otherwise SharePoint will return an error if the file already exists.
       *
       * The SharePoint REST API returns the added item, including the ID it has been assigned. You can retrieve the
       * URL of the file from the resolved promise result `res` as `res.data.d.ServerRelativeUrl`.
       *
       * @param {string} endpoint A string indicating the endpoint (SharePoint list) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object.
       *
       * @param {string} fileName The name the file should be stored under at the given endpoint.
       *
       * @param {Object} metadata An object containing all the metadata for the file to be uploaded (`Title` plus
       *                          other fields required by that document library). Set to `undefined` if you do not
       *                          want to set additional metadata at this point.
       *
       * @param {boolean} overwrite If set to `true`, SharePoint will overwrite the file if it already exists. Otherwise
       *                            it will throw an error if it already exists.
       *
       * @param {Object} file A reference to the file as returned by the HTML5 file selectors.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object returned by the REST API.
       *
       */

      // Add new item to the list, given as JavaScript object "item"
      service.addFile = function(endpoint, fileName, metadata, file, overwrite) {
        // Create new promise
        var deferred = $q.defer();

        // Asynchronously read file uploaded by user
        var reader = new FileReader();

        reader.onloadend = function () {
          if(reader.error) {
            deferred.reject({
              status: -1,
              statusText: reader.error.message
            });
          } else {

            // TODO: progress notifications

            // Obtain contextitem...
            restBaseService.updateFormDigest(config)
              .then(
                function() {
                  return $http.post(config.apiBaseUrl + endpointsFolders[endpoint] + "/files/add(overwrite=" + (overwrite ? 'true' : 'false') + ",url='" + fileName.replace("'", "''") + "')", reader.result, config.httpUploadConfig);
                }
              )
              .then(
                function (res) {
                  if(metadata) {
                    // We need to write metadata first.
                    service.setFileMetadata(endpoint, fileName, metadata).then(deferred.resolve, deferred.reject);
                  } else {
                    deferred.resolve(res);
                  }
                }, function (restErr) {
                  deferred.reject(restErr);
                }
              );
          }
        };

        // Read the file
        reader.readAsArrayBuffer(file);

        // Return promise
        return deferred.promise;
      };



      /**
       * @ngdoc method
       * @name restServices.restDocumentService#addArrayBufferAsFile
       * @methodOf restServices.restDocumentService
       * @kind function
       *
       * @description
       * This method adds a new file the the SharePoint document library indicated by `endpoint`. The metadata for the
       * file needs to be passed in as JavaScript object `metadata`, which will be suitably serialized for SharePoint.
       * The file itself needs to be passed as an ArrayBuffer object.
       *
       * You can specify if SharePoint should overwrite the file if it already exists (by setting `overwrite` to `true`),
       * otherwise SharePoint will return an error if the file already exists.
       *
       * The SharePoint REST API returns the added item, including the ID it has been assigned. You can retrieve the
       * URL of the file from the resolved promise result `res` as `res.data.d.ServerRelativeUrl`.
       *
       * @param {string} endpoint A string indicating the endpoint (SharePoint list) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object.
       *
       * @param {string} fileName The name the file should be stored under at the given endpoint.
       *
       * @param {Object} metadata An object containing all the metadata for the file to be uploaded (`Title` plus
       *                          other fields required by that document library). Set to `undefined` if you do not
       *                          want to set additional metadata at this point.
       *
       * @param {boolean} overwrite If set to `true`, SharePoint will overwrite the file if it already exists. Otherwise
       *                            it will throw an error if it already exists.
       *
       * @param {ArrayBuffer} arrayBuffer An ArrayBuffer with the file content. (Note that you can use a string, too,
       *                                  as the underlying `$http.post` also accepts strings as POST content.)
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object returned by the REST API.
       *
       */

      // Add new item to the list, given as JavaScript object "item"
      service.addArrayBufferAsFile = function(endpoint, fileName, metadata, arrayBuffer, overwrite) {
        // Obtain contextitem...
        return restBaseService.updateFormDigest(config)
          .then(
            function () {
              return $http.post(config.apiBaseUrl + endpointsFolders[endpoint] + "/files/add(overwrite=" + (overwrite ? 'true' : 'false') + ",url='" + fileName.replace("'", "''") + "')", arrayBuffer, config.httpUploadConfig);
            }
          )
          .then(
            function (res) {
              if (metadata) {
                // We need to write metadata first.
                return service.setFileMetadata(endpoint, fileName, metadata);
              } else {
                return res;
              }
            }
          );
      };



      /**
       * @ngdoc method
       * @name restServices.restDocumentService#setFileMetadata
       * @methodOf restServices.restDocumentService
       * @kind function
       *
       * @description
       * This method updates metadata for a file for additional columns on the document library. You need to specify the
       * file by given the `endpoint` it resides in and its `fileName`. The `metadata` should be a serializable JavaScript
       * object that will be sent to the SharePoint server and overwrite any existing metadata for that item.
       *
       * The SharePoint REST API returns the modified item as stored. You can retrieve it from the resolved
       * promise result `res` as `res.data.d` object.
       *
       * @param {string} endpoint A string indicating the endpoint (SharePoint list) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object.
       *
       * @param {string} fileName The name of the file whose metadata should be updated.
       *
       * @param {Object} metadata The updated metadata as serializable JavaScript object. The fields that should be
       *                          set have to be given as properties of the object.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object returned by the REST API.
       *
       */

      service.setFileMetadata = function(endpoint, fileName, metadata) {
        // Obtain contextitem...
        return restBaseService.updateFormDigest(config)
          .then(
            function() {
              // ... obtain metadata type for this item
              return $http.get(config.apiBaseUrl + endpointsFolders[endpoint] + '/Files%28\'' + fileName + '\'%29/ListItemAllFields', config.httpGetConfig);
            }
          )
          .then(
            function(res) {
              if(!res || !res.data || !res.data.d) {
                console.log('ERROR trying to retrieve file metadata. Unexpected response:');
                console.log(res);

                return $q.reject(gettext('An error occured trying to retrieve file metadata.'));
              }

              // Add SharePoint metadata to item
              metadata.__metadata = { 'type': (res.data.d.__metadata.type ? res.data.d.__metadata.type : 'SP.Data.DocumentsItem') };

              // ... and then MERGE item to document library
              return $http
                .post(config.apiBaseUrl + endpointsFolders[endpoint] + '/Files%28\'' + fileName + '\'%29/ListItemAllFields', metadata, config.httpPutConfig)
                .then(function() { return res; });    // we return the GET response, as the POST response will be empty (and not include Id etc.)
            }
          );
      };


      /**
       * @ngdoc method
       * @name restServices.restDocumentService#deleteFile
       * @methodOf restServices.restDocumentService
       * @kind function
       *
       * @description
       * This method recycles the file referenced by its filename `fileName` from the SharePoint document library
       * referenced by `endpoint`.
       *
       * @param {string} endpoint A string indicating the endpoint (document library) which should be accessed, as
       *                          configured in the `sharepointEndpoints` configuration object.
       *
       * @param {string} fileName The file name of the file to be removed.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object returned by the REST API.
       *
       */

      service.deleteFile = function(endpoint, fileName) {
        // Obtain contextitem...
        return restBaseService.updateFormDigest(config)

        // ... then DELETE from the document library.
          .then(
            function() {
              // ... by POSTing to the Recycle endpoint.
              return $http.post(config.apiBaseUrl + endpointsFolders[endpoint] + '/Files%28\'' + encodeURIComponent(fileName) + '\'%29/Recycle', '', config.httpPostConfig);
            }
          );
      };



      /**
       * @ngdoc method
       * @name restServices.restDocumentService#getLibraryInfo
       * @methodOf restServices.restDocumentService
       * @kind function
       *
       * @description
       * This method returns details on a SharePoint document library specified by its name in `libraryName`. The method
       * returns a promise that resolves to the raw data object `res` returned from the REST API query. The details of
       * the document library will be contained in the object `res.data.d`.
       *
       * Note that document libraries are just SharePoint lists with a base template value of 101, so this call is
       * equivalent to using {@link restServices.restListService#getListInfo}.
       *
       * @param {string} libraryName A string giving the name of the SharePoint document library to be queried.
       *                          _Note that this method expects the library name and not the endpoint definition!_ This
       *                          is because this function should also be used for document libraries not part of the
       *                          application's endpoint definition.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */

      service.getLibraryInfo = function(libraryName) {
        return restListService.getListInfo(undefined, libraryName);
      };



      /**
       * @ngdoc method
       * @name restServices.restDocumentService#getLibraryFields
       * @methodOf restServices.restDocumentService
       * @kind function
       *
       * @description
       * This method provides access to a list of all fields defined on the library given by its name `libraryName`. The
       * method returns a promise that resolves to the raw data object `res` returned from the REST API query. The
       * array of all library fields (each as an object with its detailed properties) will be contained as `res.data.d`.
       *
       * Note that document libraries are just SharePoint lists with a base template value of 101, so this call is
       * equivalent to using {@link restServices.restListService#getListFields}. Als note that a SharePoint list
       * contains a lot of technical fields not manually defined or used but returned by this API call.
       *
       * @param {string} libraryName A string giving the name of the SharePoint library to be queried. _Note that this
       *                          method expects the library name and not the endpoint definition!_ This is because this
       *                          function should also be used for document libraries not part of the application's
       *                          endpoint definition.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */

      service.getLibraryFields = function(libraryName) {
        return restListService.getListFields(undefined, libraryName);
      };



      /**
       * @ngdoc method
       * @name restServices.restDocumentService#createLibrary
       * @methodOf restServices.restDocumentService
       * @kind function
       *
       * @description
       * This method creates a new SharePoint document library. You need to provide the details of the library as
       * `libraryObject`, which is a JavaScript object with the following two properties:
       *
       *  * `Title`: The name of the document library to be added.
       *  * `Description`: A longer description of the document library.
       *
       * SharePoint knows more properties for libraries, but this method will only transfer the ones detailed above.
       *
       * Note that creating document libraries is equivalent to creating SharePoint lists with a base template value
       * of 101.
       *
       * @param {Object} libraryObject A JavaScript object with the two properties `Title` (name of the list) and
       *                            `Description`.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */

      service.createLibrary = function(libraryObject) {
        // Format list object for SharePoint
        var sharepointList = {
          '__metadata': { 'type': 'SP.List' },
          'Title': libraryObject.Title,
          'Description': libraryObject.Description,
          'AllowContentTypes': true,
          'BaseTemplate': 101,  // <-- identifies a document library
          'ContentTypesEnabled': false,
          'EnableVersioning': (!!libraryObject.EnableVersioning)
        };

        // Obtain contextitem...
        return restBaseService.updateFormDigest(config)

        // ... then PUT the list.
          .then(
            function() {
              return $http.post(config.apiBaseUrl + 'web/lists', sharepointList, config.httpPostConfig);
            }
          );
      };



      /**
       * @ngdoc method
       * @name restServices.restDocumentService#createLibraryField
       * @methodOf restServices.restDocumentService
       * @kind function
       *
       * @description
       * This method creates a new field for an existing SharePoint document library identified by its name
       * `libraryName`. You need to specify the details of the field you want to create as a JavaScript object
       * `fieldObject`.
       *
       * Note that SharePoint document libraries are just lists, this method thus works exactly the same as
       * {@link restServices.restListService#createListField}. Please refer to this method for the detailed workings of
       * the `fieldObject` configuration object.
       *
       * @param {string} libraryName A string giving the name of the SharePoint library to be queried. _Note that this
       *                          method expects the library name and not the endpoint definition!_ This is because this
       *                          function should also be used for libraries not part of the application's endpoint
       *                          definition.
       *
       * @param {Object} fieldObject A JavaScript object with the required properties for the field to be added.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */

      service.createLibraryField = function(libraryName, fieldObject) {
        // For all multi-line document library fields, we will set the option UnlimitedLengthInDocumentLibrary to
        // true, as otherwise the field may only contain 256 characters.
        if(fieldObject.FieldTypeKind === 3) {
          fieldObject.UnlimitedLengthInDocumentLibrary = 'true';
        }


        return restListService.createListField(libraryName, fieldObject);
      };



      /**
       * @ngdoc method
       * @name restServices.restDocumentService#deleteLibraryField
       * @methodOf restServices.restDocumentService
       * @kind function
       *
       * @description
       * This method deletes a field from the library given by its name `libraryName`. You need to provide the GUID of
       * the field in `fieldId`, which you can obtain for example using the
       * {@link restServices.restDocumentService#getLibraryFields `getLibraryFields`} method.
       *
       * @param {string} libraryName A string giving the name of the SharePoint list to be queried. _Note that this
       *                          method expects the list name and not the endpoint definition!_ This is because this
       *                          function should also be used for lists not part of the application's endpoint
       *                          definition.
       *
       * @param {string} fieldId The SharePoint GUID of the field to be removed.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */

      service.deleteLibraryField = function(libraryName, fieldId) {
        return restListService.deleteListField(libraryName, fieldId);
      };


      /**
       * @ngdoc method
       * @name restServices.restDocumentService#deleteLibrary
       * @methodOf restServices.restDocumentService
       * @kind function
       *
       * @description
       * This method deletes a SharePoint document library given by its name `libraryName`. Note that SharePoint does
       * not allow to delete lists or libraries which are linked to lookup fields in use elsewhere. You first need to
       * remove the lookup fields in question before you can delete the document library.
       *
       * Note that SharePoint document libraries are just lists, so this method serves as a wrapper for
       * {@link restServices.restListService#deleteList} and behaves exactly the same.
       *
       * @param {string} libraryName A string giving the name of the library to be deleted. _Note that this method
       *                          expects the library name and not the endpoint definition!_ This is because this function
       *                          should also be used for libraries not part of the application's endpoint definition.
       *
       * @returns {Promise} A promise which resolves to the (raw) `$http` results object `res` returned by the REST API.
       *
       */

      service.deleteLibrary = function(libraryName) {
        return restListService.deleteList(libraryName);
      };

      return service;
    }];

    return provider;
  });
