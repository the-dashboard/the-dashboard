'use strict';

/**
 * @ngdoc overview
 * @name socialServices
 * @description
 *
 * This module provides services to work with the subscriptions and interactions, which can be attached to various
 * Dashboard elements such as notes and risks and are used to increase the interactivity of The Dashboard.
 *
 */

angular.module('socialServices', ['gettext', 'restServices', 'miscServices', 'usersServices', 'dashboardScreenServices'])

/*
 When we load this module, we register for locationChangeSucess events => if they contain the appropriate parameters,
 we will display the interactionSidebar!
 */
  .run(['$rootScope', '$location', 'interactionService', function($rootScope, $location, interactionService) {
    $rootScope.$on('$locationChangeSuccess', function () {
      var searchObject = $location.search();

      console.log('socialServices captured locationChangeSuccess event!');
      console.log(searchObject);

      if(searchObject.sidebar === 'interaction' && searchObject.targetType !== undefined && searchObject.targetId !== undefined) {
        // We show the interaction sidebar!
        console.log('Showing the interaction sidebar...');
        interactionService.showSidebar(
          searchObject.targetType,
          searchObject.targetId,
          searchObject.targetDescription,
          undefined,
          true,
          true,
          true,
          searchObject.sourceUrl
        );
      }
    });
  }])


/**
 * @ngdoc service
 * @name socialServices.subscriptionService
 * @description
 *
 * This service manages subscriptions and newsfeed entries, which are displayed for each individual user.
 *
 * Currently supported are the following subscription types:
 *
 * - `interaction` subscribes to interactions (comments, highlights etc.) on a given object in the Dashboard. The
 *    `target` for this subscription should be specified as an object that contains a `targetType` value (e.g.
 *    `note` etc.) and a `targetId` value to specific the object which should be subscribed to.
 *
 * - `keywords` subscribes to a list of keywords. This will trigger a newsfeed entry for new objects -- in particular
 *    notes -- that match *all* the given keywords (AND concatenation). The `target` should be given as an array of
 *    keyword objects (with `id` and `name` elements each).
 *
 * - `risk` subscribes to an individual risk in a risk matrix. The `target` should be given as the `initialId` value of
 *   the risk to be subscribed to (to be able to subscribe to the same risk spanning across freeze points). Will trigger
 *   newsfeed alerts whenever that risk is being moved or removed. Supported actions are `changed-risk`,
 *   `deleted-risk`. (TODO TODO)
 *
 * - `task` subscribes to an individual task in the OCA task system. The `target` should be given as the `id` value of
 *   the task to be subscribed to. Note that only non-archived tasks can be subscribed to. This will trigger newsfeed
 *   alerts when the task is being signed-off by someone. (TODO TODO)
 *
 * - `auditFormData` subscribes to an individual audit form in the OCA task system. The `target` should be given as the
 *   `id` value of the audit form element to be subscribed to. This will trigger newsfeed alerts when this audit form
 *   is being signed-off by someone or changes its `result`. (TODO TODO)
 *
 * - `riskCategory` subscribes to changes to all risks in the given risk category. The `target` should be given as the
 *   name of the risk category. Supported actions are `new-risk`, `changed-risk`, `deleted-risk`.
 *
 *
 * TBD TBD TODO - Describe action types here
 *
 */

  .service('subscriptionService', ['$q', 'gettext', 'restListService', 'usersService', 'miscServices', function ($q, gettext, restListService, usersService, miscServices) {
    var subServ = {};

    // Cache for newsfeed entries for a given user (the user ID is given as stringified object key)
    var newsfeedPromises = {};

    // Cache for subscriptions by type and target string (one object, object key is "type::target string").
    var subscriptionsPromises = {};

    // Cache for subscriptions by user and optional type (nested object, first level is the user, second level is the type or '*' for all).
    var subscriptionsByUserPromises = {};



    // Helper string to select all required fields for newsObjs
    var newsObjSelect = 'Id,newsTime,userId,sourceType,sourceId,subElement,sourceUrl,subscription/Id,subscription/type,subscription/target,subscription/targetDescription,actionType,contentVariables';

    // Helper function to turn a newsfeed object into a REST object
    var newsObjToREST = function(newsObj) {
      return {
        'userId': newsObj.userId,
        'newsTime': newsObj.newsTime,
        'sourceType': newsObj.sourceType,
        'sourceId': newsObj.sourceId.toString(),
        'subElement': (newsObj.subElement === undefined || newsObj.subElement === null ? null : newsObj.subElement.toString()),
        'sourceUrl': newsObj.sourceUrl,
        'subscriptionId': newsObj.subscriptionId,
        'actionType': newsObj.actionType,
        'contentVariables': JSON.stringify(newsObj.contentVariables)
      };
    };


    // Helper function to turn a REST object into a newsfeed object
    var newsObjFromREST = function(restObj) {
      return {
        'id': restObj.Id,
        'newsTime': miscServices.parseDatetime(restObj.newsTime),
        'userId': restObj.userId,
        'sourceType': restObj.sourceType,
        'sourceId': restObj.sourceId,
        'subElement': restObj.subElement,
        'sourceUrl': restObj.sourceUrl,
        'subscriptionId': restObj.subscription.Id,
        'subscriptionType': restObj.subscription.type,
        'subscriptionTarget': restObj.subscription.target,
        'subscriptionTargetDescription': restObj.subscription.targetDescription,
        'actionType': restObj.actionType,
        'contentVariables': (restObj.contentVariables ? JSON.parse(restObj.contentVariables) : {})
      };
    };


    // Helper function to turn a subscription object into a REST object
    var subscriptionObjToREST = function(subscriptionObj) {
      return {
        userId: subscriptionObj.userId,
        type: subscriptionObj.type,
        target: subscriptionObj.target,
        targetDescription: subscriptionObj.targetDescription,
        actionType: (subscriptionObj.actionType ? subscriptionObj.actionType : '*')
      };
    };


    // Helper funciton to turn a REST object into a subscription object
    var subscriptionObjFromREST = function(restObj) {
      return {
        id: restObj.Id,
        userId: restObj.userId,
        type: restObj.type,
        target: restObj.target,
        targetDescription: restObj.targetDescription,
        actionType: (restObj.actionType === '*' ? undefined : restObj.actionType)
      };
    };





    /**
     * @ngdoc method
     * @name socialServices.subscriptionService#targetObjToString
     * @methodOf socialServices.subscriptionService
     * @kind function
     *
     * @description
     * This method turns a target object for a subscription into a single string that is being stored in the
     * SharePoint list and can be used for comparing subscriptions for matches.
     *
     *
     * @param {String} type                The subscription type (see overall description of `subscriptionService`
     *                                     for supported subscription types).
     * @param {Object} targetObj           The target object for this subscription that should be converted based on the
     *                                     subscription type. If `targetObj` is already a string, it will be returned
     *                                     unmodified.
     *
     * @returns {String}  Returns a string which is formatted according to the subscription `type`, identifying the
     *                    `targetObj`.
     *
     */

    subServ.targetObjToString = function(type, targetObj) {
      if(typeof targetObj === 'string' || targetObj instanceof String) {
        // targetObj already is a string, so we can just return it unmodified
        return targetObj;
      }

      if(typeof targetObj === 'number' || targetObj instanceof Number) {
        // A numeric value will be converted to a string and returned otherwise as-is (could be an ID value,
        // for example).
        return targetObj.toString();
      }

      // The formatting of the target string depends on the subscription type
      if(type === 'riskCategory') {
        return targetObj.category;
      } else if(type === 'risk' || type === 'task' || type === 'auditFormData') {
        return targetObj.id;
      } else if(type === 'keywords' && Array.isArray(targetObj)) {
        // First we sort the keywords, to make the targetString unique
        var keywordIds = targetObj.map(function(d) { return d.id; });
        keywordIds.sort();

        return (',' + keywordIds.join(',') + ',');
      } else if(type === 'interaction') {
        return (targetObj.targetType + ':' + targetObj.targetId);
      } else {
        return; /* Invalid type or targetObj */
      }
    };




    /**
     * @ngdoc method
     * @name socialServices.subscriptionService#triggerSubs
     * @methodOf socialServices.subscriptionService
     * @kind function
     *
     * @description
     * This method goes through all subscriptions of the given `type` that apply to the given
     * `targetObj` (which will be passed through `targetObjToString` first, see below) and fires
     * them. Subscriptions are retrieved by calling `getSubscriptions(type, targetObj)`.
     *
     * The `actionType` must be available in the `actionTypeList` stored inside the subscription service.
     * The values of `contentVariables` will be pushed into the newsfeed template that depends on the
     * subscription type and `actionType`.
     *
     *
     * @param {String} type                The subscription type (see overall description of `subscriptionService`
     *                                     for supported subscription types).
     * @param {Object} targetObj           The target object for this subscription. Can either be passed as a pre-
     *                                     processed, combined string or as an object containing the values required
     *                                     to create the target string (based on the given `type`).
     * @param {String} sourceType          The type of the element that triggered this call (e.g. `note`, `risk` etc.).
     * @param {String} sourceId            The ID of the element that triggered this call.
     * @param {String} subElement          The sub-element that triggered this call (if any).
     * @param {String} sourceUrl           The source URL for this action that will be stored in the newsfeed of the
     *                                     users. Can be empty or unset, in this case the newsfeed entry will not link
     *                                     anywhere.
     * @param {Object} contentVariables    An object of content that will be used to fill the content templates of any
     *                                     triggered subscriptions.
     * @param {String} actionType          The type of the action that triggered this subscription. Will be used to
     *                                     create the right text (content template) for the newsfeed entry. See overall
     *                                     description of `subscriptionService` for supported subscription types.
     *
     * @returns {Promise} Returns a promise which resolves to the list of new newsfeed entries created by
     *                    triggering these subscriptions.
     *
     */


    subServ.triggerSubs = function(type, targetObj, sourceType, sourceId, subElement, sourceUrl, contentVariables, actionType) {

      console.log('triggerSubs called.');
      console.log([ type, targetObj, sourceType, sourceId, subElement, sourceUrl, contentVariables, actionType ]);

      // First we need to retrieve the relevant subscriptions to be able to fire them...
      return subServ.getSubscriptions(type, targetObj, true)
        .then(function(subs) {
          var addNewsPromise = $q.resolve();
          var newsObjs = [];

          console.log('triggerSubs retrieved matching subscriptions:');
          console.log(subs);

          subs.forEach(function(subscriptionObj) {
            // Is this subscription limited by actionType? If yes, we only trigger if they match.
            if(subscriptionObj.actionType && subscriptionObj.actionType !== '*' && subscriptionObj.actionType !== actionType) {
              return;
            }

            // Subscription is relevant, so we can add a corresponding newsfeed entry.
            addNewsPromise = addNewsPromise
              .then(
                function() {
                  return subServ.addNews({
                    userId: subscriptionObj.userId,
                    sourceType: sourceType,
                    sourceId: sourceId,
                    subElement: subElement,
                    sourceUrl: sourceUrl,
                    contentVariables: contentVariables,
                    actionType: actionType,
                    subscriptionId: subscriptionObj.id,
                    subscriptionType: subscriptionObj.type,
                    subscriptionTarget: subscriptionObj.target,
                    subscriptionTargetDescription: subscriptionObj.targetDescription
                  });
                }
              )
              .then(
                function(newsObj) {
                  newsObjs.push(newsObj);
                }
              );
          });

          return addNewsPromise
            .then(
              function() {
                console.log('triggerSubs added the following news items:');
                console.log(newsObjs);

                return newsObjs;
              }
            );
        });
    };




    /**
     * @ngdoc method
     * @name socialServices.subscriptionService#updateMentions
     * @methodOf socialServices.subscriptionService
     * @kind function
     *
     * @description
     * This method is being used to implement "at-mentions": If a text entry field recognizes at-mentions, it has to
     * call a function afterwards to create a newsfeed entry for each user that is being mentioned (using the
     * `mentionedUserIds` array of the mentioned user IDs). The `noteText` should contain the text that includes
     * the at-mentions. If there already exist mentions to the element identified via the given `sourceType` and
     * `sourceId`, they will not be mentioned again.
     *
     * @param {String} sourceType         The type of the element that contains the at-mentions (currently supported are
     *                                    `note`, `risk`). Note that at-mentions in interactions (in particular
     *                                    comments) will be attached to the `sourceType` and `sourceId` of the
     *                                    underlying object (e.g. the note or risk item).
     * @param {String} sourceId           The ID of the element which (or an interaction on which) contains the
     *                                    at-mentions.
     * @param {String} subElement         The subelement that contains the at-mentions (or an empty string if the
     *                                    mention is contained in the main element).
     * @param {String} sourceUrl          The source URL for this action that will be stored in the newsfeed of the
     *                                    users. Can be empty or unset, in this case the newsfeed entry will not link
     *                                    anywhere.
     * @param {Array}  mentionedUserIds   An array of user IDs that have been mentioned in this event. For each of the
     *                                    given user IDs a newsfeed entry will be generated.
     * @param {Object} contentVariables   Variables to be used in the newsfeed entry (depends on sourceType).
     *
     * @returns {Promise} Returns a promise which resolves to an array of newsfeed entries that have been created.
     *
     */

    subServ.updateMentions = function(sourceType, sourceId, subElement, sourceUrl, mentionedUserIds, contentVariables) {
      console.log('updateMentions called.');
      console.log([ sourceType, sourceId, subElement, sourceUrl, mentionedUserIds, contentVariables ]);

      // First we need to retrieve all existing mentions so that we don't mention the same thing twice
      return subServ.getNewsBySource(sourceType, sourceId, subElement, 'mention')
        .then(
          function(newsObjs) {
            var existingMentions = newsObjs.map(function(d) { return d.userId.toString(); });

            var toBeAdded = mentionedUserIds.filter(
              function(userId) {
                return (existingMentions.indexOf(userId.toString()) < 0);
              }
            );

            var newEntries = [];
            var addPromise = $q.resolve();
            toBeAdded.forEach(function(userId) {
              addPromise = addPromise
                .then(
                  function() {
                    return subServ.addNews({
                      userId: userId,
                      sourceType: sourceType,
                      sourceId: sourceId,
                      subElement: subElement,
                      sourceUrl: sourceUrl,
                      contentVariables: contentVariables,
                      subscriptionType: 'mention',
                      actionType: 'mention'
                    });
                  }
                )
                .then(
                  function(res) {
                    // We return all added entries
                    newEntries.push(res);

                    // We remove existing promises that might include these entries
                    newsfeedPromises[res.userId.toString()] = undefined;
                  }
                );
            });

            return addPromise
              .then(
                function() {
                  console.log('Created news entries based on at-mentions:');
                  console.log(newEntries);

                  return newEntries;
                }
              );
          }
        );
    };


    /**
     * @ngdoc method
     * @name socialServices.subscriptionService#supportedSubscriptions
     * @methodOf socialServices.subscriptionService
     * @kind function
     *
     * @description
     * This method returns the supported subscription types. Can be used to determine whether an object has a
     * dedicated subscription type for changes to this object (e.g. risks have the `risk` subscription, but
     * notes have no dedicated subscription type).
     *
     *
     * @returns {Array} Returns an array with the supported subscription types.
     *
     */

    subServ.supportedSubscriptions = function() {
      return [
        'keywords',
        'riskCategory',
        'risk',
        'task',
        'note',
        'auditFormData',
        'interaction'
      ];
    };


    /**
     * @ngdoc method
     * @name socialServices.subscriptionService#supportedSubscriptions
     * @methodOf socialServices.subscriptionService
     * @kind function
     *
     * @description
     * This method returns the subscription type that matches the given Dashboard endpoint.
     *
     * (Bad design decision... it would have been better to use the endpoint keys as subscription types, which would
     * make this method unnecessary.)
     *
     * @param {String} endpoint       The Dashboard endpoint for which the subscription type should be retrieved.
     *
     * @returns {Array} Returns the subscription type for the endpoint or `undefined` if this endpoint does not have a
     *                  dedicated subscription type.
     *
     */


    var endpointsToSubscriptionType = {
      'notes':            'note',
      'risks':            'risk',
      'impact_flags':     'impact',
      'task_data':        'task',
      'audit_forms_data': 'auditFormData'
    };

    subServ.subscriptionTypeForEndpoint = function(endpoint) {
      return endpointsToSubscriptionType[endpoint];
    };



    /**
     * @ngdoc method
     * @name socialServices.subscriptionService#addNews
     * @methodOf socialServices.subscriptionService
     * @kind function
     *
     * @description
     * This method adds the given newsfeed item `newsObj` to the newsfeed (after passing it through `newsObjToREST`).
     *
     *
     * @param {Object} newsObj       The newsfeed item that should be added to the newsfeed. Needs to contain all
     *                               required fields (in particular `userId`).
     *
     * @returns {Promise} Returns a promise with the completed newsfeed object (containing its new ID).
     *
     */

    subServ.addNews = function(newsObj) {
      // We prefill the optional "newsTime" value
      if(!newsObj.newsTime) {
        newsObj.newsTime = new Date();
      }

      // We convert the news object to a REST object for SharePoint
      var restObj = newsObjToREST(newsObj);

      console.log('addNews called:');
      console.log([ newsObj, restObj ]);

      return restListService
      // Add the item in the database
        .addItem('newsfeed', restObj)
        .then(function(res) {
          if(!res.data || !res.data.d || res.data.d.Id === undefined) {
            console.log('addNews: An unexpected response was received:');
            console.log(res);

            return $q.reject(gettext('An unexpected response was received while trying to add a new newsfeed item.'));
          }

          // Clear newsfeed promise for the user this newsfeed item relates to
          newsfeedPromises[newsObj.userId.toString()] = undefined;

          // We add the returned ID to the newsObj and return it
          newsObj.id = res.data.d.Id;
          return newsObj;
        });

    };



    /**
     * @ngdoc method
     * @name socialServices.subscriptionService#getNewsBySource
     * @methodOf socialServices.subscriptionService
     * @kind function
     *
     * @description
     * This method retrieves newsfeed items belonging to a specific source element.
     *
     * @param {String} sourceType         The type of the element whose newsfeed references should be removed (currently
     *                                    supported are `note`, `risk`).
     * @param {String} sourceId           The ID of the element whose newsfeed references should be removed.
     * @param {String} subElement         The subelement whose newsfeed references should be retrieved (optional, if
     *                                    an empty string only news for the main element will be retrieved. If undefined,
     *                                    news for the main and all subelements will be retrieved.)
     * @param {String} actionType         If provided, limits the search for news of the given `actionType`. If not
     *                                    given, all actions will be retrieved.
     *
     * @returns {Promise} Returns a promise that will resolve to the list of match newsfeed entries.
     *
     */

    subServ.getNewsBySource = function(sourceType, sourceId, subElement, actionType) {
      // Note that we do not use a cached promise here as it is important that we always get the full data here.
      return restListService
        .getMergedFilteredItems(
          'newsfeed',
          'sourceType eq \'' + sourceType + '\' and sourceId eq \'' + sourceId + '\'' +
          (subElement !== undefined && subElement !== null ? ' and subElement eq \'' + subElement + '\'' : '') +
          (actionType !== undefined && actionType !== null ? ' and actionType eq \'' + actionType + '\'' : ''),
          newsObjSelect,
          'subscription'
        )
        .then(function(res) {
          if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
            console.log('ERROR: Could not getNewsBySource: The response received was not in the expected format:');
            console.log(res);

            return $q.reject(gettext('Could not retrieve news items related to this element: The response received was not in the expected format.'));
          }

          return res.data.d.results.map(newsObjFromREST);
        });
    };




    /**
     * @ngdoc method
     * @name socialServices.subscriptionService#removeNewsBySource
     * @methodOf socialServices.subscriptionService
     * @kind function
     *
     * @description
     * This method removes newsfeed items belonging to a specific source element. This method will primarily be
     * used when a given source element gets removed -- all related newsfeed entries then also need to be cleaned
     * up.
     *
     * @param {String} sourceType         The type of the element whose newsfeed references should be removed (currently
     *                                    supported are `note`, `risk`).
     * @param {String} sourceId           The ID of the element whose newsfeed references should be removed.
     * @param {String} subElement         The subelement whose newsfeed references should be removed (optional, if
     *                                    empty news for the main and all subelements will be removed.)
     *
     * @returns {Promise} Returns a promise that will resolve to `true` if all newsfeed references could be removed.
     *
     */

    subServ.removeNewsBySource = function(sourceType, sourceId, subElement) {
      // First we need to get all items, so that we can then delete them one by one.
      return subServ.getNewsBySource(sourceType, sourceId, subElement)
        .then(function(newsObjs) {
          var deletePromise = $q.resolve();

          newsObjs.forEach(function(newsObj) {
            // We delete them one after another (not in parallel) to avoid overloading the SharePoint server
            deletePromise = deletePromise.then(function() {
              return subServ.removeNews(newsObj);
            });
          });

          return deletePromise
            .then(
              function() {
                return true;
              },
              function(err) {
                console.log('ERROR: An error occured while running removeNewsBySource(' + sourceType + ', ' + sourceId + '):');
                console.log(err);

                return $q.reject(gettext('An error occured while trying to remove news items related to this element.'));
              }
            );
        });
    };



    /**
     * @ngdoc method
     * @name socialServices.subscriptionService#removeNews
     * @methodOf socialServices.subscriptionService
     * @kind function
     *
     * @description
     * This method removes the newsfeed item `newsObj` from the database.
     *
     * @param {String} newsObj             The newsfeed item to be removed.
     *
     * @returns {Promise} Returns a promise that will resolve to `true` if the newsfeed item could be removed.
     *
     */

    subServ.removeNews = function(newsObj) {
      return restListService
        .deleteItem('newsfeed', newsObj.id)
        .then(
          function() {
            // We need to clear any newsfeedPromises for that object
            newsfeedPromises[newsObj.userId.toString()] = undefined;

            return true;
          }
        );
    };



    /**
     * @ngdoc method
     * @name socialServices.subscriptionService#getNewsfeed
     * @methodOf socialServices.subscriptionService
     * @kind function
     *
     * @description
     * This method will retrieve the newsfeed for the given `userId`, limited to `limit` newest entries
     * (default = 30). If `startAfter` is given, will show news entries older than the given ID. If `userId`
     * is unset, will retrieve newsfeed for the currently active user.
     *
     * @param {integer} userId               The ID of the user whose newsfeed should be retrieved (if unset the
     *                                       newsfeed of the active user will be retrieved).
     * @param {integer} limit                How many entries from the newsfeed should be retrieved (if unset will
     *                                       default to 30).
     * @param {integer} startAfter           If given, will retrieve the newest entries older than the entry given by
     *                                       the ID `startAfter`. If unset, will retrieve the newest entries.
     * @param {boolean} collapseDuplicates   If set to `true`, multiple elements referencing the same source element
     *                                       will be collapsed by only showing the first item.
     *
     * @returns {Promise} Returns a promise that resolves to an array of which the first element is an array of the
     *                    retrieved newsfeed entries while the second element is `true` if and only if there are
     *                    additional (older) elements that can be retrieved.
     *
     */
    subServ.getNewsfeed = function(userId, limit, startAfter, collapseDuplicates) {

      return usersService.fillUserId(userId)
        .then(function(_userId) {
          userId = _userId;
          limit = (limit ? limit : 30);
          startAfter = (startAfter ? startAfter : -1);

          // Do we already have a cached newsfeed promise that is not too old (newer than 10 minutes)?
          if(newsfeedPromises[userId.toString()]) {
            if(newsfeedPromises[userId.toString()].age > Date.now()-600000 && newsfeedPromises[userId.toString()].limit >= limit && newsfeedPromises[userId.toString()].startAfter >= startAfter) {
              return newsfeedPromises[userId.toString()].promise
                .then(function(newsObjs) {
                  // We need to filter them to return not too many objects and limit them to the right number.
                  if(startAfter !== -1) {
                    return newsObjs
                      .filter(
                        function(d) {
                          return (d.id < startAfter);
                        }
                      )
                      .slice(0, limit);
                  } else {
                    return newsObjs
                      .slice(0, limit);
                  }
                });
            }
          }

          newsfeedPromises[userId.toString()] = {
            age: Date.now(),
            limit: limit,
            startAfter: startAfter
          };

          newsfeedPromises[userId.toString()].promise = restListService
            .getFilteredItems(
              'newsfeed',
              'user eq ' + userId.toString() + (startAfter !== -1 ? ' and Id lt ' + startAfter.toString() : ''),
              newsObjSelect,
              'subscription',
              'newsTime desc',
              limit)
            .then(
              function (res) {
                if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
                  console.log('ERROR: Could not getNewsfeed: The response received was not in the expected format:');
                  console.log(res);

                  return $q.reject(gettext('Could not retrieve the newsfeed items: The response received was not in the expected format.'));
                }

                return res.data.d.results.map(newsObjFromREST);
              },
              function (err) {
                console.log('ERROR: Could not getNewsfeed, an error occured:');
                console.log(err);

                return $q.reject(gettext('An error occured while trying to retrieve the newsfeed items. Please try again later.'));
              }
            );

          return newsfeedPromises[userId.toString()].promise;
        })
        .then(
          function(newsfeedItems) {
            if(collapseDuplicates) {
              var sourceHash = {};
              var collapsedItems = [];

              for(var i = newsfeedItems.length-1; i >= 0; i--) {
                var hashKey = newsfeedItems[i].sourceType + '::' + newsfeedItems[i].sourceId.toString() + '::' + (newsfeedItems[i].contentVariables && newsfeedItems[i].contentVariables.from !== undefined ? newsfeedItems[i].contentVariables.from.toString() : '') + '::' + (newsfeedItems[i].subElement === undefined || newsfeedItems[i].subElement === null ? '' : newsfeedItems[i].subElement);

                // Only repeated events within the same hour are collapsed
                if(!sourceHash[hashKey] || sourceHash[hashKey] < newsfeedItems[i].newsTime.getTime() - 3600000) {
                  sourceHash[hashKey] = newsfeedItems[i].newsTime.getTime();
                  collapsedItems.splice(0, 0, newsfeedItems[i]);
                }
              }

              return [ collapsedItems, newsfeedItems.length === limit ];
            }

            // If we do not collapseDuplicates, we return all of them.
            return [ newsfeedItems, newsfeedItems.length === limit ];
          }
        );
    };



    /**
     * @ngdoc method
     * @name socialServices.subscriptionService#addSubscription
     * @methodOf socialServices.subscriptionService
     * @kind function
     *
     * @description
     * This method adds a new subscription for the given `userId`, with the given `type` and for the given `targetObj`.
     * The `targetObj` depends on the `type` and will be transformed by the service to the target string expected by
     * the subscriptions list. Will check whether the subscription already exists and will not add it if it does.
     *
     * @param {integer} userId                  The ID of the user for whom a new subscription should be added. If
     *                                          undefined, the subscription will be added for the currently active user.
     * @param {String}  type                    The type of the target object of the subscription (see the overall
     *                                          description of the `subscriptionService` for valid types).
     * @param {Object}  targetObj               The target object for this subscription. Can either be passed as a pre-
     *                                          processed, combined string or as an object containing the values required
     *                                          to create the target string (based on the given `type`).
     * @param {String}   targetDescription      A description of the element for which this watch is being added.
     *                                          Will be displayed in any newsfeed entries triggered by this watch.
     * @param {String}  actionType              Can be left unset or set to `*` if this subscription should apply to all
     *                                          events on the target object. Otherwise, an action type can be given and
     *                                          only events of that `actionType` will trigger this subscription (see the
     *                                          overall description of the `subscriptionService` for valid types).
     *
     * @returns {Promise} Returns a promise which resolves to the created subscription object.
     *
     */

    subServ.addSubscription = function(userId, type, targetObj, targetDescription, actionType) {
      var subscriptionObj = {
        type: type,
        target: subServ.targetObjToString(type, targetObj),
        targetDescription: targetDescription,
        actionType: (actionType ? actionType : '*')
      };

      console.log('addSubscription called.');
      console.log([ userId, type, targetObj, targetDescription, actionType ]);
      console.log('Subscription object to be added:');
      console.log(subscriptionObj);

      return usersService.fillUserId(userId)
        .then(
          function(_userId) {
            userId = _userId;
            subscriptionObj.userId = userId;

            return restListService.addItem('subscriptions', subscriptionObjToREST(subscriptionObj));
          }
        )
        .then(
          function(res) {
            if(!res.data || !res.data.d || res.data.d.Id === undefined) {
              console.log('addSubscription: An unexpected response was received:');
              console.log(res);

              return $q.reject(gettext('An unexpected response was received while trying to add a new subscription item.'));
            }

            // Clear subscription promises
            subscriptionsPromises[type + '::' + subscriptionObj.target + '::true'] = undefined;
            subscriptionsPromises[type + '::' + subscriptionObj.target + '::false'] = undefined;
            subscriptionsByUserPromises[subscriptionObj.userId.toString()] = {};

            // We add the returned ID to the subscriptionObj and return it
            subscriptionObj.id = res.data.d.Id;
            return subscriptionObj;
          },
          function(err) {
            console.log('addSubscription: An ERROR occured:');
            console.log(err);
            return $q.reject(gettext('An error occured while trying to add a new subscription item.'));
          }
        );
    };



    /**
     * @ngdoc method
     * @name socialServices.subscriptionService#removeSubscription
     * @methodOf socialServices.subscriptionService
     * @kind function
     *
     * @description
     * This method removes the subscription `subscriptionObj` from the database.
     *
     * @param {integer} subscriptionObj     The subscription to be removed.
     *
     * @returns {Promise} Returns a promise which resolves to `true` if the subscription could be removed from the
     *                    database.
     *
     */

    subServ.removeSubscription = function(subscriptionObj) {
      return restListService
        .deleteItem('subscriptions', subscriptionObj.id)
        .then(
          function() {
            // We need to clear any cached promises for that object
            subscriptionsPromises[subscriptionObj.type + '::' + subscriptionObj.target + '::true'] = undefined;
            subscriptionsPromises[subscriptionObj.type + '::' + subscriptionObj.target + '::false'] = undefined;

            subscriptionsByUserPromises[subscriptionObj.userId.toString()] = {};

            return true;
          },
          function(err) {
            console.log('An ERROR occured while trying to removeSubscription:');
            console.log(subscriptionObj);
            console.log(err);

            return $q.reject(gettext('An error occured while trying to remove your subscription.'));
          }
        );
    };


    /**
     * @ngdoc method
     * @name socialServices.subscriptionService#removeSubscriptionsByTarget
     * @methodOf socialServices.subscriptionService
     * @kind function
     *
     * @description
     * This method removes all subscriptions with the given `type` and for the given `targetObj`. The `targetObj`
     * depends on the `type` and will be transformed by the service to the target string expected by the
     * subscriptions list. If `userId` is given, only subscriptions of the given user will be removed.
     *
     * @param {String}  type               The type of the target object of the subscription (see the overall
     *                                     description of the `subscriptionService` for valid types).
     * @param {Object}  targetObj          The target object for the subscription. Can either be passed as a pre-
     *                                     processed, combined string or as an object containing the values required
     *                                     to create the target string (based on the given `type`).
     * @param {integer} userId             If given, only subscriptions of the user identified by `userId` will be
     *                                     removed.
     *
     * @returns {Promise} Returns a promise which resolves to `true` if the action completed successfully.
     *
     */

    subServ.removeSubscriptionsByTarget = function(type, targetObj, userId) {
      return subServ
        .getSubscriptions(type, targetObj)
        .then(
          function(subscriptionObjs) {
            // We delete one after the other
            var deletePromise = $q.resolve();

            if(userId !== undefined && userId !== null) {
              // If a userId has been given, we filter the subscriptions for those that
              // match this userId.
              subscriptionObjs = subscriptionObjs.filter(function(d) {
                return (d.userId.toString() === userId.toString());
              });
            }

            subscriptionObjs.forEach(function(d) {
              deletePromise = deletePromise
                .then(
                  function() {
                    return restListService
                      .deleteItem('subscriptions', d.id)
                      .then(function() {
                        // We need to clear promises related to this subscription object
                        subscriptionsPromises[d.type + '::' + d.target + '::true'] = undefined;
                        subscriptionsPromises[d.type + '::' + d.target + '::false'] = undefined;
                        subscriptionsByUserPromises[d.userId.toString()] = {};
                      });
                  }
                );
            });

            return deletePromise;
          }
        )
        .then(
          function() {
            return true;
          },
          function(err) {
            console.log('removeSubscriptionsByTarget: An ERROR occured:');
            console.log(err);
            return $q.reject(gettext('An error occured while trying to remove subscriptions to this element.'));
          }
        );
    };




    /**
     * @ngdoc method
     * @name socialServices.subscriptionService#getSubscriptions
     * @methodOf socialServices.subscriptionService
     * @kind function
     *
     * @description
     * This method retrieves all subscriptions of the given `type` that apply to the given `targetObj` (which will be
     * passed through `targetObjToString` first, see below).
     *
     * @param {String}    type               The type of the target object of the subscriptions (see the overall
     *                                       description of the `subscriptionService` for valid types).
     * @param {Object}    targetObj          The target object for the subscriptions. Can either be passed as a pre-
     *                                       processed, combined string or as an object containing the values required
     *                                       to create the target string (based on the given `type`).
     * @param {boolean=}  matchOr            If set to `true`, for supported subscription types (currently only
     *                                       `keyword`), the matching will be or-based, i.e. all subscriptions that
     *                                       match any combination of the given keywords will be retrieved.
     *
     * @returns {Promise} Returns a promise which resolves to a list of subscriptions that match the given `type` and
     *                    `targetObj`.
     *
     */

    subServ.getSubscriptions = function(type, targetObj, matchOr) {
      var targetString = subServ.targetObjToString(type, targetObj);

      console.log('getSubscriptions called.');
      console.log([ type, targetObj, matchOr ]);
      console.log('Calculated targetString: ' + targetString);

      // Or-matching only works if the provided targetObj is an array.
      matchOr = (matchOr && Array.isArray(targetObj));

      // Do we already have a promise for this?
      if(!subscriptionsPromises[type + '::' + targetString + '::' + (matchOr ? 'true' : 'false')]) {
        if(!matchOr) {
          // matchOr is false: We only retrieve subscriptions that match exactly.
          subscriptionsPromises[type + '::' + targetString + '::false'] = restListService
            .getMergedFilteredItems('subscriptions', 'type eq \'' + type + '\' and target eq \'' + targetString + '\'')
            .then(
              function(res) {
                if(!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
                  console.log('ERROR: Could not getSubscriptions: The response received was not in the expected format:');
                  console.log(res);

                  return $q.reject(gettext('Could not get the subscriptions for this element: The response received was not in the expected format.'));
                }

                return res.data.d.results.map(subscriptionObjFromREST);
              }
            );
        } else {
          // matchOr is true: We need to retrieve subscriptions for each individual target object and then filter them.
          var targetObjMatch = targetObj
            .map(
              function(d) {
                return 'substringof(\'' + subServ.targetObjToString(type, [ d ]) + '\', target)';
              }
            )
            .join(' or ');

          subscriptionsPromises[type + '::' + targetString + '::true'] = restListService
            .getMergedFilteredItems('subscriptions', 'type eq \'' + type + '\' and (' + targetObjMatch + ')')
            .then(
              function(res) {
                if(!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
                  console.log('ERROR: Could not getSubscriptions: The response received was not in the expected format:');
                  console.log(res);

                  return $q.reject(gettext('Could not get the subscriptions for this element: The response received was not in the expected format.'));
                }

                return res.data.d.results.map(subscriptionObjFromREST);
              }
            )
            .then(
              function(subscriptionObjs) {
                return subscriptionObjs
                  .filter(
                    function(subscriptionObj) {
                      // Every element of the subscriptionObj.target needs to be part of the target string
                      return subscriptionObj.target.split(',').every(function(d) {
                        return (d === '' || targetString.indexOf(',' + d + ',') >= 0);
                      });
                    }
                  );
              }
            );
        }
      }

      return subscriptionsPromises[type + '::' + targetString + '::' + (matchOr ? 'true' : 'false')];
    };



    /**
     * @ngdoc method
     * @name socialServices.subscriptionService#getSubscriptionsByUser
     * @methodOf socialServices.subscriptionService
     * @kind function
     *
     * @description
     * This method retrieves all subscriptions of the given `type` for the given `userId`; if type is missing, all
     * subscriptions for the user are retrieved. If the `userId` is undefined, will retrieve subscriptions for the
     * currently active user.
     *
     * @param {integer} userId             The ID of the user whose subscriptions should be retrieved. If missing, the
     *                                     subscriptions for the currently active user will be retrieved.
     * @param {String}  type               The type of the subscriptions to be retrieved (if missing, all subscriptions
     *                                     for the given `userId` will be retrieved).
     *
     * @returns {Promise} Returns a promise which resolves to a list of matching subscriptions.
     *
     */

    subServ.getSubscriptionsByUser = function(userId, type) {
      if(type === undefined || type === null) {
        type = '*';
      }

      return usersService.fillUserId(userId)
        .then(
          function(_userId) {
            userId = _userId;

            // Do we already have a promise that we can use?
            if(subscriptionsByUserPromises[userId.toString()] && subscriptionsByUserPromises[userId.toString()][type]) {
              return subscriptionsByUserPromises[userId.toString()][type];
            }

            if(!subscriptionsByUserPromises[userId.toString()]) {
              subscriptionsByUserPromises[userId.toString()] = { };
            }

            subscriptionsByUserPromises[userId.toString()][type] = restListService
              .getMergedFilteredItems('subscriptions', 'user eq ' + userId.toString() + (type !== '*' ? ' and type eq \'' + type + '\'' : ''))
              .then(
                function(res) {
                  if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
                    console.log('ERROR: Could not getSubscriptionsByUser: The response received was not in the expected format:');
                    console.log(res);

                    return $q.reject(gettext('Could not retrieve the subscriptions: The response received was not in the expected format.'));
                  }

                  return res.data.d.results.map(subscriptionObjFromREST);
                },
                function(err) {
                  console.log('getSubscriptionsByUser: An ERROR occured:');
                  console.log(err);
                  return $q.reject(gettext('An error occured while trying to retrieve subscriptions for this user.'));
                }
              );

            return subscriptionsByUserPromises[userId.toString()][type];
          }
        );
    };


    return subServ;
  }])




  /**
   * @ngdoc service
   * @name socialServices.interactionService
   * @description
   *
   * This service manages "interactions", which are reactions and comments on other objects in the Dashboard universe
   * (supported at the moment: notes, risk tracker elements and audit forms). They can be attached and monitored and
   * displayed chronologically using a dynamic sidebar view.
   *
   */

  .service('interactionService', ['$q', '$timeout', '$location', 'gettext', 'restListService', 'usersService', 'subscriptionService', 'sidebarService', 'miscServices', function ($q, $timeout, $location, gettext, restListService, usersService, subscriptionService, sidebarService, miscServices) {
    var interServ = {};

    // Cache for interactions by target element. Object key is targetType::targetId.
    var interactionsPromises = {};

    // Cache for interactions by source user. Object key is userId.toString()::sortOldToNew true/false.
    var interactionsByUserPromises = {};

    // Helper function to retrieve the current local URL (is used if no sourceUrl is being specified)
    var fillSourceUrl = function(url) {
      if(url === undefined || url === null) {
        return ('#' + $location.url());
      } else if (url.substr(0, 2) === '##') {
        // This means we only have received a "second hash" and need to take the path from the URL
        return '#' + $location.path() + '#' + url.substr(2);
      } else {
        return url;
      }
    };

    // Helper functions to convert interactionObjs from and to REST objects
    var interactionObjToREST = function(interactionObj) {
      return {
        interactionTime: interactionObj.interactionTime,
        userId: interactionObj.userId,
        targetType: interactionObj.targetType,
        targetId: interactionObj.targetId.toString(),
        interactionType: interactionObj.interactionType,
        content: JSON.stringify(interactionObj.content)
      };
    };

    var interactionObjFromREST = function(restObj) {
      var interactionObj = {
        id: restObj.Id,
        interactionTime: miscServices.parseDatetime(restObj.interactionTime),
        userId: restObj.userId,
        targetType: restObj.targetType,
        targetId: restObj.targetId,
        interactionType: restObj.interactionType,
        content: {}
      };

      try {
        if(restObj.content) {
          interactionObj.content = JSON.parse(restObj.content);
        }
      } catch(e) {
        console.log('An ERROR occured while trying to parse interactionObj content JSON:');
        console.log(e);
        console.log('We continue with an empty content object.');
      }

      return interactionObj;
    };


    /**
     * @ngdoc method
     * @name socialServices.interactionService#addInteraction
     * @methodOf socialServices.interactionService
     * @kind function
     *
     * @description
     * This method Adds the prepared `interactionObj` to the database. Needs to contain the required fields
     * (`targetType`, `targetId`, `interactionType`, `content`). The element `id` has to be unset and will be filled
     * with the ID after it has been added to SharePoint. `interactionTime` and `user` may be unset, in which case
     * they will be filled with the current time and the currently active user, respectively. Will also call the
     * {@link socialServices.subscriptionService#triggerSubs `subscriptionService.triggerSubs`} function to check for
     * and trigger any subscriptions that match this new interaction.
     *
     * If `skipAddWatch` is false or unset, will trigger a `watch` interaction for that user with the given
     * `interactionType` as `reasonType` (by calling {@link socialServices.interactionService#addWatch `addWatch`}).
     *
     *
     * @param {Object}    interactionObj        The interaction that should be added to the database.
     * @param {String=}   sourceUrl             The source URL where this interaction was triggered and which should be
     *                                          linked on the news feed. If empty, the current $location will be used.
     * @param {String=}   targetDescription     A description of the target element of this interaction; only required
     *                                          if `skipAddWatch` is not set to `true`.
     * @param {boolean=}   skipAddWatch         If set to `true`, no `watch` interaction will be added for the source
     *                                          element.
     *
     * @returns {Promise} Returns a promise with the added interaction object (will contain the ID of the new item).
     *
     */

    interServ.addInteraction = function(interactionObj, sourceUrl, targetDescription, skipAddWatch) {
      var interactionSummary;

      sourceUrl = fillSourceUrl(sourceUrl);

      console.log('addInteraction called.');
      console.log([ interactionObj, sourceUrl, targetDescription, skipAddWatch ]);

      return usersService
        .fillUserId(interactionObj.userId)
        .then(
          function(_userId) {
            // Fill user ID
            interactionObj.userId = _userId;

            // Fill the interactionTime
            if(!interactionObj.interactionTime) {
              interactionObj.interactionTime = new Date();
            }

            // Add the interaction object to the database
            return restListService.addItem('interactions', interactionObjToREST(interactionObj));
          }
        )
        .then(
          function(res) {
            if(!res.data || !res.data.d || res.data.d.Id === undefined) {
              console.log('addInteraction: An unexpected response was received:');
              console.log(res);

              return $q.reject(gettext('An unexpected response was received while trying to add a new interaction item.'));
            }

            // Ok, now we have to clean any promises that might contain this interaction object
            interactionsPromises[interactionObj.targetType + '::' + interactionObj.targetId.toString()] = undefined;
            interactionsByUserPromises[interactionObj.userId.toString() + '::true'] = undefined;
            interactionsByUserPromises[interactionObj.userId.toString() + '::false'] = undefined;

            // We set the ID of the new object
            interactionObj.id = res.data.d.Id;

            // Ok, now we have to update the interactionSummary of the target object...
            return interServ.updateInteractionSummary(interactionObj);
          }
        )
        .then(
          function(_interactionSummary) {
            interactionSummary = _interactionSummary;
            // TODO TODO: document that interactionSummary will contain for each interaction an array with all userIds of the people who
            //            have interacted...

            // Now we need to trigger any subscriptions to the interactions of this object...
            return subscriptionService.triggerSubs('interaction',
              { targetType: interactionObj.targetType, targetId: interactionObj.targetId },
              interactionObj.targetType,
              interactionObj.targetId,
              interactionObj.id,
              interServ.addSidebarToURL(sourceUrl, interactionObj.targetType, interactionObj.targetId, targetDescription),
              { from: interactionObj.userId, interactionObj: interactionObj },
              'new-' + interactionObj.interactionType);
          }
        )
        .then(
          function() {
            // Ok, if skipAddWatch isn't set AND the user doesn't already currently watch the object, we need to add a watch for the base object...
            if(skipAddWatch !== true &&
              (!Array.isArray(interactionSummary.watch) || interactionSummary.watch.indexOf(interactionObj.userId.toString()) < 0)) {
              return interServ.addWatch(interactionObj.userId, interactionObj.targetType, interactionObj.targetId, targetDescription, sourceUrl);
            }
          }
        )
        .then(
          function() {
            // Ok, we need to notify any listeners for new interactions.
            // Ok, we need to notify any listeners for changed interactions.
            interactionListeners.forEach(
              function(d) {
                if((d.targetType === undefined || d.targetType === null || d.targetType === interactionObj.targetType) &&
                  (d.targetId === undefined || d.targetId === null || d.targetId.toString() === interactionObj.targetId.toString())) {
                  d.listener('add', interactionObj);
                }
              }
            );

            // Done. We return the amended interaction object.
            return interactionObj;
          },
          function(err) {
            console.log('An ERROR occured while trying to addInteraction:');
            console.log([ interactionObj, sourceUrl, targetDescription, skipAddWatch ]);
            console.log(err);

            return $q.reject(gettext('An error occured while trying to add a new interaction to this element.'));
          }
        );
    };




    /**
     * @ngdoc method
     * @name socialServices.interactionService#getInteractionSummary
     * @methodOf socialServices.interactionService
     * @kind function
     *
     * @description
     * This method retrieves the interactionSummary of a base element (note, risk, auditForm) identified by the given
     * `targetType` and `targetId`.
     *
     * @param {Object}  targetType    The type of element for which the interactionSummary should be retrieved (currently
     *                                supported: `note`, `risk`, `task`, `auditFormData`).
     * @param {Object}  targetId      The ID of the element for which the interactionSummary should be retrieved.
     *
     *
     * @returns {Promise} Returns a promise with the new interactionSummary.
     *
     */

    var targetTypeToEndpoint = {
      'note': 'notes',
      'risk': 'risks',
      'task': 'task_data',
      'auditFormData': 'audit_forms_data'
    };

    interServ.getInteractionSummary = function(targetType, targetId) {
      // If the target is not supported, we will return undefined (wrapped in a promise)
      if(targetTypeToEndpoint[targetType] === undefined) {
        return $q.resolve(undefined);
      }

      return restListService
        .getItemById(targetTypeToEndpoint[targetType], targetId)
        .then(
          function(res) {
            if(!res.data || !res.data.d || res.data.d.Id === undefined) {
              console.log('getInteractionSummary: An unexpected response was received:');
              console.log(res);

              return $q.reject(gettext('An unexpected response was received while trying to retrieve an interaction summary.'));
            }

            var interactionSummary;
            try {
              interactionSummary = JSON.parse(res.data.d.interactionSummary);
            } catch(e) {
              console.log('An ERROR occured while trying to parse the interactionSummary JSON, setting it to an empty object:');
              console.log(e);
            }

            if(interactionSummary === undefined || interactionSummary === null) {
              interactionSummary = {};
            }

            return interactionSummary;
          },
          function(err) {
            console.log('An ERROR occured while trying to getInteractionSummary(' + targetType + ', ' + targetId + '):');
            console.log(err);

            // If the item simply doesn't exist anymore, we resolve with undefined instead of throwing an error.
            if(err.status === 400 && err.data.error.code === '-2147024809, System.ArgumentException') {
              return;
            }

            return $q.reject(gettext('An error occured while trying to retrieve the interaction list for the target.'));
          }
        );
    };



    /**
     * @ngdoc method
     * @name socialServices.interactionService#updateInteractionSummary
     * @methodOf socialServices.interactionService
     * @kind function
     *
     * @description
     * This method updates the interactionSummary of a base element (note, risk, auditFormData) based on the passed
     * `interactionObj`. If `remove` is set to `true`, the interaction will be removed, otherwise it will be added.
     *
     * Note that there is a special case: the `unwatch` interaction will be handled as a `remove` of a `watch`
     * interaction.
     *
     * @param {Object}    interactionObj        The interaction that should be used to update the interactionSummary.
     * @param {boolean}   remove                If set to `true`, the interaction will be removed from the summary.
     *
     * @returns {Promise} Returns a promise with the new interactionSummary.
     *
     */

    interServ.updateInteractionSummary = function(interactionObj, remove) {
      var _interactionSummary;

      // First we need to retrieve the current interactionSummary...
      return interServ
        .getInteractionSummary(interactionObj.targetType, interactionObj.targetId)
        .then(
          function(interactionSummary) {
            var interactionType = interactionObj.interactionType;
            if(interactionType === 'unwatch') {
              // Special case... "unwatch" is equivalent to removing a "watch"...
              interactionType = 'watch';
              remove = !remove;
            }

            // We clean up the interactionSummary if it is empty.
            if(interactionSummary === undefined || interactionSummary === null) {
              interactionSummary = {};
            }

            // Ok.
            if(remove === true) {
              // We try to remove an existing element.
              if(Array.isArray(interactionSummary[interactionType])) {
                var elIndex = interactionSummary[interactionType].indexOf(interactionObj.userId.toString());
                if(elIndex >= 0) {
                  interactionSummary[interactionType].splice(elIndex, 1);
                }
              }
            } else {
              // We add the userId to the interactionSummary if it does not already exist.
              if(!Array.isArray(interactionSummary[interactionType])) {
                interactionSummary[interactionType] = [ interactionObj.userId.toString() ];
              } else if(interactionSummary[interactionType].indexOf(interactionObj.userId.toString()) < 0) {
                interactionSummary[interactionType].push(interactionObj.userId.toString());
              }
            }

            // Ok, we have made the necessary changes. Now we need to write them to the SharePoint list.
            _interactionSummary = interactionSummary;
            return restListService.updateItem(targetTypeToEndpoint[interactionObj.targetType], interactionObj.targetId, { interactionSummary: JSON.stringify(interactionSummary) });
          }
        )
        .then(
          function() {
            // Done, we return the modified interactionSummary element.
            return _interactionSummary;
          }
        );
    };



    /**
     * @ngdoc method
     * @name socialServices.interactionService#addWatch
     * @methodOf socialServices.interactionService
     * @kind function
     *
     * @description
     * This method will create a new `watch` interaction (using `addInteraction`) and a corresponding subscription
     * to the element.
     *
     *
     * @param {Object}   userId                 The user ID of the user for which the watch should be added. If left
     *                                          undefined, this method will add the watch for the currently active user.
     * @param {Object}   targetType             The type of element for which the watch should be added (currently
     *                                          supported: `note`, `risk`, `auditFormData`).
     * @param {Object}   targetId               The ID of the element for which the watch should be added.
     * @param {String}   targetDescription      A description of the element for which this watch is being added.
     *                                          Will be displayed in any newsfeed entries triggered by this watch.
     * @param {String=}  sourceUrl              The source URL for the watch interaction, which will be linked on the
     *                                          newsfeed. If not given, will default to the current URL visible in the
     *                                          dashboard.
     *
     * @returns {Promise} Returns a promise that resolves to the created interaction object.
     *
     */

    interServ.addWatch = function(userId, targetType, targetId, targetDescription, sourceUrl) {
      // We fill the source URL with the local URL if it has not been given
      sourceUrl = fillSourceUrl(sourceUrl);

      console.log('addWatch called.');
      console.log([ userId, targetType, targetId, targetDescription, sourceUrl ]);

      // We prepare the interaction object for this watch action
      var interactionObj = {
        targetType: targetType,
        targetId: targetId,
        interactionType: 'watch',
        content: { } // The watch object does not have any content
      };

      // We fill the user ID...
      return usersService
        .fillUserId(userId)
        .then(
          function(_userId) {
            interactionObj.userId = _userId;

            // We add the watch interaction object...
            return interServ.addInteraction(interactionObj, sourceUrl, targetDescription,true);
          }
        )
        .then(
          function(_interactionObj) {
            // We store the newly created interactionObject.
            interactionObj = _interactionObj;

            // And we add a new subscription for the given object for the given user
            // Step 1: For any interactions on that element...
            return subscriptionService.addSubscription(interactionObj.userId,
              'interaction',
              { targetType: interactionObj.targetType, targetId: interactionObj.targetId },
              targetDescription);
          }
        )
        .then(
          function() {
            // Does the watched object also support subscriptions on itself? If yes, we
            // need to add one of those as well.
            if(subscriptionService.supportedSubscriptions().indexOf(interactionObj.targetType) >= 0) {
              return subscriptionService.addSubscription(interactionObj.userId,
                interactionObj.targetType,
                interactionObj.targetId,
                targetDescription);
            }
          }
        )
        .then(
          function() {
            // Done, we return the completed interactionObj.
            return interactionObj;
          }
        );
    };



    /**
     * @ngdoc method
     * @name socialServices.interactionService#addUnwatch
     * @methodOf socialServices.interactionService
     * @kind function
     *
     * @description
     * This method will unwatch the object (by adding an `unwatch` interaction and removing the subscription
     * to the object).
     *
     * @param {Object=} userId        The user ID of the user for which the unwatch should be added. If left
     *                                undefined, this method will add the unwatch for the currently active user.
     * @param {Object}  targetType    The type of element for which the unwatch should be added (currently
     *                                supported: `note`, `risk`, `auditFormData`).
     * @param {Object}  targetId      The ID of the element for which the unwatch should be added.
     * @param {String=} sourceUrl     The source URL where the unwatch interaction was triggered. Will be linked
     *                                to on any newsfeed entries generated due to this. If unset, will be set to
     *                                the currently visible URL in the dashboard.
     *
     * @returns {Promise} Returns a promise that resolves to the created interaction object.
     *
     */

    interServ.addUnwatch = function(userId, targetType, targetId, sourceUrl) {
      // We fill the source URL with the local URL if it has not been given
      sourceUrl = fillSourceUrl(sourceUrl);

      console.log('addUnwatch called.');
      console.log([ userId, targetType, targetId, sourceUrl ]);

      // We prepare the interaction object for this unwatch action
      var interactionObj = {
        targetType: targetType,
        targetId: targetId,
        interactionType: 'unwatch',
        content: { } // The unwatch object does not have any content
      };

      // We fill the user ID...
      return usersService
        .fillUserId(userId)
        .then(
          function(_userId) {
            interactionObj.userId = _userId;

            // We add the unwatch interaction object...
            return interServ.addInteraction(interactionObj, sourceUrl, undefined,true);
          }
        )
        .then(
          function(_interactionObj) {
            // We store the newly created interactionObject.
            interactionObj = _interactionObj;

            // And we remove any subscriptions for the given object for the given user...
            // Step 1: For any interactions on that element...
            return subscriptionService.removeSubscriptionsByTarget('interaction',
              { targetType: interactionObj.targetType, targetId: interactionObj.targetId },
              interactionObj.userId);
          }
        )
        .then(
          function() {
            // Does the watched object also support subscriptions on itself? If yes, we
            // need to remove this one as well.
            if(subscriptionService.supportedSubscriptions().indexOf(interactionObj.targetType) >= 0) {
              return subscriptionService.removeSubscriptionsByTarget(interactionObj.targetType,
                interactionObj.targetId,
                interactionObj.userId);
            }
          }
        )
        .then(
          function() {
            // Done, we return the completed interactionObj.
            return interactionObj;
          }
        );
    };



    /**
     * @ngdoc method
     * @name socialServices.interactionService#removeInteraction
     * @methodOf socialServices.interactionService
     * @kind function
     *
     * @description
     * This method will remove the interaction `interactionObj` from the system. Will also call
     * {@link socialServices.subscriptionService#removeNewsBySource `subscriptionService.removeNewsBySource`} to remove
     * any newsfeed items associated with this interaction from the system.
     *
     * @param {Object} interactionObj    The interaction object of the interaction to be removed from the system.
     *
     * @returns {Promise} Returns a promise which resolves to `true` if the interaction has been removed.
     *
     */

    interServ.removeInteraction = function(interactionObj) {
      console.log('removeInteraction called.');
      console.log([ interactionObj ]);

      // In a first step, we remove any newsfeed mentions of this interaction...
      return subscriptionService
        .removeNewsBySource(interactionObj.targetType, interactionObj.targetId, interactionObj.id)
        .then(
          function() {
            // Ok. Now we remove the interaction itself.
            return restListService.deleteItem('interactions', interactionObj.id);
          }
        )
        .then(
          function() {
            // Ok, now we have to clean any promises that might contain this interaction object
            interactionsPromises[interactionObj.targetType + '::' + interactionObj.targetId.toString()] = undefined;
            interactionsByUserPromises[interactionObj.userId.toString() + '::true'] = undefined;
            interactionsByUserPromises[interactionObj.userId.toString() + '::false'] = undefined;

            // Ok, we need to notify any listeners for changed interactions.
            interactionListeners.forEach(
              function(d) {
                if((d.targetType === undefined || d.targetType === null || d.targetType === interactionObj.targetType) &&
                  (d.targetId === undefined || d.targetId === null || d.targetId.toString() === interactionObj.targetId.toString())) {
                  d.listener('remove', interactionObj);
                }
              }
            );

            // Ok. Now we update the interactionSummary.
            return interServ.updateInteractionSummary(interactionObj, true);
          }
        )
        .catch(
          function(err) {
            console.log('An ERROR occured while trying to removeInteraction:');
            console.log(interactionObj);
            console.log(err);

            return $q.reject(gettext('An error occured while trying to remove an interaction.'));
          }
        );
    };


    /**
     * @ngdoc method
     * @name socialServices.interactionService#removeInteractionsByDetails
     * @methodOf socialServices.interactionService
     * @kind function
     *
     * @description
     * This method will remove all interactions of the given type from the given user on the given object from the
     * system. Will also remove any newsfeed items associated with this interaction from the system.
     *
     * @param {String}   interaction  The interaction type that should be removed.
     * @param {integer=} userId       The user ID for which interactions should be removed. If unset, will be filled
     *                                with the currently active user. If set to `'*'`, interactions
     *                                will be retrieved irrespective of user.
     * @param {String}   targetType   The type of the target of the interaction to be removed (e.g. `notes`, `risk`
     *                                etc.).
     * @param {String}   targetId     The ID of the target of the interaction to be removed.
     *
     * @returns {Promise} Returns a promise which resolves to `true` if the interaction has been removed and `false`
     *                    if no such interaction has been found.
     *
     */

    interServ.removeInteractionsByDetails = function(interaction, userId, targetType, targetId) {
      // fill user ID
      return interServ
        .getInteractionsByDetails(interaction, userId, targetType, targetId)
        .then(
          function(interactionObjs) {
            // Empty? Then return false.
            if(!Array.isArray(interactionObjs) || interactionObjs.length < 1) {
              return false;
            }

            // We are going to remove all matching entries.
            var deletePromise = $q.resolve();
            interactionObjs.forEach(
              function(interactionObj) {
                deletePromise = deletePromise.then(
                  function() {
                    return interServ.removeInteraction(interactionObj);
                  }
                );
              }
            );

            return deletePromise.then(function() { return true; });
          }
        );
    };




    /**
     * @ngdoc method
     * @name socialServices.interactionService#removeInteractionsByTarget
     * @methodOf socialServices.interactionService
     * @kind function
     *
     * @description
     * This method will remove all interactions associated to the given target, irrespective of type and source user.
     *
     * @param {String}   targetType   The type of the target of the interaction to be removed (e.g. `notes`, `risk`
     *                                etc.).
     * @param {String}   targetId     The ID of the target of the interaction to be removed.
     *
     * @returns {Promise} Returns a promise which resolves to `true` if the interactions have been removed or `false`
     *                    if there are no interactions for this target.
     *
     */

    interServ.removeInteractionsByTarget = function(targetType, targetId) {
      return interServ
        .getInteractions(targetType, targetId)
        .then(
          function(interactionObjs) {
            // Empty? Then return false.
            if(!Array.isArray(interactionObjs) || interactionObjs.length < 1) {
              return false;
            }

            // We are going to remove all matching entries.
            var deletePromise = $q.resolve();
            interactionObjs.forEach(
              function(interactionObj) {
                deletePromise = deletePromise.then(
                  function() {
                    return interServ.removeInteraction(interactionObj);
                  }
                );
              }
            );

            return deletePromise.then(function() { return true; });
          }
        );
    };



    /**
     * @ngdoc method
     * @name socialServices.interactionService#updateInteraction
     * @methodOf socialServices.interactionService
     * @kind function
     *
     * @description
     * This method will update the given interaction `interactionObj`. Note that changes to interactions will _not_
     * trigger any subscriptions! In general, interactions should not be updated but removed and re-created if
     * necessary.
     *
     *
     * @param {Object} interactionObj     The interaction object with the updated content for the interaction. Most
     *                                    contain a valid `id` value.
     *
     * @returns {Promise} Returns a promise which resolves to the updated interaction object.
     *
     */

    interServ.updateInteraction = function(interactionObj) {
      // Ok, this is very barebones, we just call the list service and return the original object.
      return restListService
        .updateItem('interactions', interactionObj.id, interactionObj)
        .then(
          function() {
            // Ok, now we have to clean any promises that might contain this interaction object
            interactionsPromises[interactionObj.targetType + '::' + interactionObj.targetId.toString()] = undefined;
            interactionsByUserPromises[interactionObj.userId.toString() + '::true'] = undefined;
            interactionsByUserPromises[interactionObj.userId.toString() + '::false'] = undefined;

            // Ok, let's return the original interaction object.
            return interactionObj;
          }
        );
    };


    /**
     * @ngdoc method
     * @name socialServices.interactionService#getInteractions
     * @methodOf socialServices.interactionService
     * @kind function
     *
     * @description
     * This method retrieves all interactions for the given target. If `sortOldToNew` is `true`, oldest interactions
     * will be retrieved first. If `sortOldToNew` is unset or false, interactions will be ordered from new-to-old.
     *
     *
     * @param {Object}  targetType      The type of element for which interactions should be retrieved (currently
     *                                  supported: `note`, `risk`, `auditFormData`).
     * @param {Object}  targetId        The ID of the element for which interactions should be retrieved.
     * @param {Object=} sortOldToNew    By default, newest interactions will be listed first. If `sortOldToNew` is
     *                                  explicitly set to `true`, oldest interactions will be retrieved first.
     *
     * @returns {Promise} Returns a promise which resolves to an array of matching interactions objects.
     *
     */

    interServ.getInteractions = function(targetType, targetId, sortOldToNew) {
      // Do we already have an existing promise? Than we return this one (unless it is older than 10 minutes).
      if(interactionsPromises[targetType + '::' + targetId] && interactionsPromises[targetType + '::' + targetId].age > Date.now() - 600000) {
        return interactionsPromises[targetType + '::' + targetId].promise;
      }

      // Otherwise we need to set a new promise...
      interactionsPromises[targetType + '::' + targetId] = { age: Date.now() };
      interactionsPromises[targetType + '::' + targetId].promise = restListService
        .getMergedFilteredItems('interactions',
          'targetType eq \'' + targetType + '\' and targetId eq \'' + targetId + '\'',
          undefined,
          undefined,
          (sortOldToNew ? 'interactionTime asc' : 'interactionTime desc')
        )
        .then(
          function(res) {
            if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
              console.log('ERROR: Could not getInteractions: The response received was not in the expected format:');
              console.log(res);

              return $q.reject(gettext('Could not retrieve interactions related to this element: The response received was not in the expected format.'));
            }

            return res.data.d.results.map(interactionObjFromREST);
          }
        );

      return interactionsPromises[targetType + '::' + targetId].promise;
    };




    /**
     * @ngdoc method
     * @name socialServices.interactionService#getInteractionsByDetails
     * @methodOf socialServices.interactionService
     * @kind function
     *
     * @description
     * This method retrieves all interactions of the given `interaction` type, from the given `userId` and for the
     * given target (identified by `targetType` and `targetId`).
     *
     * As this method is rarely used and should always return the most up-to-date data, requests are not cached.
     *
     *
     * @param {String}    interaction      The interaction type which should be retrieved.
     * @param {integer=}  userId           The user ID for which interactions should be retrieved. if unset, the
     *                                     ID of the currently active user will be used. If set to `'*'`, interactions
     *                                     will be retrieved irrespective of user.
     * @param {String}    targetType       The type of element for which interactions should be retrieved (currently
     *                                     supported: `note`, `risk`, `auditFormData`).
     * @param {String}    targetId         The ID of the element for which interactions should be retrieved.
     *
     * @returns {Promise} Returns a promise which resolves to an array of matching interactions objects.
     *
     */

    interServ.getInteractionsByDetails = function(interaction, userId, targetType, targetId) {
      return usersService.fillUserId(userId)
        .then(
          function(_userId) {
            userId = _userId;

            return restListService
              .getFilteredItems('interactions',
                'interactionType eq \'' + interaction + '\'' + (userId === '*' ? '' : ' and user eq ' + userId.toString()) + ' and targetType eq \'' + targetType + '\' and targetId eq \'' + targetId + '\''
              )
              .then(
                function(res) {
                  if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
                    console.log('ERROR: Could not getInteractionsByDetails: The response received was not in the expected format:');
                    console.log(res);

                    return $q.reject(gettext('Could not retrieve interactions for this data combination: The response received was not in the expected format.'));
                  }

                  return res.data.d.results.map(interactionObjFromREST);
                }
              );
          }
        );
    };



    /**
     * @ngdoc method
     * @name socialServices.interactionService#getInteractionsByUser
     * @methodOf socialServices.interactionService
     * @kind function
     *
     * @description
     * This method retrieves up to `limit` (default = 60) interactions for the given user, starting at interactions
     * older or newer (depending on `sortOldToNew`) than the one with ID `startAfter`. If `userId` is undefined, will
     * retrieve interactions for the current user.
     *
     *
     * @param {Object} userId         The user ID of the user for which interactions should be retrieved. If left
     *                                undefined, this method will retrieve interactions for the active user.
     * @param {Object} limit          The number of interactions to be retrieved (default if unset is 60).
     * @param {Object} startAfter     Only interactions newer (if `sortOldToNew` = true) or older (otherwise) than the
     *                                interaction with the given ID will be displayed.
     * @param {Object} sortOldToNew   The sort order for interactions; by default newest interactions will be retrieved
     *                                first. If `sortOldToNew` is set to `true`, oldest interactions will be retrieved
     *                                first.
     *
     * @returns {Promise} Returns a promise which resolves to an array of matching interactions objects.
     *
     */

    interServ.getInteractionsByUser = function(userId, limit, startAfter, sortOldToNew) {
      return usersService.fillUserId(userId)
        .then(
          function(_userId) {
            userId = _userId;
            limit = (limit ? limit : 60);
            startAfter = (startAfter ? startAfter : 0);
            var promiseKey = userId.toString() + '::' + (sortOldToNew ? 'true' : 'false');

            // Do we already have an existing promise that covers the whole limit? Than we return this one (unless it is older than 10 minutes).
            if(interactionsByUserPromises[promiseKey]) {
              if(interactionsByUserPromises[promiseKey].age > Date.now()-600000 && interactionsByUserPromises[promiseKey].limit >= limit) {
                if((sortOldToNew && interactionsByUserPromises[promiseKey].startAfter <= startAfter) ||
                  (!sortOldToNew && interactionsByUserPromises[promiseKey].startAfter >= startAfter)) {
                  return interactionsByUserPromises[promiseKey].promise
                    .then(function(interactionObjs) {
                      // We need to filter them to return not too many objects and limit them to the right number.
                      return interactionObjs
                        .filter(
                          function(d) {
                            return ((sortOldToNew && d.id > startAfter) || (!sortOldToNew && d.id < startAfter));
                          }
                        )
                        .slice(0, limit);
                    });
                }
              }
            }

            interactionsByUserPromises[promiseKey] = {
              age: Date.now(),
              limit: limit,
              startAfter: startAfter
            };

            interactionsByUserPromises[promiseKey].promise = restListService
              .getMergedFilteredItems('interactions',
                'user eq \'' + userId + '\'' + (!startAfter ? ' and Id ' + (sortOldToNew ? 'Gt' : 'Lt') + ' ' + startAfter.toString() : ''),
                undefined,
                undefined,
                (sortOldToNew ? 'interactionTime asc' : 'interactionTime desc'),
                (limit > 0 ? limit : 60)
              )
              .then(
                function(res) {
                  if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
                    console.log('ERROR: Could not getInteractionsByUser: The response received was not in the expected format:');
                    console.log(res);

                    return $q.reject(gettext('Could not retrieve interactions of this user: The response received was not in the expected format.'));
                  }

                  return res.data.d.results.map(interactionObjFromREST);
                }
              );

            return interactionsByUserPromises[promiseKey].promise;
          }
        );
    };




    /**
     * @ngdoc method
     * @name socialServices.interactionService#showSidebar
     * @methodOf socialServices.interactionService
     * @kind function
     *
     * @description
     * This method displays a sidebar with the interactions for the given target (using the `interactionList`
     * directive). Interactions will be shown "old-to-new". By default, at most 30 (newest) interactions will be
     * displayed, but it is possible to show additional (older) messages too. (This is implemented client-side,
     * as the `interactionService.getInteractions` always retrieves all interactions for the given target.)
     *
     * (This functionality uses the sidebar features within the Dashboard, so we do not need to re-invent the wheel
     * here.)
     *
     * At the bottom there will be a comment field to directly add new comments, and icons to add new interactions.
     *
     *
     * @param {String} targetType          The type of element for which interactions should be displayed (currently
     *                                     supported: `note`, `risk`, `auditFormData`).
     * @param {String} targetId            The ID of the element for which interactions should be displayed.
     * @param {String} targetDescription   A description of the target element for which the sidebar should be
     *                                     displayed (only required if `enableNew` is `true`).
     * @param {integer=}  limit            If specified, only `limit` interactions will be displayed. If `allowMore`
     *                                     is set to `true`, the user is allowed to load additional entries, which
     *                                     will also be retrieved in `limit`-sized batches. If `limit` is omitted,
     *                                     it will default to 30.
     * @param {boolean=}  sortOldToNew     If set to `true`, oldest entries will be displayed first. Otherwise,
     *                                     newest entries will be displayed first.
     * @param {boolean=}  allowMore        If set to `true`, the user will have the option to load additional
     *                                     interactions.
     * @param {boolean=}  enableNew        If set to `true`, the list will contain a comments box that allows to
     *                                     create new comments in that view.
     * @param {String=}   sourceUrl        If specified the source URL that will be sent together with any
     *                                     interactions created in this list. If unset, the current Dashboard
     *                                     URL will be used.
     *
     * @returns {Promise} Returns `true` after the sidebar has been opened.
     *
     */

    var sidebarIsOpen = false;
    interServ.showSidebar = function(targetType, targetId, targetDescription, limit, sortOldToNew, allowMore, enableNew, sourceUrl) {
      console.log('showSidebar called.');
      console.log([ targetType, targetId, targetDescription, limit, sortOldToNew, allowMore, enableNew, sourceUrl ]);

      if(sidebarIsOpen) {
        // We need to close it first.
        console.log('We unload the old sidebar first...');
        sidebarService.unload();
      }
      sidebarIsOpen = true;

      // We wrap the open in a $timeout call so that the unload can be processed first...
      // TODO: The *right* approach would be for interactionList to watch for and support changes of targetId and targetType...
      return $timeout(
        function() {
          console.log('Opening sidebar...');
          return sidebarService.open(
            'common/services/interactionsSidebar.tmpl.html',
            {
              targetType: targetType,
              targetId: targetId,
              targetDescription: targetDescription,
              limit: limit,
              sortOldToNew: sortOldToNew,
              allowMore: allowMore,
              enableNew: enableNew,
              sourceUrl: sourceUrl,
              closeSidebar: interServ.closeSidebarViaLink
            }
          );
        }
      );

    };



    /**
     * @ngdoc method
     * @name socialServices.interactionService#hideSidebar
     * @methodOf socialServices.interactionService
     * @kind function
     *
     * @description
     * This method hides the interactions sidebar.
     *
     *
     * @returns {Promise} Returns `true` after the sidebar has been close.
     *
     */

    interServ.hideSidebar = function() {
      if(sidebarIsOpen) {
        sidebarIsOpen = false;

        // Unload the sidebar
        return sidebarService.unload();
      }
    };



    /**
     * @ngdoc method
     * @name socialServices.interactionService#openSidebarViaLink
     * @methodOf socialServices.interactionService
     * @kind function
     *
     * @description
     * This method displays a sidebar with the interactions for the given target (using the `interactionList`
     * directive). It does not open the sidebar directly, but changes the browser URL to link to the sidebar
     * query parameters. This will then trigger the opening of the sidebar.
     *
     *
     * @param {String} targetType          The type of element for which interactions should be displayed (currently
     *                                     supported: `note`, `risk`, `auditFormData`).
     * @param {String} targetId            The ID of the element for which interactions should be displayed.
     * @param {String} targetDescription   A description of the target element for which the sidebar should be
     *                                     displayed.
     * @param {String=}   sourceUrl        If specified the source URL that will be sent together with any
     *                                     interactions created in this list. If unset, the current Dashboard
     *                                     URL will be used.
     *
     *
     * @returns {boolean} Returns `true` after the URL has been changed.
     *
     */

    interServ.openSidebarViaLink = function(targetType, targetId, targetDescription, sourceUrl) {
      var searchObject = $location.search();
      if(searchObject.sidebar === 'interaction' &&
         searchObject.targetId === targetId.toString() &&
         searchObject.targetType === targetType) {
        // The sidebar is already specified in the URL, so we need to open it directly
        return interServ.showSidebar(targetType, targetId, targetDescription, undefined, true, true, true, sourceUrl);
      }

      // Change the URL
      $location.search('sidebar', 'interaction');
      $location.search('targetId', targetId);
      $location.search('targetType', targetType);
      $location.search('targetDescription', targetDescription);
      $location.search('sourceUrl', sourceUrl);

      return true;
    };



    /**
     * @ngdoc method
     * @name socialServices.interactionService#closeSidebarViaLink
     * @methodOf socialServices.interactionService
     * @kind function
     *
     * @description
     * This method closes an interaction sidebar. It does not do so directly, but indirectly
     * by changing the query parameters of the URL.
     *
     * @returns {boolean} Returns `true` after the URL has been changed.
     *
     */

    interServ.closeSidebarViaLink = function() {
      var searchObject = $location.search();
      if(!searchObject.sidebar) {
        // The sidebar is not given in the URL, so we need to close it directly after all
        return interServ.hideSidebar();
      }

      // Otherwise we can just change the URL
      $location.search('sidebar', undefined);
      $location.search('targetId', undefined);
      $location.search('targetType', undefined);
      $location.search('targetDescription', undefined);
      $location.search('sourceUrl', undefined);

      return true;
    };



    /**
     * @ngdoc method
     * @name socialServices.interactionService#addSidebarToURL
     * @methodOf socialServices.interactionService
     * @kind function
     *
     * @description
     * This method adds the sidebar query parameters to a given Dashboard-internal URL.
     *
     *
     * @param {String}   url               The input URL to which the query parameters should be added
     * @param {String}   targetType        The type of element for which interactions should be displayed (currently
     *                                     supported: `note`, `risk`, `auditFormData`).
     * @param {String}   targetId          The ID of the element for which interactions should be displayed.
     * @param {String}   targetDescription A description of the target element for which the sidebar should be
     *                                     displayed.
     * @param {String=}  sourceUrl         If specified the source URL that will be sent together with any
     *                                     interactions created in this list. If unset, the current Dashboard
     *                                     URL will be used.
     *
     *
     * @returns {boolean} Returns the
     *
     */

    interServ.addSidebarToURL = function(url, targetType, targetId, targetDescription, sourceUrl) {
      // If the URL contains a "second hash" (a hash inside the fictual hashbang) we need to move that to the end
      var secondHashIdx = url.indexOf('#', url.indexOf('#')+1);
      var secondHash = (secondHashIdx > 0 ? url.substr(secondHashIdx) : '');

      // Does the URL already have query parameters? If yes, we kill them off.
      var queryMatch = url.match(/\?|%3F/);

      var cutOffAt = (queryMatch ? queryMatch.index : (secondHashIdx > 0 ? secondHashIdx : url.length));

      console.log('addSidebarToURL called:');
      console.log([ url, targetType, targetId, targetDescription, sourceUrl ]);
      console.log([ secondHashIdx, secondHash, queryMatch, cutOffAt ]);

      return url.substr(0, cutOffAt) +
        '?sidebar=interaction&targetType=' + encodeURIComponent(targetType) +
        '&targetId=' + encodeURIComponent(targetId) +
        '&targetDescription=' + encodeURIComponent(targetDescription) +
        (sourceUrl ? '&sourceUrl=' + encodeURIComponent(sourceUrl) : '') +
        secondHash;
    };


    /**
     * @ngdoc method
     * @name socialServices.interactionService#registerInteractionListener
     * @methodOf socialServices.interactionService
     * @kind function
     *
     * @description
     * This method registers a new listener that will be called whenever a new interaction is added or an
     * existing interaction is removed.
     *
     * @param {Function}  listener    The listener that will be called whenever a new interaction is added or an
     *                                existing interaction is removed. Will be passed `'add'` or `'removed'` as first
     *                                parameter and the relevant `interactionObj` as second parameter.
     * @param {String=}   targetType  Optional; if given will only be triggered for interactions with the given
     *                                `targetType`.
     * @param {String=}   targetId    Optional; if given will only be triggered for interactions with the given
     *                                `targetId`.
     *
     * @returns {boolean}   Returns `true` if listener has been added.
     *
     */


    var interactionListeners = [];
    interServ.registerInteractionListener = function(listener, targetType, targetId) {
      interactionListeners.push({
        listener: listener,
        targetType: targetType,
        targetId: targetId
      });

      return true;
    };




    /**
     * @ngdoc method
     * @name socialServices.interactionService#removeInteractionListener
     * @methodOf socialServices.interactionService
     * @kind function
     *
     * @description
     * This method removes the given interaction listener.
     *
     * @param {Function}  listener    The listener that will be removed (you need to pass in the original function as
     *                                we will compare the objects).
     *
     * @returns {boolean}   Returns `true` if listener has been removed.
     *
     */

    interServ.removeInteractionListener = function(listener) {
      var listenerIdx = interactionListeners.findIndex(
        function(d) {
          return (d.listener === listener);
        }
      );

      if(listenerIdx < 0) {
        return false;
      } else {
        interactionListeners.splice(listenerIdx, 1);
        return true;
      }
    };


    return interServ;
  }])
;

