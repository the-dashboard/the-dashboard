/* jshint strict:false */
/* globals saveAs */

/**
 * @ngdoc overview
 * @name fileServices
 * @description
 *
 * This module exposes the FileSaver library as an AngularJS service.
 */

angular.module('fileServices', [])

  /**
   * @ngdoc service
   * @name fileServices.FileSaver
   * @description
   *
   * This service provides a wrapper around the FileSaver library, which provides an easy way to present blobs
   * as downloads to the user.
   *
   */

  .factory('FileSaver', function() {
    return { 'saveAs': saveAs };
  });

