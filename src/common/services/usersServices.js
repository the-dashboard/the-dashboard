'use strict';

/**
 * @ngdoc overview
 * @name usersServices
 * @description
 *
 * This module provides services to retrieve information the currently logged in and other users registered on
 * SharePoint. Used to resolve user IDs to their names and to identify which access rights a given user has.
 *
 */

angular.module('usersServices', ['gettext', 'restServices'])


/**
 * @ngdoc filter
 * @name usersServices#initials
 * @description
 *
 * The initials filter transforms the full name of a user into its initials.
 *
 * It expects name in the format "Lastname, Firstname Firstname Firstname".
 *
 */

  .filter('initials', function() {
    return function(input) {
      if(input === undefined || input === '') { return ''; }
      if(!angular.isString(input)) { return input.toString(); }  // Don't modify if it is e.g. a user ID

      var initials = '';
      var lastFirstName = input.split(", ");

      if(lastFirstName.length < 2) {
        // Workaround if there is no comma in the input.
        input.split(" ").forEach(function(word) {
          if(word.length >= 1) {
            initials += word[0].toUpperCase();
          }
        });
      } else {
        // Expects name in format "Lastname, Firstname Firstname Firstname".
        lastFirstName[1].split(" ").forEach(function(word) {
          if(word.length >= 1) {
            initials += word[0].toUpperCase();
          }
        });

        initials += lastFirstName[0][0].toUpperCase();
      }
      return initials;
    };
  })



  /**
   * @ngdoc filter
   * @name usersServices#userIdToName
   * @description
   *
   * The userIdToName filter transforms the user ID of a user to its full name.
   *
   * It expects name in the format "Lastname, Firstname Firstname Firstname".
   *
   */

  .filter('userIdToName', ['usersService', function(usersService) {
    var userIdToNameCache = {};

    var userIdToNameFilter = function(input) {
      if(isNaN(input)) { return input; }   // Not a valid user ID

      if(userIdToNameCache[input] === undefined) {
        // We do not yet have the name for this user ID and thus return the user ID
        // while asynchronously triggering an update
        userIdToNameCache[input] = input;
        usersService.getUserById(input).then(function(user) { userIdToNameCache[input] = user.name; });
      }

      return userIdToNameCache[input];
    };
    userIdToNameFilter.$stateful = true; // Otherwise it won't update when data is received async

    return userIdToNameFilter;
  }])


  /**
   * @ngdoc filter
   * @name usersServices#userIdToInitials
   * @description
   *
   * Combines to `userIdToName` and `initials` filters.
   *
   */

  .filter('userIdToInitials', ['$filter', function($filter) {

    var userIdToInitialsFilter = function(input) {
      return $filter('initials')($filter('userIdToName')(input));
    };
    userIdToInitialsFilter.$stateful = true;

    return userIdToInitialsFilter;
  }])



  /**
   * @ngdoc service
   * @name usersServices.usersService
   * @description
   *
   * This service allows  to retrieve information the currently logged in and other users registered on
   * SharePoint. Used to resolve user IDs to their names and to identify which access rights a given user has.
   *
   */

  .factory('usersService', ['restUserService', 'restListService', '$q', 'gettext', function(restUserService, restListService, $q, gettext) {
    var usersServ = {};

    var usersPromises = {};
    var currentUserPromise;
    var isAdminPromises = {};
    var isSiteAdminPromises = {};
    var userSettingsPromises = {};
    var hasRightsPromises = {};

    // A list of all user settings elements, used for creating the SharePoint list entry
    var userSettingsElements = [
      'language',
      'watchNewNote'
    ];


    /**
     * @ngdoc method
     * @name usersServices.usersService#getUserById
     * @methodOf usersServices.usersService
     * @kind function
     *
     * @description
     * This method takes a `userId` of an existing SharePoint user and returns a promise to a JavaScript object
     * with details on this user. The returned object provides the following information:
     *
     * ```
     * {
     *  id: <the queried user ID>,
     *  name: <the name of the user>,
     *  login: <the login name of the user>,
     *  email: <the email address of the user>
     * }
     * ```
     *
     *
     * @param {number} userId The numeric SharePoint user ID of the user to be retrieved.
     *
     * @returns {Promise} Returns a promise that resolves to a JavaScript object with user details.
     *
     */

    usersServ.getUserById = function(userId) {
      var deferred;

      // Is userId valid? Otherwise return rejected promise immediately.
      if(isNaN(userId)) {
        deferred = $q.defer();
        deferred.reject(gettext('An error occurred while loading user data: Provided userId is invalid.'));
        return deferred.promise;
      }

      // Do we already have an active promise? Then we just return this one.
      if(usersPromises[userId]) {
        return usersPromises[userId];
      }

      // Ok, no promise exists yet, so we define a new one.
      deferred = $q.defer();
      usersPromises[userId] = deferred.promise;

      // Ok, now we can actually get the data from SharePoint
      restUserService.getUserById(userId).then(
        function(res) {
          // Are we getting the right data?
          if (!res || !res.data || !res.data.d || !res.data.d.Title) {
            // Reject promise, data returned is invalid.
            deferred.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
          }

          // Ok, return user data.
          deferred.resolve({
            id: res.data.d.Id,
            name: res.data.d.Title,
            login: res.data.d.LoginName,
            email: res.data.d.Email
          });
        },
        function(restErr) {
          // An error occurred, reject promise.
          deferred.reject(gettext('An error occurred:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
        }
      );

      // Ok, lets return our usersPromises.
      return usersPromises[userId];
    };



    /**
     * @ngdoc method
     * @name usersServices.usersService#getUsersByGroup
     * @methodOf usersServices.usersService
     * @kind function
     *
     * @description
     * This method returns all users which are members of the group identified by `groupName`. If no `groupName` has been
     * specified, this method will retrieve all users on the current SharePoint site.
     *
     * @param {String=} groupName  The name of the group whose members should be retrieved. If empty or unset, all users
     *                             on the SharePoint site will be retrieved.
     *
     * @returns {Promise} A promise which resolves to an array of user objects. For each user, at least the following
     *                    fields will be populated: `id`, `name`, `login`, `email`.
     *
     */

    var getUsersByGroupPromises = {};
    usersServ.getUsersByGroup = function(groupName) {
      groupName = (groupName !== undefined && groupName !== null && groupName !== '' ? groupName : '__all__');

      // We already have an existing promise and can just return that one.
      if(getUsersByGroupPromises[groupName]) {
        return getUsersByGroupPromises[groupName];
      }

      // Do we need to get all users?
      var usersRawPromise;
      if(groupName === '__all__') {
        usersRawPromise = restUserService.getAllUsers();
      } else {
        usersRawPromise = restUserService.getUsersByGroup(groupName);
      }

      getUsersByGroupPromises[groupName] = usersRawPromise
        .then(
          function(res) {
            // Are we getting the right data?
            if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
              console.log('An ERROR occured while getUsersByGroup. The obtained data is not in the expected format:');
              console.log(res);

              return $q.reject(gettext('An error occured while trying to get group members: The returned data is not formatted as expect.'));
            }

            return res.data.d.results.map(
              function(d) {
                return {
                  id: d.Id,
                  login: d.LoginName,
                  name: d.Title,
                  email: d.Email
                };
              }
            );
          },
          function(err) {
            console.log('An ERROR was returned by the REST query for getUsersByGroup(' + groupName + '):');
            console.log(err);

            return $q.reject(gettext('An error occured while trying to retrieve group members. Please try again.'));
          }
        );

      return getUsersByGroupPromises[groupName];
    };

    


    /**
     * @ngdoc method
     * @name usersServices.usersService#getCurrentUser
     * @methodOf usersServices.usersService
     * @kind function
     *
     * @description
     * This method returns a promise to a JavaScript object with details on the currently logged in user. The returned
     * object provides the following information:
     *
     * ```
     * {
     *  id: <the user ID of the currently logged in user>,
     *  name: <the name of the user>,
     *  login: <the login name of the user>,
     *  email: <the email address of the user>
     * }
     * ```
     *
     *
     * @returns {Promise} Returns a promise that resolves to a JavaScript object with user details.
     *
     */

    usersServ.getCurrentUser = function() {
      var deferred;

      // Do we already have an active promise? Then we just return this one.
      if(currentUserPromise !== undefined) {
        return currentUserPromise;
      }

      // No, we need a new one.
      deferred = $q.defer();
      currentUserPromise = deferred.promise;

      // Get data from SharePoint.
      restUserService.getCurrentUser().then(
        function(res) {
          // Are we getting the right data?
          if (!res || !res.data || !res.data.d || !res.data.d.Title) {
            // Reject promise, data returned is invalid.
            deferred.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
          }

          // Ok, return user data.
          deferred.resolve({
            id: res.data.d.Id,
            name: res.data.d.Title,
            login: res.data.d.LoginName,
            email: res.data.d.Email
          });
        },
        function(restErr) {
          // An error occurred, reject promise.
          deferred.reject(gettext('An error occurred:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
        }
      );

      // Ok, let's return the promise to the current user.
      return currentUserPromise;
    };



    /**
     * @ngdoc method
     * @name usersServices.usersService#getCurrentUserSettings
     * @methodOf usersServices.usersService
     * @kind function
     *
     * @description
     * This method returns a promise to a JavaScript object with the user settings for user with the given ``userId''.
     * The returned object provides all the fields on the user in the "User Settings" SharePoint list.
     *
     * @param {number=} userId The numeric user ID of the user for which user settings should be retrieved.
     *
     * @returns {Promise} Returns a promise that resolves to a JavaScript object with the user settings list object.
     *
     */

    usersServ.getUserSettings = function(userId) {
      var deferred;

      // Is userId valid? Otherwise return rejected promise immediately.
      if(isNaN(userId)) {
        deferred = $q.defer();
        deferred.reject(gettext('An error occurred while loading user settings: Provided userId is invalid.'));
        return deferred.promise;
      }

      // Do we already have an active promise? Then we just return this one.
      if(userSettingsPromises[userId]) {
        return userSettingsPromises[userId];
      }


      // Ok, no promise exists yet, so we define a new one.
      deferred = $q.defer();
      userSettingsPromises[userId] = deferred.promise;

      // Ok, now we can actually get the data from SharePoint
      // We can use getFilteredItems here as we should only ever get one record (not more than 5000).
      restListService.getFilteredItems('user_settings', 'userId eq ' + userId).then(
        function(res) {
          // TODO: REMOVE DEBUG CODE
          console.log('We received user settings:');
          console.log(res);

          // Are we getting the right data?
          if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
            // Reject promise
            deferred.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
          }

          // If no settings for the user ID exist, we return an empty object.
          if(res.data.d.results.length !== 1) {
            deferred.resolve({ });
          }

          // Ok, we are simply returning the first object which contains all the user settings.
          deferred.resolve(res.data.d.results[0]);

        },
        function(restErr) {
          // TODO: REMOVE DBEUG CODE
          console.log('Error receiving user settings:');
          console.log(restErr);

          deferred.reject(gettext('An error occurred:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
        }
      );

      // Return the created promise
      return userSettingsPromises[userId];
    };



    /**
     * @ngdoc method
     * @name usersServices.usersService#getCurrentUserSettings
     * @methodOf usersServices.usersService
     * @kind function
     *
     * @description
     * This method returns a promise to a JavaScript object with the user settings for the currently logged in user.
     * The returned object provides all the fields on the user in the "User Settings" SharePoint list.
     *
     *
     * @returns {Promise} Returns a promise that resolves to a JavaScript object with the user settings list object.
     *
     */

    usersServ.getCurrentUserSettings = function() {
      return usersServ.getCurrentUser().then(
        function(userData) {
          return usersServ.getUserSettings(userData.id);
        }
      );
    };


    /**
     * @ngdoc method
     * @name usersServices.usersService#fillUserId
     * @methodOf usersServices.usersService
     * @kind function
     *
     * @description
     * This method returns a promise which resolves to a userId: If a value has been passed in, the promise
     * will resolve immediately with that value. Otherwise, the user ID of the currently logged in user will
     * be retrieved and returned.
     *
     * @param {integer} _userId   If given, this user ID will be returned (wrapped in a promise). If undefined
     *                            or null, the user ID of the currently logged in user will be returned.
     *
     * @returns {Promise} Returns a promise that resolves to the provided userID, if any, or to the user ID
     *                    of the currently logged in user.
     *
     */
    usersServ.fillUserId = function(_userId) {
      if(_userId !== undefined && _userId !== null) {
        return $q.resolve(_userId);
      }

      return usersServ
        .getCurrentUser()
        .then(function(user) {
          return user.id;
        });
    };



    /**
     * @ngdoc method
     * @name usersServices.usersService#saveUserSettings
     * @methodOf usersServices.usersService
     * @kind function
     *
     * @description
     * This method stores modified user settings -- which are provided as object in the ``settings'' parameter -- to
     * the relevant SharePoint list.
     *
     * The method extracts only the relevant values from the ``settings'' object, such that you can provide it with
     * the full object obtained by calling ``getCurrentUserSettings''. You can use it to save settings on any user,
     * it will extract the user ID from the object. If the user ID is not set on the object, we will retrieve
     * the user ID of the current user and store a new settings entry for the user.
     *
     * Note: This method will not validate whether the settings correspond to the user executing the method, i.e. it
     * can be used to change an arbitrary user's settings as long as the executing user has write access to the
     * configuration SharePoint list. As this code is fully client-side, any client-side-checking would not stop
     * a determined "attacker" from changing user's settings directly on the SharePoint list, anyways.
     *
     * It would in theory be possible to manage SharePoint access rights on a per-item level on the SharePoint list,
     * restricting other users from changing any settings. However, the effort required and potential side-effects for
     * this are large while the benefits are limited - as we expect The Dashboard to be used within closed user groups
     * which should work towards a common goal and also user settings are non-critical. You could play a prank on your
     * colleague by changing its language settings to a foreign language, but that's about it.
     *
     *
     * @param {Object} settings An object containing all user settings that should be written to the SharePoint list.
     *                          If the object contains a SharePoint item ID, it will update the existing settings item.
     *                          If the item ID is missing, it will create a new item in the settings list. In the latter
     *                          case, if the object contains a ``userIdId'' value, this value is used for assigning the
     *                          settings to the user. If the value is missing, the method obtains the current users'
     *                          user ID and uses it to store the settings object.
     *
     * @returns {Promise} Returns a promise that resolves with the (completed) ``settings'' object as soon as the
     *                    settings have been successfully saved.
     *
     */

    usersServ.saveUserSettings = function(settings) {
      console.log('saveUserSettings called:');
      console.log(settings);

      // Is this an existing item?
      if(settings.Id) {
        // Ok, an existing item needs to have a valid userId, otherwise we will resolve with an error...
        if(!settings.userIdId) {
          return $q.reject(gettext('An existing settings object needs to have a valid userId.'));
        }
      }

      // Do we need to obtain the user ID?
      var obtainUserId;
      if(!settings.userIdId) {
        obtainUserId = usersServ.getCurrentUser()
          .then(
            function(currUser) {
              settings.userIdId = currUser.id;
            }
          );
      } else {
        // Ok, we can skip this.
        obtainUserId = $q.resolve();
      }

      // Ok, let's move on and store the settings in an existing or new SharePoint list entry...
      return obtainUserId
        .then(
          // Ok, we have obtained the userId (or we already had it) and can store the settings in the
          // SharePoint list...
          function() {
            // We need to clean up the element by setting only the "real" settings
            var newSettingsObject = {
              userIdId: settings.userIdId
            };

            userSettingsElements.forEach(function(k) {
              newSettingsObject[k] = settings[k];
            });

            // Is this a new settings item?
            if(!settings.Id) {
              // Create new settings entry.
              return restListService.addItem('user_settings', newSettingsObject)
                .then(
                  function(res) {
                    // Ok. We can just return the new item as it is returned by the SharePoint server.
                    return res.data.d;
                  },
                  function(restErr) {
                    // Ok, someting went wrong with saving the list update... Transform REST error into simple text error.
                    console.log('An error occured: ' + restErr.status + ', ' + restErr.statusText);
                    return $q.reject(gettext('An error occurred:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
                  }
                );
            } else {
              // Update existing entry.
              newSettingsObject.Id = settings.Id;
              return restListService.updateItem('user_settings', settings.Id, newSettingsObject)
                .then(
                    function() {
                      // Ok. For changes, the SharePoint server doesn't return anything, so we return our newSettingsObject.
                      return newSettingsObject;
                    },
                    function(restErr) {
                      // Ok, someting went wrong with saving the list update... Transform REST error into simple text error.
                      console.log('An error occured: ' + restErr.status + ', ' + restErr.statusText);
                      return $q.reject(gettext('An error occurred:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
                    }
              );
            }

            // Done.
          }
        );
    };



    /**
     * @ngdoc method
     * @name usersServices.usersService#isAdmin
     * @methodOf usersServices.usersService
     * @kind function
     *
     * @description
     * This method identifies whether the currently logged in user has specific access rights on the SharePoint list
     * "Dashboard - Configuration". At the moment, this list is used to establish administrative rights: Anyone who
     * can modify entries on this list is deemed as an administrator.
     *
     * Access rights in SharePoint's REST API are specified as bitmasks, split up in a 32-bit high and 32-bit low part.
     * See [this list](http://web.archive.org/web/20130904153031/http://www.dctmcontent.com/sharepoint/Articles/Permissions%20and%20Mask%20Values.aspx)
     * for details on the available access rights and their numeric values. You can pass the requested access right
     * using the parameters `highRights` and `lowRights`. The method will then check whether the user's access rights
     * bitmask matches the requested rights.
     *
     * By default (if no parameters are given) it will check for write access to the list.
     *
     *
     * @param {number=} highRights The numeric access rights bitmask that should be checked against the high part
     *                            of the user's access rights.
     *
     * @param {number=} lowRights The numeric access rights bitmask that should be checked against the low part
     *                            of the user's access rights.
     *
     * @returns {Promise} Returns a promise that resolves to a boolean value specifying whether the user has the
     *                    requested access rights on the "Dashboard - Configuration" list.
     *
     */

    usersServ.isAdmin = function(highRights, lowRights) {
      var deferred;

      // Default values - we check for editListItem (=4 decimal) permission on Dashboard - Configuration
      // Alternatively, specific access rights that are to be checked on Dashboard - Configuration can be given as parameters
      // See list: http://web.archive.org/web/20130904153031/http://www.dctmcontent.com/sharepoint/Articles/Permissions%20and%20Mask%20Values.aspx
      if(highRights === undefined) {
        highRights = 0;
      }

      if(lowRights === undefined) {
        lowRights = 4;
      }

      // Do we already have an active promise? Then we just return this one.
      if(isAdminPromises[highRights + '_' + lowRights]) {
        return isAdminPromises[highRights + '_' + lowRights];
      }

      // No, we need a new one.
      deferred = $q.defer();
      isAdminPromises[highRights + '_' + lowRights] = deferred.promise;

      // Get data from SharePoint.
      restUserService.getEffectiveBasePermissions(undefined, 'Dashboard - Configuration').then(
        function(res) {
          // Are we getting the right data?
          if (!res || !res.data || !res.data.d || !res.data.d.EffectiveBasePermissions || res.data.d.EffectiveBasePermissions.Low === undefined) {
            // Reject promise, data returned is invalid.
            deferred.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
          }

          // Ok, does the user have the requested permissions?
          // JSHint doesn't like bitwise and, so we exclude this line from linting...

/* jshint ignore:start */

          deferred.resolve(
            (res.data.d.EffectiveBasePermissions.Low & lowRights) === lowRights &&  // jshint ignore:line
            (res.data.d.EffectiveBasePermissions.High & highRights) === highRights  // jshint ignore:line
          );

/* jshint ignore:end */

        },
        function(restErr) {
          // An error occurred, reject promise.
          deferred.reject(gettext('An error occurred:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
        }
      );

      // Ok, let's return the promise to the current user.
      return isAdminPromises[highRights + '_' + lowRights];
    };


      /**
       * @ngdoc method
       * @name usersServices.usersService#isSiteAdmin
       * @methodOf usersServices.usersService
       * @kind function
       *
       * @description
       * This method identifies whether the currently logged in user has rights to modify lists and document libraries
       * on the SharePoint site the Dashboard is running on. This is used for initializing the lists and libraries
       * required by The Dashboard (also e.g. for release changes) but is not required for day-to-day configuration.
       *
       * Access rights in SharePoint's REST API are specified as bitmasks, split up in a 32-bit high and 32-bit low part.
       * See [this list](http://web.archive.org/web/20130904153031/http://www.dctmcontent.com/sharepoint/Articles/Permissions%20and%20Mask%20Values.aspx)
       * for details on the available access rights and their numeric values. You can pass the requested access right
       * using the parameters `highRights` and `lowRights`. The method will then check whether the user's access rights
       * bitmask matches the requested rights.
       *
       * By default (if no parameters are given) it will check for the right to create new lists.
       *
       *
       * @param {number=} highRights The numeric access rights bitmask that should be checked against the high part
       *                            of the user's access rights.
       *
       * @param {number=} lowRights The numeric access rights bitmask that should be checked against the low part
       *                            of the user's access rights.
       *
       * @returns {Promise} Returns a promise that resolves to a boolean value specifying whether the user has the
       *                    requested access rights on the overall SharePoint site.
       *
       */

      usersServ.isSiteAdmin = function(highRights, lowRights) {
          var deferred;

          // Default values - we check for ManageWeb (=1073741824 decimal) permission on the overall site
          // See list: http://web.archive.org/web/20130904153031/http://www.dctmcontent.com/sharepoint/Articles/Permissions%20and%20Mask%20Values.aspx
          if(highRights === undefined) {
              highRights = 0;
          }

          if(lowRights === undefined) {
              lowRights = 1073741824;
          }

          // Do we already have an active promise? Then we just return this one.
          if(isSiteAdminPromises[highRights + '_' + lowRights]) {
              return isSiteAdminPromises[highRights + '_' + lowRights];
          }

          // No, we need a new one.
          deferred = $q.defer();
          isSiteAdminPromises[highRights + '_' + lowRights] = deferred.promise;

          // Get data from SharePoint.
          restUserService.getSiteUserEffectivePermissions().then(
              function(res) {
                  // Are we getting the right data?
                  if (!res || !res.data || !res.data.d || !res.data.d.GetUserEffectivePermissions || res.data.d.GetUserEffectivePermissions.Low === undefined) {
                      // Reject promise, data returned is invalid.
                      deferred.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
                  }

                  // Ok, does the user have the requested permissions?
                  // JSHint doesn't like bitwise and, so we exclude this line from linting...

                  /* jshint ignore:start */

                  deferred.resolve(
                      (res.data.d.GetUserEffectivePermissions.Low & lowRights) === lowRights &&  // jshint ignore:line
                      (res.data.d.GetUserEffectivePermissions.High & highRights) === highRights  // jshint ignore:line
                  );

                  /* jshint ignore:end */

              },
              function(restErr) {
                  // An error occurred, reject promise.
                  deferred.reject(gettext('An error occurred:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
              }
          );

          // Ok, let's return the promise to the current user.
          return isSiteAdminPromises[highRights + '_' + lowRights];
      };




    /**
     * @ngdoc method
     * @name usersServices.usersService#hasRights
     * @methodOf usersServices.usersService
     * @kind function
     *
     * @description
     * This method identifies whether the currently logged in user has the specified access to the given endpoint.
     * This can be used to determine which views should be display to the user (i.e. to hide functionality if the user
     * only has read-only access or no access at all to the given endpoint).
     *
     * Access rights in SharePoint's REST API are specified as bitmasks, split up in a 32-bit high and 32-bit low part.
     * See [this list](http://web.archive.org/web/20130904153031/http://www.dctmcontent.com/sharepoint/Articles/Permissions%20and%20Mask%20Values.aspx)
     * for details on the available access rights and their numeric values. You can pass the requested access right
     * using the parameters `highRights` and `lowRights`. The method will then check whether the user's access rights
     * bitmask matches the requested rights.
     *
     * By default (if no parameters are given) it will check for write access to the given endpoint.
     *
     *
     * @param {string} endpoint The endpoint to which access by the logged in user should be checked. Set to `undefined`
     *                          if you want to give a `listName` instead.
     *
     * @param {string=} listName The name of the list to which access by the logged in user should be checked. Leave as
     *                          `undefined` if you have given an endpoint instead.
     *
     * @param {number=} highRights The numeric access rights bitmask that should be checked against the high part
     *                            of the user's access rights.
     *
     * @param {number=} lowRights The numeric access rights bitmask that should be checked against the low part
     *                            of the user's access rights.
     *
     * @returns {Promise} Returns a promise that resolves to a boolean value specifying whether the user has
     *                    write access on the given endpoint.
     *
     */

    usersServ.hasRights = function(endpoint, listName, highRights, lowRights) {
      var deferred;

      // Default values - we check for editListItem (=4 decimal) permission on Dashboard - Configuration
      // Alternatively, specific access rights that are to be checked on Dashboard - Configuration can be given as parameters
      // See list: http://web.archive.org/web/20130904153031/http://www.dctmcontent.com/sharepoint/Articles/Permissions%20and%20Mask%20Values.aspx
      if(highRights === undefined) {
        highRights = 0;
      }

      if(lowRights === undefined) {
        lowRights = 4;
      }

      // Do we already have an active promise? Then we just return this one.
      var promiseKey = (endpoint ? '_' + endpoint : listName) + '_' + highRights + '_' + lowRights;
      if(hasRightsPromises[promiseKey]) {
        return hasRightsPromises[promiseKey];
      }

      // No, we need a new one.
      deferred = $q.defer();
      hasRightsPromises[promiseKey] = deferred.promise;

      // Get data from SharePoint.
      restUserService.getEffectiveBasePermissions(endpoint, listName).then(
        function(res) {
          // Are we getting the right data?
          if (!res || !res.data || !res.data.d || !res.data.d.EffectiveBasePermissions || res.data.d.EffectiveBasePermissions.Low === undefined) {
            // Reject promise, data returned is invalid.
            deferred.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
          }

          // Ok, does the user have the requested permissions?
          // JSHint doesn't like bitwise and, so we exclude this line from linting...

          /* jshint ignore:start */

          deferred.resolve(
            (res.data.d.EffectiveBasePermissions.Low & lowRights) === lowRights &&  // jshint ignore:line
            (res.data.d.EffectiveBasePermissions.High & highRights) === highRights  // jshint ignore:line
          );

          /* jshint ignore:end */

        },
        function(restErr) {
          // An error occurred, reject promise.
          deferred.reject(gettext('An error occurred:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
        }
      );

      // Ok, let's return the promise to the current user.
      return hasRightsPromises[promiseKey];
    };



    return usersServ;
  }]);
