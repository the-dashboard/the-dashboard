'use strict';

/**
 * @ngdoc overview
 * @name notesServices
 * @description
 *
 * This module provides services to add or modify notes and keywords for these notes.
 *
 * "Notes" in The Dashboard can be attached to elements of the risk entity tree (either as "Summary" or as
 * "Detail" notes), to individual data elements displayed in graphs or tables (e.g. "Value-at-Risk value for fixed
 * income trading on March 23rd 2006") or to keywords which can be freely defined by the user.
 *
 * All these are stored as comma-separated "keywords" on the individual note, with specifically formatted keyword IDs
 * for tree entities and data elements:
 *
 *  * `tree/<numeric entity ID>/summary` and `tree/<numeric entity ID>/details` link a note to the "Summary" or
 *    "Details" notes for a risk tree entity, which needs to exist with the given `<numeric entity ID>` in the
 *    hierarchical tree (also see {@link dashboardTreeServices.treeService}).
 *
 *  * `data/<datafile key>/<column key>/<row key>` links a note to an individual data element. The `<datafile key>`
 *    needs to exist in the Datafiles list (see {@link datafilesServices.datafilesService}), the `<column key>`
 *    needs to be defined as `keyword` in the `dataFileFormat` definition and the `<row key>` corresponds to the
 *    x-axis key of the data element (e.g. the `time` column for time series).
 *
 *  * `keyword/<numeric ID>` links to a user-defined keyword which needs to exist in the keyword store (see
 *    {@link notesServices.keywordsService} for details), where the <numeric ID> is linked to a description. The notes
 *    services provide helpers which will automatically create keywords in the keyword store which do not yet exist.
 *
 */

angular.module('notesServices', ['gettext', 'chartServices', 'restServices', 'dashboardTreeServices', 'miscServices', 'usersServices', 'dashboardScreenServices', 'socialServices', 'miscServices'])

  // TODO: implement notes caching? How to do this right?
  // TODO: refresh loaded notes from time to time (in case server updates)



  /**
   * @ngdoc filter
   * @name notesServices#keywordsPrint
   * @description
   *
   * The keywordsPrint filter turns a keyword array (where each keyword is represented as an object containing its
   * `id` and `name`) into a nicely printed, comma-separated list of keyword names.
   *
   */

    .filter('keywordsPrint', function() {
      return function(input) {
        return input.map(function(d) {
          return d.name;
        }).join(', ');
      };
    })


    /**
     * @ngdoc filter
     * @name notesServices#mapKeywordsToName
     * @description
     *
     * The mapKeywordsToName filter turns a keyword array (where each keyword is represented as a string of its ID)
     * into a nicely printed list of keyword names, using the provided keywordMapping (with keyword IDs as keys and
     * keyword names as values).
     *
     * If keywordMapping is not initialized yet, will print the keyword IDs instead.
     *
     */

  .filter('mapKeywordsToName', function() {
    return function(input, keywordMapping) {
      return input.map(function(d) {
        return (keywordMapping && keywordMapping[d] !== undefined && keywordMapping[d] !== null ? keywordMapping[d] : d);
      }).join(', ');
    };
  })



  /**
   * @ngdoc service
   * @name notesServices.notesService
   * @description
   *
   * This service creates, edits and deletes notes in the application. Each note is described by the following
   * values:
   *
   *  * `id`: SharePoint ID of the notes' list element in "Dashboard - Notes". Unique key.
   *
   *  * `noteText`: HTML-formatted rich-text of the note.
   *
   *  * `noteStatus`: Notes can be stored as "Draft", "Final" or "Reviewed". At the moment, these different stati do
   *    not trigger workflows and can be freely set by everyone who can modify a note. Dashboards can be configured
   *    such that they do not display notes in "Draft" status (e.g. for summary dashboard of sub-element summary notes).
   *
   *  * `attachments`: Array of file attachments which are stored as attachments to the note on the SharePoint list.
   *    Each file attachment is an object with a `name` value and either a `link` value (a URL pointing to the file,
   *    if it is a file that has already been uploaded to SharePoint) or a `newUploadFile` value (a file reference if the
   *    attachment is new and waiting to be uploaded when the note is saved).
   *
   *  * `keywords`: Array of keywords (stored as comma-separated list in a single text field in the SharePoint list with
   *    a closing comma at the end) for this note. The `keywords` array contains both real, user-generated keywords and
   *    automatic keywords to link notes to tree entities or individual datapoints. Each array element is an object with
   *    an `id` value (internal ID of the keyword) and a `name` value (user-displayed description). The `id` value can
   *    be any of the three forms explained in {@link notesServices} or the internal value `addKeyword/<sequence no>`.
   *    The `addKeyword/` prefix is used for user-defined keywords which do not yet exist in the keyword store and
   *    need to be added to it when adding or updating the note. Also see {@link notesServices.keywordsService}.
   *
   */

  // Returns notes for a specific keyword, adds new notes to the database
  .service('notesService', ['$q', 'gettext', 'restListService', 'usersService', 'keywordsService', 'interactionService', 'subscriptionService', function($q, gettext, restListService, usersService, keywordsService, interactionService, subscriptionService) {
    var notesServ = {};

    // Cache for already retrieved notes
    var notesCache = {};

    // Number of notes that have already been retrieved for this keyword?
    var notesCacheRetrieved = {};

    // nextUrl to retrieve additional notes for this keyword?
    var notesCacheNextUrl = {};

    // Cache for notes retrieved by ID
    var notesByIdCache = {};

    // Array of listeners to inform when new notes are being added / changed / removed
    var onNotesChanges = [];


    // Helper function to fully resolve a note we have received from the database (including author information, attachments etc.)
    var resolveRetrievedNote = function(d) {
      var keywordsList = d.Keywords.substring(0, d.Keywords.length-1).split(',');

      // New object that we can then pass around by reference
      var newNote = {
        id: d.Id,
        noteText: d.noteText,
        noteStatus: d.noteStatus,
        attachments: [],
        keywords: [],
        sourceLink: d.sourceLink,
        lastAuthor: '',
        createdDate: Date.parse(d.Created),
        lastModifiedDate: Date.parse(d.realModifiedAt !== null && d.realModifiedAt !== undefined ? d.realModifiedAt : d.Modified)
      };

      try {
        newNote.interactionSummary = JSON.parse(d.interactionSummary);
      } catch(e) {
        console.log('An ERROR occured while trying to parse the interactionSummary of this note:');
        console.log(d);
        console.log(e);
      }

      if(newNote.interactionSummary === undefined || newNote.interactionSummary === null) {
        newNote.interactionSummary = {};
      }


      var deferred = $q.defer();
      var updatePromiseArr = [];

      // Get and update keywords
      updatePromiseArr.push(
        keywordsService.getKeywordArray(keywordsList).then(
          function (keywordArray) {
            newNote.keywords = keywordArray;
            deferred.notify();
          }
        )
      );

      // Get and update last modified user
      updatePromiseArr.push(
        usersService.getUserById(d.realModifiedById !== null && d.realModifiedById !== undefined ? d.realModifiedById : d.EditorId).then(
          function (editor) {
            newNote.lastAuthor = editor.name;
            deferred.notify();
          },
          function (restErr) {
            return gettext('An error occurred loading last editor:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText;
          }
        )
      );

      // Get and update attachments (if any)
      if (d.Attachments) {
        updatePromiseArr.push(
          notesServ.getAttachments(d.Id).then(
            function (attachments) {
              newNote.attachments = attachments;
              deferred.notify();
            }
          )
        );
      }

      // We resolve this promise if all updates have been done
      $q.all(updatePromiseArr).then(deferred.resolve);

      return [newNote, deferred.promise];
    };



    // Helper function to call listeners when a note is changed
    var callListeners = function(oldNote, newNote, skipListener) {
      // TODO: Remove debug code
      console.log('callListeners called: ');
      console.log([oldNote, newNote]);

      onNotesChanges.forEach(function(listener) {
        var inScopeBefore = (oldNote && listener.filterFunc(oldNote));
        var inScopeAfter = (newNote && listener.filterFunc(newNote));

        // TODO: Remove debug code
        console.log('Listener evaluated: ');
        console.log([ listener.filterFunc, inScopeBefore, inScopeAfter, listener.newListener, listener.removedListener ]);

        if(!inScopeBefore && inScopeAfter && listener.newListener && (!skipListener || listener.newListener !== skipListener)) {
          listener.newListener(newNote);
        }

        if(inScopeBefore && !inScopeAfter && listener.removedListener && (!skipListener || listener.removedListener !== skipListener)) {
          listener.removedListener(oldNote);
        }

        if(inScopeBefore && inScopeAfter && listener.changedListener && (!skipListener || listener.changedListener !== skipListener)) {
          listener.changedListener(oldNote, newNote);
        }
      });
    };



    /**
     * @ngdoc method
     * @name notesServices.notesService#getNotesForKeyword
     * @methodOf notesServices.notesService
     * @kind function
     *
     * @description
     * This function returns a promise to an array of notes for a given keyword ID (or the prefix of a given keyword
     * ID). If the notes for this keyword ID have already been retrieved in the past, by default the function will
     * return the already loaded array. If `doReload` is set to `true`, the method will notify the promise of the
     * existing data but will go ahead and load the data from the server again, anyways, resolving the promise with the
     * fresh data.
     *
     * This function simply calls the {@link notesServices.notesService#getNotesForKeywords}, which is the multi-
     * keyword version of this function.
     *
     * @param {string} keywordId The keyword ID (or the beginning of the keyword ID, if `isPrefix` is `true`) for which
     *                           notes should be retrieved.
     *
     * @param {boolean=} doReload If set to `true`, the method will retrieve all notes from the server again even if
     *                            the notes for this `keywordId` have been retrieved before and are already available
     *                            in memory (allows to retrieve notes which have been added by others in the meantime).
     *
     * @param {boolean=} isPrefix If set to `true`, the method will retrieve all notes where at least one keyword ID
     *                            begins with the given `keywordId`. Otherwise at least one keyword ID needs to match
     *                            exactly.
     *
     * @param {integer=} maxRecords  Provides a limit for the number of notes to be retrieved to avoid retrieving a
     *                               very large number of notes from the server.
     *
     * @returns {Promise} A promise which resolves to an array of notes that are attached to the specified keyword or
     *                    to a keyword with the given prefix (depending on `isPrefix`).
     */

    notesServ.getNotesForKeyword = function(keywordId, doReload, isPrefix, maxRecords) {
      return notesServ.getNotesForKeywords([ keywordId ], undefined, doReload, isPrefix, maxRecords);
    };


    /**
     * @ngdoc method
     * @name notesServices.notesService#getNotesForKeywords
     * @methodOf notesServices.notesService
     * @kind function
     *
     * @description
     * This function returns a promise to an array of notes for a given keyword ID (or the prefix of a given keyword
     * ID). If the notes for this keyword ID have already been retrieved in the past, by default the function will
     * return the already loaded array. If `doReload` is set to `true`, the method will notify the promise of the
     * existing data but will go ahead and load the data from the server again, anyways, resolving the promise with the
     * fresh data.
     *
     * @param {Array} keywordIds The keyword IDs (or the beginning of the keyword IDs, if `isPrefix` is `true`) for
     *                           which notes should be retrieved. A note must match all keywords (or prefixes) to be
     *                           retrieved.
     *
     * @param {Array=} statusFilter If given, only retrieve notes whose status is included in the given array. If
     *                              not given (i.e. `undefined`), will retrieve notes irrespective of their status.
     *
     * @param {boolean=} doReload If set to `true`, the method will retrieve all notes from the server again even if
     *                            the notes for this `keywordId` have been retrieved before and are already available
     *                            in memory (allows to retrieve notes which have been added by others in the meantime).
     *
     * @param {boolean=} isPrefix If set to `true`, the method will retrieve all notes where at least one keyword ID
     *                            begins with the given `keywordId`. Otherwise at least one keyword ID needs to match
     *                            exactly.
     *
     * @param {integer=} maxRecords  Provides a limit for the number of notes to be retrieved to avoid retrieving a
     *                               very large number of notes from the server.
     *
     * @returns {Promise} A promise which resolves to an array of notes that are attached to the specified keyword or
     *                    to a keyword with the given prefix (depending on `isPrefix`).
     */

    notesServ.getNotesForKeywords = function(keywordIds, statusFilter, doReload, isPrefix, maxRecords) {
      var deferred = $q.defer();

      // Default is doReload = true.
      if(doReload === undefined) { doReload = true; }

      // Default is isPrefix = false.
      if(isPrefix === undefined) { isPrefix = false; }

      // Default for maxRecords is 10000.
      if(maxRecords === undefined) { maxRecords = 10000; }

      // Do we search for exact or prefixed keyword?
      var keywordSearch;
      if(isPrefix) {
        keywordSearch = keywordIds;
      } else {
        keywordSearch = keywordIds.map(function(keyword) { return keyword + ','; });
      }
      keywordSearch.sort();

      // We merge the keywordSearch array into a single string to identify the cache entry for this search.
      var keywordCacheKey = keywordSearch.join('|') + (Array.isArray(statusFilter) ? 'status:' + statusFilter.sort().join(':') : '');

      // TODO: Remove debug code
      console.log('getNotesForKeywords with CACHE KEY: ' + keywordCacheKey);
      console.log([keywordCacheKey, notesCache[keywordCacheKey], notesCacheRetrieved[keywordCacheKey], maxRecords, doReload]);

      // Do we have data from the cache?
      if(notesCache[keywordCacheKey] !== undefined && Array.isArray(notesCache[keywordCacheKey])) {
        if(doReload || notesCacheRetrieved[keywordCacheKey] < maxRecords) {
          // If we force a reload or we have not yet retrieved enough notes (compared to what was
          // requested), we will notify with what we have but will continue to execute the server request.
          deferred.notify(notesCache[keywordCacheKey]);
        } else {
          // Resolve and stop executing.
          deferred.resolve(notesCache[keywordCacheKey]);
          return deferred.promise;
        }
      }

      // If we do not reload and already have records, we will only get the additional ones.
      var nextUrl;
      var additionalRecords = maxRecords;
      if(!doReload && notesCacheNextUrl[keywordCacheKey]) {
        nextUrl = notesCacheNextUrl[keywordCacheKey];
        additionalRecords = maxRecords - notesCacheRetrieved[keywordCacheKey];
      }

      // Ok, so we are loading data from the server.
      // We always get the most recent notes, that's why we sort descending by realModifiedAt / Modified date.

      var searchString = keywordSearch
        .map(function(searchTerm) {
          return "substringof('" + searchTerm + "', Keywords)";
        })
        .join(' and ');

      if(Array.isArray(statusFilter)) {
        searchString = (keywordSearch.length >= 1 ? searchString + ' and ' : '') + '(' + statusFilter.map(function(searchTerm) {
          return "noteStatus eq '" + searchTerm + "'";
        }).join(' or ') + ')';
      }

      restListService.getMergedFilteredItems('notes', searchString, undefined, undefined, 'realModifiedAt desc, Modified desc', additionalRecords, nextUrl).then(  // Not valid OData, but works for SharePoint
        function(res) {
          // Success - got data from the server

          // TODO: Remove debug code
          console.log('Success in getting filtered notes!');
          console.log(res);

          // Are we getting the right data?
          if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
            // Reject promise
            deferred.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
          }

          // Empty notesCache for relevant keyword if we are not using nextUrl, otherwise append!
          if(!nextUrl) {
            notesCache[keywordCacheKey] = [];
          }

          // We go through all the received notes, getting keywords and if necessary loading attachment lists
          var notesPromises = [];
          res.data.d.results.forEach(function(d) {
            var keywordsList = d.Keywords.substring(0, d.Keywords.length-1).split(',');

            // Is the keyword contained in full or as prefix?
            // Note that we need to check here again even though the list from SharePoint is already
            // filtered. The reason for this is that there might be cases were for example tree/5 is
            // contained in another keyword (especially 'data/...'). SharePoint only checks for
            // whether the keyword list contains this string.
            var missingKeyword = keywordIds.findIndex(function(keywordId) {
              var keywordMissing = true;
              keywordsList.forEach(function (k) {
                if (k === keywordId || (isPrefix && k.substring(0, keywordId.length) === keywordId)) {
                  keywordMissing = false;
                }
              });

              return keywordMissing;
            });

            // Only continue if we have not found any missing keyword ("and" relationship)
            if(missingKeyword === -1) {
              // Push it to the notes cache

              // Returns [newNote, updatePromise]
              var resolverArr = resolveRetrievedNote(d);
              notesCache[keywordCacheKey].push(resolverArr[0]);
              notesPromises.push(resolverArr[1]);
            }
          });

          // Ok, we store how many records we have tried to retrieve (so that we do not have to retrieve
          // them if requested again).
          // Note that the restListService might provide more than the requested records (as it will always retrieve
          // in maxRecordsPerRequest batches). If we have received more, we will of course use them.
          notesCacheRetrieved[keywordCacheKey] = Math.max(maxRecords, notesCache[keywordCacheKey].length);
          notesCacheNextUrl[keywordCacheKey] = res.data.d.__next;

          // Ok, keywords and attachments and users are still loading, but we can already
          // return the "barebones" notes.
          deferred.notify(notesCache[keywordCacheKey]);


          $q.all(notesPromises).then(
            function() {
              // TODO: Remove debug code
              console.log('We have retrieved notes using:');
              console.log('maxRecords = ' + maxRecords);
              console.log('nextUrl = ' + nextUrl);
              console.log('We now have:');
              console.log('notesCacheRetrieved = ' + notesCacheRetrieved[keywordCacheKey]);
              console.log('notesCacheNextUrl = ' + notesCacheNextUrl[keywordCacheKey]);
              console.log('notesCache:');
              console.log(notesCache[keywordCacheKey]);

              // Ok, all attachments, keywords and users have been loaded - we are done!
              deferred.resolve(notesCache[keywordCacheKey]);
            },
            function(err) {
              deferred.reject(err);
            }
          );

        },
        function(restErr) {
          // Error - something went wrong with our REST request
          deferred.reject(gettext('An error occurred while loading notes from the database:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
        }
      );

      return deferred.promise;
    };



    /**
     * @ngdoc method
     * @name notesServices.notesService#getNotesById
     * @methodOf notesServices.notesService
     * @kind function
     *
     * @description
     * This function returns a promise to an array of notes. It retrieves all notes which are specified by their IDs
     * in the `notesIdArr`.
     *
     * @param {Array} notesIdArr  An array of IDs for the notes which should be retrieved.
     *
     * @param {boolean=} doReload If set to `true`, the method will retrieve all notes from the server again even if
     *                            the notes have been retrieved before and are already available  in memory (allows
     *                            to retrieve changes to these notes).
     *
     * @returns {Promise} A promise which resolves to an array of notes that are identified by the given `notesIdArr`.
     */

    notesServ.getNotesById = function(notesIdArr, doReload) {
      var deferred = $q.defer();

      // Default is doReload = false.
      if(doReload === undefined) { doReload = false; }

      // If we have no elements in notesIdArr or it is not an array we return an empty list.
      if(!Array.isArray(notesIdArr) || notesIdArr.length === 0) {
        return $q.resolve([]);
      }

      // We merge the keywordSearch array into a single string to identify the cache entry for this search.
      var cacheKey = notesIdArr.join('|');

      // Do we have data from the cache?
      if(notesByIdCache[cacheKey] !== undefined && !doReload) {
        return notesByIdCache[cacheKey];
      }


      // Ok, we have to do some work and get these notes.
      notesByIdCache[cacheKey] = deferred.promise;

      // We prepare a CAML query with all the notes IDs (is more suitable if we want to get a lot of IDs)
      var camlQuery = '<View>\n' +
        '<ViewFields>\n' +
        ' <FieldRef Name="Id" />\n' +
        ' <FieldRef Name="noteText" />\n' +
        ' <FieldRef Name="noteStatus" />\n' +
        ' <FieldRef Name="Keywords" />\n' +
        ' <FieldRef Name="sourceLink" />\n' +
        ' <FieldRef Name="Modified" />\n' +
        ' <FieldRef Name="Editor" />\n' +
        ' <FieldRef Name="Link" />\n' +
        '</ViewFields>\n' +
        '<Query>\n' +
        ' <Where>\n' +
        '  <In>\n' +
        '   <FieldRef Name="ID" />\n' +
        '   <Values>\n';

      notesIdArr.forEach(function(notesId) {
          camlQuery += '     <Value Type="Number">' + notesId + '</Value>\n';
      });

      camlQuery +=  '   </Values>\n' +
        '  </In>\n' +
        ' </Where>\n' +
        '</Query>\n' +
        '</View>';

      // Retrieve CAML results of mapped objects from server
      var notes = [];
      restListService.getItemsViaCAML('notes', camlQuery)
        .then(function(res) {
          // Are we getting the right data?
          if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
            // Returns already rejected promise
            return $q.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
          }

          // Yes, return the results array.
          return res.data.d.results;
        })
        .then(
          function(rawNotes) {
            // Success - got data from the server
            var notesPromises = [];

            rawNotes.forEach(function(d) {
              // Returns [newNote, updatePromise]
              var resolverArr = resolveRetrievedNote(d);
              notes.push(resolverArr[0]);
              notesPromises.push(resolverArr[1]);
            });

            // Attachments, Editors etc. are still missing, but we can notify.
            deferred.notify(notes);

            $q.all(notesPromises).then(function() {
              // Ok, now we have retrieved everything and can resolve.
              deferred.resolve(notes);
            });
          },
          function(err) {
            deferred.reject(err);
          }
        );

      return notesByIdCache[cacheKey];
    };




    /**
     * @ngdoc method
     * @name notesServices.notesService#addNote
     * @methodOf notesServices.notesService
     * @kind function
     *
     * @description
     * This method adds a new note to the SharePoint list. It checks all keywords for any that need to be added to
     * the keyword store (using {@link notesServices.keywordsService#obtainKeywordIDs `obtainKeywordIDs`}) and obtains
     * proper IDs for them. It also uploads any pending attachments to the list element and obtains links to them. After
     * the note has been successfully added to the server, the method calls all note listener callbacks (see
     * {@link notesServices.notesService#addNoteListeners addNoteListeners}) except the one specificied in
     * `skipListener` (if any).
     *
     * It returns a promise which resolves to the completed `note` object.
     *
     *
     * @param {Object} note A `note` object which should have all elements of the note except for an `id` (which is
     *                      being automatically created by SharePoint).
     *
     * @param {function} skipListener The service keeps a list of callback functions (listeners) which are to be
     *                                called when a new note is added (to update directives etc.). For this parameter,
     *                                a callback function can be passed by reference. This specific callback function
     *                                is skipped (i.e. not called) when the callbacks are iterated. The idea is to
     *                                avoid notifying the element which has created the note (and thus already knows
     *                                about it).
     *
     * @returns {Promise} A promise which resolves to the completed `note` object (i.e. with `id` value, newly
     *                   generated keyword `id`s for all new keywords and `link`s for all newly created attachments).
     *
     */

    // Add a new note to the database
    notesServ.addNote = function(note, skipListener) {
      var deferred = $q.defer();
      var author;

      // First we need to find out who we are (to set the realModifiedBy)
      usersService
        .getCurrentUser()
        .then(
          function(user) {
            author = user;

            // Add keywords to database and then continue
            return keywordsService.obtainKeywordIDs(note.keywords);
          }
        )
        .then(
          function(_keywords) {
            note.keywords = _keywords;

            // Reformat note for SharePoint
            var restNote = {
              Title: '',
              noteText: note.noteText,
              noteStatus: note.noteStatus,
              sourceLink: note.sourceLink,
              Keywords: note.keywords.map(function(k) { return k.id; }).join(',').concat(','),
              realModifiedAt: new Date(),
              realModifiedById: author.id
            };

            // Send to server
            restListService.addItem('notes', restNote).then(
              function(res) {
                // TODO: remove debug code
                console.log('Added new note!');
                console.log(res);

                // Fill in blanks in the note
                note.id = res.data.d.Id;
                note.lastAuthor = author.name;
                note.createdDate = Date.parse(res.data.d.Created);
                note.lastModifiedDate = Date.parse(res.data.d.Created);

                // Now upload attachments, if any
                var attachmentsClean = [];

                // NOTE: Sharepoint does not allow to upload multiple files at the same time for the same note!
                // That's why we have to queue those uploads!
                var iterateAttachments = function(i) {
                  if(i >= note.attachments.length) {
                    note.attachments = attachmentsClean;

                    // OK, we are done here. Finish up the note and send it out.
                    callListeners(undefined, note, skipListener);

                    // We might also have to trigger any keyword-based subscriptions about this note
                    subscriptionService.triggerSubs(
                      'keywords',
                      note.keywords,
                      'note',
                      note.id,
                      undefined,
                      '#' + note.sourceLink,
                      {
                        from: author.id,
                        text: note.noteText,
                        keywords: note.keywords
                      },
                      'new-note'
                    );

                    // Clean the cache as we have changed notes data.
                    notesCache = {};

                    deferred.resolve(note);

                  } else if(note.attachments[i].newUploadFile && !note.attachments[i].toBeRemoved) {
                    // Upload attachment
                    restListService.addAttachment('notes', note.id, note.attachments[i].name, note.attachments[i].newUploadFile).then(
                      function(res) {
                        attachmentsClean.push({
                          link: res.data.d.ServerRelativeUrl,
                          name: res.data.d.FileName
                        });

                        iterateAttachments(i+1);
                      },
                      function(restErr) {
                        deferred.reject(gettext('An error occurred while saving attachments:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
                      }
                    );
                  } else {
                    // Ignore attachment
                    iterateAttachments(i+1);
                  }
                };

                // Kick off adding the attachments
                iterateAttachments(0);
              },
              function(restErr) {
                deferred.reject(gettext('An error occurred when saving the note to the server:') + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
              }
            );
          },
          function(err) {
            deferred.reject(err);
          }
        );

      return deferred.promise;
    };



    /**
     * @ngdoc method
     * @name notesServices.notesService#updateNote
     * @methodOf notesServices.notesService
     * @kind function
     *
     * @description
     * This method allows updating of an existing note. It works similarly to
     * {@link notesServices.notesService#addNote `addNote`} in that it checks all keywords for any new ones that need to
     * be added to the keyword store (using {@link notesServices.keywordsService#obtainKeywordIDs `obtainKeywordIDs`})
     * and obtains proper IDs for them. It also uploads any new attachments to the list element and obtains links to
     * them. Attachments which have been deleted by the user are removed from the list element.
     *
     * @param {Object} note    A `note` object which should have all elements of the note and updates the note for the
     *                         given `id` element.
     *
     * @param {Object} oldNote The old `note` object prior to the changes (used to calculate whether any listeners
     *                         need to be called).
     *
     * @returns {Promise} A promise which resolves to the completed `note` object (i.e. with  newly enerated keyword
     *                    `id`s for all new keywords and `link`s for all newly created attachments).
     *
     */

    notesServ.updateNote = function(note, oldNote) {
      var deferred = $q.defer();
      var author;
      var modifiedDate;

      // First we need to find out who we are (to set the realModifiedBy)
      usersService
        .getCurrentUser()
        .then(
          function(user) {
            author = user;

            // Add keywords to database and then continue
            return keywordsService.obtainKeywordIDs(note.keywords);
          }
        )
        .then(
          function(_keywords) {
            note.keywords = _keywords;

            // Reformat note for SharePoint
            modifiedDate = new Date();
            var restNote = {
              Title: '',
              noteText: note.noteText,
              noteStatus: note.noteStatus,
              sourceLink: note.sourceLink,
              Keywords: note.keywords.map(function(k) { return k.id; }).join(',').concat(','),
              realModifiedAt: modifiedDate,
              realModifiedById: author.id

            };

            // Send to server
            return restListService.updateItem('notes', note.id, restNote);
          }
        )
        .then(
          function(res) {
            // Set last modified date
            note.lastModifiedDate = modifiedDate;
            note.lastAuthor = author.name;

            // Update attachments
            var attachmentsClean = [];

            // NOTE: Sharepoint does not allow to upload multiple files at the same time for the same note!
            // That's why we have to queue those uploads!
            var iterateAttachments = function(i) {
              if(i >= note.attachments.length) {
                note.attachments = attachmentsClean;
                
                // We are done, but we might have to inform listeners that the note has changed
                callListeners(oldNote, note);

                // Clean the cache as we have changed notes data.
                notesCache = {};

                // Resolve promise
                deferred.resolve(res);
              } else if(note.attachments[i].newUploadFile && !note.attachments[i].toBeRemoved) {
                // Upload attachment
                restListService.addAttachment('notes', note.id, note.attachments[i].name, note.attachments[i].newUploadFile).then(
                  function(res) {
                    attachmentsClean.push({
                      link: res.data.d.ServerRelativeUrl,
                      name: res.data.d.FileName
                    });

                    iterateAttachments(i+1);
                  },
                  function(restErr) {
                    deferred.reject(gettext('An error occurred while saving attachments:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
                  }
                );
              } else if(!note.attachments[i].newUploadFile && note.attachments[i].toBeRemoved) {
                // Remove attachment
                restListService.removeAttachment('notes', note.id, note.attachments[i].name).then(
                  function() {
                    iterateAttachments(i+1);
                  },
                  function(restErr) {
                    deferred.reject(gettext('An error occurred while removing attachments:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
                  }
                );

              } else {
                // Ok, keep attachment in original form if it is not removed.
                if(!note.attachments[i].toBeRemoved) { attachmentsClean.push(note.attachments[i]); }
                iterateAttachments(i+1);
              }
            };

            iterateAttachments(0);
          },
          function(restErr) {
            deferred.reject(gettext('An error occurred when saving the note to the server:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
          }                
        );

      return deferred.promise;
    };




    /**
     * @ngdoc method
     * @name notesServices.notesService#deleteNote
     * @methodOf notesServices.notesService
     * @kind function
     *
     * @description
     * This method deletes the note identified by its `note` object. As the attachments are stored as SharePoint attachments
     * on the note element, this also means that all attachments will be deleted.
     *
     * @param {Object} note The note object of the note to be deleted.
     *
     * @returns {Promise} A promise which resolves to the API response for deleting the note.
     *
     */

    notesServ.deleteNote = function(note) {
      // TODO: Remove debug code
      console.log('deleteNote has been called:');
      console.log(note);

      return restListService.deleteItem('notes', note.id).then(
        function(res) {
          // Call listeners if necessary
          callListeners(note, undefined);

          // Clean the cache as we have changed notes data.
          notesCache = {};

          // We also remove news items and interactions for this note
          // (in the background, so we can return earlier...)
          subscriptionService
            .removeNewsBySource('note', note.id)
            .then(
              function() {
                return interactionService.removeInteractionsByTarget('note', note.id);
              }
            );

          return res;
        },
        function(restErr) {
          return gettext('An error occurred deleting the note:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText;
        }
      );
    };



    /**
     * @ngdoc method
     * @name notesServices.notesService#getAttachments
     * @methodOf notesServices.notesService
     * @kind function
     *
     * @description
     * Helper function which retrieves all attachments for a given `noteID`. The attachments are returned as an array
     * of file attachments:
     *
     * ```
     * [
     *   {
     *     link: <URL at which attachment can be retrieved from SharePoint>,
     *     name: <file name which is displayed for the attachment>
     *   },
     *   ...
     * ]
     * ```
     *
     * @param {number} noteID The ID of the note for which attachments are to be retrieved (ID of the SharePoint list
     *                        element).
     *
     * @returns {Promise} A promise which resolves to the array of file attachments retrieved.
     *
     */

    notesServ.getAttachments = function(noteID) {
      var deferred = $q.defer();

      restListService.getAdditionalData('notes', noteID, 'AttachmentFiles').then(
        function(res) {
          if(!res || !res.data.d || !res.data.d.results || !Array.isArray(res.data.d.results)) {
            deferred.reject(gettext('An error occurred obtaining attachments for note') + ' ' + noteID + ': ' + gettext('Data not in expected format.'));
            return;
          }

          deferred.resolve(
            res.data.d.results.map(
              function(d) {
                return {
                  link: d.ServerRelativeUrl,
                  name: d.FileName
                };
              }
            )
          );
        },
        function(restErr) {
          deferred.reject(gettext('An error occurred obtaining attachments for note') + ' ' + noteID + ': ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
        }
      );

      return deferred.promise;
    };



    /**
     * @ngdoc method
     * @name notesServices.notesService#addNoteListeners
     * @methodOf notesServices.notesService
     * @kind function
     *
     * @description
     * Method to register new callbacks which will be called whenever a new note is added, change or removed in the
     * database. Each registered callback function will be called with the complete `note` object as parameter.
     *
     *
     * @param {function} filterFunc  This function will be called for each note to determine whether this note is
     *                               "in scope" for the given listener. This will be used to determine a delta for
     *                               changed notes (i.e. whether they are "added" or "removed").
     *
     * @param {function} newListener Callback function for new in-scope notes. It will be called whenever a note
     *                               newly meets the criteria given by `filterFunc` (because it has been newly
     *                               added or changed in a way that it is newly relevant for the filter function).
     *
     * @param {function} removedListener  Callback function for removed in-scope notes. It will be called whenever a
     *                                    note no longer meets the criteria given by `filterFunc` (because it has been
     *                                    removed or changed in a way that it is no longer relevant for the filter
     *                                    function).
     *
     * @param {function=} changedListener Callback function for notes that have changed but whose scope status has not
     *                                    (i.e. that were in scope before and after the change).
     *
     */

    notesServ.addNoteListeners = function(filterFunc, newListener, removedListener, changedListener) {
      var alreadyExists = onNotesChanges.findIndex(function(listener) {
        return (listener.filterFunc === filterFunc && listener.newListener === newListener && listener.removedListener === removedListener);
      });

      if(alreadyExists === -1) {
        onNotesChanges.push({
          'filterFunc': filterFunc,
          'newListener': newListener,
          'removedListener': removedListener,
          'changedListener': changedListener
       });
      }
    };



    /**
     * @ngdoc method
     * @name notesServices.notesService#makeNoteIntro
     * @methodOf notesServices.notesService
     * @kind function
     *
     * @description
     * This method turns a rich-text note into a short preview of that note by converting it to plaintext and
     * cutting it to the given `introLength` (or a default of 60 characters if unset). If the note is split
     * up into different HTML objects (e.g. paragraphs) it will only take the first element content (unless
     * that one is almost empty).
     *
     * @param {Object}    note          A `note` object which will be turned into an intro of itself.
     *
     * @param {integer=}  introLength   If given, the intro will be cut to this many characters. If unset,
     *                                  defaults to 60 characters.
     *
     * @returns {String}  A plaintext string which is a short intro of the given `note` object (cut to at most
     *                    `introLength` characters).
     */

    // Add a new note to the database
    notesServ.makeNoteIntro = function(note, introLength) {
      if(!introLength || introLength < 3) {
        introLength = 60;
      }

      var doc = new DOMParser().parseFromString(note.noteText, 'text/html');

      if(!doc.body) {
        return '';
      }

      var plainText;
      if(!doc.body.firstChild || !doc.body.firstChild.innerText || doc.body.firstChild.innerText.length < 10) {
        // If the first child is very small, we take all of the body. Otherwise we just take the first child,
        // which often is the header or first paragraph.
        plainText = doc.body.textContent;
      } else {
        plainText = doc.body.firstChild.innerText;
      }

      if(plainText.length > introLength) {
        return plainText.substr(0, introLength - 3).trim() + '...';
      } else {
        return plainText;
      }
    };
    
    return notesServ;
  }])






  /**
   * @ngdoc service
   * @name notesServices.keywordsService
   * @description
   *
   * This service creates and obtains keywords from the keyword store.
   *
   * Note that the service provides `tree/` (tree-entity-specific keywords) and `keyword/` (user-defined keywords)
   * keywords. The former are obtained directly from the tree structure, only the user-defined keywords are stored and
   * obtained from the "Dashboard - Keywords" SharePoint list which exists for this purpose. The datapoint-specific
   * `data/` keywords are defined ad-hoc and are not stored in any data store. They can be used to store and retrieve
   * notes, but they are ignored by the `keywordsService` and not returned in searches for available keywords.
   * See {@link notesServices} for a description of the three different keyword types.
   *
   */

  .service('keywordsService', ['$q', 'gettext', 'd3', 'restListService', 'treeService', 'miscServices', function($q, gettext, d3, restListService, treeService, miscServices) {
    var keywordServ = {};
    var keywordStorePromise;
    var keywordStoreLoading = false;

    // Internal helper function which obtains the keywords from the entity tree structure as well as from the
    // "Dashboard - Keywords" SharePoint list and stores them in an object as keyword key => keyword name pairs.
    var completeKeywordStore = function(force) {
      var keywordStore, sourcePromises, deferred;
      
      // If keyword store is currently loading, we just return a reference to
      // the running promise.                
      // We also return the loading operation if this is a force but
      // the loading is still in progress (=results are fresh)
      if(keywordStorePromise !== undefined && (!force || keywordStoreLoading)) {
        return keywordStorePromise;
      } 
      
      // Ok, we initialize the keywordStorePromise.     
      // If the keyword store has not been loaded yet or a reloading
      // is force, we load the keyword store from the REST API.
      deferred = $q.defer();
      keywordStorePromise = deferred.promise;
      keywordStoreLoading = true;     
      keywordStore = {};
      sourcePromises = [];

      // Load tree/**** keywords using dashboardTreeServices
      sourcePromises.push(
        treeService.getFlatTree().then(
          function(flatData) {
            Object.keys(flatData).forEach(function(k) {
              if(flatData[k].active) {
                // We only add active tree elements to keyword store
                keywordStore['tree/' + k + '/details'] = flatData[k].title;
                keywordStore['tree/' + k + '/summary'] = flatData[k].title + ' (Summary)';
              }
            });
          }
        )
      );

      // Load keyword/**** keywords using REST API
      sourcePromises.push(
        restListService.getMergedAllItems('keywords').then(
          function (res) {
            var localDeferred, currDatetime;
            localDeferred = $q.defer();

            // TODO: Remove debug code
            console.log('Success!');
            console.log(res);

            // Are we getting the right data?
            if (!res || !res.data || !res.data.d || !Array.isArray(res.data.d.results)) {
              // Returns already rejected promise
              localDeferred.reject(gettext('An error occurred: The obtained data object is not formatted as expected.'));
              return localDeferred;
            }

            // What date is it?
            currDatetime = new Date();

            res.data.d.results.forEach(function (d) {
              // Only add active keywords to the keyword store
              if(miscServices.parseDatetime(d.Valid_from) <= currDatetime && miscServices.parseDatetime(d.Valid_to) > currDatetime) {
                keywordStore['keyword/' + d.Id] = d.Title;
              }
            });

            // Returns promise that is already resolved
            localDeferred.resolve(keywordStore);
            return localDeferred;
          },
          function(errRest) {
            return gettext('An error occurred:') + ' ' + gettext('Status code') + ' ' + errRest.status + ', ' + errRest.statusText;
          }
        )
      );

      // Our promise returns when bot loaded successfully
      $q.all(sourcePromises).then(
        function() {
          // TODO: remove debug code
          console.log('Keyword store is fully loaded:');
          console.log(keywordStore);
        
          // Now the keyword store is fully loaded and can be returned
          keywordStoreLoading = false;
          deferred.resolve(keywordStore);
        },
        function(err) {
          // An error occurred
          keywordStoreLoading = false;
          deferred.reject(err);
        }
      );

      return keywordStorePromise;

    };



    /**
     * @ngdoc method
     * @name notesServices.keywordsService#getDataKey
     * @methodOf notesServices.notesService
     * @kind function
     *
     * @description
     * This function generates a unique key for a data entry given in the object `d`. The key will be built based on
     * the columns with `isKey` flag (as specified in the `columnsSpec` specification) by appending them together,
     * separated by a slash. Existing slashs in the data are replaced with a dash symbol.
     *
     * @param {string} datafile The datafile in which the data entry exists. Will form the first part of the key.
     *
     * @param {Object} d The data element for which the key should be generated. Must contain the `isKey` columns.
     *
     * @param {Array} columnsSpec An array of column specifications as returned by the getDatafiles reader.
     *
     * @param {string} columnName  The name of the column to be identified by this key (a key identifies a single
     *                            cell in the datafile; the `isKey` columns uniquely identify the row, this entry
     *                            identifies the column).
     *
     * @param {Object=} skipColumns An optional array of columns (or a single column name) which should be skipped for
     *                              generating the key (e.g. if key is used for internal purposes instead of being
     *                              globally unique).
     *
     * @returns {string} The generated key.
     */

    keywordServ.getDataKey = function(datafile, d, columnsSpec, columnName, skipColumns) {
      return columnsSpec.reduce(function(prev, col) {
          if(Array.isArray(skipColumns) && skipColumns.indexOf(col.display) > -1) {
            return prev + '/*';
          } else if(!Array.isArray(skipColumns) && skipColumns === col.display) {
            return prev + '/*';
          } else if (col.isKey) {
            if(d[col.display] === undefined) {
              return prev + '/';
            } else if(d3.ourSettings.formats[col.format].keyify) {
              return prev + '/' + d3.ourSettings.formats[col.format].keyify(d[col.display]);
            } else {
              return prev + '/' + d3.ourSettings.formats[col.format](d[col.display]).replace('/', '-').replace(',', ' ');
            }
          } else {
            return prev;
          }
        }, ((datafile !== '' && datafile !== undefined) ? ('data/' + datafile.replace('/', '-').replace(',', ' ')) : '')) +
           ((columnName !== '' && columnName !== undefined) ? '/' + columnsSpec.find(function(col) { return (col.display === columnName); }).keyword : '');
    };


    /**
     * @ngdoc method
     * @name notesServices.keywordsService#getAllKeywords
     * @methodOf notesServices.keywordsService
     * @kind function
     *
     * @description
     * Returns a promise to the full keyword list, i.e. all `tree/` and `keyword/` keywords (see
     * {@link notesServices.keywordsService `keywordsService`} for details). The keywords are returned as an array with
     * each array element being an object with `id` and `name` elements:
     *
     * ```
     * [
     *  {
     *   id: <keyword ID, e.g. tree/23/details or keyword/123>,
     *   name: <description of the keyword>
     *  },
     *  ...
     * ]
     * ```
     *
     * @return {Promise} A promise that resolves to an array of all existing `tree/` and `keyword/` keywords.
     *
     */

    keywordServ.getAllKeywords = function() {
      return completeKeywordStore(false).then(
        function(keywordStore) {
          return Object.keys(keywordStore).map(function(kId) {
            return { id: kId, name: keywordStore[kId] };
          });
        }
      );
    };




    /**
     * @ngdoc method
     * @name notesServices.keywordsService#filterKeywords
     * @methodOf notesServices.keywordsService
     * @kind function
     *
     * @description
     * Returns a promise to a filtered keyword list, i.e. all `tree/` and `keyword/` keywords (see
     * {@link notesServices.keywordsService `keywordsService`} for details) whose `name` property matches the
     * `searchTerm` (either exactly, if `exactMatch` is `true`, or by containing the term; in both cases the search is
     * case-insensitive). The keywords are returned as an array with each array element being an object with `id` and
     * `name` elements:
     *
     * ```
     * [
     *  {
     *   id: <keyword ID, e.g. tree/23/details or keyword/123>,
     *   name: <description of the keyword>
     *  },
     *  ...
     * ]
     * ```
     *
     * The search is done locally based on a complete keyword store and not on the server.
     *
     *
     * @param {string} searchTerm The term to be searched (case-insensitive) in the `name` property of the keyword.
     *
     * @param {boolean} exactMatch If `true`, the `name` of the keyword needs to be the same as the `searchTerm`.
     *                             Otherwise the `name` only needs to contain the `searchTerm`.
     *
     * @return {Promise} A promise that resolves to an array of `tree/` and `keyword/` keywords which match the
     *                   `searchTerm`.
     *
     */

    keywordServ.filterKeywords = function(searchTerm, exactMatch) {
      // Default is searching for searchTerm as part of keywords
      if(exactMatch === undefined) {
        exactMatch = false;
      }

      // Case-insensitive search
      searchTerm = searchTerm.toLowerCase();

      return completeKeywordStore(false).then(
        function(keywordStore) {
          // Success function, errors are just passed along
          var filteredStore = {};

          if(exactMatch) {
            Object.keys(keywordStore).forEach(function(k) {
              if (keywordStore[k].toLowerCase() === searchTerm) {
                filteredStore[k] = keywordStore[k];
              }
            });
          } else {
            Object.keys(keywordStore).forEach(function(k) {
              if (keywordStore[k].toLowerCase().indexOf(searchTerm) > -1) {
                filteredStore[k] = keywordStore[k];
              }
            });
          }

          return filteredStore;
        }
      );
    };



    /**
     * @ngdoc method
     * @name notesServices.keywordsService#getKeywordArray
     * @methodOf notesServices.keywordsService
     * @kind function
     *
     * @description
     * Turns an array of keyword IDs into a list of keyword IDs and names. The method returns a promise which will
     * resolve to an array of keyword objects of the following form:
     *
     * ```
     * [
     *  {
     *   id: <keyword ID, e.g. tree/23/details or keyword/123>,
     *   name: <description of the keyword>
     *  },
     *  ...
     * ]
     * ```
     *
     * For `tree/` and `keyword/` keywords the `name` will be obtained from the keyword store (which will be retrieved
     * from the server if necessary). For `data/` keywords, which are not stored in the keyword store (see
     * {@link notesServices.keywordsService `keywordsService`), the `name` is simply set to the `id` in the source
     * array.
     *
     *
     * @param {array} keywordIds An array of keyword ID strings.
     *
     * @return {Promise} A promise that resolves to an array of keyword objects for the given `keywordIds`.
     *
     */

    keywordServ.getKeywordArray = function(keywordIds) {
      return completeKeywordStore(false).then(
        function(keywordStore) {
          return keywordIds.map(function(kId) {
            return { id: kId, name: (keywordStore[kId] !== undefined ? keywordStore[kId] : kId) };
          });
        }
      );
    };


    /**
     * @ngdoc method
     * @name notesServices.keywordsService#getKeywordName
     * @methodOf notesServices.keywordsService
     * @kind function
     *
     * @description
     * Updates a given `keywordObject` (with `id` attribute) by setting its `name` attribute once the keywords have
     * been loaded from the server.
     * The method returns a promise which will resolve to the completed object.
     *
     * For `tree/` and `keyword/` keywords the `name` will be obtained from the keyword store (which will be retrieved
     * from the server if necessary). For `data/` keywords, which are not stored in the keyword store (see
     * {@link notesServices.keywordsService `keywordsService`), the `name` is simply set to the `id` in the source
     * object.
     *
     *
     * @param {Object} keywordObject A keyword object with filled `id` value. This object will be modified by this
     *                               function.
     *
     * @return {Promise} A promise that resolves to the completed `keywordObject`.
     *
     */

    keywordServ.getKeywordName = function(keywordObject) {
      return completeKeywordStore(false).then(
        function(keywordStore) {
          keywordObject.name = (keywordStore[keywordObject.id] !== undefined ? keywordStore[keywordObject.id] : keywordObject.id);
          return keywordObject;
        }
      );
    };



    /**
     * @ngdoc method
     * @name notesServices.keywordsService#obtainKeywordIDs
     * @methodOf notesServices.keywordsService
     * @kind function
     *
     * @description
     * Takes a keyword array, matches the keywords with the keyword store and kicks off keyword additions for new
     * keywords. Returns the completed keyword array.
     *
     * The keyword array `keywordArr` needs to contain objects with `id` and `name` properties, which will be used
     * to add new user-defined keywords to the keyword store. New keywords are identified by having an ID value of
     * `addKeyword/<sequence no>` (the sequence number is not being used but can help to keep unique IDs). This
     * temporary ID value is replaced by the method after the keyword has been added by its new proper `keyword/<ID>`
     * ID value before the array of keyword IDs is returned.
     *
     *
     * @param {Array} keywordArr An array of keyword objects with `id` and `name` properties. New user-defined
     *                           keywords need to have an ID value of the form `addKeyword/<sequence no>`.
     *
     * @return {Promise} A promise that resolves to the completed keyword array.
     *
     */

    keywordServ.obtainKeywordIDs = function(keywordArr) {
        // Do we need to add keywords to the database first?
        var keywordPromises = [];
        keywordArr.forEach(
          function(k) {
            if(k.id.substring(0, 11) === 'addKeyword/') {
              // New keyword! Need to add to database.
              keywordPromises.push(
                keywordServ.addKeyword(k.name).then(
                  function(createdKeyword) {
                    k.id = createdKeyword.id;
                  }
                )
              );
            }
          }
        );

        // Ok, we need to wait for all keyword promises to complete
        // before we can continue.
        return $q.all(keywordPromises).then(
          function() {
            return keywordArr;
          }
        );
    };





    /**
     * @ngdoc method
     * @name notesServices.keywordsService#addKeyword
     * @methodOf notesServices.keywordsService
     * @kind function
     *
     * @description
     * Takes the descriptive name of a new user-defined keyword which is to be added to the keyword store. The method
     * returns a promise to a keyword object with `id` and `name` properties:
     *
     * ```
     * {
     *  id: "keyword/<list element ID of the new keyword in the SharePoint list>",
     *  name: <keywordName as passed to this method>
     * }
     * ```
     *
     * The function also double-checks if a keyword with the given `keywordName` really does not exist on the
     * SharePoint server. If it does exist, it will return the existing keyword ID and name. If it does not, it will
     * create it on the server and return the newly obtained ID and name.
     *
     * @param {string} keywordName The descriptive name of the new keyword.
     *
     * @return {Promise} A promise that resolves to a keyword object with `id` and `name` properties.
     *
     */

      // Adds a new keyword to the keyword store (asynchronously)
    // Returns a promise to the key / value pair of the new keyword
    keywordServ.addKeyword = function(keywordName) {
      // We need to check whether keyword does not yet exist

      // First check: Does it exist in the keyword store? (Includes tree/ keys.)
      return keywordServ.filterKeywords(keywordName, true)
        .then(
          function(filteredKeywords) {
            if(Object.keys(filteredKeywords).length >= 1) {
              // Return first keyword ID that has been found
              return Object.keys(filteredKeywords)[0];
            } else {
              return undefined;
            }
          }
        )

      // Second check: Does it exist directly on the server in the
      // keywords table? (To make sure that it has not been added
      // in the meantime.)
        .then(
          function(foundKeywordID) {
            var deferred = $q.defer();

            if(foundKeywordID !== undefined) {
              // We already have an ID, so we do not need to check again
              deferred.resolve(foundKeywordID);
              return deferred.promise;
            }

            // Ok, so we are going to check directly on the server
            restListService.getMergedFilteredItems('keywords', "Title eq '" + keywordName.replace("'", "''") + "'").then(
              function(res) {
                if(res.data.d.results.length >= 1) {
                  // Return first identified keyword ID that is active or undefined otherwise
                  
                  // What date is it?
                  var currDatetime = new Date();
    
                  var i;
                  for(i = 0;i < res.data.d.results.length;i++) {
                    // Only add active keywords to the keyword store
                    if(miscServices.parseDatetime(res.data.d.results[i].Valid_from) <= currDatetime && miscServices.parseDatetime(res.data.d.results[i].Valid_to) > currDatetime) {
                       deferred.resolve('keyword/' + res.data.d.results[i].Id);
                       break;                 
                    }
                  }
                  
                  // No active keyword found? return undefined.
                  if(i === res.data.d.results.length) {
                    deferred.resolve(undefined);
                  }                  
                  
                } else {
                  deferred.resolve(undefined);
                }
              },
              function(restErr) {
                deferred.reject(gettext('An error occurred when checking for duplicates:') + ' ' + gettext('Status code') + ' ' + restErr.status + ', ' + restErr.statusText);
              }
            );

            return deferred.promise;
          }
        )

      // Ok, so we can go ahead and add it on the server
        .then(
          function(foundKeywordID) {
            var newItem;
            var deferred = $q.defer();

            if(foundKeywordID !== undefined) {
              // Keyword already exists, do not add, just return existing ID
              deferred.resolve(foundKeywordID);
              return deferred.promise;
            }

            // Ok, now add it to the server...
            // Note: We do not specify Valid_from, Valid_to as these fields are properly
            // auto-populated with default values by the SharePoint server.
            newItem = {
              'Title': keywordName
            };

            restListService.addItem('keywords', newItem).then(
              function(res) {
                // TODO: remove debug code
                console.log('Item added!');
                console.log(res);

                deferred.resolve('keyword/' + res.data.d.Id);
              },
              function(restErr) {
                deferred.reject(gettext('An error occurred while adding the new keyword:') + ' ' + restErr.status + ', ' + restErr.statusText);
              }
            );

            return deferred.promise;
          }
        )

      // Ok, so we can now add it to the keywordStore and return it to the caller
        .then(
          function(keywordID) {
            completeKeywordStore(false).then(
              function(keywordStore) {
                keywordStore[keywordID] = keywordName;
              }
            );
            
            return { id: keywordID, name: keywordName };
          }
        );
    };

    return keywordServ;
  }])



  /**
   * @ngdoc service
   * @name notesServices.notesViewer
   * @description
   *
   * This service performs lookups for notes to a given keyword and displays them in the sidebar. The `notesViewer` is
   * for example used by the {@link chartDirectives} for displaying notes as the user mouseovers the graphs.
   *
   */

  // Displays notes for a given keyword in the sidebar
  .service('notesViewer', ['$q', 'notesService', 'sidebarService', function($q, notesService, sidebarService) {
    var notesView = {};

    // Our list of notes
    var notes = [];
    var notesMap = {};
    var sidebarOpen = false;

    var prefixPromises = {};
    var prefixListeners = {};
    var listenerRegistered = {};



    // Helper for listening to note changes
    var ourNewListener = function(keywordPrefix, note) {
      // TODO: Remove debug code
      console.log('ourNewListener called for ' + keywordPrefix);
      console.log(note);

      // Go through each keyword of the new note and add this note to our map and notes array
      note.keywords.forEach(function (k) {
        var keywordParts = k.id.split('/');

        // Do we already have this keyword in our notes list?
        var entryIndex = notesMap[k.id];

        if (entryIndex !== undefined) {
          // Keyword already exists, just append note if it does not yet exist.
          if(notes[entryIndex].notes.findIndex(function(n) { return n.id === note.id; }) === -1) {
            notes[entryIndex].notes.push(note);
          }
        } else {
          // Element is new, create.
          notes.push({
            keyword: k.id,
            keywordParts: keywordParts,
            notes: [ note ]
          });

          notesMap[k.id] = notes.length-1;
        }
      });

      // We call all listeners for this keywordPrefix and deliver the new notes
      if(prefixListeners[keywordPrefix]) {
        var relevantKeywords = Object.keys(notesMap).filter(function(k) {
          return (keywordPrefix === k.substring(0, keywordPrefix.length));
        });

        prefixListeners[keywordPrefix].forEach(function(listener) {
          listener(relevantKeywords);
        });
      }

      // We reset any load notes promises
      delete prefixPromises[keywordPrefix];
    };

    var ourRemovedListener = function(keywordPrefix, note) {
      // TODO: Remove debug code
      console.log('ourRemovedListener called: ');
      console.log([keywordPrefix, note, notes, notesMap]);

      // Go through each prior keyword of the removed note and see if we need to remove it from
      // our array
      note.keywords.forEach(function(k) {
        if(notesMap[k.id] !== undefined) {
          var noteIdx = notes[notesMap[k.id]].notes.findIndex(function(n) { return n.id === note.id; });
          if(noteIdx !== -1) {
            notes[notesMap[k.id]].notes.splice(noteIdx, 1);
            if(notes[notesMap[k.id]].notes.length === 0) {
              // We remove this whole node as it is now empty
              notes.splice(notesMap[k.id], 1);

              // Reduce notesMap indexes accordingly
              Object.keys(notesMap).forEach(function(key) {
                if(notesMap[key] > notesMap[k.id]) {
                  notesMap[key]--;
                }
              });

              // Delete notesMap entry
              delete notesMap[k.id];
            }
          }
        }
      });

      // We call all listeners for this keywordPrefix and deliver the new notes
      if(prefixListeners[keywordPrefix]) {
        var relevantKeywords = Object.keys(notesMap).filter(function(k) {
          return (keywordPrefix === k.substring(0, keywordPrefix.length));
        });

        // TODO: Remove debug code
        console.log('Iterating through our listeners and calling them with new relevant keywords...');
        console.log([keywordPrefix, notes, notesMap, relevantKeywords, prefixListeners[keywordPrefix]]);

        prefixListeners[keywordPrefix].forEach(function(listener) {
          listener(relevantKeywords);
        });
      }

      // We reset any load notes promises
      delete prefixPromises[keywordPrefix];
    };

    var ourChangedListener = function(oldNote, newNote) {
      // TODO: Remove debug code
      console.log('ourChangedListener called:');
      console.log([oldNote, newNote]);

      // Update noteText and lastModifiedDate for this note
      notes.forEach(function(notesArr) {
        if(Array.isArray(notesArr.notes)) {
          for(var i = 0;i < notesArr.notes.length;i++) {
            if(notesArr.notes[i].id === newNote.id) {
              notesArr.notes[i] = newNote;
            }
          }
        }
      });

      // Reset any load notes promises affected by this change
      Object.keys(prefixPromises).forEach(function(keywordPrefix) {
        if(
          oldNote.keywords.findIndex(function(k) {
            // Match?
            return (k.id.substring(0, keywordPrefix.length) === keywordPrefix);
          }) !== -1 ||
          newNote.keywords.findIndex(function(k) {
            // Match?
            return (k.id.substring(0, keywordPrefix.length) === keywordPrefix);
          }) !== -1
        ) {
          delete prefixPromises[keywordPrefix];
        }
      });

    };



    /**
     * @ngdoc method
     * @name notesServices.notesViewer#showNotes
     * @methodOf notesServices.notesViewer
     * @kind function
     *
     * @description
     * Checks if notes for the given `keywordId` exist. If yes, it shows them in the sidebar. If no, it closes the
     * sidebar if open.
     *
     * Note that this function does not fetch any notes from the server. You need to first pre-fetch them using the
     * {@link notesServices.notesViewer#loadNotes `loadNotes`} function.
     *
     * @param {string} keywordId The keyword ID for which notes should be shown. If set to the empty string,
     *                           sidebar will be closed.
     *
     */

    notesView.showNotes = function(keywordId) {
      // Does a note exist for this keyword? If yes, we show the sidebar, otherwise we close it.
      if(keywordId && notesMap[keywordId] !== undefined && notes[notesMap[keywordId]].notes.length > 0) {
        sidebarOpen = sidebarService.open('common/services/notesViewerSidebar.tmpl.html',
          notes[notesMap[keywordId]],
          true,
          700);
      } else if(sidebarOpen) {
        sidebarService.close();
      }
    };




    /**
     * @ngdoc method
     * @name notesServices.notesViewer#loadNotes
     * @methodOf notesServices.notesViewer
     * @kind function
     *
     * @description
     * Loads notes from the server into our notes array which is used to quickly display them on the chart.
     *
     * @param {string} keywordPrefix The keyword prefix for which notes should be fetched.
     *
     * @param {function=} newListener   Listener function that will be called when notes have been loaded and whenever
     *                                  notes for the relevant prefix are subsequently added or removed. The listener
     *                                  receives an array of all relevant keywords with notes (=full update).
     *
     *
     */


    notesView.loadNotes = function(keywordPrefix, notesListener) {
      // Register listeners
      if(notesListener) {
        if(!prefixListeners[keywordPrefix]) {
          prefixListeners[keywordPrefix] = [ notesListener ];
        } else if(prefixListeners[keywordPrefix].indexOf(notesListener) === -1) {
          prefixListeners[keywordPrefix].push(notesListener);
        }
      }

      // If we already have a loading promise, we can attach to that one
      if(prefixPromises[keywordPrefix]) {
        if(notesListener) {
          return prefixPromises[keywordPrefix].then(
            function(allKeywords) {
              notesListener(allKeywords);
              return allKeywords;
            }
          );
        } else {
          return prefixPromises[keywordPrefix];
        }
      }


      // Otherwise we need to create a new one
      // Load notes from database
      prefixPromises[keywordPrefix] = notesService.getNotesForKeyword(keywordPrefix, true, true)
        .then(
          function(foundNotes) {
            var relevantKeywords = {};
            var foundSomething = false;

            // Clear keywords for which we have retrieved notes
            Object.keys(notesMap).forEach(function(keyword) {
              if(keyword.substring(0, keywordPrefix.length) === keywordPrefix) {
                notes[notesMap[keyword]].notes = [];
              }
            });

            // Iterate over all notes we have found
            foundNotes.forEach(function(n) {
              // Go over all keywords
              n.keywords.forEach(function(k) {
                if(k.id.substring(0, keywordPrefix.length) === keywordPrefix) {
                  // Match.
                  var keywordParts = k.id.split('/');

                  // Do we already have notes for this keyword?
                  if(k.id in notesMap) {
                    // Yes, we can just add our note unless it already exists...
                    notes[notesMap[k.id]].notes.push(n);
                  } else {
                    // No, we need to create a new point in our notes list.

                    notes.push({
                      keyword: k.id,
                      keywordParts: keywordParts,
                      notes: [ n ]
                    });

                    notesMap[k.id] = notes.length-1;
                  }

                  // Ok, store it as relevant keyword
                  relevantKeywords[k.id] = true;
                  foundSomething = true;
                }
              });
            });

            // Call listener
            if(notesListener) {
              notesListener(Object.keys(relevantKeywords));
            }

            return Object.keys(relevantKeywords);
          },
          function(err) {
            console.log('Error loading notes: ');
            console.log(err);
          }
        );


      // For all keywordPrefixes, we register our note listeners only once
      if(!listenerRegistered[keywordPrefix]) {
        // Relevant are all notes with the given keywordPrefix in at least one keyword
        var filterFunc = function(note) {
          return (
            note.keywords.findIndex(function(k) {
              // Match?
              return (k.id.substring(0, keywordPrefix.length) === keywordPrefix);
            }) !== -1
          );
        };

        notesService.addNoteListeners(
            filterFunc,
            function(note) { ourNewListener(keywordPrefix, note); },
            function(note) { ourRemovedListener(keywordPrefix, note); },
            ourChangedListener
          );

        listenerRegistered[keywordPrefix] = true;
      }

      // We return the newly created promise
      return prefixPromises[keywordPrefix];
    };

    return notesView;
  }])
;
