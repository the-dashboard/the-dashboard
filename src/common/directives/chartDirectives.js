'use strict';



/**
 * @ngdoc overview
 * @name chartDirectives
 * @description
 *
 * This module provides various directives for plotting charts. They are primarily
 * used by the {@link dashboardTree.controller:treeDashboardController dashboards} in
 * the Ongoing Risk Assessment (ORA) part of the application, but can in general
 * be employed elsewhere as well.
 *
 */

angular.module('chartDirectives', ['chartServices', 'miscServices', 'datafilesServices', 'notesServices', 'dashboardScreenServices', 'ngDialog', 'gettext'])

  /**
   * @ngdoc directive
   * @name chartDirectives.timeSeriesChart
   * @restrict E
   * @description
   *
   * This directive displays time series either as a line or a stacked area chart. The data for the time series is loaded
   * from a CSV data file which needs to be pre-defined in the `Dashboard - Datafiles` list. The directive will load the
   * file and look for a `time` column which is used as the x axis identifier and needs to be in a format that JavaScript
   * can interpret (e.g. ISO8601).
   *
   * All other columns will be displayed as lines or areas on the chart, depending on whether they are selected as
   * `lineColumns` or `areaColumns`.
   *
   * @scope
   * @param {number} width The width of the chart to be displayed in pixels.
   * @param {number} height The height of the chart to be displayed in pixels.
   * @param {number=} legendWidth The width of the legend to be displayed in pixels. If empty, a default value will be calculated.
   * @param {string} contentFile Gives the identifier (Title) of a datafile specified in the Dashboard - Datafiles list. The chart directive will retrieve this datafile and display it in chart form.
   * @param {string} timeColumn Gives the name of the column to be used for the x-axis (the timeline) of the chart.
   * @param {Array=} lineColumns Specifies the columns from the datafile that should be displayed as individual lines on the chart. If empty, no lines will be displayed.
   * @param {Array=} areaColumns Specifies the columns from the datafile that should be displayed as stacked area charts on the chart. If empty, no area chart will be displayed.
   * @param {boolean} relativeAreas If `true`, the areas will be scaled to 100%, so they will be shown as relative proportions.
   * @param {boolean} twoAxes If `true`, the lines and areas will be displayed on different axes (areas left, lines right). If `false`, both lines and areas will use the same axis (left).
   * @param {Object=} yAxisFix If given, this object defines lower and upper bounds for the beginning and end of the y-axis for the lines of the chart, respectively.
   *                           The object needs to contain the elements `minMin` (lower bound for y-axis minimum), `minMax` (upper bound for y-axis minimum), `maxMin`
   *                           (lower bound for y-axis maximum), `maxMax` (upper bound for y-axis maximum).
   * @param {number} mvgAvgDefault If given, specifies the initial value of the moving average slider when the chart is loaded. If empty, this defaults to 1 (i.e. no averaging of chart).
   * @param {number} initialBrushFrom If given, specifies that when loading the chart the brush should focus on the last `initialBrushFrom` number of elements, to highlight more recent periods (if empty, no brush is set initially).
   * @param {boolean} notesEnabled If set to `true`, the chart will allow for notes to be recorded on individual data records (by double clicking on the element).
   * @param {Object=} notesOptions If `notesEnabled` is set to `true`, an optional object can be passed here to overwrite options for the notes
   *                               directive, e.g. in order to enable impact flags on these notes items etc.
   *
   */

  .directive('timeSeriesChart', ['$timeout', 'ngDialog', 'gettext', 'd3', 'miscServices', 'notesViewer', 'keywordsService', 'sidebarService', 'datafileReader',
    function($timeout, ngDialog, gettext, d3, miscServices, notesViewer, keywordsService, sidebarService, datafileReader) {
      // Draw the chart
      var draw = function(svg, svgBrush, scope) {
        // fixed margin (larger on right due to legend, on bottom due to axis)
        var margin_left = Math.round(scope.width * 0.05);
        var margin_right = Math.round(scope.width * 0.05 + scope.legendWidth);
        var margin_top = Math.round(scope.height * 0.05);
        var margin_bottom = Math.round(scope.height * 0.05) + 100; /* for axis */
        var extra_margin_right = (scope.twoAxes ? margin_left : 0); /* for right axis */


        // Obtain columns for drawing the chart
        var columns, idx;
        if(Array.isArray(scope.content) && scope.content.length > 0) {
          columns = Object.keys(scope.content[0]);

          // Remove x-axis column
          idx = columns.indexOf(scope.timeColumn);
          if(idx > -1) {
            columns.splice(idx, 1);
          }
        } else {
          columns = [];
        }

        // Do we have existing activeColumns? Then use those, otherwise use all columns.
        if(!scope.activeColumns) { scope.activeColumns = columns.slice(0); }

        // Separate active columns by line or area columns
        var activeLineColumns = miscServices.intersect(scope.lineColumns, scope.activeColumns);
        var activeAreaColumns = miscServices.intersect(scope.areaColumns, scope.activeColumns);


        // Re-calculate the areaTotals if required
        if(scope.relativeAreas) {
          scope.areaTotal = scope.content.map(function(d) {
            return activeAreaColumns.reduce(function(prev, col) {
              return (prev + d[col]);
            }, 0);
          });
        } else {
          scope.areaTotal = undefined;
        }


        // Update height and width
        svg
          .attr('width', scope.width)
          .attr('height', scope.height);

        // Update clipping path
        svg
          .select('.clip-rect')
          .attr('x', margin_left)
          .attr('y', margin_top)
          .attr('width', scope.width - margin_left - margin_right - extra_margin_right)
          .attr('height', scope.height - margin_top - margin_bottom);

        // xScale => If we have an existing brush extent, we use this, otherwise we use
        //           the overall domain.
        var overallXDomain = d3.extent(scope.content, function(d) { return d[scope.timeColumn]; });

        // Define x-scale
        var xScale = d3.scaleTime()
          .domain(scope.brushExtent ? scope.brushExtent : overallXDomain)
          .range([margin_left, scope.width-margin_right-extra_margin_right]);

        // Define x-axis
        var xAxis = d3.axisBottom(xScale)
          .tickFormat(d3.timeFormat('%Y-%m-%d'));

        // Draw x-axis (rotate labels)
        var xAxisContainer = svg.select('.x-axis');

        xAxisContainer
          .attr("transform", "translate(0, " + (scope.height-margin_bottom) + ")")
          .call(xAxis)
          .selectAll('text')
          .style('text-anchor', 'end')
          .attr('dx', '-1em')
          .attr('dy', '-.55em')
          .attr('transform', 'rotate(-90)');



        // Y axis logic (left and right) starts here
        var overallYLeftDomain;
        var overallYRightDomain;
        var yLeftScale;
        var yRightScale;
        var yRightAxis;
        var yRightAxisContainer;

        if(scope.twoAxes) {
          // When we have two axes, the left domain is only for the area chart part
          overallYLeftDomain  = getYDomain(scope.content, [],                scope.areaColumns, overallXDomain, scope.timeColumn, scope.relativeAreas);
          overallYRightDomain = getYDomain(scope.content, scope.lineColumns, [],                overallXDomain, scope.timeColumn, scope.relativeAreas, scope.yAxisFix);

          yLeftScale = d3.scaleLinear()
            .range([scope.height-margin_bottom, margin_top])
            .domain(getYDomain(scope.content, [], activeAreaColumns, xScale.domain(), scope.timeColumn, scope.relativeAreas));

          yRightScale = d3.scaleLinear()
            .range([scope.height-margin_bottom, margin_top])
            .domain(getYDomain(scope.content, activeLineColumns, [], xScale.domain(), scope.timeColumn, scope.relativeAreas, scope.yAxisFix));

          // Define y-right-axis
          yRightAxis = d3.axisRight(yRightScale)
            .tickFormat(scope.columnFormats[scope.lineColumns[0]] || d3.format('f'));

          // Draw y-right-axis
          yRightAxisContainer = svg.select('.y-axis-right');

          yRightAxisContainer
            .attr("transform", "translate(" + (scope.width - margin_right - extra_margin_right) + ")")
            .call(yRightAxis);

        } else {
          overallYLeftDomain  = getYDomain(scope.content, scope.lineColumns, scope.areaColumns, overallXDomain, scope.timeColumn, scope.relativeAreas, scope.yAxisFix);
          overallYRightDomain = [0, 0]; // not used

          yLeftScale = d3.scaleLinear()
            .range([scope.height-margin_bottom, margin_top])
            .domain(getYDomain(scope.content, activeLineColumns, activeAreaColumns, xScale.domain(), scope.timeColumn, scope.relativeAreas, scope.yAxisFix));

          // no right axis - nothing to do
        }

        // Define y-axis
        var yLeftAxis;
        if(scope.relativeAreas) {
          yLeftAxis = d3.axisLeft(yLeftScale)
            .tickFormat(d3.ourSettings.formats.percentage);
        } else {
          yLeftAxis = d3.axisLeft(yLeftScale)
            .tickFormat(scope.columnFormats[(scope.areaColumns[0] ? scope.areaColumns[0] : scope.lineColumns[0])] || d3.format('f'));
        }

        // Draw y-axis
        var yLeftAxisContainer = svg.select('.y-axis-left');

        yLeftAxisContainer
          .attr("transform", "translate(" + margin_left + ")")
          .call(yLeftAxis);

        // Define chart colors
        var chartColors = d3.scaleOrdinal(d3.ourSettings.chartColors).domain(columns);


        // Update the area charts part of the chart
        var stackLayout = d3.stack()
          .keys(activeAreaColumns)
          .offset(d3.stackOffsetDiverging);

        var area = d3.area()
          .x(function(d) { return xScale(d.data[scope.timeColumn]); })
          .y0(function(d) { return yLeftScale(d[0]); })
          .y1(function(d) { return yLeftScale(d[1]); });

        updateStackedArea(svg, stackLayout, area, chartColors, scope.columnColors, activeAreaColumns, scope.timeColumn, scope.content, scope.notesKeywords, scope.areaTotal);

        // Update the line charts part of the chart
        updateLineChart(svg, chartColors, scope.columnColors, xScale, (scope.twoAxes ? yRightScale : yLeftScale), scope.lineColumns, activeLineColumns, scope.timeColumn, scope.content, scope.notesKeywords, scope.movingAverageElements, false);


        // Add legend
        var legend = svg.select('.legend');

        var legends = legend.selectAll('g').data(columns);

        var enteredLegends = legends
          .enter()
          .append('g')
          .attr('class', function(d, i) { return 'legend-' + i; })
          .attr("transform", function(d, i) { return "translate(0, " + (margin_top + (columns.length-1-i)*Math.min((scope.height-margin_top-margin_bottom)/columns.length, 30)) + ")"; });

        enteredLegends.append('rect')
          .attr('x', scope.width - scope.legendWidth)
          .attr('y', function(d) {
            if(scope.lineColumns.indexOf(d) > -1) {
              return Math.round(Math.min((scope.height-margin_top-margin_bottom)/columns.length*0.2, 6)+1);
            } else {
              return 0;
            }
          })
          .attr('width', Math.min(60, scope.legendWidth * 0.3))
          .attr('height', function(d) {
            if(scope.lineColumns.indexOf(d) > -1) {
              return Math.round(Math.min((scope.height-margin_top-margin_bottom)/columns.length*0.2, 6));
            } else {
              return Math.round(Math.min((scope.height-margin_top-margin_bottom)/columns.length*0.6, 18));
            }
          })
          .style('fill', function(d) { return scope.columnColors[d] ? scope.columnColors[d] : chartColors(d); });

        enteredLegends.append('text')
          .attr('x', scope.width - scope.legendWidth + Math.min(70, scope.legendWidth * 0.35))
          .attr('y', Math.round(Math.min((scope.height-margin_top-margin_bottom)/columns.length, 30)*0.5))
          .style('fill', 'black')
          .text(function(d) { return d; });


        // Helper function to deal with legend clicks
        var processLegendChange = function() {
          // Update yScales to ignore deactivated legend
          if(scope.twoAxes) {
            yLeftScale.domain(getYDomain(scope.content, [], activeAreaColumns, xScale.domain(), scope.timeColumn, scope.relativeAreas));
            yRightScale.domain(getYDomain(scope.content, activeLineColumns, [], xScale.domain(), scope.timeColumn, scope.relativeAreas, scope.yAxisFix));

            // Update yAxes for new scale
            yRightAxisContainer
              .transition()
              .duration(250)
              .call(yRightAxis);

          } else {
            yLeftScale.domain(getYDomain(scope.content, activeLineColumns, activeAreaColumns, xScale.domain(), scope.timeColumn, scope.relativeAreas, scope.yAxisFix));
          }

          // Update yAxes for new scale
          yLeftAxisContainer
            .transition()
            .duration(250)
            .call(yLeftAxis);

          // We need to update the areaTotal for the new active areas
          if(scope.relativeAreas) {
            scope.areaTotal = scope.content.map(function(d) {
              return activeAreaColumns.reduce(function(prev, col) {
                return (prev + d[col]);
              }, 0);
            });
          } else {
            scope.areaTotal = undefined;
          }

          // We redraw the area chart part of the chart (to hide / show areas and also to recognize scale changes)
          updateStackedArea(svg, stackLayout, area, chartColors, scope.columnColors, activeAreaColumns, scope.timeColumn, scope.content, scope.notesKeywords, scope.areaTotal);

          // We redraw the chart to recognize the change in y scales and to fade/unfade lines
          updateLineChart(svg, chartColors, scope.columnColors, xScale, (scope.twoAxes ? yRightScale : yLeftScale), scope.lineColumns, activeLineColumns, scope.timeColumn, scope.content, scope.notesKeywords, scope.movingAverageElements, true);
        };

        enteredLegends
          .merge(legends)
            // Fade out inactive columns
            .classed('faded', function(d) { return (scope.activeColumns.indexOf(d) === -1); })

            // Interactivity for legend - click shows/hides line
            .on('click', function(d) {
              var setFaded = !d3.select(this).classed('faded');

              d3.select(this).classed('faded', setFaded);

              // Is this an area or line column?
              var thisColumnArray = (scope.areaColumns.indexOf(d) > -1 ? activeAreaColumns : activeLineColumns);

              // Update activeColumns (preserve order of columns!)
              if(setFaded) {
                // Remove from active columns
                scope.activeColumns.splice(scope.activeColumns.indexOf(d), 1);

                // Also remove from helper area / line columns array
                thisColumnArray.splice(thisColumnArray.indexOf(d), 1);

              } else {
                // Add back at the right place
                var j2 = 0;
                for(var j1 = 0;j1 < columns.length;j1++) {
                  if (scope.activeColumns[j2] === columns[j1]) {
                    j2++;
                  } // They match, so increment
                  else if (columns[j1] === d) {
                    scope.activeColumns.splice(j2, 0, d);
                  } // Add here!
                }

                // Also add back at the right place in helper array
                j2 = 0;
                for(j1 = 0;j1 < columns.length;j1++) {
                  if (thisColumnArray[j2] === columns[j1]) {
                    j2++;
                  } // They match, so increment
                  else if (columns[j1] === d) {
                    thisColumnArray.splice(j2, 0, d);
                  } // Add here!
                }

                // Done.
              }

              // Re-draw, update totals
              processLegendChange();
            })

            // On a doubleclick, we select just this line or area
            .on('dblclick', function(d) {
              // Prevent other action
              d3.event.preventDefault();

              // Fade out everything but this one
              svg.select('.legend').selectAll('g').classed('faded', true);
              d3.select(this).classed('faded', false);

              // Only this line or area should be active
              scope.activeColumns = [ d ];
              if(scope.areaColumns.indexOf(d) > -1) {
                activeAreaColumns = [ d ];
                activeLineColumns = [ ];
              } else {
                activeAreaColumns = [ ];
                activeLineColumns = [ d ];
              }

              // Re-draw, update totals
              processLegendChange();
            })

            // On a right click, we reset the selection to all columns
            .on('contextmenu', function() {
              // Fade out nothing
              svg.select('.legend').selectAll('g').classed('faded', false);

              // Reset activeColumns to all columns
              scope.activeColumns = columns.slice(0);

              // Separate active columns by line or area columns
              activeLineColumns = miscServices.intersect(scope.lineColumns, scope.activeColumns);
              activeAreaColumns = miscServices.intersect(scope.areaColumns, scope.activeColumns);

              // Re-draw, update totals
              processLegendChange();

              // Stop propagating of event (do not show browser context menu)
              d3.event.preventDefault();
            });


        // Add mouseover "cursor"
        var xCursor = svg.select('.x-cursor');
        var yCursor = svg.select('.y-cursor');
        var xLabel = svg.select('.x-label');
        var yLeftLabel = svg.select('.y-label-left');
        var yRightLabel = svg.select('.y-label-right');
        var activePoint = svg.select('.data-point-active');
        var sidebarOpen = false;

        svg.on('mousemove', function() {
          var pos = d3.mouse(this);
          var xValue = xScale.invert(pos[0]);
          var yLeftValue = yLeftScale.invert(pos[1]);

          var xMin = xScale.domain()[0];
          var xMax = xScale.domain()[1];
          var yLeftMin = yLeftScale.domain()[0];
          var yLeftMax = yLeftScale.domain()[1];

          var yRightValue;
          if(scope.twoAxes) {
            yRightValue = yRightScale.invert(pos[1]);
          }


          // Get "snappy" cursor by traversing our array finding the right data point

          // Create a bisector with an accessor function
          // that traverses the array from left
          var xBisect = d3.bisector(function(d) { return d[scope.timeColumn]; }).left; // Apply the bisector
          var xIndex = xBisect(scope.content, xValue);

          // Hide cursor and cancel if mouse is outside of data area and/or no relevant data point can be found
          if(pos[0] > scope.width - margin_right - extra_margin_right || pos[0] < margin_left || pos[1] > scope.height - margin_bottom || pos[1] < margin_top || xIndex === 0 || xIndex >= scope.content.length) {
            activePoint.classed("hidden", true);
            xCursor.classed('hidden', true);
            xLabel.classed('hidden', true);
            yCursor.classed('hidden', true);
            yLeftLabel.classed('hidden', true);
            yRightLabel.classed('hidden', true);
            scope.activeElement = undefined;

            if(sidebarOpen) {
              // Close the sidebar if it is open (note that this requires an angular $apply call)
              scope.$apply(sidebarService.close);
            }

            return;
          }

          // get the nearest content time
          var d = xValue - scope.content[xIndex - 1][scope.timeColumn] > scope.content[xIndex][scope.timeColumn] - xValue ? scope.content[xIndex] : scope.content[xIndex - 1];
          var xValueSnapped = d[scope.timeColumn];

          // Nearest line or stacked area?
          var valueStackPosCounter = 0;
          var valueStackNegCounter = 0;
          var ySnapped = scope.activeColumns.reduce(function(prevVal, k) {
            var valueDifference;
            var thisValue;
            var currScale;

            // Is this column a line? Then we can just its value.
            if(scope.lineColumns.indexOf(k) > -1) {
              currScale = (scope.twoAxes ? yRightScale : yLeftScale);
              valueDifference = Math.abs(currScale(d[k]) - currScale(scope.twoAxes ? yRightValue : yLeftValue));

              if(valueDifference < prevVal.valueDifference) {
                return { value: d[k], column: k, scale: (scope.twoAxes ? yRightScale : yLeftScale), valueDifference: valueDifference };
              }
            } else {
              // This is an area column, we need to stack them up.
              // Our stack layout puts all negative values below the zero axis and all positive values above it, so we need to add them
              // up and compare them accordingly.
              if(d[k] >= 0) {
                valueStackPosCounter += (scope.relativeAreas && scope.areaTotal[xIndex - 1] > 0 ? (d[k] / scope.areaTotal[xIndex - 1]) : d[k]);
                thisValue = valueStackPosCounter;
              } else {
                valueStackNegCounter += (scope.relativeAreas && scope.areaTotal[xIndex - 1] > 0 ? (d[k] / scope.areaTotal[xIndex - 1]) : d[k]);
                thisValue = valueStackNegCounter;
              }

              valueDifference = Math.abs(yLeftScale(thisValue) - yLeftScale(yLeftValue));
              if(valueDifference < prevVal.valueDifference) {
                return { value: thisValue, column: k, scale: yLeftScale, valueDifference: valueDifference };
              }
            }

            // If the new value is further away than the previous one, we return the previous value
            return prevVal;
          }, { value: Infinity, column: undefined, scale: yLeftScale, valueDifference: Infinity });


          // TODO: Remove debug code!
          console.log('Cursoring...');
          console.log(scope);
          console.log(ySnapped);
          console.log(yLeftValue);
          console.log(yRightValue);
          console.log(yLeftScale.domain());
          console.log(yLeftScale.range());



          // We store the currently active data point (which data set and which x-value) in the activeElement
          // variable, which is used when displaying the note taking dialog (after dblclick).
          var myKeyword = keywordsService.getDataKey(scope.contentFile, d, scope.columnsSpec, ySnapped.column);

          scope.activeElement = {
              'keyword': myKeyword,
              'columnName': ySnapped.column,
              'xValue': xValueSnapped
            };

          // Does a note exist for this data point? If yes, we show the sidebar, otherwise we close it.
          scope.$apply(function() {
            notesViewer.showNotes(myKeyword);
          });

          // Draw cursor
          activePoint
            .attr("cx", xScale(xValueSnapped))
            .attr("cy", ySnapped.scale(ySnapped.value))
            .attr("r", 3)
            .classed("hidden", false);

          xCursor
            .attr('x1', xScale(xValueSnapped))
            .attr('y1', yLeftScale(yLeftMin))
            .attr('x2', xScale(xValueSnapped))
            .attr('y2', yLeftScale(yLeftMax))
            .classed('hidden', false);

          xLabel
            .attr('transform', 'translate('+ xScale(xValueSnapped) +','+(yLeftScale(yLeftMin) + 10)+')rotate(-90)')
            .classed('hidden', false);

          xLabel
            .select('text')
            .text(d3.timeFormat('%Y-%m-%d')(xValueSnapped));

          yCursor
            .attr('x1', xScale(xMin))
            .attr('y1', ySnapped.scale(ySnapped.value))
            .attr('x2', xScale(xMax))
            .attr('y2', ySnapped.scale(ySnapped.value))
            .classed('hidden', false);


          if(ySnapped.scale === yLeftScale) {
            yLeftLabel
              .attr('transform', 'translate(' + (xScale(xMin) - 10) + ',' + ySnapped.scale(ySnapped.value) + ')')
              .classed('hidden', false);

            if(scope.relativeAreas) {


              yLeftLabel
                .select('text')
                .text(d3.ourSettings.formats.percentage(ySnapped.value));
            } else {
              yLeftLabel
                .select('text')
                .text(scope.columnFormats[ySnapped.column](ySnapped.value));
            }

            if(scope.twoAxes) {
              yRightLabel.classed('hidden', true);
            }

          } else {
            yRightLabel
              .attr('transform', 'translate(' + (xScale(xMax) + 10) + ',' + ySnapped.scale(ySnapped.value) + ')')
              .classed('hidden', false);

            yRightLabel
              .select('text')
              .text(scope.columnFormats[ySnapped.column](ySnapped.value));

            yLeftLabel
              .classed('hidden', true);
          }

        });


        // Add brush

        // Update width and height
        svgBrush
          .attr('width', scope.width)
          .attr('height', 0.1*scope.height);  // 10% of full graph height

        // xScale for the brush
        var xScaleBrush = d3.scaleTime()
          .domain(overallXDomain)
          .range(xScale.range());


        // yScale for the brush
        var yLeftScaleBrush = d3.scaleLinear()
          .domain(overallYLeftDomain)
          .range([Math.floor(0.1*scope.height), 0]);

        var yRightScaleBrush;
        if(scope.twoAxes) {
          yRightScaleBrush = d3.scaleLinear()
            .domain(overallYRightDomain)
            .range([Math.floor(0.1*scope.height), 0]);
        }

        // Draw data for the brush

        // First we update the stacked area chart
        var areaBrush = d3.area()
          .x(function(d) { return xScaleBrush(d.data[scope.timeColumn]); })
          .y0(function(d) { return yLeftScaleBrush(d[0]); })
          .y1(function(d) { return yLeftScaleBrush(d[1]); });

        updateStackedArea(svgBrush, stackLayout, areaBrush, chartColors, scope.columnColors, scope.areaColumns, scope.timeColumn, scope.content, scope.notesKeywords, scope.areaTotal);

        // Now we update the line chart
        updateLineChart(svgBrush, chartColors, scope.columnColors, xScaleBrush, (scope.twoAxes ? yRightScaleBrush : yLeftScaleBrush), scope.lineColumns, scope.lineColumns, scope.timeColumn, scope.content, scope.notesKeywords, scope.movingAverageElements, false);


        // Add brush interactivity
        var brush = d3.brushX();
        brush.extent([
          [xScaleBrush.range()[0], yLeftScaleBrush.range()[1]],
          [xScaleBrush.range()[1], yLeftScaleBrush.range()[0]]
        ]);

        var brushContainer = svgBrush.select('.brush').call(brush);

        brush.on('end', function() {

          // Use the current extent
          if(!d3.event.selection) {
            xScale.domain(xScaleBrush.domain()); // reset original domain
            scope.brushExtent = undefined;
          } else {
            xScale.domain(d3.event.selection.map(xScaleBrush.invert));
            scope.brushExtent = d3.event.selection.map(xScaleBrush.invert);
          }

          // Update y axis based on new max / min
          if(scope.twoAxes) {
            yLeftScale.domain(getYDomain(scope.content, [], activeAreaColumns, xScale.domain(), scope.timeColumn, scope.relativeAreas));
            yRightScale.domain(getYDomain(scope.content, activeLineColumns, [], xScale.domain(), scope.timeColumn, scope.relativeAreas, scope.yAxisFix));

            yRightAxisContainer
              .transition()
              .duration(250)
              .call(yRightAxis);

          } else {
            yLeftScale.domain(getYDomain(scope.content, activeLineColumns, activeAreaColumns, xScale.domain(), scope.timeColumn, scope.relativeAreas, scope.yAxisFix));
          }

          // Update yAxis for new scale
          yLeftAxisContainer
            .transition()
            .duration(250)
            .call(yLeftAxis);

          // Redraw xAxis
          xAxisContainer
            .transition()
            .duration(250)
            .call(xAxis);

          xAxisContainer
            .selectAll('text')
            .style('text-anchor', 'end')
            .attr('dx', '-1em')
            .attr('dy', '-.55em')
            .attr('transform', 'rotate(-90)');

          // Redraw the data

          // Area chart first
          updateStackedArea(svg, stackLayout, area, chartColors, scope.columnColors, activeAreaColumns, scope.timeColumn, scope.content, scope.notesKeywords, scope.areaTotal);

          // Line chart second
          updateLineChart(svg, chartColors, scope.columnColors, xScale, (scope.twoAxes ? yRightScale : yLeftScale), scope.lineColumns, activeLineColumns, scope.timeColumn, scope.content, scope.notesKeywords, scope.movingAverageElements, false);

        });

        // TODO: Remove debug code!
        console.log('Moving brush...');
        console.log([scope.brushExtent, scope.initialBrushFrom, scope.content.length]);

        if(scope.brushExtent) {

          // TODO: Remove debug code!
          console.log('Moving brush to existing brushExtent...');
          console.log([scope.brushExtent, scope.initialBrushFrom, scope.content.length, scope.brushExtent.map(xScaleBrush)]);

          // Existing brush extent?
          brushContainer.call(brush.move, scope.brushExtent.map(xScaleBrush));
        } else if(scope.initialBrushFrom >= 1 && scope.initialBrushFrom < scope.content.length && scope.usedInitialBrush !== scope.initialBrushFrom) {
          // Brush for the first time if necessary.
          brushContainer.call(brush.move, [
            xScaleBrush(scope.content[scope.content.length-scope.initialBrushFrom-1][scope.timeColumn]),
            xScaleBrush(scope.content[scope.content.length-1][scope.timeColumn])
          ]);

          // This avoids that we re-initialize the brush if it has been cleared manually and the
          // graph is re-drawn afterwards
          scope.usedInitialBrush = scope.initialBrushFrom;
        }


        // TODO: make x-axis labelling format configurable (currently hard-coded at %Y-%m-%d)
        // TODO: make y-axis labelling format configurable (currently hard-coded at 'f')
        // TODO: make line chart drawing into support function (so that code is needed only once not twice)
        // TODO: bring 'var' declarations to the top

      };

      // Helper function to obtain the extent of the data in y direction
      var getYDomain = function(content, lineColumns, areaColumns, xExtent, timeColumn, relativeAreas, yAxisFix) {
        var shortenedContent = content.filter(function(d) {
          return (d[timeColumn] >= xExtent[0] && d[timeColumn] <= xExtent[1]);
        });

        // Line charts part
        // For line charts, the y-axis goes from the minimum value to the maximum value
        // for any column
        var yDomainLines = [
            d3.min(shortenedContent, function (d) {
              var min = Infinity;

              lineColumns.forEach(function (k) {
                if (d[k] < min) {
                  min = d[k];
                }
              });

              return min;
            }),
            d3.max(shortenedContent, function (d) {
              var max = 0;

              lineColumns.forEach(function (k) {
                if (d[k] > max) {
                  max = d[k];
                }
              });

              return max;
            })
          ];

        // If yAxisFix is given, we need to clip the min/max of yDomainLines
        if(yAxisFix) {
          if(yAxisFix.minMin !== undefined && yAxisFix.minMin !== '' && yDomainLines[0] < yAxisFix.minMin) {
            yDomainLines[0] = yAxisFix.minMin;
          }
          if(yAxisFix.minMax !== undefined && yAxisFix.minMax !== '' && yDomainLines[0] > yAxisFix.minMax) {
            yDomainLines[0] = yAxisFix.minMax;
          }
          if(yAxisFix.maxMin !== undefined && yAxisFix.maxMin !== '' && yDomainLines[1] < yAxisFix.maxMin) {
            yDomainLines[1] = yAxisFix.maxMin;
          }
          if(yAxisFix.maxMax !== undefined && yAxisFix.maxMax !== '' && yDomainLines[1] > yAxisFix.maxMax) {
            yDomainLines[1] = yAxisFix.maxMax;
          }
        }

        // Area charts part
        // For stacked area charts, the y-axis goes from the sum of all negative values to the maximal sum of all column values (the stack height)
        // If we fill relativeAreas, this will always be 0-100%
        var yDomainAreas;
        if(relativeAreas) {
          yDomainAreas = [0, 1];
        } else {
          yDomainAreas = [
              d3.min(shortenedContent, function (d) {
                return areaColumns.reduce(function(previousValue, currentValue) {
                  return previousValue + (d[currentValue] < 0 ? d[currentValue] : 0);
                }, 0);
              }),
              d3.max(shortenedContent, function (d) {
                return areaColumns.reduce(function(previousValue, currentValue) {
                  return previousValue + (d[currentValue] > 0 ? d[currentValue] : 0);
                }, 0);
              })
          ];
        }

        // The total domain needs to fit both the line charts and the area charts (if there are any area charts)
        var yDomain;
        if(Array.isArray(areaColumns) && areaColumns.length > 0) {
          yDomain = [
            d3.min([yDomainLines[0], yDomainAreas[0]]),
            d3.max([yDomainLines[1], yDomainAreas[1]])
          ];
        } else {
          yDomain = yDomainLines;
        }

        // We add 2% on top, so that the red dots for the notes can still be selected
        // at the maximum point.
        yDomain[1] = yDomain[1] + (yDomain[1]-yDomain[0])*0.02;

        return yDomain;
      };



      // Helper function to update the line chart when stuff is getting brushed
      var updateLineChart = function(svg, myChartColors, myColumnColors, myXScale, myYScale, myColumns, myActiveColumns, timeColumn, myContent, myNotesKeywords, fullRedraw) {
        // We need to remove all lines and redraw them... otherwise they won't appear above the area charts.
        if(fullRedraw) {
          svg.select('.data').selectAll('.chartLine')
            .remove();
        }

        var lines = svg.select('.data').selectAll('.chartLine').data(myColumns);

        lines
          .exit()
          .remove();

        lines
          .enter()
          .append('path')
          .attr('class', function(k, i) { return 'chartLine chartPath-' + i; })

          .merge(lines)
            .classed('faded', function(k) { return (myActiveColumns.indexOf(k) === -1); })
            .style('stroke', function(k) { return (myColumnColors[k] ? myColumnColors[k] : myChartColors(k)); })
            .transition()
            .duration(250)
            .attr('d', function(k) {
              var line = d3.line()
                .x(function(d) { return myXScale(d[timeColumn]); })
                .y(function(d) { return myYScale(d[k]); });

              return line(myContent);
            });


        // Add notes circles if we have notes
        if(fullRedraw) {
          svg.select('.data').selectAll('.notes-circles-lines')
            .remove();
        }

        var notesCircles = svg.select('.data').selectAll('.notes-circles-lines').data(myNotesKeywords.filter(function(note) {
          return (myActiveColumns.indexOf(note.column) > -1);
        }));

        notesCircles
          .exit()
          .remove();

        notesCircles
          .enter()
          .append('circle')
          .attr('class', 'notes-circles notes-circles-lines')
          .attr('cx', function(d) { return myXScale(d.time); })
          .attr('cy', function(d) {
            var matchTime = myContent.filter(function(cont) { return cont[timeColumn].getTime() === d.time.getTime(); })[0];
            return (matchTime ? myYScale(matchTime[d.column]) : 0);
          })
          .attr('r', 3);

        // Only update the "non-new" circles via transition
        notesCircles
          .transition()
          .duration(250)
          .attr('cx', function(d) { return myXScale(d.time); })
          .attr('cy', function(d) {
            var matchTime = myContent.filter(function(cont) { return cont[timeColumn].getTime() === d.time.getTime(); })[0];
            return (matchTime ? myYScale(matchTime[d.column]) : 0);
          });

      };


      // Helper function to update the stackedArea chart when columns are enabled or disabled
      var updateStackedArea = function(svg, myStackLayout, myArea, myChartColors, myColumnColors, myColumns, timeColumn, myContent, myNotesKeywords, myAreaTotal) {
        var mappedContent;
        if(myAreaTotal) {
          // We need to calculate the relative column values.
          mappedContent = myContent.map(function (d, i) {
            var ret = {};
            myColumns.forEach(function (k) {
              ret[k] = (myAreaTotal[i] > 0 ? (d[k] / myAreaTotal[i]) : d[k]);
            });

            ret[timeColumn] = d[timeColumn];

            return ret;
          });
        } else {
          mappedContent = myContent;
        }

        // TODO: Remove debug code
        console.log('updateStackArea called:');
        console.log([myStackLayout, myColumns, myContent, myAreaTotal, mappedContent, timeColumn, myNotesKeywords]);

        // Update keys of stacklayout
        myStackLayout.keys(myColumns);

        // Create stack layout
        var stackedData = myStackLayout(mappedContent);

        // TODO: Remove debug code
        console.log('Obtained stackedData:');
        console.log(stackedData);


        var stackedAreas = svg.select('.data')
          .selectAll(".stackedAreas")
          .data(stackedData, function(d) { return d.key; });

        stackedAreas.exit()
          .remove();

        stackedAreas.enter()
          .append('path')

          .merge(stackedAreas)
            .attr('d', function(d) { return myArea(d); })
            .style('fill', function(d) { return (myColumnColors[d.key] ? myColumnColors[d.key] : myChartColors(d.key)); })
            .attr('class', function(d, i) { return 'stackedAreas chartPath-' + i; });


        // Add notes circles if we have notes
        svg.select('.data').selectAll('.notes-circles-area')
          .remove();

        var notesCircles = svg.select('.data').selectAll('.notes-circles-area').data(myNotesKeywords.filter(function(note) {
          return (myColumns.indexOf(note.column) > -1);
        }));

        // TODO: Remove debug code
        console.log('Drawing notesCircles for: ');
        myNotesKeywords.forEach(function(note) {
          console.log([note, myColumns.indexOf(note.column), (myColumns.indexOf(note.column) > -1)]);
        });

        var cxAccessor = function(d) {
          // Find column
          var matchColumn = stackedData.find(function(cont) { return cont.key.toString() === d.column.toString(); });

          // Find time entry for column
          var matchTime;
          if(matchColumn) { matchTime = matchColumn.find(function(cont) { return cont.data[timeColumn].toString() === d.time.toString(); }); }

          // Return x value at this point
          return myArea.x()(matchTime);

        };

        var cyAccessor = function(d) {
          // Find column
          var matchColumn = stackedData.find(function(cont) { return cont.key === d.column; });

          // Find time entry for column
          var matchTime;
          if(matchColumn) { matchTime = matchColumn.find(function(cont) { return cont.data[timeColumn].getTime() === d.time.getTime(); }); }

          // Return y1 value at this point, unless we are in negative territory, then return y0 value
          return (matchTime ? (matchTime[0] < 0 ? myArea.y0()(matchTime) : myArea.y1()(matchTime)) : 0);
        };

        notesCircles
          .enter()
          .append('circle')
          .attr('cx', cxAccessor)
          .attr('cy', cyAccessor)
          .attr('r', 3)
          .attr('class', 'notes-circles notes-circles-area');

        // Only update the not-new elements via transition
        notesCircles
          .transition()
          .duration(250)
          .attr('cx', cxAccessor)
          .attr('cy', cyAccessor);

      };


      // Function for in-chart note taking - is called when user double clicks inside of the
      // clipping rectangle.
      // Uses ngDialog to show a note taking dialog and to display existing notes on this node.
      var addChartNote = function(myElement, notesOptions, zoomKeyCtrl) {
        // Open dialog with the notes view if we are double-clicking inside of the data area
        if(myElement) {
          ngDialog.open({
            template: 'common/directives/notesDialog.tmpl.html',
            className: 'ngdialog-theme-default',
            closeByDocument: false,
            data: {
              'keywordID': myElement.keyword,
              'sourceName': myElement.columnName + ' at ' + d3.timeFormat('%Y-%m-%d')(myElement.xValue),
              'notesOptions': notesOptions,
              'sourceLink': (zoomKeyCtrl ? zoomKeyCtrl.getZoomLink(d3.timeFormat('%Y-%m-%d')(myElement.xValue)) : '')
            }
          });
        }
      };



      // Function to transform content to moving average content
      // TODO: Outsource in separate service?
      var makeMovingAverage = function(source, movAvg, timeColumn) {
        // We can return raw data if the moving average is over 1 element
        if(!movAvg || movAvg <= 1 || !source || !source.length) {
          return source;
        }

        // Which columns to we need to look at?
        var columns = Object.keys(source[0]);

        // Remove time column
        columns.splice(columns.indexOf(timeColumn), 1);

        // Ok, we need to create a new content array
        var ret = [];

        // Adds up the data for the moving average per column
        var calcForEachColumn = function(c) {
          var newVal = 0;
          for(var j = i - movAvg + 1;j <= i; j++) {
            newVal += source[j][c];
          }
          ret[ret.length-1][c] = newVal / movAvg;
        };

        for(var i = movAvg;i < source.length;i++) {
          var newObj = {};
          newObj[timeColumn] = source[i][timeColumn];
          ret.push(newObj);

          columns.forEach(calcForEachColumn);
        }

        return ret;
      };


      return {
        restrict: 'E',

        scope: {
          width: '=',
          height: '=',
          legendWidth: '=?',
          contentFile: '=',
          timeColumn: '=',
          lineColumns: '=?',
          areaColumns: '=?',
          relativeAreas: '=?',
          twoAxes: '=',
          yAxisFix: '=?',
          mvgAvgDefault: '=',
          initialBrushFrom: '=',
          notesEnabled: '=',
          notesOptions: '=?'
        },

        require: '?^zoomKey',

        templateUrl: 'common/directives/chartTimeSeries.tmpl.html',

        link: function(scope, element, attrs, zoomKeyCtrl) {
          var svg, svgBrush, loadingIndicator;
          scope.loading = false;
          scope.errorMessage = '';
          scope.notesKeywords = [];
          scope.content = [];
          scope.contentRaw = [];
          scope.contentHash = '';
          scope.columnKeywords = {};
          scope.brushExtent = undefined;
          scope.maxMovingAverage = 1;
          scope.movingAverageElements = (scope.mvgAvgDefault >= 1 ? scope.mvgAvgDefault : 1);
          scope.columnsSpec = [];
          scope.keywordsHash = {};

          if(!scope.legendWidth) {
            scope.legendWidth = Math.round(0.25*scope.width);
          }


          if(!Array.isArray(scope.lineColumns)) {
            scope.lineColumns = [];
          }

          if(!Array.isArray(scope.areaColumns)) {
            scope.areaColumns = [];
          }

          scope.contentColumns = scope.lineColumns.concat(scope.areaColumns.filter(function (item) {
            return scope.lineColumns.indexOf(item) < 0;
          }));


          // Create a SVG root element
          svg = d3.select(element[0]).append('svg');

          // Add containers for graph, axes and legend
          svg.append('g').attr('class', 'data').attr('clip-path', 'url(#clip-path-' + scope.$id + ')');
          svg.append('g').attr('class', 'x-axis axis');
          svg.append('g').attr('class', 'y-axis-left axis');
          svg.append('g').attr('class', 'y-axis-right axis');
          svg.append('g').attr('class', 'legend');

          // Add clipping path
          svg.append('clipPath')
            .attr('id', 'clip-path-' + scope.$id)
            .append('rect')
            .attr('class', 'clip-rect');

          // Is the note taking functionality enabled?
          // If yes, enable double click event.
          if(scope.notesEnabled) {
            svg.on('dblclick', function() {
              // Prevent other action
              d3.event.preventDefault();

              // add Chart Note
              return addChartNote(scope.activeElement, scope.notesOptions, zoomKeyCtrl);
            });
          }

          // Add containers for mouse "cursor"
          var cursorCont = svg.append('g').attr('class', 'cursor');
          cursorCont.append('line').attr('class', 'x-cursor cursor');
          cursorCont.append('line').attr('class', 'y-cursor cursor');

          var focusCont = svg.append('g').attr('class', 'focus');
          focusCont.append('circle').attr('class', 'data-point-active');
          var xLabelNode = focusCont.append('g').attr('class', 'x-label label hidden');


          xLabelNode.append('rect')
            .style('display', 'block')
            .attr('width', 60)
            .attr('height', 15)
            .attr('transform', 'translate(-55, -11)');

          xLabelNode.append('text');


          var yLeftLabelNode = focusCont.append('g').attr('class', 'y-label-left label hidden');
          yLeftLabelNode.append('rect')
            .style('display', 'block')
            .attr('width', 60)
            .attr('height', 15)
            .attr('transform', 'translate(-55, -11)');

          yLeftLabelNode.append('text');


          var yRightLabelNode;
          if(scope.twoAxes) {
            yRightLabelNode = focusCont.append('g').attr('class', 'y-label-right label hidden');

            yRightLabelNode.append('rect')
              .style('display', 'block')
              .attr('width', 60)
              .attr('height', 15)
              .attr('transform', 'translate(-55, -11)');

            yRightLabelNode.append('text');
          }


          // Create root element for brush
          svgBrush = d3.select(element[0]).append('svg').attr('class', 'brush-svg');
          svgBrush.append('g').attr('class', 'data');
          svgBrush.append('g').attr('class', 'brush');


          // Listener that is being called when notes have been received by the notesViewer
          var notesListener = function(allKeywords) {
            // We also need to re-draw the chart if we had notes before and don't have any now.
            var oldNotes = (scope.notesKeywords ? scope.notesKeywords.length : 0);

            // Empty the notes keywords array...
            scope.notesKeywords = [];

            // Iterate over the keywords and push them to the scope...
            allKeywords.forEach(function(k) {
              if(scope.keywordsHash[k]) {
                scope.notesKeywords.push(scope.keywordsHash[k]);
              }
            });

            // TODO: Remove debug code
            console.log('notesListener received new keywords:');
            console.log([oldNotes, allKeywords, scope.notesKeywords, scope.keywordsHash]);

            // (Re-)draw the chart (if it is already loaded)
            if (scope.content && scope.content.length > 0 && (oldNotes > 0 || scope.notesKeywords.length > 0)) {
              draw(svg, svgBrush, scope);
            }
          };


          // Listener that is being called when a zoomKey has been given by the user
          // in the URL hash
          var zoomListener = function(zoomLink) {
            // The zoomLink supports two formats:
            // Just a date (in '2018-03-20' format) zooms to this element
            // Two dates zoom to from-to area
            var zoomFrom;
            var zoomTo;
            var zoomLinkMatch = zoomLink.match(/^([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9])(\/([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]))?/);

            if(zoomLinkMatch === null) {
              // Do nothing if zoom link doesn't match valid format.
              return;
            }

            if(!scope.content || !scope.timeColumn) {
              // If content hasn't loaded yet or brush does not exist yet, we do nothing
              return;
            }

            // Extract times from content
            var timeBisector = d3.bisector(function(d) { return d[scope.timeColumn]; });

            if(zoomLinkMatch[3]) {
              // Ok, we have the zoom-from -> zoom-to format
              zoomFrom = timeBisector.left(scope.content, d3.ourSettings.formats.date.parse(zoomLinkMatch[1]));
              zoomTo = Math.min(timeBisector.right(scope.content, d3.ourSettings.formats.date.parse(zoomLinkMatch[3])), scope.content.length-1);

            } else {
              // Ok, we have the "just a date" format, we zoom around this date
              var zoomAt = timeBisector.left(scope.content, d3.ourSettings.formats.date.parse(zoomLinkMatch[1]));
              zoomFrom = Math.max(0, zoomAt - 5);
              zoomTo = Math.min(zoomAt + 5, scope.content.length-1);
            }

            // Ok, now we have to set the brush extent and call draw again
            scope.brushExtent = [
              scope.content[zoomFrom][scope.timeColumn],
              scope.content[zoomTo][scope.timeColumn]
            ];

            // (Re-) draw the chart
            draw(svg, svgBrush, scope);

            // Jump to the anchor of the graph
            return '';
          };


          // Load data from file (using d3.js loading function)
          if(scope.contentFile) {
            scope.loading = true;

            // TODO - Remove debug code
            console.log('Retrieving content for chart from datafile ' + scope.contentFile);

            // Hide SVG
            svg.classed('hidden', true);

            // Create loading indicator
            loadingIndicator = d3.select(element[0]).append('div')
              .style('height', scope.height + 'px')
              .style('width', scope.width + 'px')
              .classed('loadingIndicator', true);

            // Read data file using data file reader (including caching)
            datafileReader.readDatafile(scope.contentFile)
              .then(
                function(res) {
                  var rows = res[0];
                  scope.columnsSpec = res[1];

                  // TODO - remove debug code
                  console.log('Obtained data for chart: ');
                  console.log(res);

                  // Initialize keywordsHash...
                  scope.keywordsHash = {};

                  // Ok, we need to filter the right columns.
                  scope.contentRaw = rows.map(
                    function(d) {
                      var ret = {};
                      ret[scope.timeColumn] = d[scope.timeColumn];

                      scope.contentColumns.forEach(
                        function(c) {
                          var dNumeric = Number(d[c]);
                          ret[c] = (isNaN(dNumeric) ? 0 : dNumeric);  // NaN will be set to zero

                          // We fill the notes keywords hashes...
                          var thisKey = keywordsService.getDataKey(
                            scope.contentFile,
                            d,
                            scope.columnsSpec,
                            c
                          );

                          scope.keywordsHash[thisKey] = {
                            keyword: thisKey,
                            time: d[scope.timeColumn],
                            column: c,
                            value: ret[c]
                          };
                        }
                      );

                      return ret;
                    }
                  );


                  // We set the columnKeywords, columnFormats and columnColors...
                  scope.columnKeywords = {};
                  scope.columnFormats = {};
                  scope.columnColors = {};
                  scope.columnsSpec.forEach(function(colDef) {
                    scope.columnKeywords[colDef.display] = colDef.keyword;
                    scope.columnFormats[colDef.display] = d3.ourSettings.formats[colDef.format];
                    scope.columnColors[colDef.display] = colDef.color;
                  });

                  // We sort the contentColumns by the sort order given in the column specification
                  var columnsSpecSorter = function(a, b) {
                    var indexA = scope.columnsSpec.findIndex(function(d) { return (d.display === a); });
                    var indexB = scope.columnsSpec.findIndex(function(d) { return (d.display === b); });
                    return (indexA - indexB);
                  };

                  scope.contentColumns.sort(columnsSpecSorter);
                  scope.lineColumns.sort(columnsSpecSorter);
                  scope.areaColumns.sort(columnsSpecSorter);


                  // We set the length of the moving average slider...
                  scope.maxMovingAverage = Math.max(0, Math.min(scope.contentRaw.length - 3, 365));

                  scope.content = makeMovingAverage(scope.contentRaw, scope.movingAverageElements, scope.timeColumn);
                  scope.contentHash = miscServices.hashCode(JSON.stringify(scope.content));
                  scope.errorMessage = '';
                  scope.loading = false;
                },
                function(err) {
                  console.log('An error occurred loading the graph: ');
                  console.log(err);

                  scope.content = [];
                  scope.contentHash = '';
                  scope.errorMessage = gettext('An error occurred while loading this graph:') + ' ' + err;
                  scope.loading = false;
                }
              )

              // Load notes if notes are enabled
              .then(function() {
                  if(scope.notesEnabled) {
                    notesViewer.loadNotes('data/' + scope.contentFile + '/', notesListener);
                  }
              });
          }

          // Watch the loading status of the scope
          scope.$watch('loading', function(newVal, oldVal, scope) {
            // Show chart or loading indicator?
            if(scope.loading) {
              svg.classed("hidden", true);
              loadingIndicator.attr('class', 'loadingIndicator');
            } else if(scope.errorMessage) {
              svg.classed("hidden", true);
              loadingIndicator.attr('class', 'loadingErrorMessage');
              loadingIndicator.text(scope.errorMessage);
            } else {
              svg.classed('hidden', false);
              loadingIndicator.attr('class', 'loadingIndicator hidden');
            }
          }, true);

          // Watch the content hash attribute of the scope
          // This is a shortcut to watching the whole content: Watching the huge
          // 'content' array generates a lot of work during each Angular $digest,
          // while watching contentHash is comparing only a single value.
          scope.$watch('contentHash', function(newVal, oldVal, scope) {
            // (Re-)draw the chart

            if(scope.content && scope.content.length > 0) {
              // We have content, so we register the zoom listener
              if(zoomKeyCtrl) {
                zoomKeyCtrl.onZoom(zoomListener);
              }

              // (Re-) draw the chart
              draw(svg, svgBrush, scope);
            }
          }, true);

          // Watch changes in the moving average that should be displayed and
          // re-draw chart when necessary.
          var movingAverageTimeout;
          scope.$watch('movingAverageElements', function(newVal, oldVal, scope) {
            if(newVal !== oldVal && !isNaN(newVal) && newVal >= 1 && scope.contentRaw && scope.contentRaw.length > 0) {
              if (movingAverageTimeout) {
                $timeout.cancel(movingAverageTimeout);
              }

              movingAverageTimeout = $timeout(function() {
                // Update content
                scope.content = makeMovingAverage(scope.contentRaw, newVal, scope.timeColumn);
                scope.contentHash = miscServices.hashCode(JSON.stringify(scope.content));
              }, 250); // delay 250 ms

            }
          }, true);
        }
      };
    }
  ])

  /**
   * @ngdoc directive
   * @name chartDirectives.sparklineChart
   * @restrict E
   * @description
   *
   * This directive displays a sparkline of the given size. It expects a content array of values that will be used for
   * the sparkline and an optional color for the line.
   *
   * @scope
   * @param {number} width The width of the sparkline to be displayed in pixels.
   * @param {number} height The height of the sparkline to be displayed in pixels.
   * @param {Array} content An array of numeric values which will be used to display the sparkline.
   * @param {number=} globalMin If this attribute is specified, all sparklines will be scaled to have a minimum value
   *                            of the lower of the minimum of the content values and `globalMin`. This attribute can
   *                            be used to scale multiple sparklines on the same range.
   * @param {number=} globalMax If this attribute is specified, all sparklines will be scaled to have a maximum value
   *                            of the larger of the maximum of the content values and `globalMax`. This attribute can
   *                            be used to scale multiple sparklines on the same range.
   * @param {string=} color Color of the sparkline (if not specified, the default from the stylesheet will be used).
   *
   */

  .directive('sparklineChart', ['gettext', 'd3',
    function(gettext, d3) {
      // Draw the sparkline
      var draw = function(svg, scope) {

        // TODO: Remove debug code
        console.log('Drawing sparkline chart... Scope received:');
        console.log(scope);

        // Update height and width
        svg
          .attr('width', scope.width)
          .attr('height', scope.height);

        // Create a copy of the content with all non-numeric values removed / skipped
        var cleanedContent = [];
        scope.content.forEach(function(d) {
          var dNumeric = Number(d);
          if(!isNaN(dNumeric)) {
            cleanedContent.push(dNumeric);
          }
        });

        // Define x-scale (in fact, the data is ordinal, but linear scale is easier to handle)
        var xScale = d3.scaleLinear()
          .domain([0, cleanedContent.length-1])   // number of elements in content array
          .range([0, scope.width]);

        // Define y-scale
        var yScale = d3.scaleLinear()
          .domain(d3.extent(cleanedContent.concat(scope.globalMin, scope.globalMax)))
          .range([scope.height-3, 3]);

        // Remove line if it already exists
        svg.selectAll('.sparkline-path').remove();

        // Line data
        var line = d3.line()
          .x(function(d, i) { return xScale(i); })
          .y(function(d) { return yScale(d); });

        // (Re-)Create line
        var sparklinePath = svg.append('path')
          .attr('class', 'sparkline-path')
          .attr('d', line(cleanedContent));

        // Do we have a color?
        if(scope.color) {
          sparklinePath.style('stroke', scope.color);
        }
      };


      return {
        restrict: 'E',

        scope: {
          width: '=',
          height: '=',
          content: '=',
          globalMin: '=?',
          globalMax: '=?',
          color: '=?'
        },

        template: '',

        link: function(scope, element) {
          // Create a SVG root element
          var svg = d3.select(element[0]).append('svg')
            .classed('sparkline-chart', true);

          // Do we have content? If yes, draw.
          if(scope.content && scope.content.length > 0) {
            draw(svg, scope);
          }

          // Watch the content of the scope for updates
          scope.$watch('content', function(newVal, oldVal, scope) {
            // (Re-)draw the chart
            if(scope.content && scope.content.length > 0) {
              draw(svg, scope);
            }
          }, true);

          // Watch the color for updates
          scope.$watch('color', function(newVal, oldVal, scope) {
            // (Re-)draw the chart
            if(scope.content && scope.content.length > 0) {
              draw(svg, scope);
            }
          }, true);
        }
      };
    }
  ])


  /**
   * @ngdoc directive
   * @name chartDirectives.stackedBarChart
   * @restrict E
   * @description
   *
   * This directive displays data values as stacked bar charts and allows to "move" between different points in time. For
   * this it expects two fixed columns `time` and `barColumn` which define which point in time and which stacked bar the
   * given data points relate to.
   *
   * All other columns will be displayed as stacked on top of each other for the given bar.
   *
   * @scope
   * @param {number} width The width of the chart to be displayed in pixels.
   * @param {number} height The height of the chart to be displayed in pixels.
   * @param {number=} legendWidth The width of the legend to be displayed in pixels. If empty, a default value will be calculated.
   * @param {string} contentFile Gives the identifier (Title) of a datafile specified in the Dashboard - Datafiles list. The chart directive will retrieve this datafile and display it in chart form.
   * @param {string} timeColumn Gives the name of the column that should be used as identifier for the points in time.
   * @param {string} barColumn Gives the name of the column that should be used as identifier for the individual bars.
   * @param {Array=} contentColumns If this attribute is specified and an array of column names is given, only the specified columns from the data file will be displayed. If this is omitted, all columns in the data file will be displayed.
   * @param {Array=} contentBarColumns If this attribute is specified and an array of barColumn values is given, only those barColumns will be displayed. Otherwise, all barColumns will be used.
   * @param {boolean} notesEnabled If set to `true`, the chart will allow for notes to be recorded on individual data records (by double clicking on the element).
   * @param {Object=} notesOptions If `notesEnabled` is set to `true`, an optional object can be passed here to overwrite options for the notes
   *                               directive, e.g. in order to enable impact flags on these notes items etc.
   *
   */

  .directive('stackedBarChart', ['$timeout', 'ngDialog', 'gettext', 'd3', 'miscServices', 'notesViewer', 'keywordsService', 'sidebarService', 'datafileReader',
    function($timeout, ngDialog, gettext, d3, miscServices, notesViewer, keywordsService, sidebarService, datafileReader) {

      // Draw the chart
      var draw = function(svg, scope) {
        // fixed margin (larger on right due to legend, on bottom due to axis)
        var margin_left = Math.round(scope.width * 0.05);
        var margin_right = Math.round(scope.width * 0.05 + scope.legendWidth);
        var margin_top = Math.round(scope.height * 0.05);
        var margin_bottom = Math.round(scope.height * 0.05) + 100; /* for axis */

        // TODO: Remove debug code
        console.log('Drawing barchart...');
        console.log(scope.content);



        // Obtain columns for drawing the chart
        var columns, idx;
        if(Array.isArray(scope.content) && scope.content.length > 0) {
          columns = Object.keys(scope.content[0]);

          // Remove time and barColumn columns
          idx = columns.indexOf(scope.timeColumn);
          if(idx > -1) {
            columns.splice(idx, 1);
          }

          idx = columns.indexOf(scope.barColumn);
          if(idx > -1) {
            columns.splice(idx, 1);
          }

          idx = columns.indexOf('barColumnText');
          if(idx > -1) {
            columns.splice(idx, 1);
          }

        } else {
          columns = [];
        }

        // Do we have existing activeColumns? Then use those, otherwise use all columns.
        if(!scope.activeColumns) { scope.activeColumns = columns.slice(0); }

        var contentExtract;
        if(scope.contentBarColumns) {
          // We need to filter the relevant columns
          contentExtract = scope.content.filter(function(d) {
            return(d[scope.timeColumn].getTime() === scope.currTime.getTime() && scope.contentBarColumns.indexOf(d.barColumnText) > -1);
          });
        } else {
          // We use all barColumns
          contentExtract = scope.content.filter(function(d) {
            return (d[scope.timeColumn].getTime() === scope.currTime.getTime());
          });
        }

        // Which bar columns actually exist?
        var barColumnsMap = {};
        contentExtract.forEach(function(d) {
          barColumnsMap[d.barColumnText] = true;
        });
        var currBarColumns = Object.keys(barColumnsMap);

        // Update height and width
        svg
          .attr('width', scope.width)
          .attr('height', scope.height);


        // xScale is an ordinary scale based on existing barColumn values
        var xScale = d3.scaleBand()
          .domain(currBarColumns)
          .rangeRound([ margin_left, scope.width - margin_right ], 0.05);


        var xAxis = d3.axisBottom(xScale)
          .tickSize(0)
          .tickPadding(6);


        // Draw x-axis (rotate labels)
        var xAxisContainer = svg.select('.x-axis');

        xAxisContainer
          .attr("transform", "translate(0, " + (scope.height-margin_bottom) + ")")
          .call(xAxis)
          .selectAll('text')
          .style('text-anchor', 'end')
          .attr('dx', '-1em')
          .attr('dy', '-.55em')
          .attr('transform', 'rotate(-90)');


        // Define yScale
        var yScale = d3.scaleLinear()
          .range([scope.height-margin_bottom, margin_top]);

        // Move y axis
        svg.select('.y-axis')
          .attr("transform", "translate(" + margin_left + ")");


        // Define chart colors
        var chartColors = d3.scaleOrdinal(d3.ourSettings.chartColors).domain(columns);

        // Draw chart
        drawBars(svg, contentExtract, columns, scope, xScale, yScale, chartColors);

        // Add legend
        var legend = svg.select('.legend');

        var legends = legend.selectAll('g').data(columns);

        var enteredLegends = legends
          .enter()
          .append('g')
          .attr('class', function(d, i) { return 'legend-' + i; })
          .attr("transform", function(d, i) { return "translate(0, " + (margin_top + (columns.length-1-i)*Math.min((scope.height-margin_top-margin_bottom)/columns.length, 30)) + ")"; });

        enteredLegends.append('rect')
          .attr('x', scope.width - scope.legendWidth)
          .attr('width', Math.min(60, scope.legendWidth * 0.3))
          .attr('height', Math.min((scope.height-margin_top-margin_bottom)/columns.length*0.6, 18))
          .style('fill', function(d) { return (scope.columnColors[d] ? scope.columnColors[d] : chartColors(d)); });

        enteredLegends.append('text')
          .attr('x', scope.width - scope.legendWidth + Math.min(70, scope.legendWidth * 0.35))
          .attr('y', Math.min((scope.height-margin_top-margin_bottom)/columns.length, 30)*0.5)
          .style('fill', 'black')
          .text(function(d) { return d; });

        // Fade out inactive columns
        enteredLegends
          .merge(legends)
            .classed('faded', function(d) { return (scope.activeColumns.indexOf(d) === -1); })

            // Interactivity for legend - click shows/hides line
            .on('click', function(d) {
              var setFaded = !d3.select(this).classed('faded');

              d3.select(this).classed('faded', setFaded);

              // Update activeColumns (preserve order of columns!)
              if(setFaded) {
                // Remove from active columns
                scope.activeColumns.splice(scope.activeColumns.indexOf(d), 1);
              } else {
                // Add back at the right place
                var j2 = 0;
                for(var j1 = 0;j1 < columns.length;j1++) {
                  if (scope.activeColumns[j2] === columns[j1]) {
                    j2++;
                  } // They match, so increment
                  else if (columns[j1] === d) {
                    scope.activeColumns.splice(j2, 0, d);
                  } // Add here!
                }
              }

              // Redraw chart and y-axis using new activeColumns
              drawBars(svg, contentExtract, columns, scope, xScale, yScale, chartColors);
            })

            // Doubleclick hides everything else and just shows this line
            .on('dblclick', function(d) {
              // Prevent other action
              d3.event.preventDefault();

              svg.select('.legend').selectAll('g').classed('faded', true);
              d3.select(this).classed('faded', false);

              // Active columns is now just this one column
              scope.activeColumns = [ d ];

              // Redraw chart and y-axis using new activeColumns
              drawBars(svg, contentExtract, columns, scope, xScale, yScale, chartColors);
            })

            // Right-click (which is propagated as "contextmenu" event) should clear
            // any highlighting, i.e. activate all columns
            .on('contextmenu', function() {
              svg.select('.legend').selectAll('.faded').classed('faded', false);

              // Active columns are now all columns
              scope.activeColumns = columns.slice(0);

              // Redraw chart and y-axis using new activeColumns
              drawBars(svg, contentExtract, columns, scope, xScale, yScale, chartColors);

              // Stop propagating of event (do not show browser context menu)
              d3.event.preventDefault();
            });


        // Add mouseover
        var mouseoverShown = false;

        var mouseoverBox = svg.select('.mouseover-box')
          .attr('width', Math.floor(0.9 * xScale.bandwidth()))
          .attr('height', 20);

        var mouseoverText = svg.select('.mouseover-text');

        svg.on('mousemove', function() {
          var pos = d3.mouse(this);


          // Only act on rect elements within the drawing area
          if(pos[0] < margin_left || pos[0] > scope.width - margin_right || pos[1] < margin_top || pos[1] > scope.height - margin_bottom || d3.event.target.nodeName.toUpperCase() !== 'RECT') {
            if(mouseoverShown) {
              mouseoverBox.classed('hidden', true);
              mouseoverText.classed('hidden', true);
              mouseoverShown = false;
            }

            scope.$apply(function() {
              scope.activeElement = undefined;
              notesViewer.showNotes();
            });

            return;
          }

          var d = d3.select(d3.event.target).datum();

          var col = d3.select(d3.event.target.parentNode).datum().key;
          var barCol = d.data.barColumnText;

          // We store the currently active data point (which data set, time point, barColumn and column) in the activeElement
          // variable, which is used when displaying the note taking dialog (after dblclick).
          scope.activeElement = {
            'keyword': keywordsService.getDataKey(scope.contentFile, d.data, scope.columnsSpec, col),
            'columnName': col,
            'barColumn': barCol,
            'time': scope.currTime
          };


          // TODO: Remove debug code...
          console.log('getDataKey: ');
          console.log([scope.contentFile, d, scope.columnsSpec, col]);
          console.log(scope.activeElement);


          mouseoverBox
            .attr('x', Math.floor(xScale(d.data.barColumnText) + 0.05 * xScale.bandwidth()))
            .attr('y', yScale((d[0] + d[1]) / 2) - 10)
            .classed('hidden', false);

          mouseoverText
            .text((scope.columnFormats[col] ? scope.columnFormats[col] : d3.format('.2f'))(d.data[col]))
            .attr('x', Math.floor(xScale(d.data.barColumnText) + xScale.bandwidth() / 2))
            .attr('y', yScale((d[0] + d[1]) / 2) + 5)
            .classed('hidden', false);

          mouseoverShown = true;

          // Does a note exist for this data point? If yes, we show the sidebar, otherwise we close it.
          scope.$apply(function() {
            notesViewer.showNotes(keywordsService.getDataKey(scope.contentFile, d.data, scope.columnsSpec, col));
          });
        });

      };

      // Helper function to draw
      var drawBars = function(svg, contentExtract, columns, scope, xScale, yScale, chartColors) {

        // If activeColumns isn't set, we use all columns
        if(!scope.activeColumns || scope.activeColumns.length === 0) {
          scope.activeColumns = columns.map(function(d) { return d; });
        }

        // Stack layout from D3 for stacked bar chart
        var stack = d3.stack()
          .keys(scope.activeColumns)
          .order(d3.stackOrderNone)
          .offset(d3.stackOffsetDiverging);

        var layers = stack(contentExtract);

        var yMin = d3.min(layers, function(layer) {
          return d3.min(layer, function(ele) {
              return (ele[1] > ele[0] ? ele[0] : ele[1]);
            }
          );
        });

        var yMax = d3.max(layers, function(layer) {
          return d3.max(layer, function(ele) {
              return (ele[1] > ele[0] ? ele[1] : ele[0]);
            }
          );
        });

        yScale.domain([yMin, yMax]);


        // Define y-axis
        var yAxis = d3.axisLeft(yScale)
          .tickFormat(scope.columnFormats[columns[0]] || d3.format(',f'));

        // Draw y-axis
        svg.select('.y-axis')
          .transition()
          .duration(250)
          .call(yAxis);


        // Draw stacked bar chart

        // Layers are grouped
        var layer = svg.select('.data').selectAll('.layer')
          .data(layers, function(d) { return d.key; });

        layer.exit()
          .remove();

        layer = layer.enter()
          .append('g')
          .attr('class', 'layer')

          .merge(layer)
            .style('fill', function(d) { return (scope.columnColors[d.key] ? scope.columnColors[d.key] : chartColors(d.key)); });


        // Rectangles are drawn for each layer value
        var rect = layer.selectAll('rect')
          .data(function(d) { return d; });

        rect.exit()
          .remove();

        rect.enter()
          .append('rect')
          .attr('y', function(d) {
            var myKeyword = keywordsService.getDataKey(scope.contentFile, d.data, scope.columnsSpec, d3.select(this.parentNode).datum().key);
            return yScale(d3.max(d)) + (scope.notesKeywords.indexOf(myKeyword) > -1 ? 2 : 0);
          })      // Workaround as stroke-alignment: inner is not yet supported
          .attr('height', function(d) {
            var myKeyword = keywordsService.getDataKey(scope.contentFile, d.data, scope.columnsSpec, d3.select(this.parentNode).datum().key);
            return Math.abs(yScale(d[0]) - yScale(d[1])) - (scope.notesKeywords.indexOf(myKeyword) > -1 ? 4 : 0);
          })      // Workaround as stroke-alignment: inner is not yet supported


          .merge(rect)
            .attr('x', function(d) { return xScale(d.data.barColumnText); })
            .attr('width', xScale.bandwidth())
            .classed('bar-has-notes', function(d) {
                var myKeyword = keywordsService.getDataKey(scope.contentFile, d.data, scope.columnsSpec, d3.select(this.parentNode).datum().key);
                return scope.notesKeywords.indexOf(myKeyword) > -1;
              })

            .transition()
            .duration(250)
            .attr('y', function(d) {
              var myKeyword = keywordsService.getDataKey(scope.contentFile, d.data, scope.columnsSpec, d3.select(this.parentNode).datum().key);
              return yScale(d3.max(d)) + (scope.notesKeywords.indexOf(myKeyword) > -1 ? 2 : 0);
            })      // Workaround as stroke-alignment: inner is not yet supported
            .attr('height', function(d) {
              var myKeyword = keywordsService.getDataKey(scope.contentFile, d.data, scope.columnsSpec, d3.select(this.parentNode).datum().key);
              return Math.abs(yScale(d[0]) - yScale(d[1])) - (scope.notesKeywords.indexOf(myKeyword) > -1 ? 4 : 0);
            });      // Workaround as stroke-alignment: inner is not yet supported


      };


      // Function for in-chart note taking - is called when user double clicks inside of the
      // clipping rectangle.
      // Uses ngDialog to show a note taking dialog and to display existing notes on this node.
      var addChartNote = function(myElement, notesOptions, zoomKeyCtrl) {
        // Open dialog with the notes view if we are double-clicking inside of the data area
        if(myElement) {
          ngDialog.open({
            template: 'common/directives/notesDialog.tmpl.html',
            className: 'ngdialog-theme-default',
            closeByDocument: false,
            data: {
              'keywordID': myElement.keyword,
              'sourceName': myElement.columnName + ' ' + gettext('and') + ' ' + myElement.barColumn + ' at ' + d3.timeFormat('%Y-%m-%d')(myElement.time),
              'notesOptions': notesOptions,
              'sourceLink': (zoomKeyCtrl ? zoomKeyCtrl.getZoomLink(d3.timeFormat('%Y-%m-%d')(myElement.time)) : '')
            }
          });
        }
      };



      return {
        restrict: 'E',

        scope: {
          width: '=',
          height: '=',
          legendWidth: '=?',
          contentFile: '=',
          timeColumn: '=',
          barColumn: '=',
          contentColumns: '=?',
          contentBarColumns: '=?',
          notesEnabled: '=',
          notesOptions: '=?'
        },

        require: '?^zoomKey',

        templateUrl: 'common/directives/chartBarChart.tmpl.html',

        link: function(scope, element, attrs, zoomKeyCtrl) {
          var svg, loadingIndicator;
          scope.loading = false;
          scope.errorMessage = '';
          scope.content = [];
          scope.contentHash = '';
          scope.columnKeywords = {};
          scope.notesKeywords = [];
          scope.currTime = undefined;
          scope.availableTime = [];
          scope.columnsSpec = [];
          scope.animationSpeed = 0;

          if(!scope.legendWidth) {
            scope.legendWidth = Math.round(0.25*scope.width);
          }


          // Create a SVG root element
          svg = d3.select(element[0]).append('svg');

          // Add containers for graph, axes and legend
          svg.append('g').attr('class', 'data');
          svg.append('g').attr('class', 'x-axis axis');
          svg.append('g').attr('class', 'y-axis axis');
          svg.append('g').attr('class', 'legend');

          // Add mouseover box
          svg.append('rect').attr('class', 'mouseover-box hidden');
          svg.append('text').attr('class', 'mouseover-text hidden');

          // Is the note taking functionality enabled?
          // If yes, enable double click event.
          if(scope.notesEnabled) {
            svg.on('dblclick', function() {
              // Prevent other action
              d3.event.preventDefault();

              // Show notes dialog
              return addChartNote(scope.activeElement, scope.notesOptions, zoomKeyCtrl);
            });
          }


          // The notesListener is called when new notes are received.
          var notesListener = function(allKeywords) {
            // We also need to re-draw the chart if we had notes before and don't have any now.
            var oldNotes = (scope.notesKeywords ? scope.notesKeywords.length : 0);

            // Set the notesKeywords to the received data...
            scope.notesKeywords = allKeywords;

            // (Re-)draw the chart (if it is already loaded)
            if (scope.content && scope.content.length > 0 && (oldNotes > 0 || scope.notesKeywords.length > 0)) {
              draw(svg, scope);
            }
          };


          // The zoomListener is called when a hash URL is given by the user that matches
          // this chart. It extracts the date to show.
          var zoomListener = function(zoomHash) {
            if(!zoomHash.match(/^([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9])/)) {
              // Hash doesn't match the expected format
              return;
            }

            var zoomTo = zoomHash.substr(0,10);

            // Update selectedTimeIndex
            var timeIndex = scope.availableTime.findIndex(function(d) {
              // TODO: Remove debug code
              console.log('Comparing availableTime entry...');
              console.log([d, new Date(d)]);
              console.log(d3.ourSettings.formats.date(new Date(d)));

              return d3.ourSettings.formats.date(new Date(d)) === zoomTo;
            });
            if(timeIndex !== null) {
              // Time is valid
              scope.selectedTimeIndex = timeIndex;

              // Workaround for bug in ngModel and input type=range, see https://github.com/angular/angular.js/issues/6726
              $timeout(function() {
                element.find('input').val(scope.selectedTimeIndex);
              });

              // We jump to this chart
              return '';
            }
          };


          // Load data from file (using d3.js loading function)
          if(scope.contentFile) {
            scope.loading = true;

            // Hide SVG
            svg.classed('hidden', true);

            // Create loading indicator
            loadingIndicator = d3.select(element[0]).append('div')
              .style('height', scope.height + 'px')
              .style('width', scope.width + 'px')
              .classed('loadingIndicator', true);

            // Read data file using data file reader (including caching)
            datafileReader.readDatafile(scope.contentFile)
              .then(
                function(res) {
                  var rows = res[0];
                  scope.columnsSpec = res[1];


                  // We set the columnKeywords and columnFormats...
                  scope.columnKeywords = {};
                  scope.columnFormats = {};
                  scope.columnColors = {};
                  scope.columnsSpec.forEach(function(colDef) {
                    scope.columnKeywords[colDef.display] = colDef.keyword;
                    scope.columnFormats[colDef.display] = d3.ourSettings.formats[colDef.format];
                    scope.columnColors[colDef.display] = colDef.color;
                  });


                  // TODO - remove debug code
                  console.log('Obtained data for chart: ');
                  console.log(res);

                  if(scope.contentColumns) {
                    // We sort the contentColumns by the sort order given in the column specification
                    var columnsSpecSorter = function(a, b) {
                      var indexA = scope.columnsSpec.findIndex(function(d) { return (d.display === a); });
                      var indexB = scope.columnsSpec.findIndex(function(d) { return (d.display === b); });
                      return (indexA - indexB);
                    };

                    scope.contentColumns.sort(columnsSpecSorter);


                    // Ok, we need to filter the right columns.
                    scope.content = rows.map(
                      function(d) {
                        var ret = {};
                        ret[scope.timeColumn] = d[scope.timeColumn];
                        ret[scope.barColumn] = d[scope.barColumn];
                        ret.barColumnText = (scope.columnFormats[scope.barColumn] ? scope.columnFormats[scope.barColumn](d[scope.barColumn]) : d[scope.barColumn]);

                        scope.contentColumns.forEach(
                          function(c) {
                            ret[c] = d[c];
                          }
                        );

                        return ret;
                      }
                    );
                  } else {

                    // We can just use all columns
                    scope.content = rows.map(
                      function(d) {
                        var ret = {};
                        ret[scope.timeColumn] = d[scope.timeColumn];
                        ret[scope.barColumn] = d[scope.barColumn];
                        ret.barColumnText = (scope.columnFormats[scope.barColumn] ? scope.columnFormats[scope.barColumn](d[scope.barColumn]) : d[scope.barColumn]);

                        Object.keys(d).forEach(
                          function(c) {
                            if(c !== scope.timeColumn && c !== scope.barColumn) {
                              ret[c] = d[c];
                            }
                          }
                        );

                        return ret;
                      }
                    );

                  }



                  // What is the largest time value? This will be set as currTime
                  var availableTimes = {};
                  scope.content.forEach(function(d) {
                    availableTimes[d[scope.timeColumn].getTime()] = true;
                  });


                  scope.availableTime = Object
                    .keys(availableTimes)
                    .map(function(time) { return parseInt(time, 10); })
                    .sort(function(a, b) { return a - b; });

                  scope.selectedTimeIndex = scope.availableTime.length-1;

                  // Register the zoomListener to jump to the timeIndex given in the hash
                  if(zoomKeyCtrl) {
                    zoomKeyCtrl.onZoom(zoomListener);
                  }

                  // Set current time
                  scope.currTime = new Date();
                  scope.currTime.setTime(scope.availableTime[scope.selectedTimeIndex]);

                  // Workaround for bug in ngModel and input type=range, see https://github.com/angular/angular.js/issues/6726
                  $timeout(function() {
                    element.find('input').val(scope.selectedTimeIndex);
                  });

                  scope.contentHash = miscServices.hashCode(JSON.stringify(scope.content));
                  scope.errorMessage = '';
                  scope.loading = false;
                },
                function(err) {
                  console.log('An error occurred loading the graph: ');
                  console.log(err);

                  scope.content = [];
                  scope.contentHash = '';
                  scope.errorMessage = gettext('An error occurred while loading this graph:') + ' ' + err;
                  scope.loading = false;
                }
              )

              // Load notes if notes are enabled
              .then(function() {
                if(scope.notesEnabled) {
                  notesViewer.loadNotes('data/' + scope.contentFile + '/', notesListener);
                }
              });
          }


          // Helper function to animate the date slider
          scope.startAnimation = function() {
            // If the animation is already running, we just increase the speed.
            if(scope.animationSpeed) {
              scope.animationSpeed++;
              return;
            }

            // Otherwise we need to set-up the animation first
            var nextStep;
            nextStep = function() {
              scope.selectedTimeIndex = Number(scope.selectedTimeIndex) || 0;
              scope.selectedTimeIndex = (scope.selectedTimeIndex === scope.availableTime.length-1 ? 0 : scope.selectedTimeIndex + 1);

              if(scope.animationSpeed) {
                $timeout(nextStep, 800/scope.animationSpeed);
              }
            };

            scope.animationSpeed = 1;
            nextStep();
          };

          scope.stopAnimation = function() {
            // stop the animation
            scope.animationSpeed = 0;
          };


          // Watch the loading status of the scope
          scope.$watch('loading', function(newVal, oldVal, scope) {
            // Show chart or loading indicator?
            if(scope.loading) {
              svg.classed("hidden", true);
              loadingIndicator.attr('class', 'loadingIndicator');
            } else if(scope.errorMessage) {
              svg.classed("hidden", true);
              loadingIndicator.attr('class', 'loadingErrorMessage');
              loadingIndicator.text(scope.errorMessage);
            } else {
              svg.classed('hidden', false);
              loadingIndicator.attr('class', 'loadingIndicator hidden');
            }
          }, true);

          // Watch the content hash attribute of the scope
          // This is a shortcut to watching the whole content: Watching the huge
          // 'content' array generates a lot of work during each Angular $digest,
          // while watching contentHash is comparing only a single value.
          scope.$watch('contentHash', function(newVal, oldVal, scope) {
            // (Re-)draw the chart

            if(scope.content && scope.content.length > 0) {
              draw(svg, scope);
            }
          }, true);

          // Watch the selectedTimeIndex to update currTime and redraw the chart
          scope.$watch('selectedTimeIndex', function(newVal, oldVal, scope) {
            if(newVal !== oldVal && scope.availableTime[newVal]) {
              // Update currTime
              scope.currTime.setTime(scope.availableTime[newVal]);

              // Re-draw if data has already been loaded
              if(scope.content && scope.content.length > 0) {
                draw(svg, scope);
              }
            }
          });

        }
      };
    }
  ])




  /**
   * @ngdoc directive
   * @name chartDirectives.scatterplotChart
   * @restrict E
   * @description
   *
   * This directive displays data values as scatterplot charts and allows to "move" between different points in time. A
   * mouseover can be provided that displays exact values and an additional description. The directive supports grouping
   * elements by different colors and displaying an interactive legend for them.
   *
   * The various required columns in the datafile can be configured as attributes to the directive (see below).
   *
   * @scope
   * @param {number} width                The width of the chart to be displayed in pixels.
   * @param {number} height               The height of the chart to be displayed in pixels.
   * @param {number=} legendWidth         The width of the legend to be displayed in pixels. If empty, a default value will
   *                                      be calculated.
   * @param {number} circleSize           The size of the bubbles on the chart in pixels (if `contentSize` is given, this is the
   *                                      maximum size all bubbles will be scaled relative to, otherwise it is the fixed size).
   * @param {string} contentFile          Gives the identifier (Title) of a datafile specified in the Dashboard - Datafiles list.
   *                                      The chart directive will retrieve this datafile and display it in chart form.
   * @param {string=} contentTime   	  Specifies the (display) name of the column to be used for the time value that is used
   *                                	  for the time slider. If left empty, the time slider will not be displayed.
   * @oaram {string}  contentX      	  Specifies the (display) name of the column whose values will determine the position
   *                                	  of the scatterplot item along the x axis.
   * @oaram {string}  contentY      	  Specifies the (display) name of the column whose values will determine the position
   *                                	  of the scatterplot item along the y axis.
   * @oaram {string=} contentSize   	  Specifies the (display) name of the column whose values will determine the size
   *                                	  of the bubbles on the scatterplot. If omitted, all items will be shown in the same
   *                                	  size.
   * @oaram {string=} contentGroup  	  Specifies the (display) name of the column whose values will determine the group
   *                                	  a scatterplot items belongs to (determines the colour and will be shown on the
   *                                	  legend). If omitted, all items will be shown in the same colour and no legend will
   *                                	  be displayed.
   * @oaram {string=} contentTitle  	  Specifies the (display) name of the column whose values will determine the label
   *                                	  for this data element, which will be displayed when adding or viewing notes
   *                                	  attached to this data element. If omitted, no title will be shown.
   * @oaram {string=} tooltip       	  Text that will be shown in the tooltip which is displayed when the user moves the
   *                                	  cursor over one of the scatterplot items. With `{{columname}}` the value of the
   *                                	  column `columname` for the given item can be displayed. If omitted, no description
   *                                	  will be shown.
   * @param {string}  labelX        	  The label to be displayed on the x axis of the chart.
   * @param {string}  labelY        	  The label to be displayed on the y axis of the chart.
   * @param {Array=}  groupCustomColors   If specified, this needs to be an array of objects each with a property `group` and
                                          `color`. For the given `group` entries that match an entry in the `contentGroup`
                                          column, their default color will be overwritten with the color specification (e.g.
                                          an RGB value given as `#aabbcc`) given in their `color` property.
   * @param {boolean} notesEnabled If set to `true`, the chart will allow for notes to be recorded on individual
   *                               data records (by double clicking on the element).
   * @param {Object=} notesOptions If `notesEnabled` is set to `true`, an optional object can be passed here to overwrite options for the notes
   *                               directive, e.g. in order to enable impact flags on these notes items etc.
   *
   */

  .directive('scatterplotChart', ['$timeout', 'ngDialog', 'gettext', 'd3', 'miscServices', 'notesViewer', 'keywordsService', 'sidebarService', 'datafileReader',
    function($timeout, ngDialog, gettext, d3, miscServices, notesViewer, keywordsService, sidebarService, datafileReader) {

      // Draw the chart
      var draw = function(svg, scope) {
        // fixed margin (if contentGroup is set, use larger margin on right due to legend; always use larger margin
        //               on bottom due to axis)
        var margin_left = Math.round(scope.width * 0.05);
        var margin_right = Math.round(scope.width * 0.05 + (scope.contentGroup ? scope.legendWidth : 0));
        var margin_top = Math.round(scope.height * 0.05);
        var margin_bottom = Math.round(scope.height * 0.05) + 100; /* for axis */


        // Create list of all available groups in the content
        var availableGroups = [];
        if(scope.contentGroup) {
          var tmpGroupHash = {};

          scope.content.forEach(function(d) {
            tmpGroupHash[d[scope.contentGroup]] = true;
          });

          availableGroups = Object.keys(tmpGroupHash);
        }

        // Do we have existing activeGroups? Then use those, otherwise use all available contentGroups.
        if(!scope.activeGroups) {
          scope.activeGroups = availableGroups.slice(0);
        }


        // Update height and width
        svg
          .attr('width', scope.width)
          .attr('height', scope.height);


        // Update clipping path
        svg
          .select('.clip-rect')
          .attr('x', margin_left)
          .attr('y', margin_top)
          .attr('width', scope.width - margin_left - margin_right)
          .attr('height', scope.height - margin_top - margin_bottom);


        // Define xScale
        var xScale = d3.scaleLinear()
          .range([ margin_left, scope.width - margin_right ]);

        // Move x-axis
        svg.select('.x-axis')
          .attr("transform", "translate(0, " + (scope.height-margin_bottom) + ")");

        // Print x-axis label
        svg.select('.x-axis-label')
          .text(scope.labelX)
          .attr('x', Math.floor(margin_left + (scope.width - margin_left - margin_right) / 2))
          .attr('y', Math.floor(scope.height - margin_bottom/2));


        // Define yScale
        var yScale = d3.scaleLinear()
          .range([ scope.height-margin_bottom, margin_top ]);

        // Move y axis
        svg.select('.y-axis')
          .attr("transform", "translate(" + margin_left + ")");

        // Print y-axis label
        svg.select('.y-axis-label')
          .text(scope.labelY)
          .attr('transform', 'rotate(-90)')
          .attr('x', -1*Math.floor(margin_top + (scope.height - margin_top - margin_bottom)/2))
          .attr('y', Math.floor(margin_left / 3));



        // Define size scale (square root, so that volume of circle corresponds linearly to data)
        var sizeScale = d3.scaleSqrt()
          .range([ Math.floor(0.1 * scope.circleSize), scope.circleSize ]);


        // Define chart colors
        var chartColors;
        if(scope.contentGroup) {
          chartColors = d3.scaleOrdinal(d3.ourSettings.chartColors).domain(availableGroups);

	  if(scope.groupCustomColors) {
	    var groupCustomColorsHash = {};
            scope.groupCustomColors.forEach(function(custom) {
	      groupCustomColorsHash[custom.group] = custom.color;
	    });

	    var chartColorsDefault = d3.scaleOrdinal(d3.ourSettings.chartColors).domain(availableGroups);
	    
	    chartColors = function(group) {
	      if(groupCustomColorsHash[group] !== undefined) {
		return groupCustomColorsHash[group];
	      } else {
		return chartColorsDefault(group);
	      }
	    };
	  } else {
            chartColors = d3.scaleOrdinal(d3.ourSettings.chartColors).domain(availableGroups);
	  }

	} else {
          var oneColor = d3.ourSettings.chartColors[0];
          chartColors = function() { return oneColor; };
        }


        // Draw chart
        drawScatterplot(svg, scope, availableGroups, xScale, yScale, sizeScale, chartColors);


        // Do we need a legend? (We only show a legend if we have multiple groups in our data set.)
        var legend = svg.select('.legend');
        if(availableGroups.length) {
          legend.classed('hidden', false);

          var legends = legend.selectAll('g').data(availableGroups);

          legends.exit()
            .remove();

          var enteredLegends = legends
            .enter()
            .append('g')
            .attr('class', function(d, i) { return 'legend-' + i; })
            .attr("transform", function(d, i) { return "translate(0, " + (margin_top + i*Math.min((scope.height-margin_top-margin_bottom)/availableGroups.length, 30)) + ")"; });

          enteredLegends.append('rect')
            .attr('x', scope.width - scope.legendWidth)
            .attr('width', Math.min(60, scope.legendWidth * 0.3))
            .attr('height', Math.min((scope.height-margin_top-margin_bottom)/availableGroups.length*0.6, 18))
            .style('fill', function(d) { return chartColors(d); });

          enteredLegends.append('text')
            .attr('x', scope.width - scope.legendWidth + Math.min(70, scope.legendWidth * 0.35))
            .attr('y', Math.min((scope.height-margin_top-margin_bottom)/availableGroups.length, 30)*0.5)
            .style('fill', 'black')
            .text(function(d) { return d; });

          // Update all legends
          enteredLegends
            .merge(legends)

              // Fade out inactive columns
              .classed('faded', function(d) { return (scope.activeGroups.indexOf(d) === -1); })

              // Interactivity for legend - click shows/hides line
              .on('click', function(d) {
                var setFaded = !d3.select(this).classed('faded');

                d3.select(this).classed('faded', setFaded);

                // Update activeColumns (preserve order of columns!)
                if(setFaded) {
                  // Remove from active groups
                  scope.activeGroups.splice(scope.activeGroups.indexOf(d), 1);
                } else {
                  // Add back at the right place
                  var j2 = 0;
                  for(var j1 = 0;j1 < availableGroups.length;j1++) {
                    if (scope.activeGroups[j2] === availableGroups[j1]) {
                      j2++;
                    } // They match, so increment
                    else if (availableGroups[j1] === d) {
                      scope.activeGroups.splice(j2, 0, d);
                    } // Add here!
                  }
                }

                // Redraw chart and axes using new activeGroups
                drawScatterplot(svg, scope, availableGroups, xScale, yScale, sizeScale, chartColors);
              })

              // Interactivity - double click hides all other lines
              .on('dblclick', function(d) {
                // Prevent other action
                d3.event.preventDefault();

                // Fade all but this line
                svg.select('.legend').selectAll('g').classed('faded', true);
                d3.select(this).classed('faded', false);

                // Show only this line
                scope.activeGroups = [ d ];

                // Redraw chart
                drawScatterplot(svg, scope, availableGroups, xScale, yScale, sizeScale, chartColors);
              })

              // Interactivity - right click shows everything
             .on('contextmenu', function() {
                // Fade nothing
                svg.select('.legend').selectAll('g').classed('faded', false);

                // Show only this line
                scope.activeGroups = availableGroups.slice(0);

                // Redraw chart
                drawScatterplot(svg, scope, availableGroups, xScale, yScale, sizeScale, chartColors);

               // Stop propagating of event (do not show browser context menu)
               d3.event.preventDefault();
             });

        } else {
          // We hide the legend because we do not need it.
          legend.classed('hidden', true);
        }

        // Add mouseover
        var mouseoverShown = false;

        var mouseoverGroup = svg.select('.scatterplot-mouseover');

        mouseoverGroup.select('.scatterplot-mouseover-box')
          .attr('width', Math.floor(0.3 * scope.width))
          .attr('height', Math.floor(0.15 * scope.height));

        svg.on('mousemove', function() {
          var pos = d3.mouse(this);

          // Only act on circle elements within the drawing area
          if(pos[0] < margin_left || pos[0] > scope.width - margin_right || pos[1] < margin_top || pos[1] > scope.height - margin_bottom || d3.event.target.nodeName.toUpperCase() !== 'CIRCLE') {
            if(mouseoverShown) {
              mouseoverGroup.classed('hidden', true);
              mouseoverShown = false;
            }

            scope.$apply(function() {
              scope.activeElement = undefined;
              notesViewer.showNotes();
            });
            return;
          }

          var d = d3.select(d3.event.target).datum();

          // We store the currently active data point (which data set, time point and key) in the activeElement
          // variable, which is used when displaying the note taking dialog (after dblclick).
          var myKeyword = keywordsService.getDataKey(scope.contentFile, d, scope.columnsSpec, scope.contentY);
          scope.activeElement = {
            'keyword': myKeyword,
            'description': (scope.contentTitle ? d[scope.contentTitle] : ''),
            'time': scope.currTime
          };

          mouseoverGroup
            .attr('transform', 'translate(' + (xScale(d[scope.contentX]) + 10) + ', ' + (yScale(d[scope.contentY]) + 10) + ')')
            .classed('hidden', false);

          var mouseoverLines = scope.tooltip
            .replace(/{{([^}]+)}}/g, function(match, column) {
              return (scope.columnFormats[column] ? scope.columnFormats[column](d[column]) : match);
            })
            .split('\n');

          var line;
          for(line = 1;line <= 4;line++) {
            mouseoverGroup
              .select('.scatterplot-mouseover-line-' + line)
              .text(mouseoverLines[line-1]);
          }

          mouseoverShown = true;

          // Does a note exist for this data point? If yes, we show the sidebar, otherwise we close it.
          scope.$apply(function() {
            notesViewer.showNotes(myKeyword);
          });
        });

      };


      // Helper function to draw
      var drawScatterplot = function(svg, scope, availableGroups, xScale, yScale, sizeScale, chartColors) {

        // Time-specific content
        var timedContent = scope.content.filter(function(d) { return d[scope.contentTime].getTime() === scope.currTime.getTime(); });

        // Filtered content
        var filteredContent;
        if(availableGroups.length && availableGroups.length > scope.activeGroups.length) {
          filteredContent = timedContent.filter(function(d) { return scope.activeGroups.indexOf(d[scope.contentGroup]) !== -1; });
        } else {
          filteredContent = timedContent;
        }

        // Scale x-axis
        var xExtent = d3.extent(filteredContent, function(d) { return d[scope.contentX]; });

        // TODO: Remove debug code!
        console.log('x domain setting...');
        console.log(xExtent);
        console.log(scope.circleSize);
        console.log(xScale.range());
        console.log([
          xExtent[0] - scope.circleSize * (xExtent[1] - xExtent[0]) / Math.abs(xScale.range()[1] - xScale.range()[0]),
          xExtent[1] + scope.circleSize * (xExtent[1] - xExtent[0]) / Math.abs(xScale.range()[1] - xScale.range()[0])
        ]);


        xScale.domain([
          xExtent[0] - scope.circleSize * (xExtent[1] - xExtent[0]) / Math.abs(xScale.range()[1] - xScale.range()[0]),
          xExtent[1] + scope.circleSize * (xExtent[1] - xExtent[0]) / Math.abs(xScale.range()[1] - xScale.range()[0])
        ]);

        // Scale y-axis
        var yExtent = d3.extent(filteredContent, function(d) { return d[scope.contentY]; });
        yScale.domain([
          yExtent[0] - scope.circleSize * (yExtent[1] - yExtent[0]) / Math.abs(yScale.range()[1] - yScale.range()[0]),
          yExtent[1] + scope.circleSize * (yExtent[1] - yExtent[0]) / Math.abs(yScale.range()[1] - yScale.range()[0])
        ]);

        // Scale sizes (if given, otherwise fix)
        if(scope.contentSize) {
          sizeScale.domain(
            d3.extent(filteredContent, function(d) { return d[scope.contentSize]; })
          );
        } else {
          sizeScale.domain([0, 1]);
        }


        // Define x-axis
        var xAxis = d3.axisBottom(xScale)
          .tickFormat(scope.columnFormats[scope.contentX] || d3.format('f'));


        // Draw x-axis (rotate labels)
        var xAxisContainer = svg.select('.x-axis');

        xAxisContainer
          .transition()
          .duration(250)
          .call(xAxis);

        xAxisContainer
          .selectAll('text')
          .style('text-anchor', 'end')
          .attr('dx', '-1em')
          .attr('dy', '-.55em')
          .attr('transform', 'rotate(-90)');


        // Define y-axis
        var yAxis = d3.axisLeft(yScale)
          .tickFormat(scope.columnFormats[scope.contentY] || d3.format('f'));

        // Draw y axis
        svg.select('.y-axis')
          .transition()
          .duration(250)
          .call(yAxis);



        // Get relevant notes keywords to identify notes to highlight
        var relevantKeys = {};
        scope.notesKeywords.forEach(function(k) {
          relevantKeys[k] = true;
        });


        // TODO: Remove debug code
        console.log('Drawing data for scatterplot:');
        console.log(timedContent);

        // Draw circles
        var circles = svg.select('.data').selectAll('circle')
          .data(timedContent, function(d) {
            return keywordsService.getDataKey(undefined, d, scope.columnsSpec, undefined, scope.contentTime);

          });

        circles.exit()
          .remove();

        circles.enter()
            .append('circle')
            .attr('class', 'scatterplot-element')
            .attr('cx', function(d) { return xScale(d[scope.contentX]); })      // We already set new circles to their right
            .attr('cy', function(d) { return yScale(d[scope.contentY]); })      // position so that they don't move in from nowhere

          .merge(circles)
            .attr('fill', (availableGroups.length ? function(d) { return chartColors(d[scope.contentGroup]); } : chartColors()))
            .classed('scatterplot-faded-out', (availableGroups.length && availableGroups.length > scope.activeGroups.length ? function(d) { return scope.activeGroups.indexOf(d[scope.contentGroup]) === -1; } : false))
            .classed('scatterplot-has-notes', function(d) { return (relevantKeys[keywordsService.getDataKey(scope.contentFile, d, scope.columnsSpec, scope.contentY)] === true); })

            .transition()
            .duration(250)
            .attr('cx', function(d) { return xScale(d[scope.contentX]); })
            .attr('cy', function(d) { return yScale(d[scope.contentY]); })
            .attr('r',  (scope.contentSize ? function(d) { return sizeScale(d[scope.contentSize]); } : sizeScale(1)));

      };


      // Function for in-chart note taking - is called when user double clicks inside of the
      // clipping rectangle.
      // Uses ngDialog to show a note taking dialog and to display existing notes on this node.
      var addChartNote = function(myElement, notesOptions, zoomKeyCtrl) {
        // Open dialog with the notes view if we are double-clicking inside of the data area
        if(myElement) {
          ngDialog.open({
            template: 'common/directives/notesDialog.tmpl.html',
            className: 'ngdialog-theme-default',
            closeByDocument: false,
            data: {
              'keywordID': myElement.keyword,
              'sourceName': myElement.description + ' at ' + d3.timeFormat('%Y-%m-%d')(myElement.time),
              'notesOptions': notesOptions,
              'sourceLink': (zoomKeyCtrl ? zoomKeyCtrl.getZoomLink(d3.timeFormat('%Y-%m-%d')(myElement.time)) : '')
            }
          });
        }
      };



      return {
        restrict: 'E',

        scope: {
          width: '=',
          height: '=',
          legendWidth: '=?',
          circleSize: '=',
          contentFile: '=',
          contentTime: '=',
          contentX: '=',
          contentY: '=',
          contentSize: '=?',
          contentGroup: '=?',
          contentTitle: '=?',
          tooltip: '=?',
          labelX: '=',
          labelY: '=',
	  groupCustomColors: '=?',
          notesEnabled: '=',
          notesOptions: '=?'
        },

        require: '?^zoomKey',

        templateUrl: 'common/directives/chartScatterplot.tmpl.html',

        link: function(scope, element, attrs, zoomKeyCtrl) {
          var svg, loadingIndicator;
          scope.loading = false;
          scope.errorMessage = '';
          scope.content = [];
          scope.contentHash = '';
          scope.notesKeywords = [];
          scope.currTime = undefined;
          scope.availableTime = [];
          scope.columnsSpec = [];
          scope.columnKeywords = {};
          scope.columnFormats = {};
          scope.animationSpeed = 0;

          if(scope.contentGroup && !scope.legendWidth) {
            scope.legendWidth = Math.round(0.25 * scope.width);
          }

          // Create a SVG root element
          svg = d3.select(element[0]).append('svg');

          // Add clipping path
          svg.append('clipPath')
            .attr('id', 'clip-path-' + scope.$id)
            .append('rect')
            .attr('class', 'clip-rect');

          // Add containers for graph, axes, axes labels and legend
          svg.append('g').attr('class', 'data')
            .attr('clip-path', 'url(#clip-path-' + scope.$id + ')');

          svg.append('g').attr('class', 'x-axis axis');
          svg.append('text').attr('class', 'x-axis-label axis-label');

          svg.append('g').attr('class', 'y-axis axis');
          svg.append('text').attr('class', 'y-axis-label axis-label');

          svg.append('g').attr('class', 'legend');


          // Add mouseover box
          var mouseoverGroup = svg.append('g').attr('class', 'scatterplot-mouseover hidden');

          mouseoverGroup.append('rect').attr('class', 'scatterplot-mouseover-box');

          var mouseoverTextElement = mouseoverGroup.append('text').attr('class', 'scatterplot-mouseover-text');

          mouseoverTextElement
            .append('tspan')
            .attr('class', 'scatterplot-mouseover-line-1')
            .attr('x', 0)
            .attr('dy', '1.2em');
          mouseoverTextElement
            .append('tspan')
            .attr('class', 'scatterplot-mouseover-line-2')
            .attr('x', 0)
            .attr('dy', '1.2em');
          mouseoverTextElement
            .append('tspan')
            .attr('class', 'scatterplot-mouseover-line-3')
            .attr('x', 0)
            .attr('dy', '1.2em');
          mouseoverTextElement
            .append('tspan')
            .attr('class', 'scatterplot-mouseover-line-4')
            .attr('x', 0)
            .attr('dy', '1.2em');


          // Is the note taking functionality enabled?
          // If yes, enable double click event.
          if(scope.notesEnabled) {
            svg.on('dblclick', function() {
              // Prevent other action
              d3.event.preventDefault();

              // Show notes dialog
              return addChartNote(scope.activeElement, scope.notesOptions, zoomKeyCtrl);
            });
          }


          // The notesListener is called when new notes are received.
          var notesListener = function(allKeywords) {
            // We also need to re-draw the chart if we had notes before and don't have any now.
            var oldNotes = (scope.notesKeywords ? scope.notesKeywords.length : 0);

            // Set the notesKeywords to the received data...
            scope.notesKeywords = allKeywords;

            // (Re-)draw the chart (if it is already loaded)
            if (scope.content && scope.content.length > 0 && (oldNotes > 0 || scope.notesKeywords.length > 0)) {
              draw(svg, scope);
            }
          };


          // The zoomListener is called when a hash URL is given by the user that matches
          // this chart. It extracts the date to show.
          var zoomListener = function(zoomHash) {
            if(!zoomHash.match(/^([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9])/)) {
              // Hash doesn't match the expected format
              return;
            }

            var zoomTo = zoomHash.substr(0,10);

            // Update selectedTimeIndex
            var timeIndex = scope.availableTime.findIndex(function(d) { return d3.ourSettings.formats.date(new Date(d)) === zoomTo; });
            if(timeIndex !== null) {
              // Time is valid
              scope.selectedTimeIndex = timeIndex;

              // Workaround for bug in ngModel and input type=range, see https://github.com/angular/angular.js/issues/6726
              $timeout(function() {
                element.find('input').val(scope.selectedTimeIndex);
              });

              // We jump to this chart
              return '';
            }
          };



          // Load data from file (using d3.js loading function)
          if(scope.contentFile) {
            scope.loading = true;

            // Hide SVG
            svg.classed('hidden', true);

            // Create loading indicator
            loadingIndicator = d3.select(element[0]).append('div')
              .style('height', scope.height + 'px')
              .style('width', scope.width + 'px')
              .classed('loadingIndicator', true);

            // Read data file using data file reader (including caching)
            datafileReader.readDatafile(scope.contentFile)
              .then(
                function(res) {
                  // TODO - remove debug code
                  console.log('Obtained data for chart: ');
                  console.log(res);

                  // Set content to be displayed
                  scope.content = res[0];

                  // Set column specifications
                  scope.columnsSpec = res[1];


                  // We set the columnKeywords and columnFormats...
                  scope.columnKeywords = {};
                  scope.columnFormats = {};
                  scope.columnsSpec.forEach(function(colDef) {
                    scope.columnKeywords[colDef.display] = colDef.keyword;
                    scope.columnFormats[colDef.display] = d3.ourSettings.formats[colDef.format];
                  });

                  // What is the largest time value? This will be set as currTime
                  var availableTimes = {};
                  scope.content.forEach(function(d) {
                    availableTimes[d[scope.contentTime].getTime()] = true;
                  });

                  scope.availableTime = Object
                    .keys(availableTimes)
                    .map(function(time) { return parseInt(time, 10); })
                    .sort(function(a, b) { return a - b; });

                  scope.selectedTimeIndex = scope.availableTime.length-1;

                  // Register the zoomListener to jump to the timeIndex given in the hash
                  if(zoomKeyCtrl) {
                    zoomKeyCtrl.onZoom(zoomListener);
                  }

                  scope.currTime = new Date();
                  scope.currTime.setTime(scope.availableTime[scope.selectedTimeIndex]);

                  // Workaround for bug in ngModel and input type=range, see https://github.com/angular/angular.js/issues/6726
                  $timeout(function() {
                    element.find('input').val(scope.selectedTimeIndex);
                  });

                  scope.contentHash = miscServices.hashCode(JSON.stringify(scope.content));
                  scope.errorMessage = '';
                  scope.loading = false;
                },
                function(err) {
                  console.log('An error occurred loading the graph: ');
                  console.log(err);

                  scope.content = [];
                  scope.contentHash = '';
                  scope.errorMessage = gettext('An error occurred while loading this graph:') + ' ' + err;
                  scope.loading = false;
                }
              )

              // Load notes if notes are enabled
              .then(function() {
                if(scope.notesEnabled) {
                  notesViewer.loadNotes('data/' + scope.contentFile + '/', notesListener);
                }
              });
          }

          // Helper function to animate the date slider
          scope.startAnimation = function() {
            // If the animation is already running, we just increase the speed.
            if(scope.animationSpeed) {
              scope.animationSpeed++;
              return;
            }

            // Otherwise we need to set-up the animation first
            var nextStep;
            nextStep = function() {

              // Convert to number
              scope.selectedTimeIndex = Number(scope.selectedTimeIndex) || 0;
              scope.selectedTimeIndex = (scope.selectedTimeIndex === scope.availableTime.length-1 ? 0 : scope.selectedTimeIndex + 1);

              // TODO: Remove debug code!
              console.log(scope.selectedTimeIndex);
              console.log('nextStep done.');

              if(scope.animationSpeed) {
                $timeout(nextStep, 800/scope.animationSpeed);
              }
            };

            scope.animationSpeed = 1;
            nextStep();
          };

          scope.stopAnimation = function() {
            // stop the animation
            scope.animationSpeed = 0;
          };


          // Watch the loading status of the scope
          scope.$watch('loading', function(newVal, oldVal, scope) {
            // Show chart or loading indicator?
            if(scope.loading) {
              svg.classed("hidden", true);
              loadingIndicator.attr('class', 'loadingIndicator');
            } else if(scope.errorMessage) {
              svg.classed("hidden", true);
              loadingIndicator.attr('class', 'loadingErrorMessage');
              loadingIndicator.text(scope.errorMessage);
            } else {
              svg.classed('hidden', false);
              loadingIndicator.attr('class', 'loadingIndicator hidden');
            }
          }, true);

          // Watch the content hash attribute of the scope
          // This is a shortcut to watching the whole content: Watching the huge
          // 'content' array generates a lot of work during each Angular $digest,
          // while watching contentHash is comparing only a single value.
          scope.$watch('contentHash', function(newVal, oldVal, scope) {
            // (Re-)draw the chart

            if(scope.content && scope.content.length > 0) {
              draw(svg, scope);
            }
          }, true);

          // Watch the selectedTimeIndex to update currTime and redraw the chart
          scope.$watch('selectedTimeIndex', function(newVal, oldVal, scope) {
            if(newVal !== oldVal && scope.availableTime[newVal]) {
              // Update currTime
              scope.currTime.setTime(scope.availableTime[newVal]);

              // Re-draw if data has already been loaded
              if(scope.content && scope.content.length > 0) {
                draw(svg, scope);
              }
            }
          });

        }
      };
    }
  ])





  /**
   * @ngdoc directive
   * @name chartDirectives.tableChart
   * @restrict E
   * @description
   *
   * This directive displays a datafile in table form for the Dashboard. It is meant to be used for small amounts of
   * data that can easily fit onto a single screen - this differentiates this directive from the "spreadsheet"
   * directives which are suitable for large amounts of data and scrolling.
   *
   * The table can optionally be enriched with a "heatmap" style layout (in which cell backgrounds are colored based on
   * the value in them) and with sparklines that depict the development over a single line of the table. The directive
   * also offers a "time machine" slider similar to the other chart directives and it allows note taking by double-
   * clicking into an individual cell of the table.
   *
   * @scope
   * @param {string} contentFile          Gives the identifier (Title) of a datafile specified in the Dashboard -
   *                                      Datafiles list. The chart directive will retrieve this datafile and display it
   *                                      in table form.
   * @param {string=} contentTime         Specifies the (display) name of the column to be used for the time value that
   *                                      is used for the time slider. If left empty, the time slider will not be
   *                                      displayed.
   * @oaram {string}  contentFirstColumn  Specifies the (display) name of the column whose values will be displayed as
   *                                      leftmost "heading" in the table. Will not be included in the sparklines.
   * @oaram {string}  contentColumns      Specifies the (display) names of the additional columns to be displayed in
   *                                      the table.
   * @oaram {string}  columnColumn        If a datafile column is specified here, the table will use the entries in this
   *                                      column to construct the columns of the table to be displayed (transposing
   *                                      them).
   *                                      This model can be used if you want to dynamically change the columns to be
   *                                      displayed.
   * @oaram {string}  valueColumn         If `columnColumn` is given, you need to specify the `valueColumn` from the
   *                                      datafile, which will be used to fill the cells of the generated table.
   * @oaram {string=} heatmapFrom         If given, the table cells will be colored based on the values in the cells.
   *                                      This specifies the color that will be used for the lowest cell value.
   * @param {string=} heatmapTo           If given, the table cells will be colored based on the values in the cells.
   *                                      This specifies the color that will be used for the hightes cell value.
   * @param {boolean=} showSparklines     If set to `true`, a sparkline will be shown as right-most cell, displaying
   *                                      the development across the contentColumns.
   * @param {boolean=} showColumnSparklines  If set to `true`, a sparkline will be shown as last line, displaying
   *                                         the development across the lines for each contentColumn.
   * @param {string=} sparklinesUseGlobalMax If set to `true`, the sparklines will be scaled to the global maximum of
   *                                         the whole table. If set to `false`, a per-line maximum will be used.
   * @param {boolean} notesEnabled        If set to `true`, the chart will allow for notes to be recorded on individual
   *                                      data records (by double clicking on the element).
   * @param {Object=} notesOptions        If `notesEnabled` is set to `true`, an optional object can be passed here to overwrite options for the notes
   *                                      directive, e.g. in order to enable impact flags on these notes items etc.
   */

  .directive('tableChart', ['$timeout', '$filter', 'ngDialog', 'gettext', 'd3', 'miscServices', 'notesViewer', 'keywordsService', 'sidebarService', 'datafileReader',
    function($timeout, $filter, ngDialog, gettext, d3, miscServices, notesViewer, keywordsService, sidebarService, datafileReader) {

      return {
        restrict: 'E',

        scope: {
          contentFile: '=',
          contentTime: '=?',
          contentFirstColumn: '=',
          contentColumns: '=',
          columnColumn: '=?',
          valueColumn: '=?',
          heatmapFrom: '=?',
          heatmapTo: '=?',
          showSparklines: '=?',
          showColumnSparklines: '=?',
          sparklinesUseGlobalMax: '=?',
          notesEnabled: '=',
          notesOptions: '=?'
        },

        templateUrl: 'common/directives/chartTable.tmpl.html',

        require: '?^zoomKey',

        link: function(scope, element, attrs, zoomKeyCtrl) {
          scope.loading = false;
          scope.errorMessage = '';
          scope.content = [];
          scope.contentHash = '';
          scope.tableMin = 0;
          scope.tableMax = 0;
          scope.notesKeywords = [];
          scope.currTime = undefined;
          scope.availableTime = [];
          scope.notesKeywordsMap = {};
          scope.dynamicRows = {};
          scope.dynamicColumns = {};
          scope.visibleColumns = [];
          scope.columnsSpec = [];
          scope.animationSpeed = 0;
          scope.heatmapColor = function() { return '#ffffff'; }; // By default always return white as background color


          // Filter for the current time only
          scope.onlyCurrTime = function(value) {
            return (!scope.currTime || value[scope.contentTime].getTime() === scope.currTime.getTime());
          };

          // Filter to obtain the content columns for a given value (can be dynamic)
          var getContentColumns = function(d) {
            if(d === undefined) {
              // If d is undefined, we will return the columns for the current active timestamp
              return (scope.columnColumn ? scope.visibleColumns : scope.contentColumns);
            } else {
              // Otherwise we return the right columns for the given element
              return (scope.columnColumn ? scope.dynamicColumns[d[scope.contentTime].getTime()] : scope.contentColumns);
            }
          };


          // Sort toggle
          scope.sortColumn = null;
          scope.sortReverse = false;
          scope.toggleSort = function(col) {
            if(scope.sortColumn === col) {
              // Reverse sort order
              scope.sortReverse = !scope.sortReverse;
            } else {
              // Switch sort to this column
              scope.sortColumn = col;
              scope.sortReverse = false;
            }
          };

          scope.sortColumnGetter = function(i) {
            if((scope.columnFormats[scope.sortColumn] === d3.ourSettings.formats.number ||
              scope.columnFormats[scope.sortColumn] === d3.ourSettings.formats.percentage ||
              scope.columnFormats[scope.sortColumn] === d3.ourSettings.formats.integer) &&
              (isNaN(scope.content[i][scope.sortColumn]) || scope.content[i][scope.sortColumn] === '')) {
              // For numeric columns, we sort "empty" / "NaN" values as lowest possible values
              return Number.NEGATIVE_INFINITY;
            }
            return scope.content[i][scope.sortColumn];
          };

          // Helper function to update the visibleColumns
          var visibleRowsColumnsUpdater = function() {
            // Update rows
            scope.visibleRows = scope.dynamicRows[scope.currTime.getTime()];

            // Update columns
            if(scope.columnColumn) {
              scope.visibleColumns = scope.dynamicColumns[scope.currTime.getTime()];
            } else {
              scope.visibleColumns = scope.contentColumns;
            }
          };

          // Helper function to update the columnSparklinesContent
          var sparklinesUpdater = function() {
            scope.columnSparklinesContent = {};

            if(scope.visibleRows && scope.showColumnSparklines) {
              getContentColumns().forEach(function(col) {
                scope.columnSparklinesContent[col] =
                  $filter('orderBy')(
                    scope.visibleRows,
                    scope.sortColumnGetter,
                    scope.sortReverse
                  )
                  .map(function(row) { return scope.content[row][col]; });
              });
            }
          };

          // Function for in-chart note taking - is called when user double clicks inside the table body (tbody).
          // Uses ngDialog to show a note taking dialog and to display existing notes on this node.
          scope.addChartNote = function(evt) {
            // Is note taking enabled at all? If not return immediately.
            if(!scope.notesEnabled) {
              return;
            }

            // TODO: Remove debug code
            console.log('Received double click event:');
            console.log(evt);

            // Enable jqLite
            var el = angular.element(evt.target);

            // TODO: Remove debug code
            console.log('Element and its x-notes-keyword attr:');
            console.log(el);
            console.log(el.attr('x-notes-keyword'));


            // Okay, which table cell has been clicked?
            if(el.attr('x-notes-keyword')) {
              // Ok, the cell clicked has a notes keyword and we can open the dialog.
              ngDialog.open({
                template: 'common/directives/notesDialog.tmpl.html',
                className: 'ngdialog-theme-default',
                closeByDocument: false,
                data: {
                  'keywordID': el.attr('x-notes-keyword'),
                  'sourceName': el.attr('x-notes-column') + ' for ' + el.attr('x-notes-entry') + (scope.currTime ? ' (at ' + d3.ourSettings.formats.time(scope.currTime) + ')' : ''),
                  'notesOptions': scope.notesOptions,
                  'sourceLink': (zoomKeyCtrl ? zoomKeyCtrl.getZoomLink(d3.timeFormat('%Y-%m-%d')(scope.currTime)) : '')
                }
              });
            }
          };

          // Function for notes mouseover.
          scope.mouseOver = function(evt) {
            // Is note taking enabled at all? If not return immediately.
            if(!scope.notesEnabled) {
              return;
            }

            // TODO: Remove debug code
            console.log('Received mouseover event:');
            console.log(evt);

            // Enable jqLite
            var el = angular.element(evt.target);

            // Okay, does the table cell have a keyword and does it have notes?
            if(el.attr('x-notes-keyword') && el.hasClass('table-chart-has-notes')) {
              // Ok, the cell clicked has a notes keyword and we can show the sidebar.
              notesViewer.showNotes(el.attr('x-notes-keyword'));
            } else {
              // Okay, hide notes sidebar.
              notesViewer.showNotes();
            }
          };

          scope.mouseLeave = function() {
            // Hide notes sidebar if we leave the table.
            notesViewer.showNotes();
          };



          // Helper function that updates scope.content based on scope.notesKeywordsMap with 'hasNotes' property
          var updateHasNotes = function() {
            var allColumns = getContentColumns().concat(scope.contentFirstColumn);

            var i = 0;
            var hasNotesFinder = function(c) {
              scope.content[i]._hasNotes[c] = (scope.notesKeywordsMap[scope.content[i]._notesKeywords[c]] === true);
            };

            for(i = 0; i < scope.content.length; i++) {
              // Initialize hasNotes hash...
              // hasNotes is an hash with the elements being all columns for the given entry which have
              // a notes entry mapped to true.
              scope.content[i]._hasNotes = {};

              // Iterate over all columns...
              allColumns.forEach(hasNotesFinder);
            }
          };


          // The notesListener is called when new notes are received.
          var notesListener = function(allKeywords) {
            // We also need to re-draw the chart if we had notes before and don't have any now.
            var oldNotes = (scope.notesKeywords ? scope.notesKeywords.length : 0);

            // Set the notesKeywords to the received data...
            scope.notesKeywords = allKeywords;

            // Turns the notesKeywords into a keyword map...
            scope.notesKeywordsMap = {};
            scope.notesKeywords.forEach(function(k) {
              scope.notesKeywordsMap[k] = true;
            });

            // Update the hasNotes value on the content array...
            if (scope.content && scope.content.length > 0 && (oldNotes > 0 || scope.notesKeywords.length > 0)) {
              updateHasNotes();
            }
          };


          // The zoomListener is called when a hash URL is given by the user that matches
          // this chart. It extracts the date to show.
          var zoomListener = function(zoomHash) {
            if(!zoomHash.match(/^([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9])/)) {
              // Hash doesn't match the expected format
              return;
            }

            var zoomTo = zoomHash.substr(0,10);

            // Update selectedTimeIndex
            var timeIndex = scope.availableTime.findIndex(function(d) { return d3.ourSettings.formats.date(new Date(d)) === zoomTo; });
            if(timeIndex !== null) {
              // Time is valid
              scope.selectedTimeIndex = timeIndex;

              // Workaround for bug in ngModel and input type=range, see https://github.com/angular/angular.js/issues/6726
              $timeout(function() {
                element.find('input').val(scope.selectedTimeIndex);
              });

              // We jump to this chart
              return '';
            }
          };



          // Load data from file (using d3.js loading function)
          if(scope.contentFile) {
            // Show loading indicator
            scope.loading = true;

            // Read data file using data file reader (including caching)
            datafileReader.readDatafile(scope.contentFile)
              .then(
                function(res) {
                  var rows = res[0];
                  scope.columnsSpec = res[1];

                  // TODO - remove debug code
                  console.log('Obtained data for chart: ');
                  console.log(res);

                  // We set the columnKeywords and columnFormats...
                  scope.columnKeywords = {};
                  scope.columnFormats = {};
                  scope.columnColors = {};
                  scope.columnsSpec.forEach(function(colDef) {
                    scope.columnKeywords[colDef.display] = colDef.keyword;
                    scope.columnFormats[colDef.display] = d3.ourSettings.formats[colDef.format];
                    scope.columnColors[colDef.display] = colDef.color;
                  });

                  // We copy the content to our own content array (so that we do not modify the datafile used by
                  // other directives and we can also restrict ourselves to the required columns)
                  if(scope.columnColumn) {
                    // "Special" transposed mapping if a columnColumn is specified
                    var keyToLine = {};
                    scope.content = [];
                    rows.forEach(function(d) {
                      var thisLine;

                      // TODO: Remove debug code!
                      console.log('Transposing received row: ');
                      console.log(d);

                      if(d[scope.contentTime] && d[scope.columnColumn] !== undefined && d[scope.columnColumn] !== '') {
                        var lineKey = d[scope.contentTime] + '/' + keywordsService.getDataKey(undefined, d, scope.columnsSpec, undefined, [scope.contentTime, scope.columnColumn]);

                        if(keyToLine[lineKey] === undefined) {
                          // Entry for a new row
                          thisLine = {};
                          thisLine[scope.contentFirstColumn] = d[scope.contentFirstColumn];
                          thisLine[scope.contentTime] = d[scope.contentTime];

                          thisLine._notesKeywords = {};
                          thisLine._notesKeywords[scope.contentFirstColumn] = keywordsService.getDataKey(
                            scope.contentFile,
                            d,
                            scope.columnsSpec,
                            scope.contentFirstColumn,
                            scope.columnColumn
                          );

                          keyToLine[lineKey] = thisLine;
                          scope.content.push(thisLine);

                          // Do we need to initialize a new dynamicColumns entry for a new time entry?
                          if(scope.dynamicColumns[d[scope.contentTime].getTime()] === undefined)  {
                            scope.dynamicColumns[d[scope.contentTime].getTime()] = [];
                          }
                        } else {
                          // Entry for an existing row
                          thisLine = keyToLine[lineKey];
                        }

                        // Transposing
                        thisLine[d[scope.columnColumn]] = d[scope.valueColumn];

                        thisLine._notesKeywords[d[scope.columnColumn]] = keywordsService.getDataKey(
                          scope.contentFile,
                          d,
                          scope.columnsSpec,
                          scope.valueColumn
                        );

                        // New column for this timestamp?
                        if(scope.dynamicColumns[d[scope.contentTime].getTime()].indexOf(d[scope.columnColumn]) === -1) {
                          scope.dynamicColumns[d[scope.contentTime].getTime()].push(d[scope.columnColumn]);
                        }
                      }
                    });

                  } else {
                    // Regular mapping
                    scope.content = rows.map(
                      function(d) {
                        var ret = {};
                        ret[scope.contentFirstColumn] = d[scope.contentFirstColumn];
                        ret[scope.contentTime] = d[scope.contentTime];

                        ret._notesKeywords = {};
                        ret._notesKeywords[scope.contentFirstColumn] = keywordsService.getDataKey(
                          scope.contentFile,
                          d,
                          scope.columnsSpec,
                          scope.contentFirstColumn
                        );

                        scope.contentColumns.forEach(
                          function(c) {
                            ret[c] = d[c];
                            ret._notesKeywords[c] = keywordsService.getDataKey(
                              scope.contentFile,
                              d,
                              scope.columnsSpec,
                              c
                            );
                          }
                        );

                        return ret;
                      }
                    );
                  }

                  // TODO: Remove debug code!
                  console.log('TABLE: Completed mapping of content from datafile:');
                  console.log([scope.content, scope.contentColumns, scope.dynamicColumns]);

                  // Ok, set tableMin and tableMax.
                  // We need to find minimum and maximum values across all columns...
                  scope.tableMin = d3.min(scope.content, function(d) {
                    return d3.min(getContentColumns(d), function(c) {
                      return d[c];
                    });
                  });

                  scope.tableMax = d3.max(scope.content, function(d) {
                    return d3.max(getContentColumns(d), function(c) {
                      return d[c];
                    });
                  });

                  // Fill the dynamicRows hash that will be used when the time slider is changed.
                  scope.dynamicRows = {};
                  scope.content.forEach(function(d, i) {
                    if(scope.dynamicRows[d[scope.contentTime].getTime()]) {
                      scope.dynamicRows[d[scope.contentTime].getTime()].push(i);
                    } else {
                      scope.dynamicRows[d[scope.contentTime].getTime()] = [ i ];
                    }
                  });

                  // Do we already have notesKeywords? If yes, we need to update the hasNotes value on the content
                  // array...
                  if(scope.notesKeywords && scope.notesKeywords.length > 0) {
                    updateHasNotes();
                  }

                  // We need to update the sparklineContent...
                  if(scope.showSparklines) {
                    var i = 0;
                    var getContentColumn = function(c) {
                      return scope.content[i][c];
                    };

                    for(i = 0; i < scope.content.length; i++) {
                      scope.content[i]._sparklineContent = getContentColumns(scope.content[i]).map(getContentColumn);
                    }
                  }

                  // We need to update the heatmapColor scale...
                  if(scope.heatmapFrom && scope.heatmapTo) {
                    // Heatmap values specified - we use D3 to generate a linear color scale
                    scope.heatmapColor = d3.scaleLinear()
                      .domain([scope.tableMin, scope.tableMax])
                      .range([scope.heatmapFrom, scope.heatmapTo]);

                    // TODO: If heatmapFrom, heatmapTo, showSparklines are updated, we need to update these values
                  }


                  // What is the largest time value? This will be set as currTime
                  var availableTimes = {};
                  scope.content.forEach(function(d) {
                    availableTimes[d[scope.contentTime].getTime()] = true;
                  });

                  scope.availableTime = Object
                    .keys(availableTimes)
                    .map(function(time) { return parseInt(time, 10); })
                    .sort(function(a, b) { return a - b; });

                  scope.selectedTimeIndex = scope.availableTime.length-1;

                  // Register the zoomListener to jump to the timeIndex given in the hash
                  if(zoomKeyCtrl) {
                    zoomKeyCtrl.onZoom(zoomListener);
                  }

                  scope.currTime = new Date();
                  scope.currTime.setTime(scope.availableTime[scope.selectedTimeIndex]);

                  // We need to update the visibleRows and visibleColumns...
                  visibleRowsColumnsUpdater();

                  // We need to update the columnSparklinesContent...
                  sparklinesUpdater();

                  // Workaround for bug in ngModel and input type=range, see https://github.com/angular/angular.js/issues/6726
                  $timeout(function() {
                    element.find('input').val(scope.selectedTimeIndex);
                  });

                  scope.contentHash = miscServices.hashCode(JSON.stringify(scope.content));
                  scope.errorMessage = '';
                  scope.loading = false;
                },
                function(err) {
                  console.log('An error occurred loading the graph: ');
                  console.log(err);

                  scope.content = [];
                  scope.contentHash = '';
                  scope.errorMessage = gettext('An error occurred while loading this graph:') + ' ' + err;
                  scope.loading = false;
                }
              )

              // Load notes if notes are enabled
              .then(function() {
                if(scope.notesEnabled) {
                  notesViewer.loadNotes('data/' + scope.contentFile + '/', notesListener);
                }
              });
          }

          // Helper function to animate the date slider
          scope.startAnimation = function() {
            // If the animation is already running, we just increase the speed.
            if(scope.animationSpeed) {
              scope.animationSpeed++;
              return;
            }

            // Otherwise we need to set-up the animation first
            var nextStep;
            nextStep = function() {
              scope.selectedTimeIndex = Number(scope.selectedTimeIndex) || 0;
              scope.selectedTimeIndex = (scope.selectedTimeIndex === scope.availableTime.length-1 ? 0 : scope.selectedTimeIndex + 1);

              if(scope.animationSpeed) {
                $timeout(nextStep, 800/scope.animationSpeed);
              }
            };

            scope.animationSpeed = 1;
            nextStep();
          };

          scope.stopAnimation = function() {
            // stop the animation
            scope.animationSpeed = 0;
          };


          // Watch the selectedTimeIndex to update currTime
          scope.$watch('selectedTimeIndex', function(newVal, oldVal, scope) {
            if(newVal !== oldVal && scope.availableTime[newVal]) {
              // Update currTime
              scope.currTime.setTime(scope.availableTime[newVal]);

              // We need to update the visibleRows and visibleColumns...
              visibleRowsColumnsUpdater();

              // We need to update the columnSparklinesContent...
              sparklinesUpdater();
            }
          });

          // We update the columnSparklinesContent when the sorting changes.
          scope.$watch('sortColumn', sparklinesUpdater);
          scope.$watch('sortReverse', sparklinesUpdater);

        }
      };
    }
  ])



  /**
   * @ngdoc directive
   * @name chartDirectives.portfolioChart
   * @restrict E
   * @description
   *
   * This directive displays a specific chart type which we call "Portfolio Chart". The idea is to represent a portfolio
   * of elements (e.g. organizational units) as rectangles along the x-axis with their importance (or size) being
   * represented by their width on the x-axis and their value for the indicator in question being represented by their
   * height along the y-axis. The size of the square thus represents the combined effect of importance and indicator
   * value.
   *
   * Optionally, the chart supports moving through time using a time slider and of displaying different groups of
   * elements in different colours (with an appropriate legend being displayed). A mouseover tooltip displays the
   * exact x and y values and an optional tooltip text to be provided in the datafile.
   *
   *
   * @scope
   * @param {number}  width               The width of the chart to be displayed in pixels.
   * @param {number}  height              The height of the chart to be displayed in pixels.
   * @param {number=} legendWidth         The width of the legend to be displayed in pixels. If empty, a default value will
   *                                      be calculated.
   * @param {string}  contentFile         Gives the identifier (Title) of a datafile specified in the Dashboard - Datafiles list.
   *                                      The chart directive will retrieve this datafile and display it in chart form.
   * @param {string=} contentTime         Specifies the (display) name of the column to be used for the time value that is used
   *                                      for the time slider. If left empty, the time slider will not be displayed.
   * @oaram {string}  contentX            Specifies the (display) name of the column whose values will determine the width
   *                                      of the portfolio item along the x axis.
   * @param {string=} contentDivide       Specifies the (display) name of a column which will be used to separate entries along
   *                                      the x axis. If given, divider lines will be displayed where (along the x axis in
   *                                      the direction of the data elements) the text in the `contentDivide` column changes.
   *                                      If there is enough space, the content of the `contentDivide` column will be
   *                                      displayed between these dividers.
   * @oaram {string}  contentY            Specifies the (display) name of the column whose values will determine the height
   *                                      of the portfolio item along the y axis.
   * @oaram {string=} contentGroup        Specifies the (display) name of the column whose values will determine the group
   *                                      a portfolio item belongs to (determines the colour and will be shown on the
   *                                      legend). If omitted, all items will be shown in the same colour and no legend will
   *                                      be displayed.
   * @oaram {string=} contentTitle        Specifies the (display) name of the column whose values will determine the label
   *                                      for this data element, which will be displayed when adding or viewing notes
   *                                      attached to this data element. If omitted, no title will be shown.
   * @oaram {string=} tooltip             Text that will be shown in the tooltip which is displayed when the user moves the
   *                                      cursor over one of the scatterplot items. With `{{columname}}` the value of the
   *                                      column `columname` for the given item can be displayed. If omitted, no description
   *                                      will be shown.
   * @param {Array=}  lineColumns         If specified, the columns given in this array will be overlayed as lines over the
   *                                      portfolio chart. Can be used e.g. to create benchmark lines.
   * @param {Array=}  groupCustomColors   If specified, this needs to be an array of objects each with a property `group` and
                                          `color`. For the given `group` entries that match an entry in the `contentGroup`
                                          column, their default color will be overwritten with the color specification (e.g.
                                          an RGB value given as `#aabbcc`) given in their `color` property.
   * @param {string}  labelX              The label to be displayed on the x axis of the chart.
   * @param {string}  labelY              The label to be displayed on the y axis of the chart.
   * @param {boolean} notesEnabled        If set to `true`, the chart will allow for notes to be recorded on individual data
   *                                      records (by double clicking on the element).
   * @param {Object=} notesOptions        If `notesEnabled` is set to `true`, an optional object can be passed here to overwrite
   *                                      options for the notes directive, e.g. in order to enable impact flags on these
   *                                      notes items etc.
   *
   */


  .directive('portfolioChart', ['$timeout', 'ngDialog', 'gettext', 'd3', 'miscServices', 'notesViewer', 'keywordsService', 'sidebarService', 'datafileReader',
    function($timeout, ngDialog, gettext, d3, miscServices, notesViewer, keywordsService, sidebarService, datafileReader) {
      // Draw the chart
      var draw = function(svg, svgBrush, scope) {
        // fixed margin (if contentGroup is set, use larger margin on right due to legend; always use larger margin
        //               on bottom due to axis)
        var margin_left = Math.round(scope.width * 0.05);
        var margin_right = Math.round(scope.width * 0.05 + (scope.contentGroup ? scope.legendWidth : 0));
        var margin_top = Math.round(scope.height * 0.05);
        var margin_bottom = Math.round(scope.height * 0.05) + 100; /* for axis */


        // Create list of all available groups in the content
        var availableGroups = [];
        if(scope.contentGroup) {
          var tmpGroupHash = {};

          scope.content.forEach(function(d) {
            tmpGroupHash[d[scope.contentGroup]] = true;
          });

          availableGroups = Object.keys(tmpGroupHash);
        }

        // Do we have existing activeGroups? Then use those, otherwise use all available contentGroups.
        if(!scope.activeGroups || !scope.activeGroups.length) {
          scope.activeGroups = availableGroups.slice();
        }

        // Are line columns configured for this portfolio chart?
        if(Array.isArray(scope.lineColumns) && !scope.activeLines) {
          scope.activeLines = scope.lineColumns.slice();
        } else if(!scope.activeLines) {
          scope.activeLines = [];
        }

        // Legend entries are composed of the groups and lines
        var legendEntries = [];
        availableGroups.forEach(function(d) {
          legendEntries.push({
            type: 'group',
            label: d
          });
        });

        if(Array.isArray(scope.lineColumns)) {
          scope.lineColumns.forEach(function(d) {
            legendEntries.push({
              type: 'line',
              label: d
            });
          });
        }


        // Update height and width
        svg
          .attr('width', scope.width)
          .attr('height', scope.height);


        // Update clipping path
        svg
          .select('.clip-rect')
          .attr('x', margin_left)
          .attr('y', margin_top)
          .attr('width', scope.width - margin_left - margin_right)
          .attr('height', scope.height - margin_top - margin_bottom);


        // Define xScale
        var xScale = d3.scaleLinear()
          .range([ margin_left, scope.width - margin_right ]);

        // Move x-axis
        svg.select('.x-axis')
          .attr("transform", "translate(0, " + (scope.height-margin_bottom) + ")");

        // Print x-axis label
        svg.select('.x-axis-label')
          .text(scope.labelX)
          .attr('x', Math.floor(margin_left + (scope.width - margin_left - margin_right) / 2))
          .attr('y', Math.floor(scope.height - margin_bottom/2));


        // Define yScale
        var yScale = d3.scaleLinear()
          .range([ scope.height-margin_bottom, margin_top ]);

        // Move y axis
        svg.select('.y-axis')
          .attr("transform", "translate(" + margin_left + ")");

        // Print y-axis label
        svg.select('.y-axis-label')
          .text(scope.labelY)
          .attr('transform', 'rotate(-90)')
          .attr('x', -1*Math.floor(margin_top + (scope.height - margin_top - margin_bottom)/2))
          .attr('y', Math.floor(margin_left / 3));


        // Define chart colors
        var chartColors;
        if(scope.contentGroup) {
	  if(scope.groupCustomColors) {
	    var groupCustomColorsHash = {};
            scope.groupCustomColors.forEach(function(custom) {
	      groupCustomColorsHash[custom.group] = custom.color;
	    });

	    var chartColorsDefault = d3.scaleOrdinal(d3.ourSettings.chartColors).domain(legendEntries);
	    
	    chartColors = function(group) {
	      if(groupCustomColorsHash[group] !== undefined) {
		return groupCustomColorsHash[group];
	      } else {
		return chartColorsDefault(group);
	      }
	    };
	  } else {
            chartColors = d3.scaleOrdinal(d3.ourSettings.chartColors).domain(legendEntries);
	  }
        } else {
          chartColors = d3.scaleOrdinal(d3.ourSettings.chartColors).domain([ 'MAIN' ].concat(legendEntries));
        }


        // Draw chart and (re-)set xScale based on available data, unless we already have a brushExtent
        if(!scope.brushExtent) {
          drawBoxes(svg, scope, availableGroups, xScale, yScale, chartColors, true);
        } else {
          // Performance improvement: If we have a brushExtent, we do not need to draw
          // anything, as the brush move below will trigger a re-paint anyways.
          // xScale.domain(scope.brushExtent);
          // drawBoxes(svg, scope, availableGroups, xScale, yScale, chartColors, false);
        }


        // Add brush

        // Update width and height
        svgBrush
          .attr('width', scope.width)
          .attr('height', 0.1*scope.height);  // 10% of full graph height

        // xScale for the brush
        var xScaleBrush = d3.scaleTime()
          .range(xScale.range());

        // yScale for the brush
        var yScaleBrush = d3.scaleLinear()
          .range([Math.floor(0.1*scope.height), 0]);

        // Draw data for the brush
        drawBoxes(svgBrush, scope, availableGroups, xScaleBrush, yScaleBrush, chartColors, true);

        // Add brush interactivity
        var brush = d3.brushX();
        brush.extent([
           [xScaleBrush.range()[0], yScaleBrush.range()[1]],
           [xScaleBrush.range()[1], yScaleBrush.range()[0]]
          ]);


        var brushContainer = svgBrush.select('.brush').call(brush);

        brush.on('end', function() {

          // Use the current extent
          if(!d3.event.selection) {
            xScale.domain(xScaleBrush.domain()); // reset original domain
            scope.brushExtent = undefined;
          } else {
            xScale.domain(d3.event.selection.map(xScaleBrush.invert));
            scope.brushExtent = d3.event.selection.map(xScaleBrush.invert);
          }

          // Redraw the chart - but leave xScale alone
          drawBoxes(svg, scope, availableGroups, xScale, yScale, chartColors, false);
        });


        // Existing brush extent?
        if(scope.brushExtent) {
          // TODO: Remove debug code!
          console.log('Moving brush to existing brushExtent...');
          console.log([scope.brushExtent, scope.brushExtent.map(xScaleBrush)]);

          // Existing brush extent?
          brushContainer.call(brush.move, scope.brushExtent.map(xScaleBrush));
        }



        // Do we need a legend? (We only show a legend if we have multiple groups in our data set or line columns are specified.)
        var legend = svg.select('.legend');
        if(legendEntries.length) {
          legend.classed('hidden', false);

          var legends = legend.selectAll('g').data(legendEntries, function(d) { return d.type + '-' + d.label; });

          var enteredLegends = legends
            .enter()
            .append('g')
            .attr('class', function(d, i) { return 'legend-' + i + ' legend-' + d.type; })
            .attr("transform", function(d, i) { return "translate(0, " + (margin_top + i*Math.min((scope.height-margin_top-margin_bottom)/legendEntries.length, 30)) + ")"; });

          enteredLegends.append('rect')
            .attr('x', scope.width - scope.legendWidth)
            .attr('y', function(d) {
              return (d.type === 'line' ? Math.min((scope.height-margin_top-margin_bottom)/legendEntries.length*0.2, 6) : 0);
            })
            .attr('width', Math.min(60, scope.legendWidth * 0.3))
            .attr('height', function(d) {
              if(d.type === 'line') {
                return Math.min((scope.height-margin_top-margin_bottom)/legendEntries.length*0.2, 6);
              } else {
                return Math.min((scope.height-margin_top-margin_bottom)/legendEntries.length*0.6, 18);
              }
            })
            .style('fill', function(d) { return chartColors(d.label); });

          enteredLegends.append('text')
            .attr('x', scope.width - scope.legendWidth + Math.min(70, scope.legendWidth * 0.35))
            .attr('y', Math.min((scope.height-margin_top-margin_bottom)/legendEntries.length, 30)*0.5)
            .style('fill', 'black')
            .text(function(d) { return d.label; });


          enteredLegends
            .merge(legends)

            // Fade out inactive columns
            .classed('faded', function (d) {
              return (d.type === 'group' && scope.activeGroups.indexOf(d.label) === -1) ||
                (d.type === 'line' && scope.activeLines.indexOf(d.label) === -1);
            })

            // Interactivity for legend - click shows/hides line
            .on('click', function (d) {
              var setFaded = !d3.select(this).classed('faded');

              d3.select(this).classed('faded', setFaded);

              // Update activeColumns (preserve order of columns!)
              var arrayToChange = (d.type === 'line' ? scope.activeLines : scope.activeGroups);

              if (setFaded) {
                // Remove from active groups
                arrayToChange.splice(arrayToChange.indexOf(d.label), 1);
              } else {
                // Add back at the right place
                var j2 = 0;
                for (var j1 = 0; j1 < legendEntries.length; j1++) {
                  if (arrayToChange[j2] === legendEntries[j1].label && d.type === legendEntries[j1].type) {
                    j2++;
                  } // They match, so increment
                  else if (legendEntries[j1] === d) {
                    arrayToChange.splice(j2, 0, d.label);
                  } // Add here!
                }
              }

              // Redraw chart and axes using new activeGroups
              drawBoxes(svg, scope, availableGroups, xScale, yScale, chartColors, false);
              drawBoxes(svgBrush, scope, availableGroups, xScaleBrush, yScaleBrush, chartColors, false);
            })

            // Interactivity for legend - Doubleclick shows everything but this one
            .on('dblclick', function (d) {
              // Prevent other action
              d3.event.preventDefault();

              svg.selectAll('.legend-' + d.type).classed('faded', true);
              d3.select(this).classed('faded', false);

              // The relevant array is now just this one column
              if(d.type === 'line') {
                scope.activeLines = [ d.label ];
              } else {
                scope.activeGroups = [ d.label ];
              }

              // Redraw chart and axes using new activeGroups
              drawBoxes(svg, scope, availableGroups, xScale, yScale, chartColors, false);
              drawBoxes(svgBrush, scope, availableGroups, xScaleBrush, yScaleBrush, chartColors, false);
            })

            // Interactivity for legend - Rightclick shows everything
            .on('contextmenu', function(d) {
              svg.selectAll('.legend-' + d.type).classed('faded', false);

              // The relevant array is now back to the original
              if(d.type === 'line') {
                scope.activeLines = scope.lineColumns.slice(0);
              } else {
                scope.activeGroups = availableGroups.slice(0);
              }

              // Redraw chart and axes using new activeGroups
              drawBoxes(svg, scope, availableGroups, xScale, yScale, chartColors, false);
              drawBoxes(svgBrush, scope, availableGroups, xScaleBrush, yScaleBrush, chartColors, false);

              // Stop propagating of event (do not show browser context menu)
              d3.event.preventDefault();
            });

        } else {
          // We hide the legend because we do not need it.
          legend.classed('hidden', true);
        }


        // Add mouseover
        var mouseoverShown = false;

        var mouseoverGroup = svg.select('.portfolio-chart-mouseover');

        mouseoverGroup.select('.portfolio-chart-mouseover-box')
          .attr('width', Math.floor(0.3 * scope.width))
          .attr('height', Math.floor(0.15 * scope.height));

        svg.on('mousemove', function() {
          var pos = d3.mouse(this);

          // Only act on rect elements within the drawing area
          if(pos[0] < margin_left || pos[0] > scope.width - margin_right || pos[1] < margin_top || pos[1] > scope.height - margin_bottom || d3.event.target.nodeName.toUpperCase() !== 'RECT') {
            if(mouseoverShown) {
              mouseoverGroup.classed('hidden', true);
              mouseoverShown = false;
            }

            scope.$apply(function() {
              scope.activeElement = undefined;
              notesViewer.showNotes();
            });
            return;
          }

          var d = d3.select(d3.event.target).datum();

          // We store the currently active data point (which data set, time point and key) in the activeElement
          // variable, which is used when displaying the note taking dialog (after dblclick).
          var myKeyword = keywordsService.getDataKey(scope.contentFile, d, scope.columnsSpec, scope.contentY);
          var shortKeyword = keywordsService.getDataKey(undefined, d, scope.columnsSpec);
          scope.activeElement = {
            'keyword': myKeyword,
            'shortKeyword': shortKeyword,
            'description': (scope.contentTitle ? d[scope.contentTitle] : ''),
            'time': scope.currTime
          };


          mouseoverGroup
            .attr('transform', 'translate(' + (xScale(d._portfolioStartX + d[scope.contentX]/2)) + ', ' + (yScale(d[scope.contentY]) + 10) + ')')
            .classed('hidden', false);

          var mouseoverLines = scope.tooltip
            .replace(/{{([^}]+)}}/g, function(match, column) {
              return (scope.columnFormats[column] ? scope.columnFormats[column](d[column]) : match);
            })
            .split('\n');

          // TODO: Remove debug code
          console.log('Mapped mouseover tooltip:');
          console.log([scope.tooltip, mouseoverLines]);

          var line;
          for(line = 1;line <= 4;line++) {
            mouseoverGroup
              .select('.portfolio-chart-mouseover-line-' + line)
              .text(mouseoverLines[line-1]);
          }

          mouseoverShown = true;

          // Does a note exist for this data point? If yes, we show the sidebar, otherwise we close it.
          scope.$apply(function() {
            notesViewer.showNotes(myKeyword);
          });
        });


      };



      // Helper function to update the boxes when stuff is getting brushed or faded in/out or the time is changed
      var drawBoxes = function(svg, scope, availableGroups, xScale, yScale, chartColors, setXDomain) {

        // Time-specific content
        var timedContent = scope.content.filter(function(d) { return d[scope.contentTime].getTime() === scope.currTime.getTime(); });

        // Scale x-axis
        if(setXDomain) {
          var xMax = d3.max(timedContent, function(d) { return d._portfolioStartX + d[scope.contentX]; });
          xScale.domain([0, xMax]);

          // TODO: Remove debug code!
          console.log('Scaling xAxis:');
          console.log(timedContent);
          console.log(xMax);
        }

        // Filtered content (filtered based on active groups and on x axis extent)
        var filteredContent;
        if(availableGroups.length && availableGroups.length > scope.activeGroups.length) {
          filteredContent = timedContent.filter(function(d) {
            return scope.activeGroups.indexOf(d[scope.contentGroup]) !== -1 &&    // Is in active group...
              d._portfolioStartX  <= xScale.domain()[1] &&                        // ... and bar starts prior to end of view extent ...
              (d._portfolioStartX  + d[scope.contentX]) >= xScale.domain()[0];    // ... and ends after beginning of view extent.
          });
        } else {
          filteredContent = timedContent.filter(function(d) {
            return d._portfolioStartX  <= xScale.domain()[1] &&                   // Bar starts prior to end of view extent ...
              (d._portfolioStartX  + d[scope.contentX]) >= xScale.domain()[0];    // ... and ends after beginning of view extent.
          });
        }

        // Scale y-axis
        var yScaleDomain;
        if(scope.activeLines.length) {
          yScaleDomain = [
            d3.min([0, d3.min(filteredContent, function(d) { return d3.min([ d[scope.contentY] ].concat(scope.activeLines.map(function(lineKey) { return d[lineKey]; }))); })]),
            d3.max(filteredContent, function(d) { return d3.max([ d[scope.contentY] ].concat(scope.activeLines.map(function(lineKey) { return d[lineKey]; }))); })
          ];
        } else {
          yScaleDomain = [
            d3.min([0, d3.min(filteredContent, function(d) { return d[scope.contentY]; })]),
            d3.max(filteredContent, function(d) { return d[scope.contentY]; })
          ];
        }

        // Capture special case that filteredContent is empty!
        if(yScaleDomain[1] === undefined) {
          yScaleDomain[1] = 0;
        }

        yScale.domain(yScaleDomain);


        // TODO: Remove debug code!
        console.log('Scaling yAxis:');
        console.log(filteredContent);
        console.log([0, d3.max(filteredContent, function(d) { return d[scope.contentY]; })]);

        // Define x-axis
        var xAxis = d3.axisBottom(xScale)
          .tickFormat(scope.columnFormats[scope.contentX] || d3.format('.2f'));

        // Draw x-axis (rotate labels)
        var xAxisContainer = svg.select('.x-axis');

        xAxisContainer
          .transition()
          .duration(250)
          .call(xAxis);

        xAxisContainer
          .selectAll('text')
          .style('text-anchor', 'end')
          .attr('dx', '-1em')
          .attr('dy', '-.55em')
          .attr('transform', 'rotate(-90)');


        // Define y-axis
        var yAxis = d3.axisLeft(yScale)
          .tickFormat(scope.columnFormats[scope.contentY] || d3.format('.2f'));

        // Draw y axis
        svg.select('.y-axis')
          .transition()
          .duration(250)
          .call(yAxis);


        // Get relevant notes keywords to identify notes to highlight
        var relevantKeys = {};
        scope.notesKeywords.forEach(function(k) {
          relevantKeys[k] = true;
        });


        // TODO: Remove debug code
        console.log('Drawing data for portfolio chart:');
        console.log(timedContent);

        // Draw rectangles
        var rects = svg.select('.data').selectAll('rect')
          .data(timedContent, function(d) { return keywordsService.getDataKey(undefined, d, scope.columnsSpec, undefined, scope.contentTime); });

        rects.exit()
          .remove();

        rects.enter()
            .append('rect')
            .attr('class', 'portfolio-chart-element')
            .attr('x',      function(d) { return xScale(d._portfolioStartX); })
            .attr('width',  function(d) { return xScale(d._portfolioStartX + d[scope.contentX]) - xScale(d._portfolioStartX); })
            .attr('y',      yScale(0))
            .attr('height', 0)

          .merge(rects)
            .attr('fill', (availableGroups.length ? function(d) { return chartColors(d[scope.contentGroup]); } : chartColors('MAIN')))
            .classed('portfolio-chart-faded-out', (availableGroups.length && availableGroups.length > scope.activeGroups.length ? function(d) { return scope.activeGroups.indexOf(d[scope.contentGroup]) === -1; } : false))
            .classed('portfolio-chart-has-notes', function(d) { return (relevantKeys[keywordsService.getDataKey(scope.contentFile, d, scope.columnsSpec, scope.contentY)] === true); })

            .transition()
            .duration(250)
            .attr('x',      function(d) { return xScale(d._portfolioStartX); })
            .attr('y',      function(d) { return yScale(d[scope.contentY] >= 0 ? d[scope.contentY] : 0); })
            .attr('width',  function(d) { return xScale(d._portfolioStartX + d[scope.contentX]) - xScale(d._portfolioStartX); })
            .attr('height', function(d) { return Math.abs(yScale(0) - yScale(d[scope.contentY])); });

        // Do we need to draw line chart overlays on top of our portfolio chart?
        var lines;
        if(Array.isArray(scope.lineColumns) && scope.lineColumns.length >= 1) {
          if(!rects.enter().empty()) {
            // We have added new rectangles to the portfolio chart. This means we need to completely redraw
            // the lines as otherwise they get covered by these new rectangles.
            svg.select('.data').selectAll('.chartLine')
              .remove();
          }

          lines = svg.select('.data').selectAll('.chartLine').data(scope.lineColumns);

          lines
            .exit()
            .remove();

          lines
            .enter()
            .append('path')
            .attr('class', function(k, i) { return 'portfolio-chart-line chartLine chartPath-' + i; })

            .merge(lines)
            .classed('faded', function(k) { return (scope.activeLines.indexOf(k) === -1); })
            .style('stroke', function(k) { return (scope.columnColors[k] ? scope.columnColors[k] : chartColors(k)); })

            .transition()
            .duration(250)
            .attr('d', function(k) {
              var line = d3.line()
                .curve(d3.curveStepBefore)
                .x(function(d, i) { return xScale(d._portfolioStartX + (i >= 1 ? d[scope.contentX] : 0)); })
                .y(function(d) { return yScale(d[k]); });

              return line(timedContent);
            });
        }


        // Do we need to draw divider lines?
        if(scope.contentDivide !== undefined) {
          var divisionsData = [];

          // We iterate over the content and find the dividing entries
          var currDivision = '';
          timedContent.forEach(function(d) {
            if(currDivision !== d[scope.contentDivide]) {
              divisionsData.push({
                start: d._portfolioStartX,
                label: d[scope.contentDivide]
              });
              currDivision = d[scope.contentDivide];
            }

            divisionsData[divisionsData.length-1].end = d._portfolioStartX + d[scope.contentX];
          });

          // Now we can display these divisions using D3

          // If we have added rects or lines, we need to redraw all (as they might have been overlaid)
          if(!rects.enter().empty() || (lines && !lines.enter().empty())) {
            // We have added new rectangles to the portfolio chart. This means we need to completely redraw
            // everything as otherwise they get covered by these new rectangles.
            svg.select('.data').selectAll('.portfolio-division-line')
              .remove();

            svg.select('.data').selectAll('.portfolio-division-text')
              .remove();
          }

          // Lines first
          var divisionLines = svg.select('.data').selectAll('.portfolio-division-line')
            .data(divisionsData, function(d) { return d.label; });

          divisionLines.exit()
            .remove();

          divisionLines.enter()
            .append('line')
            .attr('class', 'portfolio-division-line')
            .attr('x1', function(d) { return xScale(d.end); })
            .attr('x2', function(d) { return xScale(d.end); })

            .merge(divisionLines)
            .attr('y1', yScale.range()[0])
            .attr('y2', yScale.range()[1])

            .transition()
            .duration(250)
            .attr('x1', function(d) { return xScale(d.end); })
            .attr('x2', function(d) { return xScale(d.end); });

          // Texts second
          var divisionTexts = svg.select('.data').selectAll('.portfolio-division-text')
            .data(divisionsData, function(d) { return d.label; });

          divisionTexts.exit()
            .remove();

          divisionTexts.enter()
            .append('text')
            .attr('class', 'portfolio-division-text')
            .text(function(d) { return d.label; })
            .attr('x', function(d) { return Math.round((xScale(d.end) + xScale(d.start))/2); })

            .merge(divisionTexts)
            .classed('hidden', function(d) { return (xScale(d.end) - xScale(d.start)) < 50; })
            .attr('y', Math.round(yScale.range()[0] + 0.8*(yScale.range()[1]-yScale.range()[0])))

            .transition()
            .duration(250)
            .attr('x', function(d) { return Math.round((xScale(d.end) + xScale(d.start))/2); });
        }

      };


      // Function for in-chart note taking - is called when user double clicks inside of the
      // clipping rectangle.
      // Uses ngDialog to show a note taking dialog and to display existing notes on this node.
      var addChartNote = function(myElement, notesOptions, zoomKeyCtrl) {
        // Open dialog with the notes view if we are double-clicking inside of the data area
        if(myElement) {
          ngDialog.open({
            template: 'common/directives/notesDialog.tmpl.html',
            className: 'ngdialog-theme-default',
            closeByDocument: false,
            data: {
              'keywordID': myElement.keyword,
              'sourceName': myElement.description + ' at ' + d3.timeFormat('%Y-%m-%d')(myElement.time),
              'notesOptions': notesOptions,
              'sourceLink': (zoomKeyCtrl ? zoomKeyCtrl.getZoomLink(myElement.shortKeyword.substr(1)) : '')
            }
          });
        }
      };



      return {
        restrict: 'E',

        scope: {
          width: '=',
          height: '=',
          legendWidth: '=?',
          contentFile: '=',
          contentTime: '=',
          contentDivide: '=?',
          contentX: '=',
          contentY: '=',
          contentGroup: '=?',
          contentTitle: '=?',
          tooltip: '=?',
          lineColumns: '=?',
          labelX: '=',
          labelY: '=',
	  groupCustomColors: '=?',
          notesEnabled: '=',
          notesOptions: '=?'
        },

        require: '?^zoomKey',

        templateUrl: 'common/directives/chartPortfolio.tmpl.html',

        link: function(scope, element, attrs, zoomKeyCtrl) {
          var svg, svgBrush, loadingIndicator;
          scope.loading = false;
          scope.errorMessage = '';
          scope.content = [];
          scope.contentHash = '';
          scope.notesKeywords = [];
          scope.currTime = undefined;
          scope.availableTime = [];
          scope.brushExtent = undefined;
          scope.columnFormats = {};
          scope.columnColors = {};
          scope.columnsSpec = [];
          scope.animationSpeed = 0;

          if(scope.contentGroup && !scope.legendWidth) {
            scope.legendWidth = Math.round(0.25*scope.width);
          }

          // Create a SVG root element
          svg = d3.select(element[0]).append('svg');

          // Add clipping path
          svg.append('clipPath')
            .attr('id', 'clip-path-' + scope.$id)
            .append('rect')
            .attr('class', 'clip-rect');

          // Add containers for graph, axes, axes labels and legend
          svg.append('g').attr('class', 'data')
            .attr('clip-path', 'url(#clip-path-' + scope.$id + ')');

          svg.append('g').attr('class', 'x-axis axis');
          svg.append('text').attr('class', 'x-axis-label axis-label');

          svg.append('g').attr('class', 'y-axis axis');
          svg.append('text').attr('class', 'y-axis-label axis-label');

          svg.append('g').attr('class', 'legend');


          // Add mouseover box
          var mouseoverGroup = svg.append('g').attr('class', 'portfolio-chart-mouseover hidden');

          mouseoverGroup.append('rect').attr('class', 'portfolio-chart-mouseover-box');

          var mouseoverTextElement = mouseoverGroup.append('text').attr('class', 'portfolio-chart-mouseover-text');

          mouseoverTextElement
            .append('tspan')
            .attr('class', 'portfolio-chart-mouseover-line-1')
            .attr('x', 0)
            .attr('dy', '1.2em');
          mouseoverTextElement
            .append('tspan')
            .attr('class', 'portfolio-chart-mouseover-line-2')
            .attr('x', 0)
            .attr('dy', '1.2em');
          mouseoverTextElement
            .append('tspan')
            .attr('class', 'portfolio-chart-mouseover-line-3')
            .attr('x', 0)
            .attr('dy', '1.2em');
          mouseoverTextElement
            .append('tspan')
            .attr('class', 'portfolio-chart-mouseover-line-4')
            .attr('x', 0)
            .attr('dy', '1.2em');


          // Create root element for brush
          svgBrush = d3.select(element[0]).append('svg').attr('class', 'brush-svg');
          svgBrush.append('g').attr('class', 'data');
          svgBrush.append('g').attr('class', 'brush');


          // Is the note taking functionality enabled?
          // If yes, enable double click event.
          if(scope.notesEnabled) {
            svg.on('dblclick', function() {
              // Prevent other action
              d3.event.preventDefault();

              // Show notes dialog
              return addChartNote(scope.activeElement, scope.notesOptions, zoomKeyCtrl);
            });
          }


          // The notesListener is called when new notes are received.
          var notesListener = function(allKeywords) {
            // We also need to re-draw the chart if we had notes before and don't have any now.
            var oldNotes = (scope.notesKeywords ? scope.notesKeywords.length : 0);

            // Set the notesKeywords to the received data...
            scope.notesKeywords = allKeywords;

            // (Re-)draw the chart (if it is already loaded)
            if (scope.content && scope.content.length > 0 && (oldNotes > 0 || scope.notesKeywords.length > 0)) {
              draw(svg, svgBrush, scope);
            }
          };


          // The zoomListener is called when a hash URL is given by the user that matches
          // this chart. It extracts the date to show.
          var zoomListener = function(zoomHash) {
            var zoomMatch = zoomHash.match(/^([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9])(\/.+?)?(\/[0-9]+)?$/);
            if(!zoomMatch) {
              // Hash doesn't match the expected format
              return;
            }

            var zoomToTime = zoomHash.substr(0,10);


            // Update selectedTimeIndex
            var timeIndex = scope.availableTime.findIndex(function(d) { return d3.ourSettings.formats.date(new Date(d)) === zoomToTime; });
            if(timeIndex !== null) {
              // Time is valid
              scope.selectedTimeIndex = timeIndex;
              scope.currTime = new Date(scope.availableTime[scope.selectedTimeIndex]);

              // Workaround for bug in ngModel and input type=range, see https://github.com/angular/angular.js/issues/6726
              $timeout(function() {
                element.find('input').val(scope.selectedTimeIndex);
              });
            }

            if(zoomMatch[2]) {
              // We need to brush, too... => find the right element.
              var searchKey = '/' + zoomMatch[1] + zoomMatch[2];

              var contentItem = scope.content.find(function(d) {
                // TODO: Remove debug code
                console.log('Comparing searchKey "' + searchKey + '" to dataKey "' + keywordsService.getDataKey(undefined, d, scope.columnsSpec) + '".');
                console.log(d);

                return (searchKey === keywordsService.getDataKey(undefined, d, scope.columnsSpec));
              });

              if(contentItem) {
                // We have found the item to zoom to, so we can set the brush extent.
                scope.brushExtent = [
                  Math.max(0, contentItem._portfolioStartX - 2*contentItem[scope.contentX]),
                  contentItem._portfolioStartX + 3*contentItem[scope.contentX]
                ];

                // We need to re-draw the chart
                draw(svg, svgBrush, scope);
              }
            }

            // We jump to this chart
            return '';
          };



          // Load data from file (using d3.js loading function)
          if(scope.contentFile) {
            scope.loading = true;

            // Hide SVG
            svg.classed('hidden', true);

            // Create loading indicator
            loadingIndicator = d3.select(element[0]).append('div')
              .style('height', scope.height + 'px')
              .style('width', scope.width + 'px')
              .classed('loadingIndicator', true);

            // Read data file using data file reader (including caching)
            datafileReader.readDatafile(scope.contentFile)
              .then(
                function(res) {
                  // Set content to be displayed
                  scope.content = res[0];
                  scope.columnsSpec = res[1];

                  // TODO - remove debug code
                  console.log('Obtained data for chart: ');
                  console.log(res);

                  // We create the content array by iterating over the rows returned and setting the
                  // startX value. We also define all available times.
                  var currStartX = {};
                  var availableTimes = {};
                  scope.content.forEach(function(d) {
                    // Add to availableTimes
                    availableTimes[d[scope.contentTime].getTime()] = true;

                    // Add startX
                    d._portfolioStartX = (currStartX[d[scope.contentTime]] ? currStartX[d[scope.contentTime]] : 0);

                    // Increment startX
                    currStartX[d[scope.contentTime]] = d._portfolioStartX + d[scope.contentX];
                  });

                  // Set columnFormats for all relevant columns
                  scope.columnFormats = {};
                  scope.columnColors = {};
                  scope.columnsSpec.forEach(function(colDef) {
                    scope.columnFormats[colDef.display] = d3.ourSettings.formats[colDef.format];
                    scope.columnColors[colDef.display] = colDef.color;
                  });


                  // What is the largest time value? This will be set as currTime
                  scope.availableTime = Object
                    .keys(availableTimes)
                    .map(function(time) { return parseInt(time, 10); })
                    .sort(function(a, b) { return a - b; });

                  scope.selectedTimeIndex = scope.availableTime.length-1;
                  scope.currTime = new Date(scope.availableTime[scope.selectedTimeIndex]);

                  // Initialize contentHash
                  scope.contentHash = miscServices.hashCode(JSON.stringify(scope.content));

                  // Register the zoomListener to jump to the timeIndex given in the hash
                  if(zoomKeyCtrl) {
                    zoomKeyCtrl.onZoom(zoomListener);
                  }

                  // Workaround for bug in ngModel and input type=range, see https://github.com/angular/angular.js/issues/6726
                  $timeout(function() {
                    element.find('input').val(scope.selectedTimeIndex);
                  });

                  scope.errorMessage = '';
                  scope.loading = false;
                },
                function(err) {
                  console.log('An error occurred loading the graph: ');
                  console.log(err);

                  scope.content = [];
                  scope.contentHash = '';
                  scope.errorMessage = gettext('An error occurred while loading this graph:') + ' ' + err;
                  scope.loading = false;
                }
              )

              // Load notes if notes are enabled
              .then(function() {
                if(scope.notesEnabled) {
                  notesViewer.loadNotes('data/' + scope.contentFile + '/', notesListener);
                }
              });
          }

          // Helper function to animate the date slider
          scope.startAnimation = function() {
            // If the animation is already running, we just increase the speed.
            if(scope.animationSpeed) {
              scope.animationSpeed++;
              return;
            }

            // Otherwise we need to set-up the animation first
            var nextStep;
            nextStep = function() {
              scope.selectedTimeIndex = Number(scope.selectedTimeIndex) || 0;
              scope.selectedTimeIndex = (scope.selectedTimeIndex === scope.availableTime.length-1 ? 0 : scope.selectedTimeIndex + 1);

              if(scope.animationSpeed) {
                $timeout(nextStep, 800/scope.animationSpeed);
              }
            };

            scope.animationSpeed = 1;
            nextStep();
          };

          scope.stopAnimation = function() {
            // stop the animation
            scope.animationSpeed = 0;
          };


          // Watch the loading status of the scope
          scope.$watch('loading', function(newVal, oldVal, scope) {
            // Show chart or loading indicator?
            if(scope.loading) {
              svg.classed("hidden", true);
              loadingIndicator.attr('class', 'loadingIndicator');
            } else if(scope.errorMessage) {
              svg.classed("hidden", true);
              loadingIndicator.attr('class', 'loadingErrorMessage');
              loadingIndicator.text(scope.errorMessage);
            } else {
              svg.classed('hidden', false);
              loadingIndicator.attr('class', 'loadingIndicator hidden');
            }
          }, true);

          // Watch the content hash attribute of the scope
          // This is a shortcut to watching the whole content: Watching the huge
          // 'content' array generates a lot of work during each Angular $digest,
          // while watching contentHash is comparing only a single value.
          scope.$watch('contentHash', function(newVal, oldVal, scope) {
            // (Re-)draw the chart

            if(scope.content && scope.content.length > 0) {
              draw(svg, svgBrush, scope);
            }
          }, true);

          // Watch the selectedTimeIndex to update currTime and redraw the chart
          scope.$watch('selectedTimeIndex', function(newVal, oldVal, scope) {
            if(newVal !== oldVal && scope.availableTime[newVal]) {
              // Update currTime
              scope.currTime.setTime(scope.availableTime[newVal]);

              // Re-draw if data has already been loaded
              if(scope.content && scope.content.length > 0) {
                draw(svg, svgBrush, scope);
              }
            }
          });

        }
      };
    }
  ]);
