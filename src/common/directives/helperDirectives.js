'use strict';

/**
 * @ngdoc overview
 * @name helperDirectives
 * @description
 *
 * This module provides smaller helper directives that are used in various places of the Dashboard.
 *
 */


angular.module('helperDirectives', [])



  /**
   * @ngdoc directive
   * @name helperDirectives.contenteditable
   * @restrict AE
   * @description
   *
   * This helper directive supports ngModel binding to contenteditable HTML elements.
   *
   * The code is taken from:
   * http://gaboesquivel.com/blog/2014/in-place-editing-with-contenteditable-and-angularjs/
   *
   */

  .directive("contenteditable", function() {
    return {
      require: "ngModel",
      link: function(scope, element, attrs, ngModel) {

        function read() {
          ngModel.$setViewValue(element.html());
        }

        ngModel.$render = function() {
          element.html(ngModel.$viewValue || "");
        };

        element.bind("blur keyup change", function() {
          scope.$apply(read);
        });
      }
    };
  })




  /**
   * @ngdoc directive
   * @name helperDirectives.datetimeFormatted
   * @restrict A
   * @description
   *
   * This helper directive supports using ngModel to bind JavaScript Date values to text input fields,
   * ensuring that the date and time are displayed in a locale specific format (using momentjs) and that
   * the locale-specific text is also correctly parsed back into a Date value.
   *
   * Can be used in conjunction with `angular-bootstrap-datetimepicker` for a nicely formatted date and
   * time picker to be displayed.
   *
   */

  // Directive for nicely formatted datetime text fields
  .directive('datetimeFormatted', function () {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, element, attrs, ngModel) {

      //format text going to user (model to view)
      ngModel.$formatters.push(function(value) {
        return ((!value || value > new Date('2999-01-01T01:00:00Z')) ? '' : window.moment(value).format('L LT'));
      });

      //format text from the user (view to model)
      ngModel.$parsers.push(function(value) {
        return (value ? window.moment(value, 'L LT').toDate() : new Date('2999-12-31T12:00:00.000Z'));
      });
    }
  };
 })


  /**
   * @ngdoc directive
   * @name helperDirectives.onlyPasteText
   * @restrict A
   * @description
   *
   * This helper directive restricts a contenteditable element's copy-&-paste functionality by only 
   * allowing plain text to be pasted. 
   *
   * It achieves this by intercepting the paste event, retrieving the clipboard plaintext content and
   * inserting it at the current cursor position.
   *
   */

  // Directive for nicely formatted datetime text fields
  .directive('onlyPasteText', ['$window', function($window) {
    // from https://stackoverflow.com/a/2925633 
    function insertTextAtCursor(text) {
      var sel, range;
      if ($window.getSelection) {
        sel = $window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
          range = sel.getRangeAt(0);
          range.deleteContents();
          range.insertNode( $window.document.createTextNode(text) );
        }
      } else if ($window.document.selection && $window.document.selection.createRange) {
        $window.document.selection.createRange().text = text;
      }
    }

    
    // from https://stackoverflow.com/a/6804718
    function handlePaste(e) {
      var clipboardData, pastedData;

      // Stop data actually being pasted into div
      e.stopPropagation();
      e.preventDefault();

      // Get pasted data via clipboard API
      clipboardData = e.originalEvent.clipboardData || $window.clipboardData;

      // TODO: Remove debug code
      console.log('Clipboard data: ');
      console.log(clipboardData);
      
      pastedData = clipboardData.getData('Text');

      // Insert pasted plain text at cursor position
      insertTextAtCursor(pastedData);
    }
    
    return {
      restrict: 'A',
      link: function (scope, element) {
        element.on('paste', handlePaste);
      }
    };
  }])


  /**
   * @ngdoc directive
   * @name helperDirectives.arrayToLines
   * @restrict A
   * @description
   *
   * This helper directive supports using ngModel to bind arrays to a textarea where each line in the
   * textarea represents one array value.
   *
   */

  // Directive for nicely formatted datetime text fields
  .directive('arrayToLines', function () {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function (scope, element, attrs, ngModel) {

        //format text going to user (model to view)
        ngModel.$formatters.push(function(value) {
          return (Array.isArray(value) ? value.join('\n') + '\n' : value + '\n');
        });

        //format text from the user (view to model)
        ngModel.$parsers.push(function(value) {
          return value.trim().split('\n');
        });
      }
    };
  })



  /**
   * @ngdoc directive
   * @name helperDirectives.keepVisibleOnScroll
   * @restrict AE
   * @description
   *
   * This helper directive keeps the content of a (long) element visible, even as
   * its (smaller) content would otherwise scroll out of view.
   *
   * @param {integer} keepVisibleOnScroll   The height of the inner content in pixels.
   *
   */

  // Directive for nicely formatted datetime text fields
  .directive('keepVisibleOnScroll', ['$window', function($window) {
    return {
      restrict: 'A',
      scope: {
        keepVisibleOnScroll: '='
      },
      link: function (scope, element) {
        console.log('Linking keepVisibleOnScroll directive...');
        console.log('Element:');
        console.log(element);

        // This CSS class styles the element to animate changes in margin-top
        element.addClass('animate-margin-move');

        var angularWindow = angular.element($window);
        var parent = element.parent();

        var currPos;
        var scrollFunction = function() {
          // Where do we need to scroll?
          var elOffsetTop = parent.offset().top;
          var scrollDiff = angularWindow.scrollTop() - elOffsetTop;

          // The new inner position is the difference between the scroll position and the start
          // of the outer element. If the scroll position is above the outer element, it is zero.
          // We do not increase the inner position beyond the value that makes the element
          // bottom-aligned ("keepVisible" should contain the height of the inner content in pixels).
          var newPos = Math.max(0, Math.min(scrollDiff + 80, parent.height() - parseInt(scope.keepVisibleOnScroll)));

          if(currPos === undefined || newPos !== currPos) {
            currPos = newPos;
            element.css('margin-top', newPos + 'px');
          }

          // NOTE: Literature says one should debounce scroll event handlers... however, in
          // practice it actually seems to perform much better than with debounce $timeout.
        };

        angularWindow.on('scroll', scrollFunction);

        element.on('$destroy', function() {
          // We clean up and remove our event handler on the window element
          angularWindow.off('scroll', scrollFunction);
        });

      }
    };
  }])



  /**
   * @ngdoc directive
   * @name helperDirectives.requiredIf
   * @restrict AE
   * @description
   *
   * This helper directive can be used to mark form fields that are only required if the passed
   * expression evaluates to true.
   *
   */

  .directive('requiredIf', [function() {
    return {
      require: 'ngModel',
      scope: {
        requiredIf: '='
      },
      link: function(scope, elm, attrs, ctrl) {

        ctrl.$validators.requiredIf = function(modelValue) {
          // Is the requiredIf expression false or is the control non-empty?
          // Both validate as correctly.
          return (!scope.requiredIf || !ctrl.$isEmpty(modelValue));
        };

        scope.$watch('requiredIf', function() {
          ctrl.$validate();
        });

      }
    };
  }])

  /**
   * @ngdoc directive
   * @name helperDirectives.requiredIf
   * @restrict AE
   * @description
   *
   * This helper directive can be used to use ng-model on numeric option values.
   * (See https://docs.angularjs.org/api/ng/directive/select#binding-select-to-a-non-string-value-via-ngmodel-parsing-formatting.)
   *
   */

  .directive('convertToNumber', function() {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, ngModel) {
        ngModel.$parsers.push(function(val) {
          return parseInt(val, 10);
        });
        ngModel.$formatters.push(function(val) {
          return '' + val;
        });
      }
    };
  })

;
