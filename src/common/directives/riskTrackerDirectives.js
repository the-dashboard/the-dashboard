'use strict';

/**
 * @ngdoc overview
 * @name riskTrackerDirectives
 * @description
 *
 * This module provides directives for displaying a "risk tracker", which is composed of
 * a risk matrix where risks are visually displayed based on their impact and likelihood
 * assessments and of a table which explains the risks in more detail.
 *
 */

angular.module('riskTrackerDirectives', ['gettext', 'chartServices', 'miscServices', 'risksServices', 'helperDirectives'])

  /**
   * @ngdoc directive
   * @name riskTrackerDirectives.riskMatrix
   * @restrict E
   * @description
   *
   * This directive displays a visual risk matrix for a specified set of risk elements. A risk matrix shows
   * square icons for each risk on a matrix which ranks these risks by impact and likelihood.
   *
   * This directive should usually be used within the {@link riskTrackerDirectives.riskTracker `riskTracker`}
   * directive, which complements the visual matrix with a table that explains the risks shown on the matrix
   * in more detail.
   *
   * The visual risk matrix is interactive. The user can move the risks on the matrix and their x and y values
   * are updated automatically.
   *
   * @scope
   * @param {number}    width             The width of the matrix in pixels.
   * @param {number}    height            The height of the matrix in pixels.
   * @param {Array}     risks             An array of all risks to be tracked. This is bi-directional, so changes on the
   *                                      risk matrix will change this array.
   * @param {string}    risksHash         A hash of the `risks` array. Used for AngularJS' `$watch` as checking the hash
   *                                      is faster than checking the `risks` array on each digest.
   * @param {Array}     riskXLabels       An array of string labels (we suggest three) to describe the x-axis values
   *                                      (e.g. low, moderate, high).
   * @param {Array}     riskYLabels       An array of string labels (we suggest three) to describe the y-axis values
   *                                      (e.g. low, moderate, high).
   * @param {string}    riskXLabel        The label of the x-axis (e.g. likelihood).
   * @param {string}    riskYLabel        The label of the y-axis (e.g. impact).
   * @param {number}    elementSize       The height and width of each square representing an individual risk in pixels.
   * @param {Date=}     oldRisksFilter    If this value is given, all historic risk value which were valid at that point
   *                                      in time will be displayed on the matrix as grey squares. Those cannot be
   *                                      moved.
   * @param {boolean=}  readOnly          If set to `true`, the user will not be able to move risks on the matrix.
   */

  .directive('riskMatrix', ['d3', 'miscServices',
    function(d3, miscServices) {

      // Draw the risk matrix
      var draw = function(svg, width, height, risks, xLabels, yLabels, riskXLabel, riskYLabel, elementSize, oldRisksFilter, readOnly, dispatch) {

        // fixed margin (larger on left, bottom due to axis)
        var margin_left = Math.round(width * 0.2);
        var margin_right = Math.round(width * 0.05);
        var margin_top = Math.round(height * 0.05);
        var margin_bottom = Math.round(height * 0.2);
        var elementHalf = Math.round(elementSize/2);
        var textHeight = 5;

        // Update height and width
        svg
          .attr('width', width)
          .attr('height', height);

        // Update clipping path
        svg
          .select('.clip-rect')
          .attr('x', margin_left - elementHalf)
          .attr('y', margin_top - elementHalf)
          .attr('width', width - margin_left - margin_right + elementSize)
          .attr('height', height - margin_top - margin_bottom + elementSize);

        // Scales for internal risk values (0-100)
        var xScale = d3.scaleLinear()
          .domain([0, 100])
          .range([margin_left, width-margin_right]);

        // Define y-scale
        var yScale = d3.scaleLinear()
          .domain([0, 100])
          .range([height-margin_bottom, margin_top]);


        // Draw grid
        svg.select('.x-grid')
          .selectAll('line')
          .data(xLabels.concat([''])) // Dummy entry for last line
          .enter()
          .append('line')
          .attr('x1', function(d, i) { return xScale(i/xLabels.length * 100); })
          .attr('y1', yScale(0))
          .attr('x2', function(d, i) { return xScale(i/xLabels.length * 100); })
          .attr('y2', yScale(100));

        svg.select('.y-grid')
          .selectAll('line')
          .data(yLabels.concat([''])) // Dummy entry for last line
          .enter()
          .append('line')
          .attr('x1', xScale(0))
          .attr('y1', function(d, i) { return yScale(i/yLabels.length * 100); })
          .attr('x2', xScale(100))
          .attr('y2', function(d, i) { return yScale(i/yLabels.length * 100); });


        // Draw labels (axes) for the grid
        svg.select('.x-axis-label')
          .attr('x', Math.round(width/2))
          .attr('y', height - Math.round(margin_bottom/3) + textHeight)
          .text(riskXLabel);

        svg.select('.x-axis')
          .selectAll('text')
          .data(xLabels)
          .enter()
          .append('text')
          .attr('x', function(d, i) { return xScale((i+0.5)/xLabels.length * 100); })
          .attr('y', height-Math.round(margin_bottom*2/3 + textHeight))
          .text(function(d) { return d; });

        svg.select('.y-axis-label')
          .attr('transform', 'translate(' + Math.round(margin_left/3+textHeight) + ',' + Math.round(height/2) + ')rotate(-90)')
          .text(riskYLabel);

        svg.select('.y-axis')
          .selectAll('text')
          .data(xLabels)
          .enter()
          .append('text')
          .attr('transform', function(d, i) { return 'translate(' + Math.round(margin_left*2/3+textHeight) + ',' + yScale((i+0.5)/yLabels.length * 100) + ')rotate(-90)'; })
          .text(function(d) { return d; });


        // Prepare Drag&Drop functionality
        var moveRisk = function(d) {
          // Update data value
          d.xAxis = Math.min(100, Math.max(0, xScale.invert(d3.event.x)));
          d.yAxis = Math.min(100, Math.max(0, yScale.invert(d3.event.y)));

          // Update element
          d3.select(this)
            .attr('transform', 'translate(' + (xScale(d.xAxis)-elementHalf) + ',' + (yScale(d.yAxis)-elementHalf) + ')');
        };

        var updateRisk = function(d, i) {
          // Trigger change (and start digest)
          dispatch.call('riskChange', this, d, i);
        };

        var drag = d3.drag()     // TODO: Needs "subject" to keep relative position...
          .subject(function(d) {
            return {
              x: xScale(d.xAxis),
              y: yScale(d.yAxis)
            };
          })
          .on('drag', moveRisk)
          .on('end', updateRisk);


        // Draw each risk element
        // Each risk element is an object with the following properties:
        //  number: Number to be displayed in the rectangle
        //  active: Active true/false - can it be dragged? Also influences color!
        //  xAxis:  xAxis value on 0-100 scale
        //  yAxis:  yAxis value on 0-100 scale
        if(Array.isArray(risks) && risks.length > 0) {

          // First we draw INACTIVE RISK ELEMENTS
          var risksInactive;
          if (oldRisksFilter) {
            risksInactive = risks.filter(function(d) {
                return (!d.active && !d.isDeleted && (d.valid_from <= oldRisksFilter && d.valid_to > oldRisksFilter));
              });
          } else {
            risksInactive = [];
          }


          var riskElements = svg.select('.data')
            .selectAll('.risk-inactive')
            .data(risksInactive);

          // Remove elements
          riskElements
            .exit()
            .remove();

          // New elements
          var newGroups = riskElements
            .enter()
            .append('g');

          newGroups
            .append('rect');

          newGroups
            .append('title');

          newGroups
            .append('text');

          // Update elements
          newGroups
            .merge(riskElements)
              .classed('risk-element', true)
              .classed('risk-inactive', true)
              .attr('transform', function(d) { return 'translate(' + (xScale(d.xAxis)-elementHalf) + ',' + (yScale(d.yAxis)-elementHalf) + ')'; })

              .select('text')
              .attr('x', elementHalf)
              .attr('y', elementHalf+textHeight)
              .text(function(d) { return d.number; });

          newGroups
            .merge(riskElements)
              .select('rect')
              .attr('width', elementSize)
              .attr('height', elementSize);

          newGroups
            .merge(riskElements)
            .select('title')
            .text(function(d) { return miscServices.htmlToText(d.title); });


          // Now we draw ACTIVE RISK ELEMENTS
          var risksActive = risks.filter(function(d) {
            return (d.active && !d.isDeleted);
          });



          // Note: We remove all existing elements and draw them again in all cases.
          //       This ensures that they will appear on top of the inactive ones.
          svg.select('.data').selectAll('.risk-active').remove();

          riskElements = svg.select('.data')
            .selectAll('.risk-active')
            .data(risksActive);

          // Remove elements
          riskElements
            .exit()
            .remove();

          // New elements
          newGroups = riskElements
            .enter()
            .append('g');

          newGroups
            .append('rect');

          newGroups
            .append('title');

          newGroups
            .append('text');

          // Update elements
          var mergedElements = newGroups
            .merge(riskElements)
              .classed('risk-element', true)
              .classed('risk-active', true)
              .attr('transform', function(d) { return 'translate(' + (xScale(d.xAxis)-elementHalf) + ',' + (yScale(d.yAxis)-elementHalf) + ')'; });

          if(!readOnly) {
              mergedElements.call(drag);
          }

          newGroups
            .merge(riskElements)
              .select('text')
              .attr('x', elementHalf)
              .attr('y', elementHalf+textHeight)
              .text(function(d) { return d.number; });

          newGroups
            .merge(riskElements)
              .select('rect')
              .attr('width', elementSize)
              .attr('height', elementSize);

          newGroups
            .merge(riskElements)
                .select('title')
                .text(function(d) { return miscServices.htmlToText(d.title); });

        }
      };

      return {
        restrict: 'E',

        scope: {
          width: '=',
          height: '=',
          risks: '=',
          risksHash: '=',
          riskXLabels: '=',
          riskYLabels: '=',
          riskXLabel: '=',
          riskYLabel: '=',
          elementSize: '=',
          oldRisksFilter: '=?',
          readOnly: '=?'
        },

        link: function(scope, element) {
          var svg;

          // Create a SVG root element
          svg = d3.select(element[0]).append('svg');

          // Add containers for graph and axes
          svg.append('g').attr('class', 'x-axis axis');
          svg.append('g').attr('class', 'y-axis axis');
          svg.append('g').attr('class', 'x-grid grid');
          svg.append('g').attr('class', 'y-grid grid');
          svg.append('text').attr('class', 'x-axis-label');
          svg.append('text').attr('class', 'y-axis-label');
          svg.append('g').attr('class', 'data').attr('clip-path', 'url(#clip-path-' + scope.$id + ')');


          // Add clipping path
          svg.append('clipPath')
            .attr('id', 'clip-path-' + scope.$id)
            .append('rect')
            .attr('class', 'clip-rect');

          // Initialize the 'risksChange' event
          var dispatch = d3.dispatch('riskChange');

          dispatch.on('riskChange', function() {
            scope.$apply(
              function() {
                // Recompute hash...
                scope.risksHash = miscServices.hashCode(JSON.stringify(scope.risks));
              }
            );
          });

          // Watch the risks hash attribute of the scope
          // This is a shortcut to watching the whole content: Watching the huge
          // 'risks' array generates a lot of work during each Angular $digest,
          // while watching risksHash is comparing only a single value.
          scope.$watch('risksHash', function(newVal, oldVal, scope) {
            // Update the risk matrix
            draw(svg, scope.width, scope.height, scope.risks, scope.riskXLabels, scope.riskYLabels, scope.riskXLabel, scope.riskYLabel, scope.elementSize, scope.oldRisksFilter, scope.readOnly, dispatch);
          }, true);

          // Also draw new when the oldRisksFilter drop-down changes
          scope.$watch('oldRisksFilter', function(newVal, oldVal, scope) {
            // Update the risk matrix
            draw(svg, scope.width, scope.height, scope.risks, scope.riskXLabels, scope.riskYLabels, scope.riskXLabel, scope.riskYLabel, scope.elementSize, scope.oldRisksFilter, scope.readOnly, dispatch);
          }, true);

        }
      };
  }])


  /**
   * @ngdoc directive
   * @name riskTrackerDirectives.riskTracker
   * @restrict E
   * @description
   *
   * This directive displays a risk tracker for a specific entity. The risk tracker is composed of a visual risk matrix
   * using the {@link riskTrackerDirectives.riskMatrix} directive and of a table that lists the risks with their
   * descriptions.
   *
   * The table is interactive, meaning that risks can be edited, removed and added. Changes can be saved and also a
   * snapshot of all risks as they currently exist can be "frozen". These snapshots are used to allow a comparison
   * between current and past risks.
   *
   * @scope
   * @param {width}    width                    The width of the whole risk tracker in pixels.
   * @param {height}   height                   The height of the whole risk tracker in pixels.
   * @param {string}   riskCategory             An identifier determining which risks should be displayed, e.g. the risk
   *                                            entity. This identifier is used to retrieve risks and will also be
   *                                            stored with any new risks created from the tracker.
   * @param {string}   riskCategoryDescription  A description for this risk category that should be presented to the
   *                                            user.
   * @param {number}   maxDaysOld               Restricts the loading of risks to those which are no older than
   *                                            `maxDaysOld` days.
   * @param {string}   riskXLabel               The description of the x-axis of the risk matrix (e.g. "Likelihood").
   * @param {Array}    riskXLabels              An array of string labels (we suggest three) to describe the x-axis
   *                                            values (e.g. low, moderate, high).
   * @param {string}   riskYLabel               The description of the y-axis of the risk matrix (e.g. "Impact").
   * @param {Array}    riskYLabels              An array of string labels (we suggest three) to describe the y-axis
   *                                            values (e.g. low, moderate, high).
   * @param {boolean}  showImpacts              If set to `true` and the user selects frozen (past) risks, the directive
   *                                            will load impacts between the frozen risks and today and display them
   *                                            using the {@link impactDirectives.impactViewer `impactViewer`}
   *                                            directive.
   * @param {string=} sourceLink                If given, will be used for the `sourceUrl` of interactions and newsfeed
   *                                            entries.
   *
   */

  .directive('riskTracker', ['miscServices', 'risksService', '$q', '$filter', 'gettext',
    function(miscServices, risksService, $q, $filter, gettext) {

      return {
        restrict: 'E',
        replace: 'true',
        templateUrl: 'common/directives/riskTracker.tmpl.html',

        scope: {
          width: '=',
          height: '=',
          riskCategory: '=',
          riskCategoryDescription: '=',
          maxDaysOld: '=',
          riskXLabel: '=',
          riskXLabels: '=',
          riskYLabel: '=',
          riskYLabels: '=',
          showImpacts: '=?',
          sourceLink: '@?'
        },

        require: '?^zoomKey',

        link: function(scope, elems, attrs, zoomKeyCtrl) {
          var minDate;

          scope.loading = false;
          scope.errorMessage = "";
          scope.risks = [];
          scope.risksHash = '';
          scope.oldRiskDatetimes = [];
          scope.zoomKey = (zoomKeyCtrl ? zoomKeyCtrl.getZoomKey() : undefined);

          var addedDatetimes = {};


          // Helper function to store an "old" copy of the risk to identify changes
          var makeOldCopy = function(risk) {
            return {
              title: risk.title,
              description: risk.description,
              xAxis: risk.xAxis,
              yAxis: risk.yAxis
            };
          };

          var hasRiskChanged = function(risk, exact) {
            // Do we compare "exact" or "visible" changes?
            if(exact) {
              return (
                !risk.old ||
                risk.title !== risk.old.title ||
                risk.description !== risk.old.description ||
                Math.abs(risk.xAxis - risk.old.xAxis) > 0 ||
                Math.abs(risk.yAxis - risk.old.yAxis) > 0
              );
            } else {
              return (
                !risk.old ||
                miscServices.htmlToText(risk.title).replace(/\s+/g, ' ') !== miscServices.htmlToText(risk.old.title).replace(/\s+/g, ' ') ||
                miscServices.htmlToText(risk.description).replace(/\s+/g, ' ') !== miscServices.htmlToText(risk.old.description).replace(/\s+/g, ' ') ||
                Math.abs(risk.xAxis - risk.old.xAxis) > 3 ||
                Math.abs(risk.yAxis - risk.old.yAxis) > 3
              );
            }
          };


          // We are initializing variables we need for getting the risks setup and adding new risks
          var currDatetime = new Date();
          var currNumber = 0;
          var currLetter = 0;
          var letterList = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

          // Load the relevant risks from the database
          if(scope.riskCategory === undefined || scope.riskCategory === '') {
            scope.errorMessage = gettext('No valid risk category has been specified.');
          } else {
            // Ok, start loading.
            scope.loading = true;

            if(zoomKeyCtrl) {
              // Notify zoomKeyCtrl that we are currently loading and should not
              // jump around yet
              zoomKeyCtrl.startedLoading();
            }

            // From which date do we load risks?
            if(isNaN(scope.maxDaysOld) || scope.maxDaysOld <= 0) {
              minDate = undefined;
            } else {
              minDate = new Date();
              minDate.setDate(minDate.getDate() - scope.maxDaysOld);
            }

            // Get the risks using the risksService.
            risksService.getRisksByCategory(scope.riskCategory, minDate).then(
              function(risksResult) {
                var i;

                var hasActive = false;

                for(i = 0;i < risksResult.length;i++) {
                  if(i === 0) {
                    // First entry is always a new risk trail
                    hasActive = (risksResult[i].valid_from <= currDatetime && risksResult[i].valid_to > currDatetime);

                    scope.risks.push(risksResult[i]);

                    // Active risks are numbered with numbers, legacy ones with letters
                    if(hasActive) {
                      currNumber = currNumber + 1;
                      scope.risks[0].number = currNumber;
                    } else {
                      scope.risks[0].number = miscServices.repeatString(letterList[(currLetter % 52)], Math.floor(currLetter / 52) + 1);
                      currLetter = currLetter + 1;
                    }

                    scope.risks[0].head = true;  // Is this the first risk item in this trail?
                    scope.risks[0].active = hasActive;
                    scope.risks[0].hasActive = hasActive; // Is the risk currently active (for all historic data)?
                    scope.risks[0].isDeleted = false;
                    scope.risks[0].old = makeOldCopy(scope.risks[0]);
                  } else {
                    if(risksResult[i-1].initialId !== risksResult[i].initialId) {
                      // Ok, this is a new risk trail
                      hasActive = (risksResult[i].valid_from <= currDatetime && risksResult[i].valid_to > currDatetime);

                      scope.risks.push(risksResult[i]);

                      // Active risks are numbered with numbers, legacy ones with letters
                      if(hasActive) {
                        currNumber = currNumber + 1;
                        scope.risks[scope.risks.length-1].number = currNumber;
                      } else {
                        scope.risks[scope.risks.length-1].number = miscServices.repeatString(letterList[(currLetter % 52)], Math.floor(currLetter / 52) + 1);
                        currLetter = currLetter + 1;
                      }

                      scope.risks[scope.risks.length-1].head = true;  // Is this the first risk item in this trail?
                      scope.risks[scope.risks.length-1].active = hasActive;
                      scope.risks[scope.risks.length-1].hasActive = hasActive;
                      scope.risks[scope.risks.length-1].isDeleted = false;
                      scope.risks[scope.risks.length-1].old = makeOldCopy(scope.risks[scope.risks.length-1]);
                    } else {
                      // This is a follow-up (=older) entry for the same risk trail.
                      scope.risks.push(risksResult[i]);

                      // Active risks are numbered with numbers, legacy ones with letters
                      if(hasActive) {
                        scope.risks[scope.risks.length-1].number = currNumber;
                      } else {
                        scope.risks[scope.risks.length - 1].number = miscServices.repeatString(letterList[(currLetter % 52)], Math.floor(currLetter / 52) + 1);
                      }

                      scope.risks[scope.risks.length-1].head = false;  // Is this the first risk item in this trail?
                      scope.risks[scope.risks.length-1].active = false;
                      scope.risks[scope.risks.length-1].hasActive = hasActive;
                      scope.risks[scope.risks.length-1].isDeleted = false;
                      scope.risks[scope.risks.length-1].old = makeOldCopy(scope.risks[scope.risks.length-1]);
                    }
                  }

                  // Have we seen this valid_to date before? Otherwise add to oldRiskDatetimes!
                  // (If it is older than now.)
                  var justBefore = new Date(risksResult[i].valid_to - 1000);
                  if(risksResult[i].valid_to <= currDatetime && !(justBefore.toISOString() in addedDatetimes)) {
                    addedDatetimes[justBefore.toISOString()] = 1;
                    scope.oldRiskDatetimes.push({
                      dt: justBefore,
                      text: $filter('date')(justBefore, 'yyyy-MM-dd HH:mm')
                    });
                  }
                }

                if(zoomKeyCtrl) {
                  // Notify zoomKeyCtrl that we are no longer loading and can now jump around
                  zoomKeyCtrl.stoppedLoading();
                }

                // Set hash
                scope.risksHash = miscServices.hashCode(JSON.stringify(scope.risks));
                scope.loading = false;
                scope.errMessage = '';
              },
              function(err) {
                scope.errorMessage = err;

                if(zoomKeyCtrl) {
                  // Notify zoomKeyCtrl that we are no longer loading and can now jump around
                  zoomKeyCtrl.stoppedLoading();
                }

                scope.loading = false;
              }
            );
          }

          // Define helper functions on the scope

          // Helper to convert risk title to plain text
          scope.htmlToText = miscServices.htmlToText;

          // addRisk - adds new (empty) risk to the list
          scope.addRisk = function() {
            currNumber = currNumber + 1;
            scope.risks.push(
              {
                id: -currNumber,
                number: currNumber,
                title: gettext('New Title'),
                description: gettext('New Description'),
                riskStatus: 'Final', // TODO: Implement optional status functionality?
                                     // TODO: i18n risk status levels
                category: scope.riskCategory,
                valid_from: currDatetime,
                valid_to: new Date(2999, 12, 31, 12, 0, 0),
                xAxis: 50,
                yAxis: 50,
                isDeleted: false,
                active: true,
                old: {
                  title: '',
                  description: '',
                  xAxis: 0,
                  yAxis: 0
                }
              }
            );

            // Update hash
            scope.risksHash = miscServices.hashCode(JSON.stringify(scope.risks));
          };

          // deleteRisk - removes given risk from the list
          scope.deleteRisk = function(riskId) {
            if(!miscServices.translatedConfirm(gettext('Are you sure you want to remove this risk item? Once you save your changes, you will not be able to reactive it.'))) { return; }   // TODO: make nicer modal dialog using angularJS

            var myInitialId;
            var i;

            // We find and delete the risk.
            for(i = 0;i < scope.risks.length;i++) {
              if(scope.risks[i].id === riskId && scope.risks[i].active) {
                // Delete the risk itself.
                scope.risks[i].isDeleted = true;
                myInitialId = scope.risks[i].initialId;
              }
            }

            // If this is an already saved risk, we need to look through the related risks:
            // Now we iterate again and set all related risks to "hasActive = false" and give it a letter...
            if(myInitialId !== undefined && myInitialId >= 0) {
              var myLetter = miscServices.repeatString(letterList[(currLetter % 52)], Math.floor(currLetter / 52) + 1);
              currLetter = currLetter + 1;
              for(i = 0;i < scope.risks.length;i++) {
                if(scope.risks[i].initialId === myInitialId) {
                  // This risk no longer has an active part.
                  scope.risks[i].hasActive = false;
                  scope.risks[i].number = myLetter;
                }
              }
            }

            // Update hash
            scope.risksHash = miscServices.hashCode(JSON.stringify(scope.risks));

          };


          // saveRisks - saves all changes to the database (including added and removed risks)
          scope.savingRisks = false;
          scope.saveRisks = function(callback) {
            var updateRisksPromise = $q.resolve();

            scope.savingRisks = true;

            scope.risks.forEach(function(d) {
              // We set sourceLinks for all risks
              if(scope.sourceLink) {
                d.sourceLink = scope.sourceLink;
              }

              if(d.id < 0 && !d.isDeleted) {
                // New risk - add to database
                updateRisksPromise = updateRisksPromise
                  .then(function() { return risksService.addRisk(d); })
                  .then(
                    function(res) {
                      // update "old risk" copy
                      d.old = makeOldCopy(d);

                      return res;
                    }
                  );
                  
              } else if(d.id >= 0 && hasRiskChanged(d, true) && !d.isDeleted) {
                // Changed risk - update database
                updateRisksPromise = updateRisksPromise
                  .then(function() { return risksService.updateRisk(d, hasRiskChanged(d, false)); })
                  .then(
                    function(res) {
                      // update "old risk" copy
                      d.old = makeOldCopy(d);

                      return res;
                    }
                  );
              } else if(d.id >= 0 && d.isDeleted) {
                // Deleted risk - we do not remove isDeleted flag as this hides risk from view!
                updateRisksPromise = updateRisksPromise
                  .then(function() { return risksService.deleteRisk(d); })
                  .then(
                    function(res) {
                      // Ok, if there is another risk with this initialId, we need to update its head!
                      for(var i = 0;i < scope.risks.length;i++) {
                        if(scope.risks[i].initialId === d.initialId && !scope.risks[i].isDeleted) {
                          scope.risks[i].head = true;
                        }
                      }

                      return res;
                    }
                  );
              }
            });

            updateRisksPromise.then(
              function() {
                // All risks have been updated
                scope.savingRisks = false;

                // We call the callback (used for freezing risks)
                if(callback !== undefined) { callback(); }
              },
              function(err) {
                window.alert(err + '\n\n' + gettext('We suggest reloading the page to obtain the state currently saved in the database and to try again afterwards.'));  // TODO: nicer error handling
                scope.savingRisks = false;
              }
            );
          };


          // freezeRisks - freezes all existing risks
          scope.freezingRisks = false;
          scope.freezeRisks = function() {
            if(!miscServices.translatedConfirm(gettext('Are you sure you want to freeze all risks? All changes will be saved, then a new "snapshot" of all existing risks will be created and turned into "history". Note that this action cannot be undone and that frozen risks cannot be edited or deleted.'))) { return; }   // TODO: make nicer modal dialog using angularJS

            scope.freezingRisks = true;

            var currDatetime = new Date();

            // First we need to save risks
            scope.saveRisks(function() {
              // Now we can iterate over all risks
              var freezeRiskPromise = $q.resolve();

              scope.risks.forEach(function(d) {
                if(!d.isDeleted && d.active) {
                  // Active, not deleted risks are being frozen
                  freezeRiskPromise = freezeRiskPromise
                    .then(function() { return risksService.freezeRisk(d, currDatetime); })
                    .then(
                      function(res) {
                        // Ok, we have received the new risk object
                        // Set it to active and add the number...
                        res.active = true;
                        res.head = true;
                        res.number = d.number;
                        res.isDeleted = false;
                        res.old = makeOldCopy(res);

                        // ... and the frozen one to inactive.
                        d.active = false;
                        d.head = false;

                        // Is the frozen one deleted?
                        if(d.id < 0) { d.isDeleted = true; }


                        // We need to add this object into the risks array (at the right place)
                        for(var i = 0;i < scope.risks.length;i++) {
                          if((i === 0 || scope.risks[i-1].initialId !== res.initialId) && scope.risks[i].initialId === res.initialId) {
                            // The item should go before all other items with the same initialId!
                            scope.risks.splice(i, 0, res);
                          }
                        }

                        return res;
                      }
                    );
                }
              });

              freezeRiskPromise.then(
                function() {
                  scope.freezingRisks = false;

                  // The currDatetime - 1 sec should be added to the filter
                  var justBefore = new Date(currDatetime - 1000);
                  scope.oldRiskDatetimes.push({
                    dt: justBefore,
                    text: $filter('date')(justBefore, 'yyyy-MM-dd HH:mm')
                  });

                  // Update hash
                  scope.risksHash = miscServices.hashCode(JSON.stringify(scope.risks));
                },
                function(err) {
                  window.alert(gettext('An error occurred while freezing risk objects:') + '\n\n' + err + '\n\n' + gettext('We suggest reloading the page to obtain the clean state from the database and to try again.')); // TODO: better error handling
                  scope.freezingRisks = false;
                }
              );
            });
          };


          // Predicate function for filter for old risks depending on drop-down value
          scope.showOldRisk = function(val) {
            // We do not show old risks if the drop-down is unset
            if (!scope.oldRisksFilter) {
              return false;
            }

            // An old risk has its validity matching the oldDatetime
            return (val.valid_from <= scope.oldRisksFilter && val.valid_to > scope.oldRisksFilter);
          };

          // Helper functions to obtain "prior risk" for display of old text and title
          scope.getOldRisk = function(risk) {
            // We do not show old risks if the drop-down is unset or if the current risk has no initialId (== not yet saved)
            if (!scope.oldRisksFilter || risk.initialId === undefined || risk.initialId < 0) {
              return undefined;
            }

            // An old risk has its validity matching the oldDatetime but the same initialID
            return scope.risks.find(function(oldRisk) {
              return (oldRisk.initialId === risk.initialId && oldRisk !== risk && oldRisk.valid_from <= scope.oldRisksFilter && oldRisk.valid_to > scope.oldRisksFilter);
            });
          };

        }
      };
    }
  ]);
