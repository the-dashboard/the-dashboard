'use strict';
/* globals Tribute */

/**
 * @ngdoc overview
 * @name socialDirectives
 * @description
 *
 * This module provides directives for the social features of Dashboard, i.e. the interactions users can add to
 * Dashboard elements (such as notes, risks and audit forms) and the newsfeed that will show actions on subscribed
 * items.
 *
 * These directives are supported by the services provided in the {@link socialServices `socialServices`} module.
 *
 */

angular.module('socialDirectives', ['gettext', 'socialServices', 'usersServices', 'configServices', 'miscServices', 'notesServices'])


/**
 * @ngdoc filter
 * @name socialDirectives#elementTypeToText
 * @description
 *
 * The elementTypeToText filter transforms the internal element types in the dashboard (e.g.
 * `note`, `risk` etc.) in nicely formatted descriptions.
 *
 */

  .filter('elementTypeToText', ['gettext', function(gettext) {
    var elementTypesMapping = {
      'note': gettext('Note'),
      'risk': gettext('Risk'),
      'auditFormData': gettext('Audit form'),
      'task': gettext('OCA task')
    };
    
    return function(input) {
      return (elementTypesMapping[input] !== undefined ? elementTypesMapping[input] : input);
    };
  }])



/**
 * @ngdoc directive
 * @name socialDirectives.atMentions
 * @restrict E
 * @description
 *
 * Uses the Tribute.js package to add at-mentions to the given container (can be a textarea or a contenteditable
 * div).
 *
 * (Inspired by angular-tribute package.)
 *
 * @scope
 * @param {Array}   atMentions   This array will be updated with the user IDs of any at-mentions added to or removed
 *                               from the text.
 *
 */

// TODO: New feature should scan for at-mentions in plaintext and highlight them with a span tag... (in a
// contenteditable we can do that while adding the at-mention, but in plaintext we need to do this
// differently...)

  .directive('atMentions', ['configService', 'usersService', function(configService, usersService) {
    return {
      restrict: 'A',

      scope: {
        atMentions: '='
      },

      require: 'ngModel',

      controller: ['$scope', function($scope) {
        this.$onDestroy = function() {
          $scope.tribute.detach($scope.atMentionElement);
        };
      }],

      link: function(scope, element, attrs, ngModelCtrl) {
        scope.atMentionElement = element[0];
        scope.potentialAtMentions = {};

        if(!Array.isArray(scope.atMentions)) {
          scope.atMentions = [];
        }

        // This function checks for at-mentions in the potentialAtMentions array => are they actually
        // contained in the text? If yes, we will add them to the atMentions array.
        var checkAtMentions = function() {
          scope.atMentions = Object
            .keys(scope.potentialAtMentions)
            .filter(function(mentionedId) {
              return (ngModelCtrl.$modelValue.indexOf(scope.potentialAtMentions[mentionedId]) >= 0);
            });

          console.log('checkAtMentions called.');
          console.log([ scope.potentialAtMentions, ngModelCtrl.$modelValue, scope.atMentions ]);
        };

        // We need to load the SharePoint user list before we can attach the at-mention element...
        // Do we have a global configuration set to only retrieve users from a specific group? Otherwise we get all...
        configService
          .getConfiguration()
          .then(
            function(config) {
              return usersService.getUsersByGroup(config['At-Mentions Group']);
            }
          )
          .then(
            function(users) {
              var mappedUsers = users.map(
                function(user) {
                  // We use a combination of login, name and email of a user as key that can be searched
                  // for using the at-mentions.
                  return {
                    id: user.id,
                    key: user.name + ' ' + user.login + ' ' + user.email,
                    value: user.name
                  };
                }
              );

              // We need to initialize the potentialAtMentions will all *actual* at-mentions already known...
              scope.atMentions.forEach(
                function(mentionId) {
                  var mentionUser = mappedUsers.find(function(d) { return (d.id.toString() === mentionId.toString()); });
                  scope.potentialAtMentions[mentionUser.id.toString()] = '@' + mentionUser.value;
                }
              );
              
              // We initialize the Tribute.js object               
              scope.tribute = new Tribute({
                values: mappedUsers,
                menuItemTemplate: function(item) {
                  return item.original.value;
                }
              });

              scope.tribute.attach(scope.atMentionElement);

              scope.atMentionElement.addEventListener(
                'tribute-replaced',
                function(e) {
                  // TODO: Remove debug code
                  console.log('At-mention triggered:');
                  console.log(e.detail.item);
                  
                  // We add every replacement to the potentialAtMentions, which we then check using our $watch below
                  // (as it is possible that the user removes the @-mentions afterwards...)
                  scope.potentialAtMentions[e.detail.item.original.id.toString()] = '@' + e.detail.item.original.value;

                  // We update the actual atMentions array...
                  checkAtMentions();
                }
              );
            }
          );

        // We watch for changes to the model values to update the actual at-mentions based on the potential at-mentions list.
        ngModelCtrl.$viewChangeListeners.push(checkAtMentions);
        
      }
    };
  }])



  /**
   * @ngdoc directive
   * @name socialDirectives.dashboardNewsFeed
   * @restrict E
   * @description
   *
   * This directive displays the newsfeed for the given user (or the currently logged in user if no user has been
   * specified via attribute).
   *
   * @scope
   * @param {integer=}  userId           The user ID of the user whose newsfeed should be displayed. Optional, if not
   *                                     specified the directive will show the newsfeed of the currently active user.
   * @param {integer=}  limit            If specified, only `limit` newsfeed entries will be displayed. If `allowMore`
   *                                     is set to `true`, the user is allowed to load additional entries, which will
   *                                     also be retrieved in `limit`-sized batches. If `limit` is omitted, it will
   *                                     default to 30.
   * @param {boolean=}  allowMore        If set to `true`, the user will have the option to load additional newsfeed
   *                                     entries.
   *
   */

  .directive('dashboardNewsFeed', ['gettext', 'subscriptionService', 'miscServices', function(gettext, subscriptionService, miscServices) {
    return {
      restrict: 'E',
      scope: {
        userId: '=?',
        limit: '=?',
        allowMore: '=?'
      },

      link: function(scope) {
        scope.errMessage = undefined;
        scope.limit = (parseInt(scope.limit) ? parseInt(scope.limit) : 30);
        scope.lastLoaded = undefined;
        scope.newsfeedGroups = [];
        scope.newsfeedGroupsHash = {};
        scope.hasMore = false;

        // First we need to load the newsfeed entries for the given user, as specified by the scope values.
        scope.isLoading = true;

        console.log('link called on dashboardNewsFeed...');
        console.log(scope);

        // Helper function to add newsfeed entries to the corresponding groups
        var addNewsfeedEntries = function(newsObjs) {

          var today = new Date();

          var yesterday = new Date();
          yesterday.setDate(today.getDate()-1);

          var thisWeekString = miscServices.formatDate(today, '%Y-%V');

          var lastWeek = new Date();
          lastWeek.setDate(today.getDate()-7);
          var lastWeekString = miscServices.formatDate(lastWeek, '%Y-%V');

          var lastMonth = new Date();
          lastMonth.setDate(15);
          lastMonth.setMonth(today.getMonth()-1);


          newsObjs.forEach(function(entry) {
            // Special preparation for special case newsfeed entries

            // For changed risks we need to create an artificial risk array
            if(entry.actionType === 'changed-risk' && entry.contentVariables.risk && entry.contentVariables.risk.old) {
              entry._riskMatrix = [
                {    // Current risk
                  active: true,
                  isDeleted: false,
                  valid_from: miscServices.parseDatetime(entry.contentVariables.risk.valid_from),
                  valid_to: miscServices.parseDatetime(entry.contentVariables.risk.valid_to),
                  number: entry.contentVariables.risk.number,
                  title: entry.contentVariables.risk.title,
                  xAxis: entry.contentVariables.risk.xAxis,
                  yAxis: entry.contentVariables.risk.yAxis
                },
                {    // Past risk
                  active: false,
                  isDeleted: false,
                  valid_from: new Date('1990-01-01T01:00:00'),
                  valid_to: miscServices.parseDatetime(entry.contentVariables.risk.valid_from),
                  number: entry.contentVariables.risk.number,
                  title: entry.contentVariables.risk.old.title,
                  xAxis: entry.contentVariables.risk.old.xAxis,
                  yAxis: entry.contentVariables.risk.old.yAxis
                }
              ];
              entry._oldRisksFilter = new Date('1991-01-01T01:00:00');
            }

            // Put this object into the right group
            var group = gettext('Older');
            if (entry.newsTime.getDate() === today.getDate() && entry.newsTime.getMonth() === today.getMonth() && entry.newsTime.getFullYear() === today.getFullYear()) {
              group = gettext('Today');
            } else if (entry.newsTime.getDate() === yesterday.getDate() && entry.newsTime.getMonth() === yesterday.getMonth() && entry.newsTime.getFullYear() === yesterday.getFullYear()) {
              group = gettext('Yesterday');
            } else if (miscServices.formatDate(entry.newsTime, '%Y-%V') === thisWeekString) {
              group = gettext('This Week');
            } else if (miscServices.formatDate(entry.newsTime, '%Y-%V') === lastWeekString) {
              group = gettext('Last Week');
            } else if (entry.newsTime.getMonth() === today.getMonth() && entry.newsTime.getFullYear() === today.getFullYear()) {
              group = gettext('This Month');
            } else if (entry.newsTime.getMonth() === lastMonth.getMonth() && entry.newsTime.getFullYear() === lastMonth.getFullYear()) {
              group = gettext('Last Month');
            }

            if(!scope.newsfeedGroupsHash[group]) {
              scope.newsfeedGroupsHash[group] = { group: group, entries: [ entry ] };
              scope.newsfeedGroups.push(scope.newsfeedGroupsHash[group]);
            } else {
              scope.newsfeedGroupsHash[group].entries.push(entry);
            }
          });
        };

        subscriptionService
          .getNewsfeed(scope.userId, scope.limit, 0, true)
          .then(
            function(newsfeedData) {
              // newsfeedData is an array where the first element contains the actual newsfeed objects and
              // the second element is a boolean value that is true when additional entries could be loaded
              var newsObjs = newsfeedData[0];

              addNewsfeedEntries(newsObjs);

              scope.lastLoaded = (newsObjs.length >= 1 ? newsObjs[newsObjs.length-1].id : undefined);
              scope.isLoading = false;

              console.log('Received newsfeedGroups:');
              console.log(scope.newsfeedGroups);

              // Is it allowed to load more entries and are there more entries? (We assume there are more
              // entries if we have loaded exactly "limit" entries.)
              scope.hasMore = (scope.allowMore && newsfeedData[1]);

              console.log('hasMore?');
              console.log([ scope.allowMore, scope.hasMore, newsObjs.length, scope.limit ]);

             },
            function(err) {
               scope.errMessage = err;
               scope.isLoading = false;
             }
           );

        // We also need to provide a function to load more newsfeed entries
        scope.loadMore = function() {
          scope.isLoading = true;

          subscriptionService
            .getNewsfeed(scope.userId, scope.limit, scope.lastLoaded, true)
            .then(
              function(newsfeedData) {
                addNewsfeedEntries(newsfeedData[0]);
                scope.isLoading = false;

                // Is it allowed to load more entries and are there more entries? (We assume there are more
                // entries if we have loaded exactly "limit" entries.)
                scope.hasMore = (scope.allowMore && newsfeedData[1]);
              },
              function(err) {
                scope.errMessage = err;
                scope.isLoading = false;
              }
            );
        };


        // Helper function that will be called whenever a subscription is removed through the newsfeed buttons
        scope.removeSubCallback = function(subscriptionId) {
          // We iterate through all loaded entries and remove them if they belong to this subscription ID.
          scope.newsfeedGroups.forEach(function(group) {
            group.entries = group.entries.filter(
              function(newsObj) {
                return (newsObj.subscriptionId === undefined || newsObj.subscriptionId === null || newsObj.subscriptionId.toString() !== subscriptionId.toString());
              }
            );
          });

        };


        // That's it.
      },

      templateUrl: 'common/directives/dashboardNewsFeed.tmpl.html'
    };
  }])


  /**
   * @ngdoc directive
   * @name socialDirectives.dashboardNewsFeedElement
   * @restrict E
   * @description
   *
   * This directive displays a single newsfeed item, that needs to be passed using the `item` attribute.
   *
   * @scope
   * @param {Object}     item                The newsfeed item to be displayed by this directive.
   * @param {Function=}  removeSubCallback   Will be called with the subscription ID, if a subscription is being
   *                                         removed.
   *
   */

  .directive('dashboardNewsFeedElement', ['$q', 'keywordsService', 'interactionService', 'subscriptionService', 'miscServices', function($q, keywordsService, interactionService, subscriptionService, miscServices) {

    return {
      restrict: 'E',
      scope: {
        item: '=',
        removeSubCallback: '=?'
      },

      link: function(scope) {
        // We need to prepare a keywordMapping for newsfeed elements which need to display nicely printed keywords.
        scope.keywordMapping = undefined;
        keywordsService
          .getAllKeywords()
          .then(function(keywords) {
            scope.keywordMapping = {};
            keywords.forEach(
              function(d) {
                scope.keywordMapping[d.id] = d.name;
              }
            );
          });

        // We need to provide a function to unsubscribe from a given subscription.
        scope.removeSubscription = function(item, alwaysRemoveSubscription) {
          item.isUnsubscribing = true;

          var interactionSourceType = item.subscriptionType;
          var interactionSourceId = item.subscriptionTarget;

          if(item.subscriptionType === 'interaction') {
            // For interaction type subscriptions we need to use the sourceType and sourceId instead.
            interactionSourceType = item.sourceType;
            interactionSourceId = item.sourceId;
          }

          // For these cases, we never unwatch, we always remove the subscription directly.
          var interactionSummaryPromise;
          if(alwaysRemoveSubscription) {
            // By returning an empty interactionSummary, we will always proceed as if we are not watching.
            interactionSummaryPromise = $q.resolve(undefined);
          } else {
            interactionSummaryPromise = interactionService.getInteractionSummary(interactionSourceType, interactionSourceId);
          }

          // Are we "watching" the source object?
          // If yes, we should trigger an unwatch instead of a removeSubscription...
          interactionSummaryPromise
            .then(
              function(interactionSummary) {
                if(interactionSummary && Array.isArray(interactionSummary.watch) && interactionSummary.watch.indexOf(item.userId.toString()) >= 0) {
                  // We are "watching" => we trigger an addUnwatch!
                  return interactionService.addUnwatch(item.userId, interactionSourceType, interactionSourceId);

                } else {
                  // We are not watching, so we trigger a removeSubscription instead.

                  // We need to create a fake subscription object to be able to remove it
                  var subscriptionObj = {
                    id: item.subscriptionId,
                    userId: item.userId,
                    type: item.subscriptionType,
                    target: item.subscriptionTarget
                  };
                  
                  return subscriptionService.removeSubscription(subscriptionObj);
                }
              }
            )
            .then(
              function() {
                // We trigger the removeSubCallback, if any...
                if(scope.removeSubCallback) {
                  scope.removeSubCallback(item.subscriptionId);
                }

                // TODO: Unwatches might affect multiple subscriptions, which we do not account for here...

                item.wasUnsubscribed = true;
                item.isUnsubscribing = false;
              },
              function(err) {
                miscServices.translatedAlert(err);
                item.isUnsubscribing = false;
              }
            );
        };
      },

      templateUrl: 'common/directives/dashboardNewsFeedElement.tmpl.html'
    };

  }])


  /**
   * @ngdoc directive
   * @name socialDirectives.interactionButtons
   * @restrict E
   * @description
   *
   * This directive provides interaction buttons that show the current number of interactions for a given target item
   * (specified by `targetType` and `targetId`) and let the user toggle these interactions by clicking on the different
   * interaction items.
   *
   * @scope
   * @param {String}   targetType             The type of the target for these interactions (e.g. `note`, `risk` etc.).
   * @param {String}   targetId               The ID of the target for these interactions.
   * @param {String=}  targetDescription      A description of the target for these interactions (only required if
   *                                          interaction buttons are enabled).
   * @param {String=}  sourceUrl              If specified the source URL that will be sent together with the
   *                                          interactions created using these buttons. If unset, the current Dashboard
   *                                          URL will be used.
   * @param {Object=}  interactionSummary     If an interactionSummary object already exists for the given target, it
   *                                          can be passed using this attribute and will be used for displaying the
   *                                          interaction counts. Otherwise this attribute can also be left unset, the
   *                                          buttons will then retrieve the interactionSummary from the database
   *                                          (based on `targetType` and `targetId`).
   * @param {Array=}   enabledButtons         If passed, only interaction buttons listed in this array (based on their
   *                                          interaction value) will be enabled. If unset, all possible interactions
   *                                          will be enabled.
   *
   */

  .directive('interactionButtons', ['interactionService', 'usersService', 'miscServices', function(interactionService, usersService, miscServices) {

    return {
      restrict: 'E',
      scope: {
        targetType: '=',
        targetId: '=',
        targetDescription: '=?',
        sourceUrl: '=?',
        interactionSummary: '=?',
        enabledButtons: '=?'
      },

      link: function(scope, element) {
        scope.areLoading = {};

        // Grouped interactions
        // Interaction => Interaction group
        // Only one interaction in a given group can be set by a user at the same time, this means that
        // if the user sets another interaction we remove any existing interactions from that group.
        var interactionGroups = {
          'highlight1': 'highlights',
          'highlight2': 'highlights',
          'highlight3': 'highlights'
        };


        // If interactionSummary is not specified, we need to retrieve it from the given targetType
        // and targetId.
        if(scope.interactionSummary === undefined || scope.interactionSummary === null) {
          scope.interactionSummary = {};
          interactionService
            .getInteractionSummary(scope.targetType, scope.targetId)
            .then(
              function(interactionSummary) {
                scope.interactionSummary = interactionSummary;
              }
            );
        }

        // We need to find out which user we are
        scope.userId = undefined;
        usersService
          .getCurrentUser()
          .then(function(user) {
            scope.userId = user.id;
          });

        // Helper function to check if a given interaction button is enabled
        scope.isEnabled = function(interaction) {
          // If no valid enabledButtons array has been passed, all buttons are enabled
          if(!Array.isArray(scope.enabledButtons)) {
            return true;
          }

          // Otherwise only interactions listed in enabledButtons will be enabled
          return (scope.enabledButtons.indexOf(interaction) >= 0);
        };

        // Helper function to check the count of a given interaction
        scope.interactionCount = function(interaction) {
          return (Array.isArray(scope.interactionSummary[interaction]) && scope.interactionSummary[interaction].length > 0 ? scope.interactionSummary[interaction].length : '');
        };

        // Helper function to check if a given interaction is "active"
        // (= the user has set it, which means its user ID is included in the interactionSummary)
        scope.isActive = function(interaction) {
          // Did we already retrieve a valid user ID? If not, we always return false.
          if(scope.userId === undefined) {
            return false;
          }

          // Ok, let's check the interactionSummary array...
          return (Array.isArray(scope.interactionSummary[interaction]) && scope.interactionSummary[interaction].indexOf(scope.userId.toString()) >= 0);
        };


        // Open the interactions sidebar (when user clicks on comment button)
        scope.openInteractions = function() {
          interactionService.openSidebarViaLink(scope.targetType, scope.targetId, scope.targetDescription, scope.sourceUrl);
        };


        // Helper functions to update the local interactionSummary object after any changes
        var localAddInteraction = function(interaction) {
          // Did we already retrieve a valid user ID? If not, we just return and the user has to try again...
          if(scope.userId === undefined) {
            return;
          }

          if(Array.isArray(scope.interactionSummary[interaction])) {
            if(scope.interactionSummary[interaction].indexOf(scope.userId.toString()) < 0) {
              scope.interactionSummary[interaction].push(scope.userId.toString());
            }
          } else {
            scope.interactionSummary[interaction] = [ scope.userId.toString() ];
          }
        };

        var localRemoveInteraction = function(interaction) {
          // Did we already retrieve a valid user ID? If not, we just return and the user has to try again...
          if(scope.userId === undefined) {
            return;
          }

          if(Array.isArray(scope.interactionSummary[interaction])) {
            var idxToRemove = scope.interactionSummary[interaction].indexOf(scope.userId.toString());
            if(idxToRemove >= 0) {
              scope.interactionSummary[interaction].splice(idxToRemove, 1);
            }
          }
        };


        // We register a listener for this target, so that we get informed when interactions are changed and can
        // update our interactionSummary accordingly.
        var interactionListener = function(change, interactionObj) {
          if(change === 'add') {
            localAddInteraction(interactionObj.interactionType);
          } else if(change === 'remove') {
            localRemoveInteraction(interactionObj.interactionType);
          }
        };

        interactionService.registerInteractionListener(interactionListener, scope.targetType, scope.targetId);

        // Remove listener when directive is being destroyed
        element.on('$destroy', function() {
          interactionService.removeInteractionListener(interactionListener);
        });



        // Special case: Trigger watch or unwatch (only for the watch interaction!)
        scope.triggerWatch = function() {
          // Did we already retrieve a valid user ID? If not, we just return and the user has to try again...
          if(scope.userId === undefined) {
            return;
          }

          // We are loading...
          scope.areLoading.watch = true;

          // Is the watch interaction active? If no, we call addWatch, otherwise we use addUnwatch...
          if(scope.isActive('watch')) {
            // Watch is active => we unwatch!
            interactionService
              .addUnwatch(scope.userId, scope.targetType, scope.targetId, scope.sourceUrl)
              .then(
                function() {
                  // Done. Let's update the interactionSummary object here...
                  localRemoveInteraction('watch');

                  // And we are no longer loading...
                  scope.areLoading.watch = false;
                },
                function(err) {
                  // Display the error message.
                  miscServices.translatedAlert(err);

                  // And we are no longer loading...
                  scope.areLoading.watch = false;
                }
              );

          } else {
            // Watch is inactive => we watch!
            interactionService
              .addWatch(scope.userId, scope.targetType, scope.targetId, scope.targetDescription, scope.sourceUrl)
              .then(
                function() {
                  // Done. Let's update the interactionSummary object here...
                  localAddInteraction('watch');

                  // And we are no longer loading...
                  scope.areLoading.watch = false;
                },
                function(err) {
                  // Display the error message.
                  miscServices.translatedAlert(err);

                  // And we are no longer loading...
                  scope.areLoading.watch = false;
                }
              );
          }
        };


        // Trigger an interaction (when the user clicks on an interaction icon)
        scope.triggerInteraction = function(interaction) {
          // Did we already retrieve a valid user ID? If not, we just return and the user has to try again...
          if(scope.userId === undefined) {
            return;
          }

          // Is this interaction active? If no, we add it, if yes, we remove it.
          if(scope.isActive(interaction)) {
            // We need to remove this interaction...
            scope.areLoading[interaction] = true;
            interactionService
              .removeInteractionsByDetails(interaction, scope.userId, scope.targetType, scope.targetId)
              .then(
                function() {
                  // Update the interactionSummary...
                  localRemoveInteraction(interaction);

                  // And we are no longer loading...
                  scope.areLoading[interaction] = false;
                },
                function(err) {
                  // Display the error message.
                  miscServices.translatedAlert(err);

                  // And we are no longer loading...
                  scope.areLoading[interaction] = false;
                }
              );

          } else {
            // We need to add this interaction...
            scope.areLoading[interaction] = true;
            interactionService
              .addInteraction(
                {
                  interactionTime: new Date(),
                  userId: scope.userId,
                  targetType: scope.targetType,
                  targetId: scope.targetId,
                  interactionType: interaction,
                  content: {}
                },
                scope.sourceUrl,
                scope.targetDescription
              )
              .then(
                function() {
                  // Update the interactionSummary...
                  localAddInteraction(interaction);

                  // Note that we now also watch this object (interaction except unwatch always triggers a watch)...
                  localAddInteraction('watch');

                  // And we are no longer loading...
                  scope.areLoading[interaction] = false;

                },
                function(err) {
                  // Display the error message.
                  miscServices.translatedAlert(err);

                  // And we are no longer loading...
                  scope.areLoading[interaction] = false;

                }
              );

            // Grouped interactions: If we add a grouped interaction, we need to
            // remove any existing interactions in the same group.
            if(interactionGroups[interaction] !== undefined) {
              Object.keys(interactionGroups).forEach(
                  function(groupedInteraction) {
                    if(groupedInteraction !== interaction && interactionGroups[groupedInteraction] === interactionGroups[interaction] && scope.isActive(groupedInteraction)) {
                      // Ok, this interaction is part of the same interaction group and enabled, so we need to disable it...
                      scope.areLoading[groupedInteraction] = true;
                      interactionService
                        .removeInteractionsByDetails(groupedInteraction, scope.userId, scope.targetType, scope.targetId)
                        .then(
                          function() {
                            // Update the interactionSummary...
                            localRemoveInteraction(groupedInteraction);

                            // And we are no longer loading...
                            scope.areLoading[groupedInteraction] = false;
                          },
                          function(err) {
                            // Display the error message.
                            miscServices.translatedAlert(err);

                            // And we are no longer loading...
                            scope.areLoading[groupedInteraction] = false;
                          }
                        );
                    }
                  }
                );
            }
          }
        };


        // End.

      },

      templateUrl: 'common/directives/interactionButtons.tmpl.html'
    };

  }])


  /**
   * @ngdoc directive
   * @name socialDirectives.interactionComments
   * @restrict E
   * @description
   *
   * This directive provides a text box to enter new comments as interactions for the given target object (identified
   * by `targetType` and `targetId`). By adding a comment to a target object, the user will automatically be added as
   * a watcher on that object (by triggering a `watch` interaction).
   *
   * If the comment at-mentions any users, they will also be notified that they have been mentioned.
   *
   * @scope
   * @param {String}    targetType          The type of the target for these interactions (e.g. `note`, `risk` etc.).
   * @param {String}    targetId            The ID of the target for these interactions.
   * @param {String=}   targetDescription   A description of the target for the new interaction.
   * @param {String=}   sourceUrl           The source URL that should be sent with the interactions and the
   *                                        at-mentions. If unset, the current Dashboard URL will be sent.
   *
   */

  .directive('interactionComments', ['gettext', 'miscServices', 'interactionService', 'subscriptionService', function(gettext, miscServices, interactionService, subscriptionService) {

    return {
      restrict: 'E',
      scope: {
        targetType: '=',
        targetId: '=',
        targetDescription: '=?',
        sourceUrl: '=?'
      },

      link: function(scope) {
        // We initialize the target values for the comment textbox.
        scope.commentText = '';
        scope.commentAtMentions = [];
        scope.isSaving = false;

        // Function to submit the new comment as interaction
        scope.saveComment = function() {
          // Is there any text at all? We cannot submit empty comments...
          if(scope.commentText === '') {
            miscServices.translatedAlert(gettext('You cannot submit an empty comment.'));
            return;
          }

          // Ok, we can go ahead and submit this comment.

          // Step 1: Add the interaction itself...
          scope.isSaving = true;
          interactionService
            .addInteraction(
              {
                interactionTime: new Date(),
                targetType: scope.targetType,
                targetId: scope.targetId,
                interactionType: 'comment',
                content: {
                  commentText: scope.commentText,
                  atMentions: scope.commentAtMentions
                }
              },
              scope.sourceUrl,
              scope.targetDescription
            )
            .then(
              function(interactionObj) {
                // Ok, now we have to add the at-mentions if any...
                return subscriptionService.updateMentions(
                    scope.targetType,
                    scope.targetId,
                    interactionObj.id,
                    interactionService.addSidebarToURL(scope.sourceUrl, scope.targetType, scope.targetId, scope.targetDescription),
                    scope.commentAtMentions,
                    {
                      from: interactionObj.userId,
                      type: 'comment',
                      text: scope.commentText,
                      targetDescription: scope.targetDescription
                    }
                  );
              }
            )
            .then(
              function() {
                // Ok, we are done with storing this comment.
                scope.isSaving = false;
                scope.commentText = '';
                scope.commentAtMentions = [];
              }
            );

        };

      },

      templateUrl: 'common/directives/interactionComments.tmpl.html'
    };

  }])


  /**
   * @ngdoc directive
   * @name socialDirectives.interactionList
   * @restrict E
   * @description
   *
   * This directive displays a list of interactions for the given target (identified by `targetType` and `targetId`).
   * If `sortOldToNew` is set to `true`, the oldest entries will be displayed first.
   *
   * @scope
   * @scope
   * @param {String}    targetType             The type of the target for these interactions (e.g. `note`, `risk` etc.).
   * @param {String}    targetId               The ID of the target for these interactions.
   * @param {String=}   targetDescription      A description of the target element for which the interaction list should
   *                                           be displayed (only required if `enableNew` is `true`).
   * @param {integer=}  limit                  If specified, only `limit` interactions will be displayed. If `allowMore`
   *                                           is set to `true`, the user is allowed to load additional entries, which
   *                                           will also be retrieved in `limit`-sized batches. If `limit` is omitted,
   *                                           it will default to 30.
   * @param {boolean=}  sortOldToNew           If set to `true`, oldest entries will be displayed first. Otherwise,
   *                                           newest entries will be displayed first.
   * @param {boolean=}  allowMore              If set to `true`, the user will have the option to load additional
   *                                           interactions.
   * @param {boolean=}  enableNew              If set to `true`, the list will contain a comments box that allows to
   *                                           create new comments in that view.
   * @param {String=}   sourceUrl              If specified the source URL that will be sent together with any
   *                                           interactions created in this list. If unset, the current Dashboard
   *                                           URL will be used.
   *
   */

  .directive('interactionList', ['interactionService', function(interactionService) {

    return {
      restrict: 'E',
      scope: {
        targetType: '=',
        targetId: '=',
        targetDescription: '=?',
        limit: '=?',
        sortOldToNew: '=?',
        allowMore: '=?',
        enableNew: '=?',
        sourceUrl: '=?'
      },

      link: function(scope, element) {
        scope.interactions = [];
        scope.currentLimit = (parseInt(scope.limit) ? parseInt(scope.limit) : 30);
        scope.hasMore = false;

        // We need to retrieve the interactions for the given targetType and targetId from the database...
        interactionService
          .getInteractions(scope.targetType, scope.targetId, scope.sortOldToNew)
          .then(
            function(interactions) {
              scope.interactions = interactions;
              scope.hasMore = (scope.interactions.length > scope.currentLimit);
            }
          );


        // Helper function to "load" more interactions
        // (We always retrieve all interactions from the database, this just increases the currentLimit which
        // is used by ngRepeat to limit the number of interactions to display.)
        scope.loadMore = function() {
          scope.currentLimit += (parseInt(scope.limit) ? parseInt(scope.limit) : 30);
          scope.hasMore = (scope.interactions.length > scope.currentLimit);
        };


        // We register a listener on our target so that we get notified when interactions are being added or removed
        // and can act accordingly.
        var interactionListener = function(change, interactionObj) {
          if(change === 'add') {
            // We add the matching interaction => we need to add it at the right place, depending on how
            // we are sorted.
            if(scope.sortOldToNew) {
              // We add the new interaction at the bottom (for this we need to show all interactions)
              if(scope.currentLimit < scope.interactions.length+1) {
                scope.currentLimit = scope.interactions.length + 1;
              }

              scope.interactions.push(interactionObj);
            } else {
              // We add the comment at the top (and do not need to show all interactions for this)
              scope.interactions.splice(0, 0, interactionObj);
            }
          } else if(change === 'remove') {
            // We remove the matching interaction.
            scope.interactions = scope.interactions.filter(
              function(d) {
                return (d.id.toString() !== interactionObj.id.toString());
              }
            );
          }
        };

        interactionService.registerInteractionListener(interactionListener, scope.targetType, scope.targetId);

        // Remove listener when directive is being destroyed
        element.on('$destroy', function() {
          interactionService.removeInteractionListener(interactionListener);
        });


      },

      templateUrl: 'common/directives/interactionList.tmpl.html'
    };

  }])


  /**
   * @ngdoc directive
   * @name socialDirectives.subscriptionEye
   * @restrict E
   * @description
   *
   * This directive provides interaction buttons that show the current number of interactions for a given target item
   * (specified by `targetType` and `targetId`) and let the user toggle these interactions by clicking on the different
   * interaction items.
   *
   * @scope
   * @param {String}   type                 The subscription type (e.g. `note`, `interaction`).
   * @param {Object}   target               The target object or string that describes the target for these
   *                                        interactions. If an object is being provided, it will be converted into a
   *                                        target string. If a string is provided, it will be used as is.
   * @param {String}   targetDescription    A description of the target for this subscription.
   * @param {String=}  actionType           The action type this subscription should apply to (`'*'` or unset for all
   *                                        actions).
   *
   *
   */

  .directive('subscriptionEye', ['subscriptionService', function(subscriptionService) {

    return {
      restrict: 'E',
      scope: {
        type: '=',
        target: '=',
        targetDescription: '=',
        actionType: '=?'
      },

      link: function(scope) {
        scope.isUpdating = false;
        var subscriptionObj;

        // We load the subscriptions for the current user to be able to tell whether the user is subscribed to the
        // given target.
        var loadSubscriptionStatus = function() {                   
          var targetString = subscriptionService.targetObjToString(scope.type, scope.target);
          var actionType = (scope.actionType === undefined || scope.actionType === null ? '*' : scope.actionType);

          console.log('loadSubscriptionStatus called for type = ' + scope.type + ', target = ' + targetString + ', actionType = ' + actionType + '...');
          
          scope.isActive = false;
          scope.isLoading = true;
          return subscriptionService
            .getSubscriptionsByUser(undefined, scope.type)
            .then(
              function(subscriptionObjs) {
                subscriptionObj = subscriptionObjs
                  .find(
                    function(subscriptionObj) {
                      var thisActionType = (subscriptionObj.actionType === undefined || subscriptionObj.actionType === null ? '*' : subscriptionObj.actionType);
                      return (subscriptionObj.target === targetString && thisActionType === actionType);
                    }
                  );

                if(subscriptionObj) {
                  scope.isActive = true;
                }

                scope.isLoading = false;
              }
            );
        };
        
        loadSubscriptionStatus();
        scope.$watch('type', loadSubscriptionStatus);
        scope.$watch('target', loadSubscriptionStatus, true);
        scope.$watch('actionType', loadSubscriptionStatus);

        

        // Helper function to trigger the given subscription.
        scope.triggerSubscription = function() {
          scope.isUpdating = true;

          if(scope.isActive && subscriptionObj) {
            // We need to remove an active subscription.
            subscriptionService
              .removeSubscription(subscriptionObj)
              .then(
                function() {
                  scope.isActive = false;
                  scope.isUpdating = false;
                  subscriptionObj = undefined;
                }
              );
          } else {
            // We need to add a new subscription.
            subscriptionService
              .addSubscription(undefined, scope.type, scope.target, scope.targetDescription, scope.actionType)
              .then(
                function(_subscriptionObj) {
                  scope.isActive = true;
                  scope.isUpdating = false;
                  subscriptionObj = _subscriptionObj;
                }
              );
          }
        };


        // End.

      },

      templateUrl: 'common/directives/subscriptionEye.tmpl.html'
    };

  }])


;
