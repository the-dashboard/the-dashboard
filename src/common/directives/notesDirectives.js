'use strict';

/**
 * @ngdoc overview
 * @name notesDirectives
 * @description
 *
 * This module provides directives for displaying notes. It is designed for the
 * Ongoing Risk Assessment (ORA) dashboards where users can enter notes for each
 * risk entity.
 *
 */

angular.module('notesDirectives', ['notesServices', 'risksServices', 'usersServices', 'socialServices', 'textAngular', 'ngTagsInput', 'ngDialog', 'socialDirectives'])

  /**
   * @ngdoc directive
   * @name notesDirectives.dashboardNotes
   * @restrict E
   * @description
   *
   * This directive displays all existing notes for a given notes keyword (specified in `keywordId`) and provides an
   * edit functionality to edit and remove existing or create new notes. Optionally, it also provides a search
   * functionality to search the loaded notes (with live filtering).
   *
   * @scope
   * @param {Object}  keywordId            Specifies the ID (in the form `data/...`, `keyword/...` or `tree/...`) of the
   *                                       keyword for which notes should be retrieved. The directive will load all
   *                                       notes for the given keyword and ensure that new notes being added will be
   *                                       added with this keyword. Can be given as a string (then a single keyword will
   *                                       be filtered) or as an array (then all keywords in the array must match).
   * @param {boolean} enableKeywords       If set to `true`, the user will be able to specify additional keywords to
   *                                       assign to the note (in addition to the always included `keywordId` keywords).
   * @param {boolean} showSearch           If set to `true`, a search bar will be displayed which can be used to live
   *                                       filter the existing notes.
   * @param {boolean} showMore             If set to `true`, a "show more notes" button will be displayed when there are
   *                                       more notes available than limited by limitTo.
   * @param {Array=}   statusFilter        If given, only notes in the given status(es) will be displayed. If given can
   *                                       be a string (to filter for one status) or array (to filter for multiple
   *                                       statuses).
   * @param {boolean=} showStatusFilter    If set to `true`, the user will have the option to change the `statusFilter`.
   * @param {boolean=} showKeywordFilter   If set to `true`, the user will have the option to add further keywords to
   *                                       restrict the notes to be displayed.
   * @param {number=} limitTo              Can be used to specify the maximum number of notes to be displayed.
   * @param {boolean=} showImpacts         If set to `true`, the user will have the option to assign impact flags to the
   *                                       notes.
   * @param {string=} impactsRiskCategory  If `showImpacts` is set to `true`, a risk category (from the
   *                                       {@link riskTrackerDirectives.riskTracker `riskTracker`} directive) can be
   *                                       specified here. The user can then select a risk from this risk tracker
   *                                       category to which the impact flag should be assigned. If left empty, the
   *                                       impact flags will not be assigned to any specific risk item.
   * @param {string=} impactsRiskCategoryDescription   If an `impactsRiskCategory` is specified, a human-readable
   *                                                   description of that category can be specified here, which will
   *                                                   be used for user notifications.
   * @param {string=} showNew              If set to `"top"` or `"bottom"` the user will have the option to add new notes,
   *                                       with the button being shown at the top or bottom of the notes list,
   *                                       respectively. If unset or set to `false` or empty, the user will not be able
   *                                       to add new notes.
   * @param {string=} sourceLink           If given, will be written as sourceLink to newly created notes (editing notes will not
   *                                       change their sourceLink).
   *
   */

  .directive('dashboardNotes', ['$q', '$filter', '$timeout', '$rootScope', 'ngDialog', 'notesService', 'keywordsService', 'impactsService', 'interactionService', 'usersService', function($q, $filter, $timeout, $rootScope, ngDialog, notesService, keywordsService, impactsService, interactionService, usersService) {

    return {
      restrict: 'E',
      replace: 'true',
      require: ['?callOnChange', '?^zoomKey'],
      scope: {
        keywordId: '=',
        enableKeywords: '=',
        showSearch: '=',
        showMore: '=',
        statusFilter: '=?',
        showStatusFilter: '=?',
        showKeywordFilter: '=?',
        limitTo: '=?',
        showImpacts: '=?',
        impactsRiskCategory: '=?',
        impactsRiskCategoryDescription: '=?',
        showNew: '=?',
        sourceLink: '@?'
      },

      link: function(scope, element, attrs, ctrls) {
        // Split controllers
        var callOnChangeCtrl = ctrls[0];
        var zoomKeyCtrl = ctrls[1];

        // Default values for some of the options
        if(isNaN(scope.limitTo)) {
          scope.limitTo = 1000;
        }

        // Initialize scope variables
        scope.editorActiveNote = undefined;
        scope.searchKey = '';
        scope.currLimitTo = Number(scope.limitTo);
        scope.zoomKey = (zoomKeyCtrl ? zoomKeyCtrl.getZoomKey() : undefined);
        scope.makeNoteIntro = notesService.makeNoteIntro;

        // We need to use a "statusFilterModel" for the checkboxes in the view (workaround for how angularjs treats
        // checkboxes)
        var statusFilterObjUpdate = function() {
          if(scope.statusFilter) {
            var thisFilter;
            // We turn the statusFilter into an array if it has been given as a string.
            if((typeof scope.statusFilter === 'string' || scope.statusFilter instanceof String)) {
              thisFilter = [ scope.statusFilter ];
            } else {
              thisFilter = scope.statusFilter;
            }

            scope.statusFilterObj = { 'Draft': false, 'Final': false, 'Closed': false, 'Reviewed': false };
            thisFilter.forEach(function(status) {
              scope.statusFilterObj[status] = true;
            });
          } else {
            scope.statusFilterObj = { 'Draft': true, 'Final': true, 'Closed': true, 'Reviewed': true };
          }
        };
        statusFilterObjUpdate();

        // Helper function to turn statusFilterObj (which might have changed in the meantime) back into an array
        var getCurrentStatusFilter = function() {
          return Object.keys(scope.statusFilterObj).filter(function(k) { return scope.statusFilterObj[k]; });
        };



        // Initialize the keywordFilter array and call the keywordServices to get their names
        scope.keywordFilterLoaded = false;

        var keywordFilterUpdate = function() {
          // Fix the keywordId if given as empty or in string form
          if(scope.keywordId === undefined || scope.keywordId === '') {
            // No keyword given, so we initialize an empty array
            scope.keywordId = [ ];
          } else if((typeof scope.keywordId === 'string' || scope.keywordId instanceof String)) {
            // We turn the keywordId into an array if it has been given as a string.
            scope.keywordId = [ scope.keywordId ];
          }

          // If we have no values at all, we initialize with simple values to get started quickly
          if(!scope.keywordFilterLoaded) {
            scope.keywordFilter = scope.keywordId.map(function(keyword) {
              return { id: keyword, name: keyword };
            });
          }

          // Update with data from the keywordService once we retrieve it
          keywordsService.getKeywordArray(scope.keywordId)
            .then(function(keywordArr) {
              // TODO: Remove debug code
              console.log('Received keywordArr from keywordsService, updating keywordFilter:');
              console.log(keywordArr);

              scope.keywordFilter = keywordArr;
              scope.keywordFilterLoaded = true;
            });
        };
        keywordFilterUpdate();


        // TODO: Move notes to a "sort by artificial modification date", as the real notification date now changes
        //       when interactionSummary is updated! (Same for last modified by!)


        // Initialize notesImpacts
        scope.notesImpacts = { 'NEW': [] };

        var notesImpactsUpdate = function() {
          if(scope.showImpacts) {
            // Load existing impacts based on keywordIds
            impactsService.getImpactsBySourceKeywords('notes', scope.keywordId, false)
              .then(function(impacts) {
                // Map to notesImpacts

                // TODO: Remove debug code
                console.log('Received impacts: ');
                console.log(impacts);

                scope.notesImpacts = { 'NEW': [] };
                impacts.forEach(function(impact) {
                  if(!Array.isArray(scope.notesImpacts['note-' + impact.sourceId.toString()])) {
                    scope.notesImpacts['note-' + impact.sourceId.toString()] = [];
                  }

                  scope.notesImpacts['note-' + impact.sourceId.toString()].push(impact);
                });

                // TODO: Remove debug code
                console.log('Mapped notes:');
                console.log(scope.notesImpacts);
              });
          }
        };
        notesImpactsUpdate();


        // Watch for new / changed / removed notes.
        var noteListenerFilter = function(note) {
          // Is this note relevant for our keywordFilter and statusFilterObj?
          var matchedKeywords = note.keywords.filter(function(k) { return (scope.keywordFilter.findIndex(function(keywordObject) { return (keywordObject.id === k.id); }) !== -1); });
          return (matchedKeywords.length > 0 && scope.statusFilterObj[note.noteStatus]);
        };

        var noteListenerAdd = function(note) {
          // Does note already exist?
          if(scope.notes.findIndex(function(n) { return n.id === note.id; }) === -1) {
            scope.notes.push(note);
          }
        };

        var noteListenerRemove = function(oldNote) {
          // TODO: Remove debug code
          console.log('noteListenerRemove called:');
          console.log([oldNote, scope.notes, scope.notes.findIndex(function(note) { return note.id === oldNote.id; })]);

          if(oldNote.id < 0) { return; } // Note not an already saved note

          var noteIdx = scope.notes.findIndex(function(note) { return note.id === oldNote.id; });

          if(noteIdx === -1) { return; } // Note not found

          // Remove note
          scope.notes.splice(noteIdx, 1);
        };

        notesService.addNoteListeners(noteListenerFilter, noteListenerAdd, noteListenerRemove);


        // Initialize defaults for new notes
        var makeKeywordsNew = function() {
          return scope.keywordId.map(function(keyword) {
                    var keywordObject = {
                      id:   keyword,
                      name: keyword
                    };

                    // Start update of this object from the keywordsService.
                    keywordsService.getKeywordName(keywordObject);
                    return keywordObject;
                  });
        };

        scope.newNote = {
          id: -1,
          noteTextNew: '',
          attachmentsNew: [],
          keywordsNew: makeKeywordsNew(),
          noteStatusNew: 'Final',
          editorActive: false
        };


        // Loads initial notes based on keywordId
        var loadNotes = function() {
          if(zoomKeyCtrl) {
            // Notify zoomKeyCtrl that we are currently loading and should not
            // jump around yet
            zoomKeyCtrl.startedLoading();
          }

          notesService.getNotesForKeywords(scope.keywordFilter.map(function(k) { return k.id; }), getCurrentStatusFilter(), false, false, 4*scope.currLimitTo).then(
            function(myNotes) {
              scope.notes = myNotes;
              scope.haveAllNotes = false;

              // TODO: Remove debug code
              console.log('Received new notes:');
              console.log(myNotes);

              if(scope.notes.length < 4*scope.currLimitTo) {
                // We did not get the number of notes we asked for, so there are not that many.
                // => We have received all of them!
                scope.haveAllNotes = true;
              }

              if(zoomKeyCtrl) {
                // Notify zoomKeyCtrl that we are no longer loading and can now jump around
                zoomKeyCtrl.stoppedLoading();
              }

              scope.notesAreLoading = false;
            },
            function(err) {
              // TODO: Nicer error messaging.
              window.alert(err);

              if(zoomKeyCtrl) {
                // Notify zoomKeyCtrl that we are no longer loading and can now jump around
                zoomKeyCtrl.stoppedLoading();
              }

              scope.notesAreLoading = false;
            },
            function(preNotes) { // Notification from cache or while loading keywords, attachments
              scope.notes = preNotes;
              scope.haveAllNotes = false;
            }
          );
        };

        // Show loading indicator and initialize empty notes array...
        scope.notes = [];
        scope.haveAllNotes = false;
        scope.notesAreLoading = true;

        // Call once after initial linking
        loadNotes();

        // Watch changes to the keywordFilter and reload notes if it changes
        scope.$watch('keywordFilter', function(newVal, oldVal, scope) {
          // TODO: Remove debug code
          console.log('keywordFilter has changed: ');
          console.log([newVal, oldVal, scope]);

          // We are only interested if the IDs have changed, we do not care for changed
          // name tags.
          if(Array.isArray(oldVal) && Array.isArray(newVal)) {
            var oldKeywordIds = oldVal.map(function(k) { return k.id; }).sort();
            var newKeywordIds = newVal.map(function(k) { return k.id; }).sort();

            if(angular.equals(oldKeywordIds, newKeywordIds)) {
              // Nothing has changed after all.
              return;
            }
          }

          // TODO: Remove debug code
          console.log('And it has REALLY changed... Reloadig notes.');
          console.log([newVal, oldVal, scope]);

          // Update notes from database
          loadNotes();
        }, true);

        // Watch changes for they keywordId and reload if it changes
        scope.$watch('keywordId', function(newVal, oldVal, scope) {
          // We need to update the keyword filter
          keywordFilterUpdate();
          notesImpactsUpdate();

          // We need to change the keyword for any new note that is currently being written
          if(oldVal !== undefined && Array.isArray(oldVal)) {
            scope.newNote.keywordsNew = makeKeywordsNew()
              .concat(
                scope.newNote.keywordsNew.filter(function(keyword) { return (oldVal.indexOf(keyword.id) === -1); })  // Remove fixed keywords
              );
          }
        }, true);

        // Watch changes for they statusFilter and update the statusFilterObj if it has changed
        scope.$watch('statusFilter', statusFilterObjUpdate, true);

        // Watch changes for the statusFilterObj and reload notes if it changes
        scope.$watch('statusFilterObj', loadNotes, true);


        // Function to add new notes
        scope.createNewNote = function() {
          scope.newNote.editorActive = true;

          // Focus on text editor
          $timeout(function() {
            element.find('.note-editor-new div.ta-bind').focus();
          });
        };

        scope.newEditorCancel = function() {
          scope.newNote = {
            id: -1,
            noteTextNew: '',
            attachmentsNew: [],
            keywordsNew: makeKeywordsNew(),
            noteStatusNew: 'Final',
            editorActive: false
          };
        };

        scope.newEditorSave = function() {
          var myNote = {
            id: -1,
            noteText: scope.newNote.noteTextNew,
            attachments: scope.newNote.attachmentsNew,
            keywords: scope.newNote.keywordsNew,
            noteStatus: scope.newNote.noteStatusNew,
            sourceLink: scope.sourceLink,
            lastAuthor: '(saving)',
            lastModifiedDate: new Date(),
            interactionSummary: { },
            isSaving: true
          };
          scope.notes.push(myNote);

          scope.newEditorCancel();

          notesService.addNote(myNote, noteListenerAdd)
            .then(
              function(res) {
                if(scope.notes.indexOf(myNote) !== -1) {
                  scope.notes[scope.notes.indexOf(myNote)] = res;
                }

                res.isSaving = false;

                // TODO: Remove debug code
                console.log('Does callOnChangeCtrl exist?');
                console.log(callOnChangeCtrl);

                // If a callOnSave controller exists, trigger it
                if(callOnChangeCtrl) {
                  callOnChangeCtrl.triggerSave(res.id, (res.sourceLink ? '#' + res.sourceLink : undefined), notesService.makeNoteIntro(res), res.keywords.map(function(keyword) { return keyword.id; }));
                }

                // Move the impacts if given
                if(scope.showImpacts) {
                  scope.notesImpacts['note-' + res.id.toString()] = scope.notesImpacts.NEW;
                  scope.notesImpacts.NEW = [];
                }

                // Also watch the note, unless the user has disabled this function
                return usersService.getCurrentUserSettings()
                  .then(
                    function(settings) {
                      if(settings.watchNewNote !== 'no') {
                        return usersService.getCurrentUser()
                          .then(
                            function(user) {
                              // We already update our interactionSummary...
                              res.interactionSummary.watch = [ user.id.toString() ];

                              // Now we can actually create the watch
                              return interactionService.addWatch(user.id, 'note', res.id, notesService.makeNoteIntro(res), (res.sourceLink ? '#' + res.sourceLink : undefined));
                            }
                          );
                     }
                    }
                  );
              })
            .catch(
              function(err) {
                // TODO: make error messages appear nicer
                window.alert(err);
              }
            );
        };

        // Initialise new note
        scope.newEditorCancel();


        // Function to edit existing notes
        scope.makeEditorActive = function(note) {
          // TODO: Remove debug code
          console.log('makeEditorActive called.');
          console.log(scope.notes);

          if(note !== undefined) {
            note.noteTextNew = note.noteText;
            note.keywordsNew = note.keywords.slice(0);
            note.attachmentsNew = note.attachments.slice(0);
            note.noteStatusNew = note.noteStatus;
            scope.editorActiveNote = note;

            // Focus on text editor
            $timeout(function() {
              element.find('.note-editor-old div.ta-bind').focus();
            });
          }
        };

        // Function to save changes to note
        scope.editorSave = function(note) {
          if(note !== undefined) {
            // TODO: Do not allow submission of empty notes.

            var oldNote = {
              id: note.id,
              noteText: note.noteText,
              keywords: note.keywords.slice(0),
              attachments: note.attachments,
              noteStatus: note.noteStatus
            };

            note.noteText = note.noteTextNew;
            note.keywords = note.keywordsNew.slice(0);
            note.attachments = note.attachmentsNew;
            note.noteStatus = note.noteStatusNew;
            note.lastModifiedDate = new Date();
            note.isSaving = true;
            scope.editorActiveNote = undefined;

            return notesService.updateNote(note, oldNote)
              .then(
                function() {
                  note.isSaving = false;

                  // TODO: Remove debug code
                  console.log('Does callOnChangeCtrl exist?');
                  console.log(callOnChangeCtrl);

                  // If a callOnSave controller exists, trigger it
                  if(callOnChangeCtrl) {
                    callOnChangeCtrl.triggerSave(note.id, (note.sourceLink ? '#' + note.sourceLink : undefined), notesService.makeNoteIntro(note), note.keywords.map(function(keyword) { return keyword.id; }));
                  }
                },
                function(err) {
                  // TODO: nicer error messages
                  window.alert(err);
                }
              );
          } else {
            return $q.reject();
          }
        };

        // Function to abort editing
        scope.editorCancel = function(note) {
          if(note !== undefined) {
            if(note.id === -1) {
              // New note - remove
              scope.removeNote(note);
              scope.newNoteActive = false;
            } else {
              // Existing note - remove changes
              scope.editorActiveNote = undefined;
              delete note.noteTextNew;
              delete note.keywordsNew;
              delete note.attachmentsNew;
              delete note.noteStatusNew;
            }
          }
        };

        // Function to remove note
        scope.removeNote = function(note) {
          var deleteDialog;

          if(note !== undefined && scope.notes.indexOf(note) !== -1) {
            var myID = note.id;

            var doRemove = function() {
              var deletedNote = scope.notes.splice(scope.notes.indexOf(note), 1)[0];

              if(myID !== -1) {
                notesService.deleteNote(deletedNote).then(
                  function() {
                    // If a callOnSave controller exists, trigger it
                    if(callOnChangeCtrl) {
                      callOnChangeCtrl.triggerDelete(myID);
                    }

                    deleteDialog.close();
                  },
                  function(err) {
                    // TODO: nicer error messages
                    window.alert(err);
                  }
                );
              }
            };

            var doUnlink = function() {
              //  Remove fixed keywords of this note
              note.keywordsNew = note.keywordsNew.filter(function(keyword) {
                return (scope.keywordId.indexOf(keyword.id) === -1);
              });

              // Save changes to this note
              scope.editorSave(note).then(function() {
                // Remove note from our array
                scope.notes.splice(scope.notes.indexOf(note), 1);

                deleteDialog.close();
              });
            };



            // If note is already saved and linked to "foreign" keywords, we ask whether we should just remove the
            // keywords for this note viewer or if we should delete the note globally
            var savedAndForeign = (note.id !== -1 && note.keywordsNew.findIndex(function(keyword) {
                return (scope.keywordId.indexOf(keyword.id) === -1);
              }) !== -1);

            // Show dialog box
            deleteDialog = ngDialog.open({
              template: 'common/directives/notesRemoveDialog.tmpl.html',
              className: 'ngdialog-theme-default',
              data: {
                'note': note,
                'savedAndForeign': savedAndForeign,
                'doRemove': doRemove,
                'doUnlink': doUnlink
              }
            });


          }
        };

        // Gets called when the user selects new files to upload
        scope.filesChanged = function(isNew, elm) {
          var noteScope = angular.element(elm).scope();
          var i;

          // TODO: fix this whole thing and replace it with a proper directive!

          // TODO: disable debug data, but then this will not work anymore (see https://docs.angularjs.org/guide/production#disabling-debug-data)

          if(isNew) {
            // New notes
            for(i = 0;i < elm.files.length; i++) {
              scope.newNote.attachmentsNew.push({
                link: '',
                name: elm.files[i].name,
                newUploadFile: elm.files[i]
              });
            }
          } else {
            // Existing note
            for(i = 0;i < elm.files.length; i++) {
              noteScope.note.attachmentsNew.push({
                link: '',
                name: elm.files[i].name,
                newUploadFile: elm.files[i]
              });
            }
          }

          scope.$apply();
        };

        // Mark attachment for removal
        scope.removeAttachmentToggle = function(attachment) {
          attachment.toBeRemoved = !attachment.toBeRemoved;
        };

        var myToBeAddedID = 0;

        scope.showKeywordRemoveError = false;
        scope.keywordsOnRemoving = function($tag) {
          if(scope.keywordId.indexOf($tag.id) !== -1) {
            scope.showKeywordRemoveError = true;
            setTimeout(function() { scope.showKeywordRemoveError = false; scope.$digest(); }, 1000);
            return false;
          } else {
            return true;
          }
        };

        scope.keywordsOnAdding = function($tag) {
          if($tag.id === undefined) {
            // Set "to be added" ID value.
            $tag.id = "addKeyword/" + myToBeAddedID++;
          }
        };

        scope.keywordsLoadItems = function($query) {
          console.log($query);

          return keywordsService.getAllKeywords().then(
            function(myKeywordList) {
              return myKeywordList.filter(function(d) {
                return (d.name.toUpperCase().indexOf($query.toUpperCase()) > -1);
              });
            }
          ); // ... we fail silently, no worries if keywords are missing.
        };


        // KeywordIDs cannot be removed from the search bar
        scope.keywordbarOnRemoving = function($tag) {
          if(scope.keywordId.indexOf($tag.id) !== -1) {
            scope.showKeywordFilterRemoveError = true;
            setTimeout(function() { scope.showKeywordFilterRemoveError = false; scope.$digest(); }, 1000);
            return false;
          } else {
            return true;
          }
        };

        // Search - get additional results from server when necessary
        var searchAdditionalNotes = function() {
          // TODO: Remove debug code
          console.log('searchAdditionalNotes called. searchKey: ' + scope.searchKey);

          if(scope.searchKey.length < 3) {
            // Meanwhile the search key has been shortened, so we do not get any more notes.
            return $q.resolve();
          }

          if(scope.haveAllNotes) {
            // We already have all notes, so there is no point in trying to get more.
            return $q.resolve();
          }

          if($filter('filter')(scope.notes, scope.searchKey).length > scope.currLimitTo) {
            // We already have enough results to show, so we do not need to go to the server.
            return $q.resolve();
          }

          // How many notes to do we already have?
          var haveNotes = scope.notes.length;

          // Ok, let's get more results from the server.
          return notesService.getNotesForKeywords(scope.keywordFilter.map(function(k) { return k.id; }), getCurrentStatusFilter(), false, false, haveNotes + 200).then(
            function(myNotes) {
              scope.notes = myNotes;

              // Did we get haveNotes + 200 or should we get even more?
              if(scope.notes.length === haveNotes + 200) {
                // Recursively call ourself.
                searchAdditionalNotes();
              } else {
                // We have received all notes.
                scope.haveAllNotes = true;
              }
            },
            function(err) {
              // TODO: Nicer error messaging.
              window.alert(err);
            }
          );
        };


        var doSearchPromise;
        scope.doSearch = function() {
          // TODO: Remove debug code
          console.log('doSearch called. searchKey: ' + scope.searchKey);

          if(doSearchPromise) {
            // Cancel existing timeout
            $timeout.cancel(doSearchPromise);
            doSearchPromise = undefined;
          }

          // Is a search term with at least three characters specified? If yes start server search.
          if(scope.searchKey.length >= 3) {
            // Start getting additional notes if required.
            doSearchPromise = $timeout(searchAdditionalNotes, 1000);
          }
        };


        // Notes paging - do we have more notes than given by limitTo?
        scope.hasMoreNotes = function() {
          // TODO: Remove debug code
          console.log('hasMoreNotes called.');
          console.log($filter('filter')(scope.notes, scope.searchKey));
          console.log(scope.currLimitTo);

          return (scope.currLimitTo ? ($filter('filter')(scope.notes, scope.searchKey).length > scope.currLimitTo) : false);
        };

        // Notes paging - increase currLimitTo by original limitTo and (in the background) pre-fetch additional notes
        scope.showMoreNotes = function() {
          // TODO: Remove debug code
          console.log('Paging - Increasing limitTo from ' + scope.currLimitTo + ' by ' + scope.limitTo + ' to ' + (Number(scope.currLimitTo) + Number(scope.limitTo)) + '.');

          scope.currLimitTo = Number(scope.currLimitTo) + Number(scope.limitTo);

          if(scope.searchKey.length >= 3) {
            // Start getting additional notes to satisfy search term if required.
            return searchAdditionalNotes();
          } else if(!scope.haveAllNotes) {
            // Pre-fetch additional notes if there are more available!
            return notesService.getNotesForKeywords(scope.keywordFilter.map(function(k) { return k.id; }), getCurrentStatusFilter(), false, false, scope.currLimitTo + 3*Number(scope.limitTo)).then(
              function(myNotes) {
                scope.notes = myNotes;

                if(scope.notes.length < scope.currLimitTo + 3*Number(scope.limitTo)) {
                  // We did not get the number of notes we asked for, so there are not that many.
                  // => We have received all of them!
                  scope.haveAllNotes = true;
                }
              },
              function(err) {
                // TODO: Nicer error messaging.
                window.alert(err);
              }
            );
          } else {
            // Nothing do to.
            return $q.resolve();
          }
        };

        // Watch for notes referenced in the URL hash
        // -> maybe we need to load more notes to retrieve the given ID
        if(zoomKeyCtrl) {
          zoomKeyCtrl.onZoom(function(zoomHash) {
            var noteId = parseInt(zoomHash, 10);

            // Don't do anything if there is no valid notes ID given
            if(isNaN(noteId)) { return; }

            // Reset the search if any
            scope.searchKey = '';

            // Is the noteId already visible? Then we can just return...
            var noteIdx = scope.notes.findIndex(function(note) { return note.id === noteId; });
            if(noteIdx !== -1 && noteIdx <= scope.currLimitTo) {
              return noteId.toString();
            }

            // The noteId is not visible, so we need to load more notes...
            var recursiveLoader = function() {
              // TODO: Remove debug code
              console.log('recursiveLoader running...');
              console.log([scope.currLimitTo, scope.haveAllNotes, noteId, scope.notes]);

              if(scope.haveAllNotes && scope.currLimitTo >= scope.notes.length) {
                return undefined;
              } else {
                return scope
                  .showMoreNotes()
                  .then(function() {
                    // Do we have the note loaded and visible? Then we can return, otherwise we start again.
                    var noteIdx = scope.notes.findIndex(function(note) { return note.id === noteId; });
                    if(noteIdx !== -1 && noteIdx <= scope.currLimitTo) {
                      return noteId.toString();
                    }

                    // Start again.
                    return recursiveLoader();
                  });
              }
            };

            return recursiveLoader();
          });
        }

      },

      templateUrl: 'common/directives/dashboardNotes.tmpl.html'
    };

  }])


  /**
   * @ngdoc directive
   * @name notesDirectives.dashboardNotesViewer
   * @restrict E
   * @description
   *
   * This directive displays a simple list of notes given by their ids in the `notesIdArr`.
   *
   * @scope
   * @param {Array}  notesIdArr   An array of notes IDs which should be displayed by this directive.
   *
   */

  .directive('dashboardNotesViewer', ['notesService', function(notesService) {

    return {
      restrict: 'E',
      replace: 'true',
      scope: {
        notesIdArr: '='
      },

      link: function (scope) {
        scope.errMessage = undefined;
        scope.notesAreLoading = false;
        scope.makeNoteIntro = notesService.makeNoteIntro;

        scope.notes = [];

        // We need to retrieve the notes from the server
        scope.notesAreLoading = true;
        notesService.getNotesById(scope.notesIdArr)
          .then(
            function(notes) {
              scope.notes = notes;
              scope.notesAreLoading = false;
            },
            function(err) {
              scope.errMessage = err;
            },
            function(notes) {
              scope.notes = notes;  // Notification in-between
            }
          );

        // That's it.
      },

      templateUrl: 'common/directives/dashboardNotesViewer.tmpl.html'
    };
  }])



  /**
   * @ngdoc directive
   * @name socialDirectives.dashboardNewsFeed
   * @restrict E
   * @description
   *
   * This directive takes an array of keyword objects and prints them in a nicely formatted, comma-separated list.
   * If `enableEye` is set to `true`, the keywords can be directly subscribed to and unsubscribed from within that
   * list (by displaying a small eye next to each keyword entry).
   *
   * @scope
   * @param {Array}     keywords         The array of keyword objects to be displayed.
   * @param {boolean=}  enableEye        If set to `true`, the user will have the option to subscribe to and 
   *                                     unsubscribe from any keyword in the list.
   *
   */

  .directive('keywordsList', [ function() {
    return {
      restrict: 'E',
      scope: {
        keywords: '=',
        enableEye: '=?'
      },

      link: function() {
        // That's it.
      },

      templateUrl: 'common/directives/keywordsList.tmpl.html'
    };
  }])



;

