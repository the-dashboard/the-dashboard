'use strict';

/**
 * @ngdoc overview
 * @name objectExplorerDirectives
 * @description
 *
 * This module provides directives for displaying an "object explorer". An object explorer
 * shows a graphical depiction of a set of categories and allows the user to display lists
 * of filtered objects pertaining to that category.
 *
 */

angular.module('objectExplorerDirectives', ['gettext', 'ui.grid', 'ui.grid.resizeColumns', 'objectExplorerServices'])



/**
 * @ngdoc directive
 * @name objectExplorerDirectives.objectExplorer
 * @restrict E
 * @description
 *
 * This directive displays an object explorer. An object explorer shows a graphical depiction of a set of
 * elements and allows the user to display lists of filtered objects pertaining to these elements.
 *
 * To achieve this, the objects (which have a name, an optional link and a set of details) need to be manually
 * mapped to the elements first. See {@link objectExplorerDirectives.objectExplorerMapping} for a directive that
 * supports this mapping process by the user.
 *
 * @scope
 * @param {number} width The width of the explorer in pixels.
 * @param {number} height The height of the explorer overview in pixels (the explorer will expand vertically
 *                        to display the lists selected by the user).
 * @param {string} mappingKey  A key that identifies this object explorer among all other object explorers in
 *                             the dashboard. Object mappings will be referenced to the combination of this
 *                             mappingKey and the element key of the element selected by the user (see below).
 * @param {Array} elements The elements the object explorer will display and let the user choose among.
 *                         Each element is an object containing a value `key` that is used for mapping
 *                         the objects and a value `display` that is used as the description to display
 *                         that object.
 * @param {string} viewType Specifies how the explorer elements should be displayed. Currently, only
 *                          `process` is supported which aligns the elements in a process schematic.
 * @param {Array} views An array of objects which identify the individual views the explorer should
 *                      present to the user. See TBD TODO TBD for an explanation of these view objects.
 * @param {string=} sourceLink  Will be passed on to directives that use this link to create "back to
 *                              source" links for new elements (e.g. the Notes directive).
 */

  .directive('objectExplorer', ['$timeout', 'd3', 'objectExplorerService', function($timeout, d3, objectExplorerService) {

    return {
      restrict: 'E',
      replace: 'true',

      require: '?^zoomKey',

      scope: {
        width: '=',
        height: '=',
        mappingKey: '=',
        elements: '=',
        viewType: '=',
        views: '=',
        sourceLink: '@?'
      },

      link: function(scope, element, attrs, zoomKeyCtrl) {
        // Helper variable for "no view" (to avoid undefined errors)
        var noView = { icon: undefined, name: undefined, type: undefined, options: {} };

        // Initialize scope variables that set which views and view for which element to show
        scope.activeElement = undefined;
        scope.activeView = noView;

        // Does user have mapping rights?
        scope.hasMappingRights = false;
        objectExplorerService.hasMappingRights()
          .then(function(_hasMappingRights) {
            scope.hasMappingRights = _hasMappingRights;
          });

        // Default values for some of the options
        if(scope.viewType !== 'process') {
          scope.viewType = 'process';
        }

        if(typeof scope.width !== 'number') {
          scope.width = parseInt(scope.width);
        }

        if(!scope.width) {
          scope.width = 900;
        }

        if(typeof scope.height !== 'number') {
          scope.height = parseInt(scope.height);
        }

        if(!scope.height) {
          scope.height = Math.round(scope.width * 9 / 16);
        }


        // Obtain the SVG element defined in the template
        var svg = d3.select(element[0]).select('.object-explorer-elements');


        // Functionality to (re-)draw the elements
        var drawElements = function(svg, scope) {
          // Update height and width
          svg
            .attr('width', scope.width)
            .attr('height', scope.height)
            .style('margin-left', Math.floor((870 - scope.width)/2).toString() + 'px');

          // Distance between elements
          // TODO: needs to be changed if there are other views than "process"
          var elementsDistance = -1 * Math.floor(scope.width / scope.elements.length / 10);

          // Calculate the width of each element
          var elementWidth = Math.floor((scope.width + elementsDistance) / scope.elements.length - elementsDistance);

          console.log('Drawing object explorer:');
          console.log([scope.width, scope.height, scope.elements.length, elementsDistance, elementWidth ]);

          // Path for each element
          var path = '';
          var textBegin = 0;
          var textEnd = 0;
          if(scope.viewType === 'process') {
            var endArrow1 = Math.floor(elementWidth * 1 / 6);
            var beginArrow2 = Math.floor(elementWidth * 5 / 6);
            var endArrow2 = elementWidth;
            var middleHeight = Math.floor(scope.height / 2);
            var bottomHeight = scope.height;

            path = 'M0 0 L' + beginArrow2 + ' 0 L' + endArrow2 + ' ' + middleHeight + ' L' + beginArrow2 + ' ' + bottomHeight + ' L0 ' + bottomHeight + ' L' + endArrow1 + ' ' + middleHeight + ' Z';
            textBegin = Math.round(1.3 * endArrow1);
            textEnd = beginArrow2;

            // TODO - remove debug
            console.log('Path for elements: ' + path);
          }

          // Ok, let's display the elements
          var svgElements = svg.selectAll('g').data(scope.elements);

          // Create new elements
          var enteredElements = svgElements
            .enter()
            .append('g')
            .classed('explorer-elements', true)
            .classed('explorer-element-active', function(d) { return (scope.activeElement && scope.activeElement === d.key); });

          enteredElements
            .append('path')
            .attr('d', path);

          enteredElements
            .append('text')
            .attr('transform', 'translate(' + textBegin + ',' + Math.round(scope.height/2) + ')')
            .attr('y', 0)
            .attr('dy', 0);

          // Remove elements that are no longer needed
          svgElements
            .exit()
            .remove();

          // Align the elements sequentially
          svgElements
            .attr('transform', function(d, i) { return 'translate(' + (i*(elementWidth+elementsDistance))+ ',0)'; });

          // Set the text of the elements
          svgElements.selectAll('text')
            .text(function(d) { return d.display; })
            .call(d3.wrapHelper, textEnd - textBegin);

          // Set classes on the elements based on their conditional formatting
          svgElements
            .each(function(d) {
              // TODO: Remove debug code
              console.log('Do we have conditional formatting for ' + d.key + '?');

              if(d.conditionalFormat && Array.isArray(d.conditionalFormat) && d.conditionalFormat.length > 0) {
                var el = d3.select(this);

                // TODO: Remove debug code
                console.log('We have conditional formatting for ' + d.key + ':');
                console.log(d.conditionalFormat);

                d.conditionalFormat.forEach(function(format) {
                  // We need to get the mapped objects for this conditional formatting
                  // description

                  // TODO: Remove debug code
                  console.log('Getting mapped objects for coditional format code (' + d.key + ', ' + format.type + ')...');

                  // Why are we doing it convoluted like this? Because we are implementing
                  // an "OR" logic: Only if *all* formatters for this class evaluate to
                  // false will we set the class to false. If at least one formatter
                  // evaluates to true, we will set the class to true.
                  el.classed('object-explorer-format-' + format.style, false);

                  objectExplorerService.getMappedObjects(scope.mappingKey, d.key, format.type).then(
                    function(mappedObjects) {
                      // TODO: Remove debug code
                      console.log('Found mapped objects for conditional formatting check (' + d.key + ', ' + format.type + ').');

                      var foundIdx = mappedObjects.findIndex(function(obj) {
                        // We need to check all requirements (AND) given in the columnFilter
                        // array
                        var hit = true;

                        format.columnFilter.forEach(function(filter) {
                          var myHit;

                          // TODO: Remove debug code
                          console.log('Filtering entry with filter - BEFORE:');
                          console.log([obj, filter, hit]);

                          var relevantColumn;
                          if(filter.column === 'title' || filter.column === 'description' || filter.column === 'link') {
                            relevantColumn = obj[filter.column];
                          } else {
                            relevantColumn = obj.additionalColumns[filter.column];
                          }

                          if(relevantColumn === undefined) {
                            // If column does not exist, we assume it doesn't match and we log a warning.
                            console.log('WARNING: Conditional formatting on column "' + filter.column + '": Column does not exist.');
                            myHit = false;
                          } else {
                            switch(filter.compare) {
                              case 'equals':
                                myHit = (relevantColumn === filter.value);
                                break;

                              case 'contains':
                                myHit = (relevantColumn.indexOf(filter.value) >= 0);
                                break;

                              case 'geq':
                                myHit = (relevantColumn >= filter.value);
                                break;

                              case 'leq':
                                myHit = (relevantColumn <= filter.value);
                                break;
                            }
                          }

                          // TODO: Remove debug code
                          console.log('Filtering entry with filter - AFTER:');
                          console.log([obj, filter, hit, myHit]);

                          hit = (hit && (filter.invert ? !myHit : myHit));
                        });

                        return hit;
                      });

                      if(foundIdx > -1) {
                        el.classed('object-explorer-format-' + format.style, true);
                      }

                    },
                    function(err) {
                      console.log('ERROR occured when retrieving mapped objects (key "' + d.key + '", type "' + d.type + '" for conditional formatting:');
                      console.log(err);
                    }
                  );
                });
              }
            });

          // Make the elements clickable
          svgElements.on('click', function(d) {
            scope.$apply(function() {
              scope.activeElement = d.key;
              updateActiveElement(svg, scope);
            });
          });
        };

        // Functionality to highlight active element
        var updateActiveElement = function(svg, scope) {
          svg.selectAll('g')
            .classed('explorer-element-active', function(d) { return (scope.activeElement && scope.activeElement === d.key); });
        };

        // Function to process clicks on the view icons defined in scope.views
        scope.activateView = function(viewName) {
          if(viewName === undefined) {
            scope.activeView = noView;
          } else {
            scope.activeView = scope.views.find(function (d) { return (d.name === viewName); });

            if(scope.activeView === undefined) {
              // View name not found...
              scope.activeView = noView;
            }
          }
        };

        // Function to activate mapping mode for mapping objects to elements
        var preMappingView;
        scope.enterMappingMode = function(objectType, addColumns) {
          // Store prior view for closing mapping mode, but don't overwrite existing preMappingView
          if(!preMappingView) {
            preMappingView = scope.activeView;
          }

          // Change activeView to mapping mode view
          scope.activeView = { icon: undefined, name: 'Mapping', type: 'objectMapping', options: { type: objectType, addColumns: addColumns } };
        };

        scope.closeMappingMode = function() {
          if(preMappingView) {
            scope.activeView = preMappingView;
            preMappingView = undefined;
          }
        };

        // Initialize the object explorer by drawing the elements graphic
        drawElements(svg, scope);


        // Helper function to re-draw the conditional formatting when the mapping gets updated
        var mappingUpdatedTimeoutPromise;
        scope.mappingUpdated = function() {
          // TODO: Remove debug code
          console.log('mappingUpdated called. Re-drawing (debounced)...');

          if(mappingUpdatedTimeoutPromise) {
            $timeout.cancel(mappingUpdatedTimeoutPromise);
          }

          mappingUpdatedTimeoutPromise = $timeout(function() { drawElements(svg, scope); }, 3000);
        };

        // Register a zoomKey listener that gets triggered when the URL specifies this object
        // explorer => this will set activeView and activeElement

        // Listener that is being called when a zoomKey has been given by the user
        // in the URL hash
        var oldZoomLink;
        var zoomListener = function(zoomLink) {
          // Do not jump again if we already jumped
          if(oldZoomLink && zoomLink === oldZoomLink) {
            return;
          }

          var zoomMatch = zoomLink.match(/^([^\/]+)\/([^\/]+)\/?([0-9]*)$/);
          oldZoomLink = zoomLink;

          // TODO: Remove debug code
          console.log('zoomListener for Object Explorer called: ' + zoomLink);
          console.log(zoomMatch);

          // Do nothing if link is incomplete
          if(zoomMatch === null) {
            return;
          }

          // First element of the link is the active element
          if(zoomMatch[1] && zoomMatch[1] !== scope.activeElement) {
            scope.activeElement = zoomMatch[1];
            updateActiveElement(svg, scope);
          }

          // Second element of the link is the active view
          scope.activateView(zoomMatch[2]);

          // Jump to the anchor of this object explorer appended with the notes ID if given
          return (zoomMatch[3] ? zoomMatch[3] : '');
        };

        if(zoomKeyCtrl) {
          zoomKeyCtrl.onZoom(zoomListener);
        }

        // We will re-draw the elements SVG whenever the "elements" or
        // the "height" or the "width" changes.
        scope.$watch('elements', function(newVal, oldVal, scope) {
          if(scope.elements && scope.elements.length > 0) {
            drawElements(svg, scope);
          }
        }, true);

        scope.$watch('width', function(newVal, oldVal, scope) {
          if(scope.elements && scope.elements.length > 0) {
            drawElements(svg, scope);
          }
        }, true);

        scope.$watch('height', function(newVal, oldVal, scope) {
          if(scope.elements && scope.elements.length > 0) {
            drawElements(svg, scope);
          }
        }, true);


        // Function to replace [ELEMENT] with the active element
        var replaceKeywords = function(keyword, element) {
          if (element !== undefined) {
            if (Array.isArray(keyword)) {
              // Keyword can also be an array of keyword IDs - we replace [ELEMENT] in each element
              return keyword.map(function (k) {
                return k.replace('[ELEMENT]', element);
              });
            } else {
              return [keyword.replace('[ELEMENT]', element)];
            }
          } else {
            // Return unmodified if no element is set
            return keyword;
          }
        };

        scope.$watch('activeView', function(newVal, oldVal, scope) {
          if (newVal.type === 'notes') {
            // Set active view keywords
            scope.activeViewKeywords = replaceKeywords(newVal.options.keywordID, scope.activeElement);
          }
        });

        scope.$watch('activeElement', function(newVal, oldVal, scope) {
          if (scope.activeView && scope.activeView.type === 'notes') {
            // Set active view keywords
            scope.activeViewKeywords = replaceKeywords(scope.activeView.options.keywordID, newVal);
          }
        });


        // TODO: We need to re-draw this whenever a new mapping has been set,
        //       as this might change the conditional formatting!

      },

      templateUrl: 'common/directives/objectExplorer.tmpl.html'
    };

    }])


  /**
   * @ngdoc directive
   * @name objectExplorerDirectives.objectExplorerTable
   * @restrict E
   * @description
   *
   * This directive displays a single table of objects which have been mapped to the element
   * specified by `showElement` and are of the type specified by `showType`.
   *
   * By default, the directive will only display the title of each object. If the `link`
   * value of the object is set, the title will be turned into a hyperlink. If the
   * `addColumns` array is specified, additional columns will be displayed, one for each
   * element of that array.
   *
   * Each element of `addColumns` must be an object with the `name` property (which specifies
   * the title the column gets in the table) and the `field` property (which specifies the
   * data field from the `additionalColumnsJSON` property of each object which should be
   * displayed in that column). You can also specify the `width` of the column (as
   * percentage or fixed value).
   *
   * @scope
   * @param {string} mappingKey      The mapping key for which mapped objects should be retrieved - mappings
   *                                 must match on both the `mappingKey` and the `showElement` (see below)
   *                                 to be displayed in the table.
   * @param {string} showElement     The element for which mapped objects should be retrieved.
   * @param {string} showType        The type of objects which should be retrieved.
   * @param {Array=} addColumns      An optional array of column definitions which should be
   *                                 displayed in addition to the title of the found objects.
   * @param {integer=} lineHeight    If given, the height of each line in the table in pixels (if not
   *                                 given, any description of objects will not be visible, as default
   *                                 line height only displays one line of text).
   */

  .directive('objectExplorerTable', ['gettext', 'objectExplorerService', function(gettext, objectExplorerService) {

    return {
      restrict: 'E',
      scope: {
        mappingKey: '=',
        showElement: '=',
        showType: '=',
        addColumns: '=',
        lineHeight: '=?'
      },

      link: function(scope) {
        scope.isLoading = true;
        scope.errMessage = undefined;
        scope.showGrid = false;

        scope.objectGridOptions = {
          enableSorting: true,
          enableFiltering: true,
          enableColumnResizing: true,
          columnDefs: [ { name: 'Title', field: 'title', cellTemplate: '<div class="ui-grid-cell-contents"><a href="{{row.entity.link}}" target="_blank" ng-if="row.entity.link">{{row.entity.title}}</a><span ng-if="!row.entity.link">{{row.entity.title}}</span><span ng-if="row.entity.description" class="object-explorer-object-description"> {{row.entity.description}}</span></div>' } ],
          data : [ ]
        };

        // If given, set row height
        if(scope.lineHeight) {
          scope.objectGridOptions.rowHeight = scope.lineHeight;
        }

        // TODO - Remove debug code...
        console.log('LINK on Table called: showElement = ' + scope.showElement + ', showType = ' + scope.showType + ', addColumns = ' + scope.addColumns);
        console.log(scope.objectGridOptions);
        console.log('Done.');


        // Update columnDefs helper function
        var updateColumns = function(addColumns) {
          // Start with base columns that will be shown always
          var newColumns = [ { name: 'Title', field: 'title', cellTemplate: '<div class="ui-grid-cell-contents"><a href="{{row.entity.link}}" target="_blank" ng-if="row.entity.link">{{row.entity.title}}</a><span ng-if="!row.entity.link">{{row.entity.title}}</span><span ng-if="row.entity.description" class="object-explorer-object-description"> {{row.entity.description}}</span></div>' } ];

          // Is addColumns set?
          if(!Array.isArray(addColumns)) {
            addColumns = [];
          }

          // Add aditional columns to be shown
          addColumns.forEach(function(col) {
            newColumns.push({
              name: col.name,
              displayName: col.name,
              field: 'additionalColumns.' + col.field,
              width: col.width
            });
          });

          // Update object grid definition
          scope.objectGridOptions.columnDefs = newColumns;
        };


        // Update the mapped objects to be displayed helper function
        var updateObjects = function(showElement, showType) {
          // We are updating...
          scope.isLoading = true;
          scope.errMessage = '';
          scope.objectGridOptions.data = [];

          // TODO - Remove debug code
          console.log('updateObjects called. showElement = ' + showElement + ', showType = ' + showType);

          // Has the user selected an element?
          if(showElement === undefined || showElement === '') {
            scope.isLoading = false;
            scope.showGrid = false;
            scope.errMessage = gettext('You need to select an element first.');
          } else {
            // Let's retrieve and display the mapped objects.
            objectExplorerService.getMappedObjects(scope.mappingKey, showElement, showType).then(
              function(mappedObjects) {
                scope.objectGridOptions.data = mappedObjects;
                scope.isLoading = false;
                scope.errMessage = '';
                scope.showGrid = true;
              },
              function(err) {
                scope.isLoading = false;
                scope.errMessage = err;
                scope.showGrid = false;
              }
            );
          }
        };

        // We initialize them.
        updateColumns(scope.addColumns);
        updateObjects(scope.showElement, scope.showType);

        // We watch for changes on these values
        scope.$watch('addColumns', function(newVal, oldVal, scope) {
          updateColumns(scope.addColumns);
        }, true);

        scope.$watch('showElement', function(newVal, oldVal, scope) {
          updateObjects(scope.showElement, scope.showType);
        }, true);

        scope.$watch('showType', function(newVal, oldVal, scope) {
          updateObjects(scope.showElement, scope.showType);
        }, true);

        scope.$on('$destroy', function() {
          // TODO: Remove debug code
          console.log('DESTROY called on our table directive for showElement = ' + scope.showElement + ', showType = ' + scope.showType);
        });

        // That's it.
      },

      templateUrl: 'common/directives/objectExplorerTable.tmpl.html'
    };

  }])


  /**
   * @ngdoc directive
   * @name objectExplorerDirectives.objectExplorerMapping
   * @restrict E
   * @description
   *
   * This directive displays a large table of objects which can be mapped to the active element
   * (as given in the `activeElement` attribute) by clicking on the `Mapping` column.
   *
   * By default, the directive will only display the title of each object. If the `link`
   * value of the object is set, the title will be turned into a hyperlink. If the
   * `addColumns` array is specified, additional columns will be displayed, one for each
   * element of that array.
   *
   * Each element of `addColumns` must be an object with the `name` property (which specifies
   * the title the column gets in the table) and the `field` property (which specifies the
   * data field from the `additionalColumnsJSON` property of each object which should be
   * displayed in that column). You can also specify the `width` of the column (as
   * percentage or fixed value).
   *
   * In addition, this directive displays a calculated column which highlights (and allows filtering
   * for) objects which have no mapping yet as well as the `Mapping` column which shows all entries
   * this object is mapped to.
   *
   * @scope
   * @param {string} mappingKey      Any mappings defined by this mapping directive will be mapped to
   *                                 the combination of this `mappingKey` (which normally uniquely
   *                                 identifies an object explorer in the Dashboard) and the
   *                                 `activeElement` (see below).
   * @param {string} activeElement   The element which should be mapped to clicked objects.
   * @param {string} showType        The type of objects which should be retrieved.
   * @param {Array=} addColumns      An optional array of column definitions which should be
   *                                 displayed in addition to the title of the found objects.
   * @param {function} onMap         An optional callback function that will be called whenever the
   *                                 mapping changes.
   * @param {integer=} lineHeight    If given, the height of each line in the table in pixels (if not
   *                                 given, any description of objects will not be visible, as default
   *                                 line height only displays one line of text).
   */

  .directive('objectExplorerMapping', ['gettext', 'objectExplorerService', function(gettext, objectExplorerService) {

    return {
      restrict: 'E',
      scope: {
        mappingKey: '=',
        activeElement: '=',
        showType: '=',
        addColumns: '=',
        onMap: '=?',
        lineHeight: '=?'
      },

      link: function(scope) {
        scope.isLoading = true;
        scope.errMessage = undefined;
        scope.showGrid = false;

        scope.mappingGridOptions = {
          enableSorting: true,
          enableFiltering: true,
          enableColumnResizing: true,
          columnDefs: [ { name: 'Title', field: 'title', cellTemplate: '<div class="ui-grid-cell-contents"><a href="{{row.entity.link}}" target="_blank" ng-if="row.entity.link">{{row.entity.title}}</a><span ng-if="!row.entity.link">{{row.entity.title}}</span><span ng-if="row.entity.description" class="object-explorer-object-description"> {{row.entity.description}}</span></div>' } ],
          data : [ ]
        };

        // If given, set row height
        if(scope.lineHeight) {
          scope.mappingGridOptions.rowHeight = scope.lineHeight;
        }

        // TODO - Remove debug code...
        console.log('LINK on Mapping called: activeElement = ' + scope.activeElement + ', showType = ' + scope.showType + ', addColumns = ' + scope.addColumns);
        console.log(scope.mappingGridOptions);
        console.log('Done.');


        // Update columnDefs helper function
        var updateColumns = function(addColumns) {
          // Start with base columns that will be shown always
          var newColumns = [ { name: 'Title', field: 'title', cellTemplate: '<div class="ui-grid-cell-contents"><a href="{{row.entity.link}}" target="_blank" ng-if="row.entity.link">{{row.entity.title}}</a><span ng-if="!row.entity.link">{{row.entity.title}}</span><span ng-if="row.entity.description" class="object-explorer-object-description"> {{row.entity.description}}</span></div>' } ];

          // Is addColumns set?
          if(!Array.isArray(addColumns)) {
            addColumns = [];
          }

          // Add aditional columns to be shown
          addColumns.forEach(function(col) {
            newColumns.push({
              name: col.name,
              displayName: col.name,
              field: 'additionalColumns.' + col.field,
              width: col.width
            });
          });

          // Add "unmapped" and "Mapping" columns
          newColumns.push({ name:'Unmapped', field: 'unmapped', width: 50 });
          newColumns.push({ name:'Mapping', field: 'mapping', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.mapping}} <i class="fa fa-trash icon-as-button" title="{{ \'Remove mapping\' | translate }}" ng-show="row.entity.mapping !== \'\' && !row.entity.isSaving" ng-click="grid.appScope.removeMapping(row.entity, row.entity.mapping)"></i> <i class="fa fa-plus icon-as-button" title="{{ \'Add mapping\' | translate }}" ng-click="grid.appScope.setMapping(row.entity)"></i></div>' });
          newColumns.push({ name:'Saving', displayName: ' ', enableColumnMenu: false, enableFiltering: false, enableSorting: false, field: 'isSaving', cellTemplate: '<div class="ui-grid-cell-contents"><span ng-if="row.entity.isSaving" class="loadingIndicatorSmall">&nbsp;&nbsp;&nbsp;</span></div>', width: 50 });

          // Update object grid definition
          scope.mappingGridOptions.columnDefs = newColumns;
        };


        // Update the mapped objects to be displayed helper function
        var updateObjects = function(showType) {
          // We are updating...
          scope.isLoading = true;
          scope.errMessage = '';
          scope.mappingGridOptions.data = [];

          // TODO - Remove debug code
          console.log('updateObjects for Mapping called. showType = ' + showType);

          // Let's retrieve and display the objects of the given type.
          objectExplorerService.getObjectsByType(showType).then(
            function(objectsByType) {

              // Amend objects with unmapped column, expand mappings to multiple rows
              var gridData = [];

              objectsByType.forEach(function(d) {
                d.isSaving = 0;

                if(!Array.isArray(d.mappings) || d.mappings.length === 0) {
                  d.unmapped = 'X';
                  d.mapping = '';
                  gridData.push(d);  // by reference
                } else {
                  d.unmapped = '';

                  d.mappings.forEach(function(mapping) {
                    d.mapping = mapping;
                    gridData.push(Object.assign({}, d));  // shallow copy
                  });
                }
              });
              // Set data element
              scope.mappingGridOptions.data = gridData;

              scope.isLoading = false;
              scope.errMessage = '';
              scope.showGrid = true;
            },
            function(err) {
              scope.isLoading = false;
              scope.errMessage = err;
              scope.showGrid = false;
            }
          );
        };

        // Helper function for interactivity when clicking on Mapping column
        scope.setMapping = function(entity) {
          // Do we have an activeElement? Otherwise display error message.
          if(scope.activeElement === undefined) {
             window.alert(gettext('You need to select an element to which this object should be mapped.'));
             return;
          }

          // Is this entity already mapped to this element? In this case do nothing.
          if(entity.mappings.indexOf(scope.activeElement) > -1) {
            return;
          }

          // Does the entity have no mapping? Then we need to set unmapped to false and the individual mapping to
          // the new value.
          var newEntry;
          if(entity.unmapped === 'X') {
            entity.unmapped = '';
            entity.mapping = scope.activeElement;
            entity.mappings = [ scope.activeElement ];

            // newEntry is the old entry, as we do not need to add a line to our data grid
            newEntry = entity;
          } else {
            // Otherwise we add this mapping.
            entity.mappings.push(scope.activeElement);

            // We also need to clone the element in the data array.
            var indexOfElement = scope.mappingGridOptions.data.indexOf(entity);
            newEntry = Object.assign({}, entity);

            // Remove ui-grid internal hashKey
            delete newEntry.$$hashKey;

            // Set mapping
            newEntry.mapping = scope.activeElement;

            // TODO - Remove debug code
            console.log('Old data element:');
            console.log(entity);
            console.log('Adding new data element: ');
            console.log(newEntry);

            // Insert into data
            scope.mappingGridOptions.data.splice(indexOfElement+1, 0, newEntry);
          }

          // Store in database
          newEntry.isSaving++;

          objectExplorerService.addMapping(entity.id, scope.mappingKey, scope.activeElement)
            .then(
              function() {
                newEntry.isSaving--;
                if(scope.onMap) { scope.onMap(); }
              },
              function(err) {
                // TODO - better, more beautiful errors
                window.alert(gettext('An error occured while trying to save the new mapping: ') + err);
                newEntry.isSaving--;
              }
            );

        };

        // Helper function to remove a mapping when clicking on the trash
        scope.removeMapping = function(entity, mapping) {
          var savingEntity;
          if(!Array.isArray(entity.mappings) || entity.mappings.indexOf(mapping) === -1) {
            // Mapping does not exist, just return.
            return;
          }

          // Remove mapping from mappings array.
          entity.mappings.splice(entity.mappings.indexOf(mapping), 1);

          // Is another mapping left?
          if (entity.mappings.length >= 1) {
            // We need to remove the line corresponding to the clicked entity.
            var indexOfElement = scope.mappingGridOptions.data.indexOf(entity);
            scope.mappingGridOptions.data.splice(indexOfElement, 1);

            // The "saving" indicator should be displayed on a remaining entry for that object.
            if (indexOfElement === 0 || scope.mappingGridOptions.data[indexOfElement - 1].id !== entity.id) {
              // The remaining element must be *behind* the deleted one.
              savingEntity = scope.mappingGridOptions.data[indexOfElement];
            } else {
              savingEntity = scope.mappingGridOptions.data[indexOfElement - 1];
            }
          } else {
            // No mapping is left, so we need to set the element to unmapped (and don't remove any line)
            entity.unmapped = 'X';
            entity.mapping = '';
            savingEntity = entity;
          }

          // Store in database
          savingEntity.isSaving++;

          objectExplorerService.removeMapping(entity.id, scope.mappingKey, mapping)
            .then(
              function() {
                savingEntity.isSaving--;
                if(scope.onMap) { scope.onMap(); }
              },
              function(err) {
                // TODO - better, more beautiful errors
                window.alert(gettext('An error occured while trying to remove the mapping: ') + err);
                savingEntity.isSaving--;
              }
            );

        };

        // We initialize the table.
        updateColumns(scope.addColumns);
        updateObjects(scope.showType);

        // We watch for changes on these values
        scope.$watch('addColumns', function(newVal, oldVal, scope) {
          updateColumns(scope.addColumns);
        }, true);

        scope.$watch('showType', function(newVal, oldVal, scope) {
          updateObjects(scope.showType);
        }, true);


        // That's it.
      },

      templateUrl: 'common/directives/objectExplorerMapping.tmpl.html'
    };

  }])
;

