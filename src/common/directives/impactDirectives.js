'use strict';

/**
 * @ngdoc overview
 * @name impactDirectives
 * @description
 *
 * This module provides directives for displaying "impact flags". Impact flags can be attached to various elements in
 * the dashboard (principally notes) and depict the impact of a given note (e.g. "increasing risk", "decreasing risk",
 * "red flag"). This impact can optionally be assigned to a specific risk item specified in a
 * {@link riskTrackerDirectives.riskTracker `riskTracker`} directive.
 *
 */

angular.module('impactDirectives', ['gettext', 'ngDialog', 'risksServices', 'helperDirectives', 'miscServices'])

/**
 * @ngdoc directive
 * @name impactDirectives.callOnChange
 * @restrict A
 * @description
 *
 * This directive provides a controller that can be used to communicate with an impactManager, triggering saves
 * of the impacts when the source object is being saved.
 *
 * It provides a controller that provides the following functions:
 *  * `triggerSave(sourceId, sourceKeywords)` triggers a save of the impacts, associating them with the given `sourceId`
 *     and updating the `sourceKeywords` if necessary
 *  * `onSave(callback)` allows to register a callback function that is called when `triggerSave` runs
 *  * `removeOnSave(callback)` removes the callback function from the callback register
 *  * `triggerDelete()` triggers a removal of all impact flags as the source element is being removed
 *  * `onDelete(callback)` allows to register a callback function that is called when `triggerDelete` runs
 *  * `removeOnDelete(callback)` removes the callback function from the callback register
 *
 *
 *
 */

  .directive('callOnChange', function() {
    var callbacksSave = [];
    var callbacksDelete = [];

    return {
      restrict: 'A',
      controller: function callOnChangeController() {
        this.triggerSave = function(sourceId, sourceUrl, sourceDescription, sourceKeywords) {
          // TODO: Remove debug code
          console.log('Running callback with sourceID = ' + sourceId + ', sourceUrl = ' + sourceUrl + ', sourceDescription = ' + sourceDescription + ', sourceKeywords = ' + sourceKeywords);

          callbacksSave.forEach(function(callback) {
            console.log('Calling callback: ');
            console.log(callback);
            callback(sourceId, sourceUrl, sourceDescription, sourceKeywords);
          });
        };

        this.onSave = function(callback) {
          // Avoid adding the same function twice
          if(callbacksSave.indexOf(callback) === -1) {
            // TODO: Remove debug code
            console.log('Registering onSave callback.');

            callbacksSave.push(callback);
          }
        };

        this.removeOnSave = function(callback) {
          // TODO: Remove debug code
          console.log('Removing onSave callback.');

          var removeIndex = callbacksSave.indexOf(callback);

          if(removeIndex >= 0) {
            callbacksSave.splice(removeIndex, 1);
          }
        };

        this.triggerDelete = function(sourceId) {
          callbacksDelete.forEach(function(callback) {
            callback(sourceId);
          });
        };

        this.onDelete = function(callback) {
          // Avoid adding the same function twice
          if(callbacksDelete.indexOf(callback) === -1) {
            // TODO: Remove debug code
            console.log('Registering onDelete callback.');

            callbacksDelete.push(callback);
          }
        };

        this.removeOnDelete = function(callback) {
          var removeIndex = callbacksDelete.indexOf(callback);

          if(removeIndex >= 0) {
            callbacksDelete.splice(removeIndex, 1);
          }
        };

      }
    };
  })



/**
 * @ngdoc directive
 * @name impactDirectives.impactManager
 * @restrict E
 * @description
 *
 * This directive allows a user to attach new as well as edit and remove existing impact flags on a given source
 * element, specified by its `sourceEndpoint` and `sourceId`.
 *
 * @scope
 * @param {Array=}    impacts                   An array pointing to the impacts for this element. If not given, a new
 *                                              array will be initialized. The given array will be modified when the
 *                                              flags are modified.
 * @param {string}    sourceEndpoint            The endpoint ID (as given in the {@link restServices restServices}
 *                                              module) of the source items for this impact manager.
 * @param {integer=}  sourceId                  The ID of the source item for this impact manager. If not yet known
 *                                              (because the source item hasn't been saved to the database yet) can be
 *                                              omitted, in this case no existing impacts will be loaded. The source ID
 *                                              to associate with the impacts can then be passed through the
 *                                              {@link impactDirectives.callOnChange} `callOnChange` directive.
 * @param {Date=}     sourceDate                If given, the risk picker will display risks that were active at or
 *                                              after this point in time. Should be the date the original source
 *                                              element was created, to ensure that flags set at this point can still
 *                                              be edited. If not given, only risks currently active will be shown.
 * @param {string=}   riskCategory              The category of the risks that should be shown in the risk picker. If
 *                                              unspecified, no risks can be selected (and all impacts will have risk
 *                                              category `NONE`).
 * @param {string=}   riskCategoryDescription   A human-readable description for the given `riskCategory`. Will be used
 *                                              for user notifications about new impacts.
 *
 */

  .directive('impactManager', ['$q', '$timeout', 'gettext', 'impactsService', 'risksService', 'miscServices', function($q, $timeout, gettext, impactsService, risksService, miscServices) {

    return {
      restrict: 'E',
      require: '^^callOnChange',
      scope: {
        impacts: '=?',
        sourceEndpoint: '=',
        sourceId: '=?',
        sourceDate: '=?',
        riskCategory: '=?',
        riskCategoryDescription: '=?'
      },

      link: function(scope, element, attrs, callOnChangeCtrl) {
        scope.errMessage = undefined;
        scope.isSaving = false;
        scope.risks = [];
        scope.sourceKeywords = '';
        scope.foreignRisks = {};

        var sourceUrl;
        var sourceDescription;


        // Helper function to get foreign risks if we do not yet have them
        // (or are in the process of retrieving them)
        var getForeignRisks = function(impacts) {
          impacts.forEach(function(impact) {
            if(impact.targetRiskCategory !== 'NONE' && impact.targetRiskCategory !== undefined && impact.targetRiskCategory !== scope.riskCategory && scope.foreignRisks[impact.targetRiskCategory] === undefined) {
              // We need to load this foreignRisks category
              // We initialize their array, so it will be marked as loading.
              scope.foreignRisks[impact.targetRiskCategory] = {};

              // Let's retrieve the risks.
              risksService.getRisksByCategory(impact.targetRiskCategory, (scope.sourceDate ? scope.sourceDate : todayDate))
                .then(function (risks) {
                  risks.forEach(function(risk) {
                    scope.foreignRisks[impact.targetRiskCategory][risk.initialId.toString()] = miscServices.htmlToText(risk.title);
                  });

                  // TODO: Remove debug code
                  console.log('Updated foreign risks, based on impact with foreign risk category:');
                  console.log([scope.foreignRisks, impacts, impact]);
                });
            }
          });
        };


        // If not given, we need to set and load the impacts array
        if(!scope.impacts) {
          scope.impacts = [];

          // We need to obtain the existing impact flags for this source element.
          if(scope.sourceId !== undefined && scope.sourceId !== null) {
            impactsService.getImpactsBySource(scope.sourceEndpoint, scope.sourceId)
              .then(function(impacts) {
                impacts.forEach(function(impact) {
                  impact.targetRiskInitialId = '' + impact.targetRiskInitialId;  /* Convert to string */
                  impact.oldRiskInitialId = impact.targetRiskInitialId;
                  scope.impacts.push(impact);
                });

                getForeignRisks(impacts);
              });
          }
        } else {
          // We need to add the oldRiskInitialId property to each impact
          scope.impacts.forEach(function(impact) {
            impact.targetRiskInitialId = '' + impact.targetRiskInitialId;  /* Convert to string */
            impact.oldRiskInitialId = impact.targetRiskInitialId;
          });

          getForeignRisks(scope.impacts);
        }



        // We need to obtain the current risks stored under the riskCategory, if given
        if(scope.riskCategory !== undefined && scope.riskCategory !== null) {
          var todayDate = new Date();
          risksService.getRisksByCategory(scope.riskCategory, (scope.sourceDate ? scope.sourceDate : todayDate))
            .then(function(risks) {
              // We iterate over all risks but want to show each individual risk (as given by its initialId) only once,
              // in its most recent form. We leverage that the list returned by getRisksByCategory is already sorted
              // by initialId and then descending by validity date, so we can just get the first entry for each
              // initialId.

              var currInitialId = -1;
              risks.forEach(function(risk) {
                if(risk.initialId !== currInitialId) {
                  scope.risks.push({
                    id: risk.id,
                    initialId: risk.initialId,
                    titleText: miscServices.htmlToText(risk.title),
                    active: (risk.valid_from <= todayDate && risk.valid_to >= todayDate)
                  });

                  currInitialId = risk.initialId;
                }
              });
            });
        }


        // Helper function to remove element from impact array
        var removeImpactFromArr = function(impact) {
          var removeIndex = scope.impacts.indexOf(impact);

          if(removeIndex >= 0) {
            scope.impacts.splice(removeIndex, 1);
          }
        };

        // Helper function to update impacts before they are saved to the database
        var prepareImpactForSave = function(impact) {
          if(impact.targetRiskInitialId === null || impact.targetRiskInitialId === '' || impact.targetRiskInitialId < 0) {
            impact.targetRiskInitialId = undefined;
          }

          impact.targetRiskCategory = scope.riskCategory;
          impact.sourceEndpoint = scope.sourceEndpoint;
          impact.sourceId = scope.sourceId;
          impact.sourceKeywords = scope.sourceKeywords;
          impact.sourceUrl = sourceUrl;
          impact.sourceDescription = sourceDescription;
        };

        // Helper function to create targetDescription depending on whether an individual risk item has been selected
        // for the given impact
        var getTargetDescription = function(impact) {
          if(impact.targetRiskInitialId !== undefined) {
            var targetRisk = scope.risks.find(function(d) { return d.initialId.toString() === impact.targetRiskInitialId.toString(); });

            if(targetRisk) {
              return targetRisk.titleText;
            }
          }

          // No target description for precise risk was found, return risk category description
          return scope.riskCategoryDescription;
        };

        // We need to register functions that are called when the source item is saved or deleted
        var onSaveCallback = function(sourceId, _sourceUrl, _sourceDescription, sourceKeywordsArr) {
          // Are we talking about this element?
          if(scope.sourceId && scope.sourceId !== -1 && Number(scope.sourceId) !== Number(sourceId)) {
            // We are attached to an existing element but the callback sourceId does not match - this
            // callback is not addressed to us!
            return;

            // TODO: Find a better solution than the callOnChange directive - it is too broad, it should be on a lower level...
          }

          // We save all changes to the impacts (new impacts, modified impacts, removed impacts)
          var impactSavePromise = $q.resolve();
          scope.isSaving = true;
          scope.sourceId = sourceId;
          sourceUrl = _sourceUrl;
          sourceDescription = _sourceDescription;
          scope.sourceKeywords = sourceKeywordsArr.join(',').concat(',');

          // TODO: Remove debug code
          console.log('onSaveCallback called with sourceId = ' + sourceId + ', sourceKeywords = ' + sourceKeywordsArr);

          scope.impacts.forEach(function(impact) {
            if(impact.id < 0 && !impact.doRemove) {
              // The impact is new, should not be removed => we create it in the database

              prepareImpactForSave(impact);

              impactSavePromise = impactSavePromise.then(function() {
                return impactsService.addImpact(impact, getTargetDescription(impact))
                  .then(function(savedImpact) {
                      impact = savedImpact;
                    });
              });

            } else if(impact.id < 0 && impact.doRemove) {
              // The impact is new but should be removed => we do not have to do anything in the database and
              // just update the impacts array
              removeImpactFromArr(impact);

            } else if(impact.id >= 0 && !impact.doRemove && (impact.oldRiskInitialId !== impact.targetRiskInitialId || impact.sourceId !== sourceId || impact.sourceKeywords !== scope.sourceKeywords)) {
              // The impact has changed, because it is now assigned to a different targetRiskInitialId (or no targetRiskInitialId anymore)
              // => We update it

              prepareImpactForSave(impact);

              impactSavePromise = impactSavePromise.then(function() {
                return impactsService.updateImpact(impact, getTargetDescription(impact))
                  .then(function(savedImpact) {
                    impact = savedImpact;
                  });
              });

            } else if(impact.id >= 0 && impact.doRemove) {
              // We need to remove this impact flag
              impactSavePromise = impactSavePromise.then(function() {
                impactsService.deleteImpact(impact)
                  .then(function() {
                      removeImpactFromArr(impact);
                    });
              });
            }
          });

          impactSavePromise.then(
            function() {
              // Save completed
              scope.isSaving = false;
            },
            function(err) {
              // TODO: better error handling
              scope.isSaving = false;
              window.alert(err);
            }
          );

        };
        callOnChangeCtrl.onSave(onSaveCallback);


        var onDeleteCallback = function(sourceId) {
          // Are we talking about this element?
          if(Number(scope.sourceId) !== Number(sourceId)) {
            // We are attached to an existing element but the callback sourceId does not match - this
            // callback is not addressed to us!
            return;

            // TODO: Find a better solution than the callOnChange directive - it is too broad, it should be on a lower level...
          }

          // We need to remove all impacts that are already in the database (i.e. that have an "id") because their
          // source element has been removed
          scope.isSaving = true;

          var impactDeletePromise = $q.resolve();

          scope.impacts.forEach(function(impact) {
            if(impact.id !== undefined) {
              impactDeletePromise = impactDeletePromise.then(function() {
                return impactsService.deleteImpact(impact)
                  .then(
                    function () {
                      removeImpactFromArr(impact);
                    }
                  );
              });
            }
          });

          impactDeletePromise.then(
            function() {
              // Everything has been deleted.
              scope.isSaving = false;
            },
            function(err) {
              // TODO: better error handling
              window.alert(err);
            }
          );

        };
        callOnChangeCtrl.onDelete(onDeleteCallback);


        // SET-UP COMPLETE - FUNCTION DEFINITIONS FOR SCOPE BELOW

        // Function to add a new element to the impacts array (at the bottom)
        var newIdIterator = -1;
        scope.addImpact = function(impactType) {
          scope.impacts.push({
            'id': newIdIterator,
            'impactType': impactType,
            'sourceEndpoint': scope.sourceEndpoint,
            'targetRiskCategory': scope.riskCategory,
            'targetRiskInitialId': undefined,
            'impactDate': new Date()
          });

          newIdIterator--;
        };

        // Function to mark an element for removal from the impacts list
        scope.removeImpact = function(impact) {
          impact.doRemove = true;
        };

        // Function to restore an element from the impacts list that has been marked for removal
        scope.restoreImpact = function(impact) {
          impact.doRemove = false;
        };

        // If we get destroyed, but the callOnChangeCtrl still exists, we clean up our callbacks after 10 seconds
        // (We wait this long to wait for any saves to fire before that.)
        scope.$on('$destroy', function() {
          $timeout(function() {
            if(callOnChangeCtrl) {
              callOnChangeCtrl.removeOnSave(onSaveCallback);
              callOnChangeCtrl.removeOnDelete(onDeleteCallback);
            }
          }, 10000);
        });

        // That's it.
      },

      templateUrl: 'common/directives/impactManager.tmpl.html'
    };

  }])




  /**
   * @ngdoc directive
   * @name impactDirectives.impactViewer
   * @restrict E
   * @description
   *
   * This directive displays icons for each impact flag that satisfies the given criteria ("and" combined) of risk
   * category (`riskCategory` parameter), specific risk initialID (`targetRiskInitialId`), minimum (`fromDate`) and/or
   * maximum (`toDate`) impact date.
   *
   * The icons can be clicked on, in which case a dialog will display the detailed source items for each impact
   * type.
   *
   * @scope
   * @param {string}   viewerTitle    A description of the impact filter (e.g. the name of the risk category or the
   *                                  risk). Will be used in the dialog that is displayed when clicking on a flag.
   * @param {string=}  riskCategory   If given, only impact flags with the given risk category will be shown.
   * @param {integer=} riskInitialId  If given, only impact flags with this exact risk InitialId will be shown.
   *                                  Note that for this you also need to provide the risk's `riskCategory`, as we
   *                                  always retrieve all risks for the given category (to reduce the number of requests
   *                                  to the server). If you only want to show impacts *not* assigned to a specific
   *                                  risk, use `-1`.
   * @param {Date=}    fromDate       If given, limits the impact flags to be shown to impacts with an `impactDate` at
   *                                  or after `fromDate`.
   * @param {Date=}    toDate         If given, limits the impact flags to be shown to impacts with an `impactDate` at
   *                                  or before `toDate`.
   */

  .directive('impactViewer', ['gettext', 'ngDialog', 'impactsService', 'miscServices', function(gettext, ngDialog, impactsService, miscServices) {

    return {
      restrict: 'E',
      scope: {
        viewerTitle: '=?',
        riskCategory: '=?',
        riskInitialId: '=?',
        fromDate: '=?',
        toDate: '=?'
      },

      link: function(scope) {
        scope.errMessage = undefined;
        scope.isSaving = false;
        scope.isLoading = false;

        scope.impacts = [];

        // We fail if a `riskInitialId` is given but no `riskCategory`.
        if(scope.riskInitialId !== undefined && scope.riskCategory === undefined) {
          scope.errMessage = gettext('If you want to show impacts for a given risk, you also need to specify its risk category.');
          return;
        }

        // We need to retrieve the impacts from the server
        var updateImpacts = function(scope) {
          scope.isLoading = true;
          impactsService.getImpactsByRiskCategory(scope.riskCategory, scope.fromDate, scope.toDate)
            .then(
              function(impacts) {
                scope.isLoading = false;

                if(scope.riskInitialId === undefined) {
                  scope.impacts = impacts;
                } else {
                  scope.impacts = impacts.filter(function(impact) { return (impact.targetRiskInitialId !== undefined && impact.targetRiskInitialId.toString() === scope.riskInitialId.toString()); });
                }

                // TODO: Remove debug code
                console.log('impactViewer: Impacts loaded for ' + scope.riskCategory + ' and ID ' + scope.riskInitialId + ':');
                console.log([impacts, scope.impacts]);

              },
              function(err) {
                scope.errMessage = err;
              }
            );
        };

        updateImpacts(scope);


        // When clicking on a flag, all impacts with this impact type will be displayed in the dialog box.
        scope.showImpacts = function(impactType) {
          var impactsForType = scope.impacts.filter(function(impact) { return impact.impactType === impactType; });
          ngDialog.open({
            template: 'common/directives/impactViewerDialog.tmpl.html',
            className: 'ngdialog-theme-default',
            data: {
              impactType: impactType,
              viewerTitle: miscServices.htmlToText(scope.viewerTitle),
              impactsForType: impactsForType,
              notesIds: impactsForType
                .filter(function(impact) { return impact.sourceEndpoint === 'notes'; })
                .map(function(impact) { return impact.sourceId; })
            }
          });
        };

        scope.$watch('riskCategory', function(newVal, oldVal, scope) {
          updateImpacts(scope);
        });

        scope.$watch('fromDate', function(newVal, oldVal, scope) {
          updateImpacts(scope);
        });

        scope.$watch('toDate', function(newVal, oldVal, scope) {
          updateImpacts(scope);
        });

        scope.$watch('targetRiskInitialId', function(newVal, oldVal, scope) {
          updateImpacts(scope);
        });

        // That's it.
      },

      templateUrl: 'common/directives/impactViewer.tmpl.html'
    };

  }])


  /**
   * @ngdoc directive
   * @name impactDirectives.impactDirectViewer
   * @restrict E
   * @description
   *
   * This directive displays icons for each impact flag in the `impacts` array given.
   *
   * @scope
   * @param {Array}    impacts        The array of impacts which should be displayed.
   *
   */

  .directive('impactDirectViewer', function() {

    return {
      restrict: 'E',
      scope: {
        impacts: '='
      },

      link: function(scope) {
        // TODO: Remove debug code
        console.log('Linked impactDirectViewer with impacts: ');
        console.log(scope.impacts);
      },

      templateUrl: 'common/directives/impactDirectViewer.tmpl.html'
    };

  })

;

