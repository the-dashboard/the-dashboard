'use strict';

/**
 * @ngdoc overview
 * @name zoomDirectives
 * @description
 *
 * This module provides directives that allow nested directives to register to be notified
 * of changes in the zoom (hash part of the URL) for them.
 *
 */

angular.module('zoomDirectives', [])


/**
 * @ngdoc directive
 * @name zoomDirectives.zoomKey
 * @restrict A
 * @description
 *
 * This directive allows nested directives to register to be notified
 * of changes in the zoom (hash part of the URL) for them.
 *
 * @scope
 * @param {string} zoomKey   The prefix of the location hash that should be monitored
 *                           for changes. Only if the location hash starts with this
 *                           prefix followed by a slash the callbacks will be triggered.
 *
 */

  .directive('zoomKey', ['$location', '$timeout', '$anchorScroll', '$q', function($location, $timeout, $anchorScroll, $q) {
    return {
      scope: {
        zoomKey: '@'
      },
      controller: ['$scope', function zoomKeyController($scope) {
        $scope.callbacks = [];
        $scope.loadingCount = 0;

        // Helper function to process callbacks
        var processCallbacks = function(onlyCallback) {
          // We wrap it in a timeout, so that all synchronous stuff can run first (in particular
          // stuff that increases the loadingCount...)
          $timeout(function() {
            // Does zoomKey match and we are not loading?
            if($scope.loadingCount === 0 && $location.hash().substr(0, $scope.zoomKey.length+1) === $scope.zoomKey + '/') {
              // Yes -> Call all callbacks, unless (onlyCallback is given, then call only this one)!
              var callbackPart = $location.hash().substr($scope.zoomKey.length+1);

              (typeof onlyCallback === 'function' ? [ onlyCallback ] : $scope.callbacks).forEach(function(callback) {
                var doJump = callback(callbackPart);
                if(doJump !== undefined) {
                  // The callback indicated that we should jump to the given anchor tag
                  // Note that the callback can return a value or a promise, so we need to
                  // wrap it in a resolve() call.
                  $q.resolve(doJump).then(function(jumpTarget) {
                    $anchorScroll($scope.zoomKey + (jumpTarget !== '' ? '/' + jumpTarget : ''));
                  });
                }
              });
            }
          }, 0);
        };

        // We want to call our callbacks only after all dashboard elements have
        // stopped loading, to avoid "jumping around" after we have jumped to
        // the anchor. For this, elements can notify us that they are still
        // loading:
        this.startedLoading = function() {
          $scope.loadingCount++;
        };

        this.stoppedLoading = function() {
          $scope.loadingCount--;

          // Process callbacks if we have reached a count of zero
          if($scope.loadingCount === 0) {
            processCallbacks();
          }
        };

        // Register a new callback
        this.onZoom = function(callback) {
          // Avoid adding the same function twice
          if($scope.callbacks.indexOf(callback) === -1) {
            // TODO: Remove debug code
            console.log('Registering onZoom callback.');

            // Call only this callback
            processCallbacks(callback);

            // Add callback to the array
            $scope.callbacks.push(callback);
          }
        };

        // Remove an existing callback
        this.removeOnZoom = function(callback) {
          // TODO: Remove debug code
          console.log('Removing onSave callback.');

          var removeIndex = $scope.callbacks.indexOf(callback);

          if(removeIndex >= 0) {
            $scope.callbacks.splice(removeIndex, 1);
          }
        };

        // Obtain zoomKey of this directive.
        this.getZoomKey = function() {
          return $scope.zoomKey;
        };

        // Obtain the "full" relative link to this zoomKey
        this.getZoomLink = function(suffix) {
          return $location.path() + '#' + encodeURIComponent($scope.zoomKey + (suffix ? '/' + suffix : ''));
        };


        // Watch for changes to location hash
        $scope.$on('$locationChangeSuccess', processCallbacks);
      }]
    };
  }])

;
