'use strict';

/**
 * @ngdoc overview
 * @name tasksDirectives
 * @description
 *
 * This module provides directives for displaying task details in the OCA area of the Dashboard.
 *
 */

angular.module('tasksDirectives', ['gettext', 'tasksServices', 'miscServices'])

/**
 * @ngdoc directive
 * @name tasksDirectives.taskOverview
 * @restrict E
 * @description
 *
 * This directive displays the details (in particular the workflow status history) of a given
 * task and allows users to sign-off on the task (i.e. changing the workflow status).
 *
 * @scope
 * @param {Object}    task                 Specifies the task whose details should be shown.
 * @param {boolean=}   fromArchive         Specifies whether the task should be retrieved from the regular task
 *                                         library (if set to `false` or not set at all) or whether it should be
 *                                         retrieved from the archived tasks (if set to `true`).
 *
 */

  .directive('taskOverview', ['$q', '$window', 'gettext', 'tasksService', 'miscServices', function($q, $window, gettext, tasksService, miscServices) {

    return {
      restrict: 'E',
      replace: 'true',
      scope: {
        task: '=',
        fromArchive: '=?'
      },

      link: function(scope) {
        scope.errMessage = undefined;
        scope.isLoading = true;
        scope.isSaving = false;

        // What is the applicable workflow rule for this task?
        scope.workflowRule = 'off';
        tasksService.getWorkflowRule(scope.task.id).then(
          function(workflowRule) {
            scope.workflowRule = workflowRule;
          },
          function(err) {
            console.log('ERROR: An error occured while trying to retrieve the workflowRule for task ' + scope.task.id + ':');
            console.log([err, scope.task, scope.fromArchive]);
          }
        );

        // Do we have the right to archive tasks?
        scope.hasArchivePermissions = false;
        tasksService.hasArchivePermissions().then(
          function(_hasPermissions) {
            scope.hasArchivePermissions = _hasPermissions;
          }
        );

        // Helper that processes updates to the workflow (sign-offs etc.)
        scope.changeStatus = function(newStatus) {
          // Do we really want to save the status?
          if(miscServices.translatedConfirm(gettext('Do you really want to update the status of this task?'))) {
            // Send to database
            scope.isSaving = true;
            tasksService.updateTaskStatus(scope.task, newStatus)
              .then(
                function() {
                  scope.isSaving = false;
                },
                function(err) {
                  miscServices.translatedAlert(err);
                  scope.isSaving = false;
                }
              );
          }
        };


        // Helper function to archive the given task
        scope.archiveTask = function() {
          if(!scope.hasArchivePermissions) {
            miscServices.translatedAlert(gettext('You do not have the necessary permissions to archive tasks. Please contact your administrator.'));
            return;
          }

          if(!miscServices.translatedConfirm(gettext('Are you sure you want to archive this task? This cannot be undone.'))) {
            return;
          }

          scope.isSaving = true;

          // Ok, we are ready to go. We need to call the archiveTask function...
          tasksService.archiveTask(scope.task)
            .then(
              function() {
                miscServices.translatedAlert(gettext('The task has been archived.'));
                scope.isSaving = false;

                // Refresh the page to clear all caches (dirty hack, TODO: make cleaner)
                $window.location.assign('#/tasks');
                $window.location.reload();
              },
              function(err) {
                if(typeof err === 'string') {
                  miscServices.translatedAlert(err);
                } else {
                  console.log('ERROR: An error occured while trying to archive the task:');
                  console.log(err);

                  miscServices.translatedAlert(gettext('An error occured while trying to archive the task.'));
                  scope.isSaving = false;
                }
              }
            );
        };


      },

      templateUrl: 'common/directives/taskOverview.tmpl.html'
    };

  }])

;