/*jshint -W018 */

'use strict';

/**
 * @ngdoc overview
 * @name auditFormsDirectives
 * @description
 *
 * This module provides directives for displaying "audit forms". Audit forms can be attached to arbitrary data keywords
 * and are defined using HTML templates which can receive data from the data elements they are being attached to as
 * well as store data entered by the user.
 *
 */

angular.module('auditFormsDirectives', ['gettext', 'ngDialog', 'auditFormsServices', 'miscServices', 'usersServices', 'socialServices', 'datafilesServices'])



/**
 * @ngdoc directive
 * @name auditFormsDirectives.auditForm
 * @restrict E
 * @description
 *
 * This directive displays the specified audit form to the user, loads already saved data and saves data if requested
 * to do so by the user.
 *
 * @scope
 * @param {string}      auditFormKey      The short key that identifies the audit form to display.
 * @param {Object}      auditFormData     The data object with the details of the audit form to be displayed. If it
 *                                        contains an `id` value, the directive will manipulate the existing
 *                                        audit form data element in the database. Without `id` value, the directive
 *                                        will create a new audit form data element when the user saves the data.
 * @param {boolean=}    readOnly          If set to `true`, the audit form will be displayed read-only, it will not be
 *                                        possible to save data.
 * @param {string}      workflowRule      The workflow rule (`off`, `soft`, or `hard`) that should be applied to this
 *                                        audit form.
 * @param {Function=}   onChange          An optional callback function that will be called whenever the "result"
 *                                        value of the given audit form changes (by the user changing some values and
 *                                        then saving the form).
 *                                        Will be given the complete auditFormData object of the new audit form data.
 * @param {Function=}   onDelete          An optional callback function that will be called whenever the audit form is
 *                                        deleted (for example to close the dialog it is displayed in).
 * @param {boolean=}    autoSave          If set to `true`, the audit form will automatically save changes.
 * @param {Function=}   closeCallback     If given, the audit form will display a "save and close" button and will
 *                                        call the `closeCallback` after saving if this button is pressed.
 */

  .directive('auditForm', ['$timeout', 'gettext', 'ngDialog', 'auditFormsService', 'usersService', 'interactionService', 'miscServices', 'datafileReader', function($timeout, gettext, ngDialog, auditFormsService, usersService, interactionService, miscServices, datafileReader) {

    return {
      restrict: 'E',
      scope: {
        auditFormKey: '=',
        auditFormData: '=',
        readOnly: '=?',
        workflowRule: '=?',
        onChange: '=?',
        onDelete: '=?',
        autoSave: '=?',
        closeCallback: '=?'
      },

      link: function(scope) {
        scope.errMessage = undefined;
        scope.isSaving = false;
        scope.isLoading = false;
        scope.isLoadingAdditionalData = false;

        scope.auditForm = undefined;
        scope.additionalSourceSelect = undefined;
        scope.auditFormForm = {};


        // Short cuts to be used by included audit form
        if(!scope.auditFormData.sourceData) { scope.auditFormData.sourceData = {}; }
        if(!scope.auditFormData.resultsData) { scope.auditFormData.resultsData = {}; }
        scope.sourceData = scope.auditFormData.sourceData;
        scope.resultsData = scope.auditFormData.resultsData;

        // Initialize oldResult data.
        scope.auditFormData._oldResult = scope.auditFormData.result;


        // Retrieve auditForm using auditFormKey
        auditFormsService.getAuditForm(scope.auditFormKey)
          .then(
            function(auditForm) {
              scope.auditForm = auditForm;
              scope.isLoading = false;

              // We initialize the additionalSource selection with the first available entry
              if(Array.isArray(scope.auditForm.additionalSources) && scope.auditForm.additionalSources.length >= 1) {
                scope.additionalSourceSelect = scope.auditForm.additionalSources[0].datafile;
              }

              // We select the first tab if tabs are set
              if(Array.isArray(scope.auditForm.tabs) && scope.auditForm.tabs.length >= 1) {
                scope.tab = scope.auditForm.tabs[0].tabId;
              }


              // TODO: Remove debug code
              console.log('Initialized auditForm and additionalSourceSelect:');
              console.log([ scope.auditForm, scope.additionalSourceSelect ]);
              console.log('Results expression "' + scope.auditForm.resultExpression + '" evaluates to "' + scope.$eval(scope.auditForm.resultExpression) + '"!');


              // We set the $watch here, as we need the loaded auditForm "resultExpression" for this
              scope.$watch(scope.auditForm.resultExpression, function(newVal, oldVal) {
                // TODO: Remove debug code
                console.log('Value of results expression has changed: ' + oldVal + ' -> ' + newVal);
                scope.auditFormData.result = newVal;
              });
            }
          );


        // Store audit form changes
        scope.saveData = function() {

          // TODO: Remove debug code
          console.log('saveData called, saving auditFormData...');
          console.log(scope.auditFormData);
          console.log('Results expression "' + scope.auditForm.resultExpression + '" evaluates to "' + scope.$eval(scope.auditForm.resultExpression) + '"!');

          scope.isSaving = true;
          scope.hasSaved = false;
          var isNewForm = (scope.auditFormData.id === undefined || scope.auditFormData.id === null);
          return auditFormsService.saveFormData(scope.auditFormData)
            .then(
              function(newFormData) {
                if(isNewForm) {
                  // This is new form data (not an update of existing data)
                  // So we also need to watch this audit form.
                  usersService.getCurrentUser()
                            .then(
                              function(user) {
                                // We already update our interactionSummary...
                                if(!scope.auditFormData.interactionSummary) {
                                  scope.auditFormData.interactionSummary = { };
                                }
                                scope.auditFormData.interactionSummary.watch = [ user.id.toString() ];

                                // Now we can actually create the watch
                                return interactionService.addWatch(user.id, 'auditFormData', newFormData.id, scope.auditForm.title + ' (' + scope.auditFormData.result + ')', '##' + newFormData.id);
                              }
                            );
                }

                // Update data received from the web service
                scope.auditFormData.id = newFormData.id;
                scope.auditFormData.created_at = newFormData.created_at;
                scope.auditFormData.last_modified_at = newFormData.last_modified_at;
                scope.auditFormData.last_modified_by = newFormData.last_modified_by;

                scope.isSaving = false;
                scope.hasSaved = true;

                // TODO: Remove debug code
                console.log('Saving audit form content...');
                console.log(scope.auditFormForm);

                // We reset the form as it is pristine again
                if(scope.auditFormForm.form) {
                  scope.auditFormForm.form.$setPristine();
                }

                if(scope.onChange && scope.auditFormData._oldResult !== scope.auditFormData.result) {
                  // TODO: Remove debug code
                  console.log('Calling onChange callback: ');
                  console.log([ scope.onChange, scope.auditFormData ]);

                  // Call onChange callback
                  scope.onChange(scope.auditFormData);
                }
                scope.auditFormData._oldResult = scope.auditFormData.result;
              },
              function(err) {
                console.log('An ERROR occured while trying to save form data:' + err);
                console.log(scope.auditFormData);
                scope.errMessage = err;
                scope.isSaving = false;
              }
            );
        };


        // Auto-save if enabled
        var autoSaveFunction = function() {
          // TODO: Remove debug code
          console.log('Auto-save timeout...');
          console.log([scope, scope.autoSave, scope.auditFormForm]);

          if(scope.autoSave && !scope.readOnly) {
            if(!scope.isSaving && scope.auditFormForm.form && !scope.auditFormForm.form.$pristine && scope.auditFormForm.form.$valid) {
              // TODO: Remove debug code
              console.log('Auto-saving audit form data (form has changed, is valid and autoSave is enabled)...');

              scope.saveData();
            }

            $timeout(autoSaveFunction, 10000);
          }
        };

        $timeout(autoSaveFunction, 10000);


        // Save and close
        scope.saveAndClose = function() {
          if(scope.closeCallback) {
            return scope
              .saveData()
              .then(scope.closeCallback);
          } else {
            return scope.saveData();
          }
        };


        // Change tab
        scope.changeTab = function(tabId) {
          scope.tab = tabId;
        };


        // Show tab information if set
        scope.showTabHelp = function(helpTitle, helpText, stopPropEvent) {
          if(stopPropEvent) {
            // If this parameter is passed an $event object, we will stop propagation of the click
            stopPropEvent.stopPropagation();
          }

          ngDialog.open({
            template: 'common/directives/auditFormsHelpDialog.tmpl.html',
            className: 'ngdialog-theme-default',
            data: { 'helpTitle': helpTitle, 'helpText': helpText }
          });
        };

        // Update the workflow status of an (already saved) audit forms entry
        scope.changeWorkflowStatus = function(newStatus) {
          if(!scope.auditFormData.id) {
            miscServices.translatedAlert(gettext('Please save your audit form first before you try to update the workflow status.'));
            return;
          }

          scope.isSaving = true;

          auditFormsService.updateWorkflowStatus(scope.auditFormData, newStatus)
            .then(
              function() {
                scope.isSaving = false;
              },
              function(err) {
                console.log('ERROR while trying to changeWorkflowStatus for ID ' + scope.auditFormData.id + ' to ' + scope.newStatus + ':');
                console.log(err);

                miscServices.translatedAlert(gettext('An error occured while trying to update the workflow status. Please try again later.'));
                scope.isSaving = false;
              }
            );
        };


        // Open sign-offs dialog
        scope.showWorkflow = function() {
          ngDialog.open({
            template: 'common/directives/auditFormsDetailsDialog.tmpl.html',
            className: 'ngdialog-theme-default',
            data: {
              'auditForm': scope.auditForm,
              'auditFormData': scope.auditFormData,
              'changeWorkflowStatus': scope.changeWorkflowStatus
            }
          });
        };


        // Load data from additionalSources...
        scope.loadAdditionalSource = function() {
          // We need to ask for confirmation as this will overwrite existing information in the sheet.
          if(!miscServices.translatedConfirm(gettext('Do you really want to load additional data into this audit form?\n\nNOTE: Data loaded will overwrite existing values in the form.'))) {
            return;
          }

          // If we only have one additional source, the user does not need to select one
          var dataSourceDatafile = (scope.auditForm.additionalSources.length === 1 ? scope.auditForm.additionalSources[0].datafile : scope.additionalSourceSelect);
          var dataSource = scope.auditForm.additionalSources.find(function(source) { return source.datafile === dataSourceDatafile; });

          // Did we select a valid source?
          if(dataSource === undefined) {
            miscServices.translatedAlert(gettext('Please select the source which you want to load.'));
            return;
          }


          // Ok, let's load that datafile...
          scope.isLoadingAdditionalData = true;
          return datafileReader.readDatafile(dataSource.datafile)
            .then(function(datafile) {
              // TODO: Remove debug code
              console.log('loadAdditionalSource: We have retrieved the requested datafile "' + dataSourceDatafile + '":');
              console.log([dataSource, datafile, scope.sourceData]);

              // The first element is the array of rows in the data file - we find the first matching row by keys
              var datum = datafile[0].find(function(d) {
                return dataSource.keyMap.every(function(keyCol) {
                  return (d[keyCol.from] === scope.sourceData[keyCol.to]);
                });
              });

              // If we don't find a matching element, we show an error message
              if(datum === undefined) {
                console.log('loadAdditionalSource: We could not find a matching data element!');
                console.log([datafile, scope.sourceData, dataSource.keyMap]);

                miscServices.translatedAlert(gettext('We could not find a matching entry in the data source for this audit form. Nothing has been loaded.'));
                scope.isLoadingAdditionalData = false;
                return;
              }

              // Ok, we have a matching data element, we write resultData according to the dataMap
              dataSource.dataMap.forEach(function(map) {
                scope.resultsData[map.to] = datum[map.from];
              });

              // Done.
              miscServices.translatedAlert(gettext('The source data has successfully been loaded.'));
              scope.isLoadingAdditionalData = false;
            });
        };


        // DELETE Audit Form
        scope.deleteForm = function() {
          if(miscServices.translatedConfirm(gettext('Do you really want to delete this audit form?\n\nThis action cannot be undone.'))) {
            scope.isSaving = true;

            // Is this audit form already saved? Otherwise, we don't need to do anything...
            if(scope.auditFormData.id === undefined) {
              // Call onDelete callback if set.
              if(scope.onDelete) {
                scope.onDelete(scope.auditFormData);
              }

              scope.isSaving = false;

              return;
            }

            // Ok, the form is already saved, so we need to remove it from the database.
            auditFormsService.deleteFormData(scope.auditFormData.id)
              .then(
                function() {
                  // Deleting the forms data was successful => Call onDelete callback if set.
                  if(scope.onDelete) {
                    scope.onDelete(scope.auditFormData);
                  }

                  scope.isSaving = false;
                },
                function() {
                  miscServices.translatedAlert(gettext('An error occured while trying to delete this audit form.'));
                  scope.isSaving = false;
                });
          }
        };

        // That's it.
      },

      templateUrl: 'common/directives/auditForm.tmpl.html'
    };

  }])




  /**
   * @ngdoc directive
   * @name auditFormsDirectives.auditFormIcons
   * @restrict E
   * @description
   *
   * This directive displays icons for each audit form for which we have received auditFormData. If `editLink` is set
   * to `true`, these icons can be clicked and will display a dialog to edit the given audit form.
   *
   * If the `newFormTypes` array is given and contains valid audit form short keys, it will also display a "new" icon to
   * add new audit forms to the data element. In this case, you also need to specify `keywords`, `sourceTaskId` and
   * `sourceElement`, which will be assigned to the new audit form to be created. If you want to pass data from the
   * source element to the audit form, you also need to provide the `sourceData` object.
   *
   * Note that you cannot create audit forms on archived tasks, so if you want to be able to create new audit forms,
   * `sourceTaskArchived` needs to be `false`. The `source*` attributes are used to highlight audit forms which have
   * been created on this particular element, however, which is why you can provide an archived task here.
   *
   * @scope
   * @param {Array}       auditFormData       An array of audit form data element objects, each of which will be
   *                                          displayed as a single icon. The directive will load the audit form
   *                                          definition for each data element (using the `auditFormData.auditForm`
   *                                          short key) and use its `iconMap` definition to find the right icon
   *                                          depending on the `auditFormData.result` result value.
   * @param {boolean=}    editLink            If set to `true`, each audit form icon will be clickable and display
   *                                          the audit form for editing. If set to `false`, the audit form will
   *                                          be presented read-only ("Save" will be disabled).
   * @param {string}      workflowRule        The workflow rule (`off`, `soft`, or `hard`) that should be applied to
   *                                          these audit forms.
   * @param {Array=}      newFormTypes        If an array is given, the array elements need to be valid audit form
   *                                          short keys. The directive will then display a "plus" icon to add a new
   *                                          audit form to the given element (requires `keywords`, `sourceTaskId`,
   *                                          and `sourceElement` to be set).
   *                                          If multiple audit form short keys are given, it will display a dialog
   *                                          window to select the audit form to add to the element.
   * @param {Function=}   onNew               Callback to be called when a new audit form is saved (e.g. to attach this
   *                                          form to other elements on the page). The callback function will receive
   *                                          the new audit form element as parameter.
   * @param {Function=}   onChange            Callback to be called when changes are made to an audit form result
   *                                          (used to e.g. recalculate results from the form). The callback function
   *                                          will receive the changed audit form element as first parameter and *all*
   *                                          attached audit forms as second parameter.
   *                                          If given, the `onChangeObject` (see below) will be provided as third
   *                                          parameter.
   * @param {Object=}     onChangeObject      If specified, this object will be handed to the `onChange` callback as
   *                                          third parameter.
   * @param {Function=}   onDelete            Callback to be called when an audit form is deleted. The callback function
   *                                          will receive the deleted audit form element as parameter.
   * @param {boolean=}    autoSave            Will be passed to any audit form opened using the icons. If set to `true`,
   the audit form will automatically save itself every 10 seconds.
   * @param {Array=}      keywords            An array of keywords to assign to new audit form data elements created
   *                                          using the "plus" icon (see above).
   * @param {integer=}    sourceTaskId        The task ID in which the particular element these audit form icons are
   *                                          displayed for exists. This is used for highlighting these audit form
   *                                          data elements that were created directly on the particular element. It is
   *                                          also assigned to any new audit form data elements created using the
   *                                          "plus" icon (see above) and thus is mandatory if you want to be able
   *                                          to create new audit form elements.
   * @param {boolean=}    sourceTaskArchive   Indicates whether the task in which the particular element these audit
   *                                          form icons are displayed for is archived (`true` if task is archived).
   *                                          This is used for highlighting these audit form data elements that were
   *                                          created directly on the particular element.
   *                                          Note that you cannot create new audit form elements on archived tasks,
   *                                          so if you want to be able to add new elements `sourceTaskArchive` needs
   *                                          to be omitted or `false`.
   * @param {string=}     sourceElement       Indicates the particular element these audit form icons are displayed for.
   *                                          This is used for highlighting the audit form data elements that were
   *                                          created directly on the particular element.
   *                                          It is also assigned to any new audit form data elements created using the
   *                                          "plus" icon (see above) and thus is mandatory if you want to be able
   *                                          to create new audit form elements.
   * @param {Object=}     sourceData          This object will be passed as source data to any new audit forms created
   *                                          by using the "plus" icon (see above). Only needs to be provided if
   *                                          `newFormTypes` is set and new audit forms should be created.
   */

  .directive('auditFormIcons', ['gettext', 'ngDialog', 'auditFormsService', 'usersService', function(gettext, ngDialog, auditFormsService, usersService) {

    return {
      restrict: 'E',
      scope: {
        auditFormData: '=',
        editLink: '=?',
        workflowRule: '=',
        newFormTypes: '=?',
        onNew: '=?',
        onChange: '=?',
        onChangeObject: '=?',
        onDelete: '=?',
        autoSave: '=?',
        keywords: '=?',
        sourceTaskId: '=?',
        sourceTaskArchived: '=?',
        sourceElement: '=?',
        sourceData: '=?'
      },

      link: function(scope) {
        scope.isSaving = false;
        scope.isLoading = false;
        scope.sourceTaskArchived = !!scope.sourceTaskArchived;

        scope.auditForms = {};
        scope.newForm = {};


        // Retrieve auditForm types for mapping icons
        scope.isLoading = true;
        auditFormsService.getAllForms()
          .then(
            function(forms) {
              scope.auditForms = forms;
              scope.isLoading = false;
            },
            function(err) {
              console.log('An ERROR occured loading auditFormIcons forms:');
              console.log(err);
            }
          );


        // onChange wrapper as the audit form dialog onChange callback only sends the
        // single audit form and we need to pass on all audit forms for this icon set
        var onChangeWrapper;
        if(scope.onChange) {
          onChangeWrapper = function(singleFormData) {
            // This workaround is needed as new audit forms are only pushed to the auditFormData array after this callback has fired.
            var allForms = scope.auditFormData.indexOf(singleFormData) >= 0 ? scope.auditFormData : scope.auditFormData.concat([ singleFormData ]);

            return scope.onChange(singleFormData, allForms, scope.onChangeObject);
          };
        }

        // onDelete wrapper is called when an audit form is deleted - we then need to close
        // the dialog it is opened in and refresh any auditFormIcons that might display it
        var onDeleteWrapper = function(deletedAuditFormData) {
          // Do we need to trigger an onChange event?
          if(scope.onDelete) {
            scope.onDelete(deletedAuditFormData);
          }

          // In any case we close the open audit form dialog
          ngDialog.close();
        };


        // Open edit dialog when clicking on an existing form
        scope.openAuditForm = function(auditForm) {
          ngDialog.open({
            template: 'common/directives/auditFormsFormDialog.tmpl.html',
            className: 'ngdialog-theme-default',
            data: {
              'auditFormData': auditForm,
              'readOnly': (!scope.editLink ||
                !(scope.sourceTaskId && scope.sourceElement &&          /* read-only unless this form has been created on this item */
                  auditForm.createdInTask.toString() === scope.sourceTaskId.toString() &&
                  auditForm.archivedTask.toString() === scope.sourceTaskArchived.toString() &&
                  auditForm.createdInElement.toString() === scope.sourceElement.toString())
              ),
              'workflowRule': scope.workflowRule,
              'onChange': onChangeWrapper,
              'onDelete': onDeleteWrapper,
              'autoSave': scope.autoSave
            }
          });
        };


        // Helper function to open a new (empty) audit form of a given type
        var formsPicker;
        var newAuditFormOfType = function(formType) {
          usersService.getCurrentUser().then(function(currUser) {
            scope.newForm = {
              auditForm: formType,
              sourceData: scope.sourceData,
              resultsData: { },
              result: undefined,
              keywords: scope.keywords,
              createdInTask: scope.sourceTaskId,
              archivedTask: false,
              createdInElement: scope.sourceElement,
              workflowStatus: 'New',
              workflowChanges: [ {
                status: 'New',
                change_at: new Date(),
                change_by: currUser.id
              } ],
              assigned_to: currUser.id
            };

            // Close forms picker
            if(formsPicker) {
              formsPicker.close();
            }

            // Open edit dialog with newForm
            ngDialog.open({
              template: 'common/directives/auditFormsFormDialog.tmpl.html',
              className: 'ngdialog-theme-default',
              data: {
                'auditFormData': scope.newForm,
                'workflowRule': scope.workflowRule,
                'onChange': onChangeWrapper,
                'onDelete': onDeleteWrapper,
                'autoSave': scope.autoSave
              }
            });

            // Watch for changes to the newForm id => this indicates that the new element has been saved and should
            // now be displayed in our icon picker
            scope.$watch('newForm.id', function(newVal, oldVal) {
              if(newVal > 0 && !oldVal) {
                // Form has been saved => We add it to the auditFormData array!
                if(scope.auditFormData.findIndex(function(d) { return d.id === newVal; }) === -1) {
                  // Form hasn't been pushed before
                  scope.auditFormData.push(scope.newForm);

                  // We also call callback if necessary
                  if(scope.onNew) {
                    scope.onNew(scope.newForm);
                  }
                }
              }
            });
          });
        };

        // Open edit dialog with a new, empty form when clicking on the "Plus" button
        scope.newAuditForm = function() {
          if(scope.newFormTypes === undefined || (Array.isArray(scope.newFormTypes) && scope.newFormTypes.length === 0)) {
            // We do nothing if we have no valid audit form type
            return;
          }

          if(!Array.isArray(scope.newFormTypes)) {
            scope.newFormTypes = [ scope.newFormTypes ];
          }

          if(scope.newFormTypes.length === 1) {
            // If only one admissible audit form type has been specified, we will directly open a new audit form
            // with this type, bypassing the audit form picker
            return newAuditFormOfType(scope.newFormTypes[0]);
          }

          // Otherwise we will display the audit form picker for chosing a new audit form to add
          formsPicker = ngDialog.open({
            template: 'common/directives/auditFormsPickerDialog.tmpl.html',   // TODO - create template
            className: 'ngdialog-theme-default',
            data: {
              'newFormSpecs': scope.newFormTypes.map(function(formType) { return scope.auditForms[formType]; }),
              'newAuditFormOfType': newAuditFormOfType
            }
          });
        };



        // TODO: Opening an audit form should change the URL (to make them linkable)


        // That's it.
      },

      templateUrl: 'common/directives/auditFormIcons.tmpl.html'
    };

  }])
;
