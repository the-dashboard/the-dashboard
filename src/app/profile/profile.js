'use strict';

/**
 * @ngdoc overview
 * @name dashboardProfile
 * @description
 *
 * This module provides the profile interface of the Dashboard. In the profile, each user can change his/her user
 * settings.
 */

angular.module('dashboardProfile', ['ui.router', 'ngTagsInput', 'dashboardApp', 'usersServices', 'configServices', 'chartServices', 'miscServices', 'notesServices'])
  .config(['$stateProvider', function($stateProvider) {
    $stateProvider
      // route for the profile overview
      .state('profile', {
        url: '/profile',
        views: {
          'main@': {
            templateUrl: 'app/profile/profile.tmpl.html',
            controller: 'profileController'
          }
        }
      })

    ;
  }])


  /**
   * @ngdoc controller
   * @name dashboardProfile.controller:profileController
   * @description
   *
   * This controller provides all the functionality for the profile interface.
   *
   * The profile is used by users to change their user settings such as the language they want to use
   * The Dashboard in.
   *
   */

  .controller('profileController', ['$scope', '$q', '$timeout', 'gettext', 'gettextCatalog', 'usersService', 'configService', 'd3', function($scope, $q, $timeout, gettext, gettextCatalog, usersService, configService, d3) {
    $scope.errMessage = '';
    $scope.isLoading = true;
    $scope.isSaving = false;
    $scope.hasSaved = false;

    $scope.availableLanguages = {};

    // Load global configuration / loadConfig promise returns when data has been loaded
    var loadConfig = configService.getConfiguration()
      .then(
        function(config) {
          // Set available languages
          try{
            $scope.availableLanguages = JSON.parse(config['Available Languages']);
          } catch(e) {
            console.log('Could not load available languages!');
            console.log(e);
            $scope.availableLanguages = { 'en': 'English' };
          }

          // Set default language (or default to English if no global default is set)
          $scope.defaultLanguage = ($scope.availableLanguages[config['Default Language']] ? config['Default Language'] : 'en');
        }
      );

    // Load user settings / loadUserSettings promise returns when data has been loaded
    var loadUserSettings = usersService.getCurrentUserSettings()
      .then(
        function(settings) {
          $scope.settings = settings;

          // Setting default values where required
          if($scope.settings.watchNewNote === undefined || $scope.settings.watchNewNote === null) {
            $scope.settings.watchNewNote = 'yes';
          }
        }
      );

    // When both loadConfig and loadUserSettings have fired, form is no longer loading.
    $q.all([loadConfig, loadUserSettings]).then(
      function() {
        $scope.errMessage = '';
        $scope.isLoading = false;
      },
      function(err) {
        $scope.errMessage = gettext('An error occured loading your settings: ' + err + ' Please get in touch or try again later.');
      }
    );

    // Execute when language is changed
    $scope.changeLanguage = function() {
      // TODO: Remove debug code
      console.log('Changing language to ' + $scope.settings.language);

      gettextCatalog.setCurrentLanguage($scope.settings.language);
      d3.ourSettings.setLocale($scope.settings.language);
    };

    // Executed when "Save changes" button is pressed
    $scope.saveProfile = function() {
      // Language: Is the selected language valid? (If no language is selected, that is fine as well, in this case the default will kick in.)
      if($scope.settings.language !== undefined && !$scope.availableLanguages[$scope.settings.language]) {
        $scope.errMessage = gettext('Please select a valid language you want The Dashboard to be displayed in!');
        return;
      }

      // Ok, now we can start the saving process.
      $scope.isSaving = true;
      usersService.saveUserSettings($scope.settings).then(
        function(settings) {
          // The new settings object comes with id and userId if they didn't exist before, so we need to update
          // our scope.
          $scope.settings = settings;

          // Ok, display the success message but hide it after 5 seconds.
          $scope.isSaving = false;
          $scope.hasSaved = true;

          $timeout(function() {
            $scope.hasSaved = false;
          }, 5000);
        },
        function(err) {
          // Ok, display error message.
          $scope.errMessage = gettext('An error occured saving your changes:') + ' ' + err;
          $scope.isSaving = false;
          $scope.hasSaved = false;
        }
      );
    };

  }])

;

