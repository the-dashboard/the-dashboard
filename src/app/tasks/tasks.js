/*jshint -W018 */
'use strict';

/**
 * @ngdoc overview
 * @name dashboardTasks
 * @description
 *
 * This module provides routing and base controllers for the "Ongoing Control Assessment"
 * part of The Dashboard. The "Ongoing Control Assessment" is basically a tool to view
 * prepared data extracts (reports or lists), to record audit outcomes on a per-record
 * basis on these lists and to document performed QA on these audit results.
 * 
 * This module provides the functionality to view the overview of available reports and
 * their status, to view the individual reports and document audit results within them,
 * to perform "in-process" machine learning to help auditors filter out false positives,
 * and to show and record data in forms for performing more complicated audit analyses.
 *
 */

angular.module('dashboardTasks', ['ui.router', 'ui.grid', 'ui.grid.pinning', 'ui.grid.resizeColumns', 'ngDialog', 'learningServices', 'chartServices', 'printServices', 'tasksServices', 'usersServices', 'notesServices', 'tasksDirectives', 'auditFormsDirectives', 'auditFormsServices', 'excelServices', 'miscServices'])
  .config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    // route for the tasks overview
      .state('tasks', {
        url: '/tasks',
        views: {
          'main@': {
            templateUrl: 'app/tasks/tasks.tmpl.html',
            controller: 'tasksOverviewController'
          }
        }
      })

      .state('tasks.archive', {
        url: '/archive',
        views: {
          'main@': {
            templateUrl: 'app/tasks/tasks.tmpl.html',
            controller: 'tasksOverviewController'
          }
        },
        data: {
          archivedTasks: true
        }
      })

      // route for an individual table
      .state('tasks.table', {
        url: '/:id',
        views: {
          'main@': {
            templateUrl: 'app/tasks/table.tmpl.html',
            controller: 'tasksTableController'
          }
        }
      })

      // route for an individual table
      .state('tasks.archive.table', {
        url: '/:id',
        views: {
          'main@': {
            templateUrl: 'app/tasks/table.tmpl.html',
            controller: 'tasksTableController'
          }
        },
        data: {
          archivedTasks: true
        }
      });

  }])


  /**
   * @ngdoc controller
   * @name dashboardTasks.controller:tasksOverviewController
   * @description
   *
   * This controller shows the overview of all available audit tasks. By default only open tasks
   * assigned to the logged in user (either as auditor or QS responsible) are displayed. A clickable
   * link opens the detailed data table for the relevant task.
   *
   * The controller retrieves the tasks elements using the {@link dashboardTasksServices.tasksService}.
   * It provides functionality to filter the displayed tasks, filtering by assigned users and the status
   * of a given task (unassigned, in-progress, completed, reviewed).
   *
   */

  .controller('tasksOverviewController', ['$scope', '$state', 'tasksService', 'gettext', 'gettextCatalog', 'ngDialog', function($scope, $state, tasksService, gettext, gettextCatalog, ngDialog) {
    $scope.loading = true;
    $scope.errMessage = '';
    $scope.archivedTasks = !!($state.current.data && $state.current.data.archivedTasks);

    $scope.taskListGridOptions = {
      enableSorting: true,
      enableFiltering: true,
      columnDefs: [
        { name: gettext('Created'), field: 'created_at', width: '10%', type: 'date', cellFilter: 'date:\'dd.MM.yyyy\'' },
        { name: gettext('Title'), field: 'title', cellTemplate: '<div class="ui-grid-cell-contents"><a ui-sref="tasks.' + ($scope.archivedTasks ? 'archive.' : '') + 'table({id: row.entity.id})">{{row.entity.title}}</a> <i class="fa fa-info-circle icon-as-button" aria-hidden="true" ng-click="grid.appScope.showTaskDetails(row.entity)"></i></div>', width: '36%' },
        { name: gettext('Status'), field: 'status', width: '12%', cellFilter: 'taskStatus' },
        { name: gettext('Modified'), field: 'last_modified_at', width: '10%', type: 'date', cellFilter: 'date:\'dd.MM.yyyy\'' },
        { name: gettext('Modified By'), displayName: gettext('By'), field: 'last_modified_by', width: '4%', cellFilter: 'userIdToInitials' },
        { name: gettext('Done'), field: 'done_at', width: '10%', type: 'date', cellFilter: 'date:\'dd.MM.yyyy\'' },
        { name: gettext('Done By'), displayName: gettext('By'), field: 'done_by', width: '4%', cellFilter: 'userIdToInitials' },
        { name: gettext('Reviewed'), field: 'reviewed_at', width: '10%', type: 'date', cellFilter: 'date:\'dd.MM.yyyy\'' },
        { name: gettext('Reviewed By'), displayName: gettext('By'), field: 'reviewed_by', width: '4%', cellFilter: 'userIdToInitials' }
      ],
      data : [ ]
    };

    // Load translations for column headers
    $scope.taskListGridOptions.columnDefs.forEach(function(col) {
      col.displayName = gettextCatalog.getString(col.displayName !== undefined ? col.displayName : col.name);
    });

    $scope.showTaskDetails = function(task) {
      ngDialog.open({
        template: 'app/tasks/taskDetailsDialog.tmpl.html',
        className: 'ngdialog-theme-default',
        data: {
          'task': task,
          'fromArchive': $scope.archivedTasks
        }
      });
    };

    tasksService.getTasks($scope.archivedTasks, true).then(
      function(tasks) {
        // We sort the tasks in-place: Show newest tasks first.
	tasks.sort(function(a, b) {
          if(a.created_at > b.created_at) {
            return -1;
	  }

	  if(a.created_at < b.created_at) {
            return 1;
	  }

	  // They have the same date, we sort by name.
	  return (a.title < b.title ? -1 : 1);
	});
	
        $scope.taskListGridOptions.data = tasks;
        $scope.errMessage = '';
        $scope.loading = false;
      },
      function(err) {
        $scope.errMessage = gettext('Could not load task list: ') + err;
        $scope.loading = false;
      }
    );

  }])



  /**
   * @ngdoc controller
   * @name dashboardTasks.controller:tasksTableController
   * @description
   *
   * This controller shows a table of data elements that are the results of an OCA
   * analytics and provides functionality to manipulate them (e.g. marking them
   * as real hits or false positives).
   *
   * It uses the {@link learningServices.learningService} to provide "live"
   * machine learning when auditors mark elements as false or real hits.
   *
   */

  .controller('tasksTableController', ['$scope', '$timeout', '$transition$', '$q', '$state', '$location', 'uiGridConstants', 'ngDialog', 'gettext', 'd3', 'tasksService', 'keywordsService', 'auditFormsService', 'learningService', 'excelService', 'printService', 'miscServices', function($scope, $timeout, $transition$, $q, $state, $location, uiGridConstants, ngDialog, gettext, d3, tasksService, keywordsService, auditFormsService, learningService, excelService, printService, miscServices) {
    $scope.id = $transition$.params().id;
    $scope.archivedTasks = !!($state.current.data && $state.current.data.archivedTasks);
    $scope.loading = true;
    $scope.errMessage = '';
    $scope.calcStatus = '';
    $scope.task = {};
    $scope.taskData = [];
    $scope.gridOptions = {};
    $scope.showAuditForms = false;
    $scope.newAuditForms = [];
    $scope.machineEnabled = false;
    $scope.machineEnabledResult = undefined;
    $scope.machineEnabledColumns = [];
    $scope.resultsColumn = undefined;
    $scope.mapResults = [];
    $scope.resultsSelection = [];
    $scope.resultsSelectionIterate = false;
    $scope.keywords = [];
    $scope.thisClassifier = undefined;
    $scope.auditFormsLoaded = false;
    $scope.autoSaveEnabled = true;
    $scope.readOnly = true;
    $scope.excelEnabled = false;



    // Definition of machine learning services - we prepare the necessary functions regardless of whether we are
    // going to use them for this specific task.

    var classifierUpdate = function(entity) {
      console.log('Running classifierUpdate...');

      var resultClass;
      if(entity.result !== undefined) {
        resultClass = (entity.result === $scope.machineEnabledResult ? 'hit' : 'noHit');
      }

      var oldResultClass;
      if(entity._old_result !== undefined) {
        oldResultClass = (entity._old_result === $scope.machineEnabledResult ? 'hit' : 'noHit');
      }

      if(resultClass === oldResultClass) {
        // Nothing has changed
        return;
      }

      if(oldResultClass !== undefined) {
        // Unlearn old data
        console.log('Calling unlearn!');

        invalidateResults();
        $scope.thisClassifier.unlearn([entity._idx]);
        $scope.requestUpdate(false); // update visible entries
      }

      if(resultClass !== undefined) {
        // Add new data
        console.log('Calling train!');
        console.log(entity);

        invalidateResults();
        $scope.thisClassifier.train([{idx: entity._idx, result: resultClass}]);
        $scope.requestUpdate(false);  // update visible entries
      }

      // Update old_result
      entity._old_result = entity.result;
    };

    // Helper function that updates the classifier whenever an entity result has been updated
    var autoSaveTimer;
    var entityResultUpdated = function(entity) {
      // We need to re-draw the grid to make the updated result visible
      if($scope.gridApi) {
        $scope.gridApi.core.queueRefresh();
      }

      // We need to update the classifier if we are machine-enabled
      if($scope.machineEnabled) {
        // De-bounce classifier update
        if(entity._classifierUpdatePromise) {
          $timeout.cancel(entity._classifierUpdatePromise);
        }
        entity._classifierUpdatePromise = $timeout(function() { classifierUpdate(entity); }, 1000);
      }

      // We need to auto-save if this is enabled (de-bounced, at most one save every five seconds)
      if($scope.autoSaveEnabled) {
        // De-bounce auto-save
        if(autoSaveTimer) {
          $timeout.cancel(autoSaveTimer);
        }
        autoSaveTimer = $timeout($scope.saveResults, 5000);
      }
    };


    $scope.updateResult = function(entity, $event) {
      // TODO: Remove debug code
      console.log('updateResult called: ');
      console.log(entity);
      console.log([ $scope.resultsSelectionIterate, $scope.resultsSelection, $scope.resultsSelection.length ]);

      if(!$scope.resultsSelectionIterate || !$scope.resultsSelection || $scope.resultsSelection.length <= 1) {
        // This function is only available if we use the results iteration feature
        return;
      }

      // Stop propagation of event
      $event.stopPropagation();


      var oldResultIdx = $scope.resultsSelection.indexOf(entity.result);
      if(oldResultIdx === -1) {
        // If result is unset or unknown, start with first iteration array entry
        entity.result = $scope.resultsSelection[0];
      } else if(oldResultIdx === ($scope.resultsSelection.length - 1)) {
        // We are at the end of the results selection so we unset the result
        entity.result = undefined;
      } else {
        entity.result = $scope.resultsSelection[oldResultIdx+1];
      }

      entityResultUpdated(entity);
    };

    // When we receive new percentage values, we set them here
    var updateCallback = function(resultData, status) {
      console.log('Received data from worker: ');
      console.log([resultData, status]);

      $scope.$apply(function() {
        $scope.calcStatus = status;
      });

      resultData.forEach(function(d) {
        $scope.taskData[d[0]].guessPercentage = d[1];
        $scope.taskData[d[0]]._resultCurrent = true;
      });

      // We do not fire a data change as this triggers a re-sort,
      // instead we just redraw the data.
      if($scope.gridApi) {
        $scope.gridApi.core.queueRefresh();
      }
    };

    // We invalidate all existing results whenever we re-train or unlearn.
    var invalidateResults = function() {
      $scope.taskData.forEach(function(d) {
        d._resultCurrent = false;
      });
    };


    // Update visible or all.
    $scope.requestUpdate = function(updateAll) {
      var idxes = [];
      if(!$scope.gridApi || updateAll) {
        console.log('Updating all entries...');
        // We update percentage values for all available entries
        for(var i = 0;i < $scope.taskData.length;i++) {
          if(!$scope.taskData[i]._resultCurrent) {
            idxes.push(i);
          }
        }
      } else {
        // We update visible entries
        console.log('Updating visible entries...');
        console.log($scope.gridApi);
        $scope.gridApi.grid.renderContainers.body.renderedRows.forEach(function(row) {
          if(!$scope.taskData[row.entity._idx]._resultCurrent) {
            idxes.push(row.entity._idx);
          }
        });
      }

      console.log('Request update of percentage values...');
      console.log(idxes);
      $scope.thisClassifier.requestUpdate(idxes);
    };

    // Send training reiteration message to classifier.
    $scope.iterateTraining = function() {
      $scope.thisClassifier.requestIterate();
      invalidateResults();
    };


    $scope.updatePercentageSort = function() {
      // Push a data change.
      if($scope.gridApi) {
        $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.EDIT);
      }
    };

    // Save results to the library
    $scope.resultsAreSaving = false;
    $scope.saveResults = function() {
      $scope.resultsAreSaving = true;
      tasksService.saveResults($scope.task, $scope.taskData)
        .then(function() {
          $scope.resultsAreSaving = false;
        });
    };



    ////////////////////////////////////////////////////////////////////////
    // EXCEL EXPORT AND IMPORT
    //

    $scope.downloadExcel = function() {
      var showColumns = $scope.task.dataSpecification.dataFileFormat.columns.filter(function(col) {
        // We show only columns which are visible in the view
        return ($scope.task.dataSpecification.showColumns.indexOf(col.display) !== -1);
      });


      // If we are results-enabled we also export the results column
      if($scope.resultsColumn !== undefined) {
        showColumns.push({
          source: $scope.resultsColumn,
          display: 'result',
          format: 'result'
        });
      }


      // If we are machine-enabled, we also export "Guess"
      if($scope.machineEnabled) {
        showColumns.push({
          source: 'Guess',
          display: 'guessPercentage',
          format: 'percentage'
        });
      }

      // TODO: "result" column should have drop-down to only allow right categories

      excelService.exportDataAsDownload($scope.task.title, $scope.task.description, [
        {
          name: 'OCA',
          title: $scope.task.title,
          columns: showColumns,
          rows: $scope.taskData
        }
      ]);
    };


    $scope.excelFileChanged = function(elm) {
      // TODO: fix this whole thing and replace it with a proper directive!
      // TODO: disable debug data, but then this will not work anymore (see https://docs.angularjs.org/guide/production#disabling-debug-data)

      if(!elm.files[0]) {
        // No file, we just return
        return;
      }

      if(!elm.files[0].name.match(/\.xlsx$/i)) {
        if(!miscServices.translatedConfirm(gettext('This feature only accepts Excel XLSX files. It looks like your file has a different format. Are you sure you want to continue anyways?'))) {
          return;
        }
      }

      excelService.loadData(elm.files[0], ['OCA'])
        .then(function(resultData) {
          // TODO: Remove debug code
          console.log('Received Excel data. Merging:');
          console.log([$scope.task.dataSpecification.dataFileFormat.columns, $scope.taskData, resultData]);

          // We merge the data with the existing task data
          tasksService.mergeResults($scope.task.dataSpecification.dataFileFormat.columns, $scope.taskData, resultData[0], $scope.resultsColumn);

          // TODO: Remove debug code
          console.log('Post-merge:');
          console.log($scope.taskData);

          // TODO: Better notification
          miscServices.translatedAlert(gettext('The result data has been successfully loaded into the current view.'));
        });
    };



    ////////////////////////////////////////////////////////////////////////
    // "PRINT" FEATURE TO PRINT TABLES WITH ALL THEIR AUDIT FORMS
    //

    $scope.printWork = function() {
      var data = {};

      // Details of the given task
      data.taskId = $scope.id;
      data.task = $scope.task;

      // Array of all column names to be displayed in the header row of the table
      data.headerRow = $scope.gridOptions.columnDefs.map(function(colDef) { return colDef.name; });

      // Array of rows which are arrays of column content
      data.content = $scope.taskData.map(function(d) {
        return $scope.gridOptions.columnDefs.map(function(colDef) { return (colDef.field !== undefined ? (d3.ourSettings.formats[colDef.dashboardFormat] ? d3.ourSettings.formats[colDef.dashboardFormat](d[colDef.field]) : d[colDef.field]) : ''); });
      });

      // Gather (and number) all *direct* audit forms
      data.auditForms = [];
      var auditFormPromise = $q.resolve();
      if($scope.showAuditForms) {
        var auditFormColIdx = $scope.gridOptions.columnDefs.findIndex(function(colDef) { return (colDef.name === 'Audit Forms'); });

        // We need to get the audit form definitions so that we can include the right templates
        auditFormPromise = auditFormsService.getAllForms()
          .then(function(auditFormDefs) {

            $scope.taskData.forEach(function(d, contentIdx) {
              if(Array.isArray(d._auditFormData)) {
                d._auditFormData.forEach(function(form) {
                  // Is this audit form created at the given entry? (We only look at *direct* audit forms.)
                  if(form.createdInTask.toString() === $scope.id.toString() && (!!form.archivedTask) === $scope.archivedTasks && form.createdInElement === d._thisKeyword) {
                    data.content[contentIdx][auditFormColIdx] += (data.content[contentIdx][auditFormColIdx] === '' ? '' : ', ') + (data.auditForms.length+1).toString();
                    data.auditForms.push({ 'id': form.id, 'formDef': auditFormDefs[form.auditForm], 'sourceData': form.sourceData, 'resultsData': form.resultsData });
                  }
                });
              }
            });
          });


      }

      // TODO: Remove debug code!
      console.log('Calling openPrintView:');
      console.log(['app/tasks/tablePrintView.tmpl.html', gettext('Print View'), data]);

      return auditFormPromise.then(function() { return printService.openPrintView('app/tasks/tablePrintView.tmpl.html', gettext('Print View'), data); });
    };



    ////////////////////////////////////////////////////////////////////////
    // HERE WE START OBTAINING THE DATA FOR THE GIVEN TASK
    //


    // Obtain workflowRule for the given task
    $scope.workflowRule = 'soft';
    tasksService.getWorkflowRule($scope.id).then(
      function(workflowRule) {
        $scope.workflowRule = workflowRule;
      },
      function(err) {
        console.log('ERROR: An error occured while trying to retrieve the applicable workflowRule for task ID ' + $scope.id.toString() + '.');
        console.log(err);
      }
    );

    // Obtain details of the given task
    tasksService.getTask($scope.id, $scope.archivedTasks)
      .then(function(task) {
        // Set task details on the scope
        $scope.task = task;

        // Keywords
        $scope.keywords = task.dataSpecification.keywords;

        // Details for the results column
        $scope.resultsColumn = task.dataSpecification.resultsColumn;
        if($scope.resultsColumn !== undefined) {
          $scope.mapResults = (Array.isArray(task.dataSpecification.mapResults) ? task.dataSpecification.mapResults : []);
          $scope.resultsSelection = (Array.isArray(task.dataSpecification.resultsSelection) ? task.dataSpecification.resultsSelection : []);
          $scope.resultsSelectionIterate = (!!task.dataSpecification.resultsSelectionIterate) && $scope.resultsSelection.length >= 1;
        }

        // Are we read-only (i.e. status "Done" or "Reviewed" or we are an archived task)
        if(task.status !== 'Done' && task.status !== 'Reviewed' && task.status !== 'Archived' && !$scope.archivedTasks) {
          $scope.readOnly = false;
        }

        // Is Excel export enabled?
        if(task.dataSpecification.excelEnabled) {
          $scope.excelEnabled = true;
        }

        // Are we machine-enabled? (Note that read-only tasks will never be machine-enabled, as they cannot be changed.)
        if(!$scope.readOnly && Array.isArray(task.dataSpecification.machineEnabledColumns) && task.dataSpecification.machineEnabledColumns.length >= 1 && task.dataSpecification.machineEnabledResult !== undefined) {
          $scope.machineEnabled = true;
          $scope.machineEnabledResult = task.dataSpecification.machineEnabledResult;
          $scope.machineEnabledColumns = task.dataSpecification.machineEnabledColumns;
        }

        // Do we show audit forms?
        if(task.dataSpecification.showAuditForms) {
          $scope.showAuditForms = true;

          // If we are not readOnly we also give users the option to add new audit forms
          if(!$scope.readOnly && Array.isArray(task.dataSpecification.newAuditForms)) {
            $scope.newAuditForms = task.dataSpecification.newAuditForms;
          }
        }

        // Read data file for the task data
        return tasksService.getTaskData(task);
      })
      .then(function(taskData) {
        // Do we need to load result data as well? We need to if we are results-enabled.
        if ($scope.resultsColumn !== undefined) {
          return tasksService.loadResults($scope.task, $scope.archivedTasks, taskData);
        } else {
          // If not we can just return the unmodified taskData object.
          return taskData;
        }
      })
      .then(function(taskData) {
        $scope.taskData = taskData;

        // Prepare column definitions based on the showColumns array defined in this tasks
        // data specification.
        var columnDefs = $scope.task.dataSpecification.showColumns.map(function(col) {
          var columnDetails = $scope.task.dataSpecification.dataFileFormat.columns.find(function(d) {
            return (d.display === col);
          });

          var colDef = {
            name: col,
            field: col,
            displayName: col,
            enableCellEdit: false,
            dashboardFormat: columnDetails.format
          };

          if(columnDetails.format === 'integer' || columnDetails.format === 'number') {
            colDef.type = 'number';
          } else if(columnDetails.format === 'date') {
            colDef.type = 'date';
            colDef.cellFilter = 'date:"yyyy-MM-dd"';
          } else if(columnDetails.format === 'time') {
            colDef.type = 'date';
            colDef.cellFilter = 'date:"yyyy-MM-dd HH:mm:ss"';
          }

          return colDef;
        });

        // Are we results-enabled? Then we need to prepare the relevant column.
        if($scope.resultsColumn !== undefined) {
          if(!$scope.readOnly && $scope.resultsSelectionIterate) {
            columnDefs.push({ name: $scope.resultsColumn, displayName: $scope.resultsColumn, field: 'result', enableCellEdit: false, cellTemplate: '<div class="ui-grid-cell-contents result-column" ng-click="grid.appScope.updateResult(row.entity, $event)">{{row.entity.result}}</div>' });
          } else {
            columnDefs.push({ name: $scope.resultsColumn, displayName: $scope.resultsColumn, field: 'result', enableCellEdit: false, cellTemplate: '<div class="ui-grid-cell-contents result-column">{{row.entity.result}}</div>' });
          }
        }

        // Are we machine-enabled? Then we need to prepare the relevant columns.
        if($scope.machineEnabled) {
          columnDefs.push({ name: 'Guess', field: 'guessPercentage', enableCellEdit: false, type: 'number' });
        }

        // Do we want to show audit forms? Then we need to add the corresponding column to the grid
        if($scope.showAuditForms) {
          columnDefs.push({ name:'Audit Forms', enableCellEdit: false, enableFiltering: false, cellTemplate: '<span ng-if="!grid.appScope.auditFormsLoaded">...</span><audit-form-icons ng-if="grid.appScope.auditFormsLoaded" audit-form-data="row.entity._auditFormData" edit-link="' + ($scope.readOnly ? 'false' : 'true') + '" workflow-rule="grid.appScope.workflowRule" new-form-types="grid.appScope.newAuditForms" on-new="grid.appScope.newAuditFormReceived" on-change="grid.appScope.auditFormChanged" on-change-object="row.entity" on-delete="grid.appScope.auditFormDeleted" auto-save="grid.appScope.autoSaveEnabled" keywords="row.entity._auditFormKeywords" source-task-id="grid.appScope.id" source-task-archived="grid.appScope.archivedTasks" source-element="row.entity._thisKeyword" source-data="row.entity">' });
        }

        // We display line numbers
        columnDefs.splice(0, 0, {
          name: 'Line',
          field: '_idx',
          displayName: ' ',
          width: 50,
          dashboardFormat: 'integer',
          type: 'number',
          pinnedLeft: true
        });
        
        // We need to amend the taskData to contain idx values and to calculate their keywords
        var keywordHash = {};
        $scope.taskData.forEach(function(d, idx) {
          d._resultCurrent = false;
          d._idx = idx;

          d._auditFormKeywords = $scope.keywords.map(function(keywordTemplate) {
            return keywordTemplate.replace(/\{\{([^\}]+)\}\}/g, function(match, p1) {
              if(d[p1] !== undefined) {
                return d[p1];
              } else {
                return match;
              }
            });
          });

          d._auditFormKeywords.forEach(function(keyword) {
            if(keywordHash[keyword] === undefined) {
              keywordHash[keyword] = [ d ];
            } else {
              keywordHash[keyword].push(d);
            }
          });

          d._thisKeyword = keywordsService.getDataKey(undefined, d, $scope.task.dataSpecification.dataFileFormat.columns, undefined, undefined);
          d._auditFormData = [];
        });


        // Support function when new audit forms are added (callback)
        $scope.newAuditFormReceived = function(auditForm) {
          // We do not want to map the same audit form multiple times to the same task element
          var alreadyMapped = {};
          auditForm.keywords.forEach(function(keyword) {
            if(keywordHash[keyword]) {
              keywordHash[keyword].forEach(function(taskData) {
                if(!alreadyMapped[taskData._idx] && (!taskData._auditFormData.length || taskData._auditFormData[taskData._auditFormData.length-1] !== auditForm)) {
                  alreadyMapped[taskData._idx] = true;
                  taskData._auditFormData.push(auditForm);
                }
              });
            }
          });
        };

        // Support function when audit forms on this item are changed (callback)
        $scope.auditFormChanged = function(changedForm, allForms, entity) {
          // TODO: Remove debug code
          console.log('auditFormChanged called: ');
          console.log([changedForm, allForms, entity]);

          // When audit form is updated on this entry, we need to recalculate the result...
          // Go through all forms, check which are directly attached, iterate through resultsMap logic.

          var minIdx = Infinity;
          var minResult;
          allForms.forEach(function(form) {
            if(form.createdInTask.toString() === $scope.id.toString() && !form.archivedTask && form.createdInElement === entity._thisKeyword) {
              // This form was directly created on this task element and thus "counts" for the results

              var myResultIdx = $scope.mapResults.findIndex(function(mapObj) {
                // Find index of first mapping that matches this audit form
                return (mapObj.auditForm === form.auditForm && (mapObj.mapFrom === '*' || mapObj.mapFrom === form.result.toString()));
              });

              // TODO: Remove debug code
              console.log('Checking directly added audit form for match: ');
              console.log([form, $scope.mapResults, myResultIdx, minIdx]);

              if(myResultIdx >= 0 && myResultIdx < minIdx) {
                // This is a new "minimal match", so we update the entries result to this new result value
                // If mapTo is given, this will be used as result, otherwise the original result as specified in the
                // audit form will be used.
                minIdx = myResultIdx;
                minResult = ($scope.mapResults[myResultIdx].mapTo === undefined ? form.result : $scope.mapResults[myResultIdx].mapTo);
              }
            }
          });


          // TODO: Remove debug code
          console.log('Setting new result: ' + minResult);

          // Update the entity result column
          entity.result = minResult;

          // Trigger a classifier update if we are machine enabled
          entityResultUpdated(entity);

          // Done.
        };

        // Support function when audit forms are removed
        $scope.auditFormDeleted = function(deletedForm) {
          if(deletedForm.id === undefined) {
            // This form was never saved in the first place, so we do not need to do anything
            return;
          }

          // Otherwise we need to detach it everywhere...
          deletedForm.keywords.forEach(function(keyword) {
            if(keywordHash[keyword]) {
              keywordHash[keyword].forEach(function(taskData) {
                if(Array.isArray(taskData._auditFormData) && taskData._auditFormData.length > 0) {
                  taskData._auditFormData = taskData._auditFormData.filter(function(auditForm) {
                    return auditForm.id !== deletedForm.id;
                  });
                }
              });
            }
          });
        };


        // If audit forms are enabled, we need to retrieve audit form data for them
        if($scope.showAuditForms) {
          // TODO: Remove debug code
          console.log('RECEIVING audit form data for keywords: ');
          console.log($scope.keywords);

          auditFormsService.findFormData($scope.keywords, true)
            .then(function(auditFormData) {
              // TODO: Remove debug code
              console.log('RECEIVED audit form data:');
              console.log(auditFormData);

              // We need to map this to the actual data elements
              auditFormData.forEach($scope.newAuditFormReceived);

              // We have loaded audit forms data
              $scope.auditFormsLoaded = true;
            });
        }


        // If machine learning is enabled, we will start the classifier.
        if($scope.machineEnabled) {
          // TODO: Update here for new multi-class logic
          $scope.thisClassifier = learningService.getOrAddClassifier('tasks-' + $scope.id, $scope.taskData, $scope.machineEnabledColumns, updateCallback);
        }

        // We define the data grid to be displayed.
        $scope.gridOptions = {
          enableSorting: true,
          enableFiltering: true,
          onRegisterApi: function(_gridApi) {
            $scope.gridApi = _gridApi;

            // Only register if machine learning is enabled.
            if($scope.machineEnabled) {
              $scope.gridApi.core.on.scrollEnd(null, function() {
                console.log('scrollEnd fired.');
                $scope.requestUpdate(false);
              });
            }

          },
          scrollDebounce: 300,
          columnDefs: columnDefs,
          minimumColumnSize: 120,
          data : $scope.taskData
        };

        // Loading completed.
        $scope.loading = false;
        $scope.errMessage = '';


        // Helper function to show task details when the button is clicked
        $scope.showTaskDetails = function() {
          ngDialog.open({
            template: 'app/tasks/taskDetailsDialog.tmpl.html',
            className: 'ngdialog-theme-default',
            data: {
              'task': $scope.task,
              'fromArchive': $scope.archivedTasks
            }
          });
        };

      })
      .catch(function(err) {
        $scope.loading = false;

        console.log('An ERROR occurred while trying to retrieve this task:');
        console.log(err);

        if(!$scope.archivedTasks) {
          $scope.errMessage = gettext('An error occurred trying to retrieve this task. Maybe it has been removed or archived.');
        } else {
          $scope.errMessage = gettext('An error occurred trying to retrieve this task from the archive. Maybe it has been removed.');
        }
      })
    ;


    // Helper function to watch for changes in the location hash
    // If the "hash hash" is changed, we will open the audit form given by its form data ID
    var openedAuditForm;
    var triggerAuditFormOpen = function () {
      var formHash = $location.hash();
      var formDataId = parseInt(formHash);

      console.log('locationChangeSuccess triggered for audit form hash: ' + formDataId.toString());

      // Is an audit form already open? Then we need to close it...
      if (openedAuditForm) {
        openedAuditForm.then(
          function (ngDialog) {
            ngDialog.close();
            openedAuditForm = undefined;
          }
        );
      }

      if (!isNaN(formDataId) && formDataId >= 0) {
        openedAuditForm = auditFormsService.showFormData(formDataId);

        // When this dialog is being closed, we need to strip the hash from the URL
        openedAuditForm
          .then(
            function (ngDialog) {
              return ngDialog.closePromise;
            }
          )
          .then(
            function () {
              if ($location.hash() === formHash) {
                $location.hash('');
              }
            }
          );
      }
    };

    triggerAuditFormOpen();  // We also call it when our controller is initialized the first time.
    $scope.$on('$locationChangeSuccess', triggerAuditFormOpen);
  }]);

