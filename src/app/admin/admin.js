'use strict';

/**
 * @ngdoc overview
 * @name dashboardAdmin
 * @description
 *
 * This module provides the administration interface of The Dashboard. It registers the
 * routes in the /admin area and provides controllers for the admin functionality therein.
 */

angular.module('dashboardAdmin', ['ui.router', 'ui.tree', 'ui.bootstrap.datetimepicker', 'dashboardApp', 'dashboardTree', 'usersServices', 'adminServices', 'miscServices', 'fileServices', 'helperDirectives', 'ngDialog', 'gettext'])
  .config(['$stateProvider', function($stateProvider) {
    $stateProvider
    // route for the admin overview
      .state('admin', {
        url: '/admin',
        views: {
          'main@': {
            templateUrl: 'app/admin/index.tmpl.html',
            controller: 'adminIndexController'
          }
        }
      })

      .state('admin.loadLoadfile', {
        url: '/load-loadfile',
        views: {
          'main@': {
            templateUrl: 'app/admin/loadLoadfile.tmpl.html',
            controller: 'loadLoadfileController'
          }
        }
      })

      .state('admin.exportLoadfile', {
        url: '/export-loadfile',
        views: {
          'main@': {
            templateUrl: 'app/admin/exportLoadfile.tmpl.html',
            controller: 'exportLoadfileController'
          }
        }
      })

      .state('admin.tree', {
      url: '/tree',
      views: {
        'main@': {
          templateUrl: 'app/admin/tree.tmpl.html',
          controller: 'adminTreeController'
        }
      }
    })


    .state('admin.dashboardViews', {
      url: '/dashboard-views',
      views: {
        'main@': {
          templateUrl: 'app/admin/dashboardViews.tmpl.html',
          controller: 'treeIndexController'
        }
      }
    })

    // route for editing an individual dashboard
    .state('admin.dashboardViews.details', {
      url:'/:id',
      views: {
        'main@': {
          templateUrl: 'app/admin/dashboardViewsDetails.tmpl.html',
          controller: 'adminDashboardViewsDetailsController'
        }
      }
    })

    .state('admin.datafiles', {
      url: '/datafiles',
      views: {
        'main@': {
          templateUrl: 'app/admin/datafiles.tmpl.html',
          controller: 'adminDatafilesController'
        }
      }
    })

    .state('admin.datafiles.details', {
      url: '/:id',
      views: {
        'main@': {
          templateUrl: 'app/admin/datafilesDetails.tmpl.html',
          controller: 'adminDatafilesDetailsController'
        }
      }
    });
  }])


  /**
   * @ngdoc controller
   * @name dashboardAdmin.controller:loadLoadfileController
   * @description
   *
   * This controller provides all the functionality of the "load loadfile"
   * option in the administrative interface to load "loadfiles".
   *
   * Loadfiles can provide instructions on lists, libraries and list items to be
   * created, changed or removed from SharePoint. They are used to initialize
   * The Dashboard on a new SharePoint, to mass-import data into an enterprise
   * implementation and to move from one version of Dashboard to the next (by
   * incrementally updating the lists as required).
   *
   * Loadfiles can either be specified by their URL (to load loadfiles that
   * have already been uploaded to SharePoint) or by uploading one or more
   * files from the local computer.
   *
   */

  .controller('loadLoadfileController', ['$scope', '$q', '$http', 'gettext', 'usersService', 'miscServices', 'restListService', 'restDocumentService', function($scope, $q, $http, gettext, usersService, miscServices, restListService, restDocumentService) {
    $scope.errMessage = '';
    $scope.hasAccess = false;
    $scope.isLoading = false;
    $scope.isProcessing = false;
    $scope.warningAccepted = false;
    $scope.loadfiles = [];
    $scope.loadLog = [];

    // Check if user has the right access rights
    usersService.isSiteAdmin().then(
      function(res) {
        if(res) {
          $scope.hasAccess = true;
          $scope.errMessage = '';
        } else {
          $scope.hasAccess = false;
          $scope.errMessage = gettext('You need site admin rights on SharePoint to be able to initialize the lists required by Dashboard.');
        }
      }, function(err) {
        $scope.hasAccess = false;
        $scope.errMessage = gettext('An error occurred checking access rights: ') + err;
      }
    );

    // Helper function to process file uploads
    // Gets called when the user selects new files to upload
    $scope.filesChanged = function(elm) {
      var i;

      // TODO: fix this whole thing and replace it with a proper directive!

      // TODO: disable debug data, but then this will not work anymore (see https://docs.angularjs.org/guide/production#disabling-debug-data)

      for(i = 0;i < elm.files.length; i++) {
        $scope.loadfiles.push({
          name: elm.files[i].name,
          url: undefined,
          uploadedFile: elm.files[i]
        });
      }

      $scope.$apply();
    };

    // Helper function to remove selected loadfile
    $scope.removeLoadfile = function(loadfile) {
      var idxToRemove = $scope.loadfiles.indexOf(loadfile);
      if(idxToRemove > -1) {
        $scope.loadfiles.splice(idxToRemove, 1);
      }
    };

    // Helper function to move loadfile up or down
    $scope.moveLoadfile = function(loadfile, moveBy) {
      if($scope.loadfiles.indexOf(loadfile) > -1) {
        var newIndex = Math.max(0, Math.min($scope.loadfiles.indexOf(loadfile) + moveBy, $scope.loadfiles.length));

        // Remove loadfile from array
        $scope.loadfiles.splice($scope.loadfiles.indexOf(loadfile), 1);

        // Re-insert at new index
        $scope.loadfiles.splice(newIndex, 0, loadfile);
      }
    };

    // Helper function to add loadfile URL
    $scope.addLoadfileURL = function(loadfileURL) {
      // TODO: Validate URL
      if(loadfileURL === '') {
        miscServices.translatedAlert(gettext('Please specify a valid URL for the loadfile to be used.'));
      } else {
        $scope.loadfiles.push({
          name: loadfileURL,
          url: loadfileURL,
          uploadedFile: undefined
        });
      }
    };

    // This is where the magic happens: We process all loadfiles in
    // the loadfiles array.
    $scope.processLoadfiles = function() {
      if(!$scope.loadfiles || !$scope.loadfiles.length) {
        miscServices.translatedAlert(gettext('You need to provide at least one loadfile to be processed.'));
        return;
      }

      if(!$scope.warningAccepted) {
        miscServices.translatedAlert(gettext('You need to accept the warning before proceeding.'));
        return;
      }

      $scope.isLoading = true;
      $scope.errMessage = '';
      $scope.loadLog = [];

      var loadfilePromise = $q.resolve();
      var loadfileOperations = [];
      var idStorage = {};

      $scope.loadfiles.forEach(function(loadfile) {
        if(loadfile.uploadedFile) {
          // Get loadfile from uploaded file
          $scope.loadLog.push({
            type: 'info',
            entry: loadfile.name + ': ' + gettext('Getting loadfile from uploaded file.')
          });

          loadfilePromise = loadfilePromise.then(function() {
            // New deferred object
            var deferred = $q.defer();

            var reader = new FileReader();
            reader.addEventListener("loadend", function() {
              // reader.result contains the contents of blob as a string
              var loadfileData;
              try {
                loadfileData = JSON.parse(reader.result);
              } catch(e) { /* Do nothing */ }

              if(!loadfileData || !Array.isArray(loadfileData)) {
                deferred.reject(gettext('The file') + ' ' + loadfile.name + ' is not a valid JSON loadfile.');
                return;
              }

              loadfileOperations = loadfileOperations.concat(loadfileData);
              deferred.resolve(loadfileData);
            });
            reader.readAsText(loadfile.uploadedFile);

            return deferred.promise;
          });

        } else {
          // Get loadfile from URL
          $scope.loadLog.push({
            type: 'info',
            entry: gettext('Getting loadfile from URL') + ': ' + loadfile.url
          });

          loadfilePromise = loadfilePromise.then(
            function() {
              return $http.get(loadfile.url)
                .then(
                  function(res) {
                    if(!res.data || !Array.isArray(res.data)) {
                      return $q.reject(gettext('The file') + ' ' + loadfile.name + ' ' + gettext('is not a valid JSON loadfile.'));
                    }

                    loadfileOperations = loadfileOperations.concat(res.data);
                    return res.data;
                  },
                  function(err) {
                    console.log('Could not retrieve loadfile:');
                    console.log(err);

                    return $q.reject(gettext('Could not retrieve loadfile') + ' ' + loadfile.name + ': ' + gettext('Error') + ' ' + err.status + ': ' + err.statusText);
                  }
                );
            }
          );
        }
      });

      loadfilePromise
        .then(
          function() {
            $scope.isLoading = false;
            $scope.isProcessing = true;
            $scope.currOperation = 0;
            $scope.totalOperations = loadfileOperations.length;

            // Helper function to process individual errors
            var catchError = function(err) {
              $scope.loadLog.push({
                type: 'error',
                entry: err
              });

              if(!miscServices.translatedConfirm(gettext('An error occured while trying to process a loadfile operation. Do you want to continue?'))) {
                return $q.reject(err);
              }
            };

            var processingPromise = $q.resolve();
            loadfileOperations.forEach(function(op) {
              switch(op._action) {
                case 'createList':
                case 'createLibrary':
                  // Ok, now we create the list
                  processingPromise = processingPromise.then(function() {
                    // We create the list or library on the SharePoint database
                    $scope.loadLog.push({
                      type: 'info',
                      entry: gettext('Creating') + ' ' + (op._action === 'createLibrary' ? gettext('library') : gettext('list')) + ' "' + op.Title + '"'
                    });

                    // Creating document libraries differs from creating regular SharePoint lists only in the
                    // "create" step where we need to set a different base template. We thus call the
                    // right method here depending on whether it is a library or regular list.
                    if(op._action === 'createLibrary') {
                      return restDocumentService.createLibrary(op).catch(catchError);
                    } else {
                      return restListService.createList(op).catch(catchError);
                    }
                  });

                  // Ok, now we have to create each field on the list (chained as well)
                  op.fields.forEach(function(f) {
                    processingPromise = processingPromise.then(function() {
                      // Process field - one field after the other
                      $scope.loadLog.push({
                        type: 'info',
                        entry: gettext('Adding field') + ' "' + f.Title + '"'
                      });

                      // We need to use the right function depending on whether this is a library.
                      if(op._action === 'createLibrary') {
                        return restDocumentService.createLibraryField(op.Title, f).catch(catchError);
                      } else {
                        return restListService.createListField(op.Title, f).catch(catchError);
                      }

                    });
                  });

                  break;


                case 'deleteList':
                case 'deleteLibrary':
                  // SharePoint is strange: You cannot delete a list, if this list depends on another
                  // list (yes, this is backwards).

                  // This is why we have to iterate through all the list fields and delete any existing
                  // Lookup fields before we can delete the list itself.
                  processingPromise = processingPromise
                    .then(function() {
                      $scope.loadLog.push({
                        type: 'info',
                        entry: gettext('Deleting') + ' ' + (op._action === 'deleteLibrary' ? gettext('library') : gettext('list')) + ' "' + op.Title + '"'
                      });

                      $scope.loadLog.push({
                        type: 'info',
                        entry: gettext('Searching lookup fields on ') + ' ' + (op._action === 'deleteLibrary' ? gettext('library') : gettext('list')) + ' "' + op.Title + '"'
                      });

                      return restListService.getListFields(undefined, op.Title).then(
                        function(res) {
                          var myPromise;
                          var primaryLookupFieldIds = [];
                          var secondaryLookupFieldIds = [];

                          if(res.data && res.data.d && res.data.d.results && Array.isArray(res.data.d.results)) {
                            myPromise = $q.resolve();

                            res.data.d.results.forEach(function(f) {
                              if(f.FieldTypeKind === 7 && f.CanBeDeleted) {
                                if(f.PrimaryFieldId) {
                                  // This is a secondary field.
                                  secondaryLookupFieldIds.push(f.Id);
                                } else {
                                  // This is a primary field.
                                  primaryLookupFieldIds.push(f.Id);
                                }
                              }
                            });

                            // First we need to delete the secondary lookup fields as they depend on the primary fields...
                            // Then we can delete the primary lookup fields.
                            $scope.loadLog.push({
                              type: 'info',
                              entry: gettext('Deleting lookup fields on ') + ' ' + (op._action === 'deleteLibrary' ? gettext('library') : gettext('list')) + ' "' + op.Title + '"'
                            });

                            var lookupFieldIds = secondaryLookupFieldIds.concat(primaryLookupFieldIds);
                            lookupFieldIds.forEach(function(fieldId) {
                              myPromise = myPromise.then(function() {
                                return restListService.deleteListField(op.Title, fieldId);
                              });
                            });

                          } else {
                            console.log(res);
                            myPromise = $q.reject(gettext('List field list is not in expected format.'));
                          }

                          return myPromise;
                        }
                      ).catch(catchError);
                    })

                    // Ok, we now can delete the list
                    .then(function() {
                      $scope.loadLog.push({
                        type: 'info',
                        entry: gettext('Now we can delete the ') + ' ' + (op._action === 'deleteLibrary' ? gettext('library') : gettext('list')) + ' "' + op.Title + '"'
                      });

                      return restListService.deleteList(op.Title).catch(catchError);
                    });
                  break;


                case 'addLibraryField':
                  $scope.loadLog.push({
                    type: 'info',
                    entry: gettext('Adding field') + ' "' + op.field.Title + '" ' + gettext('to library') + ' "' + op.Title + '"'
                  });

                  processingPromise = processingPromise.then(function() {
                    return restDocumentService.createLibraryField(op.Title, op.field).catch(catchError);
                  });
                  break;


                case 'addListField':
                  $scope.loadLog.push({
                    type: 'info',
                    entry: gettext('Adding field') + ' "' + op.field.Title + '" ' + gettext('to list') + ' "' + op.Title + '"'
                  });

                  processingPromise = processingPromise.then(function() {
                    return restListService.createListField(op.Title, op.field).catch(catchError);
                  });
                  break;


                case 'removeField':
                  $scope.loadLog.push({
                    type: 'info',
                    entry: gettext('Removing field') + ' "' + op.fieldTitle + '" ' + gettext('from') + ' "' + op.Title + '"'
                  });

                  processingPromise = processingPromise.then(function() {
                    return restListService.deleteListFieldByTitle(op.Title, op.fieldTitle).catch(catchError);
                  });

                  break;


                case 'changeField':
                  if(op.Title === undefined || op.fieldTitle === undefined || !op.field) {
                    $scope.loadLog.push({
                      type: 'warning',
                      entry: gettext('Skipping changeField because not all required values (Title, FieldTitle, field) have been specified. See console for details.')
                    });

                    console.log('WARNING: Skipping changeField because not all required values (Title, FieldTitle, field) have been specified:');
                    console.log(op);
                    break;
                  }

                  processingPromise = processingPromise.then(function() {
                    return restListService.updateListField(op.Title, op.field, op.fieldTitle).catch(catchError);
                  });

                  break;


                case 'addEntry':
                  if(op._endpoint === undefined && op._listTitle === undefined) {
                    $scope.loadLog.push({
                      type: 'warning',
                      entry: gettext('Skipping addEntry because neither endpoint nor listTitle has been specified. See console for details.')
                    });

                    console.log('WARNING: Skipping addEntry because neither endpoint nor listTitle has been specified:');
                    console.log(op);
                    break;
                  }

                  processingPromise = processingPromise.then(function() {
                    var newItem = {};

                    // Iterate over all keys, replace stored IDs, do not map internal data.
                    var requiresRemapping = [];
                    Object.keys(op).forEach(function(k) {
                      if(k.substring(0, 1) !== '_') {
                        if((typeof op[k]) === 'string') {
                          // For strings, we replace the 'magic string' for stored IDs...
                            
                          newItem[k] = op[k].replace(/{{(.+?)}}/g, function(match, p1) {
                            return (idStorage[p1] !== undefined ? idStorage[p1] : match);
                          });
                          
                          if(op._storeID && newItem[k].indexOf('{{' + op._storeID + '}}') !== -1) {
                              // This item references its own ID!
                              // But we do not know this yet, so we need to
                              // remove that key and keep it for later.
                              requiresRemapping.push({ key: k, value: newItem[k] });
                              newItem[k] = null;
                          }
                        } else {
                          newItem[k] = op[k];
                        }
                      }
                    });
                    return restListService.addItem(op._endpoint, newItem, op._listTitle).then(
                      function(addResult) {
                        if(op._storeID) {
                          idStorage[op._storeID] = addResult.data.d.Id;
                          
                          if(requiresRemapping.length >= 1) {
                            // We need to remap with the new ID and then update the item again
                            newItem.Id = addResult.data.d.Id;
                            var re = new RegExp('{{' + op._storeID + '}}', 'g');
                            requiresRemapping.forEach(function(el) {
                              newItem[el.key] = el.value.replace(re, newItem.Id);
                            });
                            
                            return restListService.updateItem(op._endpoint, newItem.Id, newItem, op._listTitle);
                          }
                        }           
                     
                      }
                    ).catch(catchError);
                  });
                  break;


                case 'addFile':
                  if(op._endpoint === undefined) {
                    $scope.loadLog.push({
                      type: 'warning',
                      entry: gettext('Skipping addFile because no endpoint has been specified. See console for details.')
                    });

                    console.log('WARNING: Skipping addFile because no endpoint has been specified:');
                    console.log(op);
                    break;
                  }

                  processingPromise = processingPromise.then(function () {
                      // We need to get the file that is referenced in the URL
                      return $http.get(op._sourceUrl, { responseType: 'arraybuffer' }).catch(catchError);
                    })
                    .then(function (res) {
                      if (!res.data) {
                        return $q.reject(gettext('The file') + ' ' + op._sourceUrl + ' ' + gettext('could not be retrieved.'));
                      }

                      var newItem = {};

                      // Iterate over all keys, replace stored IDs, do not map internal data.
                      var requiresRemapping = [];
                      Object.keys(op).forEach(function (k) {
                        if (k.substring(0, 1) !== '_') {
                          if ((typeof op[k]) === 'string') {
                            // For strings, we replace the 'magic string' for stored IDs...

                            newItem[k] = op[k].replace(/{{(.+?)}}/g, function (match, p1) {
                              return (idStorage[p1] !== undefined ? idStorage[p1] : match);
                            });

                            if (op._storeID && newItem[k].indexOf('{{' + op._storeID + '}}') !== -1) {
                              // This item references its own ID!
                              // But we do not know this yet, so we need to
                              // remove that key and keep it for later.
                              requiresRemapping.push({key: k, value: newItem[k]});
                              newItem[k] = null;
                            }
                          } else {
                            newItem[k] = op[k];
                          }
                        }
                      });

                      return restDocumentService.addArrayBufferAsFile(op._endpoint, op._filename, newItem, res.data, !!op._overwrite).then(
                        function(addResult) {
                          if (op._storeID) {
                            idStorage[op._storeID] = addResult.data.d.Id;

                            if (requiresRemapping.length >= 1) {
                              // We need to remap with the new ID and then update the item again
                              newItem.Id = addResult.data.d.Id;
                              var re = new RegExp('{{' + op._storeID + '}}', 'g');
                              requiresRemapping.forEach(function (el) {
                                newItem[el.key] = el.value.replace(re, newItem.Id);
                              });

                              return restListService.updateItem(op._endpoint, newItem.Id, newItem);
                            }
                          }
                        }
                      ).catch(catchError);
                    });
                  break;

                case 'removeEntry':
                  if(op._endpoint === undefined && op._listTitle === undefined) {
                    $scope.loadLog.push({
                      type: 'warning',
                      entry: gettext('Skipping removeEntry because neither endpoint nor listTitle has been specified. See console for details.')
                    });

                    console.log('WARNING: Skipping removeEntry because neither endpoint nor listTitle has been specified:');
                    console.log(op);
                    break;
                  }

                  if(!op.searchQuery) {
                    $scope.loadLog.push({
                      type: 'warning',
                      entry: gettext('Skipping removeEntry because no searchQuery has been specified. See console for details.')
                    });

                    console.log('WARNING: Skipping removeEntry because neither endpoint nor listTitle has been specified:');
                    console.log(op);
                    break;
                  }

                  processingPromise = processingPromise.then(function() {
                    return restListService.deleteFilteredItems(op._endpoint, op.searchQuery, op._listTitle).catch(catchError);
                  });

                  break;



                case 'changeEntry':
                  if(op._endpoint === undefined && op._listTitle === undefined) {
                    $scope.loadLog.push({
                      type: 'warning',
                      entry: gettext('Skipping changeEntry because neither endpoint nor listTitle has been specified. See console for details.')
                    });

                    console.log('WARNING: Skipping changeEntry because neither endpoint nor listTitle has been specified:');
                    console.log(op);
                    break;
                  }

                  if(!op.searchQuery) {
                    $scope.loadLog.push({
                      type: 'warning',
                      entry: gettext('Skipping changeEntry because no searchQuery has been specified. See console for details.')
                    });

                    console.log('WARNING: Skipping changeEntry because neither endpoint nor listTitle has been specified:');
                    console.log(op);
                    break;
                  }

                  processingPromise = processingPromise.then(function() {
                    return restListService.updateFilteredItems(op._endpoint, op.searchQuery, op.entry, op._listTitle).catch(catchError);
                  });

                  break;


                default:
                  $scope.loadLog.push({
                    type: 'warning',
                    entry: gettext('Found operation without valid _action. See console for details.')
                  });
                  console.log('WARNING: Found operation without valid _action:');
                  console.log(op);
              }

              processingPromise = processingPromise.then(
                function() {
                  $scope.currOperation++;
                }
              );
            });

            return processingPromise;
          }
        )
        .then(
          function() {
            $scope.isProcessing = false;

            $scope.loadLog.push({
              type: 'success',
              entry: gettext('All Loadfiles have been successfully loaded into the system.')
            });

            miscServices.translatedAlert(gettext('All Loadfiles have been successfully loaded into the system.'));
          },
          function(err) {
            $scope.isProcessing = false;
            $scope.isLoading = false;
            $scope.errMessage = err;

          }
        );
    };

  }])




  /**
   * @ngdoc controller
   * @name dashboardAdmin.controller:exportLoadfileController
   * @description
   *
   * This controller provides all the functionality of the "load loadfile"
   * option in the administrative interface to load "loadfiles".
   *
   * Loadfiles can provide instructions on lists, libraries and list items to be
   * created, changed or removed from SharePoint. They are used to initialize
   * The Dashboard on a new SharePoint, to mass-import data into an enterprise
   * implementation and to move from one version of Dashboard to the next (by
   * incrementally updating the lists as required).
   *
   * Loadfiles can either be specified by their URL (to load loadfiles that
   * have already been uploaded to SharePoint) or by uploading one or more
   * files from the local computer.
   *
   */

  .controller('exportLoadfileController', ['$scope', '$q', '$http', 'gettext', 'usersService', 'restListService', 'FileSaver', function($scope, $q, $http, gettext, usersService, restListService, FileSaver) {
    $scope.errMessage = '';
    $scope.hasAccess = false;
    $scope.isProcessing = false;
    $scope.exportLog = [];
    $scope.dashboardLists = [];
    $scope.otherLists = [];
    $scope.currList = 0;
    $scope.totalLists = 0;

    // Check if user has the right access rights
    usersService.isSiteAdmin().then(
      function (res) {
        if (res) {
          $scope.hasAccess = true;
          $scope.errMessage = '';
        } else {
          $scope.hasAccess = false;
          $scope.errMessage = gettext('You need site admin rights on SharePoint to be able to initialize the lists required by Dashboard.');
        }
      }, function (err) {
        $scope.hasAccess = false;
        $scope.errMessage = gettext('An error occurred checking access rights: ') + err;
      }
    );

    // Identify dashboard lists from the available endpoints list
    var listEndpoints = restListService.getListEndpoints();
    $scope.dashboardLists = Object.keys(listEndpoints).map(function(endpoint) {
      return {
        'endpoint': endpoint,
        'listURL': listEndpoints[endpoint],
        'isSelected': false,
        'status': ''
      };
    });

    // "Select all" functionality for list checkboxes
    $scope.toggleAll = function() {
      var newVal = $scope.toggleAllValue;

      $scope.dashboardLists.forEach(function(d) {
        d.isSelected = newVal;
      });
    };

    // Add and remove other lists
    $scope.removeList = function(l) {
      var myIdx = $scope.otherLists.indexOf(l);
      if(myIdx > -1) {
        $scope.otherLists.splice(myIdx, 1);
      }
    };

    $scope.addList = function(newlistTitle) {
      $scope.otherLists.push({
        'endpoint': undefined,
        'listTitle': newlistTitle,
        'isSelected': true,
        'status': ''
      });
    };

    // Ok, this button is where all the magic happens - it creates the export file and prepares it for download
    $scope.createLoadfile = function() {
      var selectedLists = $scope.dashboardLists.concat($scope.otherLists).filter(function(l) {
        return l.isSelected;
      });

      $scope.errMessage = '';
      $scope.isProcessing = true;
      $scope.currList = 0;
      $scope.totalLists = selectedLists.length;

      if(selectedLists.length < 1) {
        // No lists have been selected => cancel.
        $scope.errMessage = gettext('You need to select at least one SharePoint list that should be used to create the loadfile.');
        $scope.isProcessing = false;
        return;
      }

      // Ok, let's go -> iterate over all the lists that should be exported. To avoid overloading the servers we process one
      // list after the other.
      var processPromise = $q.resolve();
      var loadfileTasks = [ '[' ];
      var tempListData = {};

      var listNameCache = {};

      selectedLists.forEach(function(l) {
        processPromise = processPromise
          .then(function() {
            $scope.currList++;
            l.status = gettext('Loading metadata...');

            // As a first step, we will get the details of the list to be added.
            return restListService.getListInfo(l.endpoint, l.listTitle);

          })
          .then(function(res) {
            // Is this a list or a document library?
            if(res.data.d.BaseTemplate === 101) {
              // This is a document library!
              tempListData = {
                '_action': 'createLibrary',
                'Title': res.data.d.Title,
                'Description': res.data.d.Description,
                'fields': [],
                'EnableVersioning': !!res.data.d.EnableVersioning
              };
            } else {
              // This is just a list.
              tempListData = {
                '_action': 'createList',
                'Title': res.data.d.Title,
                'Description': res.data.d.Description,
                'fields': [],
                'EnableVersioning': !!res.data.d.EnableVersioning
              };
            }


            // Ok, now we can retrieve the fields of the list to be created...
            return restListService.getListFields(l.endpoint, l.listTitle);
          })
          .then(function(res) {
            // For lookup fields, we need to modify the original lookup field entry to add all additional columns
            // that might be requested.
            var lookupFields = {};

            // Promises for completion of lookup fields.
            var lookupFieldPromise = $q.resolve();

            // Ok, we have retrieved the details of the list fields to create this list. We iterate over all fields.
            res.data.d.results
              .filter(function(field) {
                // We ignore fields which are included in the basetype (FromBaseType = true) or are
                // hidden (Hidden = true).
                return (!field.FromBaseType && !field.Hidden);
              })
              .forEach(function(field) {
                // We format each field so that it corresponds with what the loadfile expects
                var fieldDef = {};

                [ 'Title',             // Fixed list of values supported by the loadfile
                  'FieldTypeKind',
                  'Required',
                  'Indexed',
                  'EnforceUniqueValues',
                  'StaticName',
                  'DefaultValue',
                  'LookupFieldName',
                  'RichText',
                  'NumberOfLines',
                  'DisplayFormat',
                  'FillInChoice',
                  'SelectionMode'].forEach(function(k) {
                    fieldDef[k] = field[k];
                 });

                if(field.FieldTypeKind === 6) {
                  // We need to reformat the Choices array...
                  fieldDef.Choices = field.Choices.results;
                }  else if(field.FieldTypeKind === 7 && field.LookupList !== undefined) {
                  // Is this a lookup field?
                  if (!field.PrimaryFieldId) {
                    // This is the main lookup field - this is where all the magic happens.
                    fieldDef.LookupOtherColumns = [];
                    lookupFields[field.Id] = fieldDef;

                    // We need to get the list name for the lookup list, which is specified by its GUID in
                    // LookupList (where it is encapsulated by { and }, which we need to remove)...
                    var lookupListGUID = field.LookupList.slice(1,-1);

                    lookupFieldPromise = lookupFieldPromise
                      .then(function() {
                        if(listNameCache[lookupListGUID] !== undefined) {
                          fieldDef.LookupListName = listNameCache[lookupListGUID];
                        } else {
                          // TODO: Remove debug code...
                          console.log('Getting list name for GUID ' + lookupListGUID + '...');

                          return restListService.getListInfo(undefined, undefined, lookupListGUID)
                            .then(function(res) {
                              listNameCache[lookupListGUID] = res.data.d.Title;
                              fieldDef.LookupListName = res.data.d.Title;
                            });
                        }
                      });
                  } else {
                    // This is the secondary lookup field. We can just add it to the original field.
                    if(lookupFields[field.PrimaryFieldId]) {
                      lookupFields[field.PrimaryFieldId].LookupOtherColumns.push(field.LookupField);
                    }

                    // This field does not need to be added as its own field, we thus skip the push.
                    return;
                  }
                }

                tempListData.fields.push(fieldDef);
              });


            // We need to wait for all lookup fields to be completed prior to continuing...
            return lookupFieldPromise
              .then(function() {
                // We add the loadfile task that creates this list to the loadfile.
                loadfileTasks.push(JSON.stringify(tempListData) + ',\n');

                // Ok, we are done with retrieving the list details for creating the list.
                // We can now retrieve all list elements...
                l.status = gettext('Loading elements...');
                return restListService.getMergedAllItems(l.endpoint, undefined, l.listTitle);
              });

            // Done with this step.
          })
          .then(function(res) {
            // Ok, we have now retrieved all list items and can create a new task in our loadfile for each
            // one of them.

            res.data.d.results.forEach(function(item) {
              var loadfileItem = {
                '_action': 'addEntry',
                'Title': item.Title,
                '_storeID': tempListData.Title + ':' + item.Id
              };

              if(l.endpoint) {  // Either endpoint or listTitle needs to be provided
                loadfileItem._endpoint = l.endpoint;
              } else {
                loadfileItem._listTitle = l.listTitle;
              }

              // We provide values for each of the fields we have defined above.
              tempListData.fields.forEach(function(field) {
                if(field.FieldTypeKind === 7) {
                  // For lookup fields, we need to reference the element defined above
                  if(item[field.Title + 'Id']) {
                    loadfileItem[field.Title + 'Id'] = '{{' + field.LookupListName + ':' + item[field.Title + 'Id'] + '}}';
                  }
                } else {
                  loadfileItem[field.Title] = item[field.Title];
                }
              });

              // We push the new entry to the loadfile
              loadfileTasks.push(JSON.stringify(loadfileItem) + ',\n');
            });

            // Ok, we are done and can complete this list item.
            l.status = gettext('Done.');

          });
      });

      // Ok, we are done. Either successfully or with an error.
      processPromise
        .then(
          function() {
            // Done successfully.
            // Now we need to actually prepare the file vor download.
            loadfileTasks[loadfileTasks.length-1] = loadfileTasks[loadfileTasks.length-1].slice(0, -2);  // Remove final comma
            loadfileTasks.push(']');
            var blob = new Blob(loadfileTasks, {type: "text/plain;charset=utf-8"});
            FileSaver.saveAs(blob, "loadfileExport.js");

            // Done.
            $scope.errMessage = '';
            $scope.isProcessing = false;
          },
          function(err) {
            $scope.errMessage = err;
            $scope.isProcessing = false;
          }
        );

    };

  }])


  /**
   * @ngdoc controller
   * @name dashboardAdmin.controller:adminTreeController
   * @description
   *
   * This controller provides all the functionality for managing the Audit Universe (the hierarchical tree that
   * is at the core of the Ongoing Risk Assessment functionality). It supports adding, changing and reordering tree
   * elements.
   *
   * You can access the relevant admin interface at the `/admin/tree` route.
   *
   */

  .controller('adminTreeController', ['$scope', '$q', 'gettext', 'gettextCatalog', 'adminService', 'usersService', 'treeService', 'miscServices', function($scope, $q, gettext, gettextCatalog, adminService, usersService, treeService, miscServices) {
    $scope.errMessage = '';
    $scope.hasAccess = false;
    $scope.isLoading = true;
    $scope.isProcessing = false;
    $scope.tree = [];

    var loadingPromises = [];

    // Check if user has the right access rights
    // TODO: should only check if user has full rights on tree list
    loadingPromises.push(usersService.isSiteAdmin().then(
      function(res) {
        if(res) {
          $scope.hasAccess = true;
          $scope.errMessage = '';
        } else {
          $scope.hasAccess = false;
          $scope.errMessage = gettext('You need edit rights to The Dashboard\'s configuration to be able to modify the Audit Universe.');
        }
      }, function(err) {
        $scope.hasAccess = false;
        return gettext('An error occurred checking access rights: ') + err;
      }
    ));

    // Helper function to recursively load edit elements to tree
    // Original: stores original values to identify changes
    // isNew: true if the entry is a new element
    // dataKey: incrementing global key to identify object DOM elements

    var dataKey = 1;
    var infiniteDateApprox = (new Date('2999-01-01T01:00:00Z')).getTime();
    var addHelperElements = function(node) {
      node.original = {
        title: node.title,
        valid_from: node.valid_from,
        valid_to: node.valid_to,
        parent: node.parent,
        orderAsc: node.orderAsc,
	hasDashboard: node.hasDashboard
      };

      node.isNew = false;

      // valid_to_editor is undefined when it is 2999 to avoid confusing the datetimepicker
      node.valid_to_editor = (node.valid_to > infiniteDateApprox ? undefined : node.valid_to);

      // dataKey to identify DOM elements
      node.dataKey = dataKey;
      dataKey = dataKey + 1;

      // Call recursively on child nodes
      if(node.nodes && Array.isArray(node.nodes)) {
        node.nodes.forEach(addHelperElements);
      }
    };

    // Load existing Audit Universe tree
    loadingPromises.push(
      treeService.getFullHierarchy().then(
        function(treeStart) {
          // Add original elements to tree (recursively)
          addHelperElements(treeStart);

          // Expand root element
          treeStart.showDetails = true;

          // Set tree
          $scope.tree = [ treeStart ];
          $scope.isLoading = false;


          // TODO: remove debug code
          console.log('Tree has been loaded:');
          console.log($scope.tree);
        }
      )
    );

    // Has everything loaded or an error occured? Then set isLoading to false.
    $q.all(loadingPromises).then(
      function() {
        $scope.isLoading = false;
      }, function(err) {
        $scope.isLoading = false;
        $scope.errMessage = (err ? err : gettext('An unknown error occured.'));
      }
    );

    // Helper function to update valid_to from valid_to_editor
    $scope.updateValidTo = function(data) {
      data.valid_to = (data.valid_to_editor ? data.valid_to_editor : new Date('2999-12-31T12:00:00.000Z'));
    };

    // Helper function to trigger details view
    $scope.toggleDetails = function(data) {
      data.showDetails = !data.showDetails;
    };

    // Filter for active nodes
    $scope.filterActive = function(treeItem) {
      return (!$scope.onlyActive || treeItem.active);
    };

    // Helper function to identify if node is active based on current values of valid_from / valid_to
    // Note that this can differ from value of .active property, as this property will only be updated
    // for saves. (We do not want to disappear tree items immediately after new dates have been entered.)
    $scope.isActive = function(data) {
      // Return current activity flag of node
      var currDatetime = (new Date()).getTime();
      return (data.valid_from.getTime() <= currDatetime && data.valid_to.getTime() > currDatetime);
    };


    // Helper function to create new node
    $scope.newNode = function(data) {
      if(!data.nodes || !Array.isArray(data.nodes)) {
        data.nodes = [];
      }

      data.nodes.push({
        title: gettextCatalog.getString('New node'),
        valid_from: new Date(),
        valid_to: new Date('2999-12-31T12:00:00.000Z'),
        valid_to_editor: undefined,
        original: { },
        active: true,
        isNew: true,
        showDetails: false,
        dataKey: dataKey,
	hasDashboard: true
      });

      dataKey = dataKey + 1;
    };


    // Helper function to delete node that has not yet been saved (i.e. is new)
    $scope.deleteNode = function(data) {
      if(!data.isNew) {
        miscServices.translatedAlert(gettext('You can only remove nodes that are not yet stored in the database.'));
        return false;
      }

      // You also cannot delete nodes with children
      if(data.nodes && Array.isArray(data.nodes) && data.nodes.length >= 1) {
        miscServices.translatedAlert(gettext('You cannot remove nodes that have active children. Remove or re-arrange the children first.'));
        return false;
      }

      // Actually do the delete
      var recursiveFindMe = function(start) {
        if(start.nodes && Array.isArray(start.nodes)) {
          return start.nodes.some(function(n, idx) {
            if(n === data) {
              start.nodes.splice(idx, 1);
              return true;
            } else {
              return recursiveFindMe(n);
            }
          });
        }
      };

      if(!recursiveFindMe($scope.tree[0])) {
        console.log('Error: The given element was not found in the tree.');
      }

    };


    // Helper function to recalculate orderAsc values (called recursively)
    var recalculateOrder = function(startAt, node, parentId) {
      var currOrderAsc = startAt;

      // Sets orderAsc for this node
      node.orderAsc = currOrderAsc;

      // set parent ID for this node
      node.parent = parentId;

      currOrderAsc = currOrderAsc + 1;

      if(node.nodes && Array.isArray(node.nodes)) {
        node.nodes.forEach(function(n) {
          currOrderAsc = recalculateOrder(currOrderAsc, n, node.id);
        });
      }

      return currOrderAsc;
    };

    // Helper function to obtain array of changed tree entries (called recursively)
    var findChanges = function(changes, newNodes, node) {
      // TODO: Remove debug code
      console.log('findChanges running on "%s"...', node.title);

      // Has this node been added or changed?
      if(node.isNew) {
        // Node is new.
        newNodes.push(node);
      } else if(
           node.title !== node.original.title ||
           node.valid_from.getTime() !== node.original.valid_from.getTime() ||
           node.valid_to.getTime() !== node.original.valid_to.getTime() ||
           node.parent !== node.original.parent ||
           node.orderAsc !== node.original.orderAsc ||
	         node.hasDashboard !== node.original.hasDashboard
      ) {
        // Yes, node has been changed.
        changes.push(node);
      }

      // Iterate over all child nodes.
      if(node.nodes && Array.isArray(node.nodes)) {
        node.nodes.forEach(function(n) { findChanges(changes, newNodes, n); });
      }
    };


    // SAVE function to save changes made
    $scope.saveChanges = function() {
      // Confirmation dialog with warning message
      if(!miscServices.translatedConfirm(gettext('Are you sure you want to SAVE your changes? This action cannot be undone.'))) { return; }

      // Start processing
      $scope.isProcessing = true;

      // Recalculate all "orderAsc" and "parent" values
      recalculateOrder(1, $scope.tree[0], null);

      // TODO - Remove debug code
      console.log('Saving changes for modified tree:');
      console.log($scope.tree);

      // Identify changes to tree by comparing original with new values (recursively)
      var changes = [];
      var newNodes = [];
      findChanges(changes, newNodes, $scope.tree[0]);

      // For all new entries, create them (do this first such that new parents exist)
      // Note that the logic above will read the new entries in the right order, such that higher-level entries will
      // be created first.
      var changePromise = $q.resolve();

      newNodes.forEach(function(node) {
        changePromise = changePromise
          .then(function() {
            // TODO: Remove debug code
            console.log('Adding new element "%s"...', node.title);

             return treeService.addElement(node);
          })
          .then(function(newNode) {

            // TODO: Remove debug code
            console.log('New element "%s" added with ID "%s".', node.title, newNode.id);

             // Update ID of the new node
             node.id = newNode.id;
             node.isNew = false;

             // Update activity flag of node
            node.active = $scope.isActive(node);

             // Update original (this node is now no longer changed)
             node.original = {
               title: node.title,
               valid_from: node.valid_from,
               valid_to: node.valid_to,
               parent: node.parent,
               orderAsc: node.orderAsc,
	       hasDashboard: node.hasDashboard
             };

             // We need to update the parent ID for any childs
             if(node.nodes && Array.isArray(node.nodes)) {
               node.nodes.forEach(function (child) {
                 child.parent = newNode.id;
               });
             }

             // Done.
          });
      });


      // For all changed entries, update database
      changes.forEach(function(node) {
        changePromise = changePromise
          .then(function() {
             // TODO: Remove debug code
            console.log('Updating changed element "%s"...', node.title);

             return treeService.changeElement(node);
          })
          .then(function() {
            // Update activity flag of node
            node.active = $scope.isActive(node);

            // TODO: Remove debug code
            console.log('Updated changed element "%s".', node.title);

            // Update original (this node is now no longer changed)
            node.original = {
              title: node.title,
              valid_from: node.valid_from,
              valid_to: node.valid_to,
              parent: node.parent,
              orderAsc: node.orderAsc,
	      hasDashboard: node.hasDashboard
            };
          });
      });

      // TODO - ensure consistency when someone else also modifies database at same time!

      // Update form as pristine and show message.
      changePromise.then(
        function() {
          // TODO: Remove debug code
          console.log('saveChanges completed successfully.');

          // Success - update form as pristine, set isProcessing to false and show message.
          $scope.isProcessing = false;
          $scope.treeForm.$setPristine();
          miscServices.translatedAlert(gettext('All changes to the Audit Universe have been saved.'));
        },
        function(err) {
          // TODO: Remove debug code
          console.log('saveChanges reported an error.');

          // An error occured - display error message.
          $scope.isProcessing = false;
          $scope.errMessage = gettext('An error occured saving your Audit Universe changes: ') + err;
        }
      );
    };

    // CANCEL function to reset changes
    $scope.cancelChanges = function() {
      // Confirmation dialog with warning message
      if(!miscServices.translatedConfirm(gettext('Are you sure you want to CANCEL all your changes? All changes will be lost. This action cannot be undone.'))) { return; }

      // Set is loading to true
      $scope.isLoading = true;

      // Reload tree from database
      treeService.getFullHierarchy().then(
        function(treeStart) {
          $scope.tree = [ treeStart ];
          $scope.isLoading = false;

          // Set form to pristine
          $scope.treeForm.$setPristine();
        }
      );
    };

    // Helper function to change form to not pristine when element has been dropped
    $scope.treeOptions = {
      dropped: function() {
        $scope.treeForm.$pristine = false;
        return true;
      }
    };

  }])



  /**
   * @ngdoc controller
   * @name dashboardAdmin.controller:adminDashboardViewsDetailsController
   * @description
   *
   * This controller provides the admin view to edit the individual ORA dashboards associated with the
   * individual tree elements. It allows for reordering of the view components, adding new view elements
   * and changing the options for these elements.
   *
   * You can access the relevant admin interface at the `/admin/dashboard-views/:id` route, where `:id`
   * is the internal ID of the tree element whose dashboard you want to modify.
   *
   */

  .controller('adminDashboardViewsDetailsController', ['$scope', '$transition$', '$q', 'gettext', 'ngDialog', 'adminService', 'usersService', 'miscServices', 'treeService', 'dashboardViewService', 'datafilesService', 'typeToDescriptionFilter', function($scope, $transition$, $q, gettext, ngDialog, adminService, usersService, miscServices, treeService, dashboardViewService, datafilesService, typeToDescriptionFilter) {
    $scope.id = $transition$.params().id;

    $scope.errMessage = '';
    $scope.hasAccess = false;
    $scope.isLoading = true;
    $scope.isProcessing = false;

    $scope.dashboardName = '';
    $scope.dashboardElements = [];
    $scope.availableDatafiles = {};
    $scope.datafilesList = [];
    $scope.newItem = { type: '' };

    var loadingPromises = [];


    // We separately load the name of the dashboard (we do not to wait for this)
    treeService.getElementTitle($scope.id).then(
      function(name) {
        $scope.dashboardName = name;
      },
      function(err) {
        $scope.dashboardName = '(' + gettext('Error') + ')';
        console.log('Could not load dashboard name: ' + err);
      }
    );


    // Check if user has the right access rights - a user is deemed to have "admin" rights if he/she can edit content
    // on the Dashboard Configuration list.
    loadingPromises.push(usersService.isAdmin().then(
      function(res) {
        if(res) {
          $scope.hasAccess = true;
          $scope.errMessage = '';
        } else {
          $scope.hasAccess = false;
          $scope.errMessage = gettext('You need edit rights to The Dashboard\'s configuration to be able to modify the Dashboard Views.');
        }
      }, function(err) {
        $scope.hasAccess = false;
        return gettext('An error occurred checking access rights: ') + err;
      }
    ));


    // Helper function to deal with data received from dashboardViewService
    var processDashboardElements = function(view) {
      // Add "original" data to be able to identify changes
      // We also copy the elements here as otherwise we make changes to our view state even when
      // not saving!

      // Assign to scope
      $scope.dashboardElements = view.map(function(el) {
        return {
          id: el.id,
          title: el.title,
          type: el.type,
          viewKey: el.viewKey,
          options: JSON.parse(JSON.stringify(el.options)),
          orderAsc: el.orderAsc,
          original: {
            title: el.title,
            type: el.type,
            viewKey: el.viewKey,
            options_string: JSON.stringify(el.options),
            orderAsc: el.orderAsc
          },
          isNew: false,
          showDetails: false
        };
      });
    };

    // Load the current dashboard elements for the given dashboard
    loadingPromises.push(dashboardViewService.getDashboardElements($scope.id).then(processDashboardElements));


    // Load the list of available datafiles
    loadingPromises.push(datafilesService.getAllDatafiles().then(
      function(datafilesMap) {
        $scope.availableDatafiles = datafilesMap;
        $scope.datafilesList = Object.keys(datafilesMap);
      }
    ));


    // Has everything loaded or an error occurred? Then set isLoading to false.
    $q.all(loadingPromises).then(
      function() {
        $scope.isLoading = false;
      }, function(err) {
        $scope.isLoading = false;
        $scope.errMessage = (err ? err : gettext('An unknown error occured.'));
      }
    );

    // Helper function to trigger details view
    $scope.toggleDetails = function(data) {
      data.showDetails = !data.showDetails;
    };


    // Helper function to add new event
    $scope.addNew = function() {
       if(!$scope.newItem || !$scope.newItem.type) {
         // TODO REMOVE DEBUG CODE
         console.log($scope);

         miscServices.translatedAlert(gettext('You need to select the view element type for the new element.'));
         return;
       }

       $scope.dashboardElements.push({
         title: 'New ' + typeToDescriptionFilter($scope.newItem.type),
         type: $scope.newItem.type,
         viewKey: '',
         options: {},
         original: {},
         isNew: true,
         showDetails: true
       });
    };


    // Helper function to mark element as deleted
    $scope.deleteElement = function(element) {
      $scope.elementsForm.$setDirty();
      element.showDetails = false;
      element.isDeleted = true;
    };


    // Helper function to restore deleted element
    $scope.restoreElement = function(element) {
      $scope.elementsForm.$setDirty();
      element.isDeleted = false;
    };


    // SAVE changes function
    $scope.saveChanges = function() {
      // Is the form valid?
      if(!$scope.elementsForm.$valid) {
        miscServices.translatedAlert(gettext('Please complete all the required fields prior to saving.'));
        return;
      }

      // Confirmation dialog with warning message
      if(!miscServices.translatedConfirm(gettext('Are you sure you want to SAVE your changes? This action cannot be undone.'))) { return; }

      // Start processing
      $scope.isProcessing = true;

      // TODO - Remove debug code
      console.log('Saving changes for modified dashboard elements:');
      console.log($scope.dashboardElements);

      // Recalculate all "orderAsc" values, identify changes and new elements and go
      var changePromise = $q.resolve();

      $scope.dashboardElements.forEach(function(el, idx) {
        el.orderAsc = idx + 1;

        if(el.isDeleted) {
          // Is this element deleted?
          // Yes.

          // If the element is new, we don't have to do anything (we will clean them up later).
          // Otherwise, we will have to remove it from the database.
          if(!el.isNew) {
            // TODO - Remove debug code
            console.log('Will delete element "%s"...', el.title);

            changePromise = changePromise.then(
              function() {
                return dashboardViewService.deleteDashboardElement(el);
              }
            );
          }

        } else if(el.isNew) {
          // Is this element new?
          // Yes, add it to the database.

          // TODO - Remove debug code
          console.log('Will add new element "%s"...', el.title);

          changePromise = changePromise
            .then(
              function() {
                return dashboardViewService.addDashboardElement($scope.id, el);
              }
            )
            .then(
              function(newElement) {
                // Element is no longer new
                el.id = newElement.id;
                el.isNew = false;

                // New is the new old
                el.original = {
                  title: el.title,
                  type: el.type,
                  viewKey: el.viewKey,
                  options_string: JSON.stringify(el.options),
                  orderAsc: el.orderAsc
                };
              }
            );

        } else if (el.title !== el.original.title ||        // Else: Has any value of this element been changed?
            el.viewKey !== el.original.viewKey ||
            el.original.options_string !== JSON.stringify(el.options) ||
            el.orderAsc !== el.original.orderAsc
        ) {
          // Yes, modify the element in the database.

          // TODO - Remove debug code
          console.log('Will modify existing element "%s"...', el.title);

          changePromise = changePromise
            .then(
              function() {
                return dashboardViewService.changeDashboardElement($scope.id, el);
              }
            )
            .then(
              function() {
                // New is the new old
                el.original = {
                  title: el.title,
                  type: el.type,
                  viewKey: el.viewKey,
                  options_string: JSON.stringify(el.options),
                  orderAsc: el.orderAsc
                };
              }
            );
        }

        // Done.
      });


      changePromise
        .then(
          function() {
            // We need to reload everything from the database to avoid having stale data on the next call
            return dashboardViewService.getDashboardElements($scope.id, true).then(processDashboardElements);
          }
        )
        .then(
          function() {
            // Success - update form as pristine, set isProcessing to false and show message.
            $scope.isProcessing = false;
            $scope.elementsForm.$setPristine();

            miscServices.translatedAlert(gettext('All changes to the dashboard view have been saved.'));
          },
          function(err) {
            // TODO: Remove debug code
            console.log('saveChanges reported an error.');

            // An error occured - display error message.
            $scope.isProcessing = false;
            $scope.errMessage = gettext('An error occured saving your dashboard view changes: ') + err;
          }
        );
    };

    // TODO - deal with situations were multiple people edit the views at the same time


    // CANCEL changes function
    $scope.cancelChanges = function() {
      // Confirmation dialog with warning message
      if(!miscServices.translatedConfirm(gettext('Are you sure you want to CANCEL all your changes? All changes will be lost. This action cannot be undone.'))) { return; }

      // Set is loading to true
      $scope.isLoading = true;

      // Reload elements from database
      dashboardViewService.getDashboardElements($scope.id, true).then(processDashboardElements).then(
        function() {
          $scope.isLoading = false;
          $scope.elementsForm.$setPristine();
        },
        function(err) {
          $scope.isLoading = false;
          $scope.errMessage = gettext('An error occured reloading data from database: ') + err;
        }
      );
    };


    // Helper function to change form to not pristine when element has been dropped
    $scope.treeOptions = {
      dropped: function() {
        $scope.elementsForm.$pristine = false;
        return true;
      }
    };

    // Helper function to add new sub-elements to arrays in the configuration
    $scope.arrayNewElement = function(array, element) {
      // Does the array exist? Otherwise we create a new one.
      if(!Array.isArray(array)) {
        array = [];
      }

      // If no element has been specified after which the element should be inserted, we insert it
      // at the very beginning of the array.
      var targetIndex = (element !== undefined ? array.indexOf(element) : -1) + 1;

      array.splice(targetIndex, 0, {});
      return array;
    };

    // Helper function to remove element from array in the configuration
    $scope.arrayRemoveElement = function(array, element) {
      if(array.indexOf(element) > -1) {
        array.splice(array.indexOf(element), 1);
      }
    };

    // Helper function to move element in array in the configuration
    $scope.arrayMoveElement = function(array, element, moveBy) {
      if(array.indexOf(element) > -1) {
        var newIndex = Math.max(0, Math.min(array.indexOf(element) + moveBy, array.length));

        // Remove element from array
        array.splice(array.indexOf(element), 1);

        // Re-insert at new index
        array.splice(newIndex, 0, element);
      }
    };

    // Helper function to determine if element has active conditional formatting
    $scope.hasConditionalFormat = function(element) {
      return (element.conditionalFormat && Array.isArray(element.conditionalFormat) && element.conditionalFormat.length > 0);
    };

    // Helper function to open "conditionalFormat" dialog box
    $scope.conditionalFormat = function(element) {
      // TODO: Remove debug code
      console.log('conditionalFormat called on element:');
      console.log(element);

      ngDialog
        .open({
          template: 'app/admin/dashboardViewsDetails_condFormat.tmpl.html',
          className: 'ngdialog-theme-default',
          closeByDocument: false,
          scope: $scope,
          data: {
            conditionalFormat: JSON.parse(JSON.stringify(element.conditionalFormat ? element.conditionalFormat : {}))
          }
        })
        .closePromise
        .then(function(ret) {
           if(ret.value && ret.value.save) {
             // Only if dialog has been closed with "Save" button will we copy the changes
             element.conditionalFormat = ret.value.conditionalFormat;
             $scope.elementsForm.$setDirty();
           }
        });
    };


  }])




  /**
   * @ngdoc controller
   * @name dashboardAdmin.controller:adminDatafilesController
   * @description
   *
   * This controller provides a list of all datafiles in the system and allows to edit any of those as well as
   * add new ones.
   *
   * You can access the admin interface at the `/admin/datafiles` route.
   *
   */

  .controller('adminDatafilesController', ['$scope', '$q', 'gettext', 'usersService', 'datafilesService', function($scope, $q, gettext, usersService, datafilesService) {
    $scope.errMessage = '';
    $scope.hasAccess = false;
    $scope.isLoading = true;
    $scope.datafilesList = [];

    var loadingPromises = [];

    // Check if user has the right access rights - a user is deemed to have "admin" rights if he/she can edit content
    // on the Dashboard Configuration list.
    loadingPromises.push(usersService.isAdmin().then(
      function(res) {
        if(res) {
          $scope.hasAccess = true;
          $scope.errMessage = '';
        } else {
          $scope.hasAccess = false;
          $scope.errMessage = gettext('You need edit rights to The Dashboard\'s configuration to be able to modify the datafile definitions.');
        }
      }, function(err) {
        $scope.hasAccess = false;
        return gettext('An error occurred checking access rights: ') + err;
      }
    ));

    // Load the list of available datafiles
    // We force a reload because we want to make sure we admin the newest view
    loadingPromises.push(datafilesService.getAllDatafiles(true).then(
      function(datafilesMap) {
        $scope.datafilesList = Object.keys(datafilesMap).map(function(key) { return datafilesMap[key]; });
      }
    ));

    // Has everything loaded or an error occurred? Then set isLoading to false.
    $q.all(loadingPromises).then(
      function() {
        $scope.isLoading = false;
      }, function(err) {
        $scope.isLoading = false;
        $scope.errMessage = (err ? err : gettext('An unknown error occured.'));
      }
    );

  }])



  /**
   * @ngdoc controller
   * @name dashboardAdmin.controller:adminDatafilesDetailsController
   * @description
   *
   * This controller provides the details view for a given datafile ID.
   *
   * You can access the relevant admin interface at the `/admin/datafiles/:id` route, where `:id`
   * is the internal ID of the datafile you want to modify. If you set `:id` to `-1`, you will get
   * a form for a new datafile to be added.
   *
   */

  .controller('adminDatafilesDetailsController', ['$scope', '$state', '$transition$', '$q', 'gettext', 'd3', 'adminService', 'usersService', 'miscServices', 'datafilesService', function($scope, $state, $transition$, $q, gettext, d3, adminService, usersService, miscServices, datafilesService) {
    $scope.id = Number($transition$.params().id);

    $scope.errMessage = '';
    $scope.hasAccess = false;
    $scope.isLoading = true;

    $scope.columnFormats = Object.keys(d3.ourSettings.formats);

    $scope.datafile = {
      dataFile: '',
      fileName: '',
      dataFileFormat: {
        separator: ',',
        columns: []
      }
    };

    var loadingPromises = [];

    // Check if user has the right access rights - a user is deemed to have "admin" rights if he/she can edit content
    // on the Dashboard Configuration list.
    loadingPromises.push(usersService.isAdmin().then(
      function(res) {
        if(res) {
          $scope.hasAccess = true;
          $scope.errMessage = '';
        } else {
          $scope.hasAccess = false;
          $scope.errMessage = gettext('You need edit rights to The Dashboard\'s configuration to be able to modify the datafile definitions.');
        }
      }, function(err) {
        $scope.hasAccess = false;
        return gettext('An error occurred checking access rights: ') + err;
      }
    ));


    // TODO - Remove debug code
    console.log('Datafiles details loaded with ID:');
    console.log($scope.id);


    // Load the datafile (unless this is a new datafile form)
    if(!isNaN($scope.id) && $scope.id !== -1) {
      loadingPromises.push(datafilesService.getDatafileById($scope.id).then(
        function(datafile) {
          $scope.datafile = datafile;
        }
      ));
    }

    // Has everything loaded or an error occurred? Then set isLoading to false.
    $q.all(loadingPromises).then(
      function() {
        $scope.isLoading = false;
      }, function(err) {
        $scope.isLoading = false;
        $scope.errMessage = (err ? err : gettext('An unknown error occured.'));
      }
    );


    // Helper to add a new empty column definition
    $scope.addColumn = function() {
      $scope.datafile.dataFileFormat.columns.push({
        source: '',
        display: '',
        keyword: '',
        format: '',
        isKey: false
      });

      $scope.datafileForm.$setDirty();
    };

    // Helper to remove a column definition
    $scope.removeColumn = function(col) {
      var removeIndex = $scope.datafile.dataFileFormat.columns.indexOf(col);

      if(removeIndex >= 0) {
        $scope.datafile.dataFileFormat.columns.splice(removeIndex, 1);
        $scope.datafileForm.$setDirty();
      }
    };

    // Helper function to move a column (column order determines how they are displayed on the charts)
    $scope.moveColumn = function(col, moveBy) {
      if($scope.datafile.dataFileFormat.columns.indexOf(col) > -1) {
        var newIndex = Math.max(0, Math.min($scope.datafile.dataFileFormat.columns.indexOf(col) + moveBy, $scope.datafile.dataFileFormat.columns.length));

        // Remove element from array
        $scope.datafile.dataFileFormat.columns.splice($scope.datafile.dataFileFormat.columns.indexOf(col), 1);

        // Re-insert at new index
        $scope.datafile.dataFileFormat.columns.splice(newIndex, 0, col);

        // Set the form to dirty
        $scope.datafileForm.$setDirty();
      }
    };

    // Helper function to determine if we have a key
    $scope.noKey = function() {
      return (!Array.isArray($scope.datafile.dataFileFormat.columns) || $scope.datafile.dataFileFormat.columns.findIndex(function(col) { return col.isKey; }) === -1);
    };

    // Helper function to SAVE changes
    $scope.saveChanges = function() {
      // Is the form valid?
      if(!$scope.datafileForm.$valid) {
        miscServices.translatedAlert(gettext('Please complete all the required fields prior to saving.'));
        return;
      }

      // Confirmation dialog with warning message
      if(!miscServices.translatedConfirm(gettext('Are you sure you want to SAVE your changes? This action cannot be undone.'))) { return; }

      // Start processing
      $scope.isProcessing = true;


      // Is this a new datafile?
      if($scope.id === -1) {
        // New datafile => add to database.
        datafilesService.addDatafile($scope.datafile).then(function(newDatafile) {
          // Store ID and set datafile form to pristine
          $scope.id = newDatafile.id;
          $scope.datafile.id = newDatafile.id;
          $scope.datafileForm.$setPristine();

          // Show confirmation
          miscServices.translatedAlert(gettext('The new datafile has successfully been stored in the database.'));

          $scope.isProcessing = false;
        });

      } else {
        // Existing datafile => update in database.
        datafilesService.changeDatafile($scope.datafile).then(function() {
          // Set datafile form to pristine
          $scope.datafileForm.$setPristine();

          // Show confirmation
          miscServices.translatedAlert(gettext('The datafile has successfully been updated in the database.'));

          $scope.isProcessing = false;
        });
      }
    };


    // Helper function to CANCEL changes
    $scope.cancelChanges = function() {
      if(!isNaN($scope.id) && $scope.id !== -1) {
        // Confirmation dialog with warning message
        if(!miscServices.translatedConfirm(gettext('Are you sure you want to CANCEL all your changes? All changes will be lost. This action cannot be undone.'))) { return; }


        // We reload the data from the database and set the form to pristine
        $scope.isLoading = true;

        datafilesService.getDatafileById($scope.id, true).then(
          function(datafile) {
            $scope.datafile = datafile;
            $scope.isLoading = false;
            $scope.datafileForm.$setPristine();
          }
        );
      }
    };


    // Helper function to DELETE datafile definition
    $scope.deleteDatafile = function() {
      if($scope.id === -1) {
        // New datafile, just return.
        return;
      }

      // Confirmation dialog with warning message
      if(!miscServices.translatedConfirm(gettext('Are you sure you want to DELETE this datafile definition? This action cannot be undone.'))) { return; }

      // Start processing
      $scope.isProcessing = true;

      datafilesService.deleteDatafile($scope.datafile).then(function() {
        // Show message
        miscServices.translatedAlert(gettext('The datafile definition has been removed.'));

        // Redirect to datafile overview
        return $state.go('^');
      });

    };

  }])



  /**
   * @ngdoc controller
   * @name dashboardAdmin.controller:adminIndexController
   * @description
   *
   * This controller shows the entry page to the administration section. It establishes whether the user has
   * sufficient access rights and which parts of The Dashboard he/she has admin rights on.
   *
   * You can access the admin interface at the `/admin` route.
   *
   */

  .controller('adminIndexController', ['$scope', '$q', 'gettext', 'usersService', function($scope, $q, gettext, usersService) {
    $scope.errMessage = '';
    $scope.userIsSiteAdmin = false;

    // Does the user have site admin rights?
    usersService.isSiteAdmin().then(
      function(res) {
        $scope.userIsSiteAdmin = !!res;
      },
      function(err) {
        console.log('ERROR: Could not retrieve site admin settings.');
        console.log(err);
      }
    );

    // Does the user have access rights to the configuration list?
    usersService.isAdmin().then(
      function(res) {
        $scope.errMessage = '';
        if(!res) {
          $scope.errMessage = gettext('Note that you are not authorized to edit The Dashboard\'s configuration, which means that most functions below will not work for you. Please make sure that you have edit rights on the "The Dashboard - Configuration" list before you proceed.');
        }
      },
      function() {
        // An error occured. Most likely the lists have not been initialized yet.
        $scope.errMessage = gettext('We could not retrieve your user status. Most likely this means that you have not yet initialized the base lists used by The Dashboard. If you have site admin rights on your SharePoint site, you can initialize them now by using the "Initialize SharePoint for use with Dashboard" function below.');
      }
    );


  }]);








