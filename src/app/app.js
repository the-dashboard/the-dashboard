'use strict';

/**
 * @ngdoc overview
 * @name index
 * @description
 *
 * This is the technical documentation of the internal built-up of The Dashboard. The Dashboard is built as an
 * AngularJS 1 application which uses the Microsoft SharePoint REST API as a backend, storing all data in SharePoint
 * lists and document libraries. It does not use the SharePoint JavaScript API.
 *
 * This technical documentation describes the internal AngularJS modules, directives, controllers and services of the
 * application and is automatically generated from the source code using ngdocs.
 *
 */


/**
 * @ngdoc overview
 * @name dashboardApp
 * @installation
 * @description
 *
 * This is the main module of The Dashboard application. It defines the routing for the index
 * and about pages, showing simple templates for them, and contains the controllers for the
 * navigation bar.
 */

angular.module('dashboardApp', ['ui.router', 'gettext', 'ngSanitize', 'ngDialog', 'dashboardTree', 'dashboardAdmin', 'dashboardProfile', 'dashboardTasks', 'socialDirectives', 'dashboardScreenServices', 'usersServices', 'configServices', 'chartServices', 'feedbackServices', 'miscServices', 'restServices'])
  .config(['$stateProvider', '$urlServiceProvider', function($stateProvider, $urlServiceProvider) {
    $stateProvider
    // route for the home page
      .state('index', {
        url:'/',
        views: {
          'main@': {
            templateUrl: 'app/index.tmpl.html'
          }
        }
      })

      // route for the aboutus page
      .state('about', {
        url:'/about',
        views: {
          'main@': {
            templateUrl: 'app/about.tmpl.html',
            controller: 'languageController'
          }
        }
      });

    $urlServiceProvider.rules.otherwise('/');

  }])

  // RUN block initializes user data (such as the preferred language to use)
  .run(['$anchorScroll', 'gettextCatalog', 'usersService', 'configService', 'd3', function($anchorScroll, gettextCatalog, usersService, configService, d3) {
    // anchorScroll offset for navbar
    $anchorScroll.yOffset = 45;

    // Which languages exist?
    var defaultLanguage = 'en';
    var availableLanguages = {'en': 'English', 'de': 'German (Swiss)' };

    configService.getConfiguration()
      .then(
        function(config) {
          // Store configuration
          if(config['Available Languages']) {
            availableLanguages = JSON.parse(config['Available Languages']);
          }

          if(config['Default Language'] && availableLanguages[config['Default Language']]) {
            defaultLanguage = config['Default Language'];
          }

          // ... does the user have user settings defining the language to be used?
          return usersService.getCurrentUserSettings();
        }
      )
      .then(
        function(userSettings) {
          // Ok, does the user have a valid default language?
          if(userSettings.language && availableLanguages[userSettings.language] !== undefined) {
            console.log('Using user settings for language: ' + userSettings.language);
            gettextCatalog.setCurrentLanguage(userSettings.language);
            d3.ourSettings.setLocale(userSettings.language);
            window.moment.locale(userSettings.language);
          } else {
            // If not, we use the default language.
            console.log('Using default language: ' + defaultLanguage);
            gettextCatalog.setCurrentLanguage(defaultLanguage);
            d3.ourSettings.setLocale(defaultLanguage);
            window.moment.locale(defaultLanguage);
          }
        },
        function(err) {
          console.log('An error occured trying to retrieve language settings:');
          console.log(err);
          gettextCatalog.setCurrentLanguage('en');
          d3.ourSettings.setLocale('en');
          window.moment.locale('en');
        }
      );

  }])


  /**
   * @ngdoc controller
   * @name dashboardApp.controller:navbarController
   *
   * @description
   * This controller supports the main navigation bar displayed on top of all
   * Dashboard pages.
   *
   * It toggles whether the 'Administration' menu is shown based on whether the
   * user has admin privileges, as determined by the isAdmin() function of the
   * {@link usersServices.usersService `usersService`} service.
   *
   */

  .controller('navbarController', ['$scope', '$q', '$location', 'gettext', 'ngDialog', 'usersService', 'feedbackService', 'miscServices', function($scope, $q, $location, gettext, ngDialog, usersService, feedbackService, miscServices) {
    $scope.isAdmin = false;
    $scope.isTestingMode = !!window.dashboardTestEnvironment;

    usersService.isSiteAdmin()
      .then(
        function(res) {
          // If the user is site admin, we will definitely show the admin page.
          if(res) {
            return $q.resolve(true);
          }

          // Otherwise we will need to continue checking for "Configuration" edit rights as admin rights.
          return usersService.isAdmin();
        }
      )
      .then(
        function(res) {
          $scope.isAdmin = res;
        },
        function(err) {
          console.log('Error getting admin status:' + err);
        }
      );

    // Logic for feedback button
    var startFeedbackInProgress = false;
    $scope.startFeedback = function() {
      // Is screenshot generation in progress? Then we directly return.
      if(startFeedbackInProgress) { return; }

      // Display waiting indicator
      angular.element(document).find('body').addClass('waitingCursor');

      // Make sure user cannot click again while screenshot generation is in process
      startFeedbackInProgress = true;

      // Send Feedback function
      var sendScreenshot;
      var feedbackDialog;
      var sendFeedback = function(feedbackText, feedbackSmiley) {
        feedbackService
          .sendFeedback('SMILE OR FROWN: ' + feedbackSmiley + '\n\n------------------------------------\n' + feedbackText + '\n------------------------------------\n\nSource URL: ' + $location.absUrl(), sendScreenshot)
          .then(
            function() {
              miscServices.translatedAlert(gettext('Your feedback has been stored and we will get in touch soon.'));

              if(feedbackDialog) {
                feedbackDialog.close();
              }
            },
            function(err) {
              console.log('An ERROR occured while trying to save feedback:');
              console.log([feedbackText, sendScreenshot, err]);

              miscServices.translatedAlert(gettext('Your feedback could not be stored. Please try again later or see the log for details.'));

              if(feedbackDialog) {
                feedbackDialog.close();
              }
            }
          );
      };

      // We need to get the screenshot before we display the feedback dialog, as it would overlay the real interface.
      feedbackService
        .getScreenshot()
        .then(
          function(screenshot) {
            sendScreenshot = screenshot;

            // Remove waiting cursor
            angular.element(document).find('body').removeClass('waitingCursor');

            // Generation no longer in progress
            startFeedbackInProgress = false;

            // Open dialog
            feedbackDialog = ngDialog.open({
              template: 'app/feedbackDialog.tmpl.html',
              className: 'ngdialog-theme-default',
              data: {
                'sendFeedback': sendFeedback
              }
            });
          }
        );
    };


  }])

    /**
     * @ngdoc controller
     * @name dashboardApp.controller:sidebarController
     *
     * @description
     * This controller supports the main sidebar which can be enabled by views to be shown on the
     * right-hand side of the screen using the {@link dashboardScreenServices.sidebarService `sidebarService`}.
     *
     */

    .controller('sidebarController', ['$scope', '$q', '$timeout', '$transitions', 'sidebarService', function($scope, $q, $timeout, $transitions, sidebarService) {
      // Initialize scope
      $scope.data = {};
      $scope.sidebarVisibility = 'hide';
      $scope.sidebarTemplate = '';


      var loadTemplate = function(newTemplate, newData) {
        // Change the template.
        $scope.sidebarTemplate = newTemplate;

        // Change the data on the scope.
        $scope.data = newData;

        // TODO: Remove debug code
        console.log('Updated sidebar scope:');
        console.log($scope);

        // Done.
      };

      // Function to change the visibility of the sidebar
      var setVisibility = function(newVisibility) {
        $scope.sidebarVisibility = 'dashboardSidebar-' + newVisibility;
      };

      // Register functions with the sidebarService
      sidebarService.registerController(loadTemplate, setVisibility);

    }])

/**
 * @ngdoc controller
 * @name dashboardApp.controller:mainDivController
 *
 * @description
 * This controller supports the main div element that is used to display the central content of The Dashboard. It
 * registers with the {@link dashboardScreenServices.screenService `screenService`} such that the service
 * can update the left and right margins of the main div element whenever "sticky" sidebars are being displayed.
 *
 */

.controller('mainDivController', ['$scope', '$q', 'screenService', function($scope, $q, screenService) {
  // Initialize scope
  $scope.mainDivStyle = {
    'margin-left': '10px',
    'margin-right': '10px'
  };

  var setMainMargins = function(leftMargin, rightMargin) {
    // Change the margins.
    $scope.mainDivStyle = {
      'margin-left': (leftMargin+10) + 'px',
      'margin-right': (rightMargin+10) + 'px'
    };

    // Done.
  };

  // Register function with the screenService
  screenService.registerController(setMainMargins);

}])


/**
 * @ngdoc controller
 * @name dashboardApp.controller:languageController
 *
 * @description
 * This controller adds `currentLanguage` to the scope to enable templates to choose between language
 * pieces to display (for longer text that is not well-suited for gettext).
 *
 */

  .controller('languageController', ['$scope', 'gettextCatalog', function($scope, gettextCatalog) {
    // Initialize scope
    $scope.currentLanguage = gettextCatalog.getCurrentLanguage();
    if(!$scope.currentLanguage) {
      $scope.currentLanguage = 'en';
    }
  }]);


