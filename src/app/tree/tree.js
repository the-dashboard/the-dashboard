'use strict';

/**
 * @ngdoc overview
 * @name dashboardTree
 * @description
 *
 * This module provides routing and base controllers for the "Ongoing Risk Assessment"
 * part of The Dashboard. The "Ongoing Risk Assessment" is basically a hierarchical
 * tree of entities (organizational units, business proceses, applications, etc. -
 * whatever is most suitable for the specific organization). Each of these entities
 * can have a dashboard which show quantitative data and allows for notes and risk
 * assessments to be recorded. Dashboards can also show notes from other tree elements,
 * which permits an aggregation of information at various levels.
 *
 * This module provides the functionality to view the tree of entities and to view
 * individual dashboards for each entity. The dashboards itself rely on various,
 * separate directives which provide the functionality for the different dashboard
 * elements provided by the application.
 *
 */

angular.module('dashboardTree', ['ui.router', 'ngDialog', 'dashboardTreeServices', 'dashboardViewServices', 'chartServices', 'chartDirectives', 'notesDirectives', 'impactDirectives', 'riskTrackerDirectives', 'objectExplorerDirectives', 'zoomDirectives'])
  .config(['$stateProvider', function($stateProvider) {
    $stateProvider
      // route for the tree overview
      .state('tree', {
        url: '/tree',
        views: {
          'main@': {
            templateUrl: 'app/tree/tree.tmpl.html',
            controller: 'treeIndexController'
          }
        }
      })

      // route for an individual dashboard
      .state('tree.dashboard', {
        url:'/:id',
        views: {
          'main@': {
            templateUrl: 'app/tree/dashboard.tmpl.html',
            controller: 'treeDashboardController'
          }
        }
      })

      // route for an individual dashboard tab
      .state('tree.dashboard.tab', {
        url:'/:tab',
        views: {
          'main@': {
            templateUrl: 'app/tree/dashboard.tmpl.html',
            controller: 'treeDashboardController'
          }
        }
      });
  }])


  /**
   * @ngdoc controller
   * @name dashboardTree.controller:treeIndexController
   * @description
   *
   * This controller shows the hierarchical tree with all the entities. Each entity is
   * provided as a clickable link which opens the dashboard for the respective tree
   * element.
   *
   * The controller retrieves the tree elements using the {@link dashboardTreeServices.treeService}.
   * It provides functionality to toggle between only showing active tree elements or all elements,
   * including those which are not yet or no longer active.
   *
   */

  .controller('treeIndexController', ['$scope', 'treeService', function($scope, treeService) {
    $scope.tree = [];
    $scope.errMessage = '';
    $scope.loading = true;
    $scope.onlyActive = true;

    $scope.filterActive = function(treeItem) {
      return (!$scope.onlyActive || treeItem.active);
    };

    treeService.getFullHierarchy().then(
      function(treeStart) {
        $scope.tree = [ treeStart ];
        $scope.loading = false;

        // TODO: remove debug code
        console.log('Tree has been loaded:');
        console.log($scope.tree);
      },
      function(err) {
        $scope.errMessage = err;
        $scope.loading = false;
      }
    );
  }])



  /**
   * @ngdoc controller
   * @name dashboardTree.controller:treeDashboardController
   * @description
   *
   * This controller shows an individual dashboard for a single entity from the
   * hierarchical tree. It loads the details for that tree element (the name and
   * the dashboard view elements to be displayed) using the
   * {@link dashboardTreeServices.treeService} and then embeds the right directive
   * for each view element.
   *
   */

  .controller('treeDashboardController', ['$scope', '$state', '$transition$', '$sce', 'gettext', 'ngDialog', 'dashboardViewService', 'treeService', function($scope, $state, $transition$, $sce, gettext, ngDialog, dashboardViewService, treeService) {
    $scope.id = $transition$.params().id;
    $scope.tab = $transition$.params().tab;
    $scope.dashboardElements = [];
    $scope.tabs = [];
    $scope.loading = true;
    $scope.errMessage = '';
    $scope.dashboardName = '';

    $scope.showDashboardHelp = function(helpTitle, helpText, stopPropEvent) {
      if(stopPropEvent) {
        // If this parameter is passed an $event object, we will stop propagation of the click
        stopPropEvent.stopPropagation();
      }

      ngDialog.open({
        template: 'app/tree/dashboardHelpDialog.tmpl.html',
        className: 'ngdialog-theme-default',
        data: { 'helpTitle': helpTitle, 'helpText': helpText }
      });
    };

    treeService.getElementTitle($scope.id).then(
      function(name) {
        $scope.dashboardName = name;
      },
      function(err) {
        $scope.dashboardName = '(' + gettext('Error') + ')';
        console.log('Could not load dashboard name: ' + err);
      }
    );
    
    // Helper functions for tabs
    var filterVisibleElements = function() {
      var activeTab;
      if($scope.tab !== undefined) {
        activeTab = $scope.tabs.find(function(tab) {
          return (tab.tabId === $scope.tab);
        });
      }
        
      if(activeTab) {
        $scope.visibleElements = $scope.dashboardElements.filter(function(el) {
          return (el.orderAsc >= activeTab.from && el.orderAsc <= activeTab.to);
        });
      } else {
        $scope.visibleElements = $scope.dashboardElements;
      }
    };
    
    $scope.changeTab = function(tabId) {
      $scope.tab = tabId;
      
      // update URL without triggering state change
      $state.go('tree.dashboard.tab', {id: $scope.id, tab: tabId}, {notify: false});
      
      filterVisibleElements();
    };
    
    // Get elements
    dashboardViewService.getDashboardElements($scope.id).then(
      function(view) {
        $scope.dashboardElements = view;
        $scope.tabs = [];
        var currTab;

        view.forEach(function(el, i) {
          // Mark URLs for iframes as secure (note that we trust the admin user on this)
          if(el.type === 'iframe' && el.options.url) {
            el.options.insecureUrl = $sce.trustAsResourceUrl(el.options.url);
          }

          // Map tab dividers to get tab contents
          if(el.type === "tabDivider") {
            if(i > 0) {
              // We have (or should have) a previous tab.
              if(currTab === undefined) {
                // We need to create an "empty" tab..
                currTab = "";
                $scope.tabs.push({
                  tabId: "",
                  tabTitle: "Intro",
                  tabText: "Intro",
                  tabColor: "#ffffff",
                  tabSelectedColor: "#dddddd",
                  from: 0
                });
              }
              
              // We set the to of the previous tab
              $scope.tabs[$scope.tabs.length-1].to = el.orderAsc - 1;
            }
          
            currTab = el.options.tabId;
            $scope.tabs.push({
              tabId: el.options.tabId,
              tabTitle: el.title,
              tabText: el.options.tabText,
              tabColor: el.options.tabColor,
              tabSelectedColor: el.options.tabSelectedColor,
              tabHelpText: el.options.helpText,
              from: el.orderAsc,
              to: Infinity
            });
          }
        });
        
        
        // Set active tab if not set
        if(currTab !== undefined && $scope.tab === undefined) {
          $scope.tab = $scope.tabs[0].tabId;
        }
        
        // Filter visible elements
        filterVisibleElements();
        
        $scope.loading = false;
      },
      function(err) {
        $scope.errMessage = err;
        $scope.loading = false;
      }
    );
  }]);
