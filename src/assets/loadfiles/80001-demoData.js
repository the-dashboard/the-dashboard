[
  {
    "_action": "addEntry",
    "_endpoint": "tree",
    "Title": "Anthony & Anthony Publishers Inc.",
    "_storeID": "Tree:Main",
    "hasDashboard": true,
    "OrderAsc": 1
  },
  {
    "_action": "addEntry",
    "_endpoint": "tree",
    "Title": "Finance and Accounting",
    "ParentId": "{{Tree:Main}}",
    "_storeID": "Tree:Finance and Accounting",
    "hasDashboard": true,
    "OrderAsc": 2
  },
  {
    "_action": "addEntry",
    "_endpoint": "tree",
    "Title": "Accounts Payable (Purchase-to-Pay)",
    "ParentId": "{{Tree:Finance and Accounting}}",
    "_storeID": "Tree:Accounts Payable",
    "hasDashboard": true,
    "OrderAsc": 3
  },
  {
    "_action": "addEntry",
    "_endpoint": "tree",
    "Title": "Accounts Receivable (Sales Cycle)",
    "ParentId": "{{Tree:Finance and Accounting}}",
    "_storeID": "Tree:Accounts Receivable",
    "hasDashboard": true,
    "OrderAsc": 4
  },
  {
    "_action": "addEntry",
    "_endpoint": "tree",
    "Title": "Publishing Business Divisions",
    "ParentId": "{{Tree:Main}}",
    "_storeID": "Tree:Publishing Business Divisions",
    "hasDashboard": true,
    "OrderAsc": 5
  },
  {
    "_action": "addEntry",
    "_endpoint": "tree",
    "Title": "Children's Books",
    "ParentId": "{{Tree:Publishing Business Divisions}}",
    "_storeID": "Tree:Childrens Books",
    "hasDashboard": true,
    "OrderAsc": 6
  },
  {
    "_action": "addEntry",
    "_endpoint": "tree",
    "Title": "Science Fiction",
    "ParentId": "{{Tree:Publishing Business Divisions}}",
    "_storeID": "Tree:Science Fiction",
    "hasDashboard": true,
    "OrderAsc": 7
  },
  {
    "_action": "addEntry",
    "_endpoint": "tree",
    "Title": "Business & Self-help",
    "ParentId": "{{Tree:Publishing Business Divisions}}",
    "Valid_from": "2011-03-01T17:12:12Z",
    "Valid_to": "2014-09-23T07:23:23Z",
    "_storeID": "Tree:Business Self help",
    "hasDashboard": true,
    "OrderAsc": 8
  },
  {
    "_action": "addEntry",
    "_endpoint": "tree",
    "Title": "Education",
    "ParentId": "{{Tree:Publishing Business Divisions}}",
    "_storeID": "Tree:Education",
    "hasDashboard": true,
    "OrderAsc": 9
  },
  {
    "_action": "addEntry",
    "_endpoint": "tree",
    "Title": "IT Books",
    "ParentId": "{{Tree:Publishing Business Divisions}}",
    "Valid_from": "2018-07-01T17:12:12Z",
    "_storeID": "Tree:IT Books",
    "hasDashboard": true,
    "OrderAsc": 10
  },
  {
    "_action": "addEntry",
    "_endpoint": "tree",
    "Title": "Operations and HR",
    "ParentId": "{{Tree:Main}}",
    "_storeID": "Tree:Operations and HR",
    "hasDashboard": true,
    "OrderAsc": 11
  },
  {
    "_action": "addEntry",
    "_endpoint": "tree",
    "Title": "Human Resources",
    "ParentId": "{{Tree:Operations and HR}}",
    "_storeID": "Tree:Human Resources",
    "hasDashboard": true,
    "OrderAsc": 12
  },
  {
    "_action": "addEntry",
    "_endpoint": "tree",
    "Title": "Warehousing and Distribution",
    "ParentId": "{{Tree:Operations and HR}}",
    "_storeID": "Tree:Warehousing and Distribution",
    "hasDashboard": true,
    "OrderAsc": 13
  },
  {
    "_action": "addEntry",
    "_endpoint": "tree",
    "Title": "Anthony & Anthony Printers Inc.",
    "ParentId": "{{Tree:Operations and HR}}",
    "_storeID": "Tree:Anthony Anthony Printers Inc",
    "hasDashboard": true,
    "OrderAsc": 14
  },




  {
    "_action": "addEntry",
    "_endpoint": "views",
    "Title": "Risk Matrix",
    "viewKey": "risk_matrix",
    "treeElementId": "{{Tree:Main}}",
    "elementType": "riskTracker",
    "elementOptionsJSON": "{\n  \"riskCategory\": \"tree/{{Tree:Main}}\",\n  \"xLabel\": \"Likelihood\",\n  \"xLabels\": [\"Low\", \"Medium\", \"High\"],\n  \"yLabel\": \"Impact\",\n  \"yLabels\":[\"Low\", \"Medium\", \"High\"],\n  \"maxDaysOld\": 365,\n  \"showImpacts\": true\n}",
    "orderAsc": 150
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "Title": "Notes",
    "viewKey": "main_notes",
    "treeElementId": "{{Tree:Main}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n\"keywordID\": \"tree/{{Tree:Main}}/details\",\n\"enableKeywords\": true,\n\"showSearch\": true,\n  \"showImpacts\": true, \"impactsRiskCategory\": \"tree/{{Tree:Main}}\",\n  \"impactsRiskCategoryDescription\": \"Main Risks\",\n  \"showNew\": \"bottom\" \n}",
    "orderAsc": 200
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "risk_finance",
    "Title": "Risk - Finance and Accounting",
    "treeElementId": "{{Tree:Main}}",
    "elementType": "riskTracker",
    "elementOptionsJSON": "{\n  \"riskCategory\": \"tree/{{Tree:Finance and Accounting}}\",\n  \"xLabel\": \"Likelihood\",\n  \"xLabels\": [\"Low\", \"Medium\", \"High\"],\n  \"yLabel\": \"Impact\",\n  \"yLabels\":[\"Low\", \"Medium\", \"High\"],\n  \"maxDaysOld\": 365,\n  \"showImpacts\": true\n}",
    "orderAsc": 310
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "risk_publishing",
    "Title": "Risk - Publishing Business Divisions",
    "treeElementId": "{{Tree:Main}}",
    "elementType": "riskTracker",
    "elementOptionsJSON": "{\n  \"riskCategory\": \"tree/{{Tree:Publishing Business Divisions}}\",\n  \"xLabel\": \"Likelihood\",\n  \"xLabels\": [\"Low\", \"Medium\", \"High\"],\n  \"yLabel\": \"Impact\",\n  \"yLabels\":[\"Low\", \"Medium\", \"High\"],\n  \"maxDaysOld\": 365,\n  \"showImpacts\": true\n}",
    "orderAsc": 320
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "risk_operations",
    "Title": "Risk - Operations and HR",
    "treeElementId": "{{Tree:Main}}",
    "elementType": "riskTracker",
    "elementOptionsJSON": "{\n  \"riskCategory\": \"tree/{{Tree:Operations and HR}}\",\n  \"xLabel\": \"Likelihood\",\n  \"xLabels\": [\"Low\", \"Medium\", \"High\"],\n  \"yLabel\": \"Impact\",\n  \"yLabels\":[\"Low\", \"Medium\", \"High\"],\n  \"maxDaysOld\": 365,\n  \"showImpacts\": true\n}",
    "orderAsc": 330
  },




  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "risk_matrix",
    "Title": "Risk Matrix",
    "treeElementId": "{{Tree:Finance and Accounting}}",
    "elementType": "riskTracker",
    "elementOptionsJSON": "{\n  \"riskCategory\": \"tree/{{Tree:Finance and Accounting}}\",\n  \"xLabel\": \"Likelihood\",\n  \"xLabels\": [\"Low\", \"Medium\", \"High\"],\n  \"yLabel\": \"Impact\",\n  \"yLabels\":[\"Low\", \"Medium\", \"High\"],\n  \"maxDaysOld\": 365,\n  \"showImpacts\": true\n}",
    "orderAsc": 150
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "main_notes",
    "Title": "Notes",
    "treeElementId": "{{Tree:Finance and Accounting}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n\"keywordID\": \"tree/{{Tree:Finance and Accounting}}/details\",\n\"enableKeywords\": true,\n\"showSearch\": true,\n  \"showImpacts\": true, \"impactsRiskCategory\": \"tree/{{Tree:Finance and Accounting}}\",\n  \"impactsRiskCategoryDescription\": \"Finance and Accounting Risks\",\n  \"showNew\": \"bottom\" \n}",
    "orderAsc": 200
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "summary_ap",
    "Title": "Summary - Accounts Payable (Purchase-to-Pay)",
    "treeElementId": "{{Tree:Finance and Accounting}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Accounts Payable}}/summary\",\n  \"enableKeywords\": false,\n  \"showSearch\": false,\n  \"limitTo\": 1,\n  \"statusFilter\": [\"Final\", \"Closed\", \"Reviewed\"],\n  \"showNew\": \"\"\n}",
    "orderAsc": 310
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "summary_ar",
    "Title": "Summary - Accounts Receivable (Sales Cycle)",
    "treeElementId": "{{Tree:Finance and Accounting}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Accounts Receivable}}/summary\",\n  \"enableKeywords\": false,\n  \"showSearch\": false,\n  \"limitTo\": 1,\n  \"statusFilter\": [\"Final\", \"Closed\", \"Reviewed\"],\n  \"showNew\": \"\" \n}",
    "orderAsc": 320
  },





  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "summary_ap",
    "Title": "Summary - Accounts Payable (Purchase-to-Pay)",
    "treeElementId": "{{Tree:Accounts Payable}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Accounts Payable}}/summary\",\n  \"enableKeywords\": false,\n  \"showSearch\": false,\n  \"limitTo\": 3,\n  \"statusFilter\": [\"Draft\", \"Final\", \"Closed\", \"Reviewed\"],\n  \"showNew\": \"top\"\n}",
    "orderAsc": 150
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "risk_objects",
    "Title": "Risk Objects for Purchase-to-Pay",
    "treeElementId": "{{Tree:Accounts Payable}}",
    "elementType": "objectExplorer",
    "elementOptionsJSON": "{ \n   \"width\": 950, \n   \"height\": 90, \n   \"mappingKey\": \"purchaseToPay\", \n   \"elements\": [{ \n       \"key\": \"Planning\", \n       \"display\": \"Planning\", \n       \"conditionalFormat\": [\n         {\n         \"style\": \"warning\",\n         \"type\": \"Audit Report\",\n         \"columnFilter\": [\n           {\n             \"column\": \"importance\",\n             \"invert\": false,\n             \"compare\": \"contains\",\n             \"value\": \"A\"\n           }\n         ]\n       }\n       ]\n     }, \n     { \n       \"key\": \"Purchase Requisition\", \n       \"display\": \"Purchase Requisition\", \n       \"conditionalFormat\": [\n         {\n         \"style\": \"warning\",\n         \"type\": \"Audit Report\",\n         \"columnFilter\": [\n           {\n             \"column\": \"importance\",\n             \"invert\": false,\n             \"compare\": \"contains\",\n             \"value\": \"A\"\n           }\n         ]\n       }\n       ]\n     }, \n     { \n       \"key\": \"Request for Quotation\", \n       \"display\": \"Request for Quotation\", \n       \"conditionalFormat\": [\n         {\n         \"style\": \"warning\",\n         \"type\": \"Audit Report\",\n         \"columnFilter\": [\n           {\n             \"column\": \"importance\",\n             \"invert\": false,\n             \"compare\": \"contains\",\n             \"value\": \"A\"\n           }\n         ]\n       }\n       ]\n     }, \n     { \n       \"key\": \"Purchase Order\", \n       \"display\": \"Purchase Order\", \n       \"conditionalFormat\": [\n         {\n         \"style\": \"warning\",\n         \"type\": \"Audit Report\",\n         \"columnFilter\": [\n           {\n             \"column\": \"importance\",\n             \"invert\": false,\n             \"compare\": \"contains\",\n             \"value\": \"A\"\n           }\n         ]\n       }\n       ]\n     }, \n     { \n       \"key\": \"Goods Receipt\", \n       \"display\": \"Goods Receipt\", \n       \"conditionalFormat\": [\n         {\n         \"style\": \"warning\",\n         \"type\": \"Audit Report\",\n         \"columnFilter\": [\n           {\n             \"column\": \"importance\",\n             \"invert\": false,\n             \"compare\": \"contains\",\n             \"value\": \"A\"\n           }\n         ]\n       }\n       ]\n     }, \n     { \n       \"key\": \"Vendor Invoice\", \n       \"display\": \"Vendor Invoice\", \n       \"conditionalFormat\": [\n         {\n         \"style\": \"warning\",\n         \"type\": \"Audit Report\",\n         \"columnFilter\": [\n           {\n             \"column\": \"importance\",\n             \"invert\": false,\n             \"compare\": \"contains\",\n             \"value\": \"A\"\n           }\n         ]\n       }\n       ]\n     } \n   ], \n   \"viewType\": \"process\", \n   \"views\": [{ \n       \"icon\": \"assets/icon_grc.png\", \n       \"name\": \"GRC Items\", \n       \"type\": \"objectTable\", \n       \"options\": { \n         \"tableWidth\": \"420\",\n         \"tables\": [{ \n             \"title\": \"GRC Items\", \n             \"type\": \"GRC Item\", \n             \"addColumns\": [] \n           }, \n           { \n             \"title\": \"Audit Reports\", \n             \"type\": \"Audit Report\", \n             \"addColumns\": [ { \"name\": \"Rating\", \"field\": \"importance\", \"width\": \"20%\" } ] \n           } \n         ] \n       } \n     }, \n     { \n       \"icon\": \"assets/icon_help.png\", \n       \"name\": \"Help\", \n       \"type\": \"text\", \n       \"options\": { \n         \"text\": \"This is just a placeholder that needs to be filled with proper text.\" \n       } \n     } \n   ] \n }",
    "orderAsc": 170
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "detailed_notes",
    "Title": "Detailed Notes",
    "treeElementId": "{{Tree:Accounts Payable}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Accounts Payable}}/details\",\n  \"enableKeywords\": true,\n  \"showSearch\": true,\n  \"showImpacts\": true,\n  \"impactsRiskCategory\": \"tree/{{Tree:Finance and Accounting}}\",\n  \"impactsRiskCategoryDescription\": \"Finance and Accounting Risks\",\n  \"showNew\": \"top\"\n}",
    "orderAsc": 200
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "accounts_payable",
    "Title": "Total Accounts Payable",
    "treeElementId": "{{Tree:Accounts Payable}}",
    "elementType": "timeSeries",
    "elementOptionsJSON": "{\n \"dataFile\": \"accounts_payable\",\n \"timeColumn\": \"Date\",\n \"lineColumns\": [],\n \"areaColumns\": [\"due in >30d\", \"due in >20d\", \"due in >10d\", \"due in <=10d\", \"overdue\"],\n \"notesEnabled\": true \n}",
    "orderAsc": 300
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "accounts_payable_top_10",
    "Title": "Largest A/P Positions",
    "treeElementId": "{{Tree:Accounts Payable}}",
    "elementType": "stackedBars",
    "elementOptionsJSON": "{\n \"dataFile\": \"accounts_payable_top_10\",\n \"timeColumn\": \"Date\",\n \"barColumn\": \"Vendor\",\n \"notesEnabled\": true \n}",
    "orderAsc": 400
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "accounts_payable_without_p_o",
    "Title": "A/P without Purchase Order",
    "treeElementId": "{{Tree:Accounts Payable}}",
    "elementType": "timeSeries",
    "elementOptionsJSON": "{\n \"dataFile\": \"accounts_payable_without_p_o\",\n \"timeColumn\": \"Date\",\n \"lineColumns\": [\"Paid w/o PO\"],\n \"areaColumns\": [],\n \"notesEnabled\": true \n}",
    "orderAsc": 500
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "accounts_payable_largest_payments",
    "Title": "Largest Payments in FY",
    "treeElementId": "{{Tree:Accounts Payable}}",
    "elementType": "table",
    "elementOptionsJSON": "{\n \"dataFile\": \"accounts_payable_largest_payments\",\n \"timeColumn\": \"End of Period\",\n \"firstColumn\": \"Recipient\",\n \"showColumns\": [\"Payment Date\", \"Account No\", \"Amount\", \"Payment Note\", \"Entered by\", \"Approved\", \"Approved by\"]\n, \"showSparklines\": false,\n \"notesEnabled\": true \n }",
    "orderAsc": 600
  },




  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "summary_ar",
    "Title": "Summary - Accounts Receivable (Sales Cycle)",
    "treeElementId": "{{Tree:Accounts Receivable}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Accounts Receivable}}/summary\",\n  \"enableKeywords\": false,\n  \"showSearch\": false,\n  \"limitTo\": 3,\n  \"showNew\": \"top\"\n}",
    "orderAsc": 150
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "detailed_notes",
    "Title": "Detailed Notes",
    "treeElementId": "{{Tree:Accounts Receivable}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Accounts Receivable}}/details\",\n  \"enableKeywords\": true,\n  \"showSearch\": true,\n  \"showImpacts\": true, \"impactsRiskCategory\": \"tree/{{Tree:Finance and Accounting}}\",\n  \"impactsRiskCategoryDescription\": \"Finance and Accounting Risks\",\n  \"showNew\": \"top\"\n}",
    "orderAsc": 200
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "accounts_receivable",
    "Title": "Total Accounts Receivable",
    "treeElementId": "{{Tree:Accounts Receivable}}",
    "elementType": "timeSeries",
    "elementOptionsJSON": "{\n \"dataFile\": \"accounts_receivable\",\n \"timeColumn\": \"Date\",\n \"lineColumns\": [],\n \"areaColumns\": [\"not due\", \"overdue <5d\", \"overdue <10d\", \"overdue <30d\", \"overdue <90d\", \"overdue <180d\", \"overdue >180d\"],\n \"notesEnabled\": true\n}",
    "orderAsc": 300
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "accounts_receivable_top_10",
    "Title": "Largest A/R Positions",
    "treeElementId": "{{Tree:Accounts Receivable}}",
    "elementType": "stackedBars",
    "elementOptionsJSON": "{\n \"dataFile\": \"accounts_receivable_top_10\",\n \"timeColumn\": \"Date\",\n \"barColumn\": \"Customer\",\n \"notesEnabled\": true \n}",
    "orderAsc": 400
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "overdue_positions",
    "Title": "Overdue Positions",
    "treeElementId": "{{Tree:Accounts Receivable}}",
    "elementType": "portfolio",
    "elementOptionsJSON": "{\n \"dataFile\": \"accounts_receivable_overdue_portfolio\",\n \"timeColumn\": \"Date\",\n \"xColumn\": \"Amount\",\n \"yColumn\": \"Days Overdue\",\n \"groupColumn\": \"Division\",\n \"contentDivide\": \"Region\",\n \"lineColumns\": [\"Regional Risk Indicator\", \"Global Risk Indicator\"],\n \"xLabel\": \"Amount Overdue (in USD)\",\n \"yLabel\": \"Days Overdue\",\n \"contentTitle\": \"Customer / Invoice Details\",\n \"tooltip\": \"{{Customer / Invoice Details}}\\nRegion: {{Region}}\\nAmount Overdue: {{Amount}}\\nDays Overdue: {{Days Overdue}}\",\n \"notesEnabled\": true\n }",
    "orderAsc": 500
  },




  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "risk_matrix",
    "Title": "Risk Matrix",
    "treeElementId": "{{Tree:Publishing Business Divisions}}",
    "elementType": "riskTracker",
    "elementOptionsJSON": "{\n  \"riskCategory\": \"tree/{{Tree:Publishing Business Divisions}}\",\n  \"xLabel\": \"Likelihood\",\n  \"xLabels\": [\"Low\", \"Medium\", \"High\"],\n  \"yLabel\": \"Impact\",\n  \"yLabels\":[\"Low\", \"Medium\", \"High\"],\n  \"maxDaysOld\": 365,\n  \"showImpacts\": true\n}",
    "orderAsc": 150
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "main_notes",
    "Title": "Notes",
    "treeElementId": "{{Tree:Publishing Business Divisions}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n\"keywordID\": \"tree/{{Tree:Publishing Business Divisions}}/details\",\n\"enableKeywords\": true,\n\"showSearch\": true,\n  \"showImpacts\": true, \"impactsRiskCategory\": \"tree/{{Tree:Publishing Business Divisions}}\",\n  \"impactsRiskCategoryDescription\": \"Publishing Business Divisions Risks\",\n  \"showNew\": \"top\"\n}",
    "orderAsc": 200
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "top_customers",
    "Title": "Top Customers by Volume",
    "treeElementId": "{{Tree:Publishing Business Divisions}}",
    "elementType": "scatterplot",
    "elementOptionsJSON": "{\n \"dataFile\": \"top_customers\",\n \"circleSize\": 20,\n \"timeColumn\": \"Date\",\n \"xColumn\": \"Returns\",\n \"yColumn\": \"Avg Discount\",\n \"sizeColumn\": \"Volume\",\n \"groupColumn\": \"Division\",\n \"xLabel\": \"Returns (in %)\",\n \"yLabel\": \"Average Discount (in %)\",\n \"contentTitle\": \"Customer\",\n \"tooltip\": \"{{Customer}}\\n{{Division}}: {{Returns}}% Returns\",\n \"notesEnabled\": true\n }",
    "orderAsc": 250
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "summary_childrens",
    "Title": "Summary - Children's Books",
    "treeElementId": "{{Tree:Publishing Business Divisions}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Childrens Books}}/summary\",\n  \"enableKeywords\": false,\n  \"showSearch\": false,\n  \"limitTo\": 1,\n  \"statusFilter\": [\"Final\", \"Closed\", \"Reviewed\"],\n  \"showNew\": \"\"\n}",
    "orderAsc": 310
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "summary_scifi",
    "Title": "Summary - Science Fiction",
    "treeElementId": "{{Tree:Publishing Business Divisions}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Science Fiction}}/summary\",\n  \"enableKeywords\": false,\n  \"showSearch\": false,\n  \"limitTo\": 1,\n  \"statusFilter\": [\"Final\", \"Closed\", \"Reviewed\"],\n  \"showNew\": \"\"\n}",
    "orderAsc": 320
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "summary_edu",
    "Title": "Summary - Education",
    "treeElementId": "{{Tree:Publishing Business Divisions}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Education}}/summary\",\n  \"enableKeywords\": false,\n  \"showSearch\": false,\n  \"limitTo\": 1,\n  \"statusFilter\": [\"Final\", \"Closed\", \"Reviewed\"],\n  \"showNew\": \"\"\n}",
    "orderAsc": 340
  },




  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "summary_childrens",
    "Title": "Summary - Children's Books",
    "treeElementId": "{{Tree:Childrens Books}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Childrens Books}}/summary\",\n  \"enableKeywords\": false,\n  \"showSearch\": false,\n  \"limitTo\": 3,\n  \"showNew\": \"top\"\n}",
    "orderAsc": 150
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "detailed_notes",
    "Title": "Detailed Notes",
    "treeElementId": "{{Tree:Childrens Books}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Childrens Books}}/details\",\n  \"enableKeywords\": true,\n  \"showSearch\": true,\n  \"showImpacts\": true, \"impactsRiskCategory\": \"tree/{{Tree:Publishing Business Divisions}}\",\n  \"impactsRiskCategoryDescription\": \"Publishing Business Divisions Risks\",\n  \"showNew\": \"top\"\n}",
    "orderAsc": 200
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "sales_volume",
    "Title": "Sales Volume by Region",
    "treeElementId": "{{Tree:Childrens Books}}",
    "elementType": "timeSeries",
    "elementOptionsJSON": "{\n \"dataFile\": \"sales_volume\",\n \"timeColumn\": \"Date\",\n \"lineColumns\": [],\n \"areaColumns\": [\"Children – US\", \"Children – Canada\", \"Children – Europe\", \"Children – Rest\"],\n \"notesEnabled\": true\n}",
    "orderAsc": 400
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "returns",
    "Title": "Returns (% of Volume)",
    "treeElementId": "{{Tree:Childrens Books}}",
    "elementType": "timeSeries",
    "elementOptionsJSON": "{\n \"dataFile\": \"returns\",\n \"timeColumn\": \"Date\",\n \"lineColumns\": [\"Children – US\", \"Children – Canada\", \"Children – Europe\", \"Children – Rest\"],\n \"areaColumns\": [],\n \"notesEnabled\": true\n}",
    "orderAsc": 500
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "new_releases",
    "Title": "New Releases",
    "treeElementId": "{{Tree:Childrens Books}}",
    "elementType": "timeSeries",
    "elementOptionsJSON": "{\n \"dataFile\": \"new_releases\",\n \"timeColumn\": \"Date\",\n \"lineColumns\": [\"Children\"],\n \"areaColumns\": [],\n \"notesEnabled\": true\n}",
    "orderAsc": 600
  },




  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "summary_scifi",
    "Title": "Summary - Science Fiction",
    "treeElementId": "{{Tree:Science Fiction}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Science Fiction}}/summary\",\n  \"enableKeywords\": false,\n  \"showSearch\": false,\n  \"limitTo\": 3,\n  \"showNew\": \"top\"\n}",
    "orderAsc": 150
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "detailed_notes",
    "Title": "Detailed Notes",
    "treeElementId": "{{Tree:Science Fiction}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Science Fiction}}/details\",\n  \"enableKeywords\": true,\n  \"showSearch\": true,\n  \"showImpacts\": true, \"impactsRiskCategory\": \"tree/{{Tree:Publishing Business Divisions}}\",\n  \"impactsRiskCategoryDescription\": \"Publishing Business Divisions Risks\",\n  \"showNew\": \"top\"\n}",
    "orderAsc": 200
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "sales_volume",
    "Title": "Sales Volume by Region",
    "treeElementId": "{{Tree:Science Fiction}}",
    "elementType": "timeSeries",
    "elementOptionsJSON": "{\n \"dataFile\": \"sales_volume\",\n \"timeColumn\": \"Date\",\n \"lineColumns\": [],\n \"areaColumns\": [\"Sci-Fi – US\", \"Sci-Fi – Canada\", \"Sci-Fi – Europe\", \"Sci-Fi – Rest\"],\n \"notesEnabled\": true\n}",
    "orderAsc": 400
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "returns",
    "Title": "Returns (% of Volume)",
    "treeElementId": "{{Tree:Science Fiction}}",
    "elementType": "timeSeries",
    "elementOptionsJSON": "{\n \"dataFile\": \"returns\",\n \"timeColumn\": \"Date\",\n \"lineColumns\": [\"Sci-Fi – US\", \"Sci-Fi – Canada\", \"Sci-Fi – Europe\", \"Sci-Fi – Rest\"],\n \"areaColumns\": [],\n \"notesEnabled\": true\n}",
    "orderAsc": 500
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "new_releases",
    "Title": "New Releases",
    "treeElementId": "{{Tree:Science Fiction}}",
    "elementType": "timeSeries",
    "elementOptionsJSON": "{\n \"dataFile\": \"new_releases\",\n \"timeColumn\": \"Date\",\n \"lineColumns\": [\"Sci-Fi\"],\n \"areaColumns\": [],\n \"notesEnabled\": true\n}",
    "orderAsc": 600
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "top_authors_scifi",
    "Title": "Top Authors Total Sales",
    "treeElementId": "{{Tree:Science Fiction}}",
    "elementType": "table",
    "elementOptionsJSON": "{\n \"dataFile\": \"top_authors_scifi\",\n \"timeColumn\": \"Date\",\n \"firstColumn\": \"Author\",\n \"columnColumn\": \"column\",\n \"valueColumn\": \"Total Sales\",\n \"showSparklines\": true,\n \"sparklinesUseGlobalMax\": false,\n \"notesEnabled\": true \n }",
    "orderAsc": 700
  },




  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "summary_edu",
    "Title": "Summary - Education",
    "treeElementId": "{{Tree:Education}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Education}}/summary\",\n  \"enableKeywords\": false,\n  \"showSearch\": false,\n  \"limitTo\": 3,\n  \"showNew\": \"top\"\n}",
    "orderAsc": 150
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "detailed_notes",
    "Title": "Detailed Notes",
    "treeElementId": "{{Tree:Education}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Education}}/details\",\n  \"enableKeywords\": true,\n  \"showSearch\": true,\n  \"showImpacts\": true, \"impactsRiskCategory\": \"tree/{{Tree:Publishing Business Divisions}}\",\n  \"impactsRiskCategoryDescription\": \"Publishing Business Divisions Risks\",\n  \"showNew\": \"top\"\n}",
    "orderAsc": 200
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "sales_volume",
    "Title": "Sales Volume by Region",
    "treeElementId": "{{Tree:Education}}",
    "elementType": "timeSeries",
    "elementOptionsJSON": "{\n \"dataFile\": \"sales_volume\",\n \"timeColumn\": \"Date\",\n \"lineColumns\": [],\n \"areaColumns\": [\"Education – US\", \"Education – Canada\", \"Education – Europe\", \"Education – Rest\"],\n \"notesEnabled\": true\n}",
    "orderAsc": 400
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "returns",
    "Title": "Returns (% of Volume)",
    "treeElementId": "{{Tree:Education}}",
    "elementType": "timeSeries",
    "elementOptionsJSON": "{\n \"dataFile\": \"returns\",\n \"timeColumn\": \"Date\",\n \"lineColumns\": [\"Education – US\", \"Education – Canada\", \"Education – Europe\", \"Education – Rest\"],\n \"areaColumns\": [],\n \"notesEnabled\": true\n}",
    "orderAsc": 500
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "new_releases",
    "Title": "New Releases",
    "treeElementId": "{{Tree:Education}}",
    "elementType": "timeSeries",
    "elementOptionsJSON": "{\n \"dataFile\": \"new_releases\",\n \"timeColumn\": \"Date\",\n \"lineColumns\": [\"Education\"],\n \"areaColumns\": [],\n \"notesEnabled\": true\n}",
    "orderAsc": 600
  },




  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "risk_matrix",
    "Title": "Risk Matrix",
    "treeElementId": "{{Tree:Operations and HR}}",
    "elementType": "riskTracker",
    "elementOptionsJSON": "{\n  \"riskCategory\": \"tree/{{Tree:Operations and HR}}\",\n  \"xLabel\": \"Likelihood\",\n  \"xLabels\": [\"Low\", \"Medium\", \"High\"],\n  \"yLabel\": \"Impact\",\n  \"yLabels\":[\"Low\", \"Medium\", \"High\"],\n  \"maxDaysOld\": 365,\n  \"showImpacts\": true\n}",
    "orderAsc": 150
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "main_notes",
    "Title": "Notes",
    "treeElementId": "{{Tree:Operations and HR}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n\"keywordID\": \"tree/{{Tree:Operations and HR}}/details\",\n\"enableKeywords\": true,\n\"showSearch\": true,\n  \"showImpacts\": true, \"impactsRiskCategory\": \"tree/{{Tree:Operations and HR}}\",\n  \"impactsRiskCategoryDescription\": \"Operations and HR Risks\",\n  \"showNew\": \"top\"\n}",
    "orderAsc": 200
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "summary_hr",
    "Title": "Summary - Human Resources",
    "treeElementId": "{{Tree:Operations and HR}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Human Resources}}/summary\",\n  \"enableKeywords\": false,\n  \"showSearch\": false,\n  \"limitTo\": 1,\n  \"statusFilter\": [\"Final\", \"Closed\", \"Reviewed\"],\n  \"showNew\": \"\"\n}",
    "orderAsc": 310
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "summary_warehouse",
    "Title": "Summary - Warehousing and Distribution",
    "treeElementId": "{{Tree:Operations and HR}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Warehousing and Distribution}}/summary\",\n  \"enableKeywords\": false,\n  \"showSearch\": false,\n  \"limitTo\": 1,\n  \"statusFilter\": [\"Final\", \"Closed\", \"Reviewed\"],\n  \"showNew\": \"\"\n}",
    "orderAsc": 320
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "summary_printers",
    "Title": "Summary - Anthony & Anthony Printers Inc.",
    "treeElementId": "{{Tree:Operations and HR}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Anthony Anthony Printers Inc}}/summary\",\n  \"enableKeywords\": false,\n  \"showSearch\": false,\n  \"limitTo\": 1,\n  \"statusFilter\": [\"Final\", \"Closed\", \"Reviewed\"],\n  \"showNew\": \"\"\n}",
    "orderAsc": 320
  },




  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "summary_hr",
    "Title": "Summary - Human Resources",
    "treeElementId": "{{Tree:Human Resources}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Human Resources}}/summary\",\n  \"enableKeywords\": false,\n  \"showSearch\": false,\n  \"limitTo\": 3,\n  \"showNew\": \"top\"\n}",
    "orderAsc": 150
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "detailed_notes",
    "Title": "Detailed Notes",
    "treeElementId": "{{Tree:Human Resources}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Human Resources}}/details\",\n  \"enableKeywords\": true,\n  \"showSearch\": true,\n  \"showImpacts\": true, \"impactsRiskCategory\": \"tree/{{Tree:Operations and HR}}\",\n  \"impactsRiskCategoryDescription\": \"Operations and HR Risks\",\n  \"showNew\": \"top\"\n}",
    "orderAsc": 200
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "total_staff_fte",
    "Title": "Total Staff FTE",
    "treeElementId": "{{Tree:Human Resources}}",
    "elementType": "timeSeries",
    "elementOptionsJSON": "{\n \"dataFile\": \"total_staff\",\n \"timeColumn\": \"Date\",\n \"areaColumns\": [\"Administration\", \"Publishing\", \"Printing\", \"Marketing\"],\n \"notesEnabled\": true\n}",
    "orderAsc": 300
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "total_staff_turnover",
    "Title": "Turnover (in %)",
    "treeElementId": "{{Tree:Human Resources}}",
    "elementType": "timeSeries",
    "elementOptionsJSON": "{\n \"dataFile\": \"total_staff\",\n \"timeColumn\": \"Date\",\n \"lineColumns\": [\"Administration – Turnover\", \"Publishing – Turnover\", \"Printing – Turnover\", \"Marketing – Turnover\"],\n \"notesEnabled\": true\n}",
    "orderAsc": 400
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "total_staff_cost",
    "Title": "Personnel cost per FTE",
    "treeElementId": "{{Tree:Human Resources}}",
    "elementType": "timeSeries",
    "elementOptionsJSON": "{\n \"dataFile\": \"total_staff\",\n \"timeColumn\": \"Date\",\n \"lineColumns\": [\"Administration – Cost per FTE\", \"Publishing – Cost per FTE\", \"Printing – Cost per FTE\", \"Marketing – Cost per FTE\"],\n \"notesEnabled\": true\n}",
    "orderAsc": 500
  },




  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "summary_warehouse",
    "Title": "Summary - Warehousing and Distribution",
    "treeElementId": "{{Tree:Warehousing and Distribution}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Warehousing and Distribution}}/summary\",\n  \"enableKeywords\": false,\n  \"showSearch\": false,\n  \"limitTo\": 3,\n  \"showNew\": \"top\"\n}",
    "orderAsc": 150
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "detailed_notes",
    "Title": "Detailed Notes",
    "treeElementId": "{{Tree:Warehousing and Distribution}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Warehousing and Distribution}}/details\",\n  \"enableKeywords\": true,\n  \"showSearch\": true,\n  \"showImpacts\": true, \"impactsRiskCategory\": \"tree/{{Tree:Operations and HR}}\",\n  \"impactsRiskCategoryDescription\": \"Operations and HR Risks\",\n  \"showNew\": \"top\"\n}",
    "orderAsc": 200
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "costs_per_book_delivered",
    "Title": "Cost per book delivered",
    "treeElementId": "{{Tree:Warehousing and Distribution}}",
    "elementType": "stackedBars",
    "elementOptionsJSON": "{\n \"dataFile\": \"costs_per_book_delivered\",\n \"timeColumn\": \"Date\",\n \"barColumn\": \"Warehouse\",\n \"showColumns\": [\"PP&E\", \"Personnel\", \"Shrinkage\", \"Transport\"],\n \"notesEnabled\": true\n}",
    "orderAsc": 300
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "shrinkage",
    "Title": "Shrinkage (in % of value)",
    "treeElementId": "{{Tree:Warehousing and Distribution}}",
    "elementType": "timeSeries",
    "elementOptionsJSON": "{\n \"timeColumn\": \"Date\",\n \"dataFile\": \"shrinkage\",\n \"lineColumns\": [\"Portland, OR\", \"Phoenix, AZ\", \"Petersburg, VA\", \"Kansas City, KS\", \"Altamonte Springs, FL\", \"Mississauga, ON\", \"Strasbourg, France\" ],\n \"areaColumns\": [],\n \"notesEnabled\": true\n}",
    "orderAsc": 400
  },




  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "summary_printers",
    "Title": "Summary - Anthony & Anthony Printers Inc.",
    "treeElementId": "{{Tree:Anthony Anthony Printers Inc}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Anthony Anthony Printers Inc}}/summary\",\n  \"enableKeywords\": false,\n  \"showSearch\": false,\n  \"limitTo\": 3,\n  \"showNew\": \"top\"\n}",
    "orderAsc": 150
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "detailed_notes",
    "Title": "Detailed Notes",
    "treeElementId": "{{Tree:Anthony Anthony Printers Inc}}",
    "elementType": "notes",
    "elementOptionsJSON": "{\n  \"keywordID\": \"tree/{{Tree:Anthony Anthony Printers Inc}}/details\",\n  \"enableKeywords\": true,\n  \"showSearch\": true,\n  \"showImpacts\": true, \"impactsRiskCategory\": \"tree/{{Tree:Operations and HR}}\",\n  \"impactsRiskCategoryDescription\": \"Operations and HR Risks\",\n  \"showNew\": \"top\"\n}",
    "orderAsc": 200
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "printing_volume",
    "Title": "Total Printing Volume",
    "treeElementId": "{{Tree:Anthony Anthony Printers Inc}}",
    "elementType": "timeSeries",
    "elementOptionsJSON": "{\n \"dataFile\": \"printing_volume\",\n \"timeColumn\": \"Date\",\n \"areaColumns\": [\"Internal\", \"External\"],\n \"notesEnabled\": true \n}",
    "orderAsc": 300
  },
  {
    "_action": "addEntry",
    "_endpoint": "views",
    "viewKey": "printers_operating_risk",
    "Title": "Operational Risk",
    "treeElementId": "{{Tree:Anthony Anthony Printers Inc}}",
    "elementType": "table",
    "elementOptionsJSON": "{\n \"dataFile\": \"printers_operating_risk\",\n \"timeColumn\": \"Date\",\n \"firstColumn\": \"Measure\",\n \"showColumns\": [\"Current\", \"Q-1\", \"Q-2\", \"Q-3\", \"Q-4\", \"Q-5\", \"Q-6\", \"Q-7\", \"Q-8\", \"Q-9\", \"Q-10\", \"Q-11\", \"Q-12\"]\n, \"showSparklines\": true, \"sparklinesUseGlobalMax\": false,\n \"notesEnabled\": true \n }",
    "orderAsc": 400
  },




  {
    "_action": "addEntry",
    "_endpoint": "datafiles",
    "Title": "accounts_payable",
    "Description": "A/P: Total Volume",
    "fileName": "assets/example_data/accounts_payable.csv",
    "dataFileFormatJSON": "{\n \"separator\": \",\",\n \"columns\": [{\n \"source\": \"Date\",\n \"display\": \"Date\",\n \"keyword\": \"Date\",\n \"format\": \"date\",\n \"isKey\": true\n }, {\n \"source\": \"due in >30d\",\n \"display\": \"due in >30d\",\n \"keyword\": \"due_in__30d\",\n \"format\": \"integer\"\n }, {\n \"source\": \"due in >20d\",\n \"display\": \"due in >20d\",\n \"keyword\": \"due_in__20d\",\n \"format\": \"integer\"\n }, {\n \"source\": \"due in >10d\",\n \"display\": \"due in >10d\",\n \"keyword\": \"due_in__10d\",\n \"format\": \"integer\"\n }, {\n \"source\": \"due in <=10d\",\n \"display\": \"due in <=10d\",\n \"keyword\": \"due_in___10d\",\n \"format\": \"integer\"\n }, {\n \"source\": \"overdue\",\n \"display\": \"overdue\",\n \"keyword\": \"overdue\",\n \"format\": \"integer\"\n }]\n}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "datafiles",
    "Title": "accounts_payable_largest_payments",
    "Description": "A/P: Largest Payments",
    "fileName": "assets/example_data/accounts_payable_largest_payments.csv",
    "dataFileFormatJSON": "{\n \"separator\": \",\",\n \"columns\": [{\n \"source\": \"End of Period\",\n \"display\": \"End of Period\",\n \"keyword\": \"End_of_Period\",\n \"format\": \"date\"\n }, {\n \"source\": \"Payment Date\",\n \"display\": \"Payment Date\",\n \"keyword\": \"Payment_Date\",\n \"format\": \"date\",\n \"isKey\": true\n }, {\n \"source\": \"Payment Details\",\n \"display\": \"Payment Details\",\n \"keyword\": \"Payment_Details\",\n \"format\": \"text\",\n \"isKey\": true\n }, {\n \"source\": \"Recipient\",\n \"display\": \"Recipient\",\n \"keyword\": \"Recipient\",\n \"format\": \"text\"\n }, {\n \"source\": \"Account No\",\n \"display\": \"Account No\",\n \"keyword\": \"Account_No\",\n \"format\": \"text\",\n \"isKey\": true\n }, {\n \"source\": \"Amount\",\n \"display\": \"Amount\",\n \"keyword\": \"Amount\",\n \"format\": \"number\"\n }, {\n \"source\": \"Payment Note\",\n \"display\": \"Payment Note\",\n \"keyword\": \"Payment_Note\",\n \"format\": \"text\"\n }, {\n \"source\": \"Entered by\",\n \"display\": \"Entered by\",\n \"keyword\": \"Entered_by\",\n \"format\": \"text\"\n }, {\n \"source\": \"Approved\",\n \"display\": \"Approved\",\n \"keyword\": \"Approved\",\n \"format\": \"yesNo\"\n }, {\n \"source\": \"Approved by\",\n \"display\": \"Approved by\",\n \"keyword\": \"Approved_by\",\n \"format\": \"text\"\n }]\n}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "datafiles",
    "Title": "accounts_payable_top_10",
    "Description": "A/P: Top 10 Open Positions",
    "fileName": "assets/example_data/accounts_payable_top_10.csv",
    "dataFileFormatJSON": "{\n \"separator\": \",\",\n \"columns\": [{\n \"source\": \"Date\",\n \"display\": \"Date\",\n \"keyword\": \"Date\",\n \"format\": \"date\",\n \"isKey\": true\n }, {\n \"source\": \"Vendor\",\n \"display\": \"Vendor\",\n \"keyword\": \"Vendor\",\n \"format\": \"text\",\n \"isKey\": true\n }, {\n \"source\": \"due >30d\",\n \"display\": \"due >30d\",\n \"keyword\": \"due__30d\",\n \"format\": \"integer\"\n }, {\n \"source\": \"due >20d\",\n \"display\": \"due >20d\",\n \"keyword\": \"due__20d\",\n \"format\": \"integer\"\n }, {\n \"source\": \"due >10d\",\n \"display\": \"due >10d\",\n \"keyword\": \"due__10d\",\n \"format\": \"integer\"\n }, {\n \"source\": \"due <=10d\",\n \"display\": \"due <=10d\",\n \"keyword\": \"due___10d\",\n \"format\": \"integer\"\n }, {\n \"source\": \"overdue\",\n \"display\": \"overdue\",\n \"keyword\": \"overdue\",\n \"format\": \"integer\"\n }]\n}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "datafiles",
    "Title": "accounts_payable_without_p_o",
    "Description": "A/P: Accounts Payable without Purchase Order",
    "fileName": "assets/example_data/accounts_payable_without_p_o.csv",
    "dataFileFormatJSON": "{\n \"separator\": \",\",\n \"columns\": [{\n \"source\": \"Date\",\n \"display\": \"Date\",\n \"keyword\": \"Date\",\n \"format\": \"date\",\n \"isKey\": true\n }, {\n \"source\": \"Paid w/o PO\",\n \"display\": \"Paid w/o PO\",\n \"keyword\": \"Paid_w_o_PO\",\n \"format\": \"percentage\"\n }]\n}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "datafiles",
    "Title": "accounts_receivable_overdue_portfolio",
    "Description": "A/R: Overdue Accounts Receivable",
    "fileName": "assets/example_data/accounts_receivable_overdue_portfolio.csv",
    "dataFileFormatJSON": "{\n \"separator\": \",\",\n \"columns\": [{\n \"source\": \"Date\",\n \"display\": \"Date\",\n \"keyword\": \"Date\",\n \"format\": \"date\",\n \"isKey\": true\n }, {\n \"source\": \"Region\",\n \"display\": \"Region\",\n \"keyword\": \"Region\",\n \"format\": \"text\"\n }, {\n \"source\": \"Customer / Invoice Details\",\n \"display\": \"Customer / Invoice Details\",\n \"keyword\": \"Customer___Invoice_Details\",\n \"format\": \"text\",\n \"isKey\": true\n }, {\n \"source\": \"Division\",\n \"display\": \"Division\",\n \"keyword\": \"Division\",\n \"format\": \"text\",\n \"isKey\": false\n }, {\n \"source\": \"Amount\",\n \"display\": \"Amount\",\n \"keyword\": \"Amount\",\n \"format\": \"number\",\n \"isKey\": true\n }, {\n \"source\": \"Days Overdue\",\n \"display\": \"Days Overdue\",\n \"keyword\": \"Days_Overdue\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Regional Risk Indicator\",\n \"display\": \"Regional Risk Indicator\",\n \"keyword\": \"Regional_Risk_Indicator\",\n \"format\": \"integer\",\n \"isKey\": false\n }, {\n \"source\": \"Global Risk Indicator\",\n \"display\": \"Global Risk Indicator\",\n \"keyword\": \"Global_Risk_Indicator\",\n \"format\": \"integer\",\n \"isKey\": false\n }]\n}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "datafiles",
    "Title": "costs_per_book_delivered",
    "Description": "Costs per book delivered (Warehousing and Distribution)",
    "fileName": "assets/example_data/costs_per_book_delivered.csv",
    "dataFileFormatJSON": "{\n \"separator\": \",\",\n \"columns\": [{\n \"source\": \"Date\",\n \"display\": \"Date\",\n \"keyword\": \"Date\",\n \"format\": \"date\",\n \"isKey\": true\n }, {\n \"source\": \"Warehouse\",\n \"display\": \"Warehouse\",\n \"keyword\": \"Warehouse\",\n \"format\": \"text\",\n \"isKey\": true\n }, {\n \"source\": \"PP&E\",\n \"display\": \"PP&E\",\n \"keyword\": \"PP_E\",\n \"format\": \"number\"\n }, {\n \"source\": \"Personnel\",\n \"display\": \"Personnel\",\n \"keyword\": \"Personnel\",\n \"format\": \"number\"\n }, {\n \"source\": \"Shrinkage\",\n \"display\": \"Shrinkage\",\n \"keyword\": \"Shrinkage\",\n \"format\": \"number\"\n }, {\n \"source\": \"Transport\",\n \"display\": \"Transport\",\n \"keyword\": \"Transport\",\n \"format\": \"number\"\n }]\n}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "datafiles",
    "Title": "accounts_receivable_top_10",
    "Description": "A/R: Top 10 Positions",
    "fileName": "assets/example_data/accounts_receivable_top_10.csv",
    "dataFileFormatJSON": "{\n \"separator\": \",\",\n \"columns\": [{\n \"source\": \"Date\",\n \"display\": \"Date\",\n \"keyword\": \"Date\",\n \"format\": \"date\",\n \"isKey\": true\n }, {\n \"source\": \"Customer\",\n \"display\": \"Customer\",\n \"keyword\": \"Customer\",\n \"format\": \"text\",\n \"isKey\": true\n }, {\n \"source\": \"not due\",\n \"display\": \"not due\",\n \"keyword\": \"not_due\",\n \"format\": \"integer\"\n }, {\n \"source\": \"overdue <10d\",\n \"display\": \"overdue <10d\",\n \"keyword\": \"overdue__10d\",\n \"format\": \"integer\"\n }, {\n \"source\": \"overdue <30d\",\n \"display\": \"overdue <30d\",\n \"keyword\": \"overdue__30d\",\n \"format\": \"integer\"\n }, {\n \"source\": \"overdue <90d\",\n \"display\": \"overdue <90d\",\n \"keyword\": \"overdue__90d\",\n \"format\": \"integer\"\n }, {\n \"source\": \"overdue >90d\",\n \"display\": \"overdue >90d\",\n \"keyword\": \"overdue__90d\",\n \"format\": \"integer\"\n }]\n}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "datafiles",
    "Title": "new_releases",
    "Description": "New Releases (Count)",
    "fileName": "assets/example_data/new_releases.csv",
    "dataFileFormatJSON": "{\n \"separator\": \",\",\n \"columns\": [{\n \"source\": \"Date\",\n \"display\": \"Date\",\n \"keyword\": \"Date\",\n \"format\": \"date\",\n \"isKey\": true\n }, {\n \"source\": \"Children\",\n \"display\": \"Children\",\n \"keyword\": \"Children\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Sci-Fi\",\n \"display\": \"Sci-Fi\",\n \"keyword\": \"Sci_Fi\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Education\",\n \"display\": \"Education\",\n \"keyword\": \"Education\",\n \"format\": \"integer\"\n }]\n}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "datafiles",
    "Title": "accounts_receivable",
    "Description": "A/R: Total Volume",
    "fileName": "assets/example_data/accounts_receivable.csv",
    "dataFileFormatJSON": "{\n \"separator\": \",\",\n \"columns\": [{\n \"source\": \"Date\",\n \"display\": \"Date\",\n \"keyword\": \"Date\",\n \"format\": \"date\",\n \"isKey\": true\n }, {\n \"source\": \"not due\",\n \"display\": \"not due\",\n \"keyword\": \"not_due\",\n \"format\": \"integer\"\n }, {\n \"source\": \"overdue <5d\",\n \"display\": \"overdue <5d\",\n \"keyword\": \"overdue__5d\",\n \"format\": \"integer\"\n }, {\n \"source\": \"overdue <10d\",\n \"display\": \"overdue <10d\",\n \"keyword\": \"overdue__10d\",\n \"format\": \"integer\"\n }, {\n \"source\": \"overdue <30d\",\n \"display\": \"overdue <30d\",\n \"keyword\": \"overdue__30d\",\n \"format\": \"integer\"\n }, {\n \"source\": \"overdue <90d\",\n \"display\": \"overdue <90d\",\n \"keyword\": \"overdue__90d\",\n \"format\": \"integer\"\n }, {\n \"source\": \"overdue <180d\",\n \"display\": \"overdue <180d\",\n \"keyword\": \"overdue__180d\",\n \"format\": \"integer\"\n }, {\n \"source\": \"overdue >180d\",\n \"display\": \"overdue >180d\",\n \"keyword\": \"overdue__180d\",\n \"format\": \"integer\"\n }]\n}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "datafiles",
    "Title": "printers_operating_risk",
    "Description": "Printers: Operating Risk Table",
    "fileName": "assets/example_data/printers_operating_risk.csv",
    "dataFileFormatJSON": "{\n \"separator\": \",\",\n \"columns\": [{\n \"source\": \"Date\",\n \"display\": \"Date\",\n \"keyword\": \"Date\",\n \"format\": \"date\",\n \"isKey\": true\n }, {\n \"source\": \"Measure\",\n \"display\": \"Measure\",\n \"keyword\": \"Measure\",\n \"format\": \"text\",\n \"isKey\": true\n }, {\n \"source\": \"Current\",\n \"display\": \"Current\",\n \"keyword\": \"Current\",\n \"format\": \"number\"\n }, {\n \"source\": \"Q-1\",\n \"display\": \"Q-1\",\n \"keyword\": \"Q_1\",\n \"format\": \"number\"\n }, {\n \"source\": \"Q-2\",\n \"display\": \"Q-2\",\n \"keyword\": \"Q_2\",\n \"format\": \"number\"\n }, {\n \"source\": \"Q-3\",\n \"display\": \"Q-3\",\n \"keyword\": \"Q_3\",\n \"format\": \"number\"\n }, {\n \"source\": \"Q-4\",\n \"display\": \"Q-4\",\n \"keyword\": \"Q_4\",\n \"format\": \"number\"\n }, {\n \"source\": \"Q-5\",\n \"display\": \"Q-5\",\n \"keyword\": \"Q_5\",\n \"format\": \"number\"\n }, {\n \"source\": \"Q-6\",\n \"display\": \"Q-6\",\n \"keyword\": \"Q_6\",\n \"format\": \"number\"\n }, {\n \"source\": \"Q-7\",\n \"display\": \"Q-7\",\n \"keyword\": \"Q_7\",\n \"format\": \"number\"\n }, {\n \"source\": \"Q-8\",\n \"display\": \"Q-8\",\n \"keyword\": \"Q_8\",\n \"format\": \"number\"\n }, {\n \"source\": \"Q-9\",\n \"display\": \"Q-9\",\n \"keyword\": \"Q_9\",\n \"format\": \"number\"\n }, {\n \"source\": \"Q-10\",\n \"display\": \"Q-10\",\n \"keyword\": \"Q_10\",\n \"format\": \"number\"\n }, {\n \"source\": \"Q-11\",\n \"display\": \"Q-11\",\n \"keyword\": \"Q_11\",\n \"format\": \"number\"\n }, {\n \"source\": \"Q-12\",\n \"display\": \"Q-12\",\n \"keyword\": \"Q_12\",\n \"format\": \"number\"\n }]\n}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "datafiles",
    "Title": "returns",
    "Description": "Returns (in % of total)",
    "fileName": "assets/example_data/returns.csv",
    "dataFileFormatJSON": "{\n \"separator\": \",\",\n \"columns\": [{\n \"source\": \"Date\",\n \"display\": \"Date\",\n \"keyword\": \"Date\",\n \"format\": \"date\",\n \"isKey\": true\n }, {\n \"source\": \"Children – US\",\n \"display\": \"Children – US\",\n \"keyword\": \"Children___US\",\n \"format\": \"number\"\n }, {\n \"source\": \"Children – Canada\",\n \"display\": \"Children – Canada\",\n \"keyword\": \"Children___Canada\",\n \"format\": \"number\"\n }, {\n \"source\": \"Children – Europe\",\n \"display\": \"Children – Europe\",\n \"keyword\": \"Children___Europe\",\n \"format\": \"number\"\n }, {\n \"source\": \"Children – Rest\",\n \"display\": \"Children – Rest\",\n \"keyword\": \"Children___Rest\",\n \"format\": \"number\"\n }, {\n \"source\": \"Sci-Fi – US\",\n \"display\": \"Sci-Fi – US\",\n \"keyword\": \"Sci_Fi___US\",\n \"format\": \"number\"\n }, {\n \"source\": \"Sci-Fi – Canada\",\n \"display\": \"Sci-Fi – Canada\",\n \"keyword\": \"Sci_Fi___Canada\",\n \"format\": \"number\"\n }, {\n \"source\": \"Sci-Fi – Europe\",\n \"display\": \"Sci-Fi – Europe\",\n \"keyword\": \"Sci_Fi___Europe\",\n \"format\": \"number\"\n }, {\n \"source\": \"Sci-Fi – Rest\",\n \"display\": \"Sci-Fi – Rest\",\n \"keyword\": \"Sci_Fi___Rest\",\n \"format\": \"number\"\n }, {\n \"source\": \"Education – US\",\n \"display\": \"Education – US\",\n \"keyword\": \"Education___US\",\n \"format\": \"number\"\n }, {\n \"source\": \"Education – Canada\",\n \"display\": \"Education – Canada\",\n \"keyword\": \"Education___Canada\",\n \"format\": \"number\"\n }, {\n \"source\": \"Education – Europe\",\n \"display\": \"Education – Europe\",\n \"keyword\": \"Education___Europe\",\n \"format\": \"number\"\n }, {\n \"source\": \"Education – Rest\",\n \"display\": \"Education – Rest\",\n \"keyword\": \"Education___Rest\",\n \"format\": \"number\"\n }]\n}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "datafiles",
    "Title": "printing_volume",
    "Description": "Printers: Total Printing Volume (in m2)",
    "fileName": "assets/example_data/printing_volume.csv",
    "dataFileFormatJSON": "{\n \"separator\": \",\",\n \"columns\": [{\n \"source\": \"Date\",\n \"display\": \"Date\",\n \"keyword\": \"Date\",\n \"format\": \"date\",\n \"isKey\": true\n }, {\n \"source\": \"Internal\",\n \"display\": \"Internal\",\n \"keyword\": \"Internal\",\n \"format\": \"integer\"\n }, {\n \"source\": \"External\",\n \"display\": \"External\",\n \"keyword\": \"External\",\n \"format\": \"integer\"\n }]\n}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "datafiles",
    "Title": "shrinkage",
    "Description": "Shrinkage (in % of total)",
    "fileName": "assets/example_data/shrinkage.csv",
    "dataFileFormatJSON": "{\n \"separator\": \",\",\n \"columns\": [{\n \"source\": \"Date\",\n \"display\": \"Date\",\n \"keyword\": \"Date\",\n \"format\": \"date\",\n \"isKey\": true\n }, {\n \"source\": \"Portland, OR\",\n \"display\": \"Portland, OR\",\n \"keyword\": \"Portland__OR\",\n \"format\": \"number\"\n }, {\n \"source\": \"Phoenix, AZ\",\n \"display\": \"Phoenix, AZ\",\n \"keyword\": \"Phoenix__AZ\",\n \"format\": \"number\"\n }, {\n \"source\": \"Petersburg, VA\",\n \"display\": \"Petersburg, VA\",\n \"keyword\": \"Petersburg__VA\",\n \"format\": \"number\"\n }, {\n \"source\": \"Kansas City, KS\",\n \"display\": \"Kansas City, KS\",\n \"keyword\": \"Kansas_City__KS\",\n \"format\": \"number\"\n }, {\n \"source\": \"Altamonte Springs, FL\",\n \"display\": \"Altamonte Springs, FL\",\n \"keyword\": \"Altamonte_Springs__FL\",\n \"format\": \"number\"\n }, {\n \"source\": \"Mississauga,ON\",\n \"display\": \"Mississauga, ON\",\n \"keyword\": \"Mississauga__ON\",\n \"format\": \"number\"\n }, {\n \"source\": \"Strasbourg, France\",\n \"display\": \"Strasbourg, France\",\n \"keyword\": \"Strasbourg__France\",\n \"format\": \"number\"\n }]\n}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "datafiles",
    "Title": "top_customers",
    "Description": "Top Customers",
    "fileName": "assets/example_data/top_customers.csv",
    "dataFileFormatJSON": "{\n \"separator\": \",\",\n \"columns\": [{\n \"source\": \"Date\",\n \"display\": \"Date\",\n \"keyword\": \"Date\",\n \"format\": \"date\",\n \"isKey\": true\n }, {\n \"source\": \"Division\",\n \"display\": \"Division\",\n \"keyword\": \"Division\",\n \"format\": \"text\",\n \"isKey\": true\n }, {\n \"source\": \"Customer\",\n \"display\": \"Customer\",\n \"keyword\": \"Customer\",\n \"format\": \"text\",\n \"isKey\": true\n }, {\n \"source\": \"Volume\",\n \"display\": \"Volume\",\n \"keyword\": \"Volume\",\n \"format\": \"number\"\n }, {\n \"source\": \"Returns\",\n \"display\": \"Returns\",\n \"keyword\": \"Returns\",\n \"format\": \"number\"\n }, {\n \"source\": \"Avg Discount\",\n \"display\": \"Avg Discount\",\n \"keyword\": \"Avg_Discount\",\n \"format\": \"number\"\n }, {\n \"source\": \"Key\",\n \"display\": \"Key\",\n \"keyword\": \"Key\",\n \"format\": \"text\"\n }]\n}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "datafiles",
    "Title": "total_staff",
    "Description": "Total Staff FTE (incl. Turnover and Costs per FTE)",
    "fileName": "assets/example_data/total_staff.csv",
    "dataFileFormatJSON": "{\n \"separator\": \",\",\n \"columns\": [{\n \"source\": \"Date\",\n \"display\": \"Date\",\n \"keyword\": \"Date\",\n \"format\": \"date\",\n \"isKey\": true\n }, {\n \"source\": \"Administration\",\n \"display\": \"Administration\",\n \"keyword\": \"Administration\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Publishing\",\n \"display\": \"Publishing\",\n \"keyword\": \"Publishing\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Printing\",\n \"display\": \"Printing\",\n \"keyword\": \"Printing\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Marketing\",\n \"display\": \"Marketing\",\n \"keyword\": \"Marketing\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Administration – Turnover\",\n \"display\": \"Administration – Turnover\",\n \"keyword\": \"Administration___Turnover\",\n \"format\": \"number\"\n }, {\n \"source\": \"Publishing – Turnover\",\n \"display\": \"Publishing – Turnover\",\n \"keyword\": \"Publishing___Turnover\",\n \"format\": \"number\"\n }, {\n \"source\": \"Printing – Turnover\",\n \"display\": \"Printing – Turnover\",\n \"keyword\": \"Printing___Turnover\",\n \"format\": \"number\"\n }, {\n \"source\": \"Marketing – Turnover\",\n \"display\": \"Marketing – Turnover\",\n \"keyword\": \"Marketing___Turnover\",\n \"format\": \"number\"\n }, {\n \"source\": \"Administration – Cost per FTE\",\n \"display\": \"Administration – Cost per FTE\",\n \"keyword\": \"Administration___Cost_per_FTE\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Publishing – Cost per FTE\",\n \"display\": \"Publishing – Cost per FTE\",\n \"keyword\": \"Publishing___Cost_per_FTE\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Printing – Cost per FTE\",\n \"display\": \"Printing – Cost per FTE\",\n \"keyword\": \"Printing___Cost_per_FTE\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Marketing – Cost per FTE\",\n \"display\": \"Marketing – Cost per FTE\",\n \"keyword\": \"Marketing___Cost_per_FTE\",\n \"format\": \"integer\"\n }]\n}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "datafiles",
    "Title": "sales_volume",
    "Description": "Total Sales Volume",
    "fileName": "assets/example_data/sales_volume.csv",
    "dataFileFormatJSON": "{\n \"separator\": \",\",\n \"columns\": [{\n \"source\": \"Date\",\n \"display\": \"Date\",\n \"keyword\": \"Date\",\n \"format\": \"date\",\n \"isKey\": true\n }, {\n \"source\": \"Children – US\",\n \"display\": \"Children – US\",\n \"keyword\": \"Children___US\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Children – Canada\",\n \"display\": \"Children – Canada\",\n \"keyword\": \"Children___Canada\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Children – Europe\",\n \"display\": \"Children – Europe\",\n \"keyword\": \"Children___Europe\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Children – Rest\",\n \"display\": \"Children – Rest\",\n \"keyword\": \"Children___Rest\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Sci-Fi – US\",\n \"display\": \"Sci-Fi – US\",\n \"keyword\": \"Sci_Fi___US\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Sci-Fi – Canada\",\n \"display\": \"Sci-Fi – Canada\",\n \"keyword\": \"Sci_Fi___Canada\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Sci-Fi – Europe\",\n \"display\": \"Sci-Fi – Europe\",\n \"keyword\": \"Sci_Fi___Europe\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Sci-Fi – Rest\",\n \"display\": \"Sci-Fi – Rest\",\n \"keyword\": \"Sci_Fi___Rest\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Education – US\",\n \"display\": \"Education – US\",\n \"keyword\": \"Education___US\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Education – Canada\",\n \"display\": \"Education – Canada\",\n \"keyword\": \"Education___Canada\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Education – Europe\",\n \"display\": \"Education – Europe\",\n \"keyword\": \"Education___Europe\",\n \"format\": \"integer\"\n }, {\n \"source\": \"Education – Rest\",\n \"display\": \"Education – Rest\",\n \"keyword\": \"Education___Rest\",\n \"format\": \"integer\"\n }]\n}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "datafiles",
    "Title": "top_authors_scifi",
    "Description": "Top Authors Sci-fi",
    "fileName": "assets/example_data/top_authors_scifi.csv",
    "dataFileFormatJSON": "{\n \"separator\": \",\",\n \"columns\": [{\n \"source\": \"Date\",\n \"display\": \"Date\",\n \"keyword\": \"Date\",\n \"format\": \"date\",\n \"isKey\": false\n }, {\n \"source\": \"Author\",\n \"display\": \"Author\",\n \"keyword\": \"Author\",\n \"format\": \"text\",\n \"isKey\": true\n }, {\n \"source\": \"column\",\n \"display\": \"column\",\n \"keyword\": \"column\",\n \"format\": \"text\",\n \"isKey\": true\n }, {\n \"source\": \"Total Sales\",\n \"display\": \"Total Sales\",\n \"keyword\": \"Total_Sales\",\n \"format\": \"number\"\n }]\n}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "datafiles",
    "Title": "customer_assets",
    "Description": "Customer Assets for Credit Check Forms",
    "fileName": "assets/example_data/customer_assets.csv",
    "dataFileFormatJSON": "{\n \"separator\": \";\",\n \"columns\": [{\n \"source\": \"ClientID\",\n \"display\": \"ClientID\",\n \"keyword\": \"ClientID\",\n \"format\": \"text\",\n \"isKey\": true\n }, {\n \"source\": \"checking\",\n \"display\": \"checking\",\n \"keyword\": \"checking\",\n \"format\": \"number\",\n \"false\": true\n }, {\n \"source\": \"savings\",\n \"display\": \"savings\",\n \"keyword\": \"savings\",\n \"format\": \"number\",\n \"isKey\": false\n }, {\n \"source\": \"otherAssets\",\n \"display\": \"otherAssets\",\n \"keyword\": \"otherAssets\",\n \"format\": \"number\"\n }, {\n \"source\": \"date\",\n \"display\": \"date\",\n \"keyword\": \"date\",\n \"format\": \"text\"\n }]\n}"
  },



  {
    "_action": "addEntry",
    "_endpoint": "notes",
    "Title": "",
    "noteText": "<p><u><b>Results from A/P Audit 2015:</b></u></p><p>This year's audit of the Accounts Payable function did not yield any new information. This means that no significant new issues have been discovered, but it also means that our longstanding findings regarding a lack of separation of duties and of payment and PO reconciliation are still unchanged. Management has not (yet) acted on our recommendations from the 2014 A/P audit. The 2015 audit report thus primarily repeats prior findings.<br/></p><p>See attachment for detailed audit report.<br/></p><p><i>[Attachments omitted in demo data.]</i><br/></p>",
    "Keywords": "tree/{{Tree:Accounts Payable}}/details,",
    "noteStatus": "Final"
  },
  {
    "_action": "addEntry",
    "_endpoint": "notes",
    "Title": "",
    "noteText": "<p>Explanation for 2014 spike of unpaid invoices:</p><p>A bug in the ERP system that was introduced earlier 2014 made various vendor invoices &#34;disappear&#34; in the payment stage. This lead to an accumulation of unpaid invoices. Once the issue was identified (after multiple notices by vendors had been received) the issue that made invoices disappear was fixed in the emergency change EC-2014-APMASTER-39822 on 2014-09-15. However, all missing invoices had to be reconciled and paid manually, which meant that overdue invoices were only resolved over the coming days.<br/></p>",
    "Keywords": "data/accounts_payable/2014-09-05/overdue,tree/{{Tree:Accounts Payable}}/details",
    "noteStatus": "Final"
  },
  {
    "_action": "addEntry",
    "_endpoint": "notes",
    "Title": "",
    "noteText": "<p>Note that the charges on this invoice are disputed by us and will thus not be paid until a resolution has been found.<br/></p>",
    "Keywords": "data/accounts_payable_top_10/2016-12-31/Verizon Inc./overdue,",
    "noteStatus": "Final"
  },
  {
    "_action": "addEntry",
    "_endpoint": "notes",
    "Title": "",
    "noteText": "<p>A system bug removed a large part of PO from the system reconciliation master file. This file is not used for production, thus no accounting systems were affected, but it is used by our Continuous Assurance analysis system. This is why the system shows a very large number of A/P without PO (29.5%) on 2013-10-31. In reality, the A/P without PO were not outside the usual range.<br/></p><p>No further investigation required.<br/></p>",
    "Keywords": "data/accounts_payable_without_p_o/2013-10-31/Paid_w_o_PO,",
    "noteStatus": "Final"
  },
  {
    "_action": "addEntry",
    "_endpoint": "notes",
    "Title": "",
    "noteText": "<p>A system outage in February 2013 meant that a couple of invoices had to be entered manually to get them paid on time, which lead to the larger than usual number of invoices that could not be traced automatically to their PO.<br/></p>",
    "Keywords": "data/accounts_payable_without_p_o/2013-02-28/Paid_w_o_PO,",
    "noteStatus": "Final"
  },
  {
    "_action": "addEntry",
    "_endpoint": "notes",
    "Title": "",
    "noteText": "<p><b>Employee Fraud in 2016: </b>The large number of invoices paid w/o PO in March 2016 relates to the employee fraud uncovered in early 2016. In fact, the fraud (ongoing over multiple months) was also responsible for the gradual uptick in invoices paid w/o PO in the preceding months. However, in March the employee increased his activities manifold, leading to the observed spike. Please see the attached investigation report for details.</p><p><i>[Attachments omitted in demo data.]</i><br/></p>",
    "Keywords": "data/accounts_payable_without_p_o/2016-03-31/Paid_w_o_PO,tree/{{Tree:Accounts Payable}}/details,",
    "noteStatus": "Final"
  },
  {
    "_action": "addEntry",
    "_endpoint": "notes",
    "Title": "",
    "noteText": "<p>I have investigated this amount further and it seems to relate to an actual consulting contract (see attachment). I have talked to some of the managers in this department and it seems that actual consulting services on a scope commensurate to the invoice have been provided.</p><p><i>[Attachments omitted from demo data.]</i><br/></p>",
    "Keywords": "data/accounts_payable_largest_payments/2016-02-09/42409 : Global Consulting Ltd. : Contract #9083896/8-8683-55786/Amount,",
    "noteStatus": "Final"
  },
  {
    "_action": "addEntry",
    "_endpoint": "notes",
    "Title": "",
    "noteText": "<p><b>Outstanding A/P for New World Promotion:</b> In my opinion it would really make sense to investigate why these invoices are paid so late. They promise a huge discount for early payments (far larger than our financing costs) so we really should take advantage of this!<br/></p>",
    "Keywords": "data/accounts_payable_top_10/2016-12-31/New World Promotion Ltd./due___10d,tree/{{Tree:Accounts Payable}}/details,",
    "noteStatus": "Final"
  },
  {
    "_action": "addEntry",
    "_endpoint": "notes",
    "Title": "",
    "noteText": "<p>IT infrastructure costs (especially web shop) for all of 2015. Very nice to see the over 50% reduction from last year's costs based on our suggestion to switch suppliers!<br/></p>",
    "Keywords": "data/accounts_payable_largest_payments/2015-09-16/42263 : OVH SA : LICENSES #245809/5-1865-35111/Amount,",
    "noteStatus": "Final"
  },
  {
    "_action": "addEntry",
    "_endpoint": "notes",
    "Title": "",
    "noteText": "<p><u><b>Results from A/P Audit 2016:</b></u></p><p>This year's audit was overshadowed by the serious case of fraud discovered in early April. As a result of the fraud case, various A/P processes have been completely re-engineered and the control framework surrounding them has been strengthened. We have thus postponed our A/P audit until later in the year to be able to validate the effectiveness of the new controls. Based on the 2016 audit, the new controls are a significant improvement over the old control framework, almost all of our longstanding issues have been addressed. We have obtained reasonable assurance that a similar fraud would not be possible in the new set-up.<br/></p><p>See attachment for detailed audit report.</p><p><i>[Attachments omitted in demo data.]</i><br/></p>",
    "Keywords": "tree/{{Tree:Accounts Payable}}/details,",
    "noteStatus": "Final"
  },
  {
    "_action": "addEntry",
    "_endpoint": "notes",
    "Title": "",
    "noteText": "<p><b><u>Summary for Q4/2016 Risk Session</u></b></p><p>While the fraud event in early 2016 has lead to a large upwards re-assessment of Accounts Payable, our AP audit completed in Q4 has satisfied us that (finally) most of our recommendations have been implemented and so far seem to be effective. We thus see a downward trend in operational risk in this area. However, before we reduce our risk assessment for AP we want to wait for the results of the 2017 audit, to ensure that the new procedures are also long-term sustainable.<br/></p>",
    "Keywords": "tree/{{Tree:Accounts Payable}}/summary,",
    "noteStatus": "Final"
  },
  {
    "_action": "addEntry",
    "_endpoint": "notes",
    "Title": "",
    "noteText": "<p><u><b>Summary for Q1/2017 Risk Session</b></u></p><p>Tbd - I am still working on this one</p><p>Most likely we won't have much new to tell here, right? After all, our 2017 audit is still a couple of months away...</p><p>Please chime in in this draft if you want me to add anything to our Q1 summary, thanks all.<br/></p>",
    "Keywords": "tree/{{Tree:Accounts Payable}}/summary,",
    "noteStatus": "Draft"
  },
  {
    "_action": "addEntry",
    "_endpoint": "notes",
    "Title": "",
    "noteText": "<p>Beginning in May 2014, the Finance Committee has decided to implement project &#34;The Gathering&#34; with the stated aim to improve the payment delays encountered with a growing number of our customers.</p><p>Going forward, Anthony &amp; Anthony will provide discounts for early payment, will more aggressively send out payment reminders and will hand over overdue payments to collection agencies earlier.<br/></p><p>During the project, it has also been discovered that for invoices that were followed by a new invoice with a later payment date, only this new payment date was used for calculating whether payment reminders should be sent out. This meant that for regular customers, sometimes no payment reminders where sent out for very old invoices. The reason was a computer bug that was introduced earlier in 2013, which explains why overdue AR were starting to grow after that time. It has been resolved with the new ERP release implemented on May 1st.<br/></p>",
    "Keywords": "data/accounts_receivable/2014-05-20/overdue__90d,tree/{{Tree:Accounts Receivable}}/details,",
    "noteStatus": "Final"
  },
  {
    "_action": "addEntry",
    "_endpoint": "notes",
    "Title": "",
    "noteText": "<p>Apparently this invoice is disputed by Independent Booksellers Inc; Finance is currently looking into it, we expect a report by next quarter.<br/></p>",
    "Keywords": "data/accounts_receivable_top_10/2016-12-31/Independent Booksellers Inc./overdue__30d,",
    "noteStatus": "Final"
  },
  {
    "_action": "addEntry",
    "_endpoint": "notes",
    "Title": "",
    "noteText": "<p>Disputed invoice. Discussions ongoing.<br/></p>",
    "Keywords": "data/accounts_receivable_overdue_portfolio/2016-12-31/Barnes and Noble Inc. - Note #347247/21000.00/Days_Overdue,",
    "noteStatus": "Final"
  },
  {
    "_action": "addEntry",
    "_endpoint": "notes",
    "Title": "",
    "noteText": "<p><u><b>Results from 2015 Accounts Receivable Audit</b></u></p><p>The results from &#34;The Gathering&#34; have been quite impressive: Overdue AR could be reduced significantly even though overall business volume has increased. A large part of this is probably due to the bug fix introduced on May 1st, but also especially the early payment discounts seem to have had a large effect (a precise cost-benefit analysis of the change has not been completed by the time of our audit, one of the recommendations in our detailed report). Overall, we see a decreasing risk environment in this function.</p><p>See attached audit report for details.</p><p><i>[Attachments omitted from demo data.]</i><br/></p>",
    "Keywords": "tree/{{Tree:Accounts Receivable}}/details,",
    "noteStatus": "Final"
  },
  {
    "_action": "addEntry",
    "_endpoint": "notes",
    "Title": "",
    "noteText": "<p><u><b>Summary for Q4/2016 Risk Session</b></u></p><p>Based on the overall risk environment, Accounts Receivable was not scheduled for a 2016 audit, but will be audited again in 2017. During the quarter, we have not received any new information that would require a change in our risk assessment. Also key figures (overall AR volume and volume of overdue AR) are stable compared to overall business volume. There are currently two disputed invoices by vendors. Our Finance team is on the case, but none of them are material in any way.<br/></p>",
    "Keywords": "tree/{{Tree:Accounts Receivable}}/summary,",
    "noteStatus": "Final"
  },
  {
    "_action": "addEntry",
    "_endpoint": "notes",
    "Title": "",
    "noteText": "<p><u><b>Summary for Q1/2017 Risk Session</b></u></p><p>tbd - at the moment I do not see anything new in Q1, the audit is planned for Q2, so &#34;business as usual&#34;?</p><p><br/></p>",
    "Keywords": "tree/{{Tree:Accounts Receivable}}/summary,",
    "noteStatus": "Draft"
  },
  {
    "_action": "addEntry",
    "_endpoint": "notes",
    "Title": "",
    "noteText": "<p>We have identified an alarming and currently unexplained increase in shrinkage for our Petersburg, VA warehouse. At the next quarterly meeting, we will push for an audit intervention there in the coming weeks.<br/></p>",
    "Keywords": "data/shrinkage/2015-05-31/Petersburg__VA,tree/{{Tree:Warehousing and Distribution}}/details,",
    "noteStatus": "Draft"
  },
  {
    "_action": "addEntry",
    "_endpoint": "notes",
    "Title": "",
    "noteText": "<p>Our CA work has identified this alarming increase of shrinkage in our Petersburg warehouse in mid-2015. Unfortunately, actual investigation was delayed by unforeseen vacancies and other urgent work, which meant that this went on much longer than should have been possible.</p><p>This is why it took until the end of 2015 to identify the source of the problem - a former night watchman had duplicate keys made and used those to steal from our warehouse throughout the period, starting in early 2014.</p><p>We have changed the keys and pressed charges and now shrinkage in Petersburg is back to normal.<br/></p><p><br/></p>",
    "Keywords": "data/shrinkage/2015-12-31/Petersburg__VA,tree/{{Tree:Warehousing and Distribution}}/details,",
    "noteStatus": "Draft"
  },




  {
    "_action": "addEntry",
    "_endpoint": "risks",
    "Title": "Purchase-to-Pay: Employee Fraud<br>",
    "Description": "Risk of asset theft by employees through e.g. fictious invoices paid to themselves, mis-routing of deliveries, conspiring with vendors etc.<br>",
    "Category": "tree/{{Tree:Finance and Accounting}}",
    "xAxis": 46.44,
    "yAxis": 58,
    "riskStatus": "Final",
    "initialID": 1,
    "Valid_from": "2014-01-01T17:12:12Z",
    "Valid_to": "2016-05-01T17:12:12Z",
    "_storeID": "Risks:PtP Employee Fraud"
  },
  {
    "_action": "addEntry",
    "_endpoint": "risks",
    "Title": "<b>Purchase-to-Pay:</b> Vendor Fraud<br> ",
    "Description": "Risk of asset theft by vendors through e.g. fictious invoices for services not rendered, double invoicing of the same products / services, overcharging.<br>",
    "Category": "tree/{{Tree:Finance and Accounting}}",
    "xAxis": 31.33,
    "yAxis": 40.22,
    "riskStatus": "Final",
    "initialID": 2,
    "Valid_from": "2014-01-01T17:12:12Z",
    "Valid_to": "2016-05-01T17:12:12Z",
    "_storeID": "Risks:PtP Vendor Fraud"
  },
  {
    "_action": "addEntry",
    "_endpoint": "risks",
    "Title": "Sales Cycle: Large Payment Shortfalls<br>",
    "Description": "Default of large customers or other reasons why customers might not pay their invoices.<br>",
    "Category": "tree/{{Tree:Finance and Accounting}}",
    "xAxis": 66,
    "yAxis": 46.44,
    "riskStatus": "Final",
    "initialID": 3,
    "Valid_from": "2014-01-01T17:12:12Z",
    "Valid_to": "2016-05-01T17:12:12Z",
    "_storeID": "Risks:Sales Cycle Large Payment Shortfalls"
  },
  {
    "_action": "addEntry",
    "_endpoint": "risks",
    "Title": "Accounting: Employee Fraud (Fradulent Financial Reporting)<br>",
    "Description": "Risk of manipulation of financial statements by accounting employees. Mitigated by accounting ICS.<br>",
    "Category": "tree/{{Tree:Finance and Accounting}}",
    "xAxis": 12.66,
    "yAxis": 82.44,
    "riskStatus": "Final",
    "initialID": 4,
    "Valid_from": "2014-01-01T17:12:12Z",
    "Valid_to": "2016-05-01T17:12:12Z",
    "_storeID": "Risks:Accounting Employee Fraud"
  },
  {
    "_action": "addEntry",
    "_endpoint": "risks",
    "Title": "Accounting: Other Reporting / Disclosure Failures<br>",
    "Description": "Risk of other material failures in the company's financial reporting and other disclosures, through mistakes or system failures instead of fraud.<br>",
    "Category": "tree/{{Tree:Finance and Accounting}}",
    "xAxis": 52.67,
    "yAxis": 25.56,
    "riskStatus": "Final",
    "initialID": 5,
    "Valid_from": "2014-01-01T17:12:12Z",
    "Valid_to": "2016-05-01T17:12:12Z",
    "_storeID": "Risks:Accounting Other Failures"
  },




  {
    "_action": "addEntry",
    "_endpoint": "risks",
    "Title": "Purchase-to-Pay: Employee Fraud<br>",
    "Description": "Risk of asset theft by employees through e.g. fictious invoices paid to themselves, mis-routing of deliveries, conspiring with vendors etc.<br>=&gt; Risk assessment has been increased after employee fraud in early 2016. We will re-assess and maybe lower our assessment after the 2017 AP audit evaluates the sustainable effect of the implemented mitigation measures.<br>",
    "Category": "tree/{{Tree:Finance and Accounting}}",
    "xAxis": 63.33,
    "yAxis": 69.11,
    "riskStatus": "Final",
    "initialID": 1,
    "Valid_from": "2016-05-01T17:12:13Z",
    "Valid_to": "3000-01-31T17:00:00Z"
  },
  {
    "_action": "addEntry",
    "_endpoint": "risks",
    "Title": "<b>Purchase-to-Pay:</b> Vendor Fraud<br> ",
    "Description": "Risk of asset theft by vendors through e.g. fictious invoices for services not rendered, double invoicing of the same products / services, overcharging.<br>",
    "Category": "tree/{{Tree:Finance and Accounting}}",
    "xAxis": 31.33,
    "yAxis": 40.22,
    "riskStatus": "Final",
    "initialID": 2,
    "Valid_from": "2016-05-01T17:12:13Z",
    "Valid_to": "3000-01-31T17:00:00Z"
  },
  {
    "_action": "addEntry",
    "_endpoint": "risks",
    "Title": "Sales Cycle: Large Payment Shortfalls<br>",
    "Description": "Default of large customers or other reasons why customers might not pay their invoices.<br>",
    "Category": "tree/{{Tree:Finance and Accounting}}",
    "xAxis": 66,
    "yAxis": 46.44,
    "riskStatus": "Final",
    "initialID": 3,
    "Valid_from": "2016-05-01T17:12:13Z",
    "Valid_to": "3000-01-31T17:00:00Z"
  },
  {
    "_action": "addEntry",
    "_endpoint": "risks",
    "Title": "Accounting: Employee Fraud (Fradulent Financial Reporting)<br>",
    "Description": "Risk of manipulation of financial statements by accounting employees. Mitigated by accounting ICS.<br>",
    "Category": "tree/{{Tree:Finance and Accounting}}",
    "xAxis": 12.66,
    "yAxis": 82.44,
    "riskStatus": "Final",
    "initialID": 4,
    "Valid_from": "2016-05-01T17:12:13Z",
    "Valid_to": "3000-01-31T17:00:00Z"
  },
  {
    "_action": "addEntry",
    "_endpoint": "risks",
    "Title": "Accounting: Other Reporting / Disclosure Failures<br>",
    "Description": "Risk of other material failures in the company's financial reporting and other disclosures, through mistakes or system failures instead of fraud.<br>",
    "Category": "tree/{{Tree:Finance and Accounting}}",
    "xAxis": 52.67,
    "yAxis": 25.56,
    "riskStatus": "Final",
    "initialID": 5,
    "Valid_from": "2016-05-01T17:12:13Z",
    "Valid_to": "3000-01-31T17:00:00Z"
  },

  {
    "_action": "addEntry",
    "_endpoint": "object_explorer_objects",
    "Title": "Processing of Incomplete Purchase Orders [GRC-ID 39383]",
    "Description": "Processing of incomplete purchase orders can lead to erroneous or unapproved orders.",
    "Type": "GRC Item",
    "Link": "https://urlecho.appspot.com/echo?Content-Type=text%2Fhtml&body=%3Cp%3E%5BPlaceholder%20-%20would%20link%20to%20your%20GRC%20platform%5D%3C%2Fp%3E%0A%0A%3Cp%3E%3Cb%3EProcessing%20of%20Incomplete%20Purchase%20Orders%20[GRC-ID%2039383]%3C%2Fb%3E%3C%2Fp%3E",
    "additionalColumnsJSON": "{ \"grc_id\": \"39383\", \"authority\": \"Internal Controls\", \"risk_level\": \"B3\" }",
    "_storeID": "Objects:GRC39383"
  },
  {
    "_action": "addEntry",
    "_endpoint": "object_explorer_objects",
    "Title": "Loss of Purchase Order [GRC-ID 31711]",
    "Description": "Loss of purchase orders can lead to a lack of materials, leading to a production shutdown.",
    "Type": "GRC Item",
    "Link": "https://urlecho.appspot.com/echo?Content-Type=text%2Fhtml&body=%3Cp%3E%5BPlaceholder%20-%20would%20link%20to%20your%20GRC%20platform%5D%3C%2Fp%3E%0A%0A%3Cp%3E%3Cb%3ELoss%20of%20Purchase%20Order%20[GRC-ID%2031711]%3C%2Fb%3E%3C%2Fp%3E",
    "additionalColumnsJSON": "{ \"grc_id\": \"31711\", \"authority\": \"Internal Controls\", \"risk_level\": \"C2\" }",
    "_storeID": "Objects:GRC31711"
  },
  {
    "_action": "addEntry",
    "_endpoint": "object_explorer_objects",
    "Title": "Payment of Fraudulent Invoices [GRC-ID 37484]",
    "Description": "Payment of fraudulent invoices to fraudulent accounts can lead to financial losses.",
    "Type": "GRC Item",
    "Link": "https://urlecho.appspot.com/echo?Content-Type=text%2Fhtml&body=%3Cp%3E%5BPlaceholder%20-%20would%20link%20to%20your%20GRC%20platform%5D%3C%2Fp%3E%0A%0A%3Cp%3E%3Cb%3EPayment%20of%20Fraudulent%20Invoices%20[GRC-ID%2037484]%3C%2Fb%3E%3C%2Fp%3E",
    "additionalColumnsJSON": "{ \"grc_id\": \"37484\", \"authority\": \"Internal Controls\", \"risk_level\": \"A2\" }",
    "_storeID": "Objects:GRC37484"
  },
  {
    "_action": "addEntry",
    "_endpoint": "object_explorer_objects",
    "Title": "Purchase-to-Pay Audit 2017 [Audit Report 8373]",
    "Description": "The PtP Audit 2017 did not lead to any significant findings.",
    "Type": "Audit Report",
    "Link": "",
    "additionalColumnsJSON": "{ \"grc_id\": \"8373\", \"authority\": \"Audit\", \"importance\": \"C2\" }",
    "_storeID": "Objects:AR8373"
  },
  {
    "_action": "addEntry",
    "_endpoint": "object_explorer_objects",
    "Title": "Purchase Orders Audit 2014 [Audit Report 3394]",
    "Description": "Significant issues around purchase order handling where identified.",
    "Type": "Audit Report",
    "Link": "",
    "additionalColumnsJSON": "{ \"grc_id\": \"3394\", \"authority\": \"Audit\", \"importance\": \"A2\" }",
    "_storeID": "Objects:AR3394"
  },
  {
    "_action": "addEntry",
    "_endpoint": "object_explorer_mapping",
    "MappingKey": "purchaseToPay",
    "Element": "Purchase Order",
    "ObjectId": "{{Objects:GRC39383}}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "object_explorer_mapping",
    "MappingKey": "purchaseToPay",
    "Element": "Purchase Order",
    "ObjectId": "{{Objects:GRC31711}}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "object_explorer_mapping",
    "MappingKey": "purchaseToPay",
    "Element": "Purchase Order",
    "ObjectId": "{{Objects:AR3394}}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "object_explorer_mapping",
    "MappingKey": "purchaseToPay",
    "Element": "Vendor Invoice",
    "ObjectId": "{{Objects:GRC37484}}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "object_explorer_mapping",
    "MappingKey": "purchaseToPay",
    "Element": "Planning",
    "ObjectId": "{{Objects:AR8373}}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "object_explorer_mapping",
    "MappingKey": "purchaseToPay",
    "Element": "Purchase Requisition",
    "ObjectId": "{{Objects:AR8373}}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "object_explorer_mapping",
    "MappingKey": "purchaseToPay",
    "Element": "Request for Quotation",
    "ObjectId": "{{Objects:AR8373}}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "object_explorer_mapping",
    "MappingKey": "purchaseToPay",
    "Element": "Purchase Order",
    "ObjectId": "{{Objects:AR8373}}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "object_explorer_mapping",
    "MappingKey": "purchaseToPay",
    "Element": "Goods Receipt",
    "ObjectId": "{{Objects:AR8373}}"
  },
  {
    "_action": "addEntry",
    "_endpoint": "object_explorer_mapping",
    "MappingKey": "purchaseToPay",
    "Element": "Vendor Invoice",
    "ObjectId": "{{Objects:AR8373}}"
  },






  {
    "_action": "addFile",
    "_endpoint": "task_data",
    "_filename": "spending_2018_1mio.csv",
    "_sourceUrl": "assets/example_data/spending_2018_1mio.csv",
    "_overwrite": true,
    "Title": "Spending FY2018 >1 Mio USD",
    "TaskDescription": "This tasks shows all booked expenditures over 1 Mio USD for the given time period. Please evaluate for anomalies.",
    "DataDate": "2018-03-31T17:00:00Z",
    "workflowStatus": "Open",
    "workflowChanges": "[{\"status\":\"New\", \"change_at\": \"2018-03-31T18:27:18.000Z\", \"change_by\": 1 }, \n { \"status\": \"Open\", \"change_at\": \"2018-04-06T14:27:18.000Z\", \"change_by\": 1 } ]",
    "dataSpecification": "{\n  \"showColumns\": [\"Issue Date\",\"Spending Category\",\"Contract ID\",\"Payee Name\",\"Check Amount\"],\n  \"dataFileFormat\": {\n     \"separator\": \",\",\n     \"columns\": [\n         { \"source\": \"Payment ID\", \"display\": \"Payment ID\", \"format\": \"text\", \"isKey\": true },\n         { \"source\": \"Agency\", \"display\": \"Agency\", \"format\": \"text\" },\n         { \"source\": \"Associated Prime Vendor\", \"display\": \"Associated Prime Vendor\", \"format\": \"text\" },\n         { \"source\": \"Vendor\", \"display\": \"Vendor\", \"format\": \"text\" },\n         { \"source\": \"Capital Project\", \"display\": \"Capital Project\", \"format\": \"text\" },\n         { \"source\": \"Check Amount\", \"display\": \"Check Amount\", \"format\": \"number\" },\n         { \"source\": \"Contract ID\", \"display\": \"Contract ID\", \"format\": \"text\" },\n         { \"source\": \"Contract Purpose\", \"display\": \"Contract Purpose\", \"format\": \"text\" },\n         { \"source\": \"Department\", \"display\": \"Department\", \"format\": \"text\" },\n         { \"source\": \"Document ID\", \"display\": \"Document ID\", \"format\": \"text\" },\n         { \"source\": \"Expense Category\", \"display\": \"Expense Category\", \"format\": \"text\" },\n         { \"source\": \"Fiscal year\", \"display\": \"Fiscal year\", \"format\": \"text\" },\n         { \"source\": \"Industry\", \"display\": \"Industry\", \"format\": \"text\" },\n         { \"source\": \"Issue Date\", \"display\": \"Issue Date\", \"format\": \"date\" },\n         { \"source\": \"M/WBE Category\", \"display\": \"MWBE Category\", \"format\": \"text\" },\n         { \"source\": \"Payee Name\", \"display\": \"Payee Name\", \"format\": \"text\" },\n         { \"source\": \"Spending Category\", \"display\": \"Spending Category\", \"format\": \"text\" },\n         { \"source\": \"Sub Contract Reference ID\", \"display\": \"Sub Contract Reference ID\", \"format\": \"text\" },\n         { \"source\": \"Sub Vendor\", \"display\": \"Sub Vendor\", \"format\": \"text\" }\n      ]\n   },\n   \"keywords\": [\n     \"OCA/Spending/Payment/{{Payment ID}}\",\n     \"OCA/Spending/Contract/{{Contract ID}}\",\n     \"OCA/Vendor/{{Payee Name}}\"\n   ],\n   \"showAuditForms\": true,\n   \"newAuditForms\": [ \"note\", \"spending_check\" ],\n   \"excelEnabled\": \"true\",\n   \"resultsColumn\": \"Hit or not\",\n   \"resultsSelection\": [\"falsepos\",\"hit\"],\n   \"machineEnabledResult\": \"hit\",\n   \"resultsSelectionIterate\": true,\n   \"mapResults\": [\n     { \"auditForm\": \"spending_check\", \"mapFrom\": \"gold\", \"mapTo\": \"hit\" },\n     { \"auditForm\": \"note\", \"mapFrom\": \"green\", \"mapTo\": \"falsepos\" }\n   ],   \"machineEnabledColumns\": [\n     { \"column\": \"Agency\", \"type\": \"text\" },\n     { \"column\": \"Associated Prime Vendor\", \"type\": \"text\" },\n     { \"column\": \"Capital Project\", \"type\": \"text\" },\n     { \"column\": \"Check Amount\", \"type\": \"number\" },\n     { \"column\": \"Contract ID\", \"type\": \"text\" },\n     { \"column\": \"Contract Purpose\", \"type\": \"text\" },\n     { \"column\": \"Department\", \"type\": \"text\" },\n     { \"column\": \"Document ID\", \"type\": \"text\" },\n     { \"column\": \"Expense Category\", \"type\": \"text\" },\n     { \"column\": \"Fiscal year\", \"type\": \"text\" },\n     { \"column\": \"Industry\", \"type\": \"text\" },\n     { \"column\": \"Issue Date\", \"type\": \"date\" },\n     { \"column\": \"MWBE Category\", \"type\": \"text\" },\n     { \"column\": \"Payee Name\", \"type\": \"text\" },\n     { \"column\": \"Spending Category\", \"type\": \"text\" },\n     { \"column\": \"Sub Contract Reference ID\", \"type\": \"text\" },\n     { \"column\": \"Sub Vendor\", \"type\": \"text\" }\n  ]\n}\n"
  },
  {
    "_action": "addFile",
    "_endpoint": "task_data",
    "_filename": "credit_card_clients_risk.csv",
    "_sourceUrl": "assets/example_data/credit_card_clients_risk.csv",
    "_overwrite": true,
    "Title": "Credit Card Overdue Payments",
    "TaskDescription": "This task shows credit card clients and their payment history. Please identify clients at risk of default.",
    "DataDate": "2018-07-31T17:00:00Z",
    "workflowStatus": "Open",
    "workflowChanges": "[{\"status\":\"New\", \"change_at\": \"2018-03-31T18:27:18.000Z\", \"change_by\": 1 }, \n { \"status\": \"Open\", \"change_at\": \"2018-04-06T14:27:18.000Z\", \"change_by\": 1 } ]",
    "dataSpecification": "{\n  \"showColumns\": [ \"ID\", \"LIMIT_BAL\", \"SEX\", \"EDUCATION\", \"MARRIAGE\", \"AGE\", \"PAY_0\", \"PAY_2\", \"PAY_3\", \"PAY_4\", \"PAY_5\", \"PAY_6\", \"BILL_AMT1\", \"BILL_AMT2\", \"BILL_AMT3\", \"BILL_AMT4\", \"BILL_AMT5\", \"BILL_AMT6\", \"PAY_AMT1\", \"PAY_AMT2\", \"PAY_AMT3\", \"PAY_AMT4\", \"PAY_AMT5\", \"PAY_AMT6\", \"default next month\" ],\n  \"dataFileFormat\": {\n     \"separator\": \";\",\n     \"columns\": [\n          { \"source\": \"ID\", \"display\": \"ID\", \"format\": \"text\", \"isKey\": true }, \n          { \"source\": \"LIMIT_BAL\", \"display\": \"LIMIT_BAL\", \"format\": \"integer\", \"isKey\": false }, \n          { \"source\": \"SEX\", \"display\": \"SEX\", \"format\": \"text\", \"isKey\": false }, \n          { \"source\": \"EDUCATION\", \"display\": \"EDUCATION\", \"format\": \"text\", \"isKey\": false }, \n          { \"source\": \"MARRIAGE\", \"display\": \"MARRIAGE\", \"format\": \"text\", \"isKey\": false }, \n          { \"source\": \"AGE\", \"display\": \"AGE\", \"format\": \"integer\", \"isKey\": false }, \n          { \"source\": \"PAY_0\", \"display\": \"PAY_0\", \"format\": \"integer\", \"isKey\": false }, \n          { \"source\": \"PAY_2\", \"display\": \"PAY_2\", \"format\": \"integer\", \"isKey\": false }, \n          { \"source\": \"PAY_3\", \"display\": \"PAY_3\", \"format\": \"integer\", \"isKey\": false }, \n          { \"source\": \"PAY_4\", \"display\": \"PAY_4\", \"format\": \"integer\", \"isKey\": false }, \n          { \"source\": \"PAY_5\", \"display\": \"PAY_5\", \"format\": \"integer\", \"isKey\": false }, \n          { \"source\": \"PAY_6\", \"display\": \"PAY_6\", \"format\": \"integer\", \"isKey\": false }, \n          { \"source\": \"BILL_AMT1\", \"display\": \"BILL_AMT1\", \"format\": \"integer\", \"isKey\": false }, \n          { \"source\": \"BILL_AMT2\", \"display\": \"BILL_AMT2\", \"format\": \"integer\", \"isKey\": false }, \n          { \"source\": \"BILL_AMT3\", \"display\": \"BILL_AMT3\", \"format\": \"integer\", \"isKey\": false }, \n          { \"source\": \"BILL_AMT4\", \"display\": \"BILL_AMT4\", \"format\": \"integer\", \"isKey\": false }, \n          { \"source\": \"BILL_AMT5\", \"display\": \"BILL_AMT5\", \"format\": \"integer\", \"isKey\": false }, \n          { \"source\": \"BILL_AMT6\", \"display\": \"BILL_AMT6\", \"format\": \"integer\", \"isKey\": false }, \n          { \"source\": \"PAY_AMT1\", \"display\": \"PAY_AMT1\", \"format\": \"integer\", \"isKey\": false }, \n          { \"source\": \"PAY_AMT2\", \"display\": \"PAY_AMT2\", \"format\": \"integer\", \"isKey\": false }, \n          { \"source\": \"PAY_AMT3\", \"display\": \"PAY_AMT3\", \"format\": \"integer\", \"isKey\": false }, \n          { \"source\": \"PAY_AMT4\", \"display\": \"PAY_AMT4\", \"format\": \"integer\", \"isKey\": false }, \n          { \"source\": \"PAY_AMT5\", \"display\": \"PAY_AMT5\", \"format\": \"integer\", \"isKey\": false }, \n          { \"source\": \"PAY_AMT6\", \"display\": \"PAY_AMT6\", \"format\": \"integer\", \"isKey\": false }, \n          { \"source\": \"default next month\", \"display\": \"default next month\", \"format\": \"integer\", \"isKey\": false }\n      ]\n   },\n   \"keywords\": [\n     \"OCA/CreditClients/{{ID}}\"\n   ],\n   \"showAuditForms\": true,\n   \"newAuditForms\": [ \"note\", \"credit_check\" ],\n   \"excelEnabled\": \"true\",\n   \"resultsColumn\": \"Risk Grading\",\n   \"resultsSelection\": [\"Default Risk\", \"Low Risk\", \"Analyze Further\" ],\n   \"machineEnabledResult\": \"Default Risk\",\n   \"resultsSelectionIterate\": true,\n   \"mapResults\": [\n     { \"auditForm\": \"credit_check\", \"mapFrom\": \"default_risk\", \"mapTo\": \"Default Risk\" },\n     { \"auditForm\": \"credit_check\", \"mapFrom\": \"low_risk\", \"mapTo\": \"Low Risk\" },\n     { \"auditForm\": \"credit_check\", \"mapFrom\": \"*\", \"mapTo\": \"Analyze Further\" }\n   ],\n   \"machineEnabledColumns\": [\n     { \"column\": \"LIMIT_BAL\", \"type\": \"number\" },\n     { \"column\": \"SEX\", \"type\": \"text\" },\n     { \"column\": \"EDUCATION\", \"type\": \"text\" },\n     { \"column\": \"MARRIAGE\", \"type\": \"text\" },\n     { \"column\": \"AGE\", \"type\": \"number\" },\n     { \"column\": \"PAY_0\", \"type\": \"number\" },\n     { \"column\": \"PAY_2\", \"type\": \"number\" },\n     { \"column\": \"PAY_3\", \"type\": \"number\" },\n     { \"column\": \"PAY_4\", \"type\": \"number\" },\n     { \"column\": \"PAY_5\", \"type\": \"number\" },\n     { \"column\": \"PAY_6\", \"type\": \"number\" },\n     { \"column\": \"BILL_AMT1\", \"type\": \"number\" },\n     { \"column\": \"BILL_AMT2\", \"type\": \"number\" },\n     { \"column\": \"BILL_AMT3\", \"type\": \"number\" },\n     { \"column\": \"BILL_AMT4\", \"type\": \"number\" },\n     { \"column\": \"BILL_AMT5\", \"type\": \"number\" },\n     { \"column\": \"BILL_AMT6\", \"type\": \"number\" },\n     { \"column\": \"PAY_AMT1\", \"type\": \"number\" },\n     { \"column\": \"PAY_AMT2\", \"type\": \"number\" },\n     { \"column\": \"PAY_AMT3\", \"type\": \"number\" },\n     { \"column\": \"PAY_AMT4\", \"type\": \"number\" },\n     { \"column\": \"PAY_AMT5\", \"type\": \"number\" },\n     { \"column\": \"PAY_AMT6\", \"type\": \"number\" }\n  ]\n}\n",
    "_storeID": "Tasks:credit_card_clients_risk"
  },






  {
    "_action": "addFile",
    "_endpoint": "audit_forms",
    "_filename": "auditForm_note.html",
    "_sourceUrl": "assets/example_data/auditForm_note.html",
    "_overwrite": true,
    "Title": "Audit Note",
    "shortKey": "note",
    "tabs": "",
    "additionalSources": "",
    "resultExpression": "(resultsData.thisNote !== '' ? 'green' : 'red')",
    "newFormIcon": "assets/example_data/auditForm_note.png",
    "iconMap": "{\n  \"green\": \"assets/example_data/auditForm_note_green.png\",\n  \"red\": \"assets/example_data/auditForm_note_red.png\"\n}\n"
  },
  {
    "_action": "addFile",
    "_endpoint": "audit_forms",
    "_filename": "auditForm_spending_check.html",
    "_sourceUrl": "assets/example_data/auditForm_spending_check.html",
    "_overwrite": true,
    "Title": "Audit Form: Spending Check",
    "shortKey": "spending_check",
    "tabs": "",
    "additionalSources": "",
    "resultExpression": "'gold'",
    "newFormIcon": "assets/example_data/auditForm_spending_check.png",
    "iconMap": "{\n  \"gold\": \"assets/example_data/auditForm_spending_check.png\"\n}"
  },
  {
    "_action": "addFile",
    "_endpoint": "audit_forms",
    "_filename": "auditForm_credit_check.html",
    "_sourceUrl": "assets/example_data/auditForm_credit_check.html",
    "_overwrite": true,
    "Title": "Audit Form: Credit Check",
    "shortKey": "credit_check",
    "tabs": "[\n   { \"tabId\": \"master\", \"tabText\": \"Client Master\", \"tabTitle\": \"Client Master Data\", \"tabColor\": \"#ffffff\", \"tabSelectedColor\": \"grey\" },\n   { \"tabId\": \"payments\", \"tabText\": \"Payments\", \"tabTitle\": \"Payment History\", \"tabColor\": \"#ffffff\", \"tabSelectedColor\": \"grey\" }\n]",
    "additionalSources": "[ { \"datafile\": \"customer_assets\", \"keyMap\": [ { \"from\": \"ClientID\", \"to\": \"ID\" } ], \"dataMap\": [ { \"from\": \"checking\", \"to\": \"checking\" }, { \"from\": \"savings\", \"to\": \"savings\" }, { \"from\": \"otherAssets\", \"to\": \"otherAssets\" }, { \"from\": \"date\", \"to\": \"loadDate\" } ] }  ]",
    "resultExpression": "resultsData.risk_evaluation",
    "newFormIcon": "assets/example_data/auditForm_credit_check.png",
    "iconMap": "{\n  \"default_risk\": \"assets/example_data/auditForm_credit_check_default_risk.png\",\n  \"low_risk\": \"assets/example_data/auditForm_credit_check_low_risk.png\",\n  \"\": \"assets/example_data/auditForm_credit_check.png\",\n  \"investigate_further\": \"assets/example_data/auditForm_credit_check.png\",\n  \"pending_response\": \"assets/example_data/auditForm_credit_check.png\"\n}"
  },


  {
    "_action": "addEntry",
    "_endpoint": "configuration",
    "Title": "Default Language",
    "Value": "en"
  },
  {
    "_action": "addEntry",
    "_endpoint": "configuration",
    "Title": "Available Languages",
    "Value": "{ \"en\": \"English (UK)\", \"de\": \"Deutsch (Schweiz)\" }"
  },
  {
    "_action": "addEntry",
    "_endpoint": "configuration",
    "Title": "Workflow Mode",
    "Value": "hard"
  },
  {
    "_action": "addEntry",
    "_endpoint": "configuration",
    "Title": "Workflow Exceptions",
    "Value": "{ \"tasks/{{Tasks:credit_card_clients_risk}}\": \"soft\" }"
  }
]

