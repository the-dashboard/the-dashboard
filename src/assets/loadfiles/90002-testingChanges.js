[
  {
    "_action": "removeField",
    "Title": "Dashboard - Risks",
    "fieldTitle": "riskStatus"
  },
  {
    "_action": "changeField",
    "Title": "Dashboard - Notes",
    "fieldTitle": "noteStatus",
    "field": {
      "Title": "noteStatus",
      "StaticName": "noteStatus",
      "FieldTypeKind": 6,
      "Choices": ["Final", "Draft", "Reviewed", "New", "Fancy"]
    }
  },
  {
    "_action": "removeEntry",
    "_listTitle": "Dashboard - Views",
    "searchQuery": "elementType eq 'notes'"
  },
  {
    "_action": "changeEntry",
    "_listTitle": "Dashboard - Views",
    "searchQuery": "Title eq 'Risk Matrix'",
    "entry": {
      "Title": "Fancy New Risk Matrix",
      "orderAsc": 999
    }
  }
]
