[
  {
    "_action": "deleteLibrary",
    "Title": "Dashboard - Task Archive"
  },
  {
    "_action": "deleteLibrary",
    "Title": "Dashboard - Task Data"
  },
  {
    "_action": "deleteLibrary",
    "Title": "Dashboard - Task Results"
  },
  {
    "_action": "deleteLibrary",
    "Title": "Dashboard - Task Archive Results"
  },
  {
    "_action": "deleteLibrary",
    "Title": "Dashboard - Audit Forms"
  },
  {
    "_action": "deleteList",
    "Title": "Dashboard - Object Explorer Mapping"
  },
  {
    "_action": "deleteList",
    "Title": "Dashboard - Object Explorer Objects"
  },
  {
    "_action": "deleteList",
    "Title": "Dashboard - Views"
  },
  {
    "_action": "deleteList",
    "Title": "Dashboard - User Settings"
  },
  {
    "_action": "deleteList",
    "Title": "Dashboard - Risks"
  },
  {
    "_action": "deleteList",
    "Title": "Dashboard - Notes"
  },
  {
    "_action": "deleteList",
    "Title": "Dashboard - Tree"
  },
  {
    "_action": "deleteList",
    "Title": "Dashboard - Keywords"
  },
  {
    "_action": "deleteList",
    "Title": "Dashboard - Datafiles"
  },
  {
    "_action": "deleteList",
    "Title": "Dashboard - Configuration"
  },
  {
    "_action": "deleteList",
    "Title": "Dashboard - Impact Flags"
  },
  {
    "_action": "deleteList",
    "Title": "Dashboard - Audit Forms Data"
  }
]
