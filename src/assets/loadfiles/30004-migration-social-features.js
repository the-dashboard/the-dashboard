[
  {
    "_action": "deleteList",
    "Title": "Dashboard - User Alerts"
  },
  {
    "_action": "deleteList",
    "Title": "Dashboard - User Alert Triggers"
  },
  {
    "_action": "createList",
    "Title": "Dashboard - Interactions",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "interactionTime",
        "StaticName": "interactionTime",
        "FieldTypeKind": 4,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "DefaultValue": "[today]",
        "DisplayFormat": 1,
        "Indexed": "true"
      },
      {
        "Title": "user",
        "StaticName": "user",
        "FieldTypeKind": 20,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "targetType",
        "StaticName": "targetType",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "targetId",
        "StaticName": "targetId",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "interactionType",
        "StaticName": "interactionType",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "content",
        "StaticName": "content",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      }
    ]
  },


{
    "_action": "createList",
    "Title": "Dashboard - Subscriptions",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "user",
        "StaticName": "user",
        "FieldTypeKind": 20,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "type",
        "StaticName": "type",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "target",
        "StaticName": "target",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "targetDescription",
        "StaticName": "targetDescription",
        "FieldTypeKind": 2,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "false"
      },
      {
        "Title": "actionType",
        "StaticName": "actionType",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "false"
      }
    ]
  },

  {
    "_action": "createList",
    "Title": "Dashboard - Newsfeed",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "user",
        "StaticName": "user",
        "FieldTypeKind": 20,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "newsTime",
        "StaticName": "newsTime",
        "FieldTypeKind": 4,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "DefaultValue": "[today]",
        "DisplayFormat": 1,
        "Indexed": "true"
      },
      {
        "Title": "sourceType",
        "StaticName": "sourceType",
        "FieldTypeKind": 2,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "sourceId",
        "StaticName": "sourceId",
        "FieldTypeKind": 2,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "subElement",
        "StaticName": "subElement",
        "FieldTypeKind": 2,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "sourceUrl",
        "StaticName": "sourceUrl",
        "FieldTypeKind": 2,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "false"
      },
      {
        "Title": "actionType",
        "StaticName": "actionType",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "false"
      },
      {
        "Title": "contentVariables",
        "StaticName": "contentVariables",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      },
      {
        "Title": "subscription",
        "StaticName": "subscription",
        "FieldTypeKind": 7,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "true",
        "LookupField": "ID",
        "LookupListName": "Dashboard - Subscriptions",
        "LookupOtherColumns": [ "type", "target", "targetDescription" ],
        "RelationshipDeleteBehavior": 1
      }
    ]
  },

  {
    "_action": "addListField",
    "Title": "Dashboard - Notes",
    "field": {
      "Title": "realModifiedAt",
      "StaticName": "realModifiedAt",
      "FieldTypeKind": 4,
      "Required": "true",
      "EnforceUniqueValues": "false",
      "DefaultValue": "[today]",
      "DisplayFormat": 1,
      "Indexed": "true"
    }
  },
  {
    "_action": "addListField",
    "Title": "Dashboard - Notes",
    "field": {
        "Title": "realModifiedBy",
        "StaticName": "realModifiedBy",
        "FieldTypeKind": 20,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
    }
  },

  
  {
    "_action": "addListField",
    "Title": "Dashboard - Notes",
    "field": {
      "Title": "interactionSummary",
      "StaticName": "interactionSummary",
      "FieldTypeKind": 3,
      "NumberOfLines": 15,
      "Required": "false",
      "Indexed": "false"
    }
  },
  {
    "_action": "addListField",
    "Title": "Dashboard - Risks",
    "field": {
      "Title": "interactionSummary",
      "StaticName": "interactionSummary",
      "FieldTypeKind": 3,
      "NumberOfLines": 15,
      "Required": "false",
      "Indexed": "false"
    }
  },
  {
    "_action": "addListField",
    "Title": "Dashboard - Task Data",
    "field": {
      "Title": "interactionSummary",
      "StaticName": "interactionSummary",
      "FieldTypeKind": 3,
      "NumberOfLines": 15,
      "Required": "false",
      "Indexed": "false"
    }
  },
  {
    "_action": "addListField",
    "Title": "Dashboard - Audit Forms Data",
    "field": {
      "Title": "interactionSummary",
      "StaticName": "interactionSummary",
      "FieldTypeKind": 3,
      "NumberOfLines": 15,
      "Required": "false",
      "Indexed": "false"
    }
  },
  {
    "_action": "addListField",
    "Title": "Dashboard - User Settings",
    "field": {
      "Title": "watchNewNote",
      "StaticName": "watchNewNote",
      "FieldTypeKind": 2,
      "Required": "false",
      "EnforceUniqueValues": "false",
      "Indexed": "false"
    }
  },
  {
    "_action": "addListField",
    "Title": "Dashboard - Impact Flags",
    "field": {
      "Title": "sourceUrl",
      "StaticName": "sourceUrl",
      "FieldTypeKind": 2,
      "Required": "false",
      "EnforceUniqueValues": "false",
      "Indexed": "false"
    }
  },
  {
    "_action": "addListField",
    "Title": "Dashboard - Impact Flags",
    "field": {
      "Title": "sourceDescription",
      "StaticName": "sourceDescription",
      "FieldTypeKind": 2,
      "Required": "false",
      "EnforceUniqueValues": "false",
      "Indexed": "false"
    }
  }

]
