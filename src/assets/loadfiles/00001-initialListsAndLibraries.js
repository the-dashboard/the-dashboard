[
  {
    "_action": "createList",
    "Title": "Dashboard - Configuration",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "Value",
        "StaticName": "Value",
        "FieldTypeKind": 2,
        "Required": "true",
        "Indexed": "false"
      }
    ]
  },
  {
    "_action": "createList",
    "Title": "Dashboard - Datafiles",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "fileName",
        "StaticName": "fileName",
        "FieldTypeKind": 2,
        "Required": "true",
        "Indexed": "true",
        "EnforceUniqueValues": "true"
      },
      {
        "Title": "Description",
        "StaticName": "Description",
        "FieldTypeKind": 3,
        "RichText": "false",
        "Required": "false",
        "Indexed": "false"
      },
      {
        "Title": "dataFileFormatJSON",
        "StaticName": "dataFileFormatJSON",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      }
    ]
  },
  {
    "_action": "createList",
    "Title": "Dashboard - Keywords",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "Valid_from",
        "StaticName": "Valid_from",
        "FieldTypeKind": 4,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "DefaultValue": "[today]",
        "DisplayFormat": 1,
        "Indexed": "false"
      },
      {
        "Title": "Valid_to",
        "StaticName": "Valid_to",
        "FieldTypeKind": 4,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "DefaultValue": "2999-12-31T12:00:00Z",
        "DisplayFormat": 1,
        "Indexed": "false"
      }
    ]
  },
  {
    "_action": "createList",
    "Title": "Dashboard - Tree",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "Description",
        "StaticName": "Description",
        "FieldTypeKind": 3,
        "RichText": "false",
        "Required": "false",
        "Indexed": "false"
      },
      {
        "Title": "Valid_from",
        "StaticName": "Valid_from",
        "FieldTypeKind": 4,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "DefaultValue": "[today]",
        "DisplayFormat": 1,
        "Indexed": "false"
      },
      {
        "Title": "Valid_to",
        "StaticName": "Valid_to",
        "FieldTypeKind": 4,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "DefaultValue": "2999-12-31T12:00:00Z",
        "DisplayFormat": 1,
        "Indexed": "false"
      },
      {
        "Title": "Parent",
        "StaticName": "Parent",
        "FieldTypeKind": 7,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "LookupField": "ID",
        "LookupListName": "Dashboard - Tree",
        "LookupOtherColumns": [ "Title" ]
      },
      {
	"Title": "hasDashboard",
	"StaticName": "hasDashboard",
	"FieldTypeKind": 8,
	"Required": "false",
	"EnforceUniqueValues": "false",
	"Indexed": "false"
      },
      {
        "Title": "OrderAsc",
        "StaticName": "OrderAsc",
        "FieldTypeKind": 9,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "DefaultValue": "100",
        "Indexed": "false"
      }
    ]
  },
  {
    "_action": "createList",
    "Title": "Dashboard - Notes",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "noteText",
        "StaticName": "noteText",
        "FieldTypeKind": 3,
        "RichText": "false",
        "Required": "false",
        "Indexed": "false"
      },
      {
        "Title": "Keywords",
        "StaticName": "Keywords",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "noteStatus",
        "StaticName": "noteStatus",
        "FieldTypeKind": 6,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "FillInChoice": "false",
        "Choices": ["Final", "Draft", "Closed", "Reviewed"],
        "DefaultValue": "Final",
        "Indexed": "false"
      },
      {
        "Title": "sourceLink",
        "StaticName": "sourceLink",
        "FieldTypeKind": 2,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "false"
      },
      {
        "Title": "interactionSummary",
        "StaticName": "interactionSummary",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      },
      {
        "Title": "realModifiedAt",
        "StaticName": "realModifiedAt",
        "FieldTypeKind": 4,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "DefaultValue": "[today]",
        "DisplayFormat": 1,
        "Indexed": "true"
      },
      {
        "Title": "realModifiedBy",
        "StaticName": "realModifiedBy",
        "FieldTypeKind": 20,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      }
    ]
  },
  {
    "_action": "createList",
    "Title": "Dashboard - Risks",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "Description",
        "StaticName": "Description",
        "FieldTypeKind": 3,
        "RichText": "false",
        "Required": "false",
        "Indexed": "false"
      },
      {
        "Title": "Category",
        "StaticName": "Category",
        "FieldTypeKind": 2,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "xAxis",
        "StaticName": "xAxis",
        "FieldTypeKind": 9,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "false"
      },
      {
        "Title": "yAxis",
        "StaticName": "yAxis",
        "FieldTypeKind": 9,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "false"
      },
      {
        "Title": "riskStatus",
        "StaticName": "riskStatus",
        "FieldTypeKind": 6,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "FillInChoice": "false",
        "Choices": ["Final", "Draft", "Reviewed"],
        "DefaultValue": "Final",
        "Indexed": "false"
      },
      {
        "Title": "initialID",
        "StaticName": "initialID",
        "FieldTypeKind": 9,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "Valid_from",
        "StaticName": "Valid_from",
        "FieldTypeKind": 4,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "DefaultValue": "[today]",
        "DisplayFormat": 1,
        "Indexed": "false"
      },
      {
        "Title": "Valid_to",
        "StaticName": "Valid_to",
        "FieldTypeKind": 4,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "DefaultValue": "2999-12-31T12:00:00Z",
        "DisplayFormat": 1,
        "Indexed": "false"
      },
      {
        "Title": "interactionSummary",
        "StaticName": "interactionSummary",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      }
    ]
  },
  {
    "_action": "createList",
    "Title": "Dashboard - User Settings",
    "Description": "",
    "EnableVersioning": false,
    "fields": [
      {
        "Title": "userId",
        "StaticName": "userId",
        "FieldTypeKind": 20,
        "Required": "true",
        "EnforceUniqueValues": "true",
        "Indexed": "true"
      },
      {
        "Title": "language",
        "StaticName": "language",
        "FieldTypeKind": 2,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "false"
      },
      {
        "Title": "watchNewNote",
        "StaticName": "watchNewNote",
        "FieldTypeKind": 2,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "false"
      }
    ]
  },
  {
    "_action": "createList",
    "Title": "Dashboard - Views",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "viewKey",
        "StaticName": "viewKey",
        "FieldTypeKind": 2,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "treeElement",
        "StaticName": "treeElement",
        "FieldTypeKind": 7,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "LookupField": "ID",
        "LookupListName": "Dashboard - Tree",
        "LookupOtherColumns": [ "Title" ]
      },
      {
        "Title": "elementType",
        "StaticName": "elementType",
        "FieldTypeKind": 6,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "FillInChoice": "false",
        "Choices": ["notes", "timeSeries", "stackedBars", "scatterplot", "table", "portfolio", "riskTracker", "objectExplorer", "html"],
        "DefaultValue": "notes",
        "Indexed": "false"
      },
      {
        "Title": "elementOptionsJSON",
        "StaticName": "elementOptionsJSON",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      },
      {
        "Title": "orderAsc",
        "StaticName": "orderAsc",
        "FieldTypeKind": 9,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "false"
      }
    ]
  },
  {
    "_action": "createList",
    "Title": "Dashboard - Object Explorer Objects",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "Type",
        "StaticName": "Type",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "Description",
        "StaticName": "Description",
        "FieldTypeKind": 3,
        "NumberOfLines": 6,
        "Required": "false",
        "Indexed": "false"
      },
      {
        "Title": "Link",
        "StaticName": "Link",
        "FieldTypeKind": 2,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "additionalColumnsJSON",
        "StaticName": "additionalColumnsJSON",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      }
    ]
  },
  {
    "_action": "createList",
    "Title": "Dashboard - Object Explorer Mapping",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "MappingKey",
        "StaticName": "MappingKey",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "Element",
        "StaticName": "Element",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "Object",
        "StaticName": "Object",
        "FieldTypeKind": 7,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "LookupField": "ID",
        "LookupListName": "Dashboard - Object Explorer Objects",
        "LookupOtherColumns": [ "Type", "Title" ]
      }
    ]
  },
  {
    "_action": "createList",
    "Title": "Dashboard - Impact Flags",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "sourceEndpoint",
        "StaticName": "sourceEndpoint",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "sourceId",
        "StaticName": "sourceId",
        "FieldTypeKind": 9,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "sourceKeywords",
        "StaticName": "sourceKeywords",
        "FieldTypeKind": 2,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "targetRiskCategory",
        "StaticName": "targetRiskCategory",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "targetRiskInitialId",
        "StaticName": "targetRiskInitialId",
        "FieldTypeKind": 9,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "impactDate",
        "StaticName": "impactDate",
        "FieldTypeKind": 4,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "DefaultValue": "[today]",
        "DisplayFormat": 0,
        "Indexed": "true"
      },
      {
        "Title": "sourceUrl",
        "StaticName": "sourceUrl",
        "FieldTypeKind": 2,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "sourceDescription",
        "StaticName": "sourceDescription",
        "FieldTypeKind": 2,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      }
    ]
  },
  {
    "_action": "createList",
    "Title": "Dashboard - Audit Forms Data",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "auditForm",
        "StaticName": "auditForm",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "sourceData",
        "StaticName": "sourceData",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      },
      {
        "Title": "resultsData",
        "StaticName": "resultsData",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      },
      {
        "Title": "result",
        "StaticName": "result",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "false"
      },
      {
        "Title": "keywords",
        "StaticName": "keywords",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "createdInTask",
        "StaticName": "createdInTask",
        "FieldTypeKind": 9,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "archivedTask",
        "StaticName": "archivedTask",
        "FieldTypeKind": 8,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "createdInElement",
        "StaticName": "createdInElement",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "workflowStatus",
        "StaticName": "workflowStatus",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "false"
      },
      {
        "Title": "workflowChanges",
        "StaticName": "workflowChanges",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      },
      {
        "Title": "assignedTo",
        "StaticName": "assignedTo",
        "FieldTypeKind": 20,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "interactionSummary",
        "StaticName": "interactionSummary",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      }
    ]
  },
  {
    "_action": "createList",
    "Title": "Dashboard - Interactions",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "interactionTime",
        "StaticName": "interactionTime",
        "FieldTypeKind": 4,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "DefaultValue": "[today]",
        "DisplayFormat": 1,
        "Indexed": "true"
      },
      {
        "Title": "user",
        "StaticName": "user",
        "FieldTypeKind": 20,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "targetType",
        "StaticName": "targetType",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "targetId",
        "StaticName": "targetId",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "interactionType",
        "StaticName": "interactionType",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "content",
        "StaticName": "content",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      }
    ]
  },


  {
    "_action": "createList",
    "Title": "Dashboard - Subscriptions",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "user",
        "StaticName": "user",
        "FieldTypeKind": 20,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "type",
        "StaticName": "type",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "target",
        "StaticName": "target",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "targetDescription",
        "StaticName": "targetDescription",
        "FieldTypeKind": 2,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "false"
      },
      {
        "Title": "actionType",
        "StaticName": "actionType",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "false"
      }
    ]
  },

  {
    "_action": "createList",
    "Title": "Dashboard - Newsfeed",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "user",
        "StaticName": "user",
        "FieldTypeKind": 20,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "newsTime",
        "StaticName": "newsTime",
        "FieldTypeKind": 4,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "DefaultValue": "[today]",
        "DisplayFormat": 1,
        "Indexed": "true"
      },
      {
        "Title": "sourceType",
        "StaticName": "sourceType",
        "FieldTypeKind": 2,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "sourceId",
        "StaticName": "sourceId",
        "FieldTypeKind": 2,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "subElement",
        "StaticName": "subElement",
        "FieldTypeKind": 2,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "sourceUrl",
        "StaticName": "sourceUrl",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "false"
      },
      {
        "Title": "actionType",
        "StaticName": "actionType",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "false"
      },
      {
        "Title": "contentVariables",
        "StaticName": "contentVariables",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      },
      {
        "Title": "subscription",
        "StaticName": "subscription",
        "FieldTypeKind": 7,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "true",
        "LookupField": "ID",
        "LookupListName": "Dashboard - Subscriptions",
        "LookupOtherColumns": [ "type", "target", "targetDescription" ],
        "RelationshipDeleteBehavior": 1
      }
    ]
  },
  {
    "_action": "createLibrary",
    "Title": "Dashboard - Task Data",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "TaskDescription",
        "StaticName": "TaskDescription",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      },
      {
        "Title": "DataDate",
        "StaticName": "DataDate",
        "FieldTypeKind": 4,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "DefaultValue": "[today]",
        "DisplayFormat": 0,
        "Indexed": "true"
      },
      {
        "Title": "workflowStatus",
        "StaticName": "workflowStatus",
        "FieldTypeKind": 6,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "FillInChoice": "false",
        "Choices": ["New", "Open", "Done", "Reviewed", "Archived"],
        "DefaultValue": "New",
        "Indexed": "false"
      },
      {
        "Title": "workflowChanges",
        "StaticName": "workflowChanges",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      },
      {
        "Title": "dataSpecification",
        "StaticName": "dataSpecification",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      },
      {
        "Title": "assignedTo",
        "StaticName": "assignedTo",
        "FieldTypeKind": 20,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "interactionSummary",
        "StaticName": "interactionSummary",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      }
    ]
  },
  {
    "_action": "createLibrary",
    "Title": "Dashboard - Task Archive",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "TaskDescription",
        "StaticName": "TaskDescription",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      },
      {
        "Title": "DataDate",
        "StaticName": "DataDate",
        "FieldTypeKind": 4,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "DefaultValue": "[today]",
        "DisplayFormat": 0,
        "Indexed": "true"
      },
      {
        "Title": "workflowStatus",
        "StaticName": "workflowStatus",
        "FieldTypeKind": 6,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "FillInChoice": "false",
        "Choices": ["New", "Open", "Done", "Reviewed", "Archived"],
        "DefaultValue": "New",
        "Indexed": "false"
      },
      {
        "Title": "workflowChanges",
        "StaticName": "workflowChanges",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      },
      {
        "Title": "dataSpecification",
        "StaticName": "dataSpecification",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      },
      {
        "Title": "assignedTo",
        "StaticName": "assignedTo",
        "FieldTypeKind": 20,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      },
      {
        "Title": "originalId",
        "StaticName": "originalId",
        "FieldTypeKind": 9,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      }
    ]
  },
  {
    "_action": "createLibrary",
    "Title": "Dashboard - Task Results",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "taskId",
        "StaticName": "taskId",
        "FieldTypeKind": 9,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      }
    ]
  },
  {
    "_action": "createLibrary",
    "Title": "Dashboard - Task Archive Results",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "taskId",
        "StaticName": "taskId",
        "FieldTypeKind": 9,
        "Required": "true",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      }
    ]
  },
  {
    "_action": "createLibrary",
    "Title": "Dashboard - Audit Forms",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "shortKey",
        "StaticName": "shortKey",
        "FieldTypeKind": 2,
        "Required": "true",
        "EnforceUniqueValues": "true",
        "Indexed": "true"
      },
      {
        "Title": "tabs",
        "StaticName": "tabs",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      },
      {
        "Title": "additionalSources",
        "StaticName": "additionalSources",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "false",
        "Indexed": "false"
      },
      {
        "Title": "resultExpression",
        "StaticName": "resultExpression",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "true",
        "Indexed": "false"
      },
      {
        "Title": "newFormIcon",
        "StaticName": "newFormIcon",
        "FieldTypeKind": 2,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "false"
      },
      {
        "Title": "iconMap",
        "StaticName": "iconMap",
        "FieldTypeKind": 3,
        "NumberOfLines": 15,
        "Required": "true",
        "Indexed": "false"
      }
    ]
  },
  {
    "_action": "createList",
    "Title": "Dashboard - Feedback",
    "Description": "",
    "EnableVersioning": true,
    "fields": [
      {
        "Title": "Feedback",
        "StaticName": "Feedback",
        "FieldTypeKind": 3,
        "RichText": "false",
        "Required": "false",
        "Indexed": "false"
      }
    ]
  }
]
