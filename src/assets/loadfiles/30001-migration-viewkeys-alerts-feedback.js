[
  {
    "_action": "addListField",
    "Title": "Dashboard - Views",
    "field": {
        "Title": "viewKey",
        "StaticName": "viewKey",
        "FieldTypeKind": 2,
        "Required": "false",
        "EnforceUniqueValues": "false",
        "Indexed": "true"
      }
  },
  {
    "_action": "addListField",
    "Title": "Dashboard - Notes",
    "field": {
      "Title": "sourceLink",
      "StaticName": "sourceLink",
      "FieldTypeKind": 2,
      "Required": "false",
      "EnforceUniqueValues": "false",
      "Indexed": "false"
    }
  },
  {
    "_action": "addListField",
    "Title": "Dashboard - Notes",
    "field": {
      "Title": "sourceLink",
      "StaticName": "sourceLink",
      "FieldTypeKind": 2,
      "Required": "false",
      "EnforceUniqueValues": "false",
      "Indexed": "false"
    }
  },
  {
    "_action": "addListField",
    "Title": "Dashboard - User Alert Triggers",
    "field": {
      "Title": "configJson",
      "StaticName": "configJson",
      "FieldTypeKind": 3,
      "NumberOfLines": 6,
      "Required": "false",
      "Indexed": "false"
    }
  },
  {
  "_action": "createList",
  "Title": "Dashboard - Feedback",
  "Description": "",
  "EnableVersioning": true,
  "fields": [
      {
      "Title": "Feedback",
      "StaticName": "Feedback",
      "FieldTypeKind": 3,
      "RichText": "false",
      "Required": "false",
      "Indexed": "false"
      }
    ]
  }
]
