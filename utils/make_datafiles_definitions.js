// Use path and fs
const path = require('path');
const fs = require('fs');

const SEPARATOR = ',';
const FILE_ENDING = '.csv';

// Has a directory been given on the command line?
// Otherwise show information message.
var targetDir = process.argv[2];

if(targetDir === undefined) {
  console.log('You need to specify the path that will be searched for CSV files.');
  process.exit(1);
}


// Helper function that retrieves a single line from a large file
// From: https://stackoverflow.com/questions/6394951/read-nth-line-of-file-in-nodejs
function get_line(filename, line_no, callback) {
  var stream = fs.createReadStream(filename, {
    flags: 'r',
    encoding: 'utf-8',
    fd: null,
    mode: 0666,
    bufferSize: 64 * 1024
  });

  var fileData = '';
  stream.on('data', function(data){
    fileData += data;

    // The next lines should be improved
    var lines = fileData.split("\n");

    if(lines.length > +line_no){
      stream.destroy();
      callback(null, lines[+line_no]);
    }
  });

  stream.on('error', function(){
    callback('Error', null);
  });

  stream.on('end', function(){
    callback('File end reached without finding line', null);
  });

}


// Helper function that processes file
var processFile = function(fileName) {
  get_line(fileName, 0, function(err, line) {
    if(err) {
      console.log('Error reading file %s: %s', fileName, err);
      process.exit(1);
    }

    var columns = line.split(SEPARATOR);

    process.stdout.write('{\n');
    process.stdout.write('  "_endpoint": "datafiles",\n');
    process.stdout.write('  "Title": "' + path.basename(fileName, FILE_ENDING) + '",\n');
    process.stdout.write('  "Description": "<MANUALLY ADD DESCRIPTION HERE>",\n');
    process.stdout.write('  "fileName": "' + fileName + '",\n');
    process.stdout.write('  "dataFileFormatJSON": "{\\n \\"separator\\": \\"' + SEPARATOR + '\\",\\n \\"columns\\": [');

    columns.forEach(function(c, idx) {
      var withoutEnclosingQuotes = c.replace(/^"(.+)"$/g, "$1");
      var escaped = withoutEnclosingQuotes.replace('"', '\\\\\\"');
      var cleaned = withoutEnclosingQuotes.replace(/[^a-zA-Z0-9_]/g, '_');

      if(idx > 0) {
        process.stdout.write(', ');
      }
      process.stdout.write('{\\n \\"source\\": \\"' + escaped + '\\",\\n \\"display\\": \\"' + escaped + '\\",\\n \\"keyword\\": \\"' + cleaned + '\\",\\n \\"format\\": \\"<SET MANUALLY>\\"\\n }');
    });

    process.stdout.write(']\\n}"\n');
    process.stdout.write('},\n');
  });
};


// Read the directory
fs.readdir(targetDir, function (err, list) {
  // Fail and quit if something goes wrong.
  if (err) {
    console.log('An error occured reading the directory: %s', err);
    process.exit(1);
  }

  // For every file in the list
  list.forEach(function (file) {
    // Full path of that file
    var fileName = path.join(targetDir, file);


    // Get the file's stats
    fs.stat(fileName, function (err, stat) {
      if (stat && !stat.isDirectory() && fileName.substr(-1 * FILE_ENDING.length) === FILE_ENDING) {
        // This is a hit!
        processFile(fileName);
      }
    });
  });
});

